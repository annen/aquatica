package com.aquatica;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;

public class MainFrame extends JFrame {

	private static final int MAX_PNG_SIZE = 1024 * 1024;
	private JPanel contentPane;
	private javax.swing.JTextArea text;
	private JCheckBox chckbxSaveOriginal;
	private JCheckBox chckbxResizeImage;
	private JCheckBox chckbxPreparedImageName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		chckbxPreparedImageName = new JCheckBox("Prepared Image Name");
		chckbxPreparedImageName.setSelected(true);
		panel.add(chckbxPreparedImageName);
		
		chckbxResizeImage = new JCheckBox("Resize Image");
		chckbxResizeImage.setSelected(true);
		panel.add(chckbxResizeImage);
		
		chckbxSaveOriginal = new JCheckBox("Save Original");
		chckbxSaveOriginal.setSelected(true);
		panel.add(chckbxSaveOriginal);
		
		final JProgressBar progressBar = new JProgressBar();
		contentPane.add(progressBar, BorderLayout.SOUTH);
		
		javax.swing.border.TitledBorder dragBorder = new javax.swing.border.TitledBorder("Drop files");
		text = new javax.swing.JTextArea();
		contentPane.add(new javax.swing.JScrollPane(text), java.awt.BorderLayout.CENTER);
		
		new FileDrop(System.out, text, dragBorder, new FileDrop.Listener() {
			@Override
			public void filesDropped(final java.io.File[] files) {
				text.setText("");
				progressBar.setMaximum(files.length);
				progressBar.setValue(0);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						for (int i = 0; i < files.length; i++) {
							File file = files[i];
							try {
								if (!file.isDirectory()) {
									proccessFile(file);
								} else {
									for (File fileInFolder : file.listFiles()) {
										proccessFile(fileInFolder);
									}
								}
								progressBar.setValue(i + 1);
							} catch (Exception e) {
								setStatus(file.getAbsolutePath() + " ERROR\n");
							}
						}
					}
					
					private void proccessFile(File file) throws Exception {
						text.append(file.getAbsolutePath() + " Wait Process...\n");
						String filename = file.getName();
						String path = file.getAbsolutePath();
						String extension = getExt(filename);
						String name = filename.substring(0, filename.length() - extension.length() - 1);
						boolean isPng = extension.equals("png");
						boolean isJpg = extension.equals("jpg") || extension.equals("jpeg");
						if (!isJpg && !isPng) {
							setStatus(path + " Skipped");
							return;
						}
						
						boolean tmpFile = chckbxSaveOriginal.isSelected();
						File workFile = file;
						String newName = filename;
						
						File parent = file.getParentFile();
						if (tmpFile) {
							if (chckbxPreparedImageName.isSelected()) {
								name = getValidName(name);
							}
							newName = findNewName(name, extension, parent);
							workFile = File.createTempFile(name + "~", "." + extension);
							Files.copy(file.toPath(), workFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						} else {
							if (chckbxPreparedImageName.isSelected()) {
								String validName = getValidName(name);
								if (!name.equals(validName)) {
									name = validName;
									newName = findNewName(validName, extension, parent);
									File newFile = new File(parent, newName);
									file.renameTo(newFile);
									file = newFile;
									setStatus(file.getAbsolutePath() + " Renamed to " + validName);
								}
							}
							workFile = file;
							newName = file.getName();
						}
						String appDir = new File("").getAbsolutePath();
						
						if (chckbxResizeImage.isSelected()) {
							setStatus(path + " Resize...");
							runProccess(appDir + "\\magick\\magick.exe \"%FILE%\" -resize 1280x800 \"%FILE%\"".replace("%FILE%", workFile.getAbsolutePath()));
							setStatus(path + " Resized");
						}
						long size = workFile.length();
						
						if (isPng && size > MAX_PNG_SIZE) {
							setStatus(path + " Convert To JPG...");
							extension = "jpg";
							isJpg = true;
							isPng = false;
							File oldWorkFile = workFile;
							newName = findNewName(name, "jpg", parent);
							if (tmpFile) {
								workFile = File.createTempFile(name + "~", "." + extension);
								workFile.delete();
							} else {
								workFile = new File(parent, newName);
							}
							runProccess(appDir + "\\magick\\magick.exe convert \"%FILE_OLD%\" -background white -flatten \"%FILE%\""
									.replace("%FILE_OLD%", oldWorkFile.getAbsolutePath())
									.replace("%FILE%", workFile.getAbsolutePath()));
							oldWorkFile.delete();
							setStatus(path + " Converted To JPG...");
						}
						
						if (isPng) {
							setStatus(path + " Optimize...");
							// runProccess(appDir + "\\magick\\magick.exe convert \"%FILE%\" -strip \"%FILE%\"".replace("%FILE%", workFile.getAbsolutePath()));
							runProccess(appDir + "\\optipng -strip all -o3 \"%FILE%\"".replace("%FILE%", workFile.getAbsolutePath()));
							setStatus(path + " Optimized...");
						}
						if (isJpg) {
							setStatus(path + " Optimize...");
							runProccess(appDir + "\\magick\\magick.exe convert \"%FILE%\" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG \"%FILE%\"".replace("%FILE%", workFile.getAbsolutePath()));
							// runProccess(appDir + "\\jpegoptim --strip-all --all-progressive -pm85 \"%FILE%\"".replace("%FILE%", workFile.getAbsolutePath()));
							setStatus(path + " Optimized...");
						}
						if (tmpFile) {
							Files.copy(workFile.toPath(), new File(parent, newName).toPath());
							workFile.delete();
						}
						setStatus(path + " DONE");
					}
					
				});
				t.setDaemon(true);
				t.start();
			}
		});
	}
	
	private String getValidName(String fileName) {
		fileName = fileName.trim().replaceAll("\\s+", "-");
		fileName = fileName.replaceAll("-+", "-");
		StringBuilder sb = new StringBuilder(fileName);
		for (int i = 0; i < sb.length();) {
			char ch = sb.charAt(i);
			if (wrongLetter(ch)) {
				sb.deleteCharAt(i);
			} else {
				i++;
			}
		}
		fileName = sb.toString().trim();
		fileName = fileName.replaceAll("\\s+", "-");
		fileName = fileName.replaceAll("-+", "-");
		fileName = fileName.replaceAll("_+", "_");
		return fileName;
	}
	
	private boolean wrongLetter(char letter) {
		if (letter == 32 || // " "
				letter == 40 || // (
				letter == 41 || // )
				letter == 45 || // -
				letter == 46 || // .
				letter == 95 || // _
				(letter >= 48 && letter <= 57) || // 0-9
				(letter >= 65 && letter <= 90) || // A-Z
				(letter >= 97 && letter <= 122)) // a-z
			return false;
		return true;
		
		// [^\\w-.()]
	}

	private String findNewName(String name, String extension, File parent) {
		if (name.endsWith("-")) {
			name = name.substring(0, name.length() - 1);
		}
		String newName = name + "-(web)." + extension;
		File newFile = new File(parent, newName);
		int i = 2;
		while (newFile.exists()) {
			newName = name + "-(web-" + i + ")." + extension;
			newFile = new File(parent, newName);
			i++;
		}
		return newName;
	}

	private static void runProccess(String command) {
		try {
			ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", command).inheritIO();
			Process p = pb.start();
			p.waitFor();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static String getExt(String filename) {
		int index = filename.lastIndexOf(".");
		if (index < 0)
			return "";
		return filename.substring(index + 1).toLowerCase();
	}
	
	private void setStatus(String newText) {
		Document doc = text.getDocument();
		Element root = doc.getDefaultRootElement();
		int count = root.getElementCount();
		if (count < 2) {
			text.append(newText);
			return;
		}

		Element contentEl = root.getElement(count - 2);
		
		int start = contentEl.getStartOffset();
		int end = contentEl.getEndOffset();
		
		try {
			doc.remove(start, end - start);
			doc.insertString(start, newText + "\n", null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

}
