import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

public class AquaticaDeployer extends JFrame {
	
	private JPanel contentPane;
	private JCheckBox chckbxDev;
	private JCheckBox chckbxUsa;
	private JCheckBox chckbxEu;
	private JCheckBox chckbxRu;
	private JCheckBox chckbxCa;
	private JCheckBox chckbxGb;
	private JCheckBox chckbxIt;
	private JCheckBox chckbxMx;
	private JCheckBox chckbxEs;
	private JCheckBox chckbxFr;
	private JCheckBox chckbxDe;
	private JCheckBox chckbxAe;
	private JCheckBox chckbxAa;
	private JTextArea txtrOutput;
	private JPanel panel_3;
	private JTextField txtHomeFolder;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					AquaticaDeployer frame = new AquaticaDeployer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public AquaticaDeployer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.WEST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		chckbxDev = new JCheckBox("Aquatica DEV");
		panel.add(chckbxDev);
		
		chckbxUsa = new JCheckBox("Aquatica USA");
		panel.add(chckbxUsa);
		
		chckbxRu = new JCheckBox("Aquatica RU");
		panel.add(chckbxRu);
		
		chckbxCa = new JCheckBox("Aquatica CA");
		panel.add(chckbxCa);
		
		chckbxEu = new JCheckBox("Aquatica EU");
		panel.add(chckbxEu);
		
		chckbxGb = new JCheckBox("Aquatica UK");
		panel.add(chckbxGb);
		
		chckbxIt = new JCheckBox("Aquatica IT");
		panel.add(chckbxIt);
		
		chckbxMx = new JCheckBox("Aquatica MX");
		panel.add(chckbxMx);
		
		chckbxEs = new JCheckBox("Aquatica ES");
		panel.add(chckbxEs);
		
		chckbxFr = new JCheckBox("Aquatica FR");
		panel.add(chckbxFr);
		
		chckbxDe = new JCheckBox("Aquatica DE");
		panel.add(chckbxDe);
		
		chckbxAe = new JCheckBox("Aquatica AE");
		panel.add(chckbxAe);
		
		chckbxAa = new JCheckBox("Aquatica AA");
		panel.add(chckbxAa);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel.add(verticalStrut);
		
		JPanel panel_2 = new JPanel();
		panel_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel.add(panel_2);
		
		JButton btnDoit = new JButton("Do It");
		btnDoit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						if (chckbxDev.isSelected()) {
							boolean deploy = deploy("dev.aquaticaplumbing.com", "dev-capricornus");
							chckbxDev.setForeground(deploy ? Color.green : Color.red);
						}
						if (chckbxUsa.isSelected()) {
							boolean deploy = deploy("aquaticausa.com", "master_aquaticausa");
							chckbxUsa.setForeground(deploy ? Color.green : Color.red);
						}
						if (chckbxRu.isSelected()) {
							boolean deploy = deploy("vivalusso.ru", "master_aquaticaru");
							chckbxRu.setForeground(deploy ? Color.green : Color.red);
						}
						if (chckbxCa.isSelected()) {
							boolean deploy = deploy("aquaticabath.ca", "master_aquaticaca");
							chckbxCa.setForeground(deploy ? Color.green : Color.red);
						}
						
						if (chckbxEu.isSelected()) {
							boolean deploy = deploy("aquaticabath.eu", "master_eu");
							chckbxEu.setForeground(deploy ? Color.green : Color.red);
						}

						if (chckbxGb.isSelected()) {
							boolean deploy = deploy("aquaticabath.co.uk", "master_aquaticauk");
							chckbxGb.setForeground(deploy ? Color.green : Color.red);
						}
						
						if (chckbxIt.isSelected()) {
							boolean deploy = deploy("aquaticabagno.it", "master_it");
							chckbxIt.setForeground(deploy ? Color.green : Color.red);
						}
						
						if (chckbxMx.isSelected()) {
							boolean deploy = deploy("aquaticabano.com.mx", "master_aquaticamx");
							chckbxMx.setForeground(deploy ? Color.green : Color.red);
						}
						
						if (chckbxEs.isSelected()) {
							boolean deploy = deploy("aquaticabano.com", "master_aquaticaes");
							chckbxEs.setForeground(deploy ? Color.green : Color.red);
						}

						if (chckbxFr.isSelected()) {
							boolean deploy = deploy("aquaticabain.fr", "master_aquaticafr");
							chckbxFr.setForeground(deploy ? Color.green : Color.red);
						}

						if (chckbxDe.isSelected()) {
							boolean deploy = deploy("aquaticabad.de", "master_aquaticade");
							chckbxDe.setForeground(deploy ? Color.green : Color.red);
						}

						if (chckbxAe.isSelected()) {
							boolean deploy = deploy("en.aquaticamena.com", "master_aquaticaae");
							chckbxAe.setForeground(deploy ? Color.green : Color.red);
						}

						if (chckbxAa.isSelected()) {
							boolean deploy = deploy("ar.aquaticamena.com", "master_aquaticaaa");
							chckbxAa.setForeground(deploy ? Color.green : Color.red);
						}
					}
				});
				t.start();
			}
		});
		panel_2.add(btnDoit);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new CardLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "scrollPane");
		
		txtrOutput = new JTextArea();
		scrollPane.setViewportView(txtrOutput);
		DefaultCaret caret = (DefaultCaret) txtrOutput.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		panel_3 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_3, BorderLayout.NORTH);
		
		txtHomeFolder = new JTextField();
		txtHomeFolder.setText("D:\\AQUATICA\\aquatica");
		panel_3.add(txtHomeFolder);
		txtHomeFolder.setColumns(30);
	}
	
	private boolean deploy(String host, String branch) {
		try {
			boolean isDev = "dev-capricornus".equalsIgnoreCase(branch);
			
			StringBuffer sb = new StringBuffer();
			Process process = Runtime.getRuntime().exec("cmd.exe");
			final OutputStream stdin = process.getOutputStream();
			final InputStream stderr = process.getErrorStream();
			final InputStream stdout = process.getInputStream();
			
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String line;
						BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stdout));
						while ((line = brCleanUp.readLine()) != null) {
							sb.append(line + '\n');
							txtrOutput.append(line + '\n');
						}
					} catch (Exception ex) {
						txtrOutput.append(ex.getMessage());
						ex.printStackTrace();
					}
				}
			});
			t.setDaemon(true);
			t.start();
			
			Thread terror = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String line;
						BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stderr));
						while ((line = brCleanUp.readLine()) != null) {
							sb.append(line + '\n');
							txtrOutput.append(line + '\n');
						}
					} catch (Exception ex) {
						txtrOutput.append(ex.getMessage());
						ex.printStackTrace();
					}
				}
			});
			terror.setDaemon(true);
			terror.start();
			Thread.sleep(1000);
			runCommand(stdin, "D:");
			runCommand(stdin, "cd " + txtHomeFolder.getText());
			runCommand(stdin, "git fetch origin dev-capricornus:" + branch);
			runCommand(stdin, "git push origin " + branch + ":" + branch);
			
			runCommand(stdin, "cd .\\deploy\\docker");
			
			runCommand(stdin, "berks vendor cookbooks");
			
			boolean repeat = false;
			boolean deploy = false;
			for (int i = 0; i < 10; i++) {
				repeat = false;
				if (isDev) {
					// runCommand(stdin, "ssh root@" + host + " -i C:/Users/Annenkov/.ssh/id_rsa -t \"cd /var/www/aquatica-new/; git reset --hard\"");
					String[] params = new String[] { "ssh", "root@dev.aquaticaplumbing.com", "-i", "C:/Users/Annenkov/.ssh/id_rsa", "-t", "\"cd /var/www/aquatica-new/; git reset --hard\"" };
					Runtime.getRuntime().exec(params).waitFor();
					runCommand(stdin, "knife solo cook -E stage root@" + host + " -i C:/Users/Annenkov/.ssh/id_rsa");
				} else {
					runCommand(stdin, "knife solo cook root@" + host + " -i C:/Users/Annenkov/.ssh/id_rsa");
				}
				
				for (int sec = 0; sec < (10 * 60); sec++) {
					sb.setLength(0);
					Thread.sleep(1000);
					String out = sb.toString().toLowerCase();
					if (out.contains("chef client finished")) {
						deploy = true;
						break;
					}
					
					if (out.contains("bignum too big to convert into")) {
						repeat = true;
						break;
					}
					
					if (out.contains("host key verification failed")
							|| out.contains("rsync error")
							|| out.contains("failed to launch command")) {
						break;
					}
					
				}
				
				if (!repeat) {
					break;
				}
			}
			
			txtrOutput.append((deploy ? "Done: " : "Error: ") + host);
			return deploy;
		} catch (Exception ex) {
			txtrOutput.append(ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}
	
	public void runCommand(final OutputStream stdin, String command) throws Exception {
		stdin.write((command + "\r\n").getBytes());
		stdin.flush();
		Thread.sleep(1000);
	}
}
