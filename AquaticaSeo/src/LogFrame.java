

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.*;

public class LogFrame extends JFrame {
	
	private static final long serialVersionUID = 1;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JScrollPane scrollPaneFilter;
	private JList<LogValue> list;
	private JList<LogValue> listFilter;
	private volatile DefaultListModel<LogValue> model;
	private volatile DefaultListModel<LogValue> modelFilter;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					LogFrame frame = new LogFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	boolean autoScrool = true;
	
	private final ConcurrentLinkedQueue<LogValue> queue = new ConcurrentLinkedQueue<LogValue>();
	private JPanel panel;
	private JLabel lblFilterStatus;
	private JComboBox<String> filterBox;
	private DefaultComboBoxModel<String> filterModel;
	private DefaultComboBoxModel<String> botFilterModel;
	private Set<String> bots = new HashSet<String>();
	
	private DefaultListCellRenderer cellRenderer = new DefaultListCellRenderer() {
		
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			setForeground(Color.BLACK);
			setBackground(Color.white);
			final String str = value == null ? "" : value.toString();
			if (str.contains("WARNING:")) {
				setBackground(Color.YELLOW);
			} else if (str.contains("ERROR:")) {
				setBackground(Color.PINK);
			} else if (str.contains("INFO:")) {
				setForeground(Color.gray);
			} else if (str.contains("SUCCESS:")) {
				setBackground(new Color(50, 250, 50));
			} else if (str.contains("ORACLE:")) {
				setBackground(new Color(50, 250, 250));
			}
			return component;
		}
	};
	
	private MouseAdapter listMouseAdapter = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				LogValue value = list.getSelectedValue();
				String str = value == null ? "" : value.toString();
				if (str.contains("http")) {
					String url = str.substring(str.indexOf("http"), Math.max(str.indexOf(' ', str.indexOf("http")), str.length()));
					try {
						Desktop.getDesktop().browse(new URI(url));
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with open url " + url);
					}
				}
			}
		}
	};
	
	private MouseAdapter scroolMouseListener = new MouseAdapter() {
		
		@Override
		public void mouseExited(MouseEvent e) {
			autoScrool = true;
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			autoScrool = false;
		}
	};
	private JLabel lblBotFilter;
	private JComboBox<String> botFilterBox;
	
	public LogFrame() {
		setType(Type.POPUP);
		setTitle("Log");
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(size.width - 450, size.height - 230, 450, 200);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		final JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new CardLayout(0, 0));
		
		model = new DefaultListModel<LogValue>();
		scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "MainList");
		list = new JList<LogValue>(model);
		
		list.setCellRenderer(cellRenderer);
		
		list.addMouseListener(listMouseAdapter);
		scrollPane.setViewportView(list);
		list.addMouseListener(scroolMouseListener);
		scrollPane.addMouseListener(scroolMouseListener);
		scrollPane.getVerticalScrollBar().addMouseListener(scroolMouseListener);
		scrollPane.getHorizontalScrollBar().addMouseListener(scroolMouseListener);
		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (autoScrool) {
					e.getAdjustable().setValue(e.getAdjustable().getMaximum());
				}
			}
		});
		
		modelFilter = new DefaultListModel<LogValue>();
		scrollPaneFilter = new JScrollPane();
		panel_1.add(scrollPaneFilter, "FilterList");
		listFilter = new JList<LogValue>(modelFilter);
		
		listFilter.setCellRenderer(cellRenderer);
		
		listFilter.addMouseListener(listMouseAdapter);
		scrollPaneFilter.setViewportView(listFilter);
		listFilter.addMouseListener(scroolMouseListener);
		scrollPaneFilter.addMouseListener(scroolMouseListener);
		scrollPaneFilter.getVerticalScrollBar().addMouseListener(scroolMouseListener);
		scrollPaneFilter.getHorizontalScrollBar().addMouseListener(scroolMouseListener);
		scrollPaneFilter.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (autoScrool) {
					e.getAdjustable().setValue(e.getAdjustable().getMaximum());
				}
			}
		});
		
		panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		lblFilterStatus = new JLabel("Status Filter: ");
		panel.add(lblFilterStatus);
		
		filterBox = new JComboBox<String>();
		ActionListener filterAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (filterBox.getSelectedIndex() == 0 && botFilterBox.getSelectedIndex() == 0) {
					CardLayout cardLayout = (CardLayout) panel_1.getLayout();
					cardLayout.first(panel_1);
				} else {
					String selectedItem = (String) filterBox.getSelectedItem();
					boolean filterStatus = filterBox.getSelectedIndex() != 0;
					String selectedBot = (String) botFilterBox.getSelectedItem();
					boolean filterBot = botFilterBox.getSelectedIndex() != 0;
					modelFilter.clear();
					for (int i = 0; i < model.size(); i++) {
						LogValue item = model.getElementAt(i);
						String value = item.value;
						if (filterStatus && !value.contains(selectedItem + ":")) {
							continue;
						}
						if (filterBot && !value.contains(selectedBot + ":")) {
							continue;
						}
						modelFilter.addElement(item);
					}
					CardLayout cardLayout = (CardLayout) panel_1.getLayout();
					cardLayout.last(panel_1);
				}
			}
		};
		filterBox.addActionListener(filterAction);
		
		filterModel = new DefaultComboBoxModel<String>();
		filterModel.addElement("ALL");
		filterModel.addElement("SUCCESS");
		filterModel.addElement("INFO");
		filterModel.addElement("WARNING");
		filterModel.addElement("ERROR");
		filterModel.addElement("ORACLE");
		filterBox.setModel(filterModel);
		panel.add(filterBox);
		
		lblBotFilter = new JLabel("Bot Filter");
		panel.add(lblBotFilter);
		
		botFilterModel = new DefaultComboBoxModel<String>();
		botFilterModel.addElement("ALL");
		
		botFilterBox = new JComboBox<String>();
		botFilterBox.setModel(botFilterModel);
		botFilterBox.addActionListener(filterAction);
		panel.add(botFilterBox);
		
		final Thread updater = new Thread() {
			@Override
			public void run() {
				while (true) {
					while (!queue.isEmpty()) {
						final LogValue value = queue.poll();
						if (value != null) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									value.addTo(model);
								}
							});
						}
					}
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
				}
			}
		};
		updater.setDaemon(true);
		updater.start();
	}
	
	public LogValue log(String value) {
		LogValue logValue = new LogValue(prepareValue(value));
		queue.offer(logValue);
		String name = Thread.currentThread().getName();
		if (name.startsWith("~")) {
			if (!bots.contains(name)) {
				botFilterModel.addElement(name.substring(1));
				bots.add(name);
			}
		}
		return logValue;
	}
	
	static String prepareValue(String value) {
		value = value == null ? "" : value;
		String name = Thread.currentThread().getName();
		if (name.startsWith("~")) {
			value = name.substring(1) + ": " + value;
		}
		return value.replace('\t', ' ');
	}
	
}
