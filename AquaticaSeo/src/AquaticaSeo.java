import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Charsets;

public class AquaticaSeo extends JFrame {
	
	private enum PageType {
		MAIN, CATEGORY, PRODUCT, BLOG, ITESSET, GENERIC;
	}
	
	public static class Task {
		public int number = 0;
		public String url;
		public PageType type;
		public String title;
		String pageSource;
		public boolean sended;
		public boolean sitemap = true;
		
		public Task() {
		}
		
		public Task(int number, String url, PageType type) {
			this.number = number;
			this.url = url;
			this.type = type;
		}
		
		String getHost() {
			return AquaticaSeo.getHost(url);
		}
		
		String getDomain() {
			return getHost().replace("https", "").replace("http", "").replace("://", "").replace("www.", "").replace("/", "");
		}
		
		String getPath() {
			return AquaticaSeo.getPath(url);
		}
		
		void complete(String pageSource) {
			this.pageSource = pageSource;
		}
		
		public String getPageSource() {
			return pageSource;
		}
		
		@Override
		public int hashCode() {
			return url.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof Task))
				return false;
			return ((Task) obj).url.equals(url);
		}
		
		@Override
		public String toString() {
			return number + ": [" + type.name() + "] " + url;
		}
	}
	
	public static class ScanProcess {
		public int taskNum = 0;
		public List<Task> allTasks = new ArrayList<>();
		
		public ScanProcess() {
		}
		
		public Task addTask(String url, PageType type) {
			Task task = new Task(allTasks.size(), url, type);
			if (allTasks.contains(task))
				return null;
			allTasks.add(task);
			return task;
		}
		
		public Task nextTask() {
			if (hasTask())
				return allTasks.get(taskNum);
			return null;
		}
		
		public void taskProcessed() {
			taskNum++;
		}
		
		public boolean hasTask() {
			return taskNum < allTasks.size();
		}
		
		public Task findTaskForSave() {
			for (Task task : allTasks) {
				if (!task.sended && task.pageSource != null && !task.pageSource.isEmpty())
					return task;
			}
			return null;
		}
	}
	
	private static final String DEFAULT_DRIVER_PATH = "D:/Aquatica/chromedriver.exe";
	private static final String CHROME_WEBDRIVER_PATH = "webdriver.chrome.driver";
	private static final Pattern patternLinkRel = Pattern.compile("<link[^>]+hreflang[^>]+href\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>\\s*", Pattern.DOTALL + Pattern.CASE_INSENSITIVE);
	private JPanel contentPane;
	private JTable table;
	private final JComboBox<String> comboBox = new JComboBox<>();
	private JButton btnScan;
	private ChromeDriver driver;
	private DefaultTableModel model;
	private JPanel panel_1;
	private JButton btnContinue;
	private ScanProcess process = new ScanProcess();
	private File cacheFile = new File("./process.json");
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JCheckBox chckbxProducts;
	private JCheckBox chckbxBlogs;
	private JCheckBox chckbxCategories;
	private JCheckBox chckbxGenerics;
	private JProgressBar progressBar;
	private FromUrls fromUrls = new FromUrls(this);
	private CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new RedirectStrategy() {
		
		@Override
		public boolean isRedirected(HttpRequest arg0, HttpResponse arg1, HttpContext arg2) throws ProtocolException {
			return false;
		}
		
		@Override
		public HttpUriRequest getRedirect(HttpRequest arg0, HttpResponse arg1, HttpContext arg2) throws ProtocolException {
			return null;
		}
	}).build();
	private Set<String> skipped = new HashSet<>();
	private Set<String> knowed = new HashSet<>();
	private Collection<String> hosts = Arrays.asList("https://www.aquaticausa.com",
			"https://www.aquaticabath.eu",
			"https://www.vivalusso.ru",
			"https://www.aquaticabath.ca",
			"https://www.aquaticabath.co.uk",
			"https://www.aquaticabagno.it",
			"https://www.aquaticabano.com.mx",
			"https://dev.aquaticaplumbing.com",
			"https://beta.aquaticaplumbing.com",
			"http://aquatica.local");
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					AquaticaSeo frame = new AquaticaSeo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public AquaticaSeo() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		comboBox.setModel(
				new DefaultComboBoxModel<>(hosts.toArray(new String[hosts.size()])));
		comboBox.setEditable(true);
		panel.add(comboBox, BorderLayout.CENTER);
		
		panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.EAST);
		
		btnScan = new JButton("Scan");
		panel_1.add(btnScan);
		
		btnContinue = new JButton("Continue");
		btnContinue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							scanContinue();
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}
				});
				t.setDaemon(true);
				t.setName("~Scanner");
				t.start();
			}
		});
		panel_1.add(btnContinue);
		
		btnFromFile = new JButton("From File");
		btnFromFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							LOG.logInfo("Clear cache file " + cacheFile.getAbsolutePath());
							cacheFile.delete();
							File urlFile = new File("./urls.txt");
							if (!urlFile.exists()) {
								JOptionPane.showMessageDialog(null, "Not found file " + urlFile.getAbsolutePath());
								return;
							}
							List<String> lines = Files.readAllLines(urlFile.toPath(), Charsets.UTF_8);
							if (lines.isEmpty()) {
								JOptionPane.showMessageDialog(null, "Not found lines in file " + urlFile.getAbsolutePath());
								return;
							}
							scanUrls(lines, true);
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}
				});
				t.setDaemon(true);
				t.setName("~Scanner");
				t.start();
			}
		});
		
		btnFromUrls = new JButton("From Urls");
		btnFromUrls.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fromUrls.setVisible(true);
			}
		});
		panel_1.add(btnFromUrls);
		panel_1.add(btnFromFile);
		
		btnScanall = new JButton("ScanAll");
		btnScanall.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							LOG.logInfo("Clear cache file " + cacheFile.getAbsolutePath());
							cacheFile.delete();
							String[] hosts = new String[] { "https://www.aquaticausa.com",
									"https://www.aquaticabath.eu",
									"https://www.vivalusso.ru",
									"https://www.aquaticabath.ca",
									"https://www.aquaticabath.co.uk",
									"https://www.aquaticabagno.it",
									"https://www.aquaticabano.com.mx" };
							process = new ScanProcess();
							for (String host : hosts) {
								addTask(host, PageType.MAIN);
							}
							scan();
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}
				});
				t.setDaemon(true);
				t.setName("~Scanner");
				t.start();
			}
		});
		panel_1.add(btnScanall);
		btnScan.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							scanNewSite((String) comboBox.getSelectedItem());
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}
				});
				t.setDaemon(true);
				t.setName("~Scanner");
				t.start();
			}
			
		});
		
		createTable();
		contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new CardLayout(0, 0));
		
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		panel_3.add(progressBar, "name_366593053942650");
		
		panel_4 = new JPanel();
		panel_2.add(panel_4, BorderLayout.EAST);
		
		chckbxProducts = new JCheckBox("PRODUCTS");
		chckbxProducts.setSelected(true);
		panel_4.add(chckbxProducts);
		
		chckbxCategories = new JCheckBox("CATEGORIES");
		chckbxCategories.setSelected(true);
		panel_4.add(chckbxCategories);
		
		chckbxGenerics = new JCheckBox("GENERICS");
		chckbxGenerics.setSelected(true);
		panel_4.add(chckbxGenerics);
		
		chckbxBlogs = new JCheckBox("BLOGS");
		chckbxBlogs.setSelected(true);
		panel_4.add(chckbxBlogs);
		LOG.init();
	}
	
	public void createTable() {
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setCellSelectionEnabled(true);
		table.setRowSelectionAllowed(true);
		model = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"Type", "Host", "Path", "Title", "Page", "Saved"
				}) {
			
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				return columnIndex != 5 ? String.class : Boolean.class;
			}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(model);
		table.getColumnModel().getColumn(0).setMaxWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setMaxWidth(150);
		table.getColumnModel().getColumn(5).setPreferredWidth(40);
		table.getColumnModel().getColumn(5).setMaxWidth(40);
	}
	
	protected void scanNewSite(String host) throws Exception {
		if (host == null) {
			JOptionPane.showMessageDialog(null, "Invalid host ");
			LOG.logError("Invalid host ");
			return;
		}
		host = host.toLowerCase().trim();
		if (host.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Invalid host ");
			LOG.logError("Empty host ");
			return;
		}
		if (!host.startsWith("http")) {
			host = "http://" + host;
		}
		if (!host.endsWith("/")) {
			host = host + "/";
		}
		LOG.log("Host: " + host);
		LOG.logInfo("Clear cache file " + cacheFile.getAbsolutePath());
		cacheFile.delete();
		process = new ScanProcess();
		addTask(host, PageType.MAIN);
		scan();
	}
	
	protected void scanContinue() throws Exception {
		process = loadTasks();
		scan();
	}
	
	public void scanUrls(List<String> urls, boolean sitemap) {
		process = new ScanProcess();
		model.setRowCount(0);
		for (String url : urls) {
			if (!url.isEmpty()) {
				url = prepareUrl(url);
				Task task = addTask(url, getType(url));
				if (task == null) {
					continue;
				}
				task.sitemap = sitemap;
				scan();
			}
		}
	}
	
	public void scan() {
		btnScan.setEnabled(false);
		btnContinue.setEnabled(false);
		try {
			createServices();
			int errors = 0;
			int taskCount = 0;
			while (process.hasTask()) {
				try {
					taskCount++;
					if (taskCount % 100 == 0) {
						driver.quit();
						createDriver();
					}
					Task task = process.nextTask();
					progressBar.setValue(process.taskNum + 1);
					doScan(task);
					process.taskProcessed();
					updateTaskInfo(task);
				} catch (Exception ex) {
					errors++;
					LOG.logError(ex, "Problem when scan: ");
					if (errors > 10) {
						LOG.logError("Too many errors - break scan");
						break;
					}
				}
			}
		} finally {
			btnScan.setEnabled(true);
			btnContinue.setEnabled(true);
		}
	}
	
	boolean servicesCreated = false;
	private JButton btnFromFile;
	private JButton btnFromUrls;
	private JButton btnScanall;
	
	public void createServices() {
		if (servicesCreated)
			return;
		servicesCreated = true;
		registerCert();
		createDriver();
		startCacher();
		startSaver();
		loadingKnowedUrls();
		loadingSkippedUrls();
	}
	
	private static void registerCert() {
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}
					
					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					
					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};
		
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
		}
	}
	
	private void createDriver() {
		String driverPath = DEFAULT_DRIVER_PATH;
		System.setProperty(CHROME_WEBDRIVER_PATH, driverPath);
		LOG.logInfo("Used webdriver.chrome.driver as " + driverPath);
		String PROXY = "95.85.6.74:1234";
		
		org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
		proxy.setHttpProxy(PROXY)
				.setFtpProxy(PROXY)
				.setSslProxy(PROXY);
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(CapabilityType.PROXY, proxy);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--user-agent=" + "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0; ImageObserverDisable) Gecko/20100101 Firefox/66.0");
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(530, 1000));
	}
	
	public void startCacher() {
		
		try {
			if (!cacheFile.exists()) {
				LOG.logInfo("Recreate cache file " + cacheFile.getAbsolutePath());
				cacheFile.createNewFile();
			}
		} catch (IOException ex) {
			LOG.logError(ex, "Problem with creation cache file");
		}
		
		Thread saver = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
				LOG.logInfo("Cacher started");
				while (!btnScan.isEnabled()) {
					try {
						Thread.sleep(10000);
						// LOG.logInfo("Cached " + process.taskNum + " progress in " + process.allTasks.size() + " tasks ");
						objectMapper.writeValue(cacheFile, process);
					} catch (Exception ex) {
						LOG.logError(ex, "Problem in cacher");
					}
				}
			}
		});
		saver.setDaemon(true);
		saver.setName("~Cacher");
		saver.start();
		
	}
	
	public void startSaver() {
		Thread saver = new Thread(new Runnable() {
			
			@Override
			public void run() {
				LOG.logInfo("Saver started");
				while (true) {
					try {
						Task task = process.findTaskForSave();
						if (task != null) {
							if (task.number >= 0 && PageType.GENERIC.equals(task.type) && !task.url.contains("search")) {
								String subUrl = task.url;
								if (subUrl.contains("/page/")) {
									LOG.logWarning("FOUND " + task.url);
									subUrl = subUrl.replace("/page", "");
									addTask(subUrl, getType(subUrl));
									task.sitemap = false;
								}
							}
							saveTask(task);
							updateTaskInfo(task);
						}
					} catch (Exception ex) {
						LOG.logError(ex, "Problem in saver");
					}
				}
				
			}
		});
		saver.setDaemon(true);
		saver.setName("~Saver");
		saver.start();
	}
	
	public void saveTask(Task task) throws Exception {
		String info = "Task " + (task.number + 1) + ". Source length " + task.pageSource.length() + " Title: " + task.title + " " + task;
		LogValue logInfo = LOG.logInfo("Save " + info);
		
		URL url = new URL(task.getHost() + "/api/BotsCachePut");
		Map<String, String> params = new LinkedHashMap<>();
		params.put("url", task.getPath());
		params.put("title", task.title == null ? "" : task.title);
		params.put("tags", "");
		params.put("page", task.pageSource);
		params.put("sitemap", task.sitemap ? "1" : "0");
		
		StringBuilder postData = new StringBuilder();
		for (Map.Entry<String, String> param : params.entrySet()) {
			if (postData.length() != 0) {
				postData.append('&');
			}
			postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			postData.append('=');
			postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
		}
		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);
		
		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		
		StringBuilder sb = new StringBuilder();
		for (int c; (c = in.read()) >= 0;) {
			sb.append((char) c);
		}
		String response = sb.toString();
		if ("OK".equalsIgnoreCase(response)) {
			task.sended = true;
			task.pageSource = "Saved";
			logInfo.setValue("INFO: Save - OK. " + info);
		} else {
			logInfo.setValue("WARNING: Save - FAILED  " + response + " " + info);
		}
	}
	
	private void loadingSkippedUrls() {
		skipped.add("/cart");
		skipped.add("/admin");
		skipped.add("/user");
		skipped.add("/checkout");
		skipped.add("/search");
		skipped.add("/compare");
		skipped.add("https://www.vivalusso.ru/difference");
		skipped.add("/null");
	}
	
	private void loadingKnowedUrls() {
		File ignoredFile = new File("./knowed.txt");
		if (ignoredFile.exists()) {
			try {
				List<String> lines = Files.readAllLines(ignoredFile.toPath(), Charsets.UTF_8);
				for (String line : lines) {
					if (!line.isEmpty()) {
						knowed.add(line);
					}
				}
			} catch (IOException ex) {
				LOG.logError("Failed loaded ignored file");
			}
		}
	}
	
	private void doScan(Task task) throws Exception {
		LogValue log = LOG.log("Scan " + task);
		if (PageType.PRODUCT.equals(task.type) && !chckbxProducts.isSelected()) {
			task.title = "SKIPPED";
			return;
		}
		if (PageType.CATEGORY.equals(task.type) && !chckbxCategories.isSelected()) {
			task.title = "SKIPPED";
			return;
		}
		if (PageType.GENERIC.equals(task.type) && !chckbxGenerics.isSelected()) {
			task.title = "SKIPPED";
			return;
		}
		if (PageType.BLOG.equals(task.type) && !chckbxBlogs.isSelected()) {
			task.title = "SKIPPED";
			return;
		}
		String path = task.getPath();
		String checkUrl = task.url;
		if (skipped.contains(path) || skipped.contains(path.toLowerCase()) || skipped.contains(checkUrl.toLowerCase())) {
			task.title = "SKIPPED";
			return;
		}
		String host = task.getHost();
		if (!hosts.contains(host)) {
			task.title = "SKIPPED";
			return;
		}
		
		loadPage(checkUrl);
		if (!driver.findElementsByClassName("geo-footer").isEmpty()) {
			driver.executeScript("$('.geo-footer').click()");
			Thread.sleep(1000);
		}
		if (!driver.findElementsByClassName("modal").isEmpty()) {
			LOG.logInfo("Close modal window");
			driver.executeScript("$('.modal .close').click()");
			Thread.sleep(1000);
		}
		if (!driver.findElementsByClassName("trustpilot-widget").isEmpty() && driver.findElementsByCssSelector(".trustpilot .ng-hide").isEmpty()) {
			LOG.logInfo("Sleep for TrustPilot Widget 3 sec");
			Thread.sleep(3000);
		}
		if (isPage404()) {
			task.title = "404 ERROR";
			task.pageSource = "";
			task.sended = true;
			log.setValue("ERROR: " + "Error 404 " + task);
			return;
		}
		
		task.title = parseTitle();
		String pageSource = getPageSource();
		if (PageType.MAIN.equals(task.type)) {
			addTask(host + "/category/all", PageType.CATEGORY);
			addTask(host + "/itemset/all", PageType.ITESSET);
			processMain();
		}
		if (PageType.CATEGORY.equals(task.type)) {
			processCatalog();
		}
		
		if (checkUrl.endsWith("/blog")) {
			processBlog();
		}
		
		if (PageType.ITESSET.equals(task.type)) {
			processItemsset();
		}
		String realUrl = (String) driver.executeScript("return document.location.href;");
		if (!checkUrl.equalsIgnoreCase(realUrl)) {
			Task newTask = addTask(realUrl, getType(realUrl));
			if (newTask != null) {
				newTask.sitemap = false;
			}
		}
		
		pageSource = prepareLinkUrls(pageSource, checkUrl);
		task.complete(pageSource);
		log.setValue("SUCCESS: " + "Scan " + task);
	}
	
	private String prepareLinkUrls(String pageSource, String checkUrl) throws Exception {
		Set<String> removedRelUrls = new LinkedHashSet<>();
		Map<String, String> replaceLink = new HashMap<>();
		Set<String> relUrls = parseRelUrls(pageSource, checkUrl);
		Iterator<String> iterator = relUrls.iterator();
		while (iterator.hasNext()) {
			String relUrl = iterator.next();
			LogValue logInfo = LOG.logInfo("Check rel url " + relUrl);
			HttpGet request = new HttpGet(relUrl);
			request.addHeader("User-Agent", "Aquatica Bot");
			CloseableHttpResponse response = null;
			int statusCode = 0;
			try {
				response = httpClient.execute(request);
				statusCode = response.getStatusLine().getStatusCode();
			} finally {
				if (response != null) {
					response.close();
				}
			}
			if (statusCode != 200) {
				logInfo.setValue("INFO: " + "Browser check rel url " + relUrl);
				loadPage(relUrl);
				if (isPage404()) {
					removedRelUrls.add(relUrl);
					logInfo.setValue("INFO: " + "Not found Browser check rel url " + relUrl);
				} else {
					// String linkPageSource = getPageSource();
					// relUrls.addAll(parseRelUrls(linkPageSource, checkUrl));
					/*
					 * Task relTask = new Task(-1, relUrl, getType(relUrl)); relTask.title = parseTitle(); relTask.pageSource = linkPageSource; relTask.sitemap = false; relTasks.add(relTask);
					 */
					String pageUrl = getPageUrl();
					if (!relUrl.equalsIgnoreCase(pageUrl)) {
						replaceLink.put(relUrl, pageUrl);
					}
					Task newTask = addTask(relUrl, getType(relUrl));
					if (newTask != null) {
						newTask.sitemap = false;
					}
					newTask = addTask(pageUrl, getType(pageUrl));
					if (newTask != null) {
						newTask.sitemap = true;
					}
					logInfo.setValue("INFO: " + "Valid Browser check rel url " + relUrl);
				}
			} else {
				logInfo.setValue("INFO: " + "Valid check rel url " + relUrl);
			}
		}
		
		for (Entry<String, String> rename : replaceLink.entrySet()) {
			pageSource = renameLinks(pageSource, rename.getKey(), rename.getValue());
		}
		
		pageSource = removeLinks(pageSource, removedRelUrls);
		return pageSource;
	}
	
	public String renameLinks(String pageSource, String oldUrl, String newUrl) {
		LOG.logInfo("Rename old url " + oldUrl + " to Url " + newUrl);
		Pattern patternRemoteLinkRel = Pattern.compile("(<link[^>]+hreflang[^>]+href\\s*=\\s*['\"])(" + Pattern.quote(oldUrl) + ")(['\"][^>]*>\\s*)", Pattern.DOTALL + Pattern.CASE_INSENSITIVE);
		pageSource = patternRemoteLinkRel.matcher(pageSource).replaceAll("$1" + newUrl + "$3");
		return pageSource;
	}
	
	public String removeLinks(String pageSource, Set<String> removedRelUrls) {
		for (String removedUrl : removedRelUrls) {
			LOG.logInfo("Removed wrong rel url " + removedUrl);
			Pattern patternRemoteLinkRel = Pattern.compile("<link[^>]+hreflang[^>]+href\\s*=\\s*['\"]" + Pattern.quote(removedUrl) + "['\"][^>]*>\\s*", Pattern.DOTALL + Pattern.CASE_INSENSITIVE);
			pageSource = patternRemoteLinkRel.matcher(pageSource).replaceAll("");
		}
		return pageSource;
	}
	
	public Set<String> parseRelUrls(String pageSource, String checkUrl) {
		Set<String> relUrls = new LinkedHashSet<>();
		Matcher matcher = patternLinkRel.matcher(pageSource);
		while (matcher.find()) {
			String relUrl = matcher.group(1);
			if (relUrl == null || relUrl.isEmpty() || relUrl.equalsIgnoreCase(checkUrl) || getHost(relUrl).equalsIgnoreCase(getHost(checkUrl))) {
				continue;
			}
			relUrls.add(relUrl);
		}
		return relUrls;
	}
	
	public void loadPage(String url) throws Exception {
		try {
			driver.get(url);
			waitLoading();
			return;
		} catch (TimeoutException ex) {
			LOG.logWarning("Timeout loading page " + url);
		}
		Thread.sleep(1000);
		loadPage(url);
	}
	
	public boolean isPage404() {
		List<WebElement> error404 = driver.findElementsByClassName("notfound");
		List<WebElement> content = driver.findElementsByXPath("//div[@app-view-segment]/*");
		return !error404.isEmpty() || content.isEmpty();
	}
	
	public String getPageSource() {
		driver.executeScript("if (document.getElementById('lite_page')) document.getElementById('lite_page').style.display='none';");
		driver.executeScript("if (document.getElementById('lite_page')) document.getElementById('lite_page').style.background='';");
		driver.executeScript("if (document.getElementById('loader'))    document.getElementById('loader').style.display='none';");
		driver.executeScript("$('#jivo-iframe-container').remove()");
		driver.executeScript("$('jdiv').remove()");
		driver.executeScript("$('script').not(\"[type='application/ld+json']\").not(\"#mcjs\").remove()");
		String html = (String) driver.executeScript("return document.documentElement.outerHTML;");
		html = html.replaceAll("(?is)<!--(.*?)-->", "");
		return html;
	}
	
	public String parseTitle() {
		return (String) driver.executeScript("var titles = []; $('h1').each(function(index, obj) {titles.push($(this).text());}); return titles.join(' - ');");
	}
	
	public String getPageUrl() {
		return (String) driver.executeScript("return window.location.href;");
	}
	
	private void processMain() {
		String host = String.valueOf(driver.executeScript("return window.location.host;"));
		for (String url : knowed) {
			url = "https://" + host + url;
			addTask(url, getType(url));
		}
		addAllLinks();
	}
	
	public void addAllLinks() {
		String host = String.valueOf(driver.executeScript("return window.location.host;"));
		List<WebElement> links = driver.findElementsByTagName("a");
		for (WebElement link : links) {
			String href = link.getAttribute("href");
			href = prepareUrl(href);
			if (href != null) {
				if (href.startsWith("//")) {
					href = "https:" + href;
				} else if (href.startsWith("/")) {
					href = "https://" + host + href;
				}
				if (href.startsWith("https://" + host)) {
					addTask(href, getType(href));
				}
			}
		}
	}
	
	private void processCatalog() {
		List<WebElement> links = driver.findElementsByCssSelector(".catalog-product a");
		for (WebElement link : links) {
			String href = link.getAttribute("href");
			href = prepareUrl(href);
			if (href != null) {
				addTask(href, getType(href));
			}
		}
	}
	
	private void processItemsset() {
		List<WebElement> links = driver.findElementsByCssSelector(".itemset a");
		for (WebElement link : links) {
			String href = link.getAttribute("href");
			href = prepareUrl(href);
			if (href != null) {
				addTask(href, getType(href));
			}
		}
		
		processCatalog();
	}
	
	private void processBlog() {
		List<WebElement> links = driver.findElementsByCssSelector("a.post-header");
		for (WebElement link : links) {
			String href = link.getAttribute("href");
			href = prepareUrl(href);
			if (href != null) {
				addTask(href, getType(href));
			}
		}
	}
	
	public PageType getType(String href) {
		String path = getPath(href);
		if (path.equals("/"))
			return PageType.MAIN;
		if (path.contains("/category/"))
			return PageType.CATEGORY;
		if (path.contains("/itemset/"))
			return PageType.CATEGORY;
		if (path.contains("/products/"))
			return PageType.PRODUCT;
		if (path.contains("/blog"))
			return PageType.BLOG;
		return PageType.GENERIC;
	}
	
	private String prepareUrl(String href) {
		if (href == null)
			return null;
		href = href.trim().toLowerCase();
		if (href.contains("#")) {
			href = href.substring(0, href.indexOf("#"));
		}
		
		String host = getHost(href);
		String path = getPath(href);
		
		return host + path;
	}
	
	private void waitLoading() throws Exception {
		int sec = 1;
		List<WebElement> loader = driver.findElementsByCssSelector("#loader.loading");
		while (!loader.isEmpty()) {
			Thread.sleep(1000);
			sec++;
			loader = driver.findElementsByCssSelector("#loader.loading");
			if (sec > 60) {
				break;
			}
		}
	}
	
	private Task addTask(String url, PageType type) {
		Task task = process.addTask(url, type);
		if (task == null)
			return null;
		model.addRow(new Object[] { task.type.name(), task.getDomain(), task.getPath(), task.title, task.pageSource, task.sended });
		model.fireTableRowsInserted(model.getRowCount() - 1, model.getRowCount() - 1);
		progressBar.setMaximum(model.getRowCount());
		return task;
	}
	
	private void updateTaskInfo(Task task) {
		int row = task.number;
		if (row < 0 || row > model.getRowCount() - 1)
			return;
		model.setValueAt(task.title, row, 3);
		model.setValueAt(task.pageSource, row, 4);
		model.setValueAt(task.sended, row, 5);
		model.fireTableCellUpdated(row, 3);
		model.fireTableCellUpdated(row, 4);
		model.fireTableCellUpdated(row, 5);
	}
	
	private ScanProcess loadTasks() throws Exception {
		LOG.log("Loading cache");
		ObjectMapper objectMapper = new ObjectMapper();
		ScanProcess scanProcess = objectMapper.readValue(cacheFile, ScanProcess.class);
		LOG.log("Loaded " + scanProcess.allTasks.size() + " tasks. Current number " + scanProcess.taskNum);
		model.setRowCount(0);
		for (Task task : scanProcess.allTasks) {
			model.addRow(new Object[] { task.type.name(), task.getDomain(), task.getPath(), task.title, task.pageSource, task.sended });
		}
		model.fireTableRowsInserted(0, model.getRowCount() - 1);
		progressBar.setMaximum(model.getRowCount());
		return scanProcess;
	}
	
	private static String getHost(String url) {
		if (url == null || url.isEmpty())
			return null;
		
		url = url.trim().toLowerCase();
		if (url.contains("#")) {
			url = url.substring(0, url.indexOf("#"));
		}
		
		if (url.startsWith("//")) {
			url = "https:" + url;
		}
		
		if (!url.startsWith("http")) {
			url = "https://" + url;
		}
		
		int schIndex = url.indexOf("://");
		int hostIndex = url.indexOf("/", schIndex + 4);
		if (hostIndex < 0)
			return url;
		return url.substring(0, hostIndex);
	}
	
	private static String getPath(String url) {
		if (url == null || url.isEmpty())
			return null;
		
		url = url.trim().toLowerCase();
		if (url.contains("#")) {
			url = url.substring(0, url.indexOf("#"));
		}
		
		if (url.startsWith("//")) {
			url = "https:" + url;
		}
		
		if (!url.startsWith("http")) {
			url = "https://" + url;
		}
		
		int schIndex = url.indexOf("://");
		int pathIndex = url.indexOf("/", schIndex + 4);
		if (pathIndex < 0)
			return "/";
		return url.substring(pathIndex);
	}
}
