import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Splitter;

public class FromUrls extends JFrame {
	
	private static final Splitter lineSplitter = Splitter.on("\n").omitEmptyStrings().trimResults();

	private JPanel contentPane;
	
	public FromUrls(final AquaticaSeo aquaticaSeo) {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		final JTextArea txtrUrls = new JTextArea();
		scrollPane.setViewportView(txtrUrls);
		
		final JCheckBox chckbxInSitemap = new JCheckBox("In sitemap");
		chckbxInSitemap.setSelected(true);
		contentPane.add(chckbxInSitemap, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btnScan = new JButton("Scan");
		btnScan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = txtrUrls.getText();
				text = text.replace("\r", "");
				List<String> urls = lineSplitter.splitToList(text);
				if (urls.isEmpty()) {
					LOG.logWarning("Url list is empty");
					return;
				}
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							aquaticaSeo.scanUrls(urls, chckbxInSitemap.isSelected());
						} catch (Exception ex) {
							throw new RuntimeException(ex);
						}
					}
				});
				t.setDaemon(true);
				t.setName("~Scanner");
				t.start();
				FromUrls.this.setVisible(false);
			}
		});
		panel.add(btnScan);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FromUrls.this.setVisible(false);
			}
		});
		panel.add(btnCancel);

	}

}
