package com.trustename.net;

import java.net.*;
import java.net.Proxy.Type;
import java.util.Arrays;

public class ProxyAuthenticator extends Authenticator {
	private final Proxy proxy;
	private final PasswordAuthentication auth;

	public ProxyAuthenticator(Proxy proxy, String username, String password) {
		this.proxy = proxy;
		auth = new PasswordAuthentication(username, password.toCharArray());
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void register() {
		Authenticator.setDefault(this);
	}

	public void unregister() {
		Authenticator.setDefault(null);
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return auth;
	}

	@Override
	protected URL getRequestingURL() {
		String protocol = getProtocol(proxy.type());
		InetSocketAddress address = (InetSocketAddress) proxy.address();
		try {
			return new URL(protocol, address.getHostName(), address.getPort(), "");
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	private static String getProtocol(Type type) {
		switch (type) {
			case HTTP:
				return "http";
			case SOCKS:
				return "socks";
			default:
				throw new IllegalArgumentException("Invalid proxy type: " + type);
		}
	}

	@Override
	protected RequestorType getRequestorType() {
		return RequestorType.PROXY;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + proxy.hashCode();
		hash = 31 * hash + auth.getUserName().hashCode();
		hash = 31 * hash + Arrays.hashCode(auth.getPassword());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass())
			return false;
		ProxyAuthenticator a = (ProxyAuthenticator) obj;
		return proxy.equals(a.proxy)
				&& auth.getUserName().equals(a.auth.getUserName())
				&& Arrays.equals(auth.getPassword(), a.auth.getPassword());
	}
}
