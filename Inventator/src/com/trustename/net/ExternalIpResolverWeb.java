package com.trustename.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ExternalIpResolverWeb implements ExternalIpResolver {
	private static final Logger logger = LoggerFactory.getLogger(ExternalIpResolverWeb.class);

	private final InetAddress address;

	// TODO (kpavelko): сделать сервисом? в конструкторах не должно быть логики, и уж тем более I/O
	public ExternalIpResolverWeb(URL url) throws IOException {
		logger.info("Determining external IP from {}", url);
		try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
			String externalIp = in.readLine().trim();
			this.address = InetAddress.getByName(externalIp);
		}
		logger.info("External IP: {}", address);
	}

	@Override
	public InetAddress getExternalIp() {
		return address;
	}

}
