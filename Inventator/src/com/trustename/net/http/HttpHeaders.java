package com.trustename.net.http;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HttpHeaders {
	private Map<String, List<String>> items = new LinkedHashMap<>();

	public HttpHeaders() {
	}

	public HttpHeaders(Map<String, List<String>> items) {
		this.items.putAll(items);
	}

	public Map<String, List<String>> all() {
		return items;
	}

	public void addAll(HttpHeaders items) {
		addAll(items.all());
	}

	public void addAll(Map<String, List<String>> items) {
		for (Map.Entry<String, List<String>> e : items.entrySet()) {
			add(e.getKey(), e.getValue());
		}
	}

	public void add(String name, List<String> value) {
		List<String> values = items.get(name);
		if (values == null) {
			items.put(name, value);
			return;
		}
		for (String v : value) {
			if (!values.contains(v))
				values.add(v);
		}
	}

	public void add(String name, String value) {
		List<String> values = new ArrayList<>();
		values.add(value);
		add(name, values);
	}

	public void setAll(HttpHeaders items) {
		setAll(items.all());
	}

	public void setAll(Map<String, List<String>> items) {
		this.items.putAll(items);
	}

	public void set(String name, List<String> value) {
		items.put(name, value);
	}

	public void set(String name, String value) {
		List<String> values = new ArrayList<>();
		values.add(value);
		set(name, values);
	}

	public void remove(String name) {
		items.remove(name);
	}

	public void clear() {
		items.clear();
	}

	public List<String> getValues(String name) {
		return items.get(name);
	}

	public String getValue(String name) {
		List<String> values = getValues(name);
		if (values == null)
			return null;
		return values.get(0);
	}
}
