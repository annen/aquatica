package com.trustename.net.http;

import java.net.HttpCookie;
import java.util.Collection;
import java.util.List;


public interface CookiesManager {
	public void set(HttpCookie c, String defaultDomain);
	public void set(HttpCookie c);
	public void set(Collection<HttpCookie> cookies, String defaultDomain);
	public void set(Collection<HttpCookie> cookies);

	public List<HttpCookie> getForHost(String host);
}
