package com.trustename.net.http;

import java.net.HttpCookie;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class HttpRequest {
	private final URL url;
	private final HttpHeaders headers;
	private List<HttpCookie> cookies;

	public HttpRequest(String url) throws MalformedURLException {
		this(new URL(url));
	}

	public HttpRequest(URL url) {
		this(url, new HttpHeaders());
	}

	public HttpRequest(String url, HttpHeaders headers) throws MalformedURLException {
		this(new URL(url), headers);
	}

	public HttpRequest(URL url, HttpHeaders headers) {
		this.url = url;
		this.headers = (headers == null ? new HttpHeaders() : headers);
	}

	public HttpRequest referrer(URL referrer) {
		headers.add(com.google.common.net.HttpHeaders.REFERER, referrer.toString());
		return this;
	}

	public URL url() {
		return url;
	}

	public HttpHeaders headers() {
		return headers;
	}

	// overrides cookies from CookiesManager for this request  
	public void setCookies(List<HttpCookie> cookies) {
		this.cookies = cookies;
	}

	public List<HttpCookie> getCookies() {
		return cookies;
	}
}
