package com.trustename.net.http;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.google.common.collect.Multimap;
import com.trustename.net.WebProxy;

public abstract class AbstractHttpBrowserClient {
	private static final String DEFAULT_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.2; Trident/4.0; SLCC2; .NET CLR 2.0" + ".50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729)";

	protected final CookiesManager cookiesManager = new CookiesManagerMemory();
	protected final HttpClient client;
	protected URL referrer;

	public static HttpClient createHttpClient(CookiesManager cookiesManager, WebProxy proxy) {
		HttpHeaders headers = new HttpHeaders();
		headers.set(com.google.common.net.HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml,*/*");
		headers.set(com.google.common.net.HttpHeaders.ACCEPT_LANGUAGE, "en-US");
		headers.set(com.google.common.net.HttpHeaders.USER_AGENT, DEFAULT_USER_AGENT);
		headers.set(com.google.common.net.HttpHeaders.CACHE_CONTROL, "no-cache");
		headers.set(com.google.common.net.HttpHeaders.PRAGMA, "no-cache");
		HttpClient client = new HttpClient(cookiesManager, headers, proxy);
		client.setTrustAllSslCertificates(true);
		return client;
	}

	protected AbstractHttpBrowserClient(WebProxy proxy) {
		client = createHttpClient(cookiesManager, proxy);
	}

	protected AbstractHttpBrowserClient() {
		this(null);
	}

	protected HttpResponse get(HttpRequest request, boolean updateReferrer) throws Exception {
		HttpResponse resp = client.get(request);
		if (updateReferrer)
			referrer = resp.url();
		return resp;
	}

	protected HttpResponse get(String url) throws Exception {
		return get(createRequest(url), true);
	}

	protected HttpResponse post(HttpRequest request, Map<String, String> form, boolean updateReferrer) throws Exception {
		HttpResponse resp = client.post(request, form);
		if (updateReferrer)
			referrer = resp.url();
		return resp;
	}

	protected HttpResponse post(String url, Multimap<String, String> form) throws Exception {
		HttpResponse resp = client.post(createRequest(url), form);
		referrer = resp.url();
		return resp;
	}

	protected HttpResponse post(String url, Map<String, String> form) throws Exception {
		return post(createRequest(url), form, true);
	}

	protected HttpRequest createRequest(String url) throws IOException {
		HttpRequest httpRequest = new HttpRequest(url);
		if (referrer == null) {
			return httpRequest;
		}
		return httpRequest.referrer(referrer);
	}

	protected HttpResponse ajaxGet(String url) throws Exception {
		return get(createAjaxRequest(url), false);
	}

	protected HttpResponse ajaxPost(String url, Map<String, String> form) throws Exception {
		return post(createAjaxRequest(url), form, false);
	}

	protected HttpRequest createAjaxRequest(String url) throws MalformedURLException {
		HttpRequest req = new HttpRequest(url);
		req.headers().add(com.google.common.net.HttpHeaders.X_REQUESTED_WITH, "XMLHttpRequest");
		return req;
	}

}
