package com.trustename.net;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.lang.Character.isDigit;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import com.google.common.net.InternetDomainName;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.trustename.net.util.StringUtils;

public final class NetworkUtils {
	private static final String SCHEME_DELIMITER = "://";
	private static final String HTTP_PORT_SUFFUX = ":80";
	private static final Pattern PATTERN_IP4_ADDR = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9‌​]{2}|2[0-4][0-9]|25[0-5])$");
	private static final Pattern PATTERN_DOMAIN = Pattern.compile("[=:]\\s*[\"']?(?:(?:http(?:s|):)?(?://))?((?:[a-z][\\w-]+\\.)+[a-z][\\w]+)" + "[/\"'\\s\\?\\.&]", Pattern.CASE_INSENSITIVE);
	private static final Pattern PATTERN_URL = Pattern.compile("[=:\\?+]\\s*[\"']?(http(?:s|)://(?:[a-z][\\w-]+\\.)+[a-z][\\w]+[/\\?].*?)" + "[\"'\\s]", Pattern.CASE_INSENSITIVE);
	private static final Splitter URL_SPLITTER = Splitter.on(CharMatcher.anyOf(":/.#?&")).trimResults().omitEmptyStrings();

	public static int ipv4ToInt(InetAddress address) {
		byte[] ipAddressBytes = address.getAddress();
		int result = ipAddressBytes[0] & 0xff;
		result |= ipAddressBytes[1] << 8 & 0xff00;
		result |= ipAddressBytes[2] << 16 & 0xff0000;
		result |= ipAddressBytes[3] << 24 & 0xff000000;
		return result;
	}

	public static InetAddress ipv4FromInt(int value) {
		byte[] ipAddressBytes = new byte[] { (byte) value, (byte) (value >>> 8), (byte) (value >>> 16), (byte) (value >>> 24) };
		return bytesToInetAddress(ipAddressBytes);
	}

	private static InetAddress bytesToInetAddress(byte[] addr) {
		try {
			return InetAddress.getByAddress(addr);
		} catch (UnknownHostException e) {
			throw new AssertionError(e);
		}
	}

	public static String extractSchemeFromUrl(String url) {
		if (url == null)
			return null;
		int index = url.indexOf(SCHEME_DELIMITER);
		if (index < 0)
			return "";
		return url.substring(0, index);
	}

	// returns domain:port
	public static String extractHostFromUrl(String url) {
		if (url == null)
			return null;
		int index1 = getHostStartIndex(url);
		int index2 = getHostEndIndex(url, index1);
		return url.substring(index1, index2);
	}

	public static String extractPathQueryFromUrl(String url) {
		int index = getHostStartIndex(url);
		index = getHostEndIndex(url, index);
		return url.substring(index);
	}

	public static String extractDomainFromHost(String host) {
		int colon = host.lastIndexOf(':');
		if (colon < 0)
			return host;
		return host.substring(0, colon);
	}

	public static String extractDomainFromUrl(String url) {
		if (url == null)
			return null;
		int index1 = getHostStartIndex(url);
		int index2 = getDomainEndIndex(url, index1);
		return url.substring(index1, index2);
	}

	public static String stripDefaultHttpPort(String host) {
		if (host.endsWith(HTTP_PORT_SUFFUX))
			return host.substring(0, host.length() - HTTP_PORT_SUFFUX.length());
		return host;
	}

	public static String stripWwws(String domain) {
		if (domain == null)
			return null;
		int index = getWwwEndIndex(domain, 0);
		return domain.substring(index);
	}

	public static String extractBaseDomain(String url) {
		if (url == null)
			return "";
		int index1 = getHostStartIndex(url);
		index1 = getWwwEndIndex(url, index1);
		int index2 = getDomainEndIndex(url, index1);
		url = url.substring(index1, index2);
		return StringUtils.toLowerCaseAscii(url);
	}

	public static String extractDisplayUrlOrBrand(String s) {
		if (s == null || s.isEmpty())
			return s;
		int index1 = getHostStartIndex(s);
		int index2 = getHostPathOrBrandEndIndex(s, index1);
		return s.substring(index1, index2);
	}

	public static String extractBaseDomainOrBrand(String s) {
		if (s == null || s.isEmpty())
			return "";
		int index1 = getHostStartIndex(s);
		index1 = getWwwEndIndex(s, index1);
		int index2 = getDomainOrBrandEndIndex(s, index1);
		s = s.substring(index1, index2);
		return s.toLowerCase();
	}

	private static int getHostPathOrBrandEndIndex(String s, int index1) {
		int index2 = indexOfNotValidHostPathChar(s, index1);
		if (index2 < 0) {
			index2 = s.length() - 1;
			if (index1 <= index2 && s.charAt(index2) == '/')
				return index2;
			return s.length();
		}
		if (index1 > 0 || s.charAt(index2) == '?') {
			if (index2 > 0 && s.charAt(index2 - 1) == '/')
				--index2;
			return index2;
		}
		return s.length();
	}

	private static int getDomainOrBrandEndIndex(String s, int index1) {
		int index2 = getDomainEndIndex(s, index1);
		if (index2 == s.length())
			return index2;
		char ch = s.charAt(index2);
		if (ch == ':' || ch == '/')
			return index2;
		return s.length();
	}

	private static int getWwwEndIndex(String url, int index1) {
		if (url.length() - index1 < 4)
			return index1;
		int index = index1;
		for (; index < index1 + 3; ++index) {
			char ch = url.charAt(index);
			if (ch != 'w' && ch != 'W')
				return index1;
		}
		char ch = url.charAt(index++);
		if (ch == '.')
			return index;
		if (url.length() - index1 < 5)
			return index1;
		if (isDigit(ch) && url.charAt(index) == '.')
			return index + 1;
		return index1;
	}

	private static int getHostStartIndex(String url) {
		int index1 = url.indexOf(SCHEME_DELIMITER);
		if (index1 < 0)
			return 0;
		return index1 + SCHEME_DELIMITER.length();
	}

	private static int getHostEndIndex(String url, int index1) {
		int index2 = indexOfNotValidHostChar(url, index1);
		if (index2 < 0)
			return url.length();
		if (url.charAt(index2) == ':' && index2 + 1 < url.length()) {
			int index3 = indexOfNotDigitChar(url, index2 + 1);
			if (index3 < 0)
				return url.length();
			else
				return index3;
		}
		return index2;
	}

	private static int getDomainEndIndex(String url, int index1) {
		int index2 = indexOfNotValidHostChar(url, index1);
		if (index2 < 0)
			return url.length();
		return index2;
	}

	private static int indexOfNotValidHostChar(String s, int index) {
		for (; index < s.length(); ++index) {
			if (!isValidHostChar(s.charAt(index)))
				return index;
		}
		return -1;
	}

	private static boolean isValidHostChar(char ch) {
		if (ch >= '0' && ch <= '9')
			return true;
		if (ch >= 'a' && ch <= 'z')
			return true;
		if (ch >= 'A' && ch <= 'Z')
			return true;
		return ch == '.' || ch == '_' || ch == '-';
	}

	private static int indexOfNotValidHostPathChar(String s, int index) {
		for (; index < s.length(); ++index) {
			if (!isValidHostPathChar(s.charAt(index)))
				return index;
		}
		return -1;
	}

	private static boolean isValidHostPathChar(char ch) {
		if (isValidHostChar(ch))
			return true;
		return ch == ':' || ch == '/' || ch == '~' || ch == '!' || ch == '$' || ch == '&' || ch == '\'' || ch == '(' || ch == ')' || ch == '*' || ch == '+' || ch == ',' || ch == ';' || ch == '=' || ch == '@' || ch == '%';
	}

	private static int indexOfNotDigitChar(String s, int index) {
		for (; index < s.length(); ++index) {
			char ch = s.charAt(index);
			if (ch < '0' || ch > '9')
				return index;
		}
		return -1;
	}

	public static String canonicalizeDomain(String domain) {
		char[] buf = new char[domain.length()];
		int dest = 0;
		boolean lastDot = true;
		char ch = 0;
		boolean numericOnly = true;
		for (int i = 0; i < domain.length(); i++) {
			ch = domain.charAt(i);
			if (ch == '.') {
				if (lastDot) {
					continue;
				}
				lastDot = true;
				buf[dest++] = ch;
				continue;
			}
			lastDot = false;
			if (ch >= '0' && ch <= '9') {
				buf[dest] = ch;
			} else {
				ch = Character.toLowerCase(ch);
				numericOnly = false;
			}
			buf[dest++] = ch;
		}
		if (dest == 0)
			return "";
		if (ch == '.') {
			dest--;
		}
		String result = new String(buf, 0, dest);
		if (numericOnly && !result.isEmpty() && result.indexOf('.') == -1) {
			Long aLong = Longs.tryParse(result);
			if (aLong != null) {
				int ipNum = (int) aLong.longValue();
				byte[] bytes = Ints.toByteArray(ipNum);
				return toUnsignedInt(bytes[0]) + "." + toUnsignedInt(bytes[1]) + "." + toUnsignedInt(bytes[2]) + "." + toUnsignedInt(bytes[3]);
			}
		}
		return result;
	}

	public static String subdomain(String domain, int level) {
		int pos = domain.length();
		for (int i = 0; i < level; i++) {
			pos = domain.lastIndexOf('.', pos - 1);
			if (pos == -1) {
				if (i == level - 1)
					return domain;
				else
					return null;
			}
		}
		return domain.substring(pos + 1);
	}

	private static int toUnsignedInt(byte b) {
		return b >= 0 ? b : 256 + b;
	}

	public static String buildRelativeUrl(String baseUrl, String path) throws MalformedURLException {
		URL url = new URL(baseUrl);
		URL relUrl = new URL(url, path);
		return relUrl.toString();
	}

	public static String getMainDomain(String domain) {
		try {
			InternetDomainName idn = InternetDomainName.from(domain);
			if (!idn.isUnderPublicSuffix())
				return domain;
			return idn.topPrivateDomain().toString();
		} catch (IllegalArgumentException e) {
			return domain;
		}
	}

	public static boolean isValidDomain(String domain) {
		if (domain.indexOf('.') < 0)
			return false;
		try {
			InternetDomainName.from(domain);
			// return idn.hasPublicSuffix();
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	public static boolean isIpAddress(String s) {
		return PATTERN_IP4_ADDR.matcher(s).matches();
	}

	public static boolean isValidHost(String s) {
		int index = s.lastIndexOf(':');
		if (index > 0) {
			String port = s.substring(index + 1);
			if (!isValidPort(port))
				return false;
			s = s.substring(0, index);
		}
		return isIpAddress(s) || isValidDomain(s);
	}

	public static boolean isValidPort(String s) {
		Integer port = Ints.tryParse(s);
		return port != null && port > 0 && port <= 65535;
	}

	public static Iterable<String> splitUrl(String url) {
		return URL_SPLITTER.split(url);
	}

	public static Set<String> extractDomains(String text) {
		Set<String> domains = Sets.newHashSet();
		if (isNullOrEmpty(text))
			return domains;
		Matcher m = PATTERN_DOMAIN.matcher(text);
		while (m.find()) {
			domains.add(m.group(1).toLowerCase());
		}
		return domains;
	}

	public static Collection<String> extractUrls(String text) {
		Set<String> urls = Sets.newHashSet();
		if (isNullOrEmpty(text))
			return urls;
		Matcher m = PATTERN_URL.matcher(text);
		while (m.find()) {
			urls.add(m.group(1));
		}
		return urls;
	}

}
