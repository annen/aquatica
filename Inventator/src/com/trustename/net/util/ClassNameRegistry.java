package com.trustename.net.util;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassNameRegistry<T> {
	private static final Logger logger = LoggerFactory.getLogger(ClassNameRegistry.class);

	private final String prefix;
	private final String suffix;

	private final Map<String, Class<? extends T>> classes = newHashMap();

	public ClassNameRegistry(String prefix, String suffix) {
		this.prefix = prefix;
		this.suffix = suffix;
	}

	public ClassNameRegistry() {
		this(null, null);
	}

	public void add(Class<? extends T> clazz) {
		String name = clazz.getSimpleName();
		add(clazz, name);
	}

	public void add(Class<? extends T> clazz, String name) {
		name = name.toLowerCase();
		if (classes.put(name, clazz) != null)
			throw new RuntimeException("Class is already registered: " + clazz + " as: " + name);
		if (!isNullOrEmpty(prefix)) {
			if (name.startsWith(prefix.toLowerCase())) {
				name = name.substring(prefix.length());
				if (classes.put(name, clazz) != null)
					throw new RuntimeException("Class is already registered: " + clazz + " as: " + name);
			} else {
				logger.warn("Class [{}] is not starting with [{}] prefix", clazz, prefix);
			}
		}
		if (!isNullOrEmpty(suffix)) {
			if (name.endsWith(suffix.toLowerCase())) {
				name = name.substring(0, name.length() - suffix.length());
				if (classes.put(name, clazz) != null)
					throw new RuntimeException("Class is already registered: " + clazz + " as: " + name);
			} else {
				logger.warn("Class [{}] is not ending with [{}] prefix", clazz, suffix);
			}
		}
	}

	public Class<? extends T> get(String className) {
		return classes.get(className.trim().toLowerCase());
	}

}
