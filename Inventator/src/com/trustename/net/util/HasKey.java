package com.trustename.net.util;

public interface HasKey<K> {
	K getKey();
}
