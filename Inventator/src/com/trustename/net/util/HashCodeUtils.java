package com.trustename.net.util;

public final class HashCodeUtils {
	private HashCodeUtils() {
	}

	public static int hash(Object value) {
		return value == null ? 0 : value.hashCode();
	}

	public static int hash(boolean value) {
		return value ? 1231 : 1237;
	}

	public static int hash(byte value) {
		return value;
	}

	public static int hash(char value) {
		return value;
	}

	public static int hash(short value) {
		return value;
	}

	public static int hash(int value) {
		return value;
	}

	public static int hash(long value) {
		return (int) (value ^ (value >>> 32));
	}

	public static int hash(float value) {
		return Float.floatToIntBits(value);
	}

	public static int hash(double value) {
		return hash(Double.doubleToLongBits(value));
	}

	public static int hashCode(int h1, int h2) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5, int h6) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		result = 31 * result + h6;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5, int h6, int h7) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		result = 31 * result + h6;
		result = 31 * result + h7;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		result = 31 * result + h6;
		result = 31 * result + h7;
		result = 31 * result + h8;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		result = 31 * result + h6;
		result = 31 * result + h7;
		result = 31 * result + h8;
		result = 31 * result + h9;
		return result;
	}

	public static int hashCode(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10) {
		int result = 1;
		result = 31 * result + h1;
		result = 31 * result + h2;
		result = 31 * result + h3;
		result = 31 * result + h4;
		result = 31 * result + h5;
		result = 31 * result + h6;
		result = 31 * result + h7;
		result = 31 * result + h8;
		result = 31 * result + h9;
		result = 31 * result + h10;
		return result;
	}

	public static int hashCode(int... hashes) {
		int result = 1;
		for (int h : hashes)
			result = 31 * result + h;
		return result;
	}
}
