package com.trustename.net.util;

import java.io.Serializable;

public class Size implements Serializable {
	private static final long serialVersionUID = 1L;

	private static String DELIMETER = "x";

	private int width;
	private int height;

	Size() {
	}

	public Size(int width, int height) {
		if (width < 0)
			throw new IllegalArgumentException("Invalid width: " + width);
		if (height < 0)
			throw new IllegalArgumentException("Invalid height:" + height);
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = (31 * hash) + width;
		hash = (31 * hash) + height;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || (obj.getClass() != getClass()))
			return false;
		Size o = (Size) obj;
		return (width == o.width) && (height == o.height);
	}

	@Override
	public String toString() {
		return width + DELIMETER + height;
	}

	public static Size parse(String s) {
		String[] parts = s.split(DELIMETER);
		if (parts.length == 0) {
			parts = s.split(",");
		}
		if (parts.length != 2)
			throw new IllegalArgumentException("Invalid size format: " + s);
		int width = Integer.parseInt(parts[0].trim());
		int height = Integer.parseInt(parts[1].trim());
		return new Size(width, height);
	}
}
