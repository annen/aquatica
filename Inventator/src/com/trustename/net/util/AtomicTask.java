package com.trustename.net.util;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AtomicTask {
	private static final Logger logger = LoggerFactory.getLogger(AtomicTask.class);

	private final Executor executor;
	private final Runnable runnable;

	private final AtomicBoolean isTaskRunning = new AtomicBoolean();

	public AtomicTask(Executor executor, Runnable runnable) {
		this.executor = executor;
		this.runnable = runnable;
	}

	private void doRun() throws Exception {
		if (executor == null) {
			runnable.run();
			return;
		}

		final AtomicReference<Exception> exceptionHolder = new AtomicReference<>();
		final CountDownLatch latch = new CountDownLatch(1);
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
					exceptionHolder.set(e);
				} finally {
					latch.countDown();
				}
			}
		});
		latch.await();
		Exception exception = exceptionHolder.get();
		if (exception != null)
			throw exception;
	}

	public boolean tryRun() {
		if (isTaskRunning.compareAndSet(false, true)) {
			try {
				logger.info("Task [{}] started", this);
				doRun();
				logger.debug("Task [{}] finished", this);
			} catch (Throwable e) {
				logger.error("Error running task [" + this + "]", e);
			} finally {
				isTaskRunning.set(false);
			}
			return true;
		} else {
			logger.warn("Task [{}] is already running", this);
			return false;
		}
	}

	public boolean isTaskRunning() {
		return isTaskRunning.get();
	}

	@Override
	public String toString() {
		return runnable.toString();
	}

}
