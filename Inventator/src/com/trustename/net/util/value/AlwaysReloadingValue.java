package com.trustename.net.util.value;

import static com.google.common.base.Preconditions.checkNotNull;


public class AlwaysReloadingValue<T> implements Value<T> {

	public static <T> AlwaysReloadingValue<T> of(ValueReloader<T> reloader) {
		return new AlwaysReloadingValue<T>(reloader);
	}

	private final ValueReloader<T> reloader;

	public AlwaysReloadingValue(ValueReloader<T> reloader) {
		this.reloader = reloader;
	}

	@Override
	public T get() {
		try {
			T result = reloader.reload(null);
			checkNotNull(result);
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
