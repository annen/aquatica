package com.trustename.net.util.value;


public class ConstantValue<T> implements Value<T> {

	public static <T> ConstantValue<T> of(T value) {
		return new ConstantValue<T>(value);
	}

	private T value;

	public ConstantValue() {
	}

	public ConstantValue(T value) {
		this.value = value;
	}

	@Override
	public T get() {
		return value;
	}

	public void set(T value) {
		this.value = value;
	}

}
