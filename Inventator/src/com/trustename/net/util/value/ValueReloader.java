package com.trustename.net.util.value;

public interface ValueReloader<T> {

	T reload(@Nullable T previousValue) throws Exception;

}
