package com.trustename.net.util.value;


public final class IntValue {
	private int value;

	public IntValue() {
	}

	public IntValue(int value) {
		this.value = value;
	}

	public int get() {
		return value;
	}

	public void set(int value) {
		this.value = value;
	}
	
	public int increment() {
		return ++value;
	}
	
	public int decrement() {
		return --value;
	}
	
	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
