package com.trustename.net.util.value;

import static com.google.common.base.Preconditions.checkNotNull;


public class LazyValue<T> implements Value<T> {

	public static <T> LazyValue<T> of(ValueReloader<T> reloader) {
		return new LazyValue<T>(reloader);
	}

	private final ValueReloader<T> reloader;

	private volatile T value;

	public LazyValue(ValueReloader<T> reloader) {
		this.reloader = reloader;
	}

	@Override
	public T get() {
		T result = value;
		if (result != null)
			return result;

		synchronized (this) {
			result = value;
			if (result != null)
				return result;

			try {
				result = reloader.reload(null);
				checkNotNull(result);
				value = result;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			return result;
		}

	}

}
