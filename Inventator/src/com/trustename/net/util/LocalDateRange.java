package com.trustename.net.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.Period;

public final class LocalDateRange {
	private final LocalDate start;
	private final LocalDate end;

	public LocalDateRange(LocalDate start, LocalDate end) {
		this.start = checkNotNull(start);
		this.end = checkNotNull(end);
		if (start.isAfter(end))
			throw new IllegalArgumentException("Start date after end date");
	}

	public LocalDateRange(LocalDate date) {
		this(date, date);
	}

	public LocalDateRange(LocalDate start, Period period) {
		this(start, start.plus(period));
	}

	public LocalDateRange(Period period, LocalDate end) {
		this(end.minus(period), end);
	}

	public LocalDate getStart() {
		return start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public List<LocalDate> getDates() {
		List<LocalDate> dates = new ArrayList<>();
		for (LocalDate date = start; !date.isAfter(end); date = date.plusDays(1)) {
			dates.add(date);
		}
		return dates;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || getClass() != o.getClass())
			return false;
		LocalDateRange r = (LocalDateRange) o;
		return start.equals(r.start) && end.equals(r.end);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + start.hashCode();
		hash = 31 * hash + end.hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return start.toString() + " - " + end.toString();
	}

}
