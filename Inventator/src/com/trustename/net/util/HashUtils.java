package com.trustename.net.util;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class HashUtils {
	private static final Charset UTF8 = Charset.forName("UTF-8");
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

	public static byte[] md5(byte[] bytes) {
		try {
			return MessageDigest.getInstance("MD5").digest(bytes);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] md5(String s) {
		return md5(s.getBytes(UTF8));
	}

	public static String md5hex(byte[] bytes) {
		return encodeHex(md5(bytes));
	}

	public static String md5hex(String s) {
		return encodeHex(md5(s));
	}

	public static String encodeHex(byte[] bytes) {
		StringBuilder hexString = new StringBuilder();
		for (byte b : bytes) {
			String s = Integer.toHexString(0xFF & b);
			if (s.length() == 1)
				hexString.append('0');
			hexString.append(s);
		}
		return hexString.toString();
	}

	public static byte[] sha1(byte[] bytes) {
		try {
			return MessageDigest.getInstance("SHA-1").digest(bytes);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] sha1(String s) {
		return sha1(s.getBytes(UTF8));
	}

	public static String sha1hex(byte[] bytes) {
		return encodeHex(sha1(bytes));
	}

	public static String sha1hex(String s) {
		return encodeHex(sha1(s));
	}

	public static byte[] hmacSha1(byte[] key, byte[] data) {
		SecretKeySpec signingKey = new SecretKeySpec(key, HMAC_SHA1_ALGORITHM);
		Mac mac;
		try {
			mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			throw new IllegalStateException(e);
		}
		return mac.doFinal(data);
	}

	public static String hmacSha1Hex(String key, String data) {
		return encodeHex(hmacSha1(key.getBytes(), data.getBytes()));
	}
}
