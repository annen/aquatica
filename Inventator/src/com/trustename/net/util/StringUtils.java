package com.trustename.net.util;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Function;
import com.trustename.net.UrlEncode;
import com.trustename.net.util.value.Nullable;

public final class StringUtils {
	private StringUtils() {
	}

	public static String toString(Object o) {
		return o == null ? null : o.toString();
	}

	public static String reverse(String s) {
		if (s.length() < 2)
			return s;
		return new StringBuilder(s).reverse().toString();
	}

	public static String toFirstUpper(String s) {
		if (s.isEmpty())
			return s;
		if (s.length() == 1)
			return s.toUpperCase();
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	public static String toTitleCase(String s) {
		if (s.isEmpty())
			return s;
		if (s.length() == 1)
			return s.toUpperCase();
		StringBuilder sb = new StringBuilder(s);
		boolean upper = true;
		for (int i = 0; i < sb.length(); ++i) {
			char ch = sb.charAt(i);
			if (Character.isWhitespace(ch)) {
				upper = true;
				continue;
			}
			if (upper) {
				upper = false;
				sb.setCharAt(i, Character.toUpperCase(ch));
			} else {
				sb.setCharAt(i, Character.toLowerCase(ch));
			}
		}
		return sb.toString();
	}

	public static int indexOf(CharSequence s, char ch, int index) {
		for (; index < s.length(); ++index) {
			if (s.charAt(index) == ch)
				return index;
		}
		return -1;
	}

	public static int indexOfAny(CharSequence s, char ch1, char ch2, int index) {
		for (; index < s.length(); ++index) {
			char ch = s.charAt(index);
			if (ch == ch1 || ch == ch2)
				return index;
		}
		return -1;
	}

	public static int indexOfAny(CharSequence s, CharSequence find, int index) {
		for (; index < s.length(); ++index) {
			char ch = s.charAt(index);
			for (int i = 0; i < find.length(); ++i) {
				if (ch == find.charAt(i))
					return index;
			}
		}
		return -1;
	}

	public static int replaceVariables(StringBuilder sb, String prefix, String suffix, Function<String, String> pred, int pos) {
		while (pos < sb.length()) {
			int pos1 = sb.indexOf(prefix, pos);
			if (pos1 < 0) {
				break;
			}
			int pos2 = sb.indexOf(suffix, pos1 + prefix.length());
			if (pos2 < 0) {
				break;
			}
			String name = sb.substring(pos1 + prefix.length(), pos2);
			String value = pred.apply(name);
			sb.replace(pos1, pos2 + suffix.length(), value);
			pos = pos1 + value.length();
		}
		return pos;
	}

	public static String replaceVariables(String s, String prefix, String suffix, Function<String, String> pred, int pos) {
		pos = s.indexOf(prefix, pos);
		if (pos < 0)
			return s;
		StringBuilder sb = new StringBuilder(s);
		replaceVariables(sb, prefix, suffix, pred, pos);
		return sb.toString();
	}

	public static String replaceVariables(String s, String prefix, String suffix, Function<String, String> pred) {
		return replaceVariables(s, prefix, suffix, pred, 0);
	}

	public static Collection<String> getVariables(String s, final String prefix, final String suffix) {
		final Set<String> variables = newLinkedHashSet();
		StringUtils.replaceVariables(s, prefix, suffix, new Function<String, String>() {
			@Override
			public String apply(String input) {
				variables.add(prefix + input + suffix);
				return "";
			}
		});
		return variables;
	}

	// returns map {variable -> value}
	// returns null if not matched
	public static @Nullable Map<String, String> matchTemplate(String template, String s, String prefix, String suffix) {
		List<Lexem> lexems = parseTemplate(template, prefix, suffix);
		Map<String, String> vars = new LinkedHashMap<>();
		int pos = 0;
		Lexem prevLexem = new Lexem(LexemType.CONST, "");
		for (Lexem lexem : lexems) {
			if (lexem.type == LexemType.CONST) {
				if (prevLexem.type == LexemType.CONST) {
					int pos1 = pos + lexem.value.length();
					if (pos1 > s.length())
						return null;
					String value = s.substring(pos, pos1);
					if (!lexem.value.equals(value))
						return null;
					pos = pos1;
				} else {
					int pos1 = s.indexOf(lexem.value, pos);
					if (pos1 < 0)
						return null;
					String value = s.substring(pos, pos1);
					vars.put(prevLexem.value, value);
					pos = pos1 + lexem.value.length();
				}
			} else {
				if (prevLexem.type == LexemType.VAR)
					return null;
			}
			prevLexem = lexem;
		}
		if (prevLexem.type == LexemType.VAR) {
			String value = s.substring(pos);
			vars.put(prevLexem.value, value);
		} else {
			if (pos < s.length())
				return null;
		}
		return vars;
	}

	private enum LexemType {
		CONST, VAR
	}

	private static class Lexem {
		final LexemType type;
		final String value;

		public Lexem(LexemType type, String value) {
			this.type = type;
			this.value = value;
		}

	}

	private static List<Lexem> parseTemplate(String template, String prefix, String suffix) {
		List<Lexem> lexems = new ArrayList<>();
		int pos = 0;
		while (pos < template.length()) {
			int pos1 = template.indexOf(prefix, pos);
			if (pos1 < 0) {
				break;
			}
			int pos2 = template.indexOf(suffix, pos1 + prefix.length());
			if (pos2 < 0) {
				break;
			}
			if (pos != pos1) {
				String s = template.substring(pos, pos1);
				lexems.add(new Lexem(LexemType.CONST, s));
			}
			String name = template.substring(pos1 + prefix.length(), pos2);
			lexems.add(new Lexem(LexemType.VAR, name));
			pos = pos2 + suffix.length();
		}
		if (pos < template.length()) {
			String s = template.substring(pos);
			lexems.add(new Lexem(LexemType.CONST, s));
		}
		return lexems;
	}

	public static void replace(StringBuilder sb, String param, String value) {
		int index = sb.indexOf(param);
		if (index < 0)
			return;
		if (value == null) {
			value = "";
		}
		sb.replace(index, index + param.length(), value);
	}

	public static void replaceAll(StringBuilder sb, String param, String value) {
		if (value == null) {
			value = "";
		}
		int index = 0;
		for (;;) {
			index = sb.indexOf(param, index);
			if (index < 0) {
				break;
			}
			sb.replace(index, index + param.length(), value);
			index += value.length();
		}
	}

	public static void replaceEncoded(StringBuilder sb, String param, String value) {
		int index = sb.indexOf(param);
		if (index < 0)
			return;
		if (value == null) {
			value = "";
		} else {
			value = UrlEncode.encode(value);
		}
		sb.replace(index, index + param.length(), value);
	}

	public static void replaceEncodedAll(StringBuilder sb, String param, String value) {
		int index = sb.indexOf(param);
		if (index < 0)
			return;
		if (value == null) {
			value = "";
		} else {
			value = UrlEncode.encode(value);
		}
		replaceAll(sb, param, value);
	}

	public static String replace(String s, String param, String value) {
		int index = s.indexOf(param);
		if (index < 0)
			return s;
		if (value == null) {
			value = "";
		}
		return s.substring(0, index) + value + s.substring(index + param.length());
	}

	public static String trimSubstringIgnoreCase(String s, String substring) {
		if (substring.isEmpty())
			return s;
		if (s.length() < substring.length())
			return s;
		int index = s.length() - substring.length();
		String ss = s.substring(index);
		if (ss.equalsIgnoreCase(substring)) {
			s = s.substring(0, index);
		}
		if (s.length() < substring.length())
			return s;
		ss = s.substring(0, substring.length());
		if (ss.equalsIgnoreCase(substring)) {
			s = s.substring(substring.length());
		}
		return s;
	}

	public static String trimLength(String s, int len) {
		if (s != null && s.length() > len) {
			s = s.substring(0, len);
		}
		return s;
	}

	public static String ellipse(String value, int len, String ellipse) {
		if (value != null && value.length() > len) {
			int endIndex = len - ellipse.length();
			if (endIndex < 0)
				return value.substring(0, len);
			else
				return value.substring(0, endIndex) + ellipse;
		}
		return value;
	}

	public static String ellipse(String value, int len) {
		return ellipse(value, len, "...");
	}

	public static String getFirstNotNullOrEmpty(String... params) {
		for (String param : params) {
			if (!isNullOrEmpty(param))
				return param;
		}
		return null;
	}

	/*
	 * A-C -> ABC -AZ -> -AZ Z-A -> exception
	 */
	public static String expandSequences(String s) {
		int index = s.indexOf('-');
		if (index < 0)
			return s;
		StringBuilder sb = new StringBuilder();
		int curr = processSequences(s, index, 0, sb);
		while (curr < s.length()) {
			index = s.indexOf('-', curr);
			if (index < 0) {
				sb.append(s.substring(curr));
				break;
			}
			curr = processSequences(s, index, curr, sb);
		}
		return sb.toString();
	}

	private static int processSequences(String s, int index, int prevIndex, StringBuilder sb) {
		int index1 = index - 1;
		int index2 = index + 1;
		if (index == 0) {
			sb.append(s.charAt(index));
			return index2;
		}
		if (index1 > prevIndex) {
			sb.append(s.substring(prevIndex, index1));
		}
		if (index == s.length() - 1) {
			sb.append(s.charAt(index1));
			sb.append(s.charAt(index));
			return index2;
		}
		int ch1 = s.charAt(index1);
		int ch2 = s.charAt(index2);
		if (ch1 > ch2)
			throw new IllegalArgumentException("Invalid sequence: " + ch1 + "-" + ch2);
		for (int ch = ch1; ch <= ch2; ++ch) {
			sb.append((char) ch);
		}
		return index + 2;
	}

	public static String prepareKeyword(String keyword) {
		keyword = keyword.trim();
		if (keyword.isEmpty())
			return keyword;
		StringBuilder sb = new StringBuilder(keyword.length());
		boolean allowOneSpace = true;
		for (int i = 0; i < keyword.length(); i++) {
			char ch = keyword.charAt(i);
			boolean isSpace = ch <= ' ';
			if (isSpace) {
				if (!allowOneSpace) {
					continue;
				}
				ch = ' ';
				allowOneSpace = false;
			} else {
				ch = Character.toLowerCase(ch);
				allowOneSpace = true;
			}
			sb.append(ch);
		}
		return sb.toString();
	}

	public static boolean isDigit(char c) {
		return c >= '0' && c <= '9';
	}

	public static boolean isAscii(char c) {
		return isLowerCaseAscii(c) || isUpperCaseAscii(c);
	}

	public static String toLowerCaseAscii(String s) {
		int index = 0;
		for (; index < s.length(); ++index) {
			if (isUpperCaseAscii(s.charAt(index)))
				break;
		}
		if (index == s.length())
			return s;
		char[] result = new char[s.length()];
		s.getChars(0, s.length(), result, 0);
		for (; index < s.length(); ++index) {
			if (isUpperCaseAscii(result[index])) {
				result[index] = toLowerCaseAscii(result[index]);
			}
		}
		return new String(result);
	}

	private static boolean isUpperCaseAscii(char ch) {
		return ch >= 'A' && ch <= 'Z';
	}

	private static boolean isLowerCaseAscii(char ch) {
		return ch >= 'a' && ch <= 'z';
	}

	private static char toLowerCaseAscii(char ch) {
		ch += (char) 0x20;
		return ch;
	}

	private static final int POW10[] = { 1, 10, 100, 1_000, 10_000, 100_000, 1_000_000, 10_000_000 };

	public static String formatDouble(double val, int precision) {
		if (precision < 0 || precision >= POW10.length)
			throw new IllegalArgumentException("Invalid precision");
		StringBuilder sb = new StringBuilder();
		if (val < 0) {
			sb.append('-');
			val = -val;
		}
		int exp = POW10[precision];
		long lval = (long) (val * exp + 0.5);
		sb.append(lval / exp);
		long fval = lval % exp;
		if (fval != 0) {
			sb.append('.');
			for (int p = precision - 1; p > 0 && fval < POW10[p]; p--) {
				sb.append('0');
			}
			while (fval % 10 == 0) {
				fval /= 10;
			}
			sb.append(fval);
		}
		return sb.toString();
	}

	public static String formatDouble(double val) {
		return formatDouble(val, 3);
	}
}
