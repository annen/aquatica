package com.trustename.net.util.html;

public class Pair<A, B> {
	final A a;
	final B b;

	public static <X, Y> Pair<X, Y> create(X a, Y b) {
		return new Pair<X, Y>(a, b);
	}

	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}

	public A getA() {
		return this.a;
	}

	public B getB() {
		return this.b;
	}

	@Override
	public String toString() {
		return (getA() == null ? "[null]" : getA().toString()) + " " + //
				(getB() == null ? "[null]" : getB().toString());
	}
}
