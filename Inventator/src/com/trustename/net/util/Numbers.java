package com.trustename.net.util;

public final class Numbers {
	public static byte byteValue(Number value) {
		if (value == null)
			return 0;
		return value.byteValue();
	}

	public static int intValue(Number value) {
		if (value == null)
			return 0;
		return value.intValue();
	}

	public static long longValue(Number value) {
		if (value == null)
			return 0L;
		return value.longValue();
	}

	public static short shortValue(Number value) {
		if (value == null)
			return 0;
		return value.shortValue();
	}

	public static float floatValue(Number value) {
		if (value == null)
			return 0.0f;
		return value.floatValue();
	}

	public static double doubleValue(Number value) {
		if (value == null)
			return 0.0;
		return value.doubleValue();
	}
}
