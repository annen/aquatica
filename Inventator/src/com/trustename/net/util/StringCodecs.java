package com.trustename.net.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class StringCodecs {

	private static final Logger logger = LoggerFactory.getLogger(StringCodecs.class);

	public static int indexOfUrlHtmlEntity(CharSequence s) {
		return StringUtils.indexOfAny(s, '&', '%', 0);
	}

	// &lt;, &#146;, %20, %D0%BC
	public static String unescapeUrlHtml(String s) {
		int pos = indexOfUrlHtmlEntity(s);
		if (pos < 0)
			return s;
		StringBuilder sb = new StringBuilder(s);
		return unescapeUrlHtml(sb, pos).toString();
	}

	public static StringBuilder unescapeUrlHtml(StringBuilder sb, int pos) {
		while (pos >= 0 && pos < sb.length()) {
			char ch = sb.charAt(pos);
			if (ch == '&') {
				pos = decodeHtmlEntities(sb, pos);
			} else if (ch == '%') {
				pos = decodeUrlCodes(sb, pos);
			} else {
				++pos;
			}
		}
		return sb;
	}

	private static int decodeHtmlEntities(StringBuilder sb, int pos1) {
		int pos2 = StringUtils.indexOf(sb, ';', pos1 + 1);
		if (pos2 < 0)
			return pos2;
		char code = unescapeHtmlEntity(sb, pos1 + 1, pos2);
		++pos2;
		if (code == NULL_CHAR)
			return pos2;
		sb.setCharAt(pos1, code);
		++pos1;
		sb.delete(pos1, pos2);
		return pos1;
	}

	private static int decodeUrlCodes(StringBuilder sb, int pos) {
		int pos1 = pos + 1;
		int n = decodeUrlNumber(sb, pos1);
		if (n < 0)
			return pos1;
		if (n < 128) {
			sb.setCharAt(pos, (char) n);
			sb.delete(pos1, pos1 + 2);
			return pos1;
		}
		// UTF-8 codes
		logger.warn("Unsupported UTF-8 code {}", n);
		return pos1;
	}

	private static int decodeUrlNumber(CharSequence s, int pos) {
		if (pos + 2 > s.length())
			return -1;
		int d1 = Character.digit(s.charAt(pos++), 16);
		if (d1 < 0)
			return -1;
		int d2 = Character.digit(s.charAt(pos), 16);
		if (d2 < 0)
			return -1;
		return d1 * 16 + d2;
	}

	// lt, amp, #146
	private static char unescapeHtmlEntity(CharSequence s, int pos1, int pos2) {
		if (pos2 - pos1 < 2)
			return NULL_CHAR;
		if (s.charAt(pos1) != '#')
			return unescapeHtmlNamedEntity(s, pos1, pos2);
		++pos1;
		char ch = s.charAt(pos1);
		int code = -1;
		if (ch == 'x' || ch == 'X') {
			code = StringParsers.parseIntSafe(s, pos1 + 1, pos2, -1, 16);
		} else {
			code = StringParsers.parseIntSafe(s, pos1, pos2, -1, 10);
		}
		if (code < 0)
			return NULL_CHAR;
		return (char) code;
	}

	// lt
	private static Character unescapeHtmlNamedEntity(CharSequence s, int begPos, int endPos) {
		int len = endPos - begPos;
		if (len >= htmlEntities.length)
			return NULL_CHAR;
		HtmlEntity[] entities = htmlEntities[len];
		for (HtmlEntity e : entities) {
			boolean eq = true;
			for (int i = 0; i < len; ++i) {
				if (e.code[i] != s.charAt(begPos + i)) {
					eq = false;
					break;
				}
			}
			if (eq)
				return e.ch;
		}
		return NULL_CHAR;
	}

	private static final char NULL_CHAR = '\u0000';

	private static final class HtmlEntity {
		char[] code;
		char ch;

		HtmlEntity(String scode, char ch) {
			code = new char[scode.length()];
			scode.getChars(0, scode.length(), code, 0);
			this.ch = ch;
		}
	}

	private static final HtmlEntity[][] htmlEntities = {
		// 0
		{},
		// 1
		{},
		// 2
		{
			new HtmlEntity("lt", '<'),
			new HtmlEntity("gt", '>')
		},
		// 3
		{
			new HtmlEntity("amp", '&'),
			new HtmlEntity("reg", '\u00ae'),
		},
		// 4
		{
			new HtmlEntity("quot", '"'),
			new HtmlEntity("nbsp", ' '),
			new HtmlEntity("copy", '\u00a9'),
			new HtmlEntity("cent", '\u00a2'),
			new HtmlEntity("euro", '\u20ac')
		},
		// 5
		{
			new HtmlEntity("trade", '\u2122'),
			new HtmlEntity("pound", '\u00a3'),
		}
	};

	public static String escapeJava(CharSequence s) {
		StringBuilder sb = new StringBuilder(s.length());
		for (int pos = 0; pos < s.length(); pos++) {
			char ch = s.charAt(pos);
			if (ch == '\\' || ch == '\"') {
				sb.append('\\').append(ch);
			} else if (ch == '\n') {
				sb.append('\\').append('n');
			} else if (ch == '\r') {
				sb.append('\\').append('r');
			} else if (ch == '\t') {
				sb.append('\\').append('t');
			} else if (ch == '\b') {
				sb.append('\\').append('b');
			} else if (ch == '\f') {
				sb.append('\\').append('f');
			} else if (ch < 32 || ch > 0x7f) {
				sb.append(unicodeTranslateJava(ch));
			} else {
				sb.append(ch);
			}
		}
		return sb.toString();
	}

	private static CharSequence unicodeTranslateJava(char ch) {
		if (ch > 0xffff) {
			return "\\u" + hex(ch);
		} else if (ch > 0xfff) {
			return "\\u" + hex(ch);
		} else if (ch > 0xff) {
			return "\\u0" + hex(ch);
		} else if (ch > 0xf) {
			return "\\u00" + hex(ch);
		}
		return "\\u000" + hex(ch);
	}

	private static String hex(char ch) {
		return Integer.toHexString(ch);
	}
}
