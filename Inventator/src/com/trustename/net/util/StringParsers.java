package com.trustename.net.util;


public final class StringParsers {

	public static long parseLong(String str, long defValue) {
		if (str == null)
			return defValue;
		return Long.parseLong(str);
	}

	public static int parseInt(String str, int defValue) {
		if (str == null)
			return defValue;
		return Integer.parseInt(str);
	}

	public static double parseDouble(String str, double defValue) {
		if (str == null)
			return defValue;
		return Double.parseDouble(str);
	}

	public static boolean parseBoolean(String str, boolean defValue) {
		if (str == null)
			return defValue;
		return Boolean.parseBoolean(str);
	}

	public static short parseShort(String str, short defValue) {
		if (str == null)
			return defValue;
		return Short.parseShort(str);
	}

	public static byte parseByte(String str, byte defValue) {
		if (str == null)
			return defValue;
		return Byte.parseByte(str);
	}

	// returns defValue on error parsing
	public static int parseIntSafe(CharSequence s, int begIndex, int endIndex, int defValue, int radix) {
		int max = endIndex - begIndex;
		if (max <= 0)
			return defValue;

		int result = 0;
		boolean negative = false;
		int i = 0;
		int limit;
		int multmin;
		int digit;

		if (s.charAt(begIndex) == '-') {
			negative = true;
			limit = Integer.MIN_VALUE;
			i++;
		}
		else {
			limit = -Integer.MAX_VALUE;
		}
		multmin = limit / radix;
		if (i < max) {
			digit = Character.digit(s.charAt(begIndex + i), radix);
			++i;
			if (digit < 0)
				return defValue;
			result = -digit;
		}
		while (i < max) {
			// Accumulating negatively avoids surprises near MAX_VALUE
			digit = Character.digit(s.charAt(begIndex + i), radix);
			++i;
			if (digit < 0)
				return defValue;
			if (result < multmin)
				return defValue;
			result *= radix;
			if (result < limit + digit)
				return defValue;
			result -= digit;
		}
		if (negative) {
			if (i > 1) {
				return result;
			}
			else { /* Only got "-" */
				return defValue;
			}
		}
		else {
			return -result;
		}
	}

	// returns defValue on error parsing
	public static int parseIntSafe(CharSequence s, int begIndex, int endIndex, int defValue) {
		return parseIntSafe(s, begIndex, endIndex, defValue, 10);
	}

}
