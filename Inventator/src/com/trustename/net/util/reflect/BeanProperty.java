package com.trustename.net.util.reflect;

import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;

public final class BeanProperty {

	private final String name;
	private final Field field;
	private final Method setMethod;

	private BeanProperty(String name, Field field) {
		this.name = name;
		this.field = field;
		this.setMethod = null;
	}

	public BeanProperty(String name, Method setMethod) {
		this.name = name;
		this.field = null;
		this.setMethod = setMethod;
	}

	public void set(Object obj, Object value) throws Exception {
		if (field != null)
			field.set(obj, value);
		else
			setMethod.invoke(obj, value);
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		if (field != null)
			return field.getType();
		Class<?>[] params = setMethod.getParameterTypes();
		return params[0];
	}

	public static Collection<BeanProperty> getProperties(Class<?> cls) {
		List<BeanProperty> res = newArrayList();
		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			if (!isAccessableField(field))
				continue;
			res.add(new BeanProperty(field.getName(), field));
		}
		Method[] methods = cls.getDeclaredMethods();
		for (Method method : methods) {
			Class<?> type = getSetterParameterType(method);
			if (type == null)
				continue;
			String name = getSetterName(method);
			if (name == null)
				continue;
			res.add(new BeanProperty(name, method));
		}
		return res;
	}

	private static boolean isAccessableField(Field field) {
		int modifiers = field.getModifiers();
		if ((modifiers & Modifier.PUBLIC) == 0)
			return false;
		if ((modifiers & Modifier.FINAL) != 0)
			return false;
		if ((modifiers & Modifier.STATIC) != 0)
			return false;
		return true;
	}

	private static Class<?> getSetterParameterType(Method method) {
		int modifiers = method.getModifiers();
		if ((modifiers & Modifier.PUBLIC) == 0)
			return null;
		if ((modifiers & Modifier.STATIC) != 0)
			return null;
		Class<?>[] params = method.getParameterTypes();
		if (params.length != 1)
			return null;
		return params[0];
	}

	private static String getSetterName(Method method) {
		String name = method.getName();
		if (!name.startsWith("set"))
			return null;
		name = name.substring(3);
		name = name.substring(0, 1).toLowerCase() + name.substring(1);
		return name;
	}

}
