package com.trustename.net.util;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgValidation {

	public static <T> String validateNotNull(T reference) {
		if (reference == null)
			return "May not be null";
		return null;
	}

	public static String validateNotEmpty(String s) {
		if (isNullOrEmpty(s))
			return "May not be empty";
		return null;
	}

	public static String validateWithPattern(String s, Pattern pattern) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		Matcher m = pattern.matcher(s);
		if (!m.matches())
			return "Must match '" + pattern.pattern() + "'";
		return null;
	}

	public static String validatePositiveNumber(String s) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		int num;
		try {
			num = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return "Must be number";
		}
		if (num <= 0)
			return "Must be positive number";
		return null;
	}

	public static String validateNotNumber(String s) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return null;
		}
		return "May not be number";
	}

	public static String validateMd5Hash(String s) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		if (!isHexNumber(s, 32, 32))
			return "Must be MD5 hash";
		return null;
	}

	public static String validateHexNumber(String s, int minLen, int maxLen) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		if (!isHexNumber(s, minLen, maxLen))
			return "Must be hex number";
		return null;
	}

	public static String validateHexNumber(String s) {
		return validateHexNumber(s, 0, Integer.MAX_VALUE);
	}

	private static boolean isHexNumber(String s, int minLen, int maxLen) {
		if (s.length() < minLen || s.length() > maxLen)
			return false;
		for (int i = 0; i < s.length(); ++i) {
			char ch = s.charAt(i);
			boolean isOk = (ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f');
			if (!isOk)
				return false;
		}
		return true;
	}

	public static String validateLength(String s, int minLen, int maxLen) {
		String empty = validateNotEmpty(s);
		if (empty != null)
			return empty;
		if (s.length() < minLen || s.length() > maxLen)
			return "Count symbols must be from " + minLen + " to " + maxLen;
		return null;
	}
}
