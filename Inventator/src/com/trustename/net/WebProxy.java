package com.trustename.net;

import java.net.InetSocketAddress;
import java.net.Proxy;

import com.google.common.base.Objects;

public final class WebProxy {
	private final Proxy proxy;
	private final ProxyAuthenticator authenticator;

	public WebProxy(Proxy.Type type, String host, int port, String username, String password) {
		proxy = new Proxy(type, new InetSocketAddress(host, port));
		if (username == null)
			authenticator = null;
		else
			authenticator = new ProxyAuthenticator(proxy, username, password);
	}

	public WebProxy(Proxy.Type type, String host, int port) {
		this(type, host, port, null, null);
	}

	public WebProxy(String host, int port) {
		this(Proxy.Type.HTTP, host, port, null, null);
	}

	public Proxy getProxy() {
		return proxy;
	}

	public ProxyAuthenticator getAuthenticator() {
		return authenticator;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(proxy, authenticator);
	}

	@Override
	public boolean equals (Object obj) {
		if (obj == null || getClass() != obj.getClass())
			return false;
		WebProxy p = (WebProxy)obj;
		return Objects.equal(proxy,  p.proxy) && Objects.equal(authenticator,  p.authenticator);
	}

	@Override
	public String toString() {
		return proxy.toString();
	}
}
