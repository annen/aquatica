package com.annenkov;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Charsets;

public class CommerceHubImporter {
	private static final String FOUND_NEW_ROW = "FOUND NEW ROW";
	private static final String FOUND_OLD_ROW = "FOUND OLD ROW";
	private static final String DIFFERENT_COUNT = "CHANGED ";
	private static final String ORIGINAL_ROW = "NOT CHANGED";
	private final static CommerceHubImporter INSTANCE = new CommerceHubImporter();

	public final static CommerceHubImporter instance() {
		return INSTANCE;
	}

	public final CommerceHubHttpClient client;

	private CommerceHubImporter() {
		client = new CommerceHubHttpClient();
	}

	public Map<InventoryRow, String> getDiff(Map<String, InventoryRow> invOld, Map<String, InventoryRow> inv) {
		invOld = new LinkedHashMap<String, InventoryRow>(invOld);
		Map<InventoryRow, String> newRows = new LinkedHashMap<InventoryRow, String>();
		Map<InventoryRow, String> old = new LinkedHashMap<InventoryRow, String>();
		Map<InventoryRow, String> changed = new LinkedHashMap<InventoryRow, String>();
		Map<InventoryRow, String> original = new LinkedHashMap<InventoryRow, String>();
		for (Entry<String, InventoryRow> i : inv.entrySet()) {
			String upc = i.getKey();
			InventoryRow row = i.getValue();
			InventoryRow oRow = invOld.get(upc);
			if (oRow == null) {
				row.status = RowStatus.FOUND_NEW_ROW;
				newRows.put(row, FOUND_NEW_ROW);
				continue;
			}
			invOld.remove(upc);
			if (!row.count.equals(oRow.oldCount)) {
				row.status = RowStatus.DIFFERENT_COUNT;
				row.oldCount = oRow.oldCount;
				changed.put(row, DIFFERENT_COUNT + row.oldCount + "<>" + row.count);
			} else {
				row.status = RowStatus.ORIGINAL_ROW;
				row.oldCount = oRow.oldCount;
				original.put(row, ORIGINAL_ROW);
			}
		}
		for (Entry<String, InventoryRow> i : invOld.entrySet()) {
			InventoryRow row = i.getValue();
			row.status = RowStatus.FOUND_OLD_ROW;
			old.put(row, FOUND_OLD_ROW);
		}
		Map<InventoryRow, String> result = new LinkedHashMap<InventoryRow, String>();
		result.putAll(changed);
		result.putAll(newRows);
		result.putAll(old);
		result.putAll(original);
		return result;
	}

	public static void main(String[] args) throws Exception {
		CommerceHubImporter importer = CommerceHubImporter.instance();
		// checkFiles(importer);
		importer.client.login("aquaticaplumbing", "Riga201509!");
		// String allProducts = importer.client.getAllProducts();
		String allProducts = new String(Files.readAllBytes(Paths.get("./test.html")), Charsets.UTF_8);
		CommerceHubParser parser = new CommerceHubParser(allProducts);
		parser.parse();
		// LOG.log("FOUND AVAILABLE " + parser.active.size());
		// LOG.log("FOUND DISCONTINUED " + parser.discontinued.size());
		// LOG.log("FOUND UNAVAILABLE " + parser.unavailable.size());
		// LOG.log("FOUND UNKNOWN " + parser.unknown.size());
		// importer.client.changeQTY("556432106", "test", 5);
	}
}
