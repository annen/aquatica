package com.annenkov;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.annenkov.InventoryRow.Purpose;
import com.google.common.collect.Multimap;
import com.trustename.net.WebProxy;
import com.trustename.net.http.AbstractHttpBrowserClient;
import com.trustename.net.http.HttpRequest;
import com.trustename.net.http.HttpResponse;
import com.trustename.net.util.html.HtmlParserUtils;

public class CommerceHubHttpClient extends AbstractHttpBrowserClient {
	// aquaticaplumbing Riga201509! https://dsm.commercehub.com/dsm/gotoHome.do
	private static final String loginUrl = "https://apps.commercehub.com/account/login";
	private static final String homeUrl = "https://dsm.commercehub.com/dsm/gotoHome.do";
	private static final String allProductsUrl1 = "https://dsm.commercehub.com/dsm/executeSavedInventorySearch.do?standard=false&name=Inventory%3A+All+Data2&id=103748001";
	private static final String allProductsUrl2 = "https://dsm.commercehub.com/dsm/handlePromptForSearchCriteria.do";
	private static final String allProductsUrl3 = "https://dsm.commercehub.com/dsm/gotoSpecificSearchResultsPage.do?pageSize=500&level=1&goTo=1";
	@SuppressWarnings("unused")
	private static final String viewUrl = "https://dsm.commercehub.com/dsm/gotoInventoryRealmForm.do?bp=inventory&action=view_productsku&productId=";
	private static final String editUrl = "https://dsm.commercehub.com/dsm/gotoInventoryRealmForm.do?bp=inventory&action=web_editproductsku&productId=";
	private static final String editUrlPost = "https://dsm.commercehub.com/dsm/handleInventoryFormSubmission.do";
	private static final String editUrlSuccess = "https://dsm.commercehub.com/dsm/gotoInventoryRealmSuccessMessage.do";
	private static final Pattern patternBeginForm = Pattern.compile("<form id=\"credential\"[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternBeginFormAll1 = Pattern.compile("<form name=\"searchRequest\"[^>]*>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternBeginFormEdit = Pattern.compile("<form[^>]+name=\"GeneralInventoryRealmForm\"[^>]+>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternGoTo = Pattern.compile(".*window\\.location = \"([^\"]+)\";.*", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern patternAllGo = Pattern.compile(".*onclick=\"document\\.location='(/dsm/gotoNextGroupingLevel\\.do\\?values=[^']+)'.*", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MM/dd/yyyy");

	private boolean logined = false;

	public CommerceHubHttpClient() {

	}

	public CommerceHubHttpClient(WebProxy webProxy) {
		super(webProxy);
	}

	public void login(String login, String pass) throws Exception {
		if (!logined) {
			LOG.log("Get login page");
			String html = get(loginUrl).readAll();
			Map<String, String> form = HtmlParserUtils.parseForm(html, patternBeginForm);
			LOG.log("Set username = " + login + " password=" + pass);
			form.put("username", "aquaticaplumbing");
			form.put("password", "Riga201509!");
			HttpResponse resp = post(loginUrl, form);
			LOG.log("Go to home page");
			resp = get(homeUrl);
			String url = resp.url().toString();
			if (!url.equalsIgnoreCase(homeUrl)) {
				throw new RuntimeException("Unexpected Home Url " + url);
			}
		}
		LOG.log("===LOGINED===");
		logined = true;
	}

	public String getAllProducts() throws Exception {
		LOG.log("Generate report");
		String html = get(allProductsUrl1).readAll();
		Multimap<String, String> form = HtmlParserUtils.parseMultiForm(html, patternBeginFormAll1);
		LOG.log("Send report settings");
		html = post(allProductsUrl2, form).readAll();
		LOG.log("Get result report settings");
		html = get("https://dsm.commercehub.com/dsm/" + getGoToUrl(html)).readAll();
		LOG.log("Get url report");
		Matcher matcher = patternAllGo.matcher(html);
		if (!matcher.matches()) {
			throw new RuntimeException("Unexpected other content");
		}
		LOG.log("Build report");
		html = get("https://dsm.commercehub.com" + matcher.group(1)).readAll();
		LOG.log("Go to report");
		html = get("https://dsm.commercehub.com/dsm/" + getGoToUrl(html)).readAll();
		LOG.log("Send products list size in reports");
		html = get(allProductsUrl3).readAll();
		LOG.log("Get finish report");
		html = get("https://dsm.commercehub.com/dsm/" + getGoToUrl(html)).readAll();
		LOG.log("===REPORT FINISH===");
		return html;
	}

	private String getGoToUrl(String allProductsHtml) {
		Matcher gotoUrl = patternGoTo.matcher(allProductsHtml);
		if (!gotoUrl.matches()) {
			throw new RuntimeException("Unexpected other content");
		}
		return gotoUrl.group(1);
	}

	public void changeQTY(String id, String name, Purpose purpose) throws Exception {
		LOG.log(Thread.currentThread().getName() + " changed " + name);
		HttpResponse resp = get(editUrl + id);
		String html = resp.readAll();
		String descr = HtmlParserUtils.extractInnerHtml(html, "textarea");
		LinkedHashMap<String, String> form = new LinkedHashMap<String, String>();
		for (Entry<String, String> m : HtmlParserUtils.parseForm(html, patternBeginFormEdit).entrySet()) {
			if (form.size() == 9) {
				form.put("sku(0).availability", "0");
			}
			if (form.size() == 11) {
				form.put("sku(0).conditionCode", "NEW");
			}
			if (form.size() == 21) {
				form.put("sku(0).description", descr);
			}
			form.put(m.getKey(), m.getValue());
		}
		form.remove("reset");

		StringBuilder sb = new StringBuilder();
		if (purpose.newCount != null) {
			form.put("sku(0).qtyAvail", String.valueOf(purpose.newCount));
			sb.append("New Count " + purpose.newCount);
		}
		if (purpose.remoteExp) {
			form.put("sku(0).nextAvailDate", "");
			form.put("sku(0).nextShipQty", "");
			if (sb.length() > 0) {
				sb.append("+");
			}
			sb.append("Remote Next Date and Count");
		}

		if (purpose.setExp) {
			form.put("sku(0).nextAvailDate", dateFormat.print(new LocalDate().plusMonths(1)));
			form.put("sku(0).nextShipQty", "5");
			if (sb.length() > 0) {
				sb.append("+");
			}
			sb.append("Add Next Date and Count");
		}
		resp = post(editUrlPost, form);
		resp.readAll();
		String url = resp.url().toString();
		if (!url.startsWith(editUrlSuccess)) {
			throw new RuntimeException("Unexpected success Url " + url);
		}
		LOG.log("===" + Thread.currentThread().getName() + " FINISH: " + sb);
	}

	@Override
	protected HttpRequest createRequest(String url) throws IOException {
		HttpRequest request = super.createRequest(url);
		request.headers().add("Accept-Encoding", "gzip");
		return request;
	}

}
