package com.annenkov;

public enum SiteRowStatus {
	ACTIVE, DISCONTINUED, UNAVAILABLE;
}
