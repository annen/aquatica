package com.annenkov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UPCResolver {
	public static class Resolver {
		private static final Map<String, Integer> freq = new HashMap<String, Integer>();
		private static final double KEY_P = 10.0;
		private static final double PRICE_P = 4.0;
		private static final double SIZE_P = 4.0;
		private static final double SKU_P = 5.0;
		private static final double WEB_TITLE_P = 3.0;
		private final String upc;
		private String key;
		private Double price;
		private String sizeW;
		private String sizeH;
		private String sizeL;
		private String SKU;
		private String[] titles = new String[0];

		public Resolver(String upc) {
			this.upc = upc;
		}

		public Double test(String title, Double price, String info) {
			Double p = 0.0;
			title = " " + title.toLowerCase() + " ";
			info = " " + info.toLowerCase() + " ";
			price = price == null ? 0.0 : price;
			if (!upc.isEmpty() && (title.contains(upc) || info.contains(upc))) {
				p = p + KEY_P * 2;
			}
			if (key != null) {
				if (title.contains(" " + key + " ") || info.contains(" " + key + " ")) {
					p = p + KEY_P;
				} else if (title.contains(key) || info.contains(key)) {
					p = p + KEY_P * 0.7;
				}
			}
			if (this.price != null && this.price > 1 && price != null && price > 0) {
				double delta = Math.abs(price - this.price) / 1000.0;
				if (delta < 1) {
					p = p + PRICE_P * (1 - delta);
				}
			}
			if (SKU != null) {
				if (title.contains(SKU) || info.contains(SKU)) {
					p = p + SKU_P;
				}
			}
			if (sizeW != null && !sizeW.isEmpty() && ((title.contains(" " + sizeW + " ") || info.contains(" " + sizeW + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (sizeH != null && !sizeH.isEmpty() && ((title.contains(" " + sizeH + " ") || info.contains(" " + sizeH + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (sizeL != null && !sizeL.isEmpty() && ((title.contains(" " + sizeL + " ") || info.contains(" " + sizeL + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (titles != null) {
				String webTitle = prepareTitle(title);
				String webInfo = prepareTitle(info);
				for (String web_title : titles) {
					Integer f = freq.get(web_title);
					f = f == null ? 1 : f;
					if (webTitle.contains(web_title) || webInfo.contains(web_title)) {
						p = p + (WEB_TITLE_P / f.doubleValue());
					} else {
						p = p - (WEB_TITLE_P / 10.0);
					}
				}
			}
			return p;
		}

		public String getUpc() {
			return upc;
		}

		public Resolver setKey(String key) {
			this.key = key.toLowerCase();
			return this;
		}

		public Resolver setPrice(Double price) {
			this.price = price;
			return this;
		}

		public Resolver setSize(String sizeW, String sizeH, String sizeL) {
			this.sizeW = sizeW;
			this.sizeH = sizeH;
			this.sizeL = sizeL;
			return this;
		}

		public Resolver setSKU(String SKU) {
			this.SKU = SKU.toLowerCase();
			return this;
		}

		public Resolver setTitles(String... titles) {
			this.titles = titles;
			for (int i = 0; i < this.titles.length; i++) {
				this.titles[i] = prepareTitle(this.titles[i]);
				Integer f = freq.get(this.titles[i]);
				if (f == null) {
					freq.put(this.titles[i], 1);
				} else {
					freq.put(this.titles[i], f + 1);
				}
			}
			return this;
		}

		private String prepareTitle(String title) {
			return title.trim().toLowerCase().replace(",", " ").replace(".", " ").replace("$", " ").replace("  ", " ").trim();
		}

		@Override
		public String toString() {
			return "UPC:" + upc + " KEY:" + key + " TITLES:" + Arrays.toString(titles);
		}
	}

	private static final UPCResolver INSTANCE = new UPCResolver();

	public static final UPCResolver instance() {
		return INSTANCE;
	}

	final Collection<Resolver> resolvers = new ArrayList<Resolver>();
	private final Resolver def = new Resolver("").setPrice(0.0).setKey("Default");

	private UPCResolver() {
		resolvers.add(def);
		resolvers.add(new Resolver("627722002152").setKey("Waste").setTitles("Bronze").setPrice(172.50).setSize("33", "", ""));
		resolvers.add(new Resolver("627722002145").setKey("Waste").setTitles("Nickel").setPrice(172.50).setSize("33", "", ""));
		resolvers.add(new Resolver("627722002138").setKey("Waste").setTitles("Chrome").setPrice(172.50).setSize("33", "", ""));
		resolvers.add(new Resolver("627722002886").setKey("Installer").setTitles("AQ-FB-INST").setPrice(199.00).setSize("", "", ""));
		resolvers.add(new Resolver("627722002688").setKey("Italia").setTitles("Faucet").setPrice(966.00).setSize("8.75", "5.5", "4"));
		resolvers.add(new Resolver("627722002541").setKey("Matrix-D").setTitles("Faucet").setPrice(999.81).setSize("8", "7.5", "6.5"));
		resolvers.add(new Resolver("627722002558").setKey("Pure-D").setTitles("Faucet").setPrice(999.81).setSize("8.85", "8.25", "6.1"));
		resolvers.add(new Resolver("627722002534").setKey("Juno-F").setTitles("Faucet").setPrice(1104.00).setSize("32", "9", "8.6"));
		resolvers.add(new Resolver("627722002527").setKey("T4-D").setTitles("Faucet").setPrice(1104.00).setSize("12.2", "11.8", "5.2"));
		resolvers.add(new Resolver("627722002497").setKey("Inno").setTitles("Istanbul", "Faucet").setPrice(1242.00).setSize("10", "8.85", "5.2"));
		resolvers.add(new Resolver("627722002503").setKey("Inspir").setTitles("Istanbul", "Faucet").setPrice(1242.00).setSize("26.25", "11.8", "7"));
		resolvers.add(new Resolver("627722002510").setKey("Memoria").setTitles("Faucet").setPrice(1242.00).setSize("13.6", "6.6", "3.7"));

		resolvers.add(new Resolver("627722000172").setKey("014").setTitles("014A").setPrice(2484.00).setSize("59", "29.5", "23.25"));
		resolvers.add(new Resolver("627722000196").setKey("014").setTitles("014B", "014B+").setPrice(2484.00).setSize("65", "31.5", "23.75"));
		resolvers.add(new Resolver("627722000202").setKey("014").setTitles("014C").setPrice(2484.00).setSize("67", "31.5", "23.75"));
		resolvers.add(new Resolver("627722000219").setKey("014").setTitles("014D").setPrice(2484.00).setSize("70.75", "33.5", "23.75"));
		resolvers.add(new Resolver("627722000226").setKey("014").setTitles("014E").setPrice(2759.31).setSize("74.75", "35.5", "23.75"));
		resolvers.add(new Resolver("627722000233").setKey("014").setTitles("014M", "63-in L x 29.5-in W", "63 in. L X 29.5 in. W").setPrice(2484.00).setSize("63", "27.5", "22.75"));
		resolvers.add(new Resolver("627722000332").setKey("026").setTitles("026A").setPrice(2756.55).setSize("63", "31.5", "22.8"));
		resolvers.add(new Resolver("627722000349").setKey("026").setTitles("026B").setPrice(2691.00).setSize("67", "31.9", "23.5"));
		resolvers.add(new Resolver("627722000356").setKey("026").setTitles("026C").setPrice(2898.00).setSize("70.75", "33.5", "23.5"));
		resolvers.add(new Resolver("627722000394").setKey("028").setTitles("028A", "25.5").setPrice(2208.00).setSize("47.25", "29.5", "25.5"));
		resolvers.add(new Resolver("627722000400").setKey("028").setTitles("028B", "51", "51.1", "25.5").setPrice(2208.00).setSize("51.2", "29.5", "26"));
		resolvers.add(new Resolver("627722000417").setKey("028").setTitles("028C", "55", "26", "25.5").setPrice(2208.00).setSize("55.1", "29.5", "26"));
		resolvers.add(new Resolver("627722000424").setKey("028").setTitles("028D", "58.75", "29.25", "24").setPrice(2484.00).setSize("58.6", "29.2", "24"));
		resolvers.add(new Resolver("627722000455").setKey("036").setPrice(2759.31).setSize("70.3", "34", "31.5"));
		resolvers.add(new Resolver("627722000448").setKey("034").setPrice(2622.00).setSize("", "", "26"));
		resolvers.add(new Resolver("627722000462").setKey("040").setPrice(2898.00).setSize("70.75", "33.5", "23"));
		resolvers.add(new Resolver("627722000509").setKey("060").setPrice(2898.00).setSize("63", "31.5", "31.5"));
		resolvers.add(new Resolver("627722000516").setKey("103").setPrice(2484.00).setSize("64.57", "31.5", "23.62"));
		resolvers.add(new Resolver("627722000547").setKey("114").setPrice(2484.00).setSize("62.6", "31.5", "23.62"));
		resolvers.add(new Resolver("627722000561").setKey("141").setPrice(3449.31).setSize("", "", "23.25"));
		resolvers.add(new Resolver("627722000592").setKey("148").setPrice(2759.31).setSize("65", "30", "25.5"));
		resolvers.add(new Resolver("627722000622").setKey("169").setPrice(2622.00).setSize("56.5", "29.5", "23.6"));
		resolvers.add(new Resolver("627722000639").setKey("170").setPrice(3415.50).setSize("74.4", "37.6", "26.8"));
		resolvers.add(new Resolver("627722002817").setKey("174").setTitles("174A").setPrice(2670.00).setSize("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722003067").setKey("174").setTitles("174A", "Blck", "Black").setPrice(3310.00).setSize("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722000653").setKey("174").setTitles("174B").setTitles(" 174 ").setPrice(2898.00).setSize("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722003074").setKey("174").setTitles("174B", "Blck", "Black").setPrice(3450.00).setSize("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722000660").setKey("204").setTitles("A-G").setPrice(3312.00).setSize("67", "32.75", "21"));
		resolvers.add(new Resolver("627722000677").setKey("204").setTitles("A-M", "67 in. L X 32.75 in. W X 21 in. H").setPrice(3174.00).setSize("67", "33.5", "21.25"));
		resolvers.add(new Resolver("627722000684").setKey("204").setTitles("B-M").setPrice(3449.31).setSize("70", "38.5", "20"));
		resolvers.add(new Resolver("627722000707").setKey("204").setTitles("C-M").setPrice(3449.31).setSize("73", "32.6", "22.75"));
		resolvers.add(new Resolver("627722000714").setKey("271").setPrice(2622.00).setSize("70.3", "33", "23.6"));
		resolvers.add(new Resolver("627722000721").setKey("272").setPrice(2994.60).setSize("69", "33.5", "30.3"));
		resolvers.add(new Resolver("627722000738").setKey("302").setPrice(2208.00).setSize("59", "27.5", "22.8"));
		resolvers.add(new Resolver("627722000769").setKey("306").setPrice(2415.00).setSize("68", "34.65", "23.25"));
		resolvers.add(new Resolver("627722000776").setKey("308").setTitles("308A").setPrice(3312.00).setSize("54.25", "54.25", "23.5"));
		resolvers.add(new Resolver("627722000783").setKey("308").setTitles("308B").setPrice(3415.50).setSize("59", "59", "23.625"));
		resolvers.add(new Resolver("627722000790").setKey("308").setTitles("308C").setPrice(3995.10).setSize("72", "72", "23.5"));
		resolvers.add(new Resolver("627722000806").setKey("309").setTitles("309A").setPrice(2484.00).setSize("59", "32", "23.75"));
		resolvers.add(new Resolver("627722000813").setKey("309").setTitles("309B").setPrice(2622.00).setSize("63", "33", "23.75"));
		resolvers.add(new Resolver("627722000820").setKey("309").setTitles("309C").setPrice(2622.00).setSize("67", "33.5", "23.75"));
		resolvers.add(new Resolver("627722000882").setKey("314").setPrice(2484.00).setSize("57", "57", "27"));
		resolvers.add(new Resolver("627722001971").setKey("315").setPrice(2552.31).setSize("62.2", "62.2", "26"));
		resolvers.add(new Resolver("627722000899").setKey("316").setPrice(2898.00).setSize("70.8", "37.8", "25.6"));
		resolvers.add(new Resolver("627722000905").setKey("317").setPrice(2725.50).setSize("64.6", "33", "23.6"));
		resolvers.add(new Resolver("627722000943").setKey("324").setPrice(2898.00).setSize("52.76", "52.76", "22.83"));
		resolvers.add(new Resolver("627722000950").setKey("325").setPrice(3588.00).setSize("", "", "23.62"));
		resolvers.add(new Resolver("627722000936").setKey("323").setTitles("323A").setPrice(2622.00).setSize("67", "33.5", "22.8"));
		resolvers.add(new Resolver("627722000967").setKey("327").setTitles("327B").setPrice(2484.00).setSize("56.6", "30.7", "23.2"));
		resolvers.add(new Resolver("627722000981").setKey("400").setPrice(4692.00).setSize("73.25", "59", "22"));
		resolvers.add(new Resolver("627722001162").setKey("602").setTitles("602M", "Lullaby").setPrice(3174.00).setSize("61.81", "26.97", "25.59"));
		resolvers.add(new Resolver("627722001094").setKey("602").setTitles("602M", "Lullaby", "blck").setPrice(4485.00).setSize("61.81", "26.97", "25.59"));
		resolvers.add(new Resolver("627722001322").setKey("617").setTitles("A-G").setPrice(3312.00).setSize("63", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001339").setKey("617").setTitles("A-M").setPrice(3174.00).setSize("63", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001346").setKey("617").setTitles("B-G").setPrice(3450.00).setSize("70.75", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001353").setKey("617").setTitles("B-M").setPrice(3450.00).setSize("70.75", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001438").setKey("621").setTitles("621M").setPrice(3312.00).setSize("73.62", "33.25", "23.2"));
		resolvers.add(new Resolver("627722001735").setKey("700").setTitles("B700", "B700-BN", "BN", "Brushed", "Nickel").setPrice(1366.20).setSize("4.75", "33.5", "4.75"));
		resolvers.add(new Resolver("627722001711").setKey("700").setTitles("B700", "B700-CP", "CP", "Chrome").setPrice(1104.00).setSize("4.75", "33.5", "4.75"));
		resolvers.add(new Resolver("627722001520").setKey("714").setTitles("714M").setPrice(3995.00).setSize("70.75", "31.5", "24"));
		resolvers.add(new Resolver("627722001568").setKey("748").setTitles("748M").setPrice(3312.00).setSize("63", "33.5", "28.3"));
		resolvers.add(new Resolver("627722002794").setKey("748").setTitles("748G", "Glossy").setPrice(3995.00).setSize("63", "33.5", "28.3"));
		resolvers.add(new Resolver("627722002756").setKey("748").setTitles("748-BM", "Black", "748M", "Graphite", "748M Black").setPrice(4968.00).setSize("63", "33.5", "28.3"));

		resolvers.add(new Resolver("627722002060").setKey("Arab").setTitles("Arabella", "White", "Arab-F-Wht").setPrice(3588.00).setSize("68.5", "30.3", "24.4"));
		resolvers.add(new Resolver("627722002572").setKey("Arab").setTitles("Arabella", "Left", "Corner", "Arabella-L", "Aquastone White Bathtub with Rounded Left Corner -> With Rounded Left Corner").setPrice(3995.00).setSize("72", "33.85", "24.4"));
		resolvers.add(new Resolver("627722002565").setKey("Arab").setTitles("Arabella", "Wall", "Arabella-Wall", "Aquastone White Bathtub with Rounded Left Corner -> WithWall Ledge").setPrice(3995.00).setSize("68.5", "33.85", "24.4"));
		resolvers.add(new Resolver("627722001766").setKey("AdoreMe").setTitles("AdoreMe-Wht", "White").setPrice(3795.00).setSize("74.8", "35.75", "26.5"));
		resolvers.add(new Resolver("627722002343").setKey("Allegra").setTitles("Allegra-Blt", "Built-in", "Allegra-Blt-in", "-> Drop-in").setPrice(2759.31).setSize("", "", "26.75"));
		resolvers.add(new Resolver("627722002336").setKey("Allegra").setTitles("Allegra-Wht", "Freestanding", "-> Freestanding").setPrice(3995.10).setSize("", "", "26.75"));
		resolvers.add(new Resolver("6277AlleGran").setKey("Allegra").setTitles("Allegra-G", "Granite", "Gran").setPrice(3995.10).setSize("74.75", "74.75", "26.75"));
		resolvers.add(new Resolver("627722002374").setKey("Alex").setPrice(2898.00).setSize("67", "35.25", "25.2"));
		resolvers.add(new Resolver("627722001759").setKey("AdmireMe").setTitles("AdmireMe-Wht", "White").setPrice(7038.00).setSize("78.75", "59", "30.75"));
		resolvers.add(new Resolver("6277AdmMeRED").setKey("AdmireMe").setTitles("AdmireMe-Red", "Red").setPrice(8950.00).setSize("78.75", "59", "33"));
		resolvers.add(new Resolver("627722002718").setKey("AdmireMe").setTitles("AdmireMe-Relax", "Relax").setPrice(11005.50).setSize("78.75", "59", "33.25"));
		resolvers.add(new Resolver("627722002107").setKey("Cleo").setTitles("Cleopatra", "Cleopatra-Wht", "Cleo-Wht").setPrice(2622.00).setSize("61", "61", "25.25"));
		resolvers.add(new Resolver("627722000011").setKey("Cocoon").setTitles("White Corner Lucite with Microban").setPrice(2898.00).setSize("63", "31.5", "24.4"));
		resolvers.add(new Resolver("627722002664").setKey("Colonna").setTitles("Chrome").setPrice(1607.70).setSize("37.5", "7.25", "31"));
		resolvers.add(new Resolver("627722002671").setKey("Colonna").setTitles("Nickel").setPrice(1814.70).setSize("37.5", "7.25", "31"));
		resolvers.add(new Resolver("627722002695").setKey("Caesar").setTitles("Chrome").setPrice(1745.70).setSize("34.5", "4.75", "11"));
		resolvers.add(new Resolver("627722002701").setKey("Caesar").setTitles("Bronze").setPrice(1952.70).setSize("34.5", "4.75", "11"));
		resolvers.add(new Resolver("627722002923").setKey("Daydreamer-C").setTitles("Daydreamer").setPrice(5382.00).setSize("71", "31.5", "23.62"));
		resolvers.add(new Resolver("627722002114").setKey("Elise").setTitles("Elise-Wht", "White").setPrice(3995.00).setSize("67", "32", "24.5"));
		resolvers.add(new Resolver("627722002046").setKey("Emma").setTitles("Emmanuelle-Wht", "White", "Emma-Wht").setPrice(3995.00).setSize("71.75", "33.75", "31"));
		resolvers.add(new Resolver("627722002404").setKey("Evol").setTitles("Evolution-Wht", "White").setPrice(3312.00).setSize("74.8", "68.9", "30.71"));
		resolvers.add(new Resolver("627722002619").setKey("Evol").setTitles("Evolution-Wht", "Relax", "Evo-Wht-Rlx").setPrice(7935.00).setSize("74.8", "68.9", "30.71"));
		resolvers.add(new Resolver("627722002053").setKey("Fido").setTitles("Fido-Wht", "White", "Wht").setPrice(3449.00).setSize("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002596").setKey("Fido").setTitles("Fido-Black", "Black").setPrice(4485.00).setSize("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002169").setKey("Fido").setTitles("Fido-Red", "Red").setPrice(4485.00).setSize("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002299").setKey("Gemina").setTitles("Gemn", "Gemn-Rect", "Rect", "Freestanding").setPrice(3415.00).setSize("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002312").setKey("Gemina").setTitles("Gemn", "Gemn-R", "Right").setPrice(3415.50).setSize("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002305").setKey("Gemina").setTitles("Gemn", "Gemn-L", "Left").setPrice(3415.55).setSize("67", "29.5", "24.6"));
		resolvers.add(new Resolver("627722002329").setKey("Gemina").setTitles("Gemn", "Gemn-W", "Wall").setPrice(3415.55).setSize("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002640").setKey("Harmony").setTitles("Duo").setPrice(2208.00).setSize("70.85", "31.5", "22.25"));
		resolvers.add(new Resolver("627722002091").setKey("Idea").setTitles("Idea-L", "Left").setPrice(2484.00).setSize("59", "35.75", "25.25"));
		resolvers.add(new Resolver("627722002190").setKey("Idea").setTitles("Idea-R", "Right").setPrice(2484.00).setSize("59", "35.75", "25.25"));
		resolvers.add(new Resolver("627722002398").setKey("Imagin").setTitles("Istanbul").setPrice(3995.10).setSize("", "", "22"));
		resolvers.add(new Resolver("627722002411").setKey("inno").setTitles("Istanbul", "White").setSKU("Istan-Innov-Wht").setPrice(2898.00).setSize("74.8", "35.5", "22"));
		resolvers.add(new Resolver("627722000028").setKey("Inspir").setTitles("Istanbul", "White", "Wht").setPrice(8273.10).setSize("", "", "22"));
		resolvers.add(new Resolver("627722002206").setKey("Inflection").setTitles("A-L", "Left", "Corner").setPrice(2725.50).setSize("59.5", "29.53", "22.25"));
		resolvers.add(new Resolver("627722002237").setKey("Inflection").setTitles("A-F", "Without Wall", "Freestanding").setPrice(2725.50).setSize("61.42", "29.5", "22.25"));
		resolvers.add(new Resolver("627722001872").setKey("Inflection").setTitles("A-Rect", "Rectangular", "Rect").setPrice(2725.50).setSize("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002213").setKey("Inflection").setTitles("A-R", "Right", "Corner").setPrice(2725.50).setSize("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002220").setKey("Inflection").setTitles("A-W", "With Wall", "Wall").setPrice(2725.50).setSize("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002282").setKey("Inflection").setTitles("B-F", "Freestanding").setPrice(2898.00).setSize("68.9", "31.5", "25.75"));
		resolvers.add(new Resolver("627722002251").setKey("Inflection").setTitles("B-L", "Left", "Corner", "69 X 31.5 X 25.75", "Inflection-B EcoMarmor Stone Bathtub").setPrice(2898.00).setSize("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002244").setKey("Inflection").setTitles("B-Rect", "Rectangular", "Rect").setPrice(2898.00).setSize("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002268").setKey("Inflection").setTitles("B-R", "Right", "Corner").setPrice(2898.00).setSize("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002275").setKey("Inflection").setTitles("B-W", "With Wall", "Wall").setPrice(2898.00).setSize("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722001032").setKey("Karolina").setTitles("Fine Matte", "Matte", "Wht-Matte", "White").setPrice(4278.00).setSize("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722001025").setKey("Karolina").setTitles("Hand-Polished", "Glossy").setPrice(4795.00).setSize("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002749").setKey("Karolina").setTitles("Graphite", "Black").setPrice(6348.00).setSize("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002732").setKey("Karolina").setTitles("Relax", "Fine Matte", "With").setPrice(8935.50).setSize("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002725").setKey("Karolina").setTitles("Relax", "Hand-Polished", "Glossy").setPrice(8935.50).setSize("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002077").setKey("Lacus").setTitles("Lacus-Wht", "White").setPrice(2622.00).setSize("70", "70", "28.25"));
		resolvers.add(new Resolver("627722002633").setKey("Lacus").setTitles("Lacus-Wht", "Relax").setPrice(7935.00).setSize("70", "70", "28.25"));
		resolvers.add(new Resolver("627722000035").setKey("Liquid").setTitles("Space").setPrice(2898.00).setSize("55.1", "55.1", "22.5"));
		resolvers.add(new Resolver("627722001834").setKey("LoveMe").setTitles("LoveMe-Wht", "White").setPrice(3415.50).setSize("71", "33.5", "29"));
		resolvers.add(new Resolver("627722002589").setKey("LoveMe").setTitles("LoveMe-Blck", "LoveMe-Blck-Wht", "Black").setPrice(3726.00).setSize("71", "33.5", "29"));
		resolvers.add(new Resolver("627722000097").setKey("Noa 1").setTitles("Noa", "Semi-Freestanding").setPrice(3999.00).setSize("70.75", "31.5", "23"));
		resolvers.add(new Resolver("627722000103").setKey("Noa 2").setTitles("Noa", "Semi-Freestanding").setPrice(4499.00).setSize("78.75", "35.5", "23"));
		resolvers.add(new Resolver("627722001858").setKey("Nostalgia").setTitles("Nostalgia-Wht-Stn-Legs", "Stn-Legs", "Satin").setPrice(3415.50).setSize("67", "32.75", "29.75"));
		resolvers.add(new Resolver("627722001865").setKey("Nostalgia").setTitles("Nostalgia-Wht-Ash-Legs", "Ash-Legs", "Ash").setPrice(3726.00).setSize("67", "32.75", "29.75"));
		resolvers.add(new Resolver("627722002084").setKey("Oliv").setTitles("Olivia", "White", "Oliv-Wht").setPrice(2484.00).setSize("55", "55", "26.75"));
		resolvers.add(new Resolver("627722002800").setKey("Oliv").setTitles("Olivia", "Olivia-Wht-Relax", "White", "Relax").setPrice(6348.00).setSize("55", "55", "26.75"));
		resolvers.add(new Resolver("627722001810").setKey("Organic").setTitles("Organic-Bath-Coffee", "Coffee").setPrice(4968.00).setSize("91.25", "53.1", "28.75"));
		resolvers.add(new Resolver("627722001803").setKey("Organic").setTitles("Organic-Bath-Wht", "White", "Wht").setPrice(4795.50).setSize("91.25", "53.1", "28.75"));
		resolvers.add(new Resolver("627722002381").setKey("Pamela").setTitles("Pamela-Wht", "White").setPrice(3995.10).setSize("", "", "29.5"));
		resolvers.add(new Resolver("627722002626").setKey("Pamela").setTitles("Relax", "Pamela-Wht-Relax", "White", "With").setPrice(7935.00).setSize("", "", "29.53"));
		resolvers.add(new Resolver("627722002428").setKey("Pool").setTitles("Pool-Wht", "White").setPrice(3995.10).setSize("63", "63", "27.5"));
		resolvers.add(new Resolver("627722002442").setKey("Pool").setTitles("Solo-Wht", "Solo").setPrice(4692.00).setSize("74.8", "51.2", "23.4"));
		resolvers.add(new Resolver("627722002435").setKey("Pool").setTitles("Duo-Wht", "Duo").setPrice(4692.00).setSize("74.8", "55.1", "23.4"));
		resolvers.add(new Resolver("627722002930").setKey("Pure-1").setTitles("Pure 1", "LOak", "Light Oak", "Pure-1-Wht-LOak").setPrice(3933.00).setSize("67", "31.5", "24"));
		resolvers.add(new Resolver("627722003043").setKey("Pure-1").setTitles("Pure 1", "DOak", "Dark Oak", "Pure-1-Wht-DOak").setPrice(3933.00).setSize("67", "31.5", "24"));
		resolvers.add(new Resolver("627722002947").setKey("Pure-2").setTitles("Pure 2", "DOak", "Dark Oak", "Pure-2-Wht-DOak").setPrice(5382.00).setSize("82.75", "31.5", "24"));
		resolvers.add(new Resolver("627722003050").setKey("Pure-2").setTitles("Pure 2", "LOak", "Light Oak", "Pure-2-Wht-LOak").setPrice(5382.00).setSize("82.75", "31.5", "24"));
		resolvers.add(new Resolver("627722001698").setKey("Silence").setPrice(2898.00).setSize("67", "35.5", "27"));
		resolvers.add(new Resolver("627722002916").setKey("Sincera").setTitles("Sinc").setPrice(3449.31).setSize("63", "28", "24"));
		resolvers.add(new Resolver("627722002350").setKey("Sensuality").setTitles("Mini", "White").setPrice(3588.00).setSize("66.5", "33", "26.75"));
		resolvers.add(new Resolver("627722002367").setKey("Sensuality").setTitles("Mini", "Wall", "White").setPrice(3995.10).setSize("66.5", "37", "25.75"));
		resolvers.add(new Resolver("627722001940").setKey("Sensuality").setTitles("Wht", "Sensuality-White", "White").setPrice(3995.10).setSize("70", "35", "25.5"));
	}

	public Resolver getUPC(String title, Double price, String info) {
		Resolver maxRes = def;
		Double max = 5.0;
		for (Resolver resolver : resolvers) {
			Double res = resolver.test(title, price, info);
			if (res > max) {
				max = res;
				maxRes = resolver;
			}
		}
		return maxRes;
	}
}
