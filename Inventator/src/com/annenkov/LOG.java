package com.annenkov;

import java.awt.EventQueue;

public class LOG {
	private static LogFrame log;

	public static void init() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					log = new LogFrame();
					log.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void log(String value) {
		if (log != null) {
			log.log(value);
		}
		System.out.println(value);
	}
}
