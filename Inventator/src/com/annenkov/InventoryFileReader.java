package com.annenkov;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class InventoryFileReader {
	public static Map<String, InventoryRow> readFile(File xlsxFile, boolean isOld) throws Exception {
		Map<String, InventoryRow> inv = new LinkedHashMap<String, InventoryRow>();
		if (!xlsxFile.exists() || !xlsxFile.isFile())
			return inv;
		try (FileInputStream fis = new FileInputStream(xlsxFile)) {
			try (XSSFWorkbook workbook = new XSSFWorkbook(fis)) {
				XSSFSheet mySheet = workbook.getSheetAt(0);
				Iterator<Row> rowIterator = mySheet.iterator();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					Iterator<Cell> cellIterator = row.cellIterator();
					int i = 0;
					String upc = null;
					InventoryRow iRow = new InventoryRow();
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						Object o = getObject(cell);
						iRow.row.add(o);
						i++;
						if (i == 1) {
							iRow.name = String.valueOf(o);
						}
						if (i == 2) {
							upc = String.valueOf(o);
							iRow.upc = upc;
						}
						if ((i == 3) && (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
							if (isOld) {
								iRow.oldCount = (Long) o;
							} else {
								iRow.count = (Long) o;
							}
						}
						if (i == 4) {
							iRow.info = String.valueOf(o);
						}
					}
					if (upc != null && !upc.isEmpty() && upc.startsWith("6277")) {
						inv.put(upc, iRow);
					}
				}
			}
		}
		return inv;
	}

	private static Object getObject(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		case Cell.CELL_TYPE_NUMERIC:
			return new Double(cell.getNumericCellValue()).longValue();
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();
		default:
			return cell.getCellFormula();
		}
	}
}