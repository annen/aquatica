package com.annenkov;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.annenkov.InventoryRow.Purpose;
import com.google.common.io.Files;

public class CommerceHumFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MM/dd/yyyy");
	private static int botNum = 0;
	private final ExecutorService executors = Executors.newFixedThreadPool(5, new ThreadFactory() {

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			t.setName("BOT #" + (++botNum));
			return t;
		}
	});

	private JPanel contentPane;
	private JPanel analysePanel;
	private JTextField txtLogin;
	private JTextField txtPassword;
	private File invFile;
	private CommerceHubImporter importer = CommerceHubImporter.instance();
	private JTextArea txtrDiff;
	private Map<InventoryRow, String> diff;
	private JLabel lblStatus;
	private JPanel productsPanel;
	private JLabel lblFilepath;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					CommerceHumFrame frame = new CommerceHumFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CommerceHumFrame() throws Exception {
		setTitle("Inventator");
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 50, 940, 700);
		LOG.init();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));

		JPanel startPanel = new JPanel();
		contentPane.add(startPanel, "StartPanel");

		JLabel lblLogin = new JLabel("Login");
		lblLogin.setBounds(10, 16, 137, 14);

		txtLogin = new JTextField();
		txtLogin.setBounds(10, 30, 137, 20);
		txtLogin.setText("aquaticaplumbing");
		txtLogin.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(173, 16, 137, 14);

		txtPassword = new JTextField();
		txtPassword.setBounds(173, 30, 137, 20);
		txtPassword.setText("Riga201509!");
		txtPassword.setColumns(10);

		JLabel lblTitle = new JLabel("Configuration");
		lblTitle.setBounds(816, 11, 96, 20);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 16));

		lblFilepath = new JLabel("");
		lblFilepath.setBounds(173, 61, 472, 14);

		JButton btnSelectfile = new JButton("Select File ...");
		btnSelectfile.setBounds(10, 57, 144, 23);
		btnSelectfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selectFile();
			}
		});

		JButton btnAnalyze = new JButton("Analyze");
		btnAnalyze.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					analyse();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(CommerceHumFrame.this, "ERROR: " + e.getMessage());
				}
			}
		});
		btnAnalyze.setBounds(816, 578, 96, 30);

		JLabel lblReadstatus = new JLabel("Read status");
		lblReadstatus.setBounds(10, 91, 58, 14);

		txtrDiff = new JTextArea();
		txtrDiff.setFont(new Font("Courier New", Font.PLAIN, 12));
		txtrDiff.setBounds(10, 109, 796, 540);
		JScrollPane scroll = new JScrollPane(txtrDiff);
		scroll.setSize(796, 544);
		scroll.setPreferredSize(new Dimension(796, 544));
		scroll.setLocation(10, 105);
		startPanel.setLayout(null);
		startPanel.add(btnAnalyze);
		startPanel.add(lblReadstatus);
		startPanel.add(scroll);
		startPanel.add(txtLogin);
		startPanel.add(lblLogin);
		startPanel.add(lblPassword);
		startPanel.add(txtPassword);
		startPanel.add(lblTitle);
		startPanel.add(lblFilepath);
		startPanel.add(btnSelectfile);

		JButton btnSaveAsOld = new JButton("Save As Old");
		btnSaveAsOld.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveAsOld();
			}

		});
		btnSaveAsOld.setBounds(816, 619, 96, 30);
		startPanel.add(btnSaveAsOld);

		analysePanel = new JPanel();
		contentPane.add(analysePanel, "analysePanel");
		analysePanel.setLayout(new BorderLayout(0, 0));

		final JPanel panel = new JPanel();
		analysePanel.add(panel, BorderLayout.NORTH);
		panel.setPreferredSize(new Dimension(796, 25));
		panel.setLayout(null);

		lblStatus = new JLabel("Status");
		lblStatus.setBounds(0, 1, 315, 25);
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblStatus);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					productsPanel.removeAll();
					productsPanel.revalidate();
					productsPanel.repaint();
					analyse();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(CommerceHumFrame.this, "ERROR: " + e.getMessage());
				}
			}
		});
		btnRefresh.setBounds(688, 2, 110, 20);
		panel.add(btnRefresh);

		JButton btSaveOld = new JButton("Save As Old");
		btSaveOld.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveAsOld();
			}
		});
		btSaveOld.setBackground(new Color(0, 128, 128));
		btSaveOld.setBounds(808, 2, 110, 20);
		panel.add(btSaveOld);

		JScrollPane scrollPane = new JScrollPane();
		analysePanel.add(scrollPane, BorderLayout.CENTER);

		productsPanel = new JPanel();
		scrollPane.setViewportView(productsPanel);
		productsPanel.setLayout(null);
	}

	private void selectFile() {
		diff = null;
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.exists() || file.isDirectory() || (!file.getName().toLowerCase().endsWith("xls") && !file.getName().toLowerCase().endsWith("xlsx"))) {
				JOptionPane.showMessageDialog(this, "Invalid File");
				return;
			}
			invFile = file;
			lblFilepath.setText(invFile.getAbsolutePath());
			LOG.log("Open file " + invFile.getAbsolutePath());
			try {
				Map<String, InventoryRow> inventoryFile = InventoryFileReader.readFile(invFile, false);
				Map<String, InventoryRow> inventoryFileOld = InventoryFileReader.readFile(new File("./ItemRemain_OLD.xlsx"), true);
				StringBuilder sb = new StringBuilder();
				diff = importer.getDiff(inventoryFileOld, inventoryFile);
				if (inventoryFileOld.isEmpty()) {
					LOG.log("Old Invetory file not found");
				}
				for (Entry<InventoryRow, String> i : diff.entrySet()) {
					String res = i.getValue();
					InventoryRow row = i.getKey();
					sb.append(res).append(":\t").append(row).append("\r\n");
				}
				txtrDiff.setText(sb.toString());
				txtrDiff.setCaretPosition(0);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "ERROR: " + e.getMessage());
			}
		} else {
		}
	}

	private void analyse() throws Exception {
		if (diff == null) {
			JOptionPane.showMessageDialog(this, "Not set inventory file");
			return;
		}
		CardLayout cardLayout = (CardLayout) contentPane.getLayout();
		cardLayout.show(contentPane, "analysePanel");
		lblStatus.setText("Loggin  to site ...");
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					importer.client.login(txtLogin.getText(), txtPassword.getText());
					lblStatus.setText("Loading products list...");
					String allProducts = importer.client.getAllProducts();
					CommerceHubParser parser = new CommerceHubParser(allProducts);
					parser.parse();
					doBuildItems(parser);
					lblStatus.setText("");
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}

	private void doBuildItems(CommerceHubParser parser) {
		if (!parser.unknown.isEmpty()) {
			for (InventoryRow e : parser.unknown) {
				LOG.log(e.row.toString());
			}
			LOG.log("===UNKNOWN ITEMS===");
		}
		LOG.log("Draw Items");
		Map<String, InventoryRow> rows = new HashMap<String, InventoryRow>();
		for (InventoryRow e : diff.keySet()) {
			rows.put(e.upc, e);
		}
		int y = 0;
		addHeader("UPC", 0, 90);
		addHeader("ID (HD)", 90, 80);
		addHeader("PRODUCT", 170, 200);
		addHeader("HD", 370, 30);
		addHeader("AQ", 400, 30);
		addHeader("IN FILE", 430, 70);
		addHeader("EXPECTED", 500, 95);
		addHeader("", 595, 5);
		addHeader("PROPOUSE", 600, 200);
		addHeader("ACTIONS", 800, 100);
		for (Entry<String, InventoryRow> e : parser.known.entrySet()) {
			SiteRowStatus siteStatus = e.getValue().siteStatus;
			Color color = new Color(240, 150, 150);
			if (siteStatus == SiteRowStatus.ACTIVE) {
				color = new Color(100, 180 - ((y % 2) * 20), 180);
			}
			if (siteStatus == SiteRowStatus.DISCONTINUED) {
				color = new Color(180 - ((y % 2) * 10), 180 - ((y % 2) * 10), 180 - ((y % 2) * 10));
			}
			String siteUpc = e.getKey();
			Long siteCount = e.getValue().siteCount;
			InventoryRow fileRow = rows.get(siteUpc);
			Color acolor = color;
			if (fileRow == null) {
				acolor = new Color(240, 180, 100);
				fileRow = e.getValue();
				fileRow.status = RowStatus.UNKNOWN;
			}
			fileRow.siteStatus = siteStatus;
			fileRow.siteCount = siteCount;
			fileRow.expDate = e.getValue().expDate;
			fileRow.expCount = e.getValue().expCount;
			Long fileCount = fileRow.count;
			final Purpose purpose = fileRow.getPurpose();
			if (fileRow.status != RowStatus.UNKNOWN) {
				boolean needChangeColor = purpose.newCount != null && !purpose.newCount.equals(siteCount);
				acolor = needChangeColor ? new Color(100, 240, 180) : color;
			}
			// if (fileRow.status == RowStatus.DIFFERENT_COUNT) {
			// acolor = new Color(180, 100, 100);
			// }
			// if (fileRow.status == RowStatus.FOUND_NEW_ROW) {
			// acolor = new Color(100, 100, 180);
			// }
			// if (fileRow.status == RowStatus.FOUND_OLD_ROW) {
			// acolor = new Color(180, 180, 180);
			// }

			JLabel lbl = new JLabel("   " + siteUpc);
			lbl.setOpaque(true);
			lbl.setBackground(color);
			lbl.setBounds(0, y, 90, 15);
			productsPanel.add(lbl);
			final String siteId = e.getValue().info;
			lbl = new JLabel(siteId);
			lbl.setOpaque(true);
			lbl.setBackground(color);
			lbl.setBounds(90, y, 80, 15);
			productsPanel.add(lbl);
			final String SKU = e.getValue().name.substring(0, e.getValue().name.indexOf(" : "));
			lbl = new JLabel(SKU);
			lbl.setOpaque(true);
			lbl.setBackground(color);
			lbl.setBounds(170, y, 200, 15);
			productsPanel.add(lbl);

			lbl = new JLabel(String.valueOf(siteCount));
			lbl.setOpaque(true);
			lbl.setHorizontalAlignment(JLabel.CENTER);
			lbl.setBounds(370, y, 30, 15);
			lbl.setBackground(color);
			if (purpose.newCount != null) {
				strikeLbl(lbl);
			}

			productsPanel.add(lbl);

			lbl = new JLabel("" + purpose.needCount);
			lbl.setOpaque(true);
			lbl.setHorizontalAlignment(JLabel.CENTER);
			lbl.setBounds(400, y, 30, 15);
			lbl.setBackground(acolor);
			productsPanel.add(lbl);

			lbl = new JLabel("");
			lbl.setOpaque(true);
			lbl.setHorizontalAlignment(JLabel.CENTER);
			lbl.setBounds(430, y, 70, 15);
			lbl.setBackground(acolor);
			if (fileRow.status == RowStatus.UNKNOWN) {
				lbl.setText("NOT FOUND");
			} else {
				lbl.setBackground(color);
				if (!fileCount.equals(purpose.needCount)) {
					lbl.setText("" + fileCount);
				}
			}
			// if (fileRow.status == RowStatus.DIFFERENT_COUNT) {
			// lbl.setText("OLD: " + String.valueOf(fileRow.oldCount));
			// }
			// if (fileRow.status == RowStatus.FOUND_NEW_ROW) {
			// lbl.setText(" - NEW");
			// }
			// if (fileRow.status == RowStatus.FOUND_OLD_ROW) {
			// lbl.setText(" - " + "OLD");
			// }
			productsPanel.add(lbl);

			lbl = new JLabel("");
			lbl.setOpaque(true);
			lbl.setHorizontalAlignment(JLabel.CENTER);
			lbl.setBounds(500, y, 95, 15);
			lbl.setBackground(color);
			if (e.getValue().expDate != null) {
				lbl.setText(dateFormat.print(e.getValue().expDate) + " (" + e.getValue().expCount + ")");
			}
			if (purpose.remoteExp) {
				strikeLbl(lbl);
			}
			productsPanel.add(lbl);

			lbl = new JLabel();
			lbl.setOpaque(true);
			lbl.setBackground(new Color(10, 10, 10));
			lbl.setBounds(595, y, 5, 15);
			productsPanel.add(lbl);

			lbl = new JLabel(purpose.toString());
			lbl.setOpaque(true);
			lbl.setBackground(color);
			lbl.setBounds(600, y, 200, 15);
			productsPanel.add(lbl);
			if (purpose.isChanged()) {
				final JButton but = new JButton("Go");
				but.setBounds(800, y, 50, 15);
				but.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!but.isEnabled())
							return;
						but.setEnabled(false);
						executors.execute(new Runnable() {
							@Override
							public void run() {
								try {
									importer.client.changeQTY(siteId, SKU, purpose);
									but.setText("OK");
								} catch (Exception e) {
									LOG.log("ERROR:" + e);
									but.setEnabled(true);
									but.setBackground(new Color(240, 180, 180));
								}
							}
						});
					}
				});
				productsPanel.add(but);
			}
			y = y + 15;
		}
		productsPanel.setSize(796, y);
		productsPanel.setPreferredSize(new Dimension(796, y));

		productsPanel.repaint();
	}

	@SuppressWarnings("unchecked")
	private void strikeLbl(JLabel lbl) {
		@SuppressWarnings("rawtypes")
		Map attributes = lbl.getFont().getAttributes();
		attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
		lbl.setFont(new Font(attributes));
	}

	private void addHeader(String header, int x, int width) {
		JLabel lbl = new JLabel(header);
		lbl.setOpaque(true);
		lbl.setHorizontalAlignment(JLabel.CENTER);
		lbl.setBackground(new Color(180, 180, 180));
		lbl.setBounds(x, 0, width, 15);
		productsPanel.add(lbl);
	}

	private void saveAsOld() {
		if (invFile == null) {
			JOptionPane.showMessageDialog(CommerceHumFrame.this, "Not set inventory file");
			return;
		}
		try {
			File oldFile = new File("./ItemRemain_OLD.xlsx");
			oldFile.delete();
			Files.copy(invFile, oldFile);
			LOG.log("File saved as " + "ItemRemain_OLD.xlsx");
			JOptionPane.showMessageDialog(this, "File saved as " + "ItemRemain_OLD.xlsx");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(CommerceHumFrame.this, "ERROR: " + e.getMessage());
		}
	}
}
