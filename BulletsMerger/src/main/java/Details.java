import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

public class Details extends JFrame {

	private JPanel contentPane;
	private JTextField txtText;

	private final Main main;
	private Main.Bullet bullet;
	private String code;
	private JTextField txtSearch;
	private JTextArea txtrTranslate;
	private JTextArea txtrGoogleTranslate;
	private JSplitPane splitPane;
	private JList listBullets;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JScrollPane scrollPane_1;
	private Lang lang;
	private JTextArea txtProductids;

	public Details(Main main) {
		this.main = main;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		splitPane.setRightComponent(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, BorderLayout.NORTH);
		panel_4.setLayout(new BorderLayout(0, 0));

		JLabel lblSearch = new JLabel("   Search:   ");
		panel_4.add(lblSearch, BorderLayout.WEST);

		txtSearch = new JTextField();
		panel_4.add(txtSearch, BorderLayout.CENTER);
		txtSearch.setColumns(10);
		txtSearch.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent e) {
				search();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				search();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				search();
			}

			private void search() {
				String text = txtSearch.getText().trim();
				if (text.isEmpty()) {
					renderRawBullets(".*");
				} else {
					renderRawBullets(text);
				}
			}
		});
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);

		listBullets = new JList();
		listBullets.setModel(new DefaultListModel<String>());
		scrollPane.setViewportView(listBullets);

		JPanel panel_6 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_6.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_1.add(panel_6, BorderLayout.SOUTH);

		JLabel lblProducts = new JLabel("Products:   ");
		panel_6.add(lblProducts);

		txtProductids = new JTextArea();
		txtProductids.setColumns(30);
		txtProductids.setRows(1);
		txtProductids.setWrapStyleWord(true);
		txtProductids.setLineWrap(true);
		txtProductids.setText("txtProductids");
		panel_6.add(txtProductids);
		listBullets.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				JList list = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {
					int index = list.locationToIndex(evt.getPoint());
					if (index >= 0) {
						String value = (String) list.getModel().getElementAt(index);
						txtrTranslate.setText(value);
					}
				}
			}

		});

		JPanel panel_2 = new JPanel();
		splitPane.setLeftComponent(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		txtrTranslate = new JTextArea();
		txtrTranslate.setBackground(SystemColor.info);
		txtrTranslate.setRows(5);
		txtrTranslate.setLineWrap(true);
		txtrTranslate.setWrapStyleWord(true);
		panel_2.add(txtrTranslate, BorderLayout.NORTH);

		txtrGoogleTranslate = new JTextArea();
		txtrGoogleTranslate.setRows(5);
		txtrGoogleTranslate.setLineWrap(true);
		txtrGoogleTranslate.setWrapStyleWord(true);
		panel_2.add(txtrGoogleTranslate, BorderLayout.SOUTH);

		scrollPane_1 = new JScrollPane();
		panel_2.add(scrollPane_1, BorderLayout.CENTER);

		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3, BorderLayout.NORTH);
		panel_3.setLayout(new BorderLayout(0, 0));

		JLabel lblText = new JLabel("   Text:   ");
		panel_3.add(lblText, BorderLayout.WEST);

		txtText = new JTextField();
		txtText.setBackground(new Color(240, 255, 255));
		panel_3.add(txtText);
		txtText.setColumns(10);

		JPanel panel_5 = new JPanel();
		contentPane.add(panel_5, BorderLayout.SOUTH);

		JButton btnOk = new JButton("            OK            ");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				save();
			}
		});
		panel_5.add(btnOk);
		splitPane.setResizeWeight(.9d);
	}

	@Override
	protected void frameInit() {
		super.frameInit();
		Action closeAction = new AbstractAction("Close") {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		};
		Action saveAction = new AbstractAction("Save") {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		};
		JLayeredPane layeredPane = getLayeredPane();
		layeredPane.getActionMap().put("Close", closeAction);
		layeredPane.getActionMap().put("Save", saveAction);
		layeredPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Close");
		layeredPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Save");
	}

	void showDetails(int row, int col) {
		setVisible(true);
		DefaultTableModel model = (DefaultTableModel) main.table.getModel();
		code = (String) model.getValueAt(row, 0);
		bullet = main.bullets.get(code);
		txtText.setText(bullet.text);
		lang = Lang.values()[col - 1];
		String result = bullet.get(lang);
		txtrTranslate.setText(result == null ? "" : result);
		StringBuilder productIds = new StringBuilder();
		bullet.rawBullets.stream().forEach((rb) -> {
			if (productIds.length() < 100) {
				if (productIds.length() > 0) {
					productIds.append(", ");
				}
				productIds.append(rb.productSku);
			}
		});
		txtProductids.setText(productIds.toString());
		renderRawBullets(".*");
		List<String> listValues = new ArrayList<>();
		for (Main.Gipoteth gipoteth : main.calcGippos(bullet, lang)) {
			listValues.add(gipoteth.bullet.text);
		}

		Map<Lang, Translates.Translate> transMap = Translates.translates.get(bullet.code);
		if (transMap != null && transMap.containsKey(lang)) {
			txtrGoogleTranslate.setText(transMap.get(lang).text);
		}

		MyCellRenderer cellRenderer = new MyCellRenderer();
		JList<String> list = new JList<>(listValues.toArray(new String[listValues.size()]));
		list.setCellRenderer(cellRenderer);
		scrollPane_1.setViewportView(list);
		splitPane.setResizeWeight(.9d);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				JList list = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {
					int index = list.locationToIndex(evt.getPoint());
					if (index >= 0) {
						String value = (String) list.getModel().getElementAt(index);
						txtrTranslate.setText(value);
					}
				}
			}

		});
	}

	public void renderRawBullets(String regex) {
		DefaultListModel<String> listModel = (DefaultListModel<String>) listBullets.getModel();
		listModel.clear();
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
		Set<Main.RawBullet> bullets = main.rawLangBullets.get(lang);
		for (Main.RawBullet rawBullet : bullets) {
			String text = rawBullet.text;
			if (pattern.matcher(text).find()) {
				listModel.addElement(text);
			}
		}
	}

	protected void save() {
		bullet.translates.put(lang, txtrTranslate.getText());
		main.renderTable();
		main.reSave();
		setVisible(false);
	}

	class MyCellRenderer extends DefaultListCellRenderer {
		public static final String HTML_1 = "<html><body style=' vertical-align:top; width: SIZEpx; '>";
		public static final String HTML_2 = "<hr /></body></html>";

		public MyCellRenderer() {
		}

		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			int width = list.getParent().getWidth();
			String text = HTML_1.replace("SIZE", String.valueOf(width - 80)) + value.toString() + HTML_2;
			return super.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
		}

	}

}
