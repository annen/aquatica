import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

public class Main extends JFrame {

	private static final Charset CHARSET = Charset.forName("utf-8");

	static class Gipoteth {
		RawBullet bullet;

		public int position;
		Set<Integer> bPosition = null;
		Set<Integer> bnPosition = null;

		public String numbers;
		public String bNumbers;

		public Set<String> words;
		public Set<String> bwords;
		public SetView<String> intersection;

		public Set<String> transWords = Collections.emptySet();
		public SetView<String> transIntersection = Sets.union(transWords, transWords);

		public double k;

		public Gipoteth(RawBullet bullet) {
			this.bullet = bullet;
		}

		public double calc() {
			return k;
		};

		@Override
		public String toString() {
			return bullet + "\t" + calc() + "\t" + position + "\t" + bPosition + "\t" + bnPosition + "\t" + numbers + "\t" + bNumbers + "\t" + intersection + "\t" + transIntersection + "\t" + words + "\t" + bwords + "\t" + transWords;
		}

	}

	public static class Bullet {
		String code;
		String text;
		Map<Lang, String> translates = new HashMap<>();
		Map<Lang, Collection<Gipoteth>> gipp = new HashMap<>();
		List<RawBullet> rawBullets = new ArrayList<>();

		public Bullet(String text) {
			code = getCode(text);
			this.text = text;
			translates.put(Lang.ENG, text);
		}

		public String get(Lang lang) {
			String value = translates.get(lang);
			return value == null ? "" : value;

		}

		public static String getCode(String bullet) {
			bullet = bullet.trim().toLowerCase().replace("\"", "").replaceAll("[^\\d\\w\\s\\p{L}]", " ").replaceAll("\\s+", " ");

			return bullet;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public static class RawBullet {

		private static RawBullet create(String line) {
			String[] parts = line.split("\\t");
			if (parts.length != 8)
				return null;
			Lang lang = parseLang(parts[4]);
			if (lang == null)
				return null;
			String text = parts[7].trim();
			String code = Bullet.getCode(text);
			String productId = parts[0].trim();
			String productName = parts[1].trim();
			String productSku = parts[2].trim();
			String productUpc = parts[3].trim();
			int bulletNumber = Integer.parseInt(parts[5].trim().replace("F", ""));
			return new RawBullet(lang, text, code, productId, productName, productSku, productUpc, bulletNumber);
		}

		Lang lang;
		String text;
		String code;
		String productId;
		String productName;
		String productSku;
		String productUpc;
		int bulletNumber;

		public RawBullet(Lang lang, String text, String code, String productId, String productName, String productSku, String productUpc, int bulletNumber) {
			this.lang = lang;
			this.text = text;
			this.code = code;
			this.productId = productId;
			this.productName = productName;
			this.productSku = productSku;
			this.productUpc = productUpc;
			this.bulletNumber = bulletNumber;
		}

		private static Lang parseLang(String value) {
			try {
				return Lang.valueOf(value);
			} catch (Exception ex) {
			}
			return null;
		}

		@Override
		public String toString() {
			return text;
		};

		@Override
		public int hashCode() {
			return code.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == this)
				return true;
			if (!(obj instanceof RawBullet))
				return false;
			RawBullet o = (RawBullet) obj;

			return code.equals(o.code);
		}
	}

	private JPanel contentPane;
	JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					LOG.init();
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					LOG.logError(e, "Main Error");
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	private final Details details;

	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		details = new Details(this);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("All Bullets", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel_2.add(panel, BorderLayout.WEST);

		JLabel lblSearch = new JLabel("Search: ");
		panel.add(lblSearch);

		txtSearch = new JTextField();
		panel.add(txtSearch);
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10) {
					searchNext();
				}
			}
		});
		txtSearch.setColumns(50);

		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.EAST);

		JButton btnSaveBullets = new JButton("Save Bullets");
		btnSaveBullets.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (reSave()) {
					JOptionPane.showMessageDialog(null, "Saved");
				}
			}
		});
		btnSaveBullets.setBackground(new Color(135, 206, 250));
		panel_3.add(btnSaveBullets);

		JButton btnNewBullet = new JButton("New Bullet");
		btnNewBullet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = JOptionPane.showInputDialog("Enter new Bullet text (Only END)");
				if (text == null || text.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Empty bullet");
					return;
				}
				String code = Bullet.getCode(text);
				int row = 0;
				int col = 1;
				for (String c : bullets.keySet()) {
					if (c.equals(code)) {
						showCell(row, col);
						JOptionPane.showMessageDialog(null, "Bullet already exist");
						return;
					}
					row++;
				}

				Bullet bullet = new Bullet(text);
				for (Lang lang : Lang.values()) {
					if (!Lang.ENG.equals(lang)) {
						bullet.gipp.put(lang, calcGippos(bullet, lang));
					}
				}
				bullets.put(code, bullet);
				renderTable();
				showCell(table.getModel().getRowCount() - 1, 1);
				JOptionPane.showMessageDialog(null, "Bullet added");
			}
		});
		panel_3.add(btnNewBullet);

		JButton btnMergeBullets = new JButton("Merge Bullets");
		btnMergeBullets.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mergeBullets();
			}
		});
		panel_3.add(btnMergeBullets);

		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
			}

			@Override
			public void mouseReleased(MouseEvent mouseEvent) {
				JTable table = (JTable) mouseEvent.getSource();
				Point point = mouseEvent.getPoint();
				if (mouseEvent.getButton() == 2) {
					int row = table.rowAtPoint(point);
					int col = table.convertColumnIndexToModel(table.columnAtPoint(point));
					if (row >= 0 && col > 1) {
						details.showDetails(row, col);
					}
				}
			}
		});
		table.setCellSelectionEnabled(true);
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(false);
		scrollPane.setViewportView(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(
				new Object[0][0],
				new String[] {
						"code", "ENG", "RUS", "ITA", "ESP", "DEU", "FRA"
				}) {
			Class[] columnTypes = new Class[] {
					String.class, String.class, String.class, String.class, String.class, String.class, String.class
			};

			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] {
					false, true, true, true, true, true, true
			};

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		InputMap im = table.getInputMap(JTable.WHEN_FOCUSED);
		ActionMap am = table.getActionMap();

		Action deleteAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn > 1) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.setValueAt("", selectedRow, selectedColumn);
					model.fireTableCellUpdated(selectedRow, selectedColumn);
					reSave();
				}
			}
		};

		Action copyAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn >= 1) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					String data = (String) model.getValueAt(selectedRow, selectedColumn);
					StringSelection stringSelection = new StringSelection(data);
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
				}
			}
		};

		Action pasteAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn >= 1) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					Transferable data = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
					if (data != null && data.isDataFlavorSupported(DataFlavor.stringFlavor)) {
						try {
							model.setValueAt(data.getTransferData(DataFlavor.stringFlavor), selectedRow, selectedColumn);
							model.fireTableCellUpdated(selectedRow, selectedColumn);
							reSave();
						} catch (Exception ex) {
							LOG.logError(ex, "Problem with paste value");
						}
					}
				}
			}
		};

		Action detailsAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn > 1) {
					details.showDetails(selectedRow, selectedColumn);
				}
			}
		};

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "Delete");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK), "Copy");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK), "Paste");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Details");
		am.put("Delete", deleteAction);
		am.put("Copy", copyAction);
		am.put("Paste", pasteAction);
		am.put("Details", detailsAction);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(0).setMinWidth(40);
		table.getColumnModel().getColumn(0).setMaxWidth(40);
		table.setDefaultRenderer(String.class, new DefaultTableCellRenderer() {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				component.setBackground(Color.white);
				if (column > 0) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					String code = (String) model.getValueAt(row, 0);
					Bullet bullet = bullets.get(code);
					int nColumn = table.convertColumnIndexToModel(column);
					if (nColumn > 0) {
						Lang lang = Lang.values()[nColumn - 1];
						if (bullet != null) {
							if (nColumn > 1 && bullet.translates != null && bullet.translates.get(lang) != null && !bullet.translates.get(lang).isEmpty()) {
								component.setBackground(new Color(200, 255, 200));
							} else if (bullet.gipp != null && bullet.gipp.get(lang) != null && !bullet.gipp.get(lang).isEmpty()) {
								component.setBackground(new Color(255, 255, 220));
							}
						}
					}
					if (row == lastSearchRow && column == lastSearchCol) {
						component.setBackground(new Color(200, 255, 255));
					}
				}
				return component;
			}
		});
		read();
		fullScan();
	}

	Map<String, Bullet> bullets = new LinkedHashMap<>();
	Map<String, RawBullet> rawBullets = new HashMap<>();
	Map<Lang, Set<RawBullet>> rawLangBullets = new HashMap<>();

	Map<Integer, List<RawBullet>> positions = new HashMap<>();
	private JTextField txtSearch;

	private void read() {
		Translates.init();
		Proofs.init();
		File file = new File("./save.txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				LOG.logError(ex, "Problem create save file");
			}
		}
		bullets = load(file);
		renderTable();
		try {
			List<String> lines = Files.readAllLines(new File("./bullets.txt").toPath(), CHARSET);
			int i = 0;
			LogValue log = LOG.logInfo("Start Read");
			for (String line : lines) {
				RawBullet b = RawBullet.create(line);
				if (b != null) {
					if (b.lang == Lang.ENG) {
						Bullet bullet = bullets.get(b.code);
						if (bullet == null) {
							bullet = new Bullet(b.text);
							bullets.put(b.code, bullet);
						}
						bullet.rawBullets.add(b);
						Map<Lang, String> proofMap = Proofs.proofs.get(bullet.code);
						if (proofMap != null) {
							for (Lang lang : Lang.values()) {
								String proofValue = proofMap.get(lang);
								if (proofValue != null && !proofValue.isEmpty()) {
									bullet.translates.put(lang, proofValue);
								}
							}
						}
					}
					rawBullets.put(b.code, b);
					List<RawBullet> list = positions.get(b.bulletNumber);
					if (list == null) {
						list = new ArrayList<>();
						positions.put(b.bulletNumber, list);
					}
					list.add(b);

					Set<RawBullet> list2 = rawLangBullets.get(b.lang);
					if (list2 == null) {
						list2 = new HashSet<>();
						rawLangBullets.put(b.lang, list2);
					}
					list2.add(b);
				}
				if (++i % 1000 == 0) {
					log.setValue("Readed " + i + " bullets");
				}
			}
			log.setValue("Readed " + i + " bullets");

		} catch (IOException ex) {
			LOG.logError(ex, "Problem with parse RAW bullets");
		}

		renderTable();
	}

	public void renderTable() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);

		Lang[] langs = new Lang[6];
		for (int c = 1; c < model.getColumnCount(); c++) {
			String columnName = model.getColumnName(c);
			Lang lang = Lang.valueOf(columnName.toUpperCase());
			langs[c - 1] = lang;
		}

		for (Bullet b : bullets.values()) {
			model.addRow(new Object[] { b.code, b.text, b.get(langs[1]), b.get(langs[2]), b.get(langs[3]), b.get(langs[4]), b.get(langs[5]) });
		}
	}

	public boolean reSave() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Lang[] langs = new Lang[6];
		for (int c = 1; c < model.getColumnCount(); c++) {
			String columnName = model.getColumnName(c);
			Lang lang = Lang.valueOf(columnName.toUpperCase());
			langs[c - 1] = lang;
		}

		int rowCount = model.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String code = (String) model.getValueAt(i, 0);
			Bullet bullet = bullets.get(code);
			bullet.translates.put(langs[0], (String) model.getValueAt(i, 1));
			bullet.translates.put(langs[1], (String) model.getValueAt(i, 2));
			bullet.translates.put(langs[2], (String) model.getValueAt(i, 3));
			bullet.translates.put(langs[3], (String) model.getValueAt(i, 4));
			bullet.translates.put(langs[4], (String) model.getValueAt(i, 5));
			bullet.translates.put(langs[5], (String) model.getValueAt(i, 6));
		}

		StringBuilder sb = new StringBuilder();
		for (Bullet bullet : bullets.values()) {
			sb.append(bullet.code).append("\t")
					.append(bullet.get(Lang.values()[0])).append("\t")
					.append(bullet.get(Lang.values()[1])).append("\t")
					.append(bullet.get(Lang.values()[2])).append("\t")
					.append(bullet.get(Lang.values()[3])).append("\t")
					.append(bullet.get(Lang.values()[4])).append("\t")
					.append(bullet.get(Lang.values()[5])).append("\n");
		}
		String text = sb.toString();
		try {
			File file = new File("./save.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			Files.write(file.toPath(), text.getBytes(CHARSET), StandardOpenOption.CREATE);
			return true;
		} catch (IOException ex) {
			LOG.logError(ex, "Problem with save bullets");
			return false;
		}
	}

	private static Map<String, Bullet> load(File file) {
		Map<String, Bullet> bullets = new LinkedHashMap<>();
		try {
			List<String> lines = Files.readAllLines(file.toPath(), CHARSET);
			for (String line : lines) {
				if (line.trim().isEmpty()) {
					continue;
				}
				String[] parts = line.split("\t");

				String code = parts[0];

				if (parts.length > 1) {
					String text = parts[1];
					Bullet bullet = new Bullet(text);
					bullet.translates.put(Lang.values()[0], text);
					bullets.put(code, bullet);
				}

				if (parts.length > 2) {
					String text = parts[2];
					Bullet bullet = bullets.get(code);
					bullet.translates.put(Lang.values()[1], text);
				}

				if (parts.length > 3) {
					String text = parts[3];
					Bullet bullet = bullets.get(code);
					bullet.translates.put(Lang.values()[2], text);
				}

				if (parts.length > 4) {
					String text = parts[4];
					Bullet bullet = bullets.get(code);
					bullet.translates.put(Lang.values()[3], text);
				}

				if (parts.length > 5) {
					String text = parts[5];
					Bullet bullet = bullets.get(code);
					bullet.translates.put(Lang.values()[4], text);
				}

				if (parts.length > 6) {
					String text = parts[6];
					Bullet bullet = bullets.get(code);
					bullet.translates.put(Lang.values()[5], text);
				}
			}
		} catch (IOException ex) {
			LOG.logError(ex, "Problem with reload bullets");
		}
		return bullets;
	}

	private void mergeBullets() {
		JFileChooser fileChooser = new JFileChooser();
		if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(null)) {
			File selectedFile = fileChooser.getSelectedFile();
			if (selectedFile.isFile() && selectedFile.exists()) {
				Map<String, Bullet> newBullets = load(selectedFile);
				List<String> log = new ArrayList<>();
				for (Bullet b : newBullets.values()) {
					Bullet exist = bullets.get(b.code);
					if (exist == null) {
						bullets.put(b.code, b);
						log.add("ADDED NEW: " + b.get(Lang.ENG));
					} else {
						for (Lang land : Lang.values()) {
							if (!Lang.ENG.equals(land)) {
								String bValue = b.get(land);
								String existValue = exist.get(land);
								if (bValue != null && !bValue.trim().isEmpty()) {
									if ((existValue == null || existValue.trim().isEmpty())) {
										exist.translates.put(land, bValue);
										log.add("UPDATE: " + b.get(Lang.ENG));
										log.add("  " + bValue);
									} else if (!existValue.toLowerCase().equals(bValue.toLowerCase())) {
										log.add("CONFLICT: " + b.get(Lang.ENG));
										log.add(existValue + "  <===>  " + bValue);
									}
								}
							}
						}
					}
				}

				try {
					Files.write(new File("./mergelog.txt").toPath(), log, CHARSET, StandardOpenOption.CREATE);
				} catch (IOException ex) {
					LOG.logError(ex, "Problem with save merge log");
				}
				JOptionPane.showMessageDialog(null, "Merged! Details see in mergelog.txt");
				renderTable();
			}
		}
	}

	public List<Gipoteth> calcGippos(Bullet bullet, Lang lang) {
		List<Gipoteth> gipp = new ArrayList<>();
		Set<Integer> position = new HashSet<>();
		Set<Integer> nPosition = new HashSet<>();
		for (RawBullet b : bullet.rawBullets) {
			position.add(b.bulletNumber);
			nPosition.add(b.bulletNumber + 1);
			nPosition.add(b.bulletNumber - 1);
		}
		Map<Lang, Translates.Translate> trans = Translates.translates.get(bullet.code);
		Set<String> wordsA = getWords(bullet.text);

		for (RawBullet b : rawBullets.values()) {
			double k = 0;
			if (b.lang == lang) {

				Gipoteth gipoteth = new Gipoteth(b);
				int number = b.bulletNumber;
				if (position.contains(number)) {
					k = k + 3.0 / position.size();
				}
				gipoteth.position = number;
				gipoteth.bPosition = position;
				if (nPosition.contains(number)) {
					k = k + 1.0 / nPosition.size();
				}
				gipoteth.bnPosition = nPosition;
				// k = k + 5.0 / (Math.abs(b.text.length() - bullet.text.length()) + 1);
				// k = k + 5.0 / (Math.abs(b.code.length() - bullet.code.length()) + 1);
				String bNumbers = b.code.replaceAll("[^\\d]", "");
				String bulletNumbers = bullet.code.replaceAll("[^\\d]", "");
				if (!bulletNumbers.isEmpty() && bNumbers.equals(bulletNumbers)) {
					k = k + 10;
				}
				gipoteth.bNumbers = bulletNumbers;
				gipoteth.numbers = bNumbers;

				if (Math.abs(bNumbers.length() - bulletNumbers.length()) > 4) {
					k = k - 5;
				}

				Set<String> wordsB = getWords(b.text);

				SetView<String> intersection = Sets.intersection(wordsA, wordsB);
				k = k + intersection.size() * 5;
				gipoteth.words = wordsB;
				gipoteth.bwords = wordsA;
				gipoteth.intersection = intersection;
				if (trans != null) {
					Set<String> t = trans.get(b.lang).words;
					if (t != null) {
						SetView<String> transIntersection = Sets.intersection(t, wordsB);
						k = k + transIntersection.size() * 3;

						gipoteth.transWords = t;
						gipoteth.transIntersection = transIntersection;
					}
				}
				gipoteth.k = k;
				if (k > 8) {
					gipp.add(gipoteth);
				}
			}
		}

		gipp.sort(new Comparator<Gipoteth>() {

			@Override
			public int compare(Gipoteth o1, Gipoteth o2) {
				return (int) (o2.calc() - o1.calc());
			}
		});

		return gipp;
	}

	protected void fullScan() {
		final DefaultTableModel model = (DefaultTableModel) table.getModel();
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				int r = 0;
				for (Bullet bullet : bullets.values()) {
					for (Lang lang : Lang.values()) {
						if (!Lang.ENG.equals(lang)) {
							bullet.gipp.put(lang, calcGippos(bullet, lang));
						}
					}
					model.fireTableRowsUpdated(r, r);
					r++;
				}
				renderTable();
			}
		});
		t.start();
	}

	public int getColumn(Lang lang) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (int c = 0; c < model.getColumnCount(); c++) {
			String columnName = model.getColumnName(c);
			if (lang.name().toLowerCase().equals(columnName.toLowerCase()))
				return c;
		}
		throw new RuntimeException("Unknown Column Name");
	}

	private static Set<String> getWords(String text) {
		return Arrays.asList(Bullet.getCode(text).split("\\s+")).stream().filter(new Predicate<String>() {

			@Override
			public boolean test(String t) {
				return t.length() > 3;
			}
		}).collect(Collectors.toSet());
	}

	int lastSearchRow = -1;
	int lastSearchCol = -1;

	protected void searchNext() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		String text = txtSearch.getText().toLowerCase();

		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		if (row < 0) {
			row = lastSearchRow;
		}
		if (col < 0) {
			col = lastSearchCol;
		}
		if (row < 0) {
			row = 0;
		}
		if (col < 0) {
			col = 0;
		}
		col = col + 1;
		if (col >= table.getColumnCount()) {
			col = 1;
			row = row + 1;
		}
		if (row > table.getRowCount()) {
			row = 0;
			col = 1;
		}

		int startRow = row;
		int startCol = col;
		boolean found = false;
		do {
			Object value = model.getValueAt(row, table.convertColumnIndexToModel(col));
			if (value instanceof String) {
				if (((String) value).toLowerCase().contains(text)) {
					found = true;
					break;
				}
			}
			col++;

			if (col >= table.getColumnCount()) {
				col = 1;
				row = row + 1;
			}
			if (row >= table.getRowCount()) {
				row = 0;
				col = 1;
			}

		} while (startRow != row || startCol != col);

		if (!found) {
			JOptionPane.showMessageDialog(null, "Not Found");
		}
		showCell(row, col);
	}

	public void showCell(int row, int col) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		scrollToVisible(table, row);
		table.changeSelection(row, row, true, true);
		int startRow = lastSearchRow;
		int startCol = lastSearchCol;
		lastSearchRow = row;
		lastSearchCol = col;
		model.fireTableCellUpdated(startRow, table.convertColumnIndexToModel(startCol));
		model.fireTableCellUpdated(row, table.convertColumnIndexToModel(lastSearchCol));
	}

	public static void scrollToVisible(JTable table, int rowIndex) {
		if (!(table.getParent() instanceof JViewport))
			return;
		JViewport viewport = (JViewport) table.getParent();
		Rectangle rect = table.getCellRect(rowIndex, 0, true);
		Point pt = viewport.getViewPosition();
		rect.setLocation(rect.x - pt.x, rect.y - pt.y);
		table.scrollRectToVisible(rect);
	}
}
