
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class LOG {
	private static final String ERROR_PREF = "ERROR: ";
	private static LogFrame log;
	
	public static void init() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					log = new LogFrame();
					// log.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static LogValue log(String value) {
		if (value.contains(ERROR_PREF)) {
			System.err.println(value);
		} else {
			System.out.println(value);
		}
		if (log != null)
			return log.log(value);
		return LogValue.NULL;
	}
	
	public static LogFrame getLogFrame() {
		return log;
	}
	
	public static LogValue logError(String value) {
		return log(ERROR_PREF + value);
	}
	
	public static LogValue logError(Exception ex, String value) {
		return logError(ex, value, true, ex.getMessage());
	}
	
	public static LogValue logError(Exception ex, String value, boolean saveLog, String info) {
		String message = value == null ? "" : value + " [" + ex.getMessage() + " - " + ex.getClass() + "]";
		LogValue logValue = logError(message);
		JOptionPane.showMessageDialog(null, "ERROR: " + message);
		ex.printStackTrace();
		if (saveLog) {
			try {
				File logs = new File("./logs/");
				logs.mkdirs();
				File logFile = new File(logs, System.currentTimeMillis() + ".log");
				PrintWriter printer = new PrintWriter(logFile);
				printer.println(logValue.toString());
				ex.printStackTrace(printer);
				printer.println("===============================");
				printer.println(info);
				printer.flush();
				printer.close();
			} catch (Exception ex1) {
				logError(ex1, "When SaveLogs");
			}
		}
		return logValue;
	}
	
	public static LogValue logSuccess(String value) {
		return log("SUCCESS: " + value);
	}
	
	public static LogValue logWarning(String value) {
		return log("WARNING: " + value);
	}
	
	public static LogValue logInfo(String value) {
		return log("INFO: " + value);
	}
	
	public static LogValue logOracle(String value) {
		return log("ORACLE: " + value);
	}
}
