import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

public class Translates {
	
	public static class Translate {
		public String text;
		public Set<String> words;
		
		public Translate(String text, Set<String> words) {
			this.text = text;
			this.words = words;
		}
		
	}

	public static final Map<String, Map<Lang, Translate>> translates = new HashMap<>();

	public static void init() {
		part1();
		part2();
		part3();
		part4();
		part5();
		part6();
		part7();
		part8();
		part9();
	};

	public static void part9() {
		translates
				.put(getCode("Built-in Bluetooth music player system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная система Bluetooth-плееров"))
						.put(Lang.ITA, getWords("Sistema di lettore musicale Bluetooth incorporato"))
						.put(Lang.ESP, getWords("Sistema integrado de reproductor de música Bluetooth"))
						.put(Lang.DEU, getWords("Eingebautes Bluetooth-Musik-Player-System"))
						.put(Lang.FRA, getWords("Système de lecteur de musique Bluetooth intégré")).build());
		translates
				.put(getCode("Multi-colored chromotherapy LED lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Светодиодное освещение цветной хромотерапии"))
						.put(Lang.ITA, getWords("Illuminazione a LED cromoterapia multicolore"))
						.put(Lang.ESP, getWords("Iluminación LED de cromoterapia multicolora"))
						.put(Lang.DEU, getWords("Mehrfarbige Farbtherapie LED-Beleuchtung"))
						.put(Lang.FRA, getWords("Éclairage LED chromothérapie multicolore")).build());
		translates
				.put(getCode("Mirror – 1800 x 500 mm / 70.75 x 19.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Зеркало - 1800 x 500 мм / 70,75 x 19,75 дюймов"))
						.put(Lang.ITA, getWords("Specchio - 1800 x 500 mm / 70,75 x 19,75 pollici"))
						.put(Lang.ESP, getWords("Espejo - 1800 x 500 mm / 70,75 x 19,75 pulgadas"))
						.put(Lang.DEU, getWords("Spiegel - 1800 x 500 mm / 70,75 x 19,75 in"))
						.put(Lang.FRA, getWords("Miroir - 1800 x 500 mm / 70,75 x 19,75 po")).build());
		translates
				.put(getCode("Bidet mixer with 11/4 pop up waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Биде смеситель с 1 1/4 всплывающих отходов"))
						.put(Lang.ITA, getWords("Miscelatore bidet con scarico da 1 1/4"))
						.put(Lang.ESP, getWords("Mezclador de bidé con 1 1/4 de basura emergente"))
						.put(Lang.DEU, getWords("Bidetmischer mit 1 1/4 Pop-up-Abfall"))
						.put(Lang.FRA, getWords("Mitigeur de bidet avec vidage 1 1/4")).build());
		translates
				.put(getCode("Single-piece countertop in hard-wearing, hygienic AquateX™ white"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Однокомпонентная столешница из износостойкого, гигиеничного AquateX ™ белого цвета"))
						.put(Lang.ITA, getWords("Piano di lavoro monoblocco in resistente bianco AquateX ™"))
						.put(Lang.ESP, getWords("Encimera de una sola pieza en resistente, higiénico AquateX ™ blanco"))
						.put(Lang.DEU, getWords("Einteilige Arbeitsplatte aus strapazierfähigem, hygienischem AquateX ™ weiß"))
						.put(Lang.FRA, getWords("Plan de travail monobloc en AquateX ™ blanc résistant et hygiénique")).build());
		translates
				.put(getCode("Premium backing for improved heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-поддержка для улучшения удержания тепла"))
						.put(Lang.ITA, getWords("Supporto premium per una migliore ritenzione del calore"))
						.put(Lang.ESP, getWords("Soporte de primera calidad para una mejor retención de calor"))
						.put(Lang.DEU, getWords("Premium-Backing für verbesserte Wärmespeicherung"))
						.put(Lang.FRA, getWords("Support de qualité supérieure pour une meilleure rétention de la chaleur")).build());
		translates
				.put(getCode("False ceiling mounted at the recommended height of 19.75-27.5 (50-70cm) above the user’s height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подвесной потолок, установленный на рекомендуемой высоте 19,75 -27,5 (50-70 см) выше высоты пользователя"))
						.put(Lang.ITA, getWords("Controsoffitto montato all'altezza consigliata di 19,75 -27,5 (50-70 cm) sopra l'altezza dell'utente"))
						.put(Lang.ESP, getWords("Falso techo montado a la altura recomendada de 19.75 -27.5 (50-70 cm) sobre la altura del usuario"))
						.put(Lang.DEU, getWords("Die Zwischendecke wird in der empfohlenen Höhe von 50-70 cm über der Benutzerhöhe montiert"))
						.put(Lang.FRA, getWords("Faux plafond monté à la hauteur recommandée de 19,75 -27,5 (50-70cm) au-dessus de la hauteur de l'utilisateur")).build());
		translates
				.put(getCode("Backflow preventer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords(" Превентор обратного потока"))
						.put(Lang.ITA, getWords("Backflow preventer"))
						.put(Lang.ESP, getWords("Válvula antirretorno"))
						.put(Lang.DEU, getWords("Rückflussschutz"))
						.put(Lang.FRA, getWords("Anti-refoulement")).build());
		translates
				.put(getCode("Aquatica baths are manufactured with Lucite cast acrylic sheets according to the most rigorous international standards"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ванны Aquatica производятся с литовыми акриловыми листами Lucite в соответствии с самыми строгими международными стандартами"))
						.put(Lang.ITA, getWords("I bagni Aquatica sono realizzati con lastre acriliche castite Lucite secondo i più rigorosi standard internazionali"))
						.put(Lang.ESP, getWords("Los baños Aquatica están fabricados con láminas de acrílico fundido Lucite de acuerdo con los estándares internacionales más rigurosos."))
						.put(Lang.DEU, getWords("Aquatica Bäder werden mit Lucite gegossenen Acrylplatten nach den strengsten internationalen Standards hergestellt"))
						.put(Lang.FRA, getWords("Les baignoires Aquatica sont fabriquées avec des plaques acryliques coulées Lucite selon les normes internationales les plus rigoureuses")).build());
		translates
				.put(getCode("“IIP” (Intensive Impulse Pulsation) function"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Функция «IIP» (интенсивная импульсная пульсация)"))
						.put(Lang.ITA, getWords("Funzione IIP (Intensity Impulse Pulsation)"))
						.put(Lang.ESP, getWords("Función IIP (Pulsación intensiva de impulso)"))
						.put(Lang.DEU, getWords("IIP (Intensive Impulse Pulsation) -Funktion"))
						.put(Lang.FRA, getWords("Fonction IIP (Pulsation Impulsionnelle Intensive)")).build());
		translates
				.put(getCode("Maximum 1 thick tub drain overflow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальное переливное отверстие для слива ванны толщиной 1 дюйм"))
						.put(Lang.ITA, getWords("Massimo 1 troppo pieno di scarico della vasca"))
						.put(Lang.ESP, getWords("Desbordamiento máximo del desagüe de la tina de 1 "))
						.put(Lang.DEU, getWords("Maximaler 1 dicker Wanneabflussüberlauf"))
						.put(Lang.FRA, getWords("Débordement maximal du drain de la cuve de 1 ")).build());
		translates
				.put(getCode("Absolute comfort with 2 included spa headrests"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Абсолютный комфорт с 2 включенными подголовниками"))
						.put(Lang.ITA, getWords("Comfort assoluto con 2 poggiatesta spa inclusi"))
						.put(Lang.ESP, getWords("Comodidad absoluta con 2 reposacabezas de spa incluidos"))
						.put(Lang.DEU, getWords("Absoluter Komfort mit 2 integrierten Spa Kopfstützen"))
						.put(Lang.FRA, getWords("Confort absolu avec 2 appuie-tête spa inclus")).build());
		translates
				.put(getCode("4 hole deck mounted bath mixer with diverter, spout and pull-out anti-lime hand shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("4-х уровневый башенный смеситель с дивертором, носиком и выдвижным противоизносным ручным душем"))
						.put(Lang.ITA, getWords("Miscelatore vasca a 4 fori con deviatore, bocca di erogazione e doccetta estraibile anticalcare"))
						.put(Lang.ESP, getWords("Mezclador de bañera montado en la cubierta de 4 orificios con desviador, salida y ducha de mano antiderrame"))
						.put(Lang.DEU, getWords("4-Loch-Wannenrandarmatur mit Umsteller, Auslauf und ausziehbarer Anti-Kalk-Handbrause"))
						.put(Lang.FRA, getWords("Mitigeur de bain 4 trous avec inverseur, bec verseur et douchette extensible anti-calcaire")).build());
		translates
				.put(getCode("Standard for all internationally shipped Aquatica bathtubs and selected US models (e.g.Purescape400)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандарт для всех отгруженных на международном уровне ванн Aquatica и отдельных моделей США (например, Purescape400)"))
						.put(Lang.ITA, getWords("Standard per tutte le vasche da bagno Aquatica spedite a livello internazionale e modelli selezionati negli Stati Uniti (ad es. Purbesc 400)"))
						.put(Lang.ESP, getWords("Estándar para todas las bañeras Aquatica de envío internacional y modelos seleccionados de EE. UU. (Por ejemplo, PureScape400)"))
						.put(Lang.DEU, getWords("Standard für alle international ausgelieferten Aquatica Badewannen und ausgewählte US-Modelle (zBPurescape400)"))
						.put(Lang.FRA, getWords("Standard pour toutes les baignoires Aquatica livrées à l'international et certains modèles américains (p.ex.Purescape400)")).build());
		translates
				.put(getCode("Max flow rate of 6.34 GPM (24 LPM) at 5 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 6,34 GPM (24 LPM) при давлении воды 5 бар"))
						.put(Lang.ITA, getWords("Portata massima di 6,34 GPM (24 LPM) a 5 bar di pressione dell'acqua"))
						.put(Lang.ESP, getWords("Caudal máximo de 6.34 GPM (24 LPM) a 5 bar de presión de agua"))
						.put(Lang.DEU, getWords("Max. Durchflussrate von 6,34 GPM (24 LPM) bei 5 bar Wasserdruck"))
						.put(Lang.FRA, getWords("Débit maximum de 6,34 GPM (24 LPM) à une pression d'eau de 5 bars")).build());
		translates
				.put(getCode("Matt cobalt blue lacquer sides and top"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовые кобальтовые синие лакированные стороны и верх"))
						.put(Lang.ITA, getWords("Parte superiore e laccatura blu cobalto opaco"))
						.put(Lang.ESP, getWords("Lados y parte superior de laca azul cobalto mate"))
						.put(Lang.DEU, getWords("Matt kobaltblaue Lackseiten und -oberseite"))
						.put(Lang.FRA, getWords("Côtés et dessus en laque bleu cobalt mat")).build());
		translates
				.put(getCode("Glossy dark beige lacquer bases"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глянцевые темно-бежевые лаковые основы"))
						.put(Lang.ITA, getWords("Basi laccate beige scuro lucido"))
						.put(Lang.ESP, getWords("Bases de laca beige oscuro brillante"))
						.put(Lang.DEU, getWords("Glänzende dunkelbeige Lackbasen"))
						.put(Lang.FRA, getWords("Base laquée beige foncé brillant")).build());
		translates
				.put(getCode("2 year overall limited Tranquility system and air massage warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя общая ограниченная система Tranquility и гарантия на воздушный массаж"))
						.put(Lang.ITA, getWords("Garanzia limitata di 2 anni per sistema Tranquility e air massage"))
						.put(Lang.ESP, getWords("Garantía limitada de 2 años para sistema Tranquility y masaje de aire"))
						.put(Lang.DEU, getWords("2 Jahre eingeschränkte Garantie auf Tranquility System und Luftmassage"))
						.put(Lang.FRA, getWords("Système Tranquility limité de 2 ans et garantie de massage de l'air")).build());
		translates
				.put(getCode("A mounting system for faucet installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Монтажная система для установки крана"))
						.put(Lang.ITA, getWords("Un sistema di montaggio per l'installazione di rubinetti"))
						.put(Lang.ESP, getWords("Un sistema de montaje para instalación de grifería"))
						.put(Lang.DEU, getWords("Ein Befestigungssystem für die Installation von Wasserhähnen"))
						.put(Lang.FRA, getWords("Un système de montage pour l'installation du robinet")).build());
		translates
				.put(getCode("Brass coarse thread strainer with 5/16-18 UNC thread-in crossbar (fits in most drain mechanisms)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Латунный фильтр для грубой нити с 5 / 16-18 UNC-резьбовым перекладиной (подходит для большинства сливных механизмов)"))
						.put(Lang.ITA, getWords("Filtro a filo grosso in ottone con barra filettata da 5 / 16-18 UNC (si adatta alla maggior parte dei meccanismi di drenaggio)"))
						.put(Lang.ESP, getWords("Filtro de hilo grueso de latón con barra transversal con rosca de 5 / 16-18 UNC (se adapta a la mayoría de los mecanismos de drenaje)"))
						.put(Lang.DEU, getWords("Messing-Grobgewinde-Sieb mit 5 / 16-18 UNC-Gewinde-Querstange (passt in die meisten Entwässerungsmechanismen)"))
						.put(Lang.FRA, getWords("Crépine à filetage grossier en laiton avec traverse filetée 5 / 16-18 UNC (convient à la plupart des mécanismes de vidange)")).build());
		translates
				.put(getCode("Max flow rate of 3.9 GPM (14.8 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,9 GPM (14,8 LPM)"))
						.put(Lang.ITA, getWords("Portata massima di 3,9 GPM (14,8 LPM)"))
						.put(Lang.ESP, getWords("Caudal máximo de 3.9 GPM (14.8 LPM)"))
						.put(Lang.DEU, getWords("Max Durchflussrate von 3,9 GPM (14,8 LPM)"))
						.put(Lang.FRA, getWords("Débit maximum de 3,9 GPM (14,8 LPM)")).build());
		translates
				.put(getCode("Pressure difference between hot and cold water: max. 1,5 bar"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Разность давлений между горячей и холодной водой: макс.  1,5 бар"))
						.put(Lang.ITA, getWords("Differenza di pressione tra acqua calda e acqua fredda: max. 1,5 bar"))
						.put(Lang.ESP, getWords("Diferencia de presión entre agua caliente y fría: máx. 1,5 bar"))
						.put(Lang.DEU, getWords("Druckdifferenz zwischen warmem und kaltem Wasser: max. 1,5 bar"))
						.put(Lang.FRA, getWords("Différence de pression entre l'eau chaude et froide: max. 1,5 bar")).build());
		translates
				.put(getCode("Plug and chain attached to overflow outlet – easy to unplug and empty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Штепсель и цепь, присоединенные к розетке с переливом - легко отсоединяются и пустые"))
						.put(Lang.ITA, getWords("Spina e catena attaccate alla presa di troppo pieno - facili da scollegare e svuotare"))
						.put(Lang.ESP, getWords("Enchufe y cadena conectados a la salida de desbordamiento: fácil de desenchufar y vaciar"))
						.put(Lang.DEU, getWords("Stecker und Kette an Überlauf angeschlossen - einfach zu trennen und zu entleeren"))
						.put(Lang.FRA, getWords("Branchez et attachez la chaîne à la sortie de débordement - facile à débrancher et à vider")).build());
		translates
				.put(getCode("Built-in hidden waste-overflow fitting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенный скрытый монтаж"))
						.put(Lang.ITA, getWords("Raccordo per troppopieno nascosto incorporato"))
						.put(Lang.ESP, getWords("Accesorio oculto de desbordamiento de residuos incorporado"))
						.put(Lang.DEU, getWords("Eingebauter versteckter Abfall-Überlaufanschluss"))
						.put(Lang.FRA, getWords("Garniture de trop-plein cachée intégrée")).build());
		translates
				.put(getCode("24 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("24 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords("24 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords("24 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords("24 flache Luftdüsen"))
						.put(Lang.FRA, getWords("24 jets d'air à profil bas")).build());
		translates
				.put(getCode("Use on terraces, balconies, gazebos, boats, yachts and even under camping tents"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Использовать на террасах, балконах, беседках, лодках, яхтах и ​​даже в палатках кемпинга"))
						.put(Lang.ITA, getWords("Utilizzare su terrazze, balconi, gazebo, barche, yacht e persino sotto tende da campeggio"))
						.put(Lang.ESP, getWords("Uso en terrazas, balcones, gazebos, barcos, yates e incluso en tiendas de campaña"))
						.put(Lang.DEU, getWords("Verwenden Sie auf Terrassen, Balkonen, Pavillons, Booten, Yachten und sogar unter Campingzelten"))
						.put(Lang.FRA, getWords("Utilisez sur les terrasses, balcons, gazebos, bateaux, yachts et même sous des tentes de camping")).build());
		translates
				.put(getCode("Water flow rate - 4.22 GPM (16 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Расход воды - 4,22 GPM (16 LPM)"))
						.put(Lang.ITA, getWords("Portata d'acqua - 4,22 GPM (16 LPM)"))
						.put(Lang.ESP, getWords("Caudal de agua: 4.22 GPM (16 LPM)"))
						.put(Lang.DEU, getWords("Wasserdurchflussrate - 4,22 GPM (16 LPM)"))
						.put(Lang.FRA, getWords("Débit d'eau - 4.22 GPM (16 LPM)")).build());
		translates
				.put(getCode("White Microfeel coating, breathable, easy to wash and weather resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Белое покрытие из микрофила, дышащее, легко моющееся и устойчивое к атмосферным воздействиям"))
						.put(Lang.ITA, getWords("Rivestimento bianco Microfeel, traspirante, facile da lavare e resistente agli agenti atmosferici"))
						.put(Lang.ESP, getWords("Recubrimiento Microfeel blanco, transpirable, fácil de lavar y resistente a la intemperie"))
						.put(Lang.DEU, getWords("Weiße Microfeel-Beschichtung, atmungsaktiv, leicht waschbar und witterungsbeständig"))
						.put(Lang.FRA, getWords("Revêtement Microfeel blanc, respirant, facile à laver et résistant aux intempéries")).build());
		translates
				.put(getCode("AquateX™ Matte material provides excellent heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовый материал AquateX ™ обеспечивает отличное удержание тепла и долговечность"))
						.put(Lang.ITA, getWords("Il materiale AquateX ™ Matte offre un'eccellente ritenzione di calore e durata"))
						.put(Lang.ESP, getWords("El material AquateX ™ mate proporciona una excelente retención de calor y durabilidad"))
						.put(Lang.DEU, getWords("AquateX ™ Matte Material bietet eine hervorragende Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords("Le matériau AquateX ™ Matte offre une excellente rétention de la chaleur et une excellente durabilité")).build());
		translates
				.put(getCode("Unique to acrylic bathtubs – a slot style overflow opening"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникально для акриловых ванн - отверстие для переполнения слота"))
						.put(Lang.ITA, getWords("Unico per le vasche da bagno in acrilico - un'apertura di troppopieno stile slot"))
						.put(Lang.ESP, getWords("Único en bañeras de acrílico: una abertura de desbordamiento estilo ranura"))
						.put(Lang.DEU, getWords("Einzigartig in Acrylbadewannen - eine Schlitzüberlauföffnung"))
						.put(Lang.FRA, getWords("Unique aux baignoires en acrylique - une ouverture de débordement de style fente")).build());
		translates
				.put(getCode("Large 13.4 (340 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 13,4 (340 мм) душевая насадка"))
						.put(Lang.ITA, getWords("Grande soffione da 13,4 (340 mm)"))
						.put(Lang.ESP, getWords("Cabeza de ducha grande de 13.4 (340 mm)"))
						.put(Lang.DEU, getWords("Großer 13,4 (340 mm) Duschkopf"))
						.put(Lang.FRA, getWords("Grande pomme de douche de 13,4 po (340 mm)")).build());
		translates
				.put(getCode("Matte light beige AquateX™ Ecomalta top with integrated twin basin – 1930 x 505 x 12 mm / 76 x 20 x 0.5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовый светло-бежевый AquateX ™ Ecomalta с интегрированным двойным бассейном - 1930 х 505 х 12 мм / 76 х 20 х 0,5 дюйма"))
						.put(Lang.ITA, getWords("Top Ecomalta opaco beige chiaro opaco con vasca doppia integrata - 1930 x 505 x 12 mm / 76 x 20 x 0,5 in"))
						.put(Lang.ESP, getWords("Top mate AquateX ™ Ecomalta beige mate con lavabo doble integrado - 1930 x 505 x 12 mm / 76 x 20 x 0.5 in"))
						.put(Lang.DEU, getWords("Matte hellbeige AquateX ™ Ecomalta Top mit integriertem Doppelbecken - 1930 x 505 x 12 mm / 76 x 20 x 0,5 Zoll"))
						.put(Lang.FRA, getWords("Dessus AquateX ™ Ecomalta beige mat avec double vasque intégrée - 1930 x 505 x 12 mm / 76 x 20 x 0,5 po")).build());
		translates
				.put(getCode("Round top-mounted shower arm to match your shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Круглый верхний душ для душа, соответствующий вашей душевой головке"))
						.put(Lang.ITA, getWords("Braccio doccia tondo montato in alto per abbinare il tuo soffione"))
						.put(Lang.ESP, getWords("Brazo de ducha redondo montado en la parte superior para combinar con su cabezal de ducha"))
						.put(Lang.DEU, getWords("Runder Aufsatz-Duscharm passend zu Ihrem Duschkopf"))
						.put(Lang.FRA, getWords("Bras de douche rond monté sur le dessus pour s'adapter à votre pomme de douche")).build());
		translates
				.put(getCode("Honey Sherwood veneer console – 1805 x 505 x 130 mm / 71 x 20 x 5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Консоль шпона Honey Sherwood - 1805 x 505 x 130 мм / 71 x 20 x 5 дюймов"))
						.put(Lang.ITA, getWords("Consolle impiallacciata Honey Sherwood - 1805 x 505 x 130 mm / 71 x 20 x 5 in"))
						.put(Lang.ESP, getWords("Consola de chapa Honey Sherwood - 1805 x 505 x 130 mm / 71 x 20 x 5 in"))
						.put(Lang.DEU, getWords("Honig Sherwood Furnier Konsole - 1805 x 505 x 130 mm / 71 x 20 x 5 Zoll"))
						.put(Lang.FRA, getWords("Console de placage Honey Sherwood - 1805 x 505 x 130 mm / 71 x 20 x 5 po")).build());
		translates
				.put(getCode("Contemporary, symmetrical freestanding bathtub with rimless, deep and double-ended design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современная симметричная автономная ванна с безрисковой, глубокой и двухсторонней конструкцией"))
						.put(Lang.ITA, getWords("Vasca da bagno moderna, simmetrica, con design senza bordo, profondo e doppio"))
						.put(Lang.ESP, getWords("Bañera exenta simétrica, contemporánea con diseño sin borde, profundo y de doble punta"))
						.put(Lang.DEU, getWords("Zeitgenössische, symmetrische freistehende Badewanne mit randlosem, tiefem und doppelendigem Design"))
						.put(Lang.FRA, getWords("Baignoire autoportante symétrique contemporaine, sans contour, profonde et à double extrémité")).build());
		translates
				.put(getCode("Underwater chromotherapy lighting with various lighting shows"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Освещение подводной хромотерапии с различными световыми шоу"))
						.put(Lang.ITA, getWords("Illuminazione subacquea di cromoterapia con vari programmi di illuminazione"))
						.put(Lang.ESP, getWords("Iluminación de cromoterapia subacuática con varios espectáculos de iluminación"))
						.put(Lang.DEU, getWords("Unterwasser-Farbtherapie-Beleuchtung mit verschiedenen Lichtshows"))
						.put(Lang.FRA, getWords("Éclairage de chromothérapie sous-marine avec divers spectacles d'éclairage")).build());
		translates
				.put(getCode("Integrated emptying water system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Комплексная система опорожнения воды"))
						.put(Lang.ITA, getWords("Sistema integrato di svuotamento dell'acqua"))
						.put(Lang.ESP, getWords("Sistema de agua de vaciado integrado"))
						.put(Lang.DEU, getWords("Integriertes Entleerungswassersystem"))
						.put(Lang.FRA, getWords("Système de vidange intégré")).build());
		translates
				.put(getCode("Offers both rainfall and waterfall water flow at your fingertips"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предлагает как ливневые воды, так и воду водопада под рукой"))
						.put(Lang.ITA, getWords("Offre sia la pioggia che la cascata d'acqua a portata di mano"))
						.put(Lang.ESP, getWords("Ofrece agua de lluvia y flujo de agua en cascada al alcance de su mano"))
						.put(Lang.DEU, getWords("Bietet sowohl Niederschlag als auch Wasserfall Wasserfluss an Ihren Fingerspitzen"))
						.put(Lang.FRA, getWords("Offre à la fois la pluie et le débit d'eau en cascade à portée de main")).build());
		translates
				.put(getCode("Air-assist hydromassage system for more pleasurable massage effect"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Воздушная гидромассажная система для более приятного массажного эффекта"))
						.put(Lang.ITA, getWords("Sistema di idromassaggio ad aria assistita per un massaggio più piacevole"))
						.put(Lang.ESP, getWords("Sistema de hidromasaje con asistencia de aire para un efecto de masaje más placentero"))
						.put(Lang.DEU, getWords("Air-Assist-Hydromassagesystem für einen angenehmeren Massageeffekt"))
						.put(Lang.FRA, getWords("Système d'hydromassage à assistance pneumatique pour un effet de massage plus agréable")).build());
		translates
				.put(getCode("Maximum water capacity - 620 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 620 литров"))
						.put(Lang.ITA, getWords("Capacità massima d'acqua - 620 litri"))
						.put(Lang.ESP, getWords("Capacidad máxima de agua - 620 Litros"))
						.put(Lang.DEU, getWords("Maximale Wasserkapazität - 620 Liter"))
						.put(Lang.FRA, getWords("Capacité maximale en eau - 620 Litres")).build());
		translates
				.put(getCode("This item set includes a concealed body (CS147) with an option of 3 outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор элементов включает скрытый корпус (CS147) с возможностью 3 выходов"))
						.put(Lang.ITA, getWords("Questo set di articoli include un corpo nascosto (CS147) con un'opzione di 3 prese"))
						.put(Lang.ESP, getWords("Este conjunto de elementos incluye un cuerpo oculto (CS147) con una opción de 3 tomacorrientes"))
						.put(Lang.DEU, getWords("Dieses Set enthält einen versteckten Körper (CS147) mit einer Option von 3 Steckdosen"))
						.put(Lang.FRA, getWords("Cet ensemble d'articles inclut un corps caché (CS147) avec une option de 3 sorties")).build());
		translates
				.put(getCode("Freestanding white ceramic sink – 650 x 460 x 140 mm / 25.5 x 18 x 5.5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Свободная белая керамическая раковина - 650 x 460 x 140 мм / 25,5 x 18 x 5,5 дюймов"))
						.put(Lang.ITA, getWords("Lavabo freestanding in ceramica bianca - 650 x 460 x 140 mm / 25,5 x 18 x 5,5 pollici"))
						.put(Lang.ESP, getWords("Fregadero de cerámica blanco independiente - 650 x 460 x 140 mm / 25.5 x 18 x 5.5 in"))
						.put(Lang.DEU, getWords("Freistehende weiße Keramikspüle - 650 x 460 x 140 mm / 25,5 x 18 x 5,5 in"))
						.put(Lang.FRA, getWords("Évier en céramique blanc autoportant - 650 x 460 x 140 mm / 25.5 x 18 x 5.5 po")).build());
		translates
				.put(getCode("Designed for comfortable one-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного однолюстного купания"))
						.put(Lang.ITA, getWords("Progettato per una balneazione individuale da una sola persona"))
						.put(Lang.ESP, getWords("Diseñado para un baño cómodo para una persona"))
						.put(Lang.DEU, getWords("Entworfen für komfortables Ein-Personen-Baden"))
						.put(Lang.FRA, getWords("Conçu pour un bain confortable pour une personne")).build());
		translates
				.put(getCode("Monolith ceramic cistern"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Монолитная керамическая цистерна"))
						.put(Lang.ITA, getWords("Cisterna in ceramica monolitica"))
						.put(Lang.ESP, getWords("Cisterna de cerámica Monolith"))
						.put(Lang.DEU, getWords("Monolith-Keramikzisterne"))
						.put(Lang.FRA, getWords("Réservoir en céramique Monolith")).build());
		translates
				.put(getCode("No screws to handle, install, or lose"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Нет винтов для управления, установки или потери"))
						.put(Lang.ITA, getWords("Nessuna vite per maneggiare, installare o perdere"))
						.put(Lang.ESP, getWords("Sin tornillos para manejar, instalar o perder"))
						.put(Lang.DEU, getWords("Keine Schrauben zu handhaben, zu installieren oder zu verlieren"))
						.put(Lang.FRA, getWords("Aucune vis à manipuler, installer ou perdre")).build());
		translates
				.put(getCode("Recommended operational pressure: 1-5 bar"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемое рабочее давление: 1-5 бар"))
						.put(Lang.ITA, getWords("Pressione operativa raccomandata: 1-5 bar"))
						.put(Lang.ESP, getWords("Presión operacional recomendada: 1-5 bar"))
						.put(Lang.DEU, getWords("Empfohlener Betriebsdruck: 1-5 bar"))
						.put(Lang.FRA, getWords("Pression de fonctionnement recommandée: 1-5 bar")).build());
		translates
				.put(getCode("Dark brown Mokka with either Iroko wood or titanium travertine"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Темно-коричневый Мокка с ироковым деревом или титановым травертином"))
						.put(Lang.ITA, getWords("Mokka marrone scuro con legno di Iroko o travertino di titanio"))
						.put(Lang.ESP, getWords("Mokka marrón oscuro con madera de Iroko o travertino de titanio"))
						.put(Lang.DEU, getWords("Dunkelbrauner Mokka mit Iroko-Holz oder Titan-Travertin"))
						.put(Lang.FRA, getWords("Mokka brun foncé avec du bois d'Iroko ou du travertin de titane")).build());
		translates
				.put(getCode("Painted grey aluminum profiles"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Окрашенные серые алюминиевые профили"))
						.put(Lang.ITA, getWords("Profili in alluminio verniciato grigio"))
						.put(Lang.ESP, getWords("Perfiles pintados de aluminio gris"))
						.put(Lang.DEU, getWords("Graue Aluminiumprofile lackiert"))
						.put(Lang.FRA, getWords("Profils en aluminium gris peints")).build());
		translates
				.put(getCode("Freestanding white ceramic sink - 550 x 400 x 150 mm / 21.75 x 15.75 x 6 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Свободная белая керамическая раковина - 550 x 400 x 150 мм / 21,75 x 15,75 x 6 дюймов"))
						.put(Lang.ITA, getWords("Lavabo bianco in ceramica da appoggio - 550 x 400 x 150 mm / 21,75 x 15,75 x 6 pollici"))
						.put(Lang.ESP, getWords("Fregadero de cerámica blanco independiente - 550 x 400 x 150 mm / 21.75 x 15.75 x 6 in"))
						.put(Lang.DEU, getWords("Freistehende weiße Keramikspüle - 550 x 400 x 150 mm / 21,75 x 15,75 x 6 in"))
						.put(Lang.FRA, getWords("Évier en céramique blanc autoportant - 550 x 400 x 150 mm / 21.75 x 15.75 x 6 po")).build());
		translates
				.put(getCode("Remote control to choose several colour programs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удаленное управление для выбора нескольких цветовых программ"))
						.put(Lang.ITA, getWords("Telecomando per scegliere diversi programmi di colore"))
						.put(Lang.ESP, getWords("Control remoto para elegir varios programas de color"))
						.put(Lang.DEU, getWords("Fernbedienung zur Auswahl mehrerer Farbprogramme"))
						.put(Lang.FRA, getWords("Contrôle à distance pour choisir plusieurs programmes de couleur")).build());
		translates
				.put(getCode("Minimalistic capacitive glass keypad"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минималистичная емкостная стеклянная клавиатура"))
						.put(Lang.ITA, getWords("Tastiera capacitiva in vetro minimalista"))
						.put(Lang.ESP, getWords("Teclado de cristal capacitivo minimalista"))
						.put(Lang.DEU, getWords("Minimalistische kapazitive Glastastatur"))
						.put(Lang.FRA, getWords("Clavier en verre capacitif minimaliste")).build());
		translates
				.put(getCode("Stainless steel multi-function hand shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Многофункциональный ручной душ из нержавеющей стали"))
						.put(Lang.ITA, getWords("Doccetta multifunzione in acciaio inossidabile"))
						.put(Lang.ESP, getWords("Ducha de mano multifunción de acero inoxidable"))
						.put(Lang.DEU, getWords("Edelstahl-Multifunktions-Handbrause"))
						.put(Lang.FRA, getWords("Douchette à main multifonction en acier inoxydable")).build());
		translates
				.put(getCode("Discreet and  simple clean lines"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дискретные и простые чистые линии"))
						.put(Lang.ITA, getWords("Linee pulite discrete e semplici"))
						.put(Lang.ESP, getWords("Líneas limpias discretas y simples"))
						.put(Lang.DEU, getWords("Dezente und einfache klare Linien"))
						.put(Lang.FRA, getWords("Lignes propres discrètes et simples")).build());
		translates
				.put(getCode("Soft white LED - without electrical power supply"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мягкий белый светодиод - без электропитания"))
						.put(Lang.ITA, getWords("LED bianco soft - senza alimentazione elettrica"))
						.put(Lang.ESP, getWords("LED blanco suave - sin suministro de energía eléctrica"))
						.put(Lang.DEU, getWords("Weiche weiße LED - ohne Stromversorgung"))
						.put(Lang.FRA, getWords("LED blanche douce - sans alimentation électrique")).build());
		translates
				.put(getCode("False ceiling mounted at the recommended height of 15.75 (40 cm) above the user’s height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подвесной потолок, установленный на рекомендуемой высоте 15,75 дюйма (40 см) выше высоты пользователя"))
						.put(Lang.ITA, getWords("Falso soffitto montato all'altezza raccomandata di 15.75 (40 cm) sopra l'altezza dell'utente"))
						.put(Lang.ESP, getWords("Falso techo montado a la altura recomendada de 15.75 (40 cm) sobre la altura del usuario"))
						.put(Lang.DEU, getWords("Zwischendecke in der empfohlenen Höhe von 40 cm über der Benutzerhöhe montiert"))
						.put(Lang.FRA, getWords("Faux plafond monté à la hauteur recommandée de 15,75 (40 cm) au-dessus de la hauteur de l'utilisateur")).build());
		translates
				.put(getCode("Graphite black matte finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Графитовая черная матовая отделка"))
						.put(Lang.ITA, getWords("Finitura nera opaca grafite"))
						.put(Lang.ESP, getWords("Acabado mate negro grafito"))
						.put(Lang.DEU, getWords("Graphitschwarz mattes Finish"))
						.put(Lang.FRA, getWords("Finition noire graphite")).build());
		translates
				.put(getCode("Three large drawers with clip handle and soft closing slides"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Три больших ящика с ручкой для клипа и мягкими закрывающимися слайдами"))
						.put(Lang.ITA, getWords("Tre grandi cassetti con maniglia a clip e scorrevoli morbidi"))
						.put(Lang.ESP, getWords("Tres cajones grandes con mango de clip y diapositivas de cierre suave"))
						.put(Lang.DEU, getWords("Drei große Schubladen mit Clip-Griff und sanft schließenden Folien"))
						.put(Lang.FRA, getWords("Trois grands tiroirs avec poignée à pince et glissières de fermeture souples")).build());
		translates
				.put(getCode("This item set includes a thermostatic concealed body (CS 800) with an option of 1 or 2 or 3 outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор предметов включает термостатический скрытый корпус (CS 800) с возможностью 1 или 2 или 3 розетки"))
						.put(Lang.ITA, getWords("Questo set di articoli include un corpo termostatico nascosto (CS 800) con un'opzione di 1 o 2 o 3 prese"))
						.put(Lang.ESP, getWords("Este conjunto de elementos incluye un cuerpo termostático oculto (CS 800) con una opción de 1 o 2 o 3 enchufes"))
						.put(Lang.DEU, getWords("Dieses Set beinhaltet einen Thermostat UP-Körper (CS 800) mit einer Option von 1 oder 2 oder 3 Ausgängen"))
						.put(Lang.FRA, getWords("Cet ensemble d'articles inclut un corps caché thermostatique (CS 800) avec une option de 1 ou 2 ou 3 sorties")).build());
		translates
				.put(getCode("Wall mounted sink faucet with 11/4 Up&Down waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для раковины с 1 1/4 Up & Down waste"))
						.put(Lang.ITA, getWords("Rubinetto lavello a parete con scarico Up & Down da 1 1/4"))
						.put(Lang.ESP, getWords("Grifo de fregadero montado en la pared con 1 1/4 de desechos Arriba y Abajo"))
						.put(Lang.DEU, getWords("Spüle Wasserhahn mit 1 1/4 Up & Down Abfall"))
						.put(Lang.FRA, getWords("Robinet d'évier mural avec 1 1/4 Up & Down waste")).build());
		translates
				.put(getCode("Available with optional air massage system and in high gloss finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с дополнительной системой воздушного массажа и с высокой степенью блеска"))
						.put(Lang.ITA, getWords("Disponibile con sistema opzionale di massaggio ad aria e in finitura lucida"))
						.put(Lang.ESP, getWords("Disponible con sistema de masaje de aire opcional y en acabado de alto brillo"))
						.put(Lang.DEU, getWords("Erhältlich mit optionalem Luftmassagesystem und in hochglänzender Ausführung"))
						.put(Lang.FRA, getWords("Disponible avec système de massage à air en option et finition haute brillance")).build());
		translates
				.put(getCode("Comfortably fits two bathers that are 6ft or taller."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобно подходит для двух купальщиков размером 6 футов или выше."))
						.put(Lang.ITA, getWords("Comodamente si adatta a due bagnanti che sono 6 piedi o più alti."))
						.put(Lang.ESP, getWords("Se adapta cómodamente a dos bañistas que miden 6 pies o más."))
						.put(Lang.DEU, getWords("Bequem passt zwei Badegäste, die 6ft oder größer sind."))
						.put(Lang.FRA, getWords("Convient confortablement à deux baigneurs de 6 pieds ou plus.")).build());
		translates
				.put(getCode("3-hole wall-mounted shower mixer with 2 outlet diverter"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 3 отверстиями с 2 выходными дивертерами"))
						.put(Lang.ITA, getWords("Miscelatore doccia a parete a 3 fori con deviatore 2 uscite"))
						.put(Lang.ESP, getWords("Mezclador de ducha de pared con 3 orificios con 2 desviadores de salida"))
						.put(Lang.DEU, getWords("3-Loch Wand-Brause-Mischbatterie mit 2 Abluftumleitern"))
						.put(Lang.FRA, getWords("Mitigeur de douche mural à 3 trous avec 2 inverseurs")).build());
		translates
				.put(getCode("Durable and practically functional"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочный и практически функциональный"))
						.put(Lang.ITA, getWords("Durevole e praticamente funzionale"))
						.put(Lang.ESP, getWords("Durable y funcional"))
						.put(Lang.DEU, getWords("Haltbar und praktisch funktional"))
						.put(Lang.FRA, getWords("Durable et pratiquement fonctionnel")).build());
		translates
				.put(getCode("Multiplex E Duo Bath Filler, Diverter & Motorized Drain can be purchased upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Наполнитель Multiplex E Duo, Diverter & Motorized Drain можно приобрести по запросу"))
						.put(Lang.ITA, getWords("Su richiesta è possibile acquistare il doccetta e scarico motorizzato Multiplex E Duo"))
						.put(Lang.ESP, getWords("Llenadora de baño, desviador y drenaje motorizado Multiplex E Duo se pueden adquirir a pedido"))
						.put(Lang.DEU, getWords("Multiplex E Duo Bath Füller, Umsteller & motorisierter Abfluss kann auf Anfrage erworben werden"))
						.put(Lang.FRA, getWords("Le remplisseur de Bath de Multiplex E Duo, le Diverter et le drain motorisé peuvent être achetés sur demande")).build());
		translates
				.put(getCode("Arm length 19.75 (50 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукоятки 19,75 (50 см)"))
						.put(Lang.ITA, getWords("Braccio lunghezza 19,75 (50 cm)"))
						.put(Lang.ESP, getWords("Longitud del brazo 19.75 (50 cm)"))
						.put(Lang.DEU, getWords("Armlänge 19,75 (50 cm)"))
						.put(Lang.FRA, getWords("Longueur de bras 19.75 (50 cm)")).build());
		translates
				.put(getCode("2 Year Limited Warranty on the remote controller and the buttons"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 2 года на пульте дистанционного управления и кнопках"))
						.put(Lang.ITA, getWords("Garanzia limitata di 2 anni sul telecomando e sui pulsanti"))
						.put(Lang.ESP, getWords("Garantía limitada de 2 años en el control remoto y los botones"))
						.put(Lang.DEU, getWords("2 Jahre eingeschränkte Garantie auf die Fernbedienung und die Tasten"))
						.put(Lang.FRA, getWords("Garantie limitée de 2 ans sur la télécommande et les boutons")).build());
		translates
				.put(getCode("2 years warranty on components"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя гарантия на компоненты"))
						.put(Lang.ITA, getWords("2 anni di garanzia sui componenti"))
						.put(Lang.ESP, getWords("2 años de garantía en componentes"))
						.put(Lang.DEU, getWords("2 Jahre Garantie auf Komponenten"))
						.put(Lang.FRA, getWords("2 ans de garantie sur les composants")).build());
		translates
				.put(getCode("Matte white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовая белая поверхность"))
						.put(Lang.ITA, getWords("Superficie bianca opaca"))
						.put(Lang.ESP, getWords("Superficie blanca mate"))
						.put(Lang.DEU, getWords("Matte weiße Oberfläche"))
						.put(Lang.FRA, getWords("Surface blanche mate")).build());
		translates
				.put(getCode("Max flow rate of 3.9 GPM (15 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,9 GPM (15 LPM)"))
						.put(Lang.ITA, getWords("Portata massima di 3,9 GPM (15 LPM)"))
						.put(Lang.ESP, getWords("Caudal máximo de 3.9 GPM (15 LPM)"))
						.put(Lang.DEU, getWords("Max Durchflussrate von 3,9 GPM (15 LPM)"))
						.put(Lang.FRA, getWords("Débit maximum de 3,9 GPM (15 LPM)")).build());
		translates
				.put(getCode("Drawers with soft closing slides"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ящики с мягкими закрывающимися слайдами"))
						.put(Lang.ITA, getWords("Cassetti con guide a chiusura morbida"))
						.put(Lang.ESP, getWords("Cajones con diapositivas de cierre suave"))
						.put(Lang.DEU, getWords("Schubladen mit sanft schließenden Objektträgern"))
						.put(Lang.FRA, getWords("Tiroirs avec glissières de fermeture douce")).build());
		translates
				.put(getCode("Comes in subtle woven mélange grey or white beige cushions with mokka frame"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поставляется в тонких тканых меланжевых серых или белых бежевых подушках с рамкой mokka"))
						.put(Lang.ITA, getWords("Disponibile in sottili tessuti mélange grigi o bianchi cuscini beige con telaio mokka"))
						.put(Lang.ESP, getWords("Viene en sutiles cojines mélange grisáceo o beige blanco con marco mokka"))
						.put(Lang.DEU, getWords("Kommt in dezent gewebten Mélange grau oder weiß beige Kissen mit Mokkatrahmen"))
						.put(Lang.FRA, getWords("Livré dans de subtils coussins tissés gris ou blanc beige avec cadre mokka")).build());
		translates
				.put(getCode("Deodorizer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дезодорант"))
						.put(Lang.ITA, getWords("deodorante"))
						.put(Lang.ESP, getWords("Desodorizante"))
						.put(Lang.DEU, getWords("Deodorant"))
						.put(Lang.FRA, getWords("Désodorisant")).build());
		translates
				.put(getCode("Plug & play installation with NEMA plug for the US market"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Установка Plug & Play с разъемом NEMA для рынка США"))
						.put(Lang.ITA, getWords("Installazione plug & play con spina NEMA per il mercato statunitense"))
						.put(Lang.ESP, getWords("Instalación Plug & Play con enchufe NEMA para el mercado estadounidense"))
						.put(Lang.DEU, getWords("Plug & Play-Installation mit NEMA-Stecker für den US-Markt"))
						.put(Lang.FRA, getWords("Installation Plug & Play avec prise NEMA pour le marché américain")).build());
		translates
				.put(getCode("A Depending on the building type and cable conduit condition 2.5 mm2 (most installations) or 4 mm2 electrical cable is required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("A В зависимости от типа здания и состояния кабельного канала требуется 2,5 мм2 (большинство установок) или электрический кабель 4 мм2"))
						.put(Lang.ITA, getWords("A In base al tipo di edificio e alle condizioni dei conduit dei cavi, sono richiesti 2,5 mm2 (la maggior parte delle installazioni) o un cavo elettrico da 4 mm2"))
						.put(Lang.ESP, getWords("A Dependiendo del tipo de construcción y de la condición del conducto del cable, se requiere 2.5 mm2 (la mayoría de las instalaciones) o cable eléctrico de 4 mm2"))
						.put(Lang.DEU, getWords("A Je nach Gebäudeart und Kabelkanalzustand sind 2,5 mm2 (die meisten Installationen) oder 4 mm2 Kabel erforderlich"))
						.put(Lang.FRA, getWords("A En fonction du type de bâtiment et de l'état du conduit de câbles, 2,5 mm2 (la plupart des installations) ou 4 mm2 de câble électrique sont requis")).build());
		translates
				.put(getCode("Wall-mounted Slavonian oak laminate base unit in 1400 x 500 x 500 mm / 55 x 19.75 x 19.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный базовый блок из слоистого лаванского дуба в 1400 х 500 х 500 мм / 55 х 19,75 х 19,75 дюймов"))
						.put(Lang.ITA, getWords("Base in laminato rovere Slavonia montata a parete in 1400 x 500 x 500 mm / 55 x 19,75 x 19,75 in"))
						.put(Lang.ESP, getWords("Unidad de base de laminado de roble eslavo de pared en 1400 x 500 x 500 mm / 55 x 19,75 x 19,75 en"))
						.put(Lang.DEU, getWords("An der Wand befestigte slavonische Eiche Laminat Basiseinheit in 1400 x 500 x 500 mm / 55 x 19,75 x 19,75 in"))
						.put(Lang.FRA, getWords("Unité de base stratifiée en chêne de Slavonian fixée au mur en 1400 x 500 x 500 mm / 55 x 19,75 x 19,75 po")).build());
		translates
				.put(getCode("Suited to the Aquatica Anette Corner Tub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходит для угольной ванны Aquatica Anette"))
						.put(Lang.ITA, getWords("Adatto alla vasca angolare Aquatica Anette"))
						.put(Lang.ESP, getWords("Adecuado para Aquatica Anette Corner Tub"))
						.put(Lang.DEU, getWords("Passend zur Aquatica Anette Eckwanne"))
						.put(Lang.FRA, getWords("Adapté à la baignoire d'angle Aquatica Anette")).build());
		translates
				.put(getCode("All-Weather Sunbrella Acrylic fabric"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("All-Weather Sunbrella Акриловая ткань"))
						.put(Lang.ITA, getWords("Tessuto acrilico Sunbrella per tutte le stagioni"))
						.put(Lang.ESP, getWords("Tela acrílica Sunbrella para todo clima"))
						.put(Lang.DEU, getWords("Allwetter Sunbrella Acrylgewebe"))
						.put(Lang.FRA, getWords("Tissu acrylique Sunbrella toutes saisons")).build());
		translates
				.put(getCode("French cleat mounting system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Французская система крепления"))
						.put(Lang.ITA, getWords("Sistema di montaggio della tacchetta francese"))
						.put(Lang.ESP, getWords("Sistema de montaje de cala francesa"))
						.put(Lang.DEU, getWords("Französisches Befestigungssystem für Klampen"))
						.put(Lang.FRA, getWords("Système de montage de taquet français")).build());
		translates
				.put(getCode("Storage/library styled bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хранение / библиотечная ванна"))
						.put(Lang.ITA, getWords("Vasca da bagno portaoggetti / libreria"))
						.put(Lang.ESP, getWords("Bañera de almacenamiento / biblioteca"))
						.put(Lang.DEU, getWords("Badewanne im Stil einer Bibliothek"))
						.put(Lang.FRA, getWords("Rangement / bibliothèque de style baignoire")).build());
		translates
				.put(getCode("Minimum of 3 bar water pressure recommended for best performance of the shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальное давление воды 3 бар рекомендуется для наилучшей работы душевой головки"))
						.put(Lang.ITA, getWords("Minimo 3 bar di pressione dell'acqua consigliato per le migliori prestazioni del soffione"))
						.put(Lang.ESP, getWords("Se recomienda un mínimo de 3 bares de presión de agua para un mejor rendimiento del cabezal de la ducha"))
						.put(Lang.DEU, getWords("Mindestens 3 bar Wasserdruck empfohlen für die beste Leistung des Duschkopfes"))
						.put(Lang.FRA, getWords("Pression d'eau minimale de 3 bar recommandée pour une meilleure performance de la pomme de douche")).build());
		translates
				.put(getCode("Make any maintenance job, requiring the removal of the tub, quick and straight forward"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Выполняйте любое техническое обслуживание, требующее удаления ванны, быстро и прямо"))
						.put(Lang.ITA, getWords("Effettuare qualsiasi intervento di manutenzione, richiedendo la rimozione della vasca, in modo rapido e diretto"))
						.put(Lang.ESP, getWords("Haga cualquier trabajo de mantenimiento, requiriendo la remoción de la bañera, rápido y directo"))
						.put(Lang.DEU, getWords("Nehmen Sie Wartungsarbeiten vor, die das Entfernen der Wanne erfordern, schnell und unkompliziert"))
						.put(Lang.FRA, getWords("Effectuer tout travail de maintenance, nécessitant le retrait de la cuve, rapide et simple")).build());
		translates
				.put(getCode("Simple to operate and maintain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Простота в эксплуатации и обслуживании"))
						.put(Lang.ITA, getWords("Semplice da usare e mantenere"))
						.put(Lang.ESP, getWords("Simple de operar y mantener"))
						.put(Lang.DEU, getWords("Einfach zu bedienen und zu warten"))
						.put(Lang.FRA, getWords("Simple à utiliser et à entretenir")).build());
		translates
				.put(getCode("Made in Italy of genuine European materials"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано в Италии подлинных европейских материалов"))
						.put(Lang.ITA, getWords("Made in Italy di autentici materiali europei"))
						.put(Lang.ESP, getWords("Hecho en Italia de materiales europeos genuinos"))
						.put(Lang.DEU, getWords("Hergestellt in Italien aus echten europäischen Materialien"))
						.put(Lang.FRA, getWords("Fabriqué en Italie de véritables matériaux européens")).build());
		translates
				.put(getCode("Includes waste fitting, overflow fitting, trim, cable, and flexible PVC drain pipe"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Включает отстойник, переливной фитинг, отделку, кабель и гибкую трубку для слива ПВХ"))
						.put(Lang.ITA, getWords("Comprende raccordi di scarico, raccordo di troppopieno, rivestimento, cavo e tubo di scarico flessibile in PVC"))
						.put(Lang.ESP, getWords("Incluye accesorio de desagüe, accesorio de desbordamiento, moldura, cable y tubería flexible de drenaje de PVC"))
						.put(Lang.DEU, getWords("Inklusive Ablaufgarnitur, Überlaufgarnitur, Garnitur, Kabel und flexiblem PVC-Ablaufrohr"))
						.put(Lang.FRA, getWords("Comprend un raccord de vidange, un raccord de trop-plein, une garniture, un câble et un tuyau d'évacuation en PVC flexible")).build());
		translates
				.put(getCode("28 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("28 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords("28 LED a basso profilo"))
						.put(Lang.ESP, getWords("28 LED de bajo perfil"))
						.put(Lang.DEU, getWords("28 flache LEDs"))
						.put(Lang.FRA, getWords("28 LEDs à profil bas")).build());
		translates
				.put(getCode("Finish applies to trim only"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Финиш применяется только к обрезке"))
						.put(Lang.ITA, getWords("La fine si applica solo al trim"))
						.put(Lang.ESP, getWords("El acabado solo se aplica al corte"))
						.put(Lang.DEU, getWords("Finish gilt nur für die Trimmung"))
						.put(Lang.FRA, getWords("La finition s'applique uniquement à l'assiette")).build());
		translates
				.put(getCode("Matching Handshower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходящий ручной душ"))
						.put(Lang.ITA, getWords("Doccetta coordinata"))
						.put(Lang.ESP, getWords("Ducha de mano a juego"))
						.put(Lang.DEU, getWords("Passende Handbrause"))
						.put(Lang.FRA, getWords("Douchette assortie")).build());
		translates
				.put(getCode("Anti-lime shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Антивоздушная душевая головка"))
						.put(Lang.ITA, getWords("Soffione anticalcare"))
						.put(Lang.ESP, getWords("Cabezal de ducha antical"))
						.put(Lang.DEU, getWords("Anti-Kalk-Duschkopf"))
						.put(Lang.FRA, getWords("Pomme de douche anti-calcaire")).build());
		translates
				.put(getCode("Designed for concealed installations"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для скрытых установок"))
						.put(Lang.ITA, getWords("Progettato per installazioni nascoste"))
						.put(Lang.ESP, getWords("Diseñado para instalaciones ocultas"))
						.put(Lang.DEU, getWords("Entworfen für verborgene Installationen"))
						.put(Lang.FRA, getWords("Conçu pour les installations cachées")).build());
		translates
				.put(getCode("Stainless steel support frame included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Опорная рама из нержавеющей стали"))
						.put(Lang.ITA, getWords("Telaio di supporto in acciaio inossidabile incluso"))
						.put(Lang.ESP, getWords("Marco de soporte de acero inoxidable incluido"))
						.put(Lang.DEU, getWords("Edelstahlstützrahmen enthalten"))
						.put(Lang.FRA, getWords("Cadre de support en acier inoxydable inclus")).build());
		translates
				.put(getCode("Feminine cleansing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Женское очищение"))
						.put(Lang.ITA, getWords("Pulizia femminile"))
						.put(Lang.ESP, getWords("Limpieza femenina"))
						.put(Lang.DEU, getWords("Weibliche Reinigung"))
						.put(Lang.FRA, getWords("Nettoyage féminin")).build());
		translates
				.put(getCode("This item set includes a concealed body (CS435) with an option of 1 or 2 outlets 1/2”"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор предметов включает скрытый корпус (CS435) с возможностью 1 или 2 розетки 1/2 "))
						.put(Lang.ITA, getWords("Questo set di articoli include un corpo nascosto (CS435) con un'opzione di 1 o 2 uscite 1/2 "))
						.put(Lang.ESP, getWords("Este conjunto de elementos incluye un cuerpo oculto (CS435) con una opción de 1 o 2 salidas de 1/2 "))
						.put(Lang.DEU, getWords("Dieses Set enthält einen versteckten Körper (CS435) mit einer Option von 1 oder 2 Steckdosen 1/2 "))
						.put(Lang.FRA, getWords("Cet ensemble d'articles comprend un corps dissimulé (CS435) avec une option de 1 ou 2 sorties 1/2 ")).build());
		translates
				.put(getCode("Concealed hydromassage system – unmatched in the market"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Скрытая гидромассажная система - непревзойденная на рынке"))
						.put(Lang.ITA, getWords("Sistema di idromassaggio nascosto - ineguagliabile sul mercato"))
						.put(Lang.ESP, getWords("Sistema de hidromasaje oculto: inigualable en el mercado"))
						.put(Lang.DEU, getWords("Verdecktes Hydromassagesystem - unübertroffen auf dem Markt"))
						.put(Lang.FRA, getWords("Système d'hydromassage dissimulé - inégalé sur le marché")).build());
		translates
				.put(getCode("23.6 (600 mm) round built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("23,6 (600 мм) круглый встроенный душевой уголок"))
						.put(Lang.ITA, getWords("Soffione da incasso rotondo da 23,6 (600 mm)"))
						.put(Lang.ESP, getWords("Cabeza de ducha incorporada redonda de 23.6 (600 mm)"))
						.put(Lang.DEU, getWords("Runder, integrierter Brausekopf (600 mm)"))
						.put(Lang.FRA, getWords("Pomme de douche encastrée ronde de 23,6 po (600 mm)")).build());
		translates
				.put(getCode("Wave jet from 0 - 30° downward angle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Волновая струя от 0 до 30 ° вниз"))
						.put(Lang.ITA, getWords("Getto d'onda da 0 a 30 ° verso il basso"))
						.put(Lang.ESP, getWords("Chorro de olas desde 0 ° a 30 ° de ángulo hacia abajo"))
						.put(Lang.DEU, getWords("Wellenstrahl von 0 - 30 ° nach unten"))
						.put(Lang.FRA, getWords("Jet d'onde de 0 à 30 ° vers le bas")).build());
		translates
				.put(getCode("25 Year Limited Warranty on the bathtub and 3 year warranty on the wooden paneling"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гарантия 25 лет на ванну и 3-летняя гарантия на деревянную обшивку"))
						.put(Lang.ITA, getWords("Garanzia limitata di 25 anni sulla vasca da bagno e 3 anni di garanzia sui rivestimenti in legno"))
						.put(Lang.ESP, getWords("Garantía limitada de 25 años en la bañera y 3 años de garantía en los paneles de madera"))
						.put(Lang.DEU, getWords("25 Jahre eingeschränkte Garantie auf die Badewanne und 3 Jahre Garantie auf die Holzverkleidung"))
						.put(Lang.FRA, getWords("Garantie limitée de 25 ans sur la baignoire et garantie de 3 ans sur les boiseries")).build());
		translates
				.put(getCode("Includes 4 drawers and cupboard"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Включает 4 ящика и шкаф"))
						.put(Lang.ITA, getWords("Include 4 cassetti e armadio"))
						.put(Lang.ESP, getWords("Incluye 4 cajones y armario"))
						.put(Lang.DEU, getWords("Enthält 4 Schubladen und einen Schrank"))
						.put(Lang.FRA, getWords("Comprend 4 tiroirs et placard")).build());
		translates
				.put(getCode("Aesthetically pleasing and relaxing in use, the micro-LED ring is low power consumption"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эстетически приятное и расслабляющее использование, микро-светодиодное кольцо имеет низкое энергопотребление"))
						.put(Lang.ITA, getWords("Esteticamente piacevole e rilassante in uso, l'anello micro-LED è a basso consumo energetico"))
						.put(Lang.ESP, getWords("Estéticamente agradable y relajante en su uso, el anillo micro-LED tiene un bajo consumo de energía"))
						.put(Lang.DEU, getWords("Der Micro-LED-Ring ist ästhetisch ansprechend und entspannend im Gebrauch"))
						.put(Lang.FRA, getWords("Esthétiquement agréable et relaxant en cours d'utilisation, l'anneau micro-LED est faible consommation d'énergie")).build());
		translates
				.put(getCode("Complies with Nostalgia™ Series and Piccolo bathtubs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствует сериям Nostalgia ™ и ваннам Piccolo"))
						.put(Lang.ITA, getWords("Conforme alle vasche Nostalgia ™ Series e Piccolo"))
						.put(Lang.ESP, getWords("Cumple con la serie Nostalgia ™ y las bañeras Piccolo"))
						.put(Lang.DEU, getWords("Entspricht den Nostalgia ™ - und Piccolo-Badewannen"))
						.put(Lang.FRA, getWords("Conforme aux baignoires Nostalgia ™ et Piccolo")).build());
		translates
				.put(getCode("5 year limited warranty on the bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на ванну"))
						.put(Lang.ITA, getWords("Garanzia limitata di 5 anni sulla vasca"))
						.put(Lang.ESP, getWords("5 años de garantía limitada en la bañera"))
						.put(Lang.DEU, getWords("5 Jahre Garantie auf die Badewanne"))
						.put(Lang.FRA, getWords("Garantie limitée de 5 ans sur la baignoire")).build());
		translates
				.put(getCode("Built-in rectangular sink"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная прямоугольная раковина"))
						.put(Lang.ITA, getWords("Lavello rettangolare incorporato"))
						.put(Lang.ESP, getWords("Fregadero rectangular incorporado"))
						.put(Lang.DEU, getWords("Eingebaute rechteckige Spüle"))
						.put(Lang.FRA, getWords("Évier rectangulaire intégré")).build());
		translates
				.put(getCode("Soft chromotherapy lighting with various lighting shows"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мягкое освещение хромотерапии с различными освещенными шоу"))
						.put(Lang.ITA, getWords("Illuminazione soft di cromoterapia con vari programmi di illuminazione"))
						.put(Lang.ESP, getWords("Iluminación suave de cromoterapia con varios espectáculos de iluminación"))
						.put(Lang.DEU, getWords("Soft-Chromotherapie-Beleuchtung mit verschiedenen Lichtshows"))
						.put(Lang.FRA, getWords("Éclairage de chromothérapie douce avec divers spectacles d'éclairage")).build());
		translates
				.put(getCode("Natural oak console – 1831 x 505 x 60 mm / 72 x 20 x 2.25 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Естественная дубовая консоль - 1831 x 505 x 60 мм / 72 x 20 x 2,25 дюйма"))
						.put(Lang.ITA, getWords("Consolle in rovere naturale - 1831 x 505 x 60 mm / 72 x 20 x 2,25 pollici"))
						.put(Lang.ESP, getWords("Consola de roble natural - 1831 x 505 x 60 mm / 72 x 20 x 2.25 in"))
						.put(Lang.DEU, getWords("Natureiche Konsole - 1831 x 505 x 60 mm / 72 x 20 x 2,25 in"))
						.put(Lang.FRA, getWords("Console en chêne naturel - 1831 x 505 x 60 mm / 72 x 20 x 2,25 po")).build());
	}

	public static void part8() {
		translates
				.put(getCode("Available with optional curved tempered glass shower cabin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с опциональной изогнутой душевой кабиной с закаленным стеклом"))
						.put(Lang.ITA, getWords("Disponibile con box doccia curvo in vetro temperato opzionale"))
						.put(Lang.ESP, getWords("Disponible con cabina de ducha de vidrio templado curvo opcional"))
						.put(Lang.DEU, getWords("Erhältlich mit einer optionalen Duschkabine aus gehärtetem Glas"))
						.put(Lang.FRA, getWords("Disponible avec cabine de douche en verre trempé courbe en option")).build());
		translates
				.put(getCode("Color and material is homogeneous throughout – zero risk of delamination or discoloring"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет и материал однородны повсюду - нулевой риск расслоения или обесцвечивания"))
						.put(Lang.ITA, getWords("Colore e materiale sono omogenei - zero rischi di delaminazione o scolorimento"))
						.put(Lang.ESP, getWords("El color y el material son homogéneos en todas partes: cero riesgo de deslaminación o decoloración"))
						.put(Lang.DEU, getWords("Farbe und Material sind durchgehend homogen - kein Delaminations- oder Verfärbungsrisiko"))
						.put(Lang.FRA, getWords("La couleur et le matériau sont homogènes partout - aucun risque de délamination ou de décoloration")).build());
		translates
				.put(getCode("Large 19.75 square head, with 140 rainfall outlets and single waterfall jet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 19,75-дюймовая квадратная головка, с 140 выходами из ливня и одиночным водопадом"))
						.put(Lang.ITA, getWords("Grande testa quadrata 19,75 , con 140 prese di pioggia e getto a cascata singola"))
						.put(Lang.ESP, getWords("Gran cabeza cuadrada de 19.75 , con 140 salidas de lluvia y chorro de cascada individual"))
						.put(Lang.DEU, getWords("Großer quadratischer Kopf von 19,75 mit 140 Regenduschen und Einzelwasserfallstrahl"))
						.put(Lang.FRA, getWords("Grande tête carrée de 19,75 po avec 140 sorties de pluie et jet d'eau unique")).build());
		translates
				.put(getCode("Classical design with modern twist"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классический дизайн с современной твисткой"))
						.put(Lang.ITA, getWords("Design classico con tocco moderno"))
						.put(Lang.ESP, getWords("Diseño clásico con un toque moderno"))
						.put(Lang.DEU, getWords("Klassisches Design mit moderner Note"))
						.put(Lang.FRA, getWords("Design classique avec une touche moderne")).build());
		translates
				.put(getCode("Available with CE/TUV certified European/International 220V or UL certified USA/American 110V system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступна с сертифицированной CE / TUV европейской / международной системой 220V или UL с США / Американской системой 110V"))
						.put(Lang.ITA, getWords("Disponibile con certificazione CE / TUV europea / internazionale 220 V o UL USA / American 110 V sistema"))
						.put(Lang.ESP, getWords("Disponible con CE / TUV certificado europeo / internacional 220V o UL certificado sistema de Estados Unidos / América 110V"))
						.put(Lang.DEU, getWords("Verfügbar mit CE / TÜV-zertifiziertem europäischem / internationalem 220V oder UL-zertifiziertem USA / amerikanischem 110V System"))
						.put(Lang.FRA, getWords("Disponible avec CE / TUV certifié européen / international 220V ou UL certifié USA / système américain 110V")).build());
		translates
				.put(getCode("Durable AquateX™ advanced composite features pleasantly warm and silky-smooth surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочный AquateX ™ улучшенный композитный материал имеет приятную теплую и шелковисто-гладкую поверхность"))
						.put(Lang.ITA, getWords("Durevole AquateX ™ caratteristiche composite avanzate piacevolmente calde e superficie liscia come la seta"))
						.put(Lang.ESP, getWords("El compuesto avanzado AquateX ™ resistente presenta una superficie agradablemente cálida y suave como la seda"))
						.put(Lang.DEU, getWords("Das haltbare AquateX ™ Advanced Composite zeichnet sich durch eine angenehm warme und seidig glatte Oberfläche aus"))
						.put(Lang.FRA, getWords("Le composite avancé AquateX ™ durable présente une surface agréablement chaude et soyeuse")).build());
		translates
				.put(getCode("High-quality solid brass finished in chrome"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высококачественная твердая латунь, отделанная хромом"))
						.put(Lang.ITA, getWords("Ottone massiccio di alta qualità rifinito in cromo"))
						.put(Lang.ESP, getWords("Latón macizo de alta calidad acabado en cromo"))
						.put(Lang.DEU, getWords("Hochwertiges Messing massiv verchromt"))
						.put(Lang.FRA, getWords("Laiton massif de haute qualité fini en chrome")).build());
		translates
				.put(getCode("Available with optional air massage system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с дополнительной системой воздушного массажа"))
						.put(Lang.ITA, getWords("Disponibile con sistema opzionale di massaggio ad aria"))
						.put(Lang.ESP, getWords("Disponible con sistema de masaje de aire opcional"))
						.put(Lang.DEU, getWords("Verfügbar mit optionalem Luftmassagesystem"))
						.put(Lang.FRA, getWords("Disponible avec un système de massage à air en option")).build());
		translates
				.put(getCode("Overhead rain shower, bath filler, and hand shower included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Верхний дождевой душ, наполнитель для ванной и ручной душ включены"))
						.put(Lang.ITA, getWords("Soffione a pioggia in testa, riempitivo per il bagno e doccino inclusi"))
						.put(Lang.ESP, getWords("Incluye ducha de lluvia, ducha de baño y ducha de mano"))
						.put(Lang.DEU, getWords("Overhead-Regendusche, Wanneneinlauf und Handbrause inklusive"))
						.put(Lang.FRA, getWords("Douche à effet pluie, garniture de bain et douchette incluses")).build());
		translates
				.put(getCode("Steel frame treated with nano-ceramic cataphoresis and powder coating processes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стальная рама, обработанная нанокерамическими процессами катафореза и порошкового покрытия"))
						.put(Lang.ITA, getWords("Telaio in acciaio trattato con cataforesi nanoceramica e processi di verniciatura a polvere"))
						.put(Lang.ESP, getWords("Estructura de acero tratada con cataforesis nanocerámica y procesos de recubrimiento en polvo"))
						.put(Lang.DEU, getWords("Stahlrahmen mit nanokeramischer Kataphorese und Pulverbeschichtung"))
						.put(Lang.FRA, getWords("Châssis en acier traité par cataphorèse nanocéramique et procédés de revêtement en poudre")).build());
		translates
				.put(getCode("12 x whirlpool jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("12 x джакузи"))
						.put(Lang.ITA, getWords("12 getti idromassaggio"))
						.put(Lang.ESP, getWords("12 x jets de hidromasaje"))
						.put(Lang.DEU, getWords("12 x Whirlpooldüsen"))
						.put(Lang.FRA, getWords("12 jets d'hydromassage")).build());
		translates
				.put(getCode("Decorative wood paneling"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Декоративная деревянная обшивка"))
						.put(Lang.ITA, getWords("Pannelli decorativi in ​​legno"))
						.put(Lang.ESP, getWords("Paneles de madera decorativos"))
						.put(Lang.DEU, getWords("Dekorative Holzverkleidung"))
						.put(Lang.FRA, getWords("Lambris décoratif en bois")).build());
		translates
				.put(getCode("Electronic control panel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Электронная панель управления"))
						.put(Lang.ITA, getWords("Pannello di controllo elettronico"))
						.put(Lang.ESP, getWords("Panel de control electrónico"))
						.put(Lang.DEU, getWords("Elektronisches Bedienfeld"))
						.put(Lang.FRA, getWords("Panneau de contrôle électronique")).build());
		translates
				.put(getCode("Scratch and chemical resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Царапины и химически стойкие"))
						.put(Lang.ITA, getWords("Resistente ai graffi e ai prodotti chimici"))
						.put(Lang.ESP, getWords("Scratch y resistente a productos químicos"))
						.put(Lang.DEU, getWords("Kratzfest und chemikalienbeständig"))
						.put(Lang.FRA, getWords("Scratch et résistant aux produits chimiques")).build());
		translates
				.put(getCode("Single blue color"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Одноместный синий цвет"))
						.put(Lang.ITA, getWords("Unico colore blu"))
						.put(Lang.ESP, getWords("Color azul único"))
						.put(Lang.DEU, getWords("Einzelne blaue Farbe"))
						.put(Lang.FRA, getWords("Couleur bleue unique")).build());
		translates
				.put(getCode("Available with extra options: relax air massage, Tranquillity water heating system, whirlpool and hydrorelax massage, Bluetooth Audio"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно с дополнительными опциями: расслабляющий воздушный массаж, система подогрева воды по тишине, джакузи и гидрорелексационный массаж, Bluetooth Audio"))
						.put(Lang.ITA, getWords("Disponibile con opzioni extra: massaggio rilassante all'aria, sistema di riscaldamento dell'acqua Tranquility, idromassaggio e massaggio Hydrorelax, Bluetooth Audio"))
						.put(Lang.ESP, getWords("Disponible con opciones adicionales: masaje de aire relajante, sistema de calentamiento de agua Tranquility, hidromasaje y masaje hydrorelax, audio Bluetooth"))
						.put(Lang.DEU, getWords("Erhältlich mit zusätzlichen Optionen: Relax-Luftmassage, Tranquility-Wasser-Heizsystem, Whirlpool und Hydrorelax-Massage, Bluetooth Audio"))
						.put(Lang.FRA, getWords("Disponible avec des options supplémentaires: massage relaxant à l'air, système de chauffage de l'eau Tranquility, massage hydromassage et hydromassage, Bluetooth Audio")).build());
		translates
				.put(getCode("3 finishes - chrome, white matte, black matte"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3 отделки - хром, белый матовый, черный матовый"))
						.put(Lang.ITA, getWords("3 finiture: cromo, bianco opaco, nero opaco"))
						.put(Lang.ESP, getWords("3 acabados - cromo, blanco mate, negro mate"))
						.put(Lang.DEU, getWords("3 Ausführungen - Chrom, Weiß matt, Schwarz matt"))
						.put(Lang.FRA, getWords("3 finitions - chrome, blanc mat, noir mat")).build());
		translates
				.put(getCode("Glossy white finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глянцевая белая отделка"))
						.put(Lang.ITA, getWords("Finitura bianco lucido"))
						.put(Lang.ESP, getWords("Acabado blanco brillante"))
						.put(Lang.DEU, getWords("Glänzendes weißes Finish"))
						.put(Lang.FRA, getWords("Fini blanc brillant")).build());
		translates
				.put(getCode("12 RGB led"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("12 светодиодов RGB"))
						.put(Lang.ITA, getWords("12 led RGB"))
						.put(Lang.ESP, getWords("12 RGB led"))
						.put(Lang.DEU, getWords("12 RGB LED"))
						.put(Lang.FRA, getWords("12 RVB a mené")).build());
		translates
				.put(getCode("3 x 2” electronically controlled electrical membrane drain outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3 x 2 с электроникой с электрическим мембранным сливным отверстием"))
						.put(Lang.ITA, getWords("Scarichi a membrana elettrici a controllo elettronico da 3 x 2 "))
						.put(Lang.ESP, getWords("3 salidas de drenaje de membrana eléctricas con control electrónico de 3 x 2 "))
						.put(Lang.DEU, getWords("3 x 2 elektronisch gesteuerte Ablassöffnungen für elektrische Membranen"))
						.put(Lang.FRA, getWords("Sorties de drainage à membrane électrique à commande électronique de 3 x 2 po")).build());
		translates
				.put(getCode("Designed for comfortable four-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного купания с четырьмя лицами"))
						.put(Lang.ITA, getWords("Progettato per una balneazione a quattro persone confortevole"))
						.put(Lang.ESP, getWords("Diseñado para un baño cómodo para cuatro personas"))
						.put(Lang.DEU, getWords("Entworfen für komfortables Baden für vier Personen"))
						.put(Lang.FRA, getWords("Conçu pour une baignade confortable pour quatre personnes")).build());
		translates
				.put(getCode("Underwater chromotherapy with slow colour rotation or fixed colour mode in one of 6 available tones of choice"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подводная хромотерапия с медленным вращением цвета или фиксированным цветовым режимом в одном из 6 доступных тонов выбора"))
						.put(Lang.ITA, getWords("Cromoterapia subacquea con rotazione dei colori lenta o modalità a colori fissi in una delle 6 tonalità disponibili a scelta"))
						.put(Lang.ESP, getWords("La cromoterapia subacuática con rotación de color lenta o modo de color fijo en uno de los 6 tonos de elección disponibles"))
						.put(Lang.DEU, getWords("Unterwasser-Chromotherapie mit langsamer Farbrotation oder festem Farbmodus in einem von 6 verfügbaren Farbtönen"))
						.put(Lang.FRA, getWords("Chromothérapie sous-marine avec rotation lente des couleurs ou mode de couleur fixe dans l'un des 6 tons de choix disponibles")).build());
		translates
				.put(getCode("Lifetime warranty on mechanism"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Пожизненная гарантия на механизм"))
						.put(Lang.ITA, getWords("Garanzia a vita sul meccanismo"))
						.put(Lang.ESP, getWords("Garantía de por vida en el mecanismo"))
						.put(Lang.DEU, getWords("Lebenslange Garantie auf Mechanismus"))
						.put(Lang.FRA, getWords("Garantie à vie sur le mécanisme")).build());
		translates
				.put(getCode("Designed for comfortable one-person bathing (Tulip) or two-person bathing (Tulip Grande)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного однолюстного купания (Tulip) или купания двух человек (Tulip Grande)"))
						.put(Lang.ITA, getWords("Progettato per una balneazione individuale da una persona (Tulip) o una balneazione per due persone (Tulip Grande)"))
						.put(Lang.ESP, getWords("Diseñado para un baño cómodo para una persona (Tulip) o para dos personas (Tulip Grande)"))
						.put(Lang.DEU, getWords("Entworfen für komfortables Ein-Personen-Baden (Tulip) oder Zwei-Personen-Baden (Tulip Grande)"))
						.put(Lang.FRA, getWords("Conçu pour la baignade confortable d'une personne (Tulip) ou la baignade de deux personnes (Tulip Grande)")).build());
		translates
				.put(getCode("220/240V power with 16A-dedicated 220V RCD protected circuit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("220/240 В с защищенной 16 А-защищенной цепью, защищенной RCD"))
						.put(Lang.ITA, getWords("Alimentazione 220 / 240V con circuito protetto da 16 V dedicato a 220 V RCD"))
						.put(Lang.ESP, getWords("Potencia de 220/240 V con circuito protegido RCD de 220 V dedicado a 16 A"))
						.put(Lang.DEU, getWords("220 / 240V Stromversorgung mit 16A-dediziertem 220V RCD-Schutzschaltung"))
						.put(Lang.FRA, getWords("Alimentation 220 / 240V avec circuit protégé RCD 220V dédié 16A")).build());
		translates
				.put(getCode("One water self closing tap for best performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Один водонагреватель для максимальной производительности"))
						.put(Lang.ITA, getWords("Un rubinetto a chiusura automatica dell'acqua per le migliori prestazioni"))
						.put(Lang.ESP, getWords("Un grifo con cierre automático de agua para un mejor rendimiento"))
						.put(Lang.DEU, getWords("Ein Wasserhahn zum Selbstschließen für beste Leistung"))
						.put(Lang.FRA, getWords("Un robinet à fermeture automatique pour une meilleure performance")).build());
		translates
				.put(getCode("Recommended minimum of 3 bar water pressure for best performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемое минимальное давление воды 3 бар для лучшей производительности"))
						.put(Lang.ITA, getWords("Minimo consigliato di 3 bar di pressione dell'acqua per le migliori prestazioni"))
						.put(Lang.ESP, getWords("Mínimo recomendado de 3 bares de presión de agua para un mejor rendimiento"))
						.put(Lang.DEU, getWords("Empfohlenes Minimum von 3 bar Wasserdruck für beste Leistung"))
						.put(Lang.FRA, getWords("Pression d'eau minimale recommandée de 3 bars pour de meilleures performances")).build());
		translates
				.put(getCode("Underwater ?hromotherapy with slow color rotation or fixed color mode in one of 6 available tones of choice"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подводная хромотерапия с медленным вращением цвета или фиксированным цветовым режимом в одном из 6 доступных тонов выбора"))
						.put(Lang.ITA, getWords("Cromoterapia subacquea con rotazione dei colori lenta o modalità a colori fissi in una delle 6 tonalità disponibili a scelta"))
						.put(Lang.ESP, getWords("Hromoterapia subacuática con rotación de color lenta o modo de color fijo en uno de los 6 tonos de elección disponibles"))
						.put(Lang.DEU, getWords("Unterwassertherapie mit langsamer Farbrotation oder festem Farbmodus in einem von 6 verfügbaren Farbtönen"))
						.put(Lang.FRA, getWords("Hromothérapie sous-marine avec rotation lente des couleurs ou mode couleur fixe dans l'un des 6 tons de choix disponibles")).build());
		translates
				.put(getCode("Telescopic pipe extends from 380 mm min to 400 mm max"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Телескопическая труба простирается от 380 мм до 400 мм макс."))
						.put(Lang.ITA, getWords("Il tubo telescopico si estende da 380 mm min a 400 mm max"))
						.put(Lang.ESP, getWords("El tubo telescópico se extiende desde 380 mm mínimo hasta 400 mm máximo"))
						.put(Lang.DEU, getWords("Teleskoprohr erstreckt sich von 380 mm min bis 400 mm max"))
						.put(Lang.FRA, getWords("Le tube télescopique s'étend de 380 mm min à 400 mm max")).build());
		translates
				.put(getCode("Grey powder coated aluminum frame"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Серый порошковый алюминиевый каркас"))
						.put(Lang.ITA, getWords("Telaio in alluminio verniciato a polvere grigio"))
						.put(Lang.ESP, getWords("Marco de aluminio recubierto de polvo gris"))
						.put(Lang.DEU, getWords("Grau pulverbeschichteter Aluminiumrahmen"))
						.put(Lang.FRA, getWords("Cadre en aluminium recouvert de poudre grise")).build());
		translates
				.put(getCode("Also available with a powder-coated aluminum frame in white (A 12) or graphite (A 14), with a dark grey (S9) fiber cement top"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Также поставляется с алюминиевой рамой с порошковым покрытием в белом (A 12) или графитом (A 14), с темно-серой (S9) волокнистой цементной крышкой"))
						.put(Lang.ITA, getWords("Disponibile anche con telaio in alluminio verniciato a polvere in bianco (A 12) o grafite (A 14), con top in fibrocemento grigio scuro (S9)"))
						.put(Lang.ESP, getWords("También disponible con un marco de aluminio con recubrimiento de polvo en blanco (A 12) o grafito (A 14), con un top de fibrocemento gris oscuro (S9)"))
						.put(Lang.DEU, getWords("Auch mit einem pulverbeschichteten Aluminiumrahmen in Weiß (A 12) oder Graphit (A 14), mit einer dunkelgrauen (S9) Faserzementplatte erhältlich"))
						.put(Lang.FRA, getWords("En outre disponible avec un cadre en aluminium enduit de poudre en blanc (A 12) ou en graphite (A 14), avec un dessus en fibrociment gris foncé (S9)")).build());
		translates
				.put(getCode("Handheld shower attachment allows for a more targeted cleaning"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ручная установка для душа позволяет проводить целенаправленную очистку"))
						.put(Lang.ITA, getWords("L'accessorio per doccia palmare consente una pulizia più mirata"))
						.put(Lang.ESP, getWords("El accesorio de la ducha de mano permite una limpieza más específica"))
						.put(Lang.DEU, getWords("Handbrause ermöglicht eine gezieltere Reinigung"))
						.put(Lang.FRA, getWords("L'accessoire de douche à main permet un nettoyage plus ciblé")).build());
		translates
				.put(getCode("EcoMarmor™ Lite cutting edge lightweight cast stone composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("EcoMarmor ™ Lite режущая кромка из легкого литого каменного композита"))
						.put(Lang.ITA, getWords("EcoMarmor ™ Lite: composito leggero in pietra fuso"))
						.put(Lang.ESP, getWords("EcoMarmor ™ Lite, compuesto de piedra fundida liviana de vanguardia"))
						.put(Lang.DEU, getWords("EcoMarmor ™ Lite Schneidkanten-Leichtbau-Verbundwerkstoff"))
						.put(Lang.FRA, getWords("EcoMarmor ™ Lite Composite en pierre coulée légère de pointe")).build());
		translates
				.put(getCode("Ceiling mounted and Eco-friendly shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Потолочная и экологически чистая душевая насадка"))
						.put(Lang.ITA, getWords("Soffione montato a soffitto ed ecologico"))
						.put(Lang.ESP, getWords("Cabeza de ducha respetuosa del medio ambiente montada en el techo"))
						.put(Lang.DEU, getWords("Deckenmontierter und umweltfreundlicher Duschkopf"))
						.put(Lang.FRA, getWords("Tête de douche fixée au plafond et écologique")).build());
		translates
				.put(getCode("Hydromassage Zone Control (HZC™) for 2 bathers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Управление гидромассажной зоной (HZC ™) для 2 купальщиков"))
						.put(Lang.ITA, getWords("Hydromassage Zone Control (HZC ™) per 2 bagnanti"))
						.put(Lang.ESP, getWords("Control de Zona de Hidromasaje (HZC ™) para 2 bañistas"))
						.put(Lang.DEU, getWords("Hydromassage Zone Control (HZC ™) für 2 Badegäste"))
						.put(Lang.FRA, getWords("Contrôle de zone d'hydromassage (HZC ™) pour 2 baigneurs")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 2 outlet diverter and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 2 выходными диверторами и металлической пластиной"))
						.put(Lang.ITA, getWords("Miscelatore doccia a parete con deviatore 2 uscite e piastra metallica"))
						.put(Lang.ESP, getWords("Mezclador de ducha de pared con 2 desviadores de salida y placa de metal"))
						.put(Lang.DEU, getWords("Wand-Brausemischer mit 2 Auslaufweichen und Metallplatte"))
						.put(Lang.FRA, getWords("Mitigeur de douche mural avec inverseur à 2 sorties et plaque métallique")).build());
		translates
				.put(getCode("Freestanding construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Автономное строительство"))
						.put(Lang.ITA, getWords("Costruzione indipendente"))
						.put(Lang.ESP, getWords("Construcción independiente"))
						.put(Lang.DEU, getWords("Freistehende Konstruktion"))
						.put(Lang.FRA, getWords("Construction autoportante")).build());
		translates
				.put(getCode("Jet type: continuous or with a variable flow for better cleansing action"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Тип струи: непрерывный или с переменным потоком для лучшего очищающего действия"))
						.put(Lang.ITA, getWords("Tipo di getto: continuo o con flusso variabile per una migliore azione detergente"))
						.put(Lang.ESP, getWords("Tipo de chorro: continuo o con flujo variable para una mejor acción limpiadora"))
						.put(Lang.DEU, getWords("Jet-Typ: kontinuierlich oder mit einem variablen Fluss für eine bessere Reinigungswirkung"))
						.put(Lang.FRA, getWords("Jet type: continu ou à débit variable pour une meilleure action nettoyante")).build());
		translates
				.put(getCode("Durable, stain resistant coating"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочное, устойчивое к пятнам покрытие"))
						.put(Lang.ITA, getWords("Rivestimento durevole e resistente alle macchie"))
						.put(Lang.ESP, getWords("Revestimiento duradero y resistente a las manchas"))
						.put(Lang.DEU, getWords("Haltbare, schmutzabweisende Beschichtung"))
						.put(Lang.FRA, getWords("Revêtement durable et résistant aux taches")).build());
		translates
				.put(getCode("Constructed of 8mm thick 100% heavy gauge sanitary grade precision acrylic"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен из 8 мм толстой 100% тяжелой калибровки санитарной точности точности акрил"))
						.put(Lang.ITA, getWords("Costruito con acrilico di precisione di grado sanitario di spessore calibro 100% di spessore 8mm"))
						.put(Lang.ESP, getWords("Construido de 8 mm de espesor 100% de calibre sanitario de grado sanitario de precisión"))
						.put(Lang.DEU, getWords("Konstruiert aus 8mm dickem 100% dickem Sanitär-Präzisions-Acryl"))
						.put(Lang.FRA, getWords("Construit de 8mm d'épaisseur 100% épais de qualité sanitaire acrylique de précision")).build());
		translates
				.put(getCode("27 jets and 12 air nozzles in the spa"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("27 струй и 12 воздушных сопел в спа-салоне"))
						.put(Lang.ITA, getWords("27 getti e 12 ugelli d'aria nella spa"))
						.put(Lang.ESP, getWords("27 jets y 12 boquillas de aire en el spa"))
						.put(Lang.DEU, getWords("27 Düsen und 12 Luftdüsen im Spa"))
						.put(Lang.FRA, getWords("27 jets et 12 buses d'air dans le spa")).build());
		translates
				.put(getCode("Easily cleaned without affecting its surface characteristics"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Легко очищается, не влияя на его поверхностные характеристики"))
						.put(Lang.ITA, getWords("Facilmente pulito senza alterare le sue caratteristiche superficiali"))
						.put(Lang.ESP, getWords("Se limpia fácilmente sin afectar sus características superficiales"))
						.put(Lang.DEU, getWords("Leicht zu reinigen, ohne seine Oberflächeneigenschaften zu beeinträchtigen"))
						.put(Lang.FRA, getWords("Facilement nettoyé sans affecter ses caractéristiques de surface")).build());
		translates
				.put(getCode("Premium foam insulation for superior heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-пеноизоляция для превосходного удержания тепла"))
						.put(Lang.ITA, getWords("Isolamento in schiuma Premium per una migliore ritenzione del calore"))
						.put(Lang.ESP, getWords("Aislamiento de espuma de primera calidad para una retención de calor superior"))
						.put(Lang.DEU, getWords("Premium-Schaumisolierung für überlegene Wärmespeicherung"))
						.put(Lang.FRA, getWords("Isolation en mousse de qualité supérieure pour une rétention de chaleur supérieure")).build());
		translates
				.put(getCode("15 (380 mm) square built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("15 (380 мм) встроенная душевая насадка"))
						.put(Lang.ITA, getWords("Soffione da incasso quadrato da 380 mm (15 )"))
						.put(Lang.ESP, getWords("Cabezal de ducha incorporado cuadrado de 15 (380 mm)"))
						.put(Lang.DEU, getWords("15 (380 mm) quadratischer Einbauduschkopf"))
						.put(Lang.FRA, getWords("Pomme de douche encastrée carrée de 15 (380 mm)")).build());
		translates
				.put(getCode("Preinstalled decorative pop-up waste fitting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предустановленный декоративный всплывающий мусор"))
						.put(Lang.ITA, getWords("Scarico pop-up decorativo preinstallato"))
						.put(Lang.ESP, getWords("Accesorio de desagüe emergente decorativo preinstalado"))
						.put(Lang.DEU, getWords("Vorinstallierte dekorative Ablaufgarnitur"))
						.put(Lang.FRA, getWords("Garniture de vidage décorative préinstallée")).build());
		translates
				.put(getCode("Wall-mounted built-in, at recommended height of 2 meters"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный встроенный, на рекомендуемой высоте 2 метра"))
						.put(Lang.ITA, getWords("Built-in a parete, all'altezza consigliata di 2 metri"))
						.put(Lang.ESP, getWords("Montado en la pared incorporado, a la altura recomendada de 2 metros"))
						.put(Lang.DEU, getWords("Wandeinbau, in der empfohlenen Höhe von 2 Metern"))
						.put(Lang.FRA, getWords("Mural encastré, à la hauteur recommandée de 2 mètres")).build());
		translates
				.put(getCode("Spa with galvanised metal structure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Спа с оцинкованной структурой металла"))
						.put(Lang.ITA, getWords("Spa con struttura metallica zincata"))
						.put(Lang.ESP, getWords("Spa con estructura de metal galvanizado"))
						.put(Lang.DEU, getWords("Spa mit verzinkter Metallstruktur"))
						.put(Lang.FRA, getWords("Spa avec structure en métal galvanisé")).build());
		translates
				.put(getCode("Lever handle two outlet diverter control – up for the hand shower, down for the faucet filler"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рычаг рукоятки управления двумя выходными дивертерами - для ручного душа, вниз для наполнителя крана"))
						.put(Lang.ITA, getWords("La leva gestisce due comandi per il deviatore di scarico - per la doccetta, verso il basso per il rubinetto di riempimento"))
						.put(Lang.ESP, getWords("Maneta de palanca dos control de desviación de salida - arriba para la ducha de mano, abajo para el llenador de la llave"))
						.put(Lang.DEU, getWords("Drückergriff mit zwei Auslassweichen - oben für die Handbrause, unten für den Wasserhahnfüller"))
						.put(Lang.FRA, getWords("Levier poignée à deux prises de contrôle de sortie - vers le haut pour la douchette, vers le bas pour le remplisseur de robinet")).build());
		translates
				.put(getCode("5 year limited warranty on the blower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на воздуходувку"))
						.put(Lang.ITA, getWords("Garanzia limitata di 5 anni sul ventilatore"))
						.put(Lang.ESP, getWords("Garantía limitada de 5 años en el soplador"))
						.put(Lang.DEU, getWords("5 Jahre Garantie auf das Gebläse"))
						.put(Lang.FRA, getWords("Garantie limitée de 5 ans sur le souffleur")).build());
		translates
				.put(getCode("Max flow rate of the shower head: 3.9 GPM (15 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальный расход ливня: 3,9 GPM (15 LPM)"))
						.put(Lang.ITA, getWords("Portata massima del soffione: 3.9 GPM (15 LPM)"))
						.put(Lang.ESP, getWords("Caudal máximo del cabezal de la ducha: 3.9 GPM (15 LPM)"))
						.put(Lang.DEU, getWords("Max Durchflussrate des Duschkopfes: 3,9 GPM (15 LPM)"))
						.put(Lang.FRA, getWords("Débit maximum de la pomme de douche: 3.9 GPM (15 LPM)")).build());
		translates
				.put(getCode("Posterior cleansing with a variable spray (5 settings)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Задняя очистка с переменным спреем (5 настроек)"))
						.put(Lang.ITA, getWords("Pulizia posteriore con spray variabile (5 impostazioni)"))
						.put(Lang.ESP, getWords("Limpieza posterior con un spray variable (5 configuraciones)"))
						.put(Lang.DEU, getWords("Hintere Reinigung mit einem variablen Spray (5 Einstellungen)"))
						.put(Lang.FRA, getWords("Nettoyage postérieur avec un spray variable (5 réglages)")).build());
		translates
				.put(getCode("Contemporary, one-of-a-kind and complete bathroom furniture set"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современный, единственный в своем роде комплект мебели для ванной комнаты"))
						.put(Lang.ITA, getWords("Set da bagno contemporaneo, unico nel suo genere e completo"))
						.put(Lang.ESP, getWords("Conjunto de muebles de baño contemporáneo, único y completo"))
						.put(Lang.DEU, getWords("Modernes, einzigartiges und komplettes Badezimmermöbelset"))
						.put(Lang.FRA, getWords("Mobilier de salle de bain contemporain, unique en son genre et complet")).build());
		translates
				.put(getCode("Anti-bacterial additives are added to the acrylic polymers during the manufacturing stage"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Антибактериальные добавки добавляют к акриловым полимерам на стадии изготовления"))
						.put(Lang.ITA, getWords("Additivi antibatterici vengono aggiunti ai polimeri acrilici durante la fase di produzione"))
						.put(Lang.ESP, getWords("Aditivos antibacterianos se agregan a los polímeros acrílicos durante la etapa de fabricación"))
						.put(Lang.DEU, getWords("Antibakterielle Additive werden den Acrylpolymeren während der Herstellungsphase hinzugefügt"))
						.put(Lang.FRA, getWords("Des additifs anti-bactériens sont ajoutés aux polymères acryliques au cours de la fabrication")).build());
		translates
				.put(getCode("Warm air massage"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Теплый воздух"))
						.put(Lang.ITA, getWords("Massaggio ad aria calda"))
						.put(Lang.ESP, getWords("Masaje con aire caliente"))
						.put(Lang.DEU, getWords("Warmluftmassage"))
						.put(Lang.FRA, getWords("Massage à l'air chaud")).build());
		translates
				.put(getCode("Plugs into a regular household electrical outlet; 2 Minute Purge Cycle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Вставляет в обычную бытовую электрическую розетку; 2-минутный цикл продувки"))
						.put(Lang.ITA, getWords("Si inserisce in una normale presa di corrente domestica; Ciclo di spurgo di 2 minuti"))
						.put(Lang.ESP, getWords("Se conecta a una toma de corriente doméstica normal; Ciclo de purga de 2 minutos"))
						.put(Lang.DEU, getWords("Stecker in eine normale Haushaltssteckdose; 2 Minuten Spülzyklus"))
						.put(Lang.FRA, getWords("Se branche dans une prise électrique domestique ordinaire; Cycle de purge de 2 minutes")).build());
		translates
				.put(getCode("Aromatherapy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords(" Ароматерапия"))
						.put(Lang.ITA, getWords(" aromaterapia"))
						.put(Lang.ESP, getWords(" Aromaterapia"))
						.put(Lang.DEU, getWords(" Aromatherapie"))
						.put(Lang.FRA, getWords(" Aromathérapie")).build());
		translates
				.put(getCode("Anti-lime showerhead of modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Антивоздушная душевая головка современного дизайна"))
						.put(Lang.ITA, getWords("Soffione anticalcare di design moderno"))
						.put(Lang.ESP, getWords("Cabezal de ducha antical del diseño moderno"))
						.put(Lang.DEU, getWords("Kalkduschkopf mit modernem Design"))
						.put(Lang.FRA, getWords("Pomme de douche anti-calcaire de conception moderne")).build());
		translates
				.put(getCode("Budget friendly – unbeatable price/performance proposition"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Бюджет дружелюбный - непревзойденное предложение цены / производительности"))
						.put(Lang.ITA, getWords("Budget friendly - proposta imbattibile di prezzo / prestazioni"))
						.put(Lang.ESP, getWords("Presupuesto amigable: propuesta inmejorable de precio / rendimiento"))
						.put(Lang.DEU, getWords("Budget freundlich - unschlagbares Preis- / Leistungsverhältnis"))
						.put(Lang.FRA, getWords("Budget sympa - prix imbattable / proposition de performance")).build());
		translates
				.put(getCode("Compact, small footprint, premium design freestanding bathtub with roomy interiors"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Компактная, небольшая площадь, высококачественная автономная ванна с вместительными интерьерами"))
						.put(Lang.ITA, getWords("Compatto, piccolo ingombro, vasca freestanding di design di alta qualità con interni spaziosi"))
						.put(Lang.ESP, getWords("Bañera independiente de diseño premium, pequeña y de diseño compacto con espaciosos interiores"))
						.put(Lang.DEU, getWords("Kompakt, kleine Standfläche, Premium-Design freistehende Badewanne mit geräumigen Innenräumen"))
						.put(Lang.FRA, getWords("Compact, petit encombrement, baignoire autoportante de conception premium avec des intérieurs spacieux")).build());
		translates
				.put(getCode("Designed for comfortable six-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного купания на шесть человек"))
						.put(Lang.ITA, getWords("Progettato per una comoda balneazione per sei persone"))
						.put(Lang.ESP, getWords("Diseñado para un baño cómodo para seis personas"))
						.put(Lang.DEU, getWords("Entworfen für komfortables Baden für sechs Personen"))
						.put(Lang.FRA, getWords("Conçu pour une baignade confortable pour six personnes")).build());
		translates
				.put(getCode("Over 16 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более 16 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords("Oltre 16 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords("Más de 16 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords("Über 16 Low-Profile-Luftdüsen"))
						.put(Lang.FRA, getWords("Plus de 16 jets d'air à profil bas")).build());
		translates
				.put(getCode("UV resistant high quality materials"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("УФ-устойчивые материалы высокого качества"))
						.put(Lang.ITA, getWords("Materiali di alta qualità resistenti ai raggi UV"))
						.put(Lang.ESP, getWords("Materiales resistentes a los rayos UV de alta calidad"))
						.put(Lang.DEU, getWords("UV-beständige hochwertige Materialien"))
						.put(Lang.FRA, getWords("Matériaux de haute qualité résistants aux UV")).build());
		translates
				.put(getCode("Adjustable height legs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Регулируемые ножки высоты"))
						.put(Lang.ITA, getWords("Gambe regolabili in altezza"))
						.put(Lang.ESP, getWords("Piernas de altura ajustable"))
						.put(Lang.DEU, getWords("Höhenverstellbare Beine"))
						.put(Lang.FRA, getWords("Jambes réglables de taille")).build());
		translates
				.put(getCode("Available with optional advanced hydro and/or air massage system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с дополнительной прогрессивной системой гидро- и / или воздушного массажа"))
						.put(Lang.ITA, getWords("Disponibile con sistema opzionale avanzato di idro e / o massaggio ad aria"))
						.put(Lang.ESP, getWords("Disponible con sistema avanzado opcional de hidromasaje y / o aire"))
						.put(Lang.DEU, getWords("Verfügbar mit optionalem fortschrittlichem Hydro- und / oder Luftmassagesystem"))
						.put(Lang.FRA, getWords("Disponible avec système de massage avancé hydro et / ou air en option")).build());
		translates
				.put(getCode("Standard 1 ½  waste outlet - installs like any other bath waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандартный 1 ½ сливной вывод - устанавливается, как и любые другие отходы ванны"))
						.put(Lang.ITA, getWords("Scarico standard da 1 ½ - si installa come qualsiasi altro rifiuto da bagno"))
						.put(Lang.ESP, getWords("Salida de desperdicio estándar de 1 ½ - se instala como cualquier otro residuo de baño"))
						.put(Lang.DEU, getWords("Standard 1 ½ Waste Outlet - installiert wie jeder andere Badabfall"))
						.put(Lang.FRA, getWords("Sortie d'évacuation standard de 1 ½ - s'installe comme tout autre déchet de baignoire")).build());
		translates
				.put(getCode("Premium acrylic with high gloss white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум акрил с белой поверхностью с высоким блеском"))
						.put(Lang.ITA, getWords("Acrilico Premium con superficie bianca lucida"))
						.put(Lang.ESP, getWords("Acrílico premium con superficie blanca de alto brillo"))
						.put(Lang.DEU, getWords("Premium-Acryl mit hochglänzender weißer Oberfläche"))
						.put(Lang.FRA, getWords("Acrylique Premium avec surface blanche haute brillance")).build());
		translates
				.put(getCode("Dual Action Ozone and UV-C disinfection system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система обеззараживания двойного действия озоном и УФ-С"))
						.put(Lang.ITA, getWords("Sistema di disinfezione Dual Action Ozone e UV-C"))
						.put(Lang.ESP, getWords("Sistema de desinfección Dual Ozone y UV-C"))
						.put(Lang.DEU, getWords("Dual Action Ozon- und UV-C-Desinfektionssystem"))
						.put(Lang.FRA, getWords("Système de désinfection double couche Ozone et UV-C")).build());
		translates
				.put(getCode("cUPC certified"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("cUPC сертифицирован"))
						.put(Lang.ITA, getWords("certificato cUPC"))
						.put(Lang.ESP, getWords("certificado por cUPC"))
						.put(Lang.DEU, getWords("cUPC zertifiziert"))
						.put(Lang.FRA, getWords("cUPC certifié")).build());
		translates
				.put(getCode("66 jets and 12 air nozzles in the spa"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("66 струй и 12 воздушных сопел в спа-салоне"))
						.put(Lang.ITA, getWords("66 getti e 12 ugelli d'aria nella spa"))
						.put(Lang.ESP, getWords("66 jets y 12 boquillas de aire en el spa"))
						.put(Lang.DEU, getWords("66 Düsen und 12 Luftdüsen im Spa"))
						.put(Lang.FRA, getWords("66 jets et 12 buses d'air dans le spa")).build());
		translates
				.put(getCode("This item set includes a concealed body (CS 535) with an option of 1 or 2 outlets 1/2”"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор предметов включает скрытый корпус (CS 535) с опцией 1 или 2 выхода 1/2 "))
						.put(Lang.ITA, getWords("Questo set di articoli include un corpo nascosto (CS 535) con un'opzione di 1 o 2 uscite 1/2 "))
						.put(Lang.ESP, getWords("Este conjunto de elementos incluye un cuerpo oculto (CS 535) con una opción de 1 o 2 salidas de 1/2 "))
						.put(Lang.DEU, getWords("Dieses Set beinhaltet einen versteckten Körper (CS 535) mit einer Option von 1 oder 2 Ausgängen 1/2 "))
						.put(Lang.FRA, getWords("Cet ensemble d'articles comprend un corps dissimulé (CS 535) avec une option de 1 ou 2 sorties 1/2 ")).build());
		translates
				.put(getCode("Designed for comfortable four-person soaking"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного пропитывания четырех человек"))
						.put(Lang.ITA, getWords("Progettato per un ammollo a quattro persone confortevole"))
						.put(Lang.ESP, getWords("Diseñado para un cómodo remojo de cuatro personas"))
						.put(Lang.DEU, getWords("Konzipiert für komfortables Vier-Personen-Einweichen"))
						.put(Lang.FRA, getWords("Conçu pour le trempage confortable de quatre personnes")).build());
		translates
				.put(getCode("Deep, full-body soak"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глубокое, полное погружение в тело"))
						.put(Lang.ITA, getWords("Immergersi in profondità"))
						.put(Lang.ESP, getWords("Remojo profundo y de cuerpo entero"))
						.put(Lang.DEU, getWords("Tiefes Ganzkörperbad"))
						.put(Lang.FRA, getWords("Profond, le corps entier tremper")).build());
		translates
				.put(getCode("Hypoallergenic black solid surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная черная твердая поверхность"))
						.put(Lang.ITA, getWords("Superficie solida nera ipoallergenica"))
						.put(Lang.ESP, getWords("Superficie sólida negra hipoalergénica"))
						.put(Lang.DEU, getWords("Hypoallergene schwarze feste Oberfläche"))
						.put(Lang.FRA, getWords("Surface solide noire hypoallergénique")).build());
		translates
				.put(getCode("This item set includes a concealed body (CS137) with an option of 2 outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор элементов включает скрытый корпус (CS137) с возможностью 2 выходов"))
						.put(Lang.ITA, getWords("Questo set di articoli include un corpo nascosto (CS137) con un'opzione di 2 prese"))
						.put(Lang.ESP, getWords("Este conjunto de elementos incluye un cuerpo oculto (CS137) con una opción de 2 tomacorrientes"))
						.put(Lang.DEU, getWords("Dieses Set enthält einen versteckten Körper (CS137) mit einer Option von 2 Steckdosen"))
						.put(Lang.FRA, getWords("Cet ensemble d'articles inclut un corps caché (CS137) avec une option de 2 sorties")).build());
		translates
				.put(getCode("Non-porous surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непористая поверхность"))
						.put(Lang.ITA, getWords("Superficie non porosa"))
						.put(Lang.ESP, getWords("Superficie no porosa"))
						.put(Lang.DEU, getWords("Nicht poröse Oberfläche"))
						.put(Lang.FRA, getWords("Surface non poreuse")).build());
		translates
				.put(getCode("Electronic diverter between handshower and filling outlet (allows for integration with bath waste-overflow with filling function via overflow or via drain OR by adding a decorative spout of your choice like the Italia Waterfall)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Электронный дивертер между раздаточным устройством и розеткой (позволяет интегрироваться с переливкой ванны с функцией наполнения через перелив или через слив ИЛИ путем добавления декоративного наполнителя по вашему выбору, такого как водопад Italia)"))
						.put(Lang.ITA, getWords("Deviatore elettronico tra doccetta e bocchettone di riempimento (consente l'integrazione con troppopieno della vasca da bagno con funzione di riempimento tramite troppopieno o tramite scarico OPPURE aggiungendo un beccuccio decorativo a scelta come la Cascata Italia)"))
						.put(Lang.ESP, getWords("Desviador electrónico entre la ducha de mano y la salida de llenado (permite la integración con desbordamiento de residuos de baño con función de llenado por rebose o por drenaje O agregando un caño decorativo de su elección como la cascada de Italia)"))
						.put(Lang.DEU, getWords("Elektronischer Umsteller zwischen Handbrause und Abfüllstutzen (ermöglicht die Integration in Badablaufüberlauf mit Abfüllfunktion über Überlauf oder über Abfluss ODER durch Hinzufügen eines dekorativen Ausgusses Ihrer Wahl wie der Italia Waterfall)"))
						.put(Lang.FRA, getWords("Inverseur électronique entre la douchette et la sortie de remplissage (permet l'intégration avec le déversement des eaux usées de la baignoire avec fonction de remplissage par trop-plein ou par drain OU en ajoutant un bec décoratif de votre choix comme la cascade Italia)"))
						.build());
		translates
				.put(getCode("5 year limited warranty on the furniture; 1 year limited warranty on the basin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на мебель; 1 год ограниченной гарантии на бассейн"))
						.put(Lang.ITA, getWords("Garanzia limitata di 5 anni sui mobili; Garanzia limitata di 1 anno sul bacino"))
						.put(Lang.ESP, getWords("Garantía limitada de 5 años en los muebles; 1 año de garantía limitada en el lavabo"))
						.put(Lang.DEU, getWords("5 Jahre beschränkte Garantie auf die Möbel; 1 Jahr Garantie auf das Becken"))
						.put(Lang.FRA, getWords("Garantie limitée de 5 ans sur les meubles; Garantie limitée de 1 an sur le bassin")).build());
		translates
				.put(getCode("Double insulation for saving energy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Двойная изоляция для экономии энергии"))
						.put(Lang.ITA, getWords("Doppio isolamento per risparmiare energia"))
						.put(Lang.ESP, getWords("Doble aislamiento para ahorrar energía"))
						.put(Lang.DEU, getWords("Doppelte Isolierung zum Energiesparen"))
						.put(Lang.FRA, getWords("Double isolation pour économiser l'énergie")).build());
		translates
				.put(getCode("Cartridge filter"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Картриджный фильтр"))
						.put(Lang.ITA, getWords("Filtro a cartuccia"))
						.put(Lang.ESP, getWords("Filtro de cartucho"))
						.put(Lang.DEU, getWords("Kartuschenfilter"))
						.put(Lang.FRA, getWords("Filtre à cartouche")).build());
		translates
				.put(getCode("Drawer chest dimensions: 55 L x 20.75 W x 16.5 H (in) / 140 L x 53 W x 42 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Размеры ящика: 55 L x 20,75 W x 16,5 H (дюйм) / 140 L x 53 Вт x 42 H (см)"))
						.put(Lang.ITA, getWords("Dimensioni cassa del cassetto: 55 L x 20,75 W x 16,5 H (in) / 140 L x 53 W x 42 H (cm)"))
						.put(Lang.ESP, getWords("Dimensiones del cofre del cajón: 55 L x 20.75 W x 16.5 H (in) / 140 L x 53 W x 42 H (cm)"))
						.put(Lang.DEU, getWords("Schubladenschrank Abmessungen: 55 L x 20,75 B x 16,5 H (in) / 140 L x 53 B x 42 H (cm)"))
						.put(Lang.FRA, getWords("Dimensions du coffre du tiroir: 55 L x 20,75 L x 16,5 H (po) / 140 L x 53 L x 42 H (cm)")).build());
		translates
				.put(getCode("1400 x 700 mm (55 x 27.5 in) wall mirror with built-in LED strip lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1400 x 700 мм (55 x 27,5 дюймов) настенное зеркало со встроенным светодиодным освещением"))
						.put(Lang.ITA, getWords("Specchio da parete 1400 x 700 mm (55 x 27,5 pollici) con illuminazione a strisce LED incorporata"))
						.put(Lang.ESP, getWords("Espejo de pared de 1400 x 700 mm (55 x 27.5 in) con iluminación incorporada en tira de LED"))
						.put(Lang.DEU, getWords("Wandspiegel 1400 x 700 mm (55 x 27,5 in) mit integrierter LED-Streifenbeleuchtung"))
						.put(Lang.FRA, getWords("Miroir mural de 1400 x 700 mm (55 x 27,5 po) avec éclairage à DEL intégré")).build());
		translates
				.put(getCode("Feature cover-plate with bracket kit and a filter."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Специальная крышка с комплектом кронштейнов и фильтром."))
						.put(Lang.ITA, getWords("Feature piastra di copertura con kit staffa e filtro."))
						.put(Lang.ESP, getWords("Característica placa de cubierta con kit de soporte y un filtro."))
						.put(Lang.DEU, getWords("Feature Deckplatte mit Halterung Kit und einem Filter."))
						.put(Lang.FRA, getWords("Plaque de recouvrement de caractéristiques avec kit de support et un filtre.")).build());
		translates
				.put(getCode("Available in a variety of custom color options white, monochrome, pearl or granite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступный в различных пользовательских цветовых вариантах белый, монохромный, жемчужный или гранитный"))
						.put(Lang.ITA, getWords("Disponibile in una varietà di opzioni colore personalizzate bianco, monocromo, perla o granito"))
						.put(Lang.ESP, getWords("Disponible en una variedad de opciones de color personalizado blanco, monocromo, perla o granito"))
						.put(Lang.DEU, getWords("Erhältlich in einer Vielzahl von benutzerdefinierten Farboptionen weiß, einfarbig, Perlmutt oder Granit"))
						.put(Lang.FRA, getWords("Disponible dans une variété d'options de couleur personnalisées blanc, monochrome, perle ou granit")).build());
		translates
				.put(getCode("Arm length 10 (25 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукава 10 (25 см)"))
						.put(Lang.ITA, getWords("Braccio lunghezza 10 (25 cm)"))
						.put(Lang.ESP, getWords("Longitud del brazo 10 (25 cm)"))
						.put(Lang.DEU, getWords("Armlänge 10 (25 cm)"))
						.put(Lang.FRA, getWords("Longueur de bras 10 (25 cm)")).build());
		translates
				.put(getCode("Non-porous semi-glossy surface for easy cleaning and sanitizing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непористая полуглянцевая поверхность для легкой очистки и дезинфекции"))
						.put(Lang.ITA, getWords("Superficie semi-lucida non porosa per una facile pulizia e sanificazione"))
						.put(Lang.ESP, getWords("Superficie semibrillante no porosa para facilitar la limpieza y la desinfección"))
						.put(Lang.DEU, getWords("Nicht poröse halbglänzende Oberfläche für einfache Reinigung und Desinfektion"))
						.put(Lang.FRA, getWords("Surface semi-brillante non poreuse pour un nettoyage et une désinfection faciles")).build());
		translates
				.put(getCode("Available in triple plated chrome or brushed nickel finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно в трехслойной хромированной или матовой никелевой отделке"))
						.put(Lang.ITA, getWords("Disponibile in tre finiture cromate o nichel spazzolato"))
						.put(Lang.ESP, getWords("Disponible en cromo plateado triple o acabado de níquel cepillado"))
						.put(Lang.DEU, getWords("Erhältlich in dreifach verchromter oder gebürsteter Nickel-Ausführung"))
						.put(Lang.FRA, getWords("Disponible en chrome plaqué triple ou nickel brossé")).build());
		translates
				.put(getCode("Wide and useful long rim designed to accommodate a variety of bath fillers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Широкий и полезный длинный обод, предназначенный для размещения различных банных наполнителей"))
						.put(Lang.ITA, getWords("Bordo lungo ampio e utile progettato per ospitare una varietà di riempitivi per il bagno"))
						.put(Lang.ESP, getWords("Borde largo ancho y útil diseñado para acomodar una variedad de rellenos de baño"))
						.put(Lang.DEU, getWords("Breite und nützliche lange Felge für eine Vielzahl von Badfüllern"))
						.put(Lang.FRA, getWords("Longue et large bordure utile conçue pour accueillir une variété de remplissages de bain")).build());
		translates
				.put(getCode("Avaliable in four colors: white, black, grey and sandstone"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступны в четырех цветах: белый, черный, серый и песчаник"))
						.put(Lang.ITA, getWords("Disponibile in quattro colori: bianco, nero, grigio e arenaria"))
						.put(Lang.ESP, getWords("Disponible en cuatro colores: blanco, negro, gris y arenisca"))
						.put(Lang.DEU, getWords("Erhältlich in vier Farben: Weiß, Schwarz, Grau und Sandstein"))
						.put(Lang.FRA, getWords("Disponible en quatre couleurs: blanc, noir, gris et grès")).build());
		translates
				.put(getCode("Extra deep, full-body soak"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра глубокое, полное погружение в тело"))
						.put(Lang.ITA, getWords("Imbevitura extra profonda per tutto il corpo"))
						.put(Lang.ESP, getWords("Remojo profundo y profundo en todo el cuerpo"))
						.put(Lang.DEU, getWords("Extra tiefes Ganzkörperbad"))
						.put(Lang.FRA, getWords("Trempage profond et complet du corps")).build());
		translates
				.put(getCode("Variable Speed massage with wave & pulse modes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Массаж с переменной скоростью с волновыми и импульсными режимами"))
						.put(Lang.ITA, getWords("Massaggio a velocità variabile con modalità onda e impulso"))
						.put(Lang.ESP, getWords("Masaje de velocidad variable con modos de onda y pulso"))
						.put(Lang.DEU, getWords("Variable Geschwindigkeitsmassage mit Wellen- und Pulsmodi"))
						.put(Lang.FRA, getWords("Massage à vitesse variable avec modes d'onde et d'impulsion")).build());
		translates
				.put(getCode("EcoMarmor™ cutting edge cast stone composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("EcoMarmor ™ режущий кромка"))
						.put(Lang.ITA, getWords("EcoMarmor ™ tagliente in pietra composita all'avanguardia"))
						.put(Lang.ESP, getWords("Compuesto de piedra de molde de vanguardia EcoMarmor ™"))
						.put(Lang.DEU, getWords("EcoMarmor ™ Schneidkanten-Verbundwerkstoff"))
						.put(Lang.FRA, getWords("EcoMarmor ™ composite de pierre coulée")).build());
		translates
				.put(getCode("Three individual temperature settings for the family"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Три индивидуальные настройки температуры для семьи"))
						.put(Lang.ITA, getWords("Tre impostazioni di temperatura individuali per la famiglia"))
						.put(Lang.ESP, getWords("Tres configuraciones de temperatura individuales para la familia"))
						.put(Lang.DEU, getWords("Drei individuelle Temperatureinstellungen für die Familie"))
						.put(Lang.FRA, getWords("Trois réglages de température individuels pour la famille")).build());
		translates
				.put(getCode("Optional trim kits available in many popular finishes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительные комплекты отделки доступны во многих популярных концах"))
						.put(Lang.ITA, getWords("Kit di finiture opzionali disponibili in molte finiture popolari"))
						.put(Lang.ESP, getWords("Kits de equipamiento opcionales disponibles en muchos acabados populares"))
						.put(Lang.DEU, getWords("Optionale Trimm-Kits in vielen beliebten Ausführungen erhältlich"))
						.put(Lang.FRA, getWords("Trousses de finition en option disponibles dans de nombreuses finitions populaires")).build());
		translates
				.put(getCode("Clean Scandinavian design with minimal detailing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чистый скандинавский дизайн с минимальной детализацией"))
						.put(Lang.ITA, getWords("Pulire il design scandinavo con dettagli minimi"))
						.put(Lang.ESP, getWords("Diseño escandinavo limpio con detalles mínimos"))
						.put(Lang.DEU, getWords("Sauberes skandinavisches Design mit minimalen Details"))
						.put(Lang.FRA, getWords("Design scandinave propre avec un minimum de détails")).build());
		translates
				.put(getCode("Low step-in design for physically challenged people"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Низкий ступенчатый дизайн для людей с физическими недостатками"))
						.put(Lang.ITA, getWords("Design step-in basso per persone disabili"))
						.put(Lang.ESP, getWords("Diseño de paso bajo para personas con discapacidades físicas"))
						.put(Lang.DEU, getWords("Low-Step-In-Design für körperlich behinderte Menschen"))
						.put(Lang.FRA, getWords("Conception à faible seuil pour les personnes à mobilité réduite")).build());
		translates
				.put(getCode("Comfortably fits two bathers that are 6ft or taller"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобно подходит для двух купальщиков размером 6 футов или выше"))
						.put(Lang.ITA, getWords("Comodamente si adatta a due bagnanti che sono 6 piedi o più alti"))
						.put(Lang.ESP, getWords("Se adapta cómodamente a dos bañistas que miden 6 pies o más"))
						.put(Lang.DEU, getWords("Bequem passt zwei Badegäste, die 6ft oder größer sind"))
						.put(Lang.FRA, getWords("Convient confortablement à deux baigneurs de 6 pieds ou plus")).build());
		translates
				.put(getCode("EcoMarmor™ Lite  cutting edge light-weight cast stone composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("EcoMarmor ™ Lite режущая кромка из легкого литого каменного композита"))
						.put(Lang.ITA, getWords("EcoMarmor ™ Lite in lastra di pietra fusa leggera e leggera"))
						.put(Lang.ESP, getWords("Compuesto de piedra fundida liviana de vanguardia EcoMarmor ™ Lite"))
						.put(Lang.DEU, getWords("EcoMarmor ™ Lite ist ein hochmoderner Leichtbetonstein"))
						.put(Lang.FRA, getWords("EcoMarmor ™ Lite composite de pierre coulée légère")).build());
		translates
				.put(getCode("Maximum Flow Rate: 6.87 GPM (26 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока: 6,87 GPM (26 LPM)"))
						.put(Lang.ITA, getWords("Portata massima: 6,87 GPM (26 LPM)"))
						.put(Lang.ESP, getWords("Caudal máximo: 6.87 GPM (26 LPM)"))
						.put(Lang.DEU, getWords("Maximale Durchflussrate: 6,87 GPM (26 LPM)"))
						.put(Lang.FRA, getWords("Débit maximum: 6,87 GPM (26 LPM)")).build());
		translates
				.put(getCode("Unique, sleek vessel-boat styled freestanding slipper bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальная, гладкая сухая тапочка с судовой лодкой"))
						.put(Lang.ITA, getWords("Vasca da bagno a slitta autoportante dal design unico ed elegante"))
						.put(Lang.ESP, getWords("Bañera deslizable independiente, elegante y con diseño de bote de barco"))
						.put(Lang.DEU, getWords("Einzigartige, schlanke, freistehende Badewanne im Bootsschiffstil"))
						.put(Lang.FRA, getWords("Baignoire autoportante unique et élégante de style bateau-bateau")).build());
		translates
				.put(getCode("False-ceiling mounted"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подвесной потолок"))
						.put(Lang.ITA, getWords("Falso soffitto montato"))
						.put(Lang.ESP, getWords("Falso techo montado"))
						.put(Lang.DEU, getWords("Zwischendecken montiert"))
						.put(Lang.FRA, getWords("Faux plafond monté")).build());
		translates
				.put(getCode("Does not include faucet and drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Не включает кран и слив"))
						.put(Lang.ITA, getWords("Non include rubinetto e scarico"))
						.put(Lang.ESP, getWords("No incluye grifo y drenaje"))
						.put(Lang.DEU, getWords("Enthält keinen Wasserhahn und Ablauf"))
						.put(Lang.FRA, getWords("Ne comprend pas le robinet et le drain")).build());
		translates
				.put(getCode("Integrated washbasin - 11 different dimensions available – at varying prices"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенный умывальник - доступно 11 различных размеров - по разным ценам"))
						.put(Lang.ITA, getWords("Lavabo integrato - 11 diverse dimensioni disponibili - a prezzi variabili"))
						.put(Lang.ESP, getWords("Lavabo integrado - 11 diferentes dimensiones disponibles - a diferentes precios"))
						.put(Lang.DEU, getWords("Integriertes Waschbecken - 11 verschiedene Abmessungen verfügbar - zu unterschiedlichen Preisen"))
						.put(Lang.FRA, getWords("Vasque intégrée - 11 différentes dimensions disponibles - à des prix variables")).build());
		translates
				.put(getCode("Classical design with wide and symmetrical rim - a reflection of opulence"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классический дизайн с широким и симметричным ободом - отражение богатства"))
						.put(Lang.ITA, getWords("Design classico con bordo ampio e simmetrico - un riflesso di opulenza"))
						.put(Lang.ESP, getWords("Diseño clásico con borde ancho y simétrico - un reflejo de la opulencia"))
						.put(Lang.DEU, getWords("Klassisches Design mit breitem und symmetrischem Rand - ein Spiegelbild von Opulenz"))
						.put(Lang.FRA, getWords("Design classique avec jante large et symétrique - un reflet de l'opulence")).build());
		translates
				.put(getCode("Feature cover-plate with bracket kit and a filter"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Специальная крышка с комплектом кронштейнов и фильтром"))
						.put(Lang.ITA, getWords("Feature piastra di copertura con kit staffa e filtro"))
						.put(Lang.ESP, getWords("Característica placa de cubierta con kit de soporte y un filtro"))
						.put(Lang.DEU, getWords("Feature Deckplatte mit Halterung Kit und einem Filter"))
						.put(Lang.FRA, getWords("Plaque de recouvrement avec kit de support et filtre")).build());
		translates
				.put(getCode("Clip handle with metallic strip trim"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Зажимная ручка с металлической стяжкой"))
						.put(Lang.ITA, getWords("Maniglia a clip con listello metallico"))
						.put(Lang.ESP, getWords("Manija del clip con el ajuste de la tira metálica"))
						.put(Lang.DEU, getWords("Clip-Griff mit Metallstreifen"))
						.put(Lang.FRA, getWords("Poignée à clip avec garniture à bande métallique")).build());
		translates
				.put(getCode("Simple, straight-forward operation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Простая, прямолинейная работа"))
						.put(Lang.ITA, getWords("Operazione semplice e diretta"))
						.put(Lang.ESP, getWords("Operación simple y directa"))
						.put(Lang.DEU, getWords("Einfache, unkomplizierte Bedienung"))
						.put(Lang.FRA, getWords("Opération simple et directe")).build());
	}

	public static void part7() {
		translates
				.put(getCode("Velvety texture which is warm and pleasant to the touch"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Бархатная текстура, теплая и приятная на ощупь"))
						.put(Lang.ITA, getWords("Texture vellutata che è calda e piacevole al tatto"))
						.put(Lang.ESP, getWords("Textura aterciopelada que es cálida y agradable al tacto"))
						.put(Lang.DEU, getWords("Samtige Textur, die sich warm und angenehm anfühlt"))
						.put(Lang.FRA, getWords("Texture veloutée qui est chaude et agréable au toucher")).build());
		translates
				.put(getCode("No color matching required upon repair"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Отсутствие соответствия цвета при ремонте"))
						.put(Lang.ITA, getWords("Nessuna corrispondenza di colore richiesta al momento della riparazione"))
						.put(Lang.ESP, getWords("No se requiere coincidencia de color en la reparación"))
						.put(Lang.DEU, getWords("Bei der Reparatur ist keine Farbanpassung erforderlich"))
						.put(Lang.FRA, getWords("Aucune correspondance de couleur requise lors de la réparation")).build());
		translates
				.put(getCode("Anti-lime nozzle system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система анти-извести"))
						.put(Lang.ITA, getWords("Sistema di ugelli anticalcare"))
						.put(Lang.ESP, getWords("Sistema de boquilla antical"))
						.put(Lang.DEU, getWords("Antikalk-Düsensystem"))
						.put(Lang.FRA, getWords("Système de buse anti-calcaire")).build());
		translates
				.put(getCode("2 Drain hole required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2 Требуется сливное отверстие"))
						.put(Lang.ITA, getWords("2 Vuoto foro richiesto"))
						.put(Lang.ESP, getWords("Se requiere un orificio de drenaje de 2 "))
						.put(Lang.DEU, getWords("2 Ablaufbohrung erforderlich"))
						.put(Lang.FRA, getWords("Trou de vidange de 2 requis")).build());
		translates
				.put(getCode("Hypoallergenic white solid surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная белая твердая поверхность"))
						.put(Lang.ITA, getWords("Superficie solida bianca ipoallergenica"))
						.put(Lang.ESP, getWords("Superficie sólida blanca hipoalergénica"))
						.put(Lang.DEU, getWords("Hypoallergene weiße feste Oberfläche"))
						.put(Lang.FRA, getWords("Surface solide blanche hypoallergénique")).build());
		translates
				.put(getCode("Frame Color: Brushed Gold - Cushion Color: White"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет рамки: Матовое золото - Подушка Цвет: Белый"))
						.put(Lang.ITA, getWords("Colore telaio: oro spazzolato - Colore cuscino: bianco"))
						.put(Lang.ESP, getWords("Color del marco: Brushed Gold - Cushion Color: White"))
						.put(Lang.DEU, getWords("Rahmenfarbe: Gebürstetes Gold - Kissenfarbe: Weiß"))
						.put(Lang.FRA, getWords("Couleur de la monture: Or brossé - Coussin Couleur: Blanc")).build());
		translates
				.put(getCode("Easy to clean - use the ScotchBright sponge and detergent, just like with your dishes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Легко чистить - используйте губку ScotchBright и моющее средство, как и ваши блюда"))
						.put(Lang.ITA, getWords("Facile da pulire: usa la spugna ScotchBright e il detergente, proprio come i tuoi piatti"))
						.put(Lang.ESP, getWords("Fácil de limpiar: use la esponja y el detergente ScotchBright, al igual que con sus platos"))
						.put(Lang.DEU, getWords("Einfach zu reinigen - verwenden Sie den ScotchBright Schwamm und das Reinigungsmittel, genau wie bei Ihrem Geschirr"))
						.put(Lang.FRA, getWords("Facile à nettoyer - utilisez l'éponge et le détergent ScotchBright, comme avec votre vaisselle")).build());
		translates
				.put(getCode("Progressive bidet mixer with 1 1/4 Up&Down waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прогрессивный смеситель для биде с 1 1/4 Up & Down waste"))
						.put(Lang.ITA, getWords("Miscelatore bidet progressivo con scarico Up & Down da 1 1/4 "))
						.put(Lang.ESP, getWords("Mezclador de bidet progresivo con descarga de 1 1/4 hacia arriba y hacia abajo"))
						.put(Lang.DEU, getWords("Progressive Bidetmischer mit 1 1/4 Up & Down Abfall"))
						.put(Lang.FRA, getWords("Mélangeur de bidet progressif avec 1 1/4 Up & Down")).build());
		translates
				.put(getCode("Freestanding circular bath with infinity-style rim"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Независимая круглая ванна с бесконечным ободом"))
						.put(Lang.ITA, getWords("Vasca circolare indipendente con bordo a sfioro"))
						.put(Lang.ESP, getWords("Bañera circular independiente con borde de estilo infinito"))
						.put(Lang.DEU, getWords("Freistehende kreisförmige Badewanne mit Rand im Infinity-Stil"))
						.put(Lang.FRA, getWords("Baignoire circulaire autoportante avec rebord à l'infini")).build());
		translates
				.put(getCode("Very comfortable, premium design corner bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень удобная, дизайнерская угловая ванна"))
						.put(Lang.ITA, getWords("Vasca da bagno angolare di design molto confortevole"))
						.put(Lang.ESP, getWords("Bañera de esquina de diseño premium muy cómoda"))
						.put(Lang.DEU, getWords("Sehr komfortable Eckbadewanne im Premium-Design"))
						.put(Lang.FRA, getWords("Très confortable, baignoire d'angle design de première qualité")).build());
		translates
				.put(getCode("The product is certified for the USA and Canadian market"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Продукт сертифицирован для рынка США и Канады"))
						.put(Lang.ITA, getWords("Il prodotto è certificato per il mercato statunitense e canadese"))
						.put(Lang.ESP, getWords("El producto está certificado para el mercado estadounidense y canadiense"))
						.put(Lang.DEU, getWords("Das Produkt ist für den US-amerikanischen und kanadischen Markt zertifiziert"))
						.put(Lang.FRA, getWords("Le produit est certifié pour le marché américain et canadien")).build());
		translates
				.put(getCode("Over 20 low-profile air jets and 20 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более 20 низкопрофильных воздушных струй и 20 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords("Oltre 20 getti d'aria a basso profilo e 20 LED a basso profilo"))
						.put(Lang.ESP, getWords("Más de 20 chorros de aire de perfil bajo y 20 LED de bajo perfil"))
						.put(Lang.DEU, getWords("Über 20 Low-Profile-Luftdüsen und 20 Low-Profile-LEDs"))
						.put(Lang.FRA, getWords("Plus de 20 jets d'air à profil bas et 20 LED à profil bas")).build());
		translates
				.put(getCode("Underwater ?hromotherapy with slow color rotation or fixed color mode in one of 6 available tones"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подводная хромотерапия с медленным вращением цвета или фиксированным цветовым режимом в одном из 6 доступных тонов"))
						.put(Lang.ITA, getWords("Cromoterapia subacquea con rotazione dei colori lenta o modalità a colori fissi in una delle 6 tonalità disponibili"))
						.put(Lang.ESP, getWords("Hromoterapia subacuática con rotación de color lenta o modo de color fijo en uno de los 6 tonos disponibles"))
						.put(Lang.DEU, getWords("Unterwassertherapie mit langsamer Farbrotation oder festem Farbmodus in einem von 6 verfügbaren Farbtönen"))
						.put(Lang.FRA, getWords("Hromothérapie sous-marine avec rotation lente des couleurs ou mode couleur fixe dans l'un des 6 tons disponibles")).build());
		translates
				.put(getCode("Digital shower control dimensions: 2.2 L x 0.5 W x 4.25 H (in) / 6 L x 1 W x 11 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цифровые размеры для управления душем: 2,2 л x 0,5 Вт х 4,25 Н (дюйм) / 6 л x 1 Вт x 11 Н (см)"))
						.put(Lang.ITA, getWords("Dimensioni della doccia digitale: 2,2 L x 0,5 W x 4,25 H (in) / 6 L x 1 W x 11 H (cm)"))
						.put(Lang.ESP, getWords("Dimensiones del control digital de la ducha: 2.2 L x 0.5 W x 4.25 H (in) / 6 L x 1 W x 11 H (cm)"))
						.put(Lang.DEU, getWords("Digitale Duschsteuerung Abmessungen: 2,2 L x 0,5 W x 4,25 H (Zoll) / 6 L x 1 W x 11 H (cm)"))
						.put(Lang.FRA, getWords("Dimensions de contrôle de douche numérique: 2,2 L x 0,5 W x 4,25 H (po) / 6 L x 1 L x 11 H (cm)")).build());
		translates
				.put(getCode("Coloured LED lights"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цветные светодиодные фонари"))
						.put(Lang.ITA, getWords("Luci a LED colorate"))
						.put(Lang.ESP, getWords("Luces LED de colores"))
						.put(Lang.DEU, getWords("Farbige LED-Leuchten"))
						.put(Lang.FRA, getWords("Lumières LED colorées")).build());
		translates
				.put(getCode("Solid wood pieced top"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Твердая древесина"))
						.put(Lang.ITA, getWords("Top in legno massello"))
						.put(Lang.ESP, getWords("Tapa de madera maciza"))
						.put(Lang.DEU, getWords("Massivholzplatte oben"))
						.put(Lang.FRA, getWords("Dessus en bois massif")).build());
		translates
				.put(getCode("Useful wider foot side rim for handy accessories like soap, shampoo, candles and even drinks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полезная более широкая задняя сторона обода для удобных аксессуаров, таких как мыло, шампунь, свечи и даже напитки"))
						.put(Lang.ITA, getWords("Bordo laterale utile più largo per accessori utili come sapone, shampoo, candele e persino bevande"))
						.put(Lang.ESP, getWords("Útil borde lateral más ancho para accesorios prácticos como jabón, champú, velas e incluso bebidas"))
						.put(Lang.DEU, getWords("Nützlicher breiter Fußrand für praktische Accessoires wie Seife, Shampoo, Kerzen und sogar Getränke"))
						.put(Lang.FRA, getWords("Jante latérale utile pour les accessoires pratiques comme le savon, le shampoing, les bougies et même les boissons")).build());
		translates
				.put(getCode("Clip handles with subtle trim"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Клипсы с тонкой отделкой"))
						.put(Lang.ITA, getWords("Maniglie a clip con finiture sottili"))
						.put(Lang.ESP, getWords("Manijas de clip con un ajuste sutil"))
						.put(Lang.DEU, getWords("Clip-Griffe mit dezenter Verzierung"))
						.put(Lang.FRA, getWords("Poignées à clip avec garniture subtile")).build());
		translates
				.put(getCode("100% recyclable and fire-resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("100% перерабатываемый и огнестойкий"))
						.put(Lang.ITA, getWords("100% riciclabile e resistente al fuoco"))
						.put(Lang.ESP, getWords("100% reciclable y resistente al fuego"))
						.put(Lang.DEU, getWords("100% recycelbar und feuerbeständig"))
						.put(Lang.FRA, getWords("100% recyclable et ignifuge")).build());
		translates
				.put(getCode("Chrome finished solid brass, made in Europe"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромированная цельная латунь, изготовленная в Европе"))
						.put(Lang.ITA, getWords("Ottone massiccio con finitura cromata, prodotto in Europa"))
						.put(Lang.ESP, getWords("Latón macizo acabado en cromo, fabricado en Europa"))
						.put(Lang.DEU, getWords("Chromiertes massives Messing, hergestellt in Europa"))
						.put(Lang.FRA, getWords("Chrome fini en laiton massif, fabriqué en Europe")).build());
		translates
				.put(getCode("Certified for Canadian and US markets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сертифицировано для рынков Канады и США"))
						.put(Lang.ITA, getWords("Certificato per i mercati canadese e statunitense"))
						.put(Lang.ESP, getWords("Certificado para los mercados canadiense y estadounidense"))
						.put(Lang.DEU, getWords("Zertifiziert für den kanadischen und US-amerikanischen Markt"))
						.put(Lang.FRA, getWords("Certifié pour les marchés canadiens et américains")).build());
		translates
				.put(getCode("Contemporary modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современный современный дизайн"))
						.put(Lang.ITA, getWords("Design moderno contemporaneo"))
						.put(Lang.ESP, getWords("Diseño moderno contemporáneo"))
						.put(Lang.DEU, getWords("Zeitgenössisches modernes Design"))
						.put(Lang.FRA, getWords("Design moderne et contemporain")).build());
		translates
				.put(getCode("Lucite exterior grade cast acrylic sheets with superior UV performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Литиевые литые акриловые листы Lucite с превосходной УФ-характеристикой"))
						.put(Lang.ITA, getWords("Lastre acriliche colate di qualità esterna Lucite con prestazioni UV superiori"))
						.put(Lang.ESP, getWords("Hojas acrílicas de fundición Lucite para exteriores con un rendimiento UV superior"))
						.put(Lang.DEU, getWords("Lucite Exterior Grade gegossen Acrylplatten mit überlegener UV-Leistung"))
						.put(Lang.FRA, getWords("Feuilles d'acrylique coulées de qualité extérieure Lucite avec des performances UV supérieures")).build());
		translates
				.put(getCode("Superb AquateX™ Matte material properties and tub thickness ensure fantastic heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Превосходные свойства материала AquateX ™ Matte и толщина ванны обеспечивают фантастическое удержание тепла"))
						.put(Lang.ITA, getWords("Le superbe proprietà del materiale AquateX ™ Matte e lo spessore della vasca assicurano una fantastica ritenzione di calore"))
						.put(Lang.ESP, getWords("Las excelentes propiedades del material AquateX ™ Matte y el espesor de la cubeta garantizan una fantástica retención de calor"))
						.put(Lang.DEU, getWords("Hervorragende AquateX ™ Matte Materialeigenschaften und Wannenstärke sorgen für eine hervorragende Wärmespeicherung"))
						.put(Lang.FRA, getWords("Les propriétés exceptionnelles des matériaux Matte AquateX ™ Matte et l'épaisseur de la baignoire assurent une rétention de la chaleur fantastique")).build());
		translates
				.put(getCode("“Auto” function (hygiene + dryer)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Функция «Авто» (гигиена + сушилка)"))
						.put(Lang.ITA, getWords("Funzione Auto (igiene + asciugatrice)"))
						.put(Lang.ESP, getWords("Función Auto (higiene + secadora)"))
						.put(Lang.DEU, getWords("Auto -Funktion (Hygiene + Trockner)"))
						.put(Lang.FRA, getWords("Fonction Auto (hygiène + sèche-linge)")).build());
		translates
				.put(getCode("Designed for two-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для купания двух человек"))
						.put(Lang.ITA, getWords("Progettato per il bagno di due persone"))
						.put(Lang.ESP, getWords("Diseñado para el baño de dos personas"))
						.put(Lang.DEU, getWords("Entworfen für das Baden mit zwei Personen"))
						.put(Lang.FRA, getWords("Conçu pour la baignade pour deux personnes")).build());
		translates
				.put(getCode("120V power with 2 x 15A GFCI protected circuits"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мощность 120 В с 2 x 15A защитными цепями GFCI"))
						.put(Lang.ITA, getWords("Potenza 120 V con 2 circuiti protetti GFCI da 15A"))
						.put(Lang.ESP, getWords("Potencia de 120 V con 2 circuitos protectores GFCI de 15 A"))
						.put(Lang.DEU, getWords("120-V-Strom mit 2 x 15 A GFCI-geschützten Stromkreisen"))
						.put(Lang.FRA, getWords("Alimentation 120 V avec 2 circuits protégés par GFCI de 2 x 15 A")).build());
		translates
				.put(getCode("Modern 100 x 100 x 17 cm (39.5 x 39.5 x 6.75 in) coffee table"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современный кофейный столик размером 100 х 100 х 17 см (39,5 х 39,5 х 6,75 дюйма)"))
						.put(Lang.ITA, getWords("Tavolino moderno 100 x 100 x 17 cm (39,5 x 39,5 x 6,75 pollici)"))
						.put(Lang.ESP, getWords("Mesa de centro moderna de 100 x 100 x 17 cm (39.5 x 39.5 x 6.75 in)"))
						.put(Lang.DEU, getWords("Moderner Couchtisch mit 100 x 100 x 17 cm (39,5 x 39,5 x 6,75 in)"))
						.put(Lang.FRA, getWords("Table basse moderne de 100 x 100 x 17 cm (39,5 x 39,5 x 6,75 po)")).build());
		translates
				.put(getCode("Rock solid design and sturdy construction with steel frame – will not wobble upon use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Скальный массивный дизайн и прочная конструкция со стальной рамой - не будут качаться при использовании"))
						.put(Lang.ITA, getWords("Design solido e robusta con telaio in acciaio - non oscillerà al momento dell'uso"))
						.put(Lang.ESP, getWords("Diseño sólido y sólida con estructura de acero: no se tambaleará con el uso"))
						.put(Lang.DEU, getWords("Rock solid Design und robuste Konstruktion mit Stahlrahmen - wackelt nicht bei der Verwendung"))
						.put(Lang.FRA, getWords("Conception robuste et robuste avec cadre en acier - ne chancelle pas lors de l'utilisation")).build());
		translates
				.put(getCode("Rainfall jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Осадки дождя"))
						.put(Lang.ITA, getWords("Getti di pioggia"))
						.put(Lang.ESP, getWords("Chorros de lluvia"))
						.put(Lang.DEU, getWords("Regenstrahlen"))
						.put(Lang.FRA, getWords("Jets de pluie")).build());
		translates
				.put(getCode("14 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("14 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords("14 LED a basso profilo"))
						.put(Lang.ESP, getWords("14 LED de bajo perfil"))
						.put(Lang.DEU, getWords("14 flache LEDs"))
						.put(Lang.FRA, getWords("14 LEDs à profil bas")).build());
		translates
				.put(getCode("110V power with 20A-dedicated GFCI protected circuit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мощность 110 В с защищаемой 20А цепью, защищенной GFCI"))
						.put(Lang.ITA, getWords("Potenza 110 V con circuito protetto GFCI dedicato 20A"))
						.put(Lang.ESP, getWords("Potencia de 110 V con circuito protegido por GFCI dedicado a 20A"))
						.put(Lang.DEU, getWords("110-V-Strom mit 20 A-dedizierter GFCI-Schutzschaltung"))
						.put(Lang.FRA, getWords("Alimentation 110 V avec circuit protégé par GFCI dédié de 20 A")).build());
		translates
				.put(getCode("Very small footprint with spacious interior"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень небольшая площадь с просторным интерьером"))
						.put(Lang.ITA, getWords("Ingombro molto ridotto con interni spaziosi"))
						.put(Lang.ESP, getWords("Huella muy pequeña con espacioso interior"))
						.put(Lang.DEU, getWords("Sehr kleine Standfläche mit geräumigem Innenraum"))
						.put(Lang.FRA, getWords("Très petite empreinte avec un intérieur spacieux")).build());
		translates
				.put(getCode("Easily repairable and restorable surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Легко ремонтируемая и восстанавливаемая поверхность"))
						.put(Lang.ITA, getWords("Superficie facilmente riparabile e ripristinabile"))
						.put(Lang.ESP, getWords("Superficie fácilmente reparable y restaurable"))
						.put(Lang.DEU, getWords("Leicht reparierbare und wiederherstellbare Oberfläche"))
						.put(Lang.FRA, getWords("Surface facilement réparable et réparable")).build());
		translates
				.put(getCode("Very deep bathtub designed for full body immersion"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень глубокая ванна, предназначенная для полного погружения в тело"))
						.put(Lang.ITA, getWords("Vasca molto profonda progettata per l'immersione completa del corpo"))
						.put(Lang.ESP, getWords("Bañera muy profunda diseñada para inmersión total del cuerpo"))
						.put(Lang.DEU, getWords("Sehr tiefe Badewanne zum Ganzkörpertauchen"))
						.put(Lang.FRA, getWords("Baignoire très profonde conçue pour une immersion complète dans le corps")).build());
		translates
				.put(getCode("Single piece 10.5 mm (0.25 in) thick matt white mineral/marble composite (1050 x 505 mm / 41.25 x 20 in)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовый белый минеральный / мраморный композит толщиной 10,5 мм (0,25 дюйма) (1050 x 505 мм / 41,25 x 20 дюймов)"))
						.put(Lang.ITA, getWords("Composito minerale / marmo bianco opaco monolitico da 10,5 mm (0,25 pollici) (1050 x 505 mm / 41,25 x 20 pollici)"))
						.put(Lang.ESP, getWords("Compuesto mineral / mármol blanco mate de 10,5 mm (0,25 pulg.) De una sola pieza (1050 x 505 mm / 41,25 x 20 in)"))
						.put(Lang.DEU, getWords("Einzelstück 10,5 mm dickes, mattweißes Mineral / Marmor-Komposit (1050 x 505 mm / 41,25 x 20 in)"))
						.put(Lang.FRA, getWords("Pièce unique en matériau minéral / marbre blanc mat de 10,5 mm (0,25 po) d'épaisseur (1050 x 505 mm / 41,25 x 20 po)")).build());
		translates
				.put(getCode("Unique antibacterial characteristics"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальные антибактериальные характеристики"))
						.put(Lang.ITA, getWords("Caratteristiche antibatteriche uniche"))
						.put(Lang.ESP, getWords("Características antibacterianas únicas"))
						.put(Lang.DEU, getWords("Einzigartige antibakterielle Eigenschaften"))
						.put(Lang.FRA, getWords("Caractéristiques antibactériennes uniques")).build());
		translates
				.put(getCode("Sleek, slimline rectangular styling – only 2.25 inches deep"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гладкий, тонкий прямоугольный стиль - всего 2,25 дюйма"))
						.put(Lang.ITA, getWords("Stile rettangolare sottile e sottile: solo 2,25 pollici di profondità"))
						.put(Lang.ESP, getWords("Diseño rectangular elegante y delgado: solo 2.25 pulgadas de profundidad"))
						.put(Lang.DEU, getWords("Schlankes, schlankes, rechteckiges Styling - nur 2,25 Zoll tief"))
						.put(Lang.FRA, getWords("Style rectangulaire épuré et mince - seulement 2,25 pouces de profondeur")).build());
		translates
				.put(getCode("Large square built-in shower head - 13.4x13.4 (34x34 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая встроенная душевая насадка - 13.4x13.4 (34x34 см)"))
						.put(Lang.ITA, getWords("Grande soffione a incasso quadrato - 13,4x13,4 (34x34 cm)"))
						.put(Lang.ESP, getWords("Cabezal de ducha cuadrado grande incorporado - 13.4x13.4 (34x34 cm)"))
						.put(Lang.DEU, getWords("Großer quadratischer integrierter Duschkopf - 34x34 cm (13.4x13.4 )"))
						.put(Lang.FRA, getWords("Grande pomme de douche encastrée carrée - 34 x 34 cm (13,4 x 13,4 po)")).build());
		translates
				.put(getCode("Wide and useful side rims for handy accessories like soap, shampoo, candles, and drinks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Широкие и полезные боковые диски для удобных аксессуаров, таких как мыло, шампунь, свечи и напитки"))
						.put(Lang.ITA, getWords("Cerchi laterali ampi e utili per accessori utili come sapone, shampoo, candele e bevande"))
						.put(Lang.ESP, getWords("Llantas laterales amplias y útiles para accesorios prácticos como jabón, champú, velas y bebidas"))
						.put(Lang.DEU, getWords("Breite und nützliche Seitenränder für handliches Zubehör wie Seife, Shampoo, Kerzen und Getränke"))
						.put(Lang.FRA, getWords("Jantes latérales larges et utiles pour des accessoires pratiques comme du savon, du shampoing, des bougies et des boissons")).build());
		translates
				.put(getCode("Stylish metal strip griff handle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стильная ручка с металлической лентой"))
						.put(Lang.ITA, getWords("Manico in elegante striscia di metallo"))
						.put(Lang.ESP, getWords("Elegante tira de griff con tira de metal"))
						.put(Lang.DEU, getWords("Stylischer Griff aus Metallgriff"))
						.put(Lang.FRA, getWords("Poignée griffée élégante en métal")).build());
		translates
				.put(getCode("Wall-mounted built-in waterfall shower - 9.25x5.5 (23.5x14 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный встроенный водопадный душ - 9,25x5,5 (23,5 x 14 см)"))
						.put(Lang.ITA, getWords("Doccia a cascata da incasso da parete - 9.25x5.5 (23.5x14 cm)"))
						.put(Lang.ESP, getWords("Ducha de cascada incorporada en la pared - 9.25x5.5 (23.5x14 cm)"))
						.put(Lang.DEU, getWords("Wand eingebaute Wasserfalldusche - 9.25x5.5 (23.5x14 cm)"))
						.put(Lang.FRA, getWords("Douche cascade encastrée murale - 9.25x5.5 (23.5x14 cm)")).build());
		translates
				.put(getCode("Anti-Lime qualities"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Анти-липовые качества"))
						.put(Lang.ITA, getWords("Qualità anti-calce"))
						.put(Lang.ESP, getWords("Calidades anti-cal"))
						.put(Lang.DEU, getWords("Antikalk-Qualitäten"))
						.put(Lang.FRA, getWords("Qualités anti-calcaire")).build());
		translates
				.put(getCode("220/240V power with 16A-dedicated RCD protected circuit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("220/240 В с защищенной схемой защиты от RCD с 16 А"))
						.put(Lang.ITA, getWords("Alimentazione 220 / 240V con circuito protetto RCD dedicato 16A"))
						.put(Lang.ESP, getWords("Potencia de 220/240 V con circuito protegido RCD dedicado de 16 A"))
						.put(Lang.DEU, getWords("220/240-V-Stromversorgung mit 16-A-dediziertem RCD-Schutzschaltkreis"))
						.put(Lang.FRA, getWords("Alimentation 220 / 240V avec circuit protégé par RCD dédié 16A")).build());
		translates
				.put(getCode("The 5th generation AquateX™ material provides excellent heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Материал AquateX ™ 5-го поколения обеспечивает отличное удержание тепла и долговечность"))
						.put(Lang.ITA, getWords("Il materiale AquateX ™ di quinta generazione offre un'eccellente ritenzione di calore e durata"))
						.put(Lang.ESP, getWords("La 5ª generación del material AquateX ™ proporciona una excelente retención de calor y durabilidad"))
						.put(Lang.DEU, getWords("Das AquateX ™ Material der 5. Generation bietet eine ausgezeichnete Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords("Le matériau AquateX ™ de 5ème génération offre une excellente rétention de la chaleur et une excellente durabilité")).build());
		translates
				.put(getCode("Standard plumbing connections"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандартные сантехнические соединения"))
						.put(Lang.ITA, getWords("Connessioni idrauliche standard"))
						.put(Lang.ESP, getWords("Conexiones de fontanería estándar"))
						.put(Lang.DEU, getWords("Standard-Wasseranschlüsse"))
						.put(Lang.FRA, getWords("Raccords de plomberie standard")).build());
		translates
				.put(getCode("Ultra-smooth velvety texture which is warm and pleasant to the touch"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ультра-гладкая бархатистая текстура, теплая и приятная на ощупь"))
						.put(Lang.ITA, getWords("Texture vellutata ultra-liscia, calda e piacevole al tatto"))
						.put(Lang.ESP, getWords("Textura aterciopelada ultra suave que es cálida y agradable al tacto"))
						.put(Lang.DEU, getWords("Ultra-glatte samtige Textur, die warm und angenehm zu berühren ist"))
						.put(Lang.FRA, getWords("Texture veloutée ultra-douce, chaleureuse et agréable au toucher")).build());
		translates
				.put(getCode("20 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("20 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords("20 LED a basso profilo"))
						.put(Lang.ESP, getWords("20 LED de bajo perfil"))
						.put(Lang.DEU, getWords("20 flache LEDs"))
						.put(Lang.FRA, getWords("20 LEDs à profil bas")).build());
		translates
				.put(getCode("28 jets and 12 air nozzles in the spa"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("28 струй и 12 воздушных сопел в спа-салоне"))
						.put(Lang.ITA, getWords("28 getti e 12 ugelli d'aria nella spa"))
						.put(Lang.ESP, getWords("28 jets y 12 boquillas de aire en el spa"))
						.put(Lang.DEU, getWords("28 Düsen und 12 Luftdüsen im Spa"))
						.put(Lang.FRA, getWords("28 jets et 12 buses d'air dans le spa")).build());
		translates
				.put(getCode("Warm air dryer with variable temperature setting (5 levels)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Термовоздушная сушилка с переменной температурой (5 уровней)"))
						.put(Lang.ITA, getWords("Essiccatore ad aria calda a temperatura variabile (5 livelli)"))
						.put(Lang.ESP, getWords("Secador de aire caliente con ajuste de temperatura variable (5 niveles)"))
						.put(Lang.DEU, getWords("Warmlufttrockner mit variabler Temperatureinstellung (5 Stufen)"))
						.put(Lang.FRA, getWords("Sécheur à air chaud avec réglage de température variable (5 niveaux)")).build());
		translates
				.put(getCode("Seat and back are supported by polyester belts specifically formulated for outdoor use."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сиденье и спинка поддерживаются полиэфирными лентами, специально разработанными для использования на открытом воздухе."))
						.put(Lang.ITA, getWords("Sedile e schienale sono supportati da cinghie in poliestere appositamente formulate per uso esterno."))
						.put(Lang.ESP, getWords("Asiento y respaldo son compatibles con cinturones de poliéster específicamente formulados para uso en exteriores."))
						.put(Lang.DEU, getWords("Sitz und Rücken werden von speziell für den Außenbereich entwickelten Polyestergurten unterstützt."))
						.put(Lang.FRA, getWords("L'assise et le dossier sont soutenus par des ceintures en polyester spécialement conçues pour une utilisation en extérieur.")).build());
		translates
				.put(getCode("Chrome finished solid brass"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромированная твердая латунь"))
						.put(Lang.ITA, getWords("Ottone massiccio rifinito in cromo"))
						.put(Lang.ESP, getWords("Acabado en latón macizo"))
						.put(Lang.DEU, getWords("Verchromtes Messing"))
						.put(Lang.FRA, getWords("Chrome fini en laiton massif")).build());
		translates
				.put(getCode("Two removable side panels for easy installation and servicing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Две съемные боковые панели для простой установки и обслуживания"))
						.put(Lang.ITA, getWords("Due pannelli laterali rimovibili per una facile installazione e manutenzione"))
						.put(Lang.ESP, getWords("Dos paneles laterales extraíbles para una fácil instalación y servicio"))
						.put(Lang.DEU, getWords("Zwei abnehmbare Seitenwände für einfache Installation und Wartung"))
						.put(Lang.FRA, getWords("Deux panneaux latéraux amovibles pour une installation et un entretien faciles")).build());
		translates
				.put(getCode("Absolute comfort with 2 included soft gel headrests"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Абсолютный комфорт с 2 включенными мягкими гель-подголовниками"))
						.put(Lang.ITA, getWords("Comfort assoluto con 2 poggiatesta in gel morbido inclusi"))
						.put(Lang.ESP, getWords("Comodidad absoluta con 2 reposacabezas de gel suave incluidos"))
						.put(Lang.DEU, getWords("Absoluter Komfort mit 2 mitgelieferten Soft-Gel Kopfstützen"))
						.put(Lang.FRA, getWords("Confort absolu avec 2 appuie-tête en gel souple inclus")).build());
		translates
				.put(getCode("Maximum water capacity - 2500 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 2500 литров"))
						.put(Lang.ITA, getWords("Capacità massima dell'acqua - 2500 litri"))
						.put(Lang.ESP, getWords("Capacidad máxima de agua - 2500 Litros"))
						.put(Lang.DEU, getWords("Maximale Wasserkapazität - 2500 Liter"))
						.put(Lang.FRA, getWords("Capacité maximale d'eau - 2500 litres")).build());
		translates
				.put(getCode("Ergonomic back/foot rest design – recommended for people with back problems"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичный дизайн спины / ног - рекомендуется для людей с проблемами спины"))
						.put(Lang.ITA, getWords("Design ergonomico della schiena / poggiapiedi - consigliato per le persone con problemi alla schiena"))
						.put(Lang.ESP, getWords("Diseño ergonómico de respaldo / reposapiés: recomendado para personas con problemas de espalda"))
						.put(Lang.DEU, getWords("Ergonomische Rücken- / Fußstütze Design - empfohlen für Menschen mit Rückenproblemen"))
						.put(Lang.FRA, getWords("Conception ergonomique de dossier / repose-pieds - recommandé pour les personnes ayant des problèmes de dos")).build());
		translates
				.put(getCode("2 year overall limited air massage warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя ограниченная воздушная массажная гарантия"))
						.put(Lang.ITA, getWords("Garanzia di 2 anni per un massaggio d'aria limitato"))
						.put(Lang.ESP, getWords("Garantía de masaje de aire limitado general de 2 años"))
						.put(Lang.DEU, getWords("2 Jahre eingeschränkte Luftmassage Garantie"))
						.put(Lang.FRA, getWords("Garantie limitée de 2 ans sur le massage de l'air")).build());
		translates
				.put(getCode("Bluetooth protocol support: A2DP/AVRCP revision 4.1"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поддержка протокола Bluetooth: версия A2DP / AVRCP 4.1"))
						.put(Lang.ITA, getWords("Supporto del protocollo Bluetooth: revisione A2DP / AVRCP 4.1"))
						.put(Lang.ESP, getWords("Compatibilidad con el protocolo Bluetooth: revisión A2DP / AVRCP 4.1"))
						.put(Lang.DEU, getWords("Bluetooth Protokoll Unterstützung: A2DP / AVRCP Revision 4.1"))
						.put(Lang.FRA, getWords("Prise en charge du protocole Bluetooth: révision A2DP / AVRCP 4.1")).build());
		translates
				.put(getCode("Choice of fabrics also to match your set – mélange grey, white beige, white or dark grey"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Выбор тканей также соответствует вашему набору - серый серый, белый бежевый, белый или темно-серый"))
						.put(Lang.ITA, getWords("Scelta di tessuti anche per abbinarli al tuo set: grigio mélange, beige bianco, bianco o grigio scuro"))
						.put(Lang.ESP, getWords("Elección de los tejidos también para combinar con su conjunto: gris mélange, beige blanco, gris blanco o gris oscuro"))
						.put(Lang.DEU, getWords("Auswahl an Stoffen auch passend zu Ihrem Set - Melange Grau, Weiß Beige, Weiß oder Dunkelgrau"))
						.put(Lang.FRA, getWords("Choix de tissus également pour assortir votre ensemble - mélange gris, blanc beige, blanc ou gris foncé")).build());
		translates
				.put(getCode("AquateX™ Matte material provides for excellent heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовый материал AquateX ™ обеспечивает отличную удержание и долговечность тепла"))
						.put(Lang.ITA, getWords("Il materiale AquateX ™ Matte garantisce un'eccellente ritenzione di calore e durata"))
						.put(Lang.ESP, getWords("El material AquateX ™ mate proporciona una excelente retención de calor y durabilidad"))
						.put(Lang.DEU, getWords("AquateX ™ Matte Material sorgt für hervorragende Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords("Le matériau AquateX ™ Matte offre une excellente rétention de la chaleur et une excellente durabilité")).build());
		translates
				.put(getCode("Drop-in design, freestanding construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Конструкция с раздельной конструкцией, автономная конструкция"))
						.put(Lang.ITA, getWords("Design drop-in, costruzione indipendente"))
						.put(Lang.ESP, getWords("Diseño empotrado, construcción independiente"))
						.put(Lang.DEU, getWords("Drop-in-Design, freistehende Konstruktion"))
						.put(Lang.FRA, getWords("Conception Drop-in, construction autoportante")).build());
		translates
				.put(getCode("Aesthetically pleasing and relaxing in use, the micro-LED ring is low power consumption."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эстетически приятное и расслабляющее использование, микро-светодиодное кольцо имеет низкое энергопотребление."))
						.put(Lang.ITA, getWords("Esteticamente piacevole e rilassante in uso, l'anello micro-LED è a basso consumo energetico."))
						.put(Lang.ESP, getWords("Estéticamente agradable y relajante en el uso, el anillo micro-LED es de bajo consumo de energía."))
						.put(Lang.DEU, getWords("Der Micro-LED-Ring ist ästhetisch ansprechend und entspannend im Gebrauch."))
						.put(Lang.FRA, getWords("Esthétiquement agréable et relaxant en cours d'utilisation, l'anneau micro-LED est faible consommation d'énergie.")).build());
		translates
				.put(getCode("A matching cable diameter is needed to sustain the above current rating"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Для поддержания вышеуказанного номинального тока необходим соответствующий диаметр кабеля"))
						.put(Lang.ITA, getWords("È necessario un diametro del cavo corrispondente per sostenere la corrente nominale sopra riportata"))
						.put(Lang.ESP, getWords("Se necesita un diámetro de cable correspondiente para mantener la calificación actual anterior"))
						.put(Lang.DEU, getWords("Ein passender Kabeldurchmesser ist erforderlich, um die obige Stromstärke zu erhalten"))
						.put(Lang.FRA, getWords("Un diamètre de câble correspondant est nécessaire pour maintenir la cote actuelle ci-dessus")).build());
		translates
				.put(getCode("Hygienic - inert, hypoallergenic and non-toxic material"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гигиенично-инертный, гипоаллергенный и нетоксичный материал"))
						.put(Lang.ITA, getWords("Igienico: materiale inerte, ipoallergenico e non tossico"))
						.put(Lang.ESP, getWords("Higiénico - material inerte, hipoalergénico y no tóxico"))
						.put(Lang.DEU, getWords("Hygienisch - inert, hypoallergen und nicht toxisch"))
						.put(Lang.FRA, getWords("Hygiénique - matériau inerte, hypoallergénique et non toxique")).build());
		translates
				.put(getCode("Scratchproof and resistant to abrasion"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Устойчивость к царапинам и стойкость к истиранию"))
						.put(Lang.ITA, getWords("Antigraffio e resistente all'abrasione"))
						.put(Lang.ESP, getWords("A prueba de arañazos y resistente a la abrasión"))
						.put(Lang.DEU, getWords("Kratzfest und abriebfest"))
						.put(Lang.FRA, getWords("Résistant aux rayures et résistant à l'abrasion")).build());
		translates
				.put(getCode("Intuitive wheel controls and adjustable hand shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Интуитивное управление колесами и регулируемый ручной душ"))
						.put(Lang.ITA, getWords("Comandi ruota intuitivi e doccetta regolabile"))
						.put(Lang.ESP, getWords("Controles de ruedas intuitivos y ducha de mano ajustable"))
						.put(Lang.DEU, getWords("Intuitive Radsteuerung und verstellbare Handbrause"))
						.put(Lang.FRA, getWords("Commandes de roues intuitives et douchette à main réglable")).build());
		translates
				.put(getCode("Robust, hardwearing brass construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочная, износостойкая латунная конструкция"))
						.put(Lang.ITA, getWords("Costruzione in ottone robusta e resistente"))
						.put(Lang.ESP, getWords("Construcción de bronce resistente y resistente"))
						.put(Lang.DEU, getWords("Robuste, strapazierfähige Messingkonstruktion"))
						.put(Lang.FRA, getWords("Construction en laiton robuste et résistante")).build());
		translates
				.put(getCode("3-way diverter valve to control water operations"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3-ходовой отводной клапан для управления работой воды"))
						.put(Lang.ITA, getWords("Valvola deviatrice a 3 vie per il controllo delle operazioni idrauliche"))
						.put(Lang.ESP, getWords("Válvula de desvío de 3 vías para controlar las operaciones de agua"))
						.put(Lang.DEU, getWords("3-Wege-Umschaltventil zur Steuerung des Wasserbetriebs"))
						.put(Lang.FRA, getWords("Vanne de dérivation à trois voies pour contrôler les opérations de l'eau")).build());
		translates
				.put(getCode("Ultra modern, minimalist style in chrome finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ультра современный, минималистский стиль в хромированной отделке"))
						.put(Lang.ITA, getWords("Stile ultra moderno e minimalista in finitura cromata"))
						.put(Lang.ESP, getWords("Estilo ultra moderno y minimalista en acabado cromado"))
						.put(Lang.DEU, getWords("Ultra moderner, minimalistischer Stil in Chrom-Finish"))
						.put(Lang.FRA, getWords("Ultra moderne, style minimaliste en finition chromée")).build());
		translates
				.put(getCode("Designed by Marco Acerbis"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Разработано Marco Acerbis"))
						.put(Lang.ITA, getWords("Progettato da Marco Acerbis"))
						.put(Lang.ESP, getWords("Diseñado por Marco Acerbis"))
						.put(Lang.DEU, getWords("Entworfen von Marco Acerbis"))
						.put(Lang.FRA, getWords("Conçu par Marco Acerbis")).build());
		translates
				.put(getCode("Anti-lime top-mounted round showerhead of modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Анти-липовый верхний круглый душ из современного дизайна"))
						.put(Lang.ITA, getWords("Soffione tondo rotondo anticalcare con design moderno"))
						.put(Lang.ESP, getWords("Cabezal de ducha redondo montado en la parte superior antical con diseño moderno"))
						.put(Lang.DEU, getWords("Anti-Kalk Aufsatz Duschkopf von modernem Design"))
						.put(Lang.FRA, getWords("Pomme de douche ronde anti-calcaire montée sur le dessus de conception moderne")).build());
		translates
				.put(getCode("Super compact design intended to fit spaces where no other tub would fit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Супер компактный дизайн, предназначенный для размещения пространств, где никакая другая ванна не подходит"))
						.put(Lang.ITA, getWords("Design ultracompatto progettato per adattarsi a spazi dove nessun'altra vasca si adattava"))
						.put(Lang.ESP, getWords("Diseño súper compacto destinado a espacios donde no cabía ninguna otra bañera"))
						.put(Lang.DEU, getWords("Super kompaktes Design für Räume, in die keine andere Wanne passen würde"))
						.put(Lang.FRA, getWords("Conception super compacte conçue pour s'adapter à des espaces où aucune autre baignoire ne rentre")).build());
		translates
				.put(getCode("Contemporary, one-of-a-kind design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современный, единственный в своем роде дизайн"))
						.put(Lang.ITA, getWords("Design contemporaneo e unico nel suo genere"))
						.put(Lang.ESP, getWords("Diseño contemporáneo, único en su tipo"))
						.put(Lang.DEU, getWords("Zeitgenössisches, einzigartiges Design"))
						.put(Lang.FRA, getWords("Design contemporain et unique en son genre")).build());
		translates
				.put(getCode("Fingertip control of mixer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Управление конвейером микшера"))
						.put(Lang.ITA, getWords("Controllo a portata di mano del mixer"))
						.put(Lang.ESP, getWords("Control de la mano del mezclador"))
						.put(Lang.DEU, getWords("Fingerspitzensteuerung des Mixers"))
						.put(Lang.FRA, getWords("Contrôle du bout des doigts du mélangeur")).build());
		translates
				.put(getCode("n°1 woofer 135 mm"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("n ° 1 вуфер 135 мм"))
						.put(Lang.ITA, getWords("n ° 1 woofer 135 mm"))
						.put(Lang.ESP, getWords("n ° 1 woofer 135 mm"))
						.put(Lang.DEU, getWords("Nr. 1 Woofer 135 mm"))
						.put(Lang.FRA, getWords("n ° 1 woofer 135 mm")).build());
		translates
				.put(getCode("Boasts all of the perks of the full-size Sensuality, but in a petite, space-conscious design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Обладает всеми преимуществами полноразмерной чувственности, но в маленьком, космическом сознании"))
						.put(Lang.ITA, getWords("Vanta tutti i vantaggi della Sensuality full-size, ma con un design minuzioso e attento allo spazio"))
						.put(Lang.ESP, getWords("Se jacta de todas las ventajas de la Sensualidad de tamaño completo, pero en un diseño pequeño, consciente del espacio"))
						.put(Lang.DEU, getWords("Verfügt über alle Vorteile der lebensgroßen Sinnlichkeit, aber in einem kleinen, raumbewussten Design"))
						.put(Lang.FRA, getWords("Bénéficie de tous les avantages de la Sensuality pleine grandeur, mais dans un design petit, espace-conscient")).build());
		translates
				.put(getCode("Wall-mounted at maximum of 1.5 metres to th the floor"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный на высоте не более 1,5 метров до пола"))
						.put(Lang.ITA, getWords("Montaggio a parete ad un massimo di 1,5 metri dal pavimento"))
						.put(Lang.ESP, getWords("Montado en la pared a un máximo de 1.5 metros hasta el piso"))
						.put(Lang.DEU, getWords("Wandmontage bei maximal 1,5 Metern zum Boden"))
						.put(Lang.FRA, getWords("Mural à un maximum de 1,5 mètre du sol")).build());
		translates
				.put(getCode("Water flow rate - 4.75 GPM (18 LPM) with 3 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("«Расход воды - 4,75 GPM (18 LPM) с давлением воды 3 бар"))
						.put(Lang.ITA, getWords("Portata d'acqua - 4,75 GPM (18 LPM) con pressione dell'acqua di 3 bar"))
						.put(Lang.ESP, getWords("Caudal de agua: 4.75 GPM (18 LPM) con 3 bares de presión de agua"))
						.put(Lang.DEU, getWords("Wasserflussrate - 4,75 GPM (18 LPM) mit 3 bar Wasserdruck"))
						.put(Lang.FRA, getWords("Débit d'eau - 4,75 GPM (18 LPM) avec une pression d'eau de 3 bars")).build());
		translates
				.put(getCode("Operating temperature range: -10°C to +60°C"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Диапазон рабочих температур: от -10 ° C до + 60 ° C"))
						.put(Lang.ITA, getWords("Campo di temperatura di funzionamento: da -10 ° C a + 60 ° C"))
						.put(Lang.ESP, getWords("Rango de temperatura de funcionamiento: -10 ° C a + 60 ° C"))
						.put(Lang.DEU, getWords("Betriebstemperaturbereich: -10 ° C bis + 60 ° C"))
						.put(Lang.FRA, getWords("Plage de température de fonctionnement: -10 ° C à + 60 ° C")).build());
		translates
				.put(getCode("Quick dry foam in all cushions"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Быстрая сухая пена во всех подушках"))
						.put(Lang.ITA, getWords("Schiuma rapida e asciutta su tutti i cuscini"))
						.put(Lang.ESP, getWords("Espuma de secado rápido en todos los cojines"))
						.put(Lang.DEU, getWords("Schnell trockener Schaum in allen Kissen"))
						.put(Lang.FRA, getWords("Mousse à séchage rapide dans tous les coussins")).build());
		translates
				.put(getCode("Foam insulation for improved heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изоляция пены для улучшения удержания тепла"))
						.put(Lang.ITA, getWords("Isolamento in schiuma per una migliore ritenzione del calore"))
						.put(Lang.ESP, getWords("Aislamiento de espuma para una mejor retención de calor"))
						.put(Lang.DEU, getWords("Schaumisolierung für verbesserte Wärmespeicherung"))
						.put(Lang.FRA, getWords("Isolation en mousse pour une meilleure rétention de la chaleur")).build());
		translates
				.put(getCode("Kids function"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Детская функция"))
						.put(Lang.ITA, getWords("Funzione per bambini"))
						.put(Lang.ESP, getWords("Función de niños"))
						.put(Lang.DEU, getWords("Kinder funktionieren"))
						.put(Lang.FRA, getWords("Fonction enfants")).build());
		translates
				.put(getCode("Oxide console with built-in sink - 1805 x 505 x 130 mm / 71 x 20 x 5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Оксидная консоль со встроенной раковиной - 1805 x 505 x 130 мм / 71 x 20 x 5 дюймов"))
						.put(Lang.ITA, getWords("Consolle di ossido con lavello incorporato - 1805 x 505 x 130 mm / 71 x 20 x 5 pollici"))
						.put(Lang.ESP, getWords("Consola de óxido con fregadero incorporado - 1805 x 505 x 130 mm / 71 x 20 x 5 in"))
						.put(Lang.DEU, getWords("Oxid Konsole mit eingebauter Spüle - 1805 x 505 x 130 mm / 71 x 20 x 5 in"))
						.put(Lang.FRA, getWords("Console d'oxyde avec évier intégré - 1805 x 505 x 130 mm / 71 x 20 x 5 po")).build());
		translates
				.put(getCode("Wall-mount sleek interface with high quality brass control wheel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный гладкий интерфейс с высококачественным латунным колесом управления"))
						.put(Lang.ITA, getWords("Interfaccia elegante montata a parete con rotella di controllo in ottone di alta qualità"))
						.put(Lang.ESP, getWords("Interfaz elegante montada en la pared con rueda de control de latón de alta calidad"))
						.put(Lang.DEU, getWords("Schnittstelle zur Wandmontage mit hochwertigem Messing-Einstellrad"))
						.put(Lang.FRA, getWords("Interface élégante murale avec volant de commande en laiton de haute qualité")).build());
		translates
				.put(getCode("2 Year Limited Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords(" Ограниченная гарантия на 2 года"))
						.put(Lang.ITA, getWords("Garanzia limitata di 2 anni"))
						.put(Lang.ESP, getWords("Garantía limitada de 2 años"))
						.put(Lang.DEU, getWords("2 Jahre beschränkte Garantie"))
						.put(Lang.FRA, getWords("Garantie limitée de 2 ans")).build());
		translates
				.put(getCode("Designed to coordinate with a selection of Aquatica's AquateX™ sinks: Spoon 2, Coletta, Dante"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для координации с выбором Aquatica's AquateX ™: Ложка 2, Колетта, Данте"))
						.put(Lang.ITA, getWords("Progettato per coordinarsi con una selezione di lavelli AquateX ™ di Aquatica: Spoon 2, Coletta, Dante"))
						.put(Lang.ESP, getWords("Diseñado para coordinar con una selección de fregaderos AquateX ™ de Aquatica: Spoon 2, Coletta, Dante"))
						.put(Lang.DEU, getWords("Zur Abstimmung mit einer Auswahl von Aquatica AquateX ™ Spülen: Spoon 2, Coletta, Dante"))
						.put(Lang.FRA, getWords("Conçu pour se coordonner avec une sélection de lavabos AquateX ™ d'Aquatica: Spoon 2, Coletta, Dante")).build());
		translates
				.put(getCode("Durable semi-gloss synthetic resin coating for natural stone like look"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочное полуглянцевое синтетическое полимерное покрытие для натурального камня, похожее на внешний вид"))
						.put(Lang.ITA, getWords("Rivestimento in resina sintetica semilucida resistente per un aspetto simile alla pietra naturale"))
						.put(Lang.ESP, getWords("Revestimiento de resina sintética duradera semibrillante para piedra natural"))
						.put(Lang.DEU, getWords("Haltbare halbglänzende Kunstharzbeschichtung für Natursteinoptik"))
						.put(Lang.FRA, getWords("Revêtement en résine synthétique semi-brillante durable pour une apparence de pierre naturelle")).build());
		translates
				.put(getCode("Standard 19.5 ft cable length distance between control mixer & controller"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандартное расстояние длины кабеля 19,5 футов между контроллером и контроллером"))
						.put(Lang.ITA, getWords("Lunghezza standard del cavo di 19,5 ft tra il mixer di controllo e il controller"))
						.put(Lang.ESP, getWords("Distancia de longitud de cable estándar de 19.5 pies entre el mezclador de control y el controlador"))
						.put(Lang.DEU, getWords("Standard 19,5 ft Kabellänge Abstand zwischen Steuerung Mixer und Controller"))
						.put(Lang.FRA, getWords("Distance de câble standard de 19,5 pi entre le mélangeur de contrôle et le contrôleur")).build());
		translates
				.put(getCode("Beige cushions with Iroko wood frame and arms"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Бежевые подушки с деревянной рамой и рукавами Ироко"))
						.put(Lang.ITA, getWords("Cuscini beige con struttura e braccioli in legno Iroko"))
						.put(Lang.ESP, getWords("Cojines beige con marco de madera Iroko y brazos"))
						.put(Lang.DEU, getWords("Beige Kissen mit Iroko Holzrahmen und Armen"))
						.put(Lang.FRA, getWords("Coussins beige avec armature en bois d'Iroko et bras")).build());
		translates
				.put(getCode("Large 15.7x15.7 (400x400 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 15,7x15,7 (400x400 мм) душевая насадка"))
						.put(Lang.ITA, getWords("Soffione grande 15.7x15.7 (400x400 mm)"))
						.put(Lang.ESP, getWords("Cabezal de ducha grande de 15.7x15.7 (400x400 mm)"))
						.put(Lang.DEU, getWords("Großer 15,7x15,7 (400x400 mm) Duschkopf"))
						.put(Lang.FRA, getWords("Grande pomme de douche de 15,7 x 15,7 po (400 x 400 mm)")).build());
		translates
				.put(getCode("Fingertip control of water temperature and flow through separate taps"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Fingertip контроль температуры и расхода воды через отдельные отводы"))
						.put(Lang.ITA, getWords("Controllo a portata di mano della temperatura dell'acqua e flusso attraverso rubinetti separati"))
						.put(Lang.ESP, getWords("Control de la temperatura del agua y el flujo a través de grifos separados"))
						.put(Lang.DEU, getWords("Fingertip Kontrolle der Wassertemperatur und Durchfluss durch separate Wasserhähne"))
						.put(Lang.FRA, getWords("Contrôle du bout des doigts de la température et de l'écoulement de l'eau à travers des robinets séparés")).build());
		translates
				.put(getCode("5 Year Limited Warranty on the motor and the pump"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия 5 лет на двигатель и насос"))
						.put(Lang.ITA, getWords("Garanzia limitata di 5 anni sul motore e sulla pompa"))
						.put(Lang.ESP, getWords("Garantía limitada de 5 años en el motor y la bomba"))
						.put(Lang.DEU, getWords("5 Jahre eingeschränkte Garantie auf den Motor und die Pumpe"))
						.put(Lang.FRA, getWords("Garantie limitée de 5 ans sur le moteur et la pompe")).build());
		translates
				.put(getCode("2 waterfalls in the spa"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2 водопада в спа-салоне"))
						.put(Lang.ITA, getWords("2 cascate nella spa"))
						.put(Lang.ESP, getWords("2 cascadas en el spa"))
						.put(Lang.DEU, getWords("2 Wasserfälle im Spa"))
						.put(Lang.FRA, getWords("2 cascades dans le spa")).build());
		translates
				.put(getCode("220/240V 50Hz or 60Hz AC power with 25A RCD (GFCI) protected fuse"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("220/240 В 50 Гц или 60 Гц переменного тока с защищенным предохранителем 25 А RCD (GFCI)"))
						.put(Lang.ITA, getWords("Alimentazione CA 220 / 240V 50Hz o 60Hz con fusibile protetto da 25 A RCD (GFCI)"))
						.put(Lang.ESP, getWords("220 / 240V 50Hz o 60Hz de alimentación de CA con fusible protegido a 25 A RCD (GFCI)"))
						.put(Lang.DEU, getWords("220 / 240V 50Hz oder 60Hz Wechselstrom mit 25A RCD (GFCI) Sicherung"))
						.put(Lang.FRA, getWords("Alimentation 220 / 240V 50Hz ou 60Hz avec un fusible protégé par un RCD 25 A (GFCI)")).build());
		translates
				.put(getCode("Brethable fabric"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подвижная ткань"))
						.put(Lang.ITA, getWords("Tessuto brethable"))
						.put(Lang.ESP, getWords("Tela brethable"))
						.put(Lang.DEU, getWords("Breathable Gewebe"))
						.put(Lang.FRA, getWords("Tissu Brethable")).build());
		translates
				.put(getCode("Spa with double insulation for saving energy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Спа с двойной изоляцией для экономии энергии"))
						.put(Lang.ITA, getWords("Spa con doppio isolamento per risparmiare energia"))
						.put(Lang.ESP, getWords("Spa con doble aislamiento para ahorrar energía"))
						.put(Lang.DEU, getWords("Spa mit Doppelisolierung zum Energiesparen"))
						.put(Lang.FRA, getWords("Spa avec double isolation pour économiser l'énergie")).build());
		translates
				.put(getCode("Designed for one or two-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для купания одного или двух человек"))
						.put(Lang.ITA, getWords("Progettato per la balneazione di una o due persone"))
						.put(Lang.ESP, getWords("Diseñado para baño de una o dos personas"))
						.put(Lang.DEU, getWords("Entworfen für das Baden mit einer oder zwei Personen"))
						.put(Lang.FRA, getWords("Conçu pour la baignade pour une ou deux personnes")).build());
		translates
				.put(getCode("Compact and space saving walk-in bathtub intended to fit spaces where no other bathtub would ever fit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Компактная и компактная ванна для ванны, предназначенная для размещения помещений, где никакая другая ванна никогда не будет"))
						.put(Lang.ITA, getWords("Vasca da bagno compatta e salvaspazio progettata per adattarsi a spazi dove nessun'altra vasca si adattava mai"))
						.put(Lang.ESP, getWords("Bañera compacta y que ahorra espacio, pensada para adaptarse a espacios donde ninguna otra bañera cabría"))
						.put(Lang.DEU, getWords("Kompakte und platzsparende begehbare Badewanne für Räume, in die keine andere Badewanne passen würde"))
						.put(Lang.FRA, getWords("Baignoire encastrée compacte et peu encombrante conçue pour s'adapter à des espaces où aucune autre baignoire ne serait adaptée")).build());
		translates
				.put(getCode("Colored LED light ring indicates water temperature"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цветное светодиодное кольцо указывает температуру воды"))
						.put(Lang.ITA, getWords("L'anello luminoso a LED colorato indica la temperatura dell'acqua"))
						.put(Lang.ESP, getWords("El anillo de luz LED coloreado indica la temperatura del agua"))
						.put(Lang.DEU, getWords("Farbiger LED-Lichtring zeigt die Wassertemperatur an"))
						.put(Lang.FRA, getWords("L'anneau lumineux coloré de LED indique la température de l'eau")).build());
		translates
				.put(getCode("All Items are purchased together and are not sold separately"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Все предметы приобретаются вместе и не продаются отдельно"))
						.put(Lang.ITA, getWords("Tutti gli articoli sono acquistati insieme e non sono venduti separatamente"))
						.put(Lang.ESP, getWords("Todos los artículos se compran juntos y no se venden por separado"))
						.put(Lang.DEU, getWords("Alle Artikel werden zusammen gekauft und sind nicht separat erhältlich"))
						.put(Lang.FRA, getWords("Tous les articles sont achetés ensemble et ne sont pas vendus séparément")).build());
		translates
				.put(getCode("7.5” x 5” (190 x 130 mm) metal wall plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("7.5 x 5 (190 x 130 мм) металлическая настенная плита"))
						.put(Lang.ITA, getWords("7,5 x 5 (190 x 130 mm) piastra a muro in metallo"))
						.put(Lang.ESP, getWords("Placa de pared de metal de 7.5 x 5 (190 x 130 mm)"))
						.put(Lang.DEU, getWords("7.5 x 5 (190 x 130 mm) Metallwandplatte"))
						.put(Lang.FRA, getWords("Plaque murale en métal de 7,5 po x 5 po (190 x 130 mm)")).build());
		translates
				.put(getCode("Steel frame is also available in white"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стальная рама также доступна в белом исполнении"))
						.put(Lang.ITA, getWords("La struttura in acciaio è disponibile anche in bianco"))
						.put(Lang.ESP, getWords("El marco de acero también está disponible en blanco"))
						.put(Lang.DEU, getWords("Stahlrahmen ist auch in weiß erhältlich"))
						.put(Lang.FRA, getWords("Le cadre en acier est également disponible en blanc")).build());
		translates
				.put(getCode("Continuous water heating"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непрерывное нагревание воды"))
						.put(Lang.ITA, getWords("Riscaldamento dell'acqua"))
						.put(Lang.ESP, getWords("Calentamiento continuo de agua"))
						.put(Lang.DEU, getWords("Kontinuierliche Warmwasserbereitung"))
						.put(Lang.FRA, getWords("Chauffage à eau continu")).build());
		translates
				.put(getCode("White or brushed gold frame with either Iroko wood or silver travertine"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Белая или матовая золотая рама с деревом Iroko или серебряным травертином"))
						.put(Lang.ITA, getWords("Montatura in oro bianco o spazzolato con travertino in legno Iroko o argento"))
						.put(Lang.ESP, getWords("Marco de oro blanco o cepillado con madera de Iroko o travertino de plata"))
						.put(Lang.DEU, getWords("Weißer oder gebürsteter Goldrahmen mit Iroko Holz oder Silber Travertin"))
						.put(Lang.FRA, getWords("Cadre en or blanc ou brossé avec du bois d'Iroko ou du travertin argenté")).build());
		translates
				.put(getCode("Aerated spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Аэрозольный аэрозоль"))
						.put(Lang.ITA, getWords("Spray aerato"))
						.put(Lang.ESP, getWords("Pulverizador aireado"))
						.put(Lang.DEU, getWords("Belüfteter Spray"))
						.put(Lang.FRA, getWords("Spray aéré")).build());
		translates
				.put(getCode("Large drawer includes push&pull opening and soft closing mechanism"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большой выдвижной ящик включает в себя выдвижное и вытягивающее отверстие и механизм мягкого закрытия"))
						.put(Lang.ITA, getWords("Ampio cassetto con apertura push & pull e meccanismo di chiusura morbida"))
						.put(Lang.ESP, getWords("El cajón grande incluye abertura de empuje y extracción y mecanismo de cierre suave"))
						.put(Lang.DEU, getWords("Große Schublade mit Push & Pull-Öffnung und Soft-Schließmechanismus"))
						.put(Lang.FRA, getWords("Grand tiroir comprenant une ouverture par poussée et traction et un mécanisme de fermeture en douceur")).build());
	}

	public static void part6() {
		translates
				.put(getCode("Minimum Flow Rate: 3.8 GPM (14.3 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальная скорость потока: 3,8 GPM (14,3 LPM)"))
						.put(Lang.ITA, getWords(" Portata minima: 3,8 GPM (14,3 LPM)"))
						.put(Lang.ESP, getWords(" Caudal mínimo: 3.8 GPM (14.3 LPM)"))
						.put(Lang.DEU, getWords(" Mindestflussrate: 3,8 GPM (14,3 LPM)"))
						.put(Lang.FRA, getWords(" Débit minimum: 3,8 GPM (14,3 LPM)")).build());
		translates
				.put(getCode("Clip handle with contrasting trim"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Клип-рукоятка с контрастной отделкой"))
						.put(Lang.ITA, getWords(" Maniglia a clip con finiture a contrasto"))
						.put(Lang.ESP, getWords(" Mango de clip con ajuste de contraste"))
						.put(Lang.DEU, getWords(" Clip-Griff mit kontrastierendem Rand"))
						.put(Lang.FRA, getWords(" Poignée à clip avec garniture contrastante")).build());
		translates
				.put(getCode("Drain column with siphon and feet can be made in: white, chrome, gold and bronze finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дренажная колонна с сифоном и ногами может быть выполнена из: белого, хромового, золотого и бронзового отделов"))
						.put(Lang.ITA, getWords(" Colonna di scarico con sifone e piedi possono essere realizzati in: bianco, cromato, oro e bronzo"))
						.put(Lang.ESP, getWords(" La columna de desagüe con sifón y pies se puede fabricar con acabado blanco, cromado, dorado y bronce"))
						.put(Lang.DEU, getWords(" Abtropfsäule mit Siphon und Füßen können hergestellt werden in: Weiß, Chrom, Gold und Bronze"))
						.put(Lang.FRA, getWords(" Colonne de vidange avec siphon et pieds pouvant être réalisés en: blanc, chrome, or et bronze")).build());
		translates
				.put(getCode("Swiss-engineered smart and stylish three-outlet thermostatic digital shower control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Швейцарский инженерный интеллектуальный и стильный трехрежимный термостатический цифровой душ-контроль"))
						.put(Lang.ITA, getWords(" Comando digitale della doccia termostatico a tre uscite intelligente ed elegante"))
						.put(Lang.ESP, getWords(" Control digital termostático de ducha de tres salidas inteligente y elegante con diseño suizo"))
						.put(Lang.DEU, getWords(" In der Schweiz entwickelte, intelligente und elegante Thermostat-Digital-Duschsteuerung mit drei Abgängen"))
						.put(Lang.FRA, getWords(" Robinet de douche numérique thermostatique à trois prises intelligent et élégant de conception suisse")).build());
		translates
				.put(getCode("Additional feet washer self-closing jet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Самостоятельная струя"))
						.put(Lang.ITA, getWords(" Rondella autochiudente per lavaggio piedi aggiuntivo"))
						.put(Lang.ESP, getWords(" Jet adicional de cierre automático de la lavadora de pies"))
						.put(Lang.DEU, getWords(" Zusätzliche Fußwaschanlage mit selbstschließendem Strahl"))
						.put(Lang.FRA, getWords(" Jet additionnel de frein à fermeture automatique")).build());
		translates
				.put(getCode("110V 60Hz power with 30A GFCI and 20A GFCI Protected"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мощность 110 В 60 Гц с защитой 30FC GFCI и 20A GFCI"))
						.put(Lang.ITA, getWords(" Potenza 110 V 60 Hz con GFCI 30A e GFCI 20A protetti"))
						.put(Lang.ESP, getWords(" Potencia de 110V 60Hz con 30 GFCI y 20 GFCI protegido"))
						.put(Lang.DEU, getWords(" 110V 60Hz Leistung mit 30A GFCI und 20A GFCI geschützt"))
						.put(Lang.FRA, getWords(" Alimentation 110V 60Hz avec GFCI 30A et GFCI 20A Protégé")).build());
		translates
				.put(getCode("1 x 2kW heater"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 х 2 кВт нагреватель"))
						.put(Lang.ITA, getWords(" 1 riscaldatore da 2kW"))
						.put(Lang.ESP, getWords(" 1 x calentador de 2kW"))
						.put(Lang.DEU, getWords(" 1 x 2kW Heizung"))
						.put(Lang.FRA, getWords(" 1 réchauffeur de 2 kW")).build());
		translates
				.put(getCode("Aesthetically attractive, design class accessory"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эстетически привлекательный аксессуар класса дизайна"))
						.put(Lang.ITA, getWords(" Esteticamente attraente, accessorio di design"))
						.put(Lang.ESP, getWords(" Estéticamente atractivo, accesorio de clase de diseño"))
						.put(Lang.DEU, getWords(" Ästhetisch ansprechendes Accessoire der Designklasse"))
						.put(Lang.FRA, getWords(" Esthétiquement attrayant, accessoire de classe de conception")).build());
		translates
				.put(getCode("Preinstalled cable drive pop up, and waste-overflow fitting included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Появился предустановленный кабельный привод, а также фитинг с переливом"))
						.put(Lang.ITA, getWords(" Alloggiamento cavo preinstallato e raccordo raccogli-rifiuti incluso"))
						.put(Lang.ESP, getWords(" Unidad emergente de cable preinstalada y accesorio de desbordamiento de residuos incluido"))
						.put(Lang.DEU, getWords(" Vorinstallierter Kabelantrieb Pop-up und Abfall-Überlauf-Fitting enthalten"))
						.put(Lang.FRA, getWords(" Le lecteur de câble préinstallé s'ouvre et le raccord de trop-plein inclus")).build());
		translates
				.put(getCode("Crafted in Europe"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Создано в Европе"))
						.put(Lang.ITA, getWords(" Realizzato in Europa"))
						.put(Lang.ESP, getWords(" Hecho a mano en Europa"))
						.put(Lang.DEU, getWords(" Hergestellt in Europa"))
						.put(Lang.FRA, getWords(" Fabriqué en Europe")).build());
		translates
				.put(getCode("Can be installed with Waterproofing Membrane"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Может быть установлен с гидроизоляционной мембраной"))
						.put(Lang.ITA, getWords(" Può essere installato con membrana impermeabilizzante"))
						.put(Lang.ESP, getWords(" Se puede instalar con membrana impermeabilizante"))
						.put(Lang.DEU, getWords(" Kann mit wasserabweisender Membrane installiert werden"))
						.put(Lang.FRA, getWords(" Peut être installé avec une membrane d'étanchéité")).build());
		translates
				.put(getCode("Cable Length 30"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина кабеля 30 "))
						.put(Lang.ITA, getWords(" Lunghezza del cavo 30 "))
						.put(Lang.ESP, getWords(" Longitud del cable 30 "))
						.put(Lang.DEU, getWords(" Kabellänge 30 "))
						.put(Lang.FRA, getWords(" Longueur du câble 30 ")).build());
		translates
				.put(getCode("Top mounted head with wall bracket mount"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Верхняя головка с настенным кронштейном"))
						.put(Lang.ITA, getWords(" Testa superiore montata con supporto a parete"))
						.put(Lang.ESP, getWords(" Cabeza montada en la parte superior con soporte de montaje en pared"))
						.put(Lang.DEU, getWords(" Aufsatzkopf mit Wandhalterung"))
						.put(Lang.FRA, getWords(" Tête montée avec support mural")).build());
		translates
				.put(getCode("Available with optional brass, PVC or ABS DWV fittings"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступны с дополнительными фитингами из латуни, ПВХ или ABS DWV"))
						.put(Lang.ITA, getWords(" Disponibile con raccordi DWV in ottone, PVC o ABS opzionali"))
						.put(Lang.ESP, getWords(" Disponible con accesorios DWV de latón, PVC o ABS opcionales"))
						.put(Lang.DEU, getWords(" Erhältlich mit optionalen DWV-Anschlüssen aus Messing, PVC oder ABS"))
						.put(Lang.FRA, getWords(" Disponible avec des raccords DWV en laiton, PVC ou ABS en option")).build());
		translates
				.put(getCode("Supplied with market specific plug for other markets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поставляется с штекером на рынке для других рынков"))
						.put(Lang.ITA, getWords(" Fornito con spina specifica del mercato per altri mercati"))
						.put(Lang.ESP, getWords(" Se suministra con un enchufe específico para el mercado para otros mercados"))
						.put(Lang.DEU, getWords(" Lieferung mit marktspezifischem Stecker für andere Märkte"))
						.put(Lang.FRA, getWords(" Livré avec fiche spécifique au marché pour les autres marchés")).build());
		translates
				.put(getCode("Swiss-engineered smart and stylish two-outlet shower control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Швейцарский инженерный элегантный и стильный двухуровневый душ"))
						.put(Lang.ITA, getWords(" Comando per doccia a due uscite intelligente ed elegante"))
						.put(Lang.ESP, getWords(" Control de ducha de dos salidas elegante y elegante diseñado por Swiss"))
						.put(Lang.DEU, getWords(" In der Schweiz entwickelte intelligente und stilvolle Zwei-Ausgänge-Duschsteuerung"))
						.put(Lang.FRA, getWords(" Commande de douche intelligente et élégante à deux prises de conception suisse")).build());
		translates
				.put(getCode("Seven colors LED light without electrical power supply"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Семицветный светодиодный светильник без электропитания"))
						.put(Lang.ITA, getWords(" LED a sette colori senza alimentazione elettrica"))
						.put(Lang.ESP, getWords(" Luz LED de siete colores sin suministro de energía eléctrica"))
						.put(Lang.DEU, getWords(" Sieben Farben LED-Licht ohne Stromversorgung"))
						.put(Lang.FRA, getWords(" Lumière de sept couleurs LED sans alimentation électrique")).build());
		translates
				.put(getCode("Powerful Back Massage with Air Assist Turbo Massage (AATM™)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мощный массаж спины с воздушным массажем (AATM ™)"))
						.put(Lang.ITA, getWords(" Massaggio alla schiena potente con Air Assist Turbo Massage (AATM ™)"))
						.put(Lang.ESP, getWords(" Potente masaje de espalda con Air Assist Turbo Massage (AATM ™)"))
						.put(Lang.DEU, getWords(" Leistungsstarke Rückenmassage mit Air Assist Turbo Massage (AATM ™)"))
						.put(Lang.FRA, getWords(" Massage puissant du dos avec Air Assist Turbo Massage (AATM ™)")).build());
		translates
				.put(getCode("Water flow rate - 4.75 GPM (18 LPM) with 3 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Расход воды - 4,75 GPM (18 LPM) с давлением воды 3 бар"))
						.put(Lang.ITA, getWords(" Portata d'acqua - 4,75 GPM (18 LPM) con pressione dell'acqua di 3 bar"))
						.put(Lang.ESP, getWords(" Caudal de agua: 4.75 GPM (18 LPM) con 3 bares de presión de agua"))
						.put(Lang.DEU, getWords(" Wasserdurchflussrate - 4,75 GPM (18 LPM) bei 3 bar Wasserdruck"))
						.put(Lang.FRA, getWords(" Débit d'eau - 4,75 GPM (18 LPM) avec une pression d'eau de 3 bars")).build());
		translates
				.put(getCode("Integrated towel hanger - 730 mm / 28.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная вешалка для полотенец - 730 мм / 28,75 дюйма"))
						.put(Lang.ITA, getWords(" Portasciugamani integrato - 730 mm / 28,75 in"))
						.put(Lang.ESP, getWords(" Percha de toalla integrada - 730 mm / 28.75 in"))
						.put(Lang.DEU, getWords(" Integrierter Handtuchhalter - 730 mm / 28,75 in"))
						.put(Lang.FRA, getWords(" Porte-serviettes intégré - 730 mm / 28.75 po")).build());
		translates
				.put(getCode("Compact, small footprint, premium design freestanding bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Компактная, небольшая площадь, высококачественная автономная ванна"))
						.put(Lang.ITA, getWords(" Vasca da bagno indipendente dal design compatto e di dimensioni ridotte"))
						.put(Lang.ESP, getWords(" Bañera independiente compacta, de tamaño pequeño y diseño premium"))
						.put(Lang.DEU, getWords(" Kompakt, kleine Standfläche, Premium-Design freistehende Badewanne"))
						.put(Lang.FRA, getWords(" Compact, petit encombrement, baignoire autoportante de conception premium")).build());
		translates
				.put(getCode("Masterful engineering combining powerful water flow in a very slim housing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мастерская инженерия, сочетающая мощный поток воды в очень тонком корпусе"))
						.put(Lang.ITA, getWords(" Ingegneria magistrale che combina un potente flusso d'acqua in un alloggiamento molto sottile"))
						.put(Lang.ESP, getWords(" Ingeniería magistral que combina un poderoso flujo de agua en una carcasa muy delgada"))
						.put(Lang.DEU, getWords(" Meisterhafte Technik kombiniert kraftvollen Wasserfluss in einem sehr schlanken Gehäuse"))
						.put(Lang.FRA, getWords(" Ingénierie magistrale combinant un débit d'eau puissant dans un boîtier très mince")).build());
		translates
				.put(getCode("Maximum Flow Rate: 6.3 GPM (24 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока: 6,3 GPM (24 LPM)"))
						.put(Lang.ITA, getWords(" Portata massima: 6,3 GPM (24 LPM)"))
						.put(Lang.ESP, getWords(" Caudal máximo: 6.3 GPM (24 LPM)"))
						.put(Lang.DEU, getWords(" Maximale Durchflussrate: 6,3 GPM (24 LPM)"))
						.put(Lang.FRA, getWords(" Débit maximum: 6.3 GPM (24 LPM)")).build());
		translates
				.put(getCode("Open fronted central open-fronted unit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Открытый фронт центрального открытого фасада"))
						.put(Lang.ITA, getWords(" Unità frontale aperta fronte centrale aperta"))
						.put(Lang.ESP, getWords(" Unidad frontal abierta con frente abierto"))
						.put(Lang.DEU, getWords(" Offene Front-Front-Unit"))
						.put(Lang.FRA, getWords(" Unité centrale ouverte à façade ouverte")).build());
		translates
				.put(getCode("1.25” (3.2cm) up and down waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1,25 (3,2 см) вверх и вниз отходы"))
						.put(Lang.ITA, getWords(" 1.25 (3.2 cm) su e giù per i rifiuti"))
						.put(Lang.ESP, getWords(" Residuos de 1.25 (3.2 cm) hacia arriba y hacia abajo"))
						.put(Lang.DEU, getWords(" 1,25 (3,2 cm) auf und ab verschwenden"))
						.put(Lang.FRA, getWords(" 1.25 (3.2cm) de haut en bas des déchets")).build());
		translates
				.put(getCode("On/Off control panel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Панель управления вкл. / Выкл."))
						.put(Lang.ITA, getWords(" Pannello di controllo On / Off"))
						.put(Lang.ESP, getWords(" Panel de control de encendido / apagado"))
						.put(Lang.DEU, getWords(" Ein / Aus-Bedienfeld"))
						.put(Lang.FRA, getWords(" Panneau de contrôle On / Off")).build());
		translates
				.put(getCode("Operates with minimum pressure of 0.5 to 1.5 bar for a full cascade shower that saves water"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Работает с минимальным давлением от 0,5 до 1,5 бар для полного каскадного душа, который экономит воду"))
						.put(Lang.ITA, getWords(" Funziona con una pressione minima da 0,5 a 1,5 bar per una doccia a cascata completa che consente di risparmiare acqua"))
						.put(Lang.ESP, getWords(" Funciona con una presión mínima de 0.5 a 1.5 bar para una ducha de cascada completa que ahorra agua"))
						.put(Lang.DEU, getWords(" Arbeitet mit einem Mindestdruck von 0,5 bis 1,5 bar für eine vollständige Kaskadendusche, die Wasser spart"))
						.put(Lang.FRA, getWords(" Fonctionne avec une pression minimale de 0,5 à 1,5 bar pour une douche en cascade complète qui économise l'eau")).build());
		translates
				.put(getCode("Unique, patented design with built-in ergonomically sculpted headrest – several industrial design patents pending in the USA and EU"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальный, запатентованный дизайн со встроенным эргономичным скульптурным подголовником - несколько патентов промышленного дизайна, ожидающих рассмотрения в США и ЕС"))
						.put(Lang.ITA, getWords(" Design unico e brevettato con poggiatesta integrato ergonomicamente scolpito - diversi brevetti di design industriale pendenti negli Stati Uniti e in Europa"))
						.put(Lang.ESP, getWords(" Diseño único y patentado con reposacabezas integrado ergonómicamente esculpido - varias patentes de diseño industrial pendientes en los EE. UU. Y la UE"))
						.put(Lang.DEU, getWords(" Einzigartiges, patentiertes Design mit integrierter ergonomisch geformter Kopfstütze - mehrere Industriedesign-Patente angemeldet in den USA und der EU"))
						.put(Lang.FRA, getWords(" Conception unique et brevetée avec appuie-tête ergonomique intégré - plusieurs brevets de conception industrielle en attente aux États-Unis et en Europe")).build());
		translates
				.put(getCode("Includes an easy to wash, weather-resistant removable cover"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Включает легкую мытью, устойчивую к погодным условиям съемную крышку"))
						.put(Lang.ITA, getWords(" Include una cover rimovibile facile da lavare e resistente alle intemperie"))
						.put(Lang.ESP, getWords(" Incluye una funda extraíble resistente a la intemperie y fácil de lavar"))
						.put(Lang.DEU, getWords(" Enthält einen leicht zu waschenden, witterungsbeständigen abnehmbaren Bezug"))
						.put(Lang.FRA, getWords(" Comprend un couvercle amovible facile à laver et résistant aux intempéries")).build());
		translates
				.put(getCode("Quick dry foam padding"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Быстрая сухая пена"))
						.put(Lang.ITA, getWords(" Imbottitura in schiuma asciutta rapida"))
						.put(Lang.ESP, getWords(" Acolchado de espuma de secado rápido"))
						.put(Lang.DEU, getWords(" Schnelltrocknende Schaumstoffpolsterung"))
						.put(Lang.FRA, getWords(" Rembourrage en mousse à séchage rapide")).build());
		translates
				.put(getCode("Constructed of AquateX™ in fine matte finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из AquateX ™ в тонкой матовой отделке"))
						.put(Lang.ITA, getWords(" Realizzato in AquateX ™ con finitura opaca"))
						.put(Lang.ESP, getWords(" Construido de AquateX ™ en fino acabado mate"))
						.put(Lang.DEU, getWords(" Hergestellt aus AquateX ™ in feinem Matt-Finish"))
						.put(Lang.FRA, getWords(" Construit d'AquateX ™ dans la finition mate fine")).build());
		translates
				.put(getCode("Stainless steel nozzles"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сопла из нержавеющей стали"))
						.put(Lang.ITA, getWords(" Ugelli in acciaio inossidabile"))
						.put(Lang.ESP, getWords(" Boquillas de acero inoxidable"))
						.put(Lang.DEU, getWords(" Düsen aus rostfreiem Stahl"))
						.put(Lang.FRA, getWords(" Buses en acier inoxydable")).build());
		translates
				.put(getCode("Created by renowned Italian designer Massimo Farinatti"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Созданный известным итальянским дизайнером Массимо Фаринатти"))
						.put(Lang.ITA, getWords(" Creato dal famoso designer italiano Massimo Farinatti"))
						.put(Lang.ESP, getWords(" Creado por el reconocido diseñador italiano Massimo Farinatti"))
						.put(Lang.DEU, getWords(" Erstellt von dem renommierten italienischen Designer Massimo Farinatti"))
						.put(Lang.FRA, getWords(" Créé par le célèbre designer italien Massimo Farinatti")).build());
		translates
				.put(getCode("Standard 1 1/2 threaded outlet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандартный 1 1/2 резьбовой выход"))
						.put(Lang.ITA, getWords(" Presa filettata standard da 1 1/2 "))
						.put(Lang.ESP, getWords(" Salida roscada estándar de 1 1/2 "))
						.put(Lang.DEU, getWords(" Standard 1 1/2 Gewindeauslass"))
						.put(Lang.FRA, getWords(" Sortie filetée standard de 1 1/2 ")).build());
		translates
				.put(getCode("Can be made in gold, silver, copper plated or old brass finish upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Может быть изготовлено из золота, серебра, меди или старой латуни по запросу"))
						.put(Lang.ITA, getWords(" Può essere realizzato in oro, argento, rame placcato o finitura ottone antico su richiesta"))
						.put(Lang.ESP, getWords(" Puede ser hecho en oro, plata, cobre plateado o latón viejo acabado bajo pedido"))
						.put(Lang.DEU, getWords(" Kann auf Anfrage in Gold, Silber, Kupfer oder Altmessing hergestellt werden"))
						.put(Lang.FRA, getWords(" Peut être fait en or, argent, cuivre plaqué ou vieux laiton sur demande")).build());
		translates
				.put(getCode("External thermostatic bath mixer with anti-lime hand shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Внешний термостатический смеситель для ванны с анти-лимонным душем"))
						.put(Lang.ITA, getWords(" Miscelatore termostatico vasca esterno con doccetta anticalcare"))
						.put(Lang.ESP, getWords(" Mezclador de baño termostático externo con ducha de mano antical"))
						.put(Lang.DEU, getWords(" Externer Thermostat-Wannenmischer mit Antikalk-Handbrause"))
						.put(Lang.FRA, getWords(" Mitigeur thermostatique de bain externe avec douchette anti-calcaire")).build());
		translates
				.put(getCode("Maximum water capacity - 1800 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 1800 литров"))
						.put(Lang.ITA, getWords(" Capacità massima dell'acqua - 1800 litri"))
						.put(Lang.ESP, getWords(" Capacidad máxima de agua - 1800 Litros"))
						.put(Lang.DEU, getWords(" Maximale Wasserkapazität - 1800 Liter"))
						.put(Lang.FRA, getWords(" Capacité maximale d'eau - 1800 Litres")).build());
		translates
				.put(getCode("Solid brass, chrome finished"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Твердая латунь, хромированная"))
						.put(Lang.ITA, getWords(" Ottone massiccio, finitura cromata"))
						.put(Lang.ESP, getWords(" Latón macizo, acabado en cromo"))
						.put(Lang.DEU, getWords(" Massives Messing, verchromt"))
						.put(Lang.FRA, getWords(" Laiton massif, fini chrome")).build());
		translates
				.put(getCode("Discreet clip handles with durable metallic trim"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дискретные ручки для клипов с прочной металлической отделкой"))
						.put(Lang.ITA, getWords(" Manici a clip discreti con finiture metalliche resistenti"))
						.put(Lang.ESP, getWords(" Mangos de clip discretos con adornos metálicos duraderos"))
						.put(Lang.DEU, getWords(" Dezente Clip-Griffe mit langlebigen Metallic-Beschlägen"))
						.put(Lang.FRA, getWords(" Poignées à clip discrètes avec garniture métallique durable")).build());
		translates
				.put(getCode("2 year overall limited whirlpool system warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя гарантия ограниченной системы водоворота"))
						.put(Lang.ITA, getWords(" Garanzia del sistema idromassaggio di 2 anni complessivi"))
						.put(Lang.ESP, getWords(" 2 años de garantía limitada general del sistema de hidromasaje"))
						.put(Lang.DEU, getWords(" 2 Jahre eingeschränkte Whirlpool-Systemgarantie"))
						.put(Lang.FRA, getWords(" Garantie limitée générale de 2 ans sur le système balnéo")).build());
		translates
				.put(getCode("Two outlet thermostatic shower control dimensions: 2 L x 0.75 W x 4.5 H (in) / 5 L x 2 W x 11.5 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Два выходных терморегулирующих регулятора: 2 L x 0,75 Вт x 4,5 H (дюйм) / 5 L x 2 Вт x 11,5 H (см)"))
						.put(Lang.ITA, getWords(" Due dimensioni della doccia termostatica di uscita: 2 L x 0,75 W x 4,5 H (in) / 5 L x 2 W x 11,5 H (cm)"))
						.put(Lang.ESP, getWords(" Dos dimensiones de control de la ducha termostática de salida: 2 L x 0.75 W x 4.5 H (in) / 5 L x 2 W x 11.5 H (cm)"))
						.put(Lang.DEU, getWords(" Zwei Auslass-Thermostat-Brausesteuerungsmaße: 2 L x 0,75 W x 4,5 H (Zoll) / 5 L x 2 W x 11,5 H (cm)"))
						.put(Lang.FRA, getWords(" Dimensions de contrôle de douche thermostatique à deux prises: 2 L x 0,75 W x 4,5 H (po) / 5 L x 2 L x 11,5 H (cm)")).build());
		translates
				.put(getCode("EcoMarmor™ material provides for unparalleled heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Материал EcoMarmor ™ обеспечивает беспрецедентное удержание тепла и долговечность"))
						.put(Lang.ITA, getWords(" Il materiale EcoMarmor ™ garantisce una ritenzione del calore e una durata senza pari"))
						.put(Lang.ESP, getWords(" El material EcoMarmor ™ proporciona una retención de calor y durabilidad sin igual"))
						.put(Lang.DEU, getWords(" EcoMarmor ™ -Material sorgt für beispiellose Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords(" Le matériau EcoMarmor ™ offre une rétention de chaleur et une durabilité inégalées")).build());
		translates
				.put(getCode("Female thread for 1-1/2 brass tube (not included)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Внутренняя резьба для латунной трубки 1-1 / 2 (не входит в комплект)"))
						.put(Lang.ITA, getWords(" Filetto femmina per tubo di ottone da 1-1 / 2 (non incluso)"))
						.put(Lang.ESP, getWords(" Rosca hembra para tubo de latón de 1-1 / 2 (no incluido)"))
						.put(Lang.DEU, getWords(" Innengewinde für 1-1 / 2 Messingrohr (nicht im Lieferumfang enthalten)"))
						.put(Lang.FRA, getWords(" Filetage femelle pour tube en laiton de 1-1 / 2 (non inclus)")).build());
		translates
				.put(getCode("European craftsmanship"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Европейское мастерство"))
						.put(Lang.ITA, getWords(" Artigianato europeo"))
						.put(Lang.ESP, getWords(" Artesanía europea"))
						.put(Lang.DEU, getWords(" Europäische Handwerkskunst"))
						.put(Lang.FRA, getWords(" Artisanat européen")).build());
		translates
				.put(getCode("5 year limited warranty on the blower and the pump"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на воздуходувку и насос"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 5 anni sul ventilatore e sulla pompa"))
						.put(Lang.ESP, getWords(" Garantía limitada de 5 años en el soplador y la bomba"))
						.put(Lang.DEU, getWords(" 5 Jahre Garantie auf das Gebläse und die Pumpe"))
						.put(Lang.FRA, getWords(" Garantie limitée de 5 ans sur le souffleur et la pompe")).build());
		translates
				.put(getCode("Designed to coordinate beautifully with a number of Aquatica's freestanding bathtubs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для красивой координации с несколькими автономными ваннами Aquatica"))
						.put(Lang.ITA, getWords(" Progettato per coordinarsi magnificamente con un numero di vasche freestanding Aquatica"))
						.put(Lang.ESP, getWords(" Diseñado para coordinar maravillosamente con una serie de bañeras independientes de Aquatica"))
						.put(Lang.DEU, getWords(" Entwickelt für die perfekte Koordination mit einer Reihe von freistehenden Badewannen von Aquatica"))
						.put(Lang.FRA, getWords(" Conçu pour se coordonner magnifiquement avec un certain nombre de baignoires autoportantes Aquatica")).build());
		translates
				.put(getCode("Reliable and robust – built to last"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Надежный и надежный"))
						.put(Lang.ITA, getWords(" Affidabile e robusto, costruito per durare"))
						.put(Lang.ESP, getWords(" Fiable y robusto: construido para durar"))
						.put(Lang.DEU, getWords(" Zuverlässig und robust - für die Ewigkeit gebaut"))
						.put(Lang.FRA, getWords(" Fiable et robuste - conçu pour durer")).build());
		translates
				.put(getCode("Constructed from top quality ceramic materials"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из высококачественных керамических материалов"))
						.put(Lang.ITA, getWords(" Costruito con materiali ceramici di alta qualità"))
						.put(Lang.ESP, getWords(" Construido con materiales cerámicos de alta calidad"))
						.put(Lang.DEU, getWords(" Hergestellt aus hochwertigen Keramikmaterialien"))
						.put(Lang.FRA, getWords(" Construit à partir de matériaux céramiques de qualité supérieure")).build());
		translates
				.put(getCode("Traditional freestanding slipper bathtub design with an updated modern twist"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Традиционный автономный дизайн тапочки для ванной с обновленной современной твисткой"))
						.put(Lang.ITA, getWords(" Design tradizionale della vasca da bagno a pantofola freestanding con un tocco moderno aggiornato"))
						.put(Lang.ESP, getWords(" Diseño de bañera de zapatilla independiente tradicional con un toque moderno actualizado"))
						.put(Lang.DEU, getWords(" Traditionelle freistehende Pantoffel-Badewannendesign mit einem modernisierten modernen Twist"))
						.put(Lang.FRA, getWords(" Conception traditionnelle de baignoire-sabot autoportante avec une touche moderne mise à jour")).build());
		translates
				.put(getCode("Will be a perfect addition to Bollicine collection"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Будет отличным дополнением к коллекции Bollicine"))
						.put(Lang.ITA, getWords(" Sarà un'aggiunta perfetta alla collezione Bollicine"))
						.put(Lang.ESP, getWords(" Será una adición perfecta a la colección Bollicine"))
						.put(Lang.DEU, getWords(" Wird eine perfekte Ergänzung zu Bollicine Sammlung sein"))
						.put(Lang.FRA, getWords(" Sera un ajout parfait à la collection Bollicine")).build());
		translates
				.put(getCode("Dual control - temperature and water flow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Двойное управление - температура и расход воды"))
						.put(Lang.ITA, getWords(" Doppio controllo - temperatura e flusso d'acqua"))
						.put(Lang.ESP, getWords(" Control doble: temperatura y flujo de agua"))
						.put(Lang.DEU, getWords(" Dual control - Temperatur und Wasserdurchfluss"))
						.put(Lang.FRA, getWords(" Double contrôle - température et débit d'eau")).build());
		translates
				.put(getCode("UV-resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("УФ-лучи"))
						.put(Lang.ITA, getWords(" Resistente ai raggi UV"))
						.put(Lang.ESP, getWords(" Resistente a los rayos UV"))
						.put(Lang.DEU, getWords(" UV-beständig"))
						.put(Lang.FRA, getWords(" Résistant aux UV")).build());
		translates
				.put(getCode("Flashing light stops when temperature is reached"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мигающий свет останавливается при достижении температуры"))
						.put(Lang.ITA, getWords(" La luce lampeggiante si arresta quando viene raggiunta la temperatura"))
						.put(Lang.ESP, getWords(" La luz intermitente se detiene cuando se alcanza la temperatura"))
						.put(Lang.DEU, getWords(" Das Blinklicht hört auf, wenn die Temperatur erreicht ist"))
						.put(Lang.FRA, getWords(" La lumière clignotante s'arrête lorsque la température est atteinte")).build());
		translates
				.put(getCode("Arm length 15.75 (40 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукоятки 15,75 (40 см)"))
						.put(Lang.ITA, getWords(" Braccio lunghezza 15.75 (40 cm)"))
						.put(Lang.ESP, getWords(" Longitud del brazo 15.75 (40 cm)"))
						.put(Lang.DEU, getWords(" Armlänge 15,75 (40 cm)"))
						.put(Lang.FRA, getWords(" Longueur de bras 15.75 (40 cm)")).build());
		translates
				.put(getCode("Intended for wall installations"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для настенных установок"))
						.put(Lang.ITA, getWords("Destinato alle installazioni a muro"))
						.put(Lang.ESP, getWords("Destinado a instalaciones de pared"))
						.put(Lang.DEU, getWords("Geeignet für Wandinstallationen"))
						.put(Lang.FRA, getWords("Destiné aux installations murales")).build());
		translates
				.put(getCode("Large 9.8x9.8 (250x250 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 9.8x9.8 (250x250 мм) душевая насадка"))
						.put(Lang.ITA, getWords("Soffione grande 9,8x9,8 (250x250 mm)"))
						.put(Lang.ESP, getWords("Cabezal de ducha grande de 9.8x9.8 (250x250 mm)"))
						.put(Lang.DEU, getWords("Großer 9,8 x 9,8 (250 x 250 mm) Duschkopf"))
						.put(Lang.FRA, getWords("Grande pomme de douche de 9,8 x 9,8 po (250 x 250 mm)")).build());
		translates
				.put(getCode("Non-porous glossy surface for easy cleaning and sanitizing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непористая глянцевая поверхность для легкой очистки и дезинфекции"))
						.put(Lang.ITA, getWords("Superficie lucida non porosa per una facile pulizia e sanificazione"))
						.put(Lang.ESP, getWords("Superficie brillante no porosa para facilitar la limpieza y la desinfección"))
						.put(Lang.DEU, getWords("Nicht poröse, glänzende Oberfläche für einfache Reinigung und Desinfektion"))
						.put(Lang.FRA, getWords("Surface brillante non poreuse pour un nettoyage et une désinfection faciles")).build());
		translates
				.put(getCode("10 year limited warranty on the bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("10-летняя ограниченная гарантия на ванну"))
						.put(Lang.ITA, getWords("Garanzia limitata di 10 anni sulla vasca"))
						.put(Lang.ESP, getWords("Garantía limitada de 10 años en la bañera"))
						.put(Lang.DEU, getWords("10 Jahre Garantie auf die Badewanne"))
						.put(Lang.FRA, getWords("Garantie limitée de 10 ans sur la baignoire")).build());
		translates
				.put(getCode("Max flow rate of 7.5 GPM (28.4 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 7,5 GPM (28,4 LPM)"))
						.put(Lang.ITA, getWords("Portata massima di 7,5 GPM (28,4 LPM)"))
						.put(Lang.ESP, getWords("Caudal máximo de 7.5 GPM (28.4 LPM)"))
						.put(Lang.DEU, getWords("Max. Durchflussrate von 7,5 GPM (28,4 LPM)"))
						.put(Lang.FRA, getWords("Débit maximum de 7,5 GPM (28,4 LPM)")).build());
		translates
				.put(getCode("Stackable"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Составная"))
						.put(Lang.ITA, getWords("impilabile"))
						.put(Lang.ESP, getWords("Apilable"))
						.put(Lang.DEU, getWords(" Stapelbar"))
						.put(Lang.FRA, getWords("Empilable")).build());
		translates
				.put(getCode("Deep, full-body soaking experience"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глубокое, полное погружение в тело"))
						.put(Lang.ITA, getWords("Esperienza di immersione profonda e completa"))
						.put(Lang.ESP, getWords("Experiencia profunda de inmersión en todo el cuerpo"))
						.put(Lang.DEU, getWords("Tiefes Ganzkörpererlebnis"))
						.put(Lang.FRA, getWords("Profonde expérience de trempage dans tout le corps")).build());
		translates
				.put(getCode("Minimum pressure recommended for best peformance - 3bar"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальное давление, рекомендуемое для наилучшего торможения - 3 бар"))
						.put(Lang.ITA, getWords("Pressione minima consigliata per la migliore prestazione - 3 bar"))
						.put(Lang.ESP, getWords("Presión mínima recomendada para el mejor rendimiento - 3bar"))
						.put(Lang.DEU, getWords("Mindestdruck empfohlen für beste Leistung - 3bar"))
						.put(Lang.FRA, getWords("Pression minimale recommandée pour la meilleure performance - 3bar")).build());
		translates
				.put(getCode("Freestanding white ceramic sink - 600 x 460 x 180 mm / 25.5 x 18 x 7 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Свободная белая керамическая раковина - 600 x 460 x 180 мм / 25,5 x 18 x 7 дюймов"))
						.put(Lang.ITA, getWords("Lavabo freestanding in ceramica bianca - 600 x 460 x 180 mm / 25,5 x 18 x 7 in"))
						.put(Lang.ESP, getWords("Fregadero de cerámica blanco independiente - 600 x 460 x 180 mm / 25.5 x 18 x 7 in"))
						.put(Lang.DEU, getWords("Freistehende weiße Keramik-Spüle - 600 x 460 x 180 mm / 25.5 x 18 x 7 in"))
						.put(Lang.FRA, getWords("Évier en céramique blanc autoportant - 600 x 460 x 180 mm / 25.5 x 18 x 7 po")).build());
		translates
				.put(getCode("Useful widened foot side rim designed to accommodate many popular bath fillers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полезная расширенная задняя сторона обода, предназначенная для размещения многих популярных наполнителей для ванны"))
						.put(Lang.ITA, getWords("Orlo allargato utile del lato del piede destinato per accomodare molti riempitivi popolari del bagno"))
						.put(Lang.ESP, getWords("El borde lateral del pie ampliado y útil está diseñado para acomodar muchos rellenos de baño populares"))
						.put(Lang.DEU, getWords("Nützliche verbreiterte Fußseitenfelge, die entworfen ist, um viele populäre Badfüller unterzubringen"))
						.put(Lang.FRA, getWords("Jante de pied élargie utile conçue pour accueillir de nombreuses remplisseuses de bain populaires")).build());
		translates
				.put(getCode("A transparent curved acrylic door opens inwards to save space"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прозрачная изогнутая акриловая дверь открывается внутрь, чтобы сэкономить место"))
						.put(Lang.ITA, getWords("Una porta in acrilico curvo trasparente si apre verso l'interno per risparmiare spazio"))
						.put(Lang.ESP, getWords("Una puerta de acrílico curvada transparente se abre hacia adentro para ahorrar espacio"))
						.put(Lang.DEU, getWords("Eine transparente, gebogene Acryltür öffnet sich platzsparend nach innen"))
						.put(Lang.FRA, getWords("Une porte acrylique incurvée transparente s'ouvre vers l'intérieur pour économiser de l'espace")).build());
		translates
				.put(getCode("Superb AquateX™ material properties and tub thickness ensure fantastic heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Превосходные свойства материала AquateX ™ и толщина ванны обеспечивают фантастическое удержание тепла"))
						.put(Lang.ITA, getWords("Le superbe proprietà del materiale AquateX ™ e lo spessore della vasca assicurano una fantastica ritenzione di calore"))
						.put(Lang.ESP, getWords("Las excelentes propiedades del material AquateX ™ y el espesor de la cubeta garantizan una fantástica retención de calor"))
						.put(Lang.DEU, getWords("Hervorragende AquateX ™ Materialeigenschaften und Wannenstärke sorgen für eine hervorragende Wärmespeicherung"))
						.put(Lang.FRA, getWords("Les propriétés exceptionnelles du matériau AquateX ™ et l'épaisseur de la baignoire assurent une rétention de la chaleur fantastique")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 3 outlet shut-off valve and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 3 выпускными запорными клапанами и металлической пластиной"))
						.put(Lang.ITA, getWords("Miscelatore doccia esterno a parete con valvola d'intercettazione a 3 uscite e piastra metallica"))
						.put(Lang.ESP, getWords("Mezclador de ducha de pared con válvula de cierre de 3 salidas y placa de metal"))
						.put(Lang.DEU, getWords("Wand-Brausemischer mit 3-Wege-Absperrventil und Metallplatte"))
						.put(Lang.FRA, getWords("Mitigeur de douche mural avec 3 vannes d'arrêt et plaque métallique")).build());
		translates
				.put(getCode("Sink faucet with 11/4 pop up waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Раковина с краном 1 1/4"))
						.put(Lang.ITA, getWords("Rubinetto per lavabo con scarico da 1 1/4"))
						.put(Lang.ESP, getWords("Lavabo grifo con 1 1/4 de desechos pop-up"))
						.put(Lang.DEU, getWords("Waschbecken Wasserhahn mit 1 1/4 Pop-up-Abfall"))
						.put(Lang.FRA, getWords("Robinet d'évier avec 1 1/4 pop up déchets")).build());
		translates
				.put(getCode("30.3 (770 mm) round built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("30,3 (770 мм) круглой встроенной душевой головкой"))
						.put(Lang.ITA, getWords("Soffione da incasso rotondo da 30,3 (770 mm)"))
						.put(Lang.ESP, getWords("Cabezal de ducha redondo de 30.3 (770 mm)"))
						.put(Lang.DEU, getWords("30,3 (770 mm) runde Duschbrause"))
						.put(Lang.FRA, getWords("Pomme de douche encastrée ronde de 30,3 po (770 mm)")).build());
		translates
				.put(getCode("22 jets and 7 air nozzles in the spa"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("22 форсунки и 7 воздушных сопел в спа-салоне"))
						.put(Lang.ITA, getWords("22 getti e 7 ugelli d'aria nella spa"))
						.put(Lang.ESP, getWords("22 jets y 7 boquillas de aire en el spa"))
						.put(Lang.DEU, getWords("22 Düsen und 7 Luftdüsen im Spa"))
						.put(Lang.FRA, getWords("22 jets et 7 buses d'air dans le spa")).build());
		translates
				.put(getCode("Suitable for indoor and outdoor use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходит для внутреннего и наружного использования"))
						.put(Lang.ITA, getWords("Adatto per uso interno ed esterno"))
						.put(Lang.ESP, getWords("Adecuado para uso en interiores y al aire libre"))
						.put(Lang.DEU, getWords("Geeignet für den Innen- und Außenbereich"))
						.put(Lang.FRA, getWords("Convient pour une utilisation intérieure et extérieure")).build());
		translates
				.put(getCode("Maximum water capacity - 2000 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 2000 литров"))
						.put(Lang.ITA, getWords("Capacità massima dell'acqua - 2000 litri"))
						.put(Lang.ESP, getWords("Capacidad máxima de agua - 2000 Litros"))
						.put(Lang.DEU, getWords("Maximale Wasserkapazität - 2000 Liter"))
						.put(Lang.FRA, getWords("Capacité maximale en eau - 2000 litres")).build());
		translates
				.put(getCode("Large overflow slots for quick draining"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большие слоты для переполнения для быстрого слива"))
						.put(Lang.ITA, getWords("Grandi slot di troppo pieno per uno scarico rapido"))
						.put(Lang.ESP, getWords("Ranuras de desbordamiento grandes para drenaje rápido"))
						.put(Lang.DEU, getWords("Große Überlaufschlitze für schnelles Entleeren"))
						.put(Lang.FRA, getWords("Grandes fentes de débordement pour une vidange rapide")).build());
		translates
				.put(getCode("Chromotherapy with multiple perimeter LEDs for infinity edge"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромотерапия с несколькими светодиодами периметра для края бесконечности"))
						.put(Lang.ITA, getWords("Cromoterapia con LED perimetrali multipli per bordo infinito"))
						.put(Lang.ESP, getWords("Cromoterapia con múltiples LED perimetrales para el borde infinito"))
						.put(Lang.DEU, getWords("Chromotherapie mit mehreren Perimeter-LEDs für die Infinity-Kante"))
						.put(Lang.FRA, getWords("Chromothérapie avec plusieurs DEL périmétriques pour le bord de l'infini")).build());
		translates
				.put(getCode("Complies with ASME A112.18.2-2005 and CSA B125.2-05"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствует ASME A112.18.2-2005 и CSA B125.2-05"))
						.put(Lang.ITA, getWords("Conforme a ASME A112.18.2-2005 e CSA B125.2-05"))
						.put(Lang.ESP, getWords("Cumple con ASME A112.18.2-2005 y CSA B125.2-05"))
						.put(Lang.DEU, getWords("Entspricht ASME A112.18.2-2005 und CSA B125.2-05"))
						.put(Lang.FRA, getWords("Conforme à ASME A112.18.2-2005 et CSA B125.2-05")).build());
		translates
				.put(getCode("Unique Infinity R1 soft gel headrests can be purchased upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальные мягкие гвоздики Infinity R1 можно приобрести по запросу"))
						.put(Lang.ITA, getWords("Su richiesta, è possibile acquistare esclusivi poggiatesta in gel morbido Infinity R1"))
						.put(Lang.ESP, getWords("Reposacabezas de gel suave Infinity R1 únicos se pueden comprar a pedido"))
						.put(Lang.DEU, getWords("Einzigartige Infinity R1 Soft Gel Kopfstützen können auf Anfrage erworben werden"))
						.put(Lang.FRA, getWords("Les appuie-têtes en gel souple Infinity R1 uniques peuvent être achetés sur demande")).build());
		translates
				.put(getCode("ABS base for additional insulation, stability and structural integrity"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Основание ABS для дополнительной изоляции, стабильности и целостности конструкции"))
						.put(Lang.ITA, getWords("Base in ABS per ulteriore isolamento, stabilità e integrità strutturale"))
						.put(Lang.ESP, getWords("Base ABS para aislamiento adicional, estabilidad e integridad estructural"))
						.put(Lang.DEU, getWords("ABS-Basis für zusätzliche Isolierung, Stabilität und strukturelle Integrität"))
						.put(Lang.FRA, getWords("Base en ABS pour plus d'isolation, de stabilité et d'intégrité structurelle")).build());
		translates
				.put(getCode("Large 15x15 (380x380 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая насадка 15x15 (380x380 мм)"))
						.put(Lang.ITA, getWords("Soffione grande 15x15 (380x380 mm)"))
						.put(Lang.ESP, getWords("Cabezal de ducha grande de 15x15 (380x380 mm)"))
						.put(Lang.DEU, getWords("Großer 15x15 (380x380 mm) Duschkopf"))
						.put(Lang.FRA, getWords("Grande pomme de douche 15x15 (380x380 mm)")).build());
		translates
				.put(getCode("Sofa and seats are supported by polyester belts specifically formulated for outdoor use and coordinated to the frame colour."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Диван и сиденья поддерживаются полиэфирными лентами, специально разработанными для использования на открытом воздухе и согласованными с цветом рамы."))
						.put(Lang.ITA, getWords("Divano e sedili sono supportati da cinghie in poliestere appositamente formulate per uso esterno e coordinate al colore del telaio."))
						.put(Lang.ESP, getWords("El sofá y los asientos están respaldados por correas de poliéster específicamente formuladas para uso en exteriores y coordinadas con el color del marco."))
						.put(Lang.DEU, getWords("Sofa und Sitze werden von Polyestergurten getragen, die speziell für den Außenbereich entwickelt wurden und auf die Gestellfarbe abgestimmt sind."))
						.put(Lang.FRA, getWords("Le sofa et les sièges sont soutenus par des ceintures de polyester spécifiquement formulées pour l'usage extérieur et coordonnées à la couleur de cadre.")).build());
		translates
				.put(getCode("Compact, super small footprint corner bathtub intended to fit spaces where no other bathtub would ever fit"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Компактная, очень маленькая угловая ванна для ног, предназначенная для помещений, где никакая другая ванна никогда не подойдет"))
						.put(Lang.ITA, getWords("Vasca angolare compatta a ingombro ridotto, progettata per adattarsi a spazi in cui nessuna vasca potrebbe mai adattarsi"))
						.put(Lang.ESP, getWords("Bañera de esquina pequeña y compacta, diseñada para adaptarse a espacios donde ninguna otra bañera cabría"))
						.put(Lang.DEU, getWords("Kompakte, sehr kleine Eckbadewanne für Räume, in die keine andere Badewanne passen würde"))
						.put(Lang.FRA, getWords("Baignoire d'angle compacte et super compacte conçue pour les espaces où aucune autre baignoire ne serait adaptée")).build());
		translates
				.put(getCode("Silver finishing aluminum profiles"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Серебряные отделочные алюминиевые профили"))
						.put(Lang.ITA, getWords("Profili in alluminio finitura argento"))
						.put(Lang.ESP, getWords("Perfiles de aluminio con acabado plateado"))
						.put(Lang.DEU, getWords("Silberfinish-Aluminiumprofile"))
						.put(Lang.FRA, getWords("Profilés en aluminium finition argent")).build());
		translates
				.put(getCode("The optional Multiplex E Electronic Bath Filler, Diverter & Motorized Drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительный электронный наполнитель Multiplex E, дивертер и моторизованный слив"))
						.put(Lang.ITA, getWords("Il dispositivo di riempimento elettronico per vasca Multiplex E, deviatore e scarico motorizzato"))
						.put(Lang.ESP, getWords("El relleno de baño electrónico opcional Multiplex E, desviador y drenaje motorizado"))
						.put(Lang.DEU, getWords("Der optionale elektronische Badewannenfüller Multiplex E, Umsteller und motorisierter Abfluss"))
						.put(Lang.FRA, getWords("La remplisseuse de bain électronique Multiplex E en option, déviateur et drain motorisé")).build());
		translates
				.put(getCode("IAPMO (cUPC) certified with non-corrugated, flexible-tubing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("IAPMO (cUPC), сертифицированный с использованием не гофрированных гибких труб"))
						.put(Lang.ITA, getWords("IAPMO (cUPC) certificato con tubi flessibili non corrugati"))
						.put(Lang.ESP, getWords("IAPMO (cUPC) certificado con tubería flexible no corrugada"))
						.put(Lang.DEU, getWords("IAPMO (cUPC) zertifiziert mit nicht gewellten, flexiblen Schläuchen"))
						.put(Lang.FRA, getWords("IAPMO (cUPC) certifié avec tubes flexibles non ondulés")).build());
		translates
				.put(getCode("Square profile, slimline shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Квадратный профиль, тонкая душевая головка"))
						.put(Lang.ITA, getWords("Profilo quadrato, soffione sottile"))
						.put(Lang.ESP, getWords("Perfil cuadrado, cabezal de ducha delgado"))
						.put(Lang.DEU, getWords("Quadratisches Profil, schmaler Duschkopf"))
						.put(Lang.FRA, getWords("Profil carré, pomme de douche slimline")).build());
		translates
				.put(getCode("Lights to indicate temperature and water flow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Загорается, чтобы указать температуру и расход воды"))
						.put(Lang.ITA, getWords("Luci per indicare la temperatura e il flusso dell'acqua"))
						.put(Lang.ESP, getWords("Luces para indicar la temperatura y el flujo de agua"))
						.put(Lang.DEU, getWords("Leuchtet, um Temperatur und Wasserfluss anzuzeigen"))
						.put(Lang.FRA, getWords("Lumières pour indiquer la température et le débit d'eau")).build());
		translates
				.put(getCode("1200 x 700 mm (47.25 x 27.5 in) wall mirror with built-in LED strip lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1200 x 700 мм (47,25 x 27,5 дюйма) настенное зеркало со встроенным светодиодным освещением"))
						.put(Lang.ITA, getWords("Specchio da parete 1200 x 700 mm (47,25 x 27,5 pollici) con illuminazione a strisce LED incorporata"))
						.put(Lang.ESP, getWords("Espejo de pared de 1200 x 700 mm (47.25 x 27.5 in) con iluminación incorporada en tira de LED"))
						.put(Lang.DEU, getWords("1200 x 700 mm (47,25 x 27,5 in) Wandspiegel mit integrierter LED-Streifenbeleuchtung"))
						.put(Lang.FRA, getWords("Miroir mural de 1200 x 700 mm (47,25 x 27,5 po) avec éclairage à DEL intégré")).build());
		translates
				.put(getCode("Made of AquateX™ award-winning composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изготовлен из композитного материала AquateX ™, отмеченного наградами"))
						.put(Lang.ITA, getWords("Realizzato con il composito pluripremiato AquateX ™"))
						.put(Lang.ESP, getWords("Hecho de compuesto premiado AquateX ™"))
						.put(Lang.DEU, getWords("Hergestellt aus dem preisgekrönten AquateX ™ Composite"))
						.put(Lang.FRA, getWords("Fabriqué en composite composite primé AquateX ™")).build());
		translates
				.put(getCode("Extra deep, perfect for full body soaks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра глубокий, идеально подходит для полного ухода за телом"))
						.put(Lang.ITA, getWords("Molto profondo, perfetto per il bagno completo"))
						.put(Lang.ESP, getWords("Extra profundo, perfecto para baños de cuerpo completo"))
						.put(Lang.DEU, getWords("Extra tief, perfekt für Ganzkörper-Soaks"))
						.put(Lang.FRA, getWords("Extra profond, parfait pour le corps entier imbibe")).build());
		translates
				.put(getCode("Nine RGB LED lights"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Девять светодиодов RGB"))
						.put(Lang.ITA, getWords("Nove luci a LED RGB"))
						.put(Lang.ESP, getWords("Nueve luces LED RGB"))
						.put(Lang.DEU, getWords("Neun RGB-LED-Leuchten"))
						.put(Lang.FRA, getWords("Neuf lumières LED RGB")).build());
		translates
				.put(getCode("Built-in metal base frame and adjustable height metal legs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная металлическая опорная рама и регулируемые по высоте металлические ножки"))
						.put(Lang.ITA, getWords("Basamento metallico incorporato e gambe in metallo regolabili in altezza"))
						.put(Lang.ESP, getWords("Marco base de metal incorporado y patas metálicas de altura ajustable"))
						.put(Lang.DEU, getWords("Eingebauter Metallfußrahmen und höhenverstellbare Metallbeine"))
						.put(Lang.FRA, getWords("Structure de base en métal intégrée et pieds en métal réglables en hauteur")).build());
		translates
				.put(getCode("Attractive chrome, brushed nickel or old brass finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Привлекательный хром, матовый никель или старая латунь"))
						.put(Lang.ITA, getWords("Attraente cromatura, nichel spazzolato o finitura ottone antico"))
						.put(Lang.ESP, getWords("Cromo atractivo, níquel cepillado o acabado en latón envejecido"))
						.put(Lang.DEU, getWords("Attraktives Chrom, gebürstetes Nickel oder Messing alt"))
						.put(Lang.FRA, getWords("Attrayant chrome, nickel brossé ou vieux laiton")).build());
		translates
				.put(getCode("Useful widened rim designed to accommodate certain bath filler models"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полезный расширенный обод, предназначенный для размещения некоторых моделей наполнителей ванны"))
						.put(Lang.ITA, getWords("Orlo allargato utile progettato per adattarsi a determinati modelli di riempitivi per il bagno"))
						.put(Lang.ESP, getWords("Borde ensanchado útil diseñado para acomodar ciertos modelos de llenado de baño"))
						.put(Lang.DEU, getWords("Nützliche verbreiterte Felge, die für bestimmte Modelle von Wannenfüllern geeignet ist"))
						.put(Lang.FRA, getWords("Jante élargie utile conçue pour recevoir certains modèles de remplisseuses de baignoire")).build());
		translates
				.put(getCode("24 x air massage jets and 47 x optional air massage jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("24 струй воздуха для воздуха и 47 х дополнительных воздушных массажных струй"))
						.put(Lang.ITA, getWords("24 x getti d'aria e 47 x getti d'aria opzionali"))
						.put(Lang.ESP, getWords("24 jets de masaje de aire y 47 x chorros de masaje de aire opcionales"))
						.put(Lang.DEU, getWords("24 x Luftmassagedüsen und 47 x optionale Luftmassagedüsen"))
						.put(Lang.FRA, getWords("24 jets de massage à air et 47 jets de massage à air en option")).build());
		translates
				.put(getCode("Spa with ABS Base"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Спа с базой ABS"))
						.put(Lang.ITA, getWords("Spa con base in ABS"))
						.put(Lang.ESP, getWords("Spa con base ABS"))
						.put(Lang.DEU, getWords("Spa mit ABS-Basis"))
						.put(Lang.FRA, getWords("Spa avec base en ABS")).build());
		translates
				.put(getCode("Tall wooden library cabinet/bookshelf in a lacquered dull white finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высокий деревянный библиотечный шкаф / книжная полка в лакированной тусклой белой отделке"))
						.put(Lang.ITA, getWords("Libreria alta libreria / libreria in legno laccato bianco opaco"))
						.put(Lang.ESP, getWords("Biblioteca / estante alto de biblioteca de madera en acabado blanco opaco lacado"))
						.put(Lang.DEU, getWords("Hoher Bibliothekschrank aus Holz / Bücherregal in lackiertem, mattweißem Finish"))
						.put(Lang.FRA, getWords("Grande bibliothèque / bibliothèque en bois laqué blanc mat")).build());
		translates
				.put(getCode("Dual action water sanitization system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Двойная система водоочистки"))
						.put(Lang.ITA, getWords("Sistema di sanificazione dell'acqua a doppia azione"))
						.put(Lang.ESP, getWords("Sistema de desinfección de agua de doble acción"))
						.put(Lang.DEU, getWords("Dual-Action-Wasserdesinfektionssystem"))
						.put(Lang.FRA, getWords("Système d'assainissement à double action de l'eau")).build());
		translates
				.put(getCode("On/Off Remote control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Вкл. / Выкл."))
						.put(Lang.ITA, getWords("On / Off Controllo remoto"))
						.put(Lang.ESP, getWords("Control remoto encendido / apagado"))
						.put(Lang.DEU, getWords("Ein / Aus Fernbedienung"))
						.put(Lang.FRA, getWords("On / Off Télécommande")).build());
		translates
				.put(getCode("This item can be purchased with marble or stone shower control handle upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот предмет можно приобрести с помощью ручки управления мраморным или каменным душем по запросу"))
						.put(Lang.ITA, getWords("Su richiesta questo articolo può essere acquistato con maniglia di comando per doccia in marmo o pietra"))
						.put(Lang.ESP, getWords("Este artículo se puede comprar con mango de control de ducha de mármol o piedra a pedido"))
						.put(Lang.DEU, getWords("Dieser Artikel kann auf Anfrage mit Marmor- oder Steinduschgriff erhalten werden"))
						.put(Lang.FRA, getWords("Cet article peut être acheté avec le marbre ou la poignée en pierre de commande de douche sur demande")).build());
		translates
				.put(getCode("Premium design freestanding bathtub with roomy interiors"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-дизайн отдельно стоящей ванной с просторными интерьерами"))
						.put(Lang.ITA, getWords("Vasca da bagno freestanding di design di alta qualità con interni spaziosi"))
						.put(Lang.ESP, getWords("Bañera independiente de diseño premium con interiores espaciosos"))
						.put(Lang.DEU, getWords("Freistehende Premium-Design-Badewanne mit geräumigen Innenräumen"))
						.put(Lang.FRA, getWords("Baignoire autoportante de conception premium avec des intérieurs spacieux")).build());
		translates
				.put(getCode("Colour Sense chromotherapy system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система хромотерапии Color Sense"))
						.put(Lang.ITA, getWords("Sistema cromoterapico Color Sense"))
						.put(Lang.ESP, getWords("Sistema de cromoterapia Color Sense"))
						.put(Lang.DEU, getWords("Color Sense Farbtherapiesystem"))
						.put(Lang.FRA, getWords("Système de chromothérapie Color Sense")).build());
		translates
				.put(getCode("Drain cover is of the same material as the tub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дренажная крышка имеет тот же материал, что и ванна"))
						.put(Lang.ITA, getWords("La copertura dello scarico è dello stesso materiale della vasca"))
						.put(Lang.ESP, getWords("La tapa del desagüe es del mismo material que la bañera"))
						.put(Lang.DEU, getWords("Die Ablaufabdeckung besteht aus dem gleichen Material wie die Wanne"))
						.put(Lang.FRA, getWords("La couverture de drainage est du même matériau que la baignoire")).build());
		translates
				.put(getCode("Wooden, under basin furniture piece with drawers in lacquered white finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Деревянная мебель под плитой с ящиками в лакированной белой отделке"))
						.put(Lang.ITA, getWords("Mobile in legno, sotto lavabo con cassetti in finitura laccato bianco"))
						.put(Lang.ESP, getWords("Mueble de madera bajo lavabo con cajones en acabado blanco lacado"))
						.put(Lang.DEU, getWords("Untertischmöbel aus Holz mit Schubladen in weiß lackierter Ausführung"))
						.put(Lang.FRA, getWords("Meuble sous lavabo en bois avec tiroirs laqué blanc")).build());
		translates
				.put(getCode("Hot and cold integrated thermostatic control valve with anti-scalding safety mechanism"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Горячий и холодный встроенный термостатический регулирующий клапан с противоскользящим предохранительным механизмом"))
						.put(Lang.ITA, getWords("Valvola di controllo termostatica integrata calda e fredda con meccanismo di sicurezza antiscottatura"))
						.put(Lang.ESP, getWords("Válvula de control termostático integrado caliente y frío con mecanismo de seguridad anti-escaldado"))
						.put(Lang.DEU, getWords("Warmes und kaltes thermostatisches Regelventil mit Sicherheit gegen Verbrühung"))
						.put(Lang.FRA, getWords("Vanne de régulation thermostatique intégrée chaude et froide avec mécanisme de sécurité anti-brûlure")).build());
		translates
				.put(getCode("Available with optional hydromassage system or Tranquility system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно с дополнительной гидромассажной системой или системой Tranquility"))
						.put(Lang.ITA, getWords("Disponibile con sistema idromassaggio opzionale o sistema Tranquility"))
						.put(Lang.ESP, getWords("Disponible con sistema de hidromasaje opcional o sistema Tranquility"))
						.put(Lang.DEU, getWords("Erhältlich mit optionalem Hydromassagesystem oder Tranquility-System"))
						.put(Lang.FRA, getWords("Disponible avec système d'hydromassage en option ou système Tranquility")).build());
		translates
				.put(getCode("Pure Line exterior perimeter lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чистое внешнее освещение периметра"))
						.put(Lang.ITA, getWords("Illuminazione perimetrale esterna Pure Line"))
						.put(Lang.ESP, getWords("Iluminación perimetral exterior Pure Line"))
						.put(Lang.DEU, getWords("Pure Line Außenumfangsbeleuchtung"))
						.put(Lang.FRA, getWords("Éclairage périmétrique extérieur Pure Line")).build());
		translates
				.put(getCode("Stainless steel finish controls with solid brass construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Элементы управления из нержавеющей стали с твердой латунной конструкцией"))
						.put(Lang.ITA, getWords("Comandi in acciaio inox con costruzione in ottone massiccio"))
						.put(Lang.ESP, getWords("Controles de acabado de acero inoxidable con construcción de latón macizo"))
						.put(Lang.DEU, getWords("Edelstahl-Finish-Bedienelemente mit massiver Messingkonstruktion"))
						.put(Lang.FRA, getWords("Commandes de finition en acier inoxydable avec construction en laiton massif")).build());
	}

	public static void part5() {
		translates
				.put(getCode("Wall-mounted at recommended installation of 40cm above user’s height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный монтаж при рекомендуемой установке на 40 см выше высоты пользователя"))
						.put(Lang.ITA, getWords(" Installazione a parete con l'installazione consigliata di 40 cm sopra l'altezza dell'utente"))
						.put(Lang.ESP, getWords(" Montado en la pared en la instalación recomendada de 40 cm por encima de la altura del usuario"))
						.put(Lang.DEU, getWords(" Wandmontage bei empfohlener Installation von 40 cm über der Benutzerhöhe"))
						.put(Lang.FRA, getWords(" Mural à l'installation recommandée de 40 cm au-dessus de la hauteur de l'utilisateur")).build());
		translates
				.put(getCode("25-years limited structural integrity warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("25-летняя ограниченная гарантия целостности"))
						.put(Lang.ITA, getWords(" Garanzia di integrità strutturale limitata di 25 anni"))
						.put(Lang.ESP, getWords(" Garantía de integridad estructural limitada de 25 años"))
						.put(Lang.DEU, getWords(" 25 Jahre eingeschränkte Gewährleistung der strukturellen Integrität"))
						.put(Lang.FRA, getWords(" Garantie limitée de 25 ans sur l'intégrité structurale")).build());
		translates
				.put(getCode("240V 50/60Hz power with 20A GFCI Protected"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("240В 50/60 Гц с защитой 20А GFCI"))
						.put(Lang.ITA, getWords(" Alimentazione 240V 50 / 60Hz con GFCI 20A protetta"))
						.put(Lang.ESP, getWords(" Potencia de 240V 50 / 60Hz con 20 GFCI protegido"))
						.put(Lang.DEU, getWords(" 240V 50 / 60Hz Leistung mit 20A GFCI geschützt"))
						.put(Lang.FRA, getWords(" Puissance de 240V 50 / 60Hz avec 20FC GFCI protégé")).build());
		translates
				.put(getCode("1/2” water inlet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Впускное отверстие 1/2 "))
						.put(Lang.ITA, getWords(" Ingresso acqua 1/2 "))
						.put(Lang.ESP, getWords(" Entrada de agua de 1/2 "))
						.put(Lang.DEU, getWords(" 1/2 Wassereinlass"))
						.put(Lang.FRA, getWords(" Entrée d'eau 1/2 ")).build());
		translates
				.put(getCode("Fits most bathtubs with built in overflow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходит для большинства ванн со встроенным переливом"))
						.put(Lang.ITA, getWords(" Si adatta alla maggior parte delle vasche da bagno con troppopieno incorporato"))
						.put(Lang.ESP, getWords(" Se adapta a la mayoría de las bañeras con desbordamiento incorporado"))
						.put(Lang.DEU, getWords(" Passt für die meisten Badewannen mit eingebautem Überlauf"))
						.put(Lang.FRA, getWords(" Convient à la plupart des baignoires avec trop-plein intégré")).build());
		translates
				.put(getCode("7.75” x 4.75” (200 x 120 mm) metal wall plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("7,75 x 4,75 (200 x 120 мм) металлическая настенная плита"))
						.put(Lang.ITA, getWords(" Placca di metallo da 7,75 x 4,75 (200 x 120 mm)"))
						.put(Lang.ESP, getWords(" Placa de pared de metal de 7.75 x 4.75 (200 x 120 mm)"))
						.put(Lang.DEU, getWords(" 7,75 x 4,75 (200 x 120 mm) Metallwandplatte"))
						.put(Lang.FRA, getWords(" Plaque murale en métal de 7,75 po x 4,75 po (200 x 120 mm)")).build());
		translates
				.put(getCode("Available for Left or Right Corners"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно для левого или правого угла"))
						.put(Lang.ITA, getWords(" Disponibile per angoli sinistro o destro"))
						.put(Lang.ESP, getWords(" Disponible para esquinas izquierda o derecha"))
						.put(Lang.DEU, getWords(" Verfügbar für linke oder rechte Ecken"))
						.put(Lang.FRA, getWords(" Disponible pour les coins gauche ou droit")).build());
		translates
				.put(getCode("In-wall dual wheel electronic interface with up to three individually saved settings"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенный двухколесный электронный интерфейс с тремя индивидуально сохраненными настройками"))
						.put(Lang.ITA, getWords(" Interfaccia elettronica a doppia ruota integrata nella parete con fino a tre impostazioni salvate singolarmente"))
						.put(Lang.ESP, getWords(" Interfaz electrónica de doble rueda en la pared con hasta tres configuraciones guardadas individualmente"))
						.put(Lang.DEU, getWords(" In-Wall-Dualrad-Elektronikschnittstelle mit bis zu drei individuell gespeicherten Einstellungen"))
						.put(Lang.FRA, getWords(" Interface électronique double roue intégrée avec jusqu'à trois réglages enregistrés individuellement")).build());
		translates
				.put(getCode("Built-in, semi built-in or freestanding construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная, полуавтоматическая или автономная конструкция"))
						.put(Lang.ITA, getWords(" Built-in, semi-built-in o costruzione indipendente"))
						.put(Lang.ESP, getWords(" Construcción incorporada, semi empotrada o independiente"))
						.put(Lang.DEU, getWords(" Eingebaute, halb eingebaute oder freistehende Konstruktion"))
						.put(Lang.FRA, getWords(" Construction encastrable, semi-encastrée ou autoportante")).build());
		translates
				.put(getCode("Ergonomically designed built-in seat for comfortable bathing experience"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичное встроенное сиденье для комфортного купания"))
						.put(Lang.ITA, getWords(" Sedile incorporato dal design ergonomico per un'esperienza di bagno confortevole"))
						.put(Lang.ESP, getWords(" Asiento incorporado ergonómicamente diseñado para una experiencia de baño cómoda"))
						.put(Lang.DEU, getWords(" Ergonomisch geformter Einbausitz für komfortables Badeerlebnis"))
						.put(Lang.FRA, getWords(" Siège intégré ergonomique pour une expérience de bain confortable")).build());
		translates
				.put(getCode("Extremely durable and retains heat for much longer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чрезвычайно долговечен и сохраняет тепло дольше"))
						.put(Lang.ITA, getWords(" Estremamente resistente e mantiene il calore per molto più a lungo"))
						.put(Lang.ESP, getWords(" Extremadamente durable y retiene el calor por mucho más tiempo"))
						.put(Lang.DEU, getWords(" Extrem haltbar und speichert die Wärme viel länger"))
						.put(Lang.FRA, getWords(" Extrêmement durable et retient la chaleur pour beaucoup plus longtemps")).build());
		translates
				.put(getCode("Standard 1 1/2 threaded waste outlet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стандартная резьбовая розетка 1 1/2 "))
						.put(Lang.ITA, getWords(" Scarico filettato standard da 1 1/2 "))
						.put(Lang.ESP, getWords(" Salida de desechos roscada estándar de 1 1/2 "))
						.put(Lang.DEU, getWords(" Standard 1 1/2 Gewindeauslauf"))
						.put(Lang.FRA, getWords(" Sortie de vidange filetée standard de 1 1/2 ")).build());
		translates
				.put(getCode("Convenient integrated waterproof mirror"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобное интегрированное водонепроницаемое зеркало"))
						.put(Lang.ITA, getWords(" Comodo specchio impermeabile integrato"))
						.put(Lang.ESP, getWords(" Conveniente espejo impermeable integrado"))
						.put(Lang.DEU, getWords(" Praktischer integrierter wasserdichter Spiegel"))
						.put(Lang.FRA, getWords(" Miroir étanche intégré pratique")).build());
		translates
				.put(getCode("Avaliable in four colors: white, black, black and white or bronze"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступны в четырех цветах: белый, черный, черный и белый или бронзовый"))
						.put(Lang.ITA, getWords(" Disponibile in quattro colori: bianco, nero, nero e bianco o bronzo"))
						.put(Lang.ESP, getWords(" Disponible en cuatro colores: blanco, negro, blanco y negro o bronce"))
						.put(Lang.DEU, getWords(" Erhältlich in vier Farben: Weiß, Schwarz, Schwarz und Weiß oder Bronze"))
						.put(Lang.FRA, getWords(" Disponible en quatre couleurs: blanc, noir, noir et blanc ou bronze")).build());
		translates
				.put(getCode("1 ½  flanged brass tube for Aquatica cable-drive and concealed bath wastes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 ½ фланцевая латунная трубка для кабельного привода Aquatica и скрытые ванны"))
						.put(Lang.ITA, getWords(" Tubo in ottone flangiato da 1 ½ per cavo-guida Aquatica e rifiuti da bagno a scomparsa"))
						.put(Lang.ESP, getWords(" Tubo de latón con brida de 1 ½ para cableado Aquatica y residuos de baño ocultos"))
						.put(Lang.DEU, getWords(" 1 ½ geflanschtes Messingrohr für Aquatica Kabelantrieb und verdeckte Badabfälle"))
						.put(Lang.FRA, getWords(" Tube en laiton à bride de 1 ½ pour l'entraînement par câble Aquatica et les déchets de bain dissimulés")).build());
		translates
				.put(getCode("The stylish Bollicine Shower Control has external bath/shower mixer and two outlet diverter"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стильный Bollicine Shower Control имеет внешний смеситель для ванны / душа и два выходных дивертора"))
						.put(Lang.ITA, getWords(" L'elegante Bollicine Shower Control ha un miscelatore esterno vasca / doccia e due deviatori"))
						.put(Lang.ESP, getWords(" El elegante Bollicine Shower Control tiene un mezclador externo de baño / ducha y dos desviadores de salida"))
						.put(Lang.DEU, getWords(" Die stilvolle Bollicine Shower Control verfügt über einen externen Wannen / Brausemischer und zwei Auslaufweichen"))
						.put(Lang.FRA, getWords(" L'élégant Bollicine Shower Control dispose d'un mitigeur bain / douche et d'un inverseur à deux sorties")).build());
		translates
				.put(getCode("Round Rain style shower head design, stainless steel construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Круглый дизайн душевой кабины «Дождь», конструкция из нержавеющей стали"))
						.put(Lang.ITA, getWords(" Soffione tondo in stile Rain, costruzione in acciaio inox"))
						.put(Lang.ESP, getWords(" Diseño de cabezal de ducha estilo lluvia redondo, construcción en acero inoxidable"))
						.put(Lang.DEU, getWords(" Round Rain -Duschkopf Design, Edelstahlkonstruktion"))
						.put(Lang.FRA, getWords(" Pomme de douche ronde Rain, construction en acier inoxydable")).build());
		translates
				.put(getCode("Arm length 6 (15 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукава 6 (15 см)"))
						.put(Lang.ITA, getWords(" Braccio lunghezza 6 (15 cm)"))
						.put(Lang.ESP, getWords(" Longitud del brazo 6 (15 cm)"))
						.put(Lang.DEU, getWords(" Armlänge 6 (15 cm)"))
						.put(Lang.FRA, getWords(" Longueur de bras 6 (15 cm)")).build());
		translates
				.put(getCode("Heated seat with variable temperature setting (3 levels)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подогрев сиденья с переменной температурой (3 уровня)"))
						.put(Lang.ITA, getWords(" Sedile riscaldato con regolazione della temperatura variabile (3 livelli)"))
						.put(Lang.ESP, getWords(" Asiento calefaccionado con ajuste de temperatura variable (3 niveles)"))
						.put(Lang.DEU, getWords(" Sitzheizung mit variabler Temperatureinstellung (3 Stufen)"))
						.put(Lang.FRA, getWords(" Siège chauffant avec réglage de température variable (3 niveaux)")).build());
		translates
				.put(getCode("This item set includes a concealed body (CS 800) with an option of 1 or 2 or 3 outlets 1/2”"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор предметов включает скрытый корпус (CS 800) с возможностью 1 или 2 или 3 розетки 1/2 "))
						.put(Lang.ITA, getWords(" Questo set di articoli include un corpo nascosto (CS 800) con un'opzione di 1 o 2 o 3 uscite 1/2 "))
						.put(Lang.ESP, getWords(" Este conjunto de elementos incluye un cuerpo oculto (CS 800) con una opción de 1 o 2 o 3 salidas de 1/2 "))
						.put(Lang.DEU, getWords(" Dieses Set beinhaltet einen Unterputzkörper (CS 800) mit einer Option von 1 oder 2 oder 3 Ausgängen 1/2 "))
						.put(Lang.FRA, getWords(" Cet ensemble d'articles comprend un corps dissimulé (CS 800) avec une option de 1 ou 2 ou 3 sorties 1/2 ")).build());
		translates
				.put(getCode("Fingertip control of temperature mixing and flow rate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Управление контуром температурного смешивания и расхода"))
						.put(Lang.ITA, getWords(" Controllo a portata di mano della miscelazione e della portata della temperatura"))
						.put(Lang.ESP, getWords(" Control de la temperatura de la mezcla y el caudal"))
						.put(Lang.DEU, getWords(" Fingertip Kontrolle der Temperaturmischung und Durchflussrate"))
						.put(Lang.FRA, getWords(" Contrôle du bout des doigts du mélange de température et du débit")).build());
		translates
				.put(getCode("Posterior cleansing with a variable spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Задняя очистка с переменным спреем"))
						.put(Lang.ITA, getWords(" Pulizia posteriore con spray variabile"))
						.put(Lang.ESP, getWords(" Limpieza posterior con un spray variable"))
						.put(Lang.DEU, getWords(" Nachreinigung mit einem variablen Spray"))
						.put(Lang.FRA, getWords(" Nettoyage postérieur avec un jet variable")).build());
		translates
				.put(getCode("Classically rounded rectangular shape"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классически округленная прямоугольная форма"))
						.put(Lang.ITA, getWords(" Forma rettangolare classica arrotondata"))
						.put(Lang.ESP, getWords(" Forma rectangular clásica redondeada"))
						.put(Lang.DEU, getWords(" Klassisch abgerundete rechteckige Form"))
						.put(Lang.FRA, getWords(" Forme rectangulaire arrondie classique")).build());
		translates
				.put(getCode("Made in Europe of solely European sourced components"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано в Европе исключительно из европейских источников"))
						.put(Lang.ITA, getWords(" Prodotto in Europa con componenti esclusivamente europei"))
						.put(Lang.ESP, getWords(" Hecho en Europa de componentes exclusivamente de origen europeo"))
						.put(Lang.DEU, getWords(" Hergestellt in Europa aus ausschließlich europäischen Komponenten"))
						.put(Lang.FRA, getWords(" Fabriqué en Europe avec des composants exclusivement européens")).build());
		translates
				.put(getCode("Progressive sink faucet with 11/4 Up&Down waste"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прогрессивный смеситель для раковины с 1 1/4 Up & Down waste"))
						.put(Lang.ITA, getWords(" Rubinetto progressivo per lavello con scarico Up & Down da 1 1/4"))
						.put(Lang.ESP, getWords(" Grifo de fregadero progresivo con desechos de 1 1/4 Arriba y Abajo"))
						.put(Lang.DEU, getWords(" Progressive Waschbecken Wasserhahn mit 1 1/4 Up & Down Abfall"))
						.put(Lang.FRA, getWords(" Robinet d'évier progressif avec 1 1/4 up & down waste")).build());
		translates
				.put(getCode("Unique space-conscious design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальный космический дизайн"))
						.put(Lang.ITA, getWords(" Design unico e attento allo spazio"))
						.put(Lang.ESP, getWords(" Diseño único consciente del espacio"))
						.put(Lang.DEU, getWords(" Einzigartiges, raumbewusstes Design"))
						.put(Lang.FRA, getWords(" Conception unique et sensible à l'espace")).build());
		translates
				.put(getCode("Designer face plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Лицевая панель дизайнера"))
						.put(Lang.ITA, getWords(" Frontale di design"))
						.put(Lang.ESP, getWords(" Placa frontal de diseño"))
						.put(Lang.DEU, getWords(" Designer-Frontplatte"))
						.put(Lang.FRA, getWords(" Plaque de devant design")).build());
		translates
				.put(getCode("No damage from errant screwdrivers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Отсутствие повреждений от ошибочных отверток"))
						.put(Lang.ITA, getWords(" Nessun danno causato da cacciaviti errati"))
						.put(Lang.ESP, getWords(" Sin daños por destornilladores errantes"))
						.put(Lang.DEU, getWords(" Keine Beschädigung durch fehlerhafte Schraubendreher"))
						.put(Lang.FRA, getWords(" Aucun dommage de tournevis errant")).build());
		translates
				.put(getCode("3-Hole installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Установка с 3 отверстиями"))
						.put(Lang.ITA, getWords(" Installazione a 3 fori"))
						.put(Lang.ESP, getWords(" Instalación de 3 agujeros"))
						.put(Lang.DEU, getWords(" 3-Loch-Installation"))
						.put(Lang.FRA, getWords(" Installation à 3 trous")).build());
		translates
				.put(getCode("Matt yellow lacquer open front shelf – 500 x 365 x 250 mm / 19.75 x 14.25 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовая желтая лаковая открытая передняя полка - 500 x 365 x 250 мм / 19,75 x 14,25 x 10 дюймов"))
						.put(Lang.ITA, getWords(" Ripiano anteriore aperto laccato giallo opaco - 500 x 365 x 250 mm / 19,75 x 14,25 x 10 in"))
						.put(Lang.ESP, getWords(" Estante frontal abierto lacado en color amarillo mate - 500 x 365 x 250 mm / 19.75 x 14.25 x 10 in"))
						.put(Lang.DEU, getWords(" Mattes gelbes offenes Vorderregal - 500 x 365 x 250 mm"))
						.put(Lang.FRA, getWords(" Étagère ouverte ouverte en laque jaune mat - 500 x 365 x 250 mm / 19.75 x 14.25 x 10 po")).build());
		translates
				.put(getCode("Water flow rate - 18 l/min"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Расход воды - 18 л / мин"))
						.put(Lang.ITA, getWords(" Portata d'acqua - 18 l / min"))
						.put(Lang.ESP, getWords(" Caudal de agua: 18 l / min"))
						.put(Lang.DEU, getWords(" Wasserdurchflussrate - 18 l / min"))
						.put(Lang.FRA, getWords(" Débit d'eau - 18 l / min")).build());
		translates
				.put(getCode("Minimalist, space conscious modern design and superior Northern European workmanship"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минималистский, современный космический дизайн и превосходное североевропейское мастерство"))
						.put(Lang.ITA, getWords(" Design moderno, minimalista, attento allo spazio e superiore lavorazione nordeuropea"))
						.put(Lang.ESP, getWords(" Diseño moderno minimalista, consciente del espacio y mano de obra superior del norte de Europa"))
						.put(Lang.DEU, getWords(" Minimalistisches, raumbewusstes, modernes Design und hochwertige nordeuropäische Verarbeitung"))
						.put(Lang.FRA, getWords(" Un design moderne minimaliste et respectueux de l'espace et un travail supérieur en Europe du Nord")).build());
		translates
				.put(getCode("Extra large 23.6 (600 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра большая душевая насадка 23,6 (600 мм)"))
						.put(Lang.ITA, getWords(" Soffione extra large da 23,6 (600 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 23.6 (600 mm)"))
						.put(Lang.DEU, getWords(" Extra großer 23,6 (600 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 23,6 po (600 mm)")).build());
		translates
				.put(getCode("Quick dry foam cushions"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Быстрые сухие пенные подушки"))
						.put(Lang.ITA, getWords(" Cuscini in schiuma asciutta e rapida"))
						.put(Lang.ESP, getWords(" Cojines de espuma de secado rápido"))
						.put(Lang.DEU, getWords(" Schnelltrocknende Schaumstoffpolster"))
						.put(Lang.FRA, getWords(" Coussins en mousse à séchage rapide")).build());
		translates
				.put(getCode("Freestanding vessel basin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Резервный бассейн"))
						.put(Lang.ITA, getWords(" Bacino del vaso indipendente"))
						.put(Lang.ESP, getWords(" Cuenca independiente"))
						.put(Lang.DEU, getWords(" Freistehendes Becken"))
						.put(Lang.FRA, getWords(" Bassin de cuve autoportant")).build());
		translates
				.put(getCode("Available in our most popular finishes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступный в нашей самой популярной отделке"))
						.put(Lang.ITA, getWords(" Disponibile nelle nostre finiture più popolari"))
						.put(Lang.ESP, getWords(" Disponible en nuestros acabados más populares"))
						.put(Lang.DEU, getWords(" Erhältlich in unseren beliebtesten Ausführungen"))
						.put(Lang.FRA, getWords(" Disponible dans nos finitions les plus populaires")).build());
		translates
				.put(getCode("Warm water with variable temperature setting (3 levels)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Теплая вода с переменной температурой (3 уровня)"))
						.put(Lang.ITA, getWords(" Acqua calda a temperatura variabile (3 livelli)"))
						.put(Lang.ESP, getWords(" Agua caliente con ajuste de temperatura variable (3 niveles)"))
						.put(Lang.DEU, getWords(" Warmes Wasser mit variabler Temperatureinstellung (3 Stufen)"))
						.put(Lang.FRA, getWords(" Eau chaude avec réglage de température variable (3 niveaux)")).build());
		translates
				.put(getCode("Non-porous surface for easy cleaning and sanitizing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непористая поверхность для легкой очистки и дезинфекции"))
						.put(Lang.ITA, getWords(" Superficie non porosa per una facile pulizia e sanificazione"))
						.put(Lang.ESP, getWords(" Superficie no porosa para facilitar la limpieza y la desinfección"))
						.put(Lang.DEU, getWords(" Nicht poröse Oberfläche für einfache Reinigung und Desinfektion"))
						.put(Lang.FRA, getWords(" Surface non poreuse pour un nettoyage et une désinfection faciles")).build());
		translates
				.put(getCode("Bold form in stylish, Chrome finished stainless steel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Жирная форма из стильной, хромированной нержавеющей стали"))
						.put(Lang.ITA, getWords(" Forma audace in elegante acciaio inossidabile cromato"))
						.put(Lang.ESP, getWords(" Forma intrépida en acero inoxidable con acabado cromado"))
						.put(Lang.DEU, getWords(" Bold Form in stilvollem, verchromtem Edelstahl"))
						.put(Lang.FRA, getWords(" Forme audacieuse dans l'acier inoxydable fini élégant de chrome")).build());
		translates
				.put(getCode("Large 19.75 head, including 144 rainfall spouts and the single  waterfall slot"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 19,75-дюймовая голова, в том числе 144 ливня осадков и единственный водопад"))
						.put(Lang.ITA, getWords(" Testa grande 19,75 , con 144 becchi di pioggia e la fessura a cascata singola"))
						.put(Lang.ESP, getWords(" Gran cabeza de 19,75 , incluyendo 144 caños de lluvia y la ranura de una sola cascada"))
						.put(Lang.DEU, getWords(" Große 19,75 Kopf, einschließlich 144 Niederschlag Tüllen und der einzelne Wasserfall Slot"))
						.put(Lang.FRA, getWords(" Grande tête de 19.75 , comprenant 144 becs de pluie et la seule fente cascade")).build());
		translates
				.put(getCode("Wall-mounted built-in waterfall shower - 9.25x5.5 (23.5x14 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный встроенный водопадный душ - 9,25 x5,5 (23,5 x 14 см)"))
						.put(Lang.ITA, getWords(" Doccia a cascata da incasso a parete - 9.25 x5.5 (23.5x14 cm)"))
						.put(Lang.ESP, getWords(" Ducha de cascada incorporada en la pared - 9.25 x5.5 (23.5x14 cm)"))
						.put(Lang.DEU, getWords(" An der Wand befestigte eingebaute Wasserfalldusche - 23,5x14 cm (9,55 x5.5)"))
						.put(Lang.FRA, getWords(" Douche cascade encastrée murale - 9.25 x5.5 (23.5x14 cm)")).build());
		translates
				.put(getCode("Flexible hose 59” (150cm) in length for full body reach"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гибкий шланг длиной 59 дюймов (150 см) для полного охвата тела"))
						.put(Lang.ITA, getWords(" Tubo flessibile da 59 (150 cm) di lunghezza per raggiungere il corpo intero"))
						.put(Lang.ESP, getWords(" Manguera flexible de 59 (150 cm) de longitud para un alcance total del cuerpo"))
						.put(Lang.DEU, getWords(" Flexibler Schlauch mit einer Länge von 150 cm (150 cm) für die volle Körperweite"))
						.put(Lang.FRA, getWords(" Tuyau flexible de 59 (150cm) de longueur pour atteindre le corps entier")).build());
		translates
				.put(getCode("Large 11.8x5.5 (300x140 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая головка 11.8x5.5 (300x140 мм)"))
						.put(Lang.ITA, getWords(" Soffione grande da 11,8x5,5 (300x140 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 11.8x5.5 (300x140 mm)"))
						.put(Lang.DEU, getWords(" Große 11,8 x 5,5 (300 x 140 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 11,8 x 5,5 po (300 x 140 mm)")).build());
		translates
				.put(getCode("Hygienic acrylic sheets from Lucite International ensure superior hygiene conditions"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гигиеничные акриловые листы от Lucite International обеспечивают превосходные гигиенические условия"))
						.put(Lang.ITA, getWords(" I fogli acrilici igienici di Lucite International garantiscono condizioni igieniche superiori"))
						.put(Lang.ESP, getWords(" Las láminas acrílicas higiénicas de Lucite International aseguran condiciones de higiene superiores"))
						.put(Lang.DEU, getWords(" Hygienische Acrylplatten von Lucite International sorgen für hervorragende hygienische Bedingungen"))
						.put(Lang.FRA, getWords(" Les feuilles acryliques hygiéniques de Lucite International garantissent des conditions d'hygiène supérieures")).build());
		translates
				.put(getCode("Handshower with 81 jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ручной душ с 81 форсункой"))
						.put(Lang.ITA, getWords(" Doccetta con 81 getti"))
						.put(Lang.ESP, getWords(" Ducha de mano con 81 chorros"))
						.put(Lang.DEU, getWords(" Handbrause mit 81 Düsen"))
						.put(Lang.FRA, getWords(" Douchette avec 81 jets")).build());
		translates
				.put(getCode("28 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("28 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords(" 28 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords(" 28 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords(" 28 flache Luftdüsen"))
						.put(Lang.FRA, getWords(" 28 jets d'air à profil bas")).build());
		translates
				.put(getCode("Galvanised steel structure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Оцинкованная стальная конструкция"))
						.put(Lang.ITA, getWords(" Struttura in acciaio zincato"))
						.put(Lang.ESP, getWords(" Estructura de acero galvanizado"))
						.put(Lang.DEU, getWords(" Galvanisierte Stahlstruktur"))
						.put(Lang.FRA, getWords(" Structure en acier galvanisé")).build());
		translates
				.put(getCode("Best suited with the Anette Shower Tinted Curved Glass Shower Cabin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Лучше всего подходит для душевой кабины с тонированной изогнутой душевой кабиной Anette"))
						.put(Lang.ITA, getWords(" Più adatto con la cabina doccia in vetro curvato tinto di doccia Anette"))
						.put(Lang.ESP, getWords(" El más adecuado con la cabina de ducha de vidrio curvo tintado Anette Shower"))
						.put(Lang.DEU, getWords(" Bestens geeignet für die Dusche Duschkabine aus gekantetem Glas von Anette Shower"))
						.put(Lang.FRA, getWords(" Convient le mieux à la cabine de douche en verre incurvé Anette")).build());
		translates
				.put(getCode("Rich matt brown lacquer cabinet  - 1800 x 500 x 250 mm / 70.75 x 19.75 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Богатый матовый коричневый лаковый шкаф - 1800 x 500 x 250 мм / 70,75 x 19,75 x 10 дюймов"))
						.put(Lang.ITA, getWords(" Armadio ricco di laccato marrone opaco - 1800 x 500 x 250 mm / 70,75 x 19,75 x 10 in"))
						.put(Lang.ESP, getWords(" Armario lacado marrón mate rico: 1800 x 500 x 250 mm / 70,75 x 19,75 x 10 in"))
						.put(Lang.DEU, getWords(" Rich matt braun Lackschrank - 1800 x 500 x 250 mm / 70,75 x 19,75 x 10 in"))
						.put(Lang.FRA, getWords(" Armoire laquée brun mat riche - 1800 x 500 x 250 mm / 70.75 x 19.75 x 10 po")).build());
		translates
				.put(getCode("Constructed of solid brass"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построенный из цельной латуни"))
						.put(Lang.ITA, getWords(" Costruito in ottone massiccio"))
						.put(Lang.ESP, getWords(" Construido de latón sólido"))
						.put(Lang.DEU, getWords(" Hergestellt aus massivem Messing"))
						.put(Lang.FRA, getWords(" Construit en laiton massif")).build());
		translates
				.put(getCode("Wall storage unit dimensions: 13.75 L x 7 D x 63 H (in) / 35 L x 18 D x 160 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Размеры блока хранения на стене: 13,75 L x 7 D x 63 H (дюйм) / 35 L x 18 D x 160 H (см)"))
						.put(Lang.ITA, getWords(" Dimensioni del contenitore a parete: 13,75 L x 7 P x 63 H (in) / 35 L x 18 P x 160 H (cm)"))
						.put(Lang.ESP, getWords(" Dimensiones de la unidad de almacenamiento de pared: 13.75 L x 7 D x 63 H (in) / 35 L x 18 D x 160 H (cm)"))
						.put(Lang.DEU, getWords(" Wandspeichermaße: 13,75 L x 7 T x 63 H (Zoll) / 35 L x 18 T x 160 H (cm)"))
						.put(Lang.FRA, getWords(" Dimensions de l'unité de rangement mural: 13,75 L x 7 P x 63 H (po) / 35 L x 18 P x 160 H (cm)")).build());
		translates
				.put(getCode("Comfortable – unclutters your bathing experience and frees space inside the bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобный - позволяет залить воду в ванну и освобождает место в ванной"))
						.put(Lang.ITA, getWords(" Comodo: sblocca la tua esperienza di bagno e libera lo spazio all'interno della vasca"))
						.put(Lang.ESP, getWords(" Cómodo: despeja su experiencia de baño y libera espacio dentro de la bañera"))
						.put(Lang.DEU, getWords(" Komfortabel - räumt Ihr Badeerlebnis auf und gibt Platz in der Badewanne frei"))
						.put(Lang.FRA, getWords(" Confortable - Dégage votre expérience de baignade et libère de l'espace à l'intérieur de la baignoire")).build());
		translates
				.put(getCode("Flexible connection with patented anti-torsion system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гибкое соединение с запатентованной системой против кручения"))
						.put(Lang.ITA, getWords(" Connessione flessibile con sistema anti-torsione brevettato"))
						.put(Lang.ESP, getWords(" Conexión flexible con sistema patentado anti-torsión"))
						.put(Lang.DEU, getWords(" Flexible Verbindung mit patentiertem Anti-Torsions-System"))
						.put(Lang.FRA, getWords(" Connexion flexible avec système anti-torsion breveté")).build());
		translates
				.put(getCode("Generous proportions  - 21 3/4 by 12.5 wide for superior water coverage"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Щедрые пропорции - 21 3/4 на 12,5 ширины для превосходного покрытия воды"))
						.put(Lang.ITA, getWords(" Proporzioni generose - 21 3/4 di 12,5 di larghezza per una copertura idrica superiore"))
						.put(Lang.ESP, getWords(" Proporciones generosas: 21 3/4 por 12.5 de ancho para una cobertura de agua superior"))
						.put(Lang.DEU, getWords(" Großzügige Proportionen - 21 3/4 von 12,5 für eine hervorragende Wasserabdeckung"))
						.put(Lang.FRA, getWords(" Des proportions généreuses - 21 3/4 par 12.5 de large pour une couverture d'eau supérieure")).build());
		translates
				.put(getCode("Optional stainless steel heat exchanger installation for external heating source"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Опциональная установка теплообменника из нержавеющей стали для внешнего источника нагрева"))
						.put(Lang.ITA, getWords(" Installazione opzionale dello scambiatore di calore in acciaio inox per fonte di riscaldamento esterna"))
						.put(Lang.ESP, getWords(" Instalación de intercambiador de calor de acero inoxidable opcional para fuente de calefacción externa"))
						.put(Lang.DEU, getWords(" Optionale Edelstahl-Wärmetauscher-Installation für externe Heizquelle"))
						.put(Lang.FRA, getWords(" Installation d'un échangeur de chaleur en acier inoxydable en option pour une source de chauffage externe")).build());
		translates
				.put(getCode("Operating humidity range: 0 to 98%"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Диапазон влажности при эксплуатации: от 0 до 98%"))
						.put(Lang.ITA, getWords(" Intervallo di umidità operativa: da 0 a 98%"))
						.put(Lang.ESP, getWords(" Rango de humedad de funcionamiento: 0 a 98%"))
						.put(Lang.DEU, getWords(" Betriebsfeuchtigkeitsbereich: 0 bis 98%"))
						.put(Lang.FRA, getWords(" Plage d'humidité de fonctionnement: 0 à 98%")).build());
		translates
				.put(getCode("Optional energy saving cover"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительная экономичная крышка"))
						.put(Lang.ITA, getWords(" Coperchio opzionale a risparmio energetico"))
						.put(Lang.ESP, getWords(" Cubierta de ahorro de energía opcional"))
						.put(Lang.DEU, getWords(" Optionale Energiesparabdeckung"))
						.put(Lang.FRA, getWords(" Couverture d'économie d'énergie en option")).build());
		translates
				.put(getCode("Requires a simple 90 degree PVC elbow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Требуется простой 90-градусный ПВХ-локоть"))
						.put(Lang.ITA, getWords(" Richiede un semplice gomito in PVC a 90 gradi"))
						.put(Lang.ESP, getWords(" Requiere un simple codo de PVC de 90 grados"))
						.put(Lang.DEU, getWords(" Benötigt einen einfachen 90 Grad PVC Ellenbogen"))
						.put(Lang.FRA, getWords(" Nécessite un simple coude en PVC de 90 degrés")).build());
		translates
				.put(getCode("Minimum Flow Rate: 1.37 GPM (5.2 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальная скорость потока: 1,37 GPM (5,2 LPM)"))
						.put(Lang.ITA, getWords(" Portata minima: 1,37 GPM (5,2 LPM)"))
						.put(Lang.ESP, getWords(" Caudal mínimo: 1.37 GPM (5.2 LPM)"))
						.put(Lang.DEU, getWords(" Mindestflussrate: 1,37 GPM (5,2 LPM)"))
						.put(Lang.FRA, getWords(" Débit minimum: 1,37 gal / min (5,2 LPM)")).build());
		translates
				.put(getCode("Gloss white ceramic free-standing washbasin – 550 x 400 x 150 mm / 21.75 x 15.75 x 6 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глянцевая белая керамическая отдельно стоящая раковина - 550 x 400 x 150 мм / 21,75 x 15,75 x 6 дюймов"))
						.put(Lang.ITA, getWords(" Lavabo da appoggio in ceramica bianco lucido - 550 x 400 x 150 mm / 21,75 x 15,75 x 6 in"))
						.put(Lang.ESP, getWords(" Lavabo independiente de cerámica blanca brillante - 550 x 400 x 150 mm / 21.75 x 15.75 x 6 in"))
						.put(Lang.DEU, getWords(" Standwaschbecken aus glänzendem Weiß - 550 x 400 x 150 mm / 21,75 x 15,75 x 6 in"))
						.put(Lang.FRA, getWords(" Vasque à poser en céramique blanche brillante - 550 x 400 x 150 mm / 21.75 x 15.75 x 6 in")).build());
		translates
				.put(getCode("Concealed Waste-Overflow System - no visible overflow opening"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Скрытая система переполнения - нет видимого отверстия для перелива"))
						.put(Lang.ITA, getWords(" Sistema di traboccamento dei rifiuti nascosto - nessuna apertura di troppo pieno visibile"))
						.put(Lang.ESP, getWords(" Sistema de desbordamiento de desperdicios ocultos: sin apertura de desbordamiento visible"))
						.put(Lang.DEU, getWords(" Verdecktes Überlaufsystem - keine sichtbare Überlauföffnung"))
						.put(Lang.FRA, getWords(" Système de trop-plein dissimulé - pas de débordement visible")).build());
		translates
				.put(getCode("Single piece white gloss ceramic console (900 mm x 500 mm / 35.5 x 19.75 in) with built-in ceramic basin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Однокомпонентная белая глянцевая керамическая консоль (900 мм х 500 мм / 35,5 х 19,75 дюйма) со встроенным керамическим резервуаром"))
						.put(Lang.ITA, getWords(" Consolle ceramica monocottura bianco lucido (900 mm x 500 mm / 35,5 x 19,75 pollici) con vasca in ceramica integrata"))
						.put(Lang.ESP, getWords(" Consola de cerámica blanca brillante de una pieza (900 mm x 500 mm / 35.5 x 19.75 in) con lavabo de cerámica incorporado"))
						.put(Lang.DEU, getWords(" Einteilige weißglänzende Keramikkonsole (900 mm x 500 mm) mit eingebautem Keramikbecken"))
						.put(Lang.FRA, getWords(" Console monobloc en céramique brillante blanche (900 mm x 500 mm / 35.5 x 19.75 in) avec cuve en céramique intégrée")).build());
		translates
				.put(getCode("500 mm (19.75 in) squere frameless wall mirror with overhead LED lamp"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("500 мм (19,75 дюйма) скребковое бескаркасное настенное зеркало с верхней светодиодной лампой"))
						.put(Lang.ITA, getWords(" Specchio da parete squere 500 mm (19.75 in) senza cornice con lampada a LED"))
						.put(Lang.ESP, getWords(" Espejo de pared sin marco Squere de 500 mm (19.75 in) con lámpara LED de techo"))
						.put(Lang.DEU, getWords(" 500 mm (19.75 in) squaré rahmenloser Wandspiegel mit LED-Deckenleuchte"))
						.put(Lang.FRA, getWords(" Miroir mural sans cadre de 500 mm (19.75 in) avec lampe LED au plafond")).build());
		translates
				.put(getCode("Constructed from solid brass"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен из цельной латуни"))
						.put(Lang.ITA, getWords(" Costruito in ottone massiccio"))
						.put(Lang.ESP, getWords(" Construido de latón sólido"))
						.put(Lang.DEU, getWords(" Hergestellt aus massivem Messing"))
						.put(Lang.FRA, getWords(" Construit à partir de laiton massif")).build());
		translates
				.put(getCode("Optional strainer featuring integrated overflow holes and unique vertical weeping channels (this prevents build-up at overflow holes)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительный фильтр с интегрированными отверстиями для перелива и уникальными вертикальными плавными каналами (это предотвращает накопление в отверстиях перелива)"))
						.put(Lang.ITA, getWords(" Filtro opzionale con fori di troppopieno integrati e canali di inclinazione verticali unici (questo impedisce l'accumulo nei fori di troppopieno)"))
						.put(Lang.ESP, getWords(" Filtro opcional con orificios de desbordamiento integrados y canales únicos verticales de llanto (esto evita la acumulación en los orificios de desbordamiento)"))
						.put(Lang.DEU, getWords(" Optionales Sieb mit integrierten Überlaufbohrungen und einzigartigen vertikalen Schmutzkanälen (verhindert Ansammlungen an Überlaufbohrungen)"))
						.put(Lang.FRA, getWords(" Crépine en option avec trous de trop-plein intégrés et canaux de déversement verticaux uniques (empêchant l'accumulation dans les trous de trop-plein)")).build());
		translates
				.put(getCode("5 year limited warranty on structural integrity"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на структурную целостность"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 5 anni sull'integrità strutturale"))
						.put(Lang.ESP, getWords(" Garantía limitada de 5 años en integridad estructural"))
						.put(Lang.DEU, getWords(" 5 Jahre eingeschränkte Garantie auf strukturelle Integrität"))
						.put(Lang.FRA, getWords(" Garantie limitée de 5 ans sur l'intégrité structurale")).build());
		translates
				.put(getCode("Robust freestanding construction intended for corner applications"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочная автономная конструкция, предназначенная для угловых применений"))
						.put(Lang.ITA, getWords(" Robusta costruzione autoportante destinata ad applicazioni angolari"))
						.put(Lang.ESP, getWords(" Construcción robusta e independiente para aplicaciones de esquina"))
						.put(Lang.DEU, getWords(" Robuste freistehende Konstruktion für Eckanwendungen"))
						.put(Lang.FRA, getWords(" Construction robuste et autoportante destinée aux applications de coin")).build());
		translates
				.put(getCode("Weather resistant, polyester belts for seat and back support"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Погодостойкие, полиэфирные ленты для поддержки сиденья и спины"))
						.put(Lang.ITA, getWords(" Cinghie in poliestere resistenti agli agenti atmosferici per il supporto di sedile e schienale"))
						.put(Lang.ESP, getWords(" Resistente a la intemperie, cinturones de poliéster para el respaldo del asiento y la espalda"))
						.put(Lang.DEU, getWords(" Wetterbeständige Polyestergurte für Sitz- und Rückenstütze"))
						.put(Lang.FRA, getWords(" Ceintures polyester résistantes aux intempéries pour le soutien du siège et du dossier")).build());
		translates
				.put(getCode("Automatic cleaning of the wands before and after each use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Автоматическая очистка палочек до и после каждого использования"))
						.put(Lang.ITA, getWords(" Pulizia automatica delle bacchette prima e dopo ogni utilizzo"))
						.put(Lang.ESP, getWords(" Limpieza automática de las varillas antes y después de cada uso"))
						.put(Lang.DEU, getWords(" Automatische Reinigung der Stäbe vor und nach jedem Gebrauch"))
						.put(Lang.FRA, getWords(" Nettoyage automatique des baguettes avant et après chaque utilisation")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с металлической пластиной"))
						.put(Lang.ITA, getWords(" Miscelatore per doccia a muro con piastra in metallo"))
						.put(Lang.ESP, getWords(" Mezclador de ducha de pared con placa de metal"))
						.put(Lang.DEU, getWords(" Wand-Brausebatterie mit Metallplatte"))
						.put(Lang.FRA, getWords(" Mitigeur de douche mural avec plaque en métal")).build());
		translates
				.put(getCode("Must be matched with 3 ways mixer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Должен быть согласован с 3-мя способами"))
						.put(Lang.ITA, getWords(" Deve essere abbinato al miscelatore a 3 vie"))
						.put(Lang.ESP, getWords(" Debe coincidir con el mezclador de 3 maneras"))
						.put(Lang.DEU, getWords(" Muss mit 3-Wege-Mixer abgestimmt werden"))
						.put(Lang.FRA, getWords(" Doit être associé à un mélangeur 3 voies")).build());
		translates
				.put(getCode("Modern and contemporary style"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современный и современный стиль"))
						.put(Lang.ITA, getWords(" Stile moderno e contemporaneo"))
						.put(Lang.ESP, getWords(" Estilo moderno y contemporáneo"))
						.put(Lang.DEU, getWords(" Moderner und zeitgenössischer Stil"))
						.put(Lang.FRA, getWords(" Style moderne et contemporain")).build());
		translates
				.put(getCode("Code compliant with American standard 1.5 waste outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Код, соответствующий американским стандартным 1,5-дюймовым отходам"))
						.put(Lang.ITA, getWords(" Codice conforme ai rifiuti standard americani da 1,5 "))
						.put(Lang.ESP, getWords(" Código que cumple con las salidas de desperdicios estándares estadounidenses de 1.5 "))
						.put(Lang.DEU, getWords(" Der Code entspricht den amerikanischen 1,5-Zoll-Abfallsteckdosen"))
						.put(Lang.FRA, getWords(" Code conforme aux prises d'évacuation standard américaines de 1,5 po")).build());
		translates
				.put(getCode("Designed and made in Italy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Разработан и изготовлен в Италии"))
						.put(Lang.ITA, getWords(" Progettato e realizzato in Italia"))
						.put(Lang.ESP, getWords(" Diseñado y hecho en Italia"))
						.put(Lang.DEU, getWords(" Entworfen und hergestellt in Italien"))
						.put(Lang.FRA, getWords(" Conçu et fabriqué en Italie")).build());
		translates
				.put(getCode("Multi-bed swimming pool sand filtration system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Многослойная система фильтрации песка для бассейнов"))
						.put(Lang.ITA, getWords(" Sistema di filtrazione a sabbia per piscina a più letti"))
						.put(Lang.ESP, getWords(" Sistema de filtración de arena para piscinas con varias camas"))
						.put(Lang.DEU, getWords(" Multi-Bett-Schwimmbad-Sand-Filter-System"))
						.put(Lang.FRA, getWords(" Système de filtration de sable de piscine à plusieurs lits")).build());
		translates
				.put(getCode("3 Year Limited Warranty on all other pieces"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3-летняя ограниченная гарантия на все остальные части"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 3 anni su tutti gli altri pezzi"))
						.put(Lang.ESP, getWords(" Garantía limitada de 3 años en todas las demás piezas"))
						.put(Lang.DEU, getWords(" 3 Jahre eingeschränkte Garantie auf alle anderen Teile"))
						.put(Lang.FRA, getWords(" Garantie limitée de 3 ans sur toutes les autres pièces")).build());
		translates
				.put(getCode("Two frameless wall mirrors, each 1100 x 350 mm / 43.25 x 13.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Два бескаркасных настенных зеркала, каждый 1100 х 350 мм / 43,25 х 13,75 дюйма"))
						.put(Lang.ITA, getWords(" Due specchi da parete senza cornice, ciascuno da 1100 x 350 mm / 43,25 x 13,75 pollici"))
						.put(Lang.ESP, getWords(" Dos espejos de pared sin marco, cada uno de 1100 x 350 mm / 43.25 x 13.75 in"))
						.put(Lang.DEU, getWords(" Zwei rahmenlose Wandspiegel, jeweils 1100 x 350 mm"))
						.put(Lang.FRA, getWords(" Deux miroirs muraux sans cadre, chacun de 1100 x 350 mm / 43,25 x 13,75 po")).build());
		translates
				.put(getCode("2 Minute Purge Cycle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-минутный цикл продувки"))
						.put(Lang.ITA, getWords(" Ciclo di spurgo di 2 minuti"))
						.put(Lang.ESP, getWords(" Ciclo de purga de 2 minutos"))
						.put(Lang.DEU, getWords(" 2 Minuten Spülzyklus"))
						.put(Lang.FRA, getWords(" Cycle de purge de 2 minutes")).build());
		translates
				.put(getCode("Designed for comfortable two-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного купания двух человек"))
						.put(Lang.ITA, getWords(" Progettato per una balneazione doppia per due persone"))
						.put(Lang.ESP, getWords(" Diseñado para un baño cómodo para dos personas"))
						.put(Lang.DEU, getWords(" Entworfen für komfortables Baden für zwei Personen"))
						.put(Lang.FRA, getWords(" Conçu pour une baignade confortable pour deux personnes")).build());
		translates
				.put(getCode("Cutting edge water flow technology"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Технология перекачивания воды"))
						.put(Lang.ITA, getWords(" Tecnologia all'avanguardia per il flusso dell'acqua"))
						.put(Lang.ESP, getWords(" Tecnología de flujo de agua de vanguardia"))
						.put(Lang.DEU, getWords(" Modernste Wasserströmungstechnologie"))
						.put(Lang.FRA, getWords(" Technologie d'écoulement de l'eau de pointe")).build());
		translates
				.put(getCode("Three outlet thermostatic shower control mixer dimensions: 10.25 W x 7 H x 3 D (in) / 26 W x 18 H x 7.5 D (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Три выходных термостатических смесителя для управления душем: 10,25 W x 7 H x 3 D (дюйм) / 26 Вт x 18 H x 7,5 D (см)"))
						.put(Lang.ITA, getWords(" Miscelatore termostatico per doccia a tre uscite dimensioni: 10.25 W x 7 H x 3 D (in) / 26 W x 18 H x 7.5 D (cm)"))
						.put(Lang.ESP, getWords(" Dimensiones del mezclador de control de la ducha termostática de tres salidas: 10.25 W x 7 H x 3 D (in) / 26 W x 18 H x 7.5 D (cm)"))
						.put(Lang.DEU, getWords(" Drei Auslass-Thermostat-Brausebatterie-Mischer Abmessungen: 10,25 W x 7 H x 3 D (Zoll) / 26 W x 18 H x 7,5 D (cm)"))
						.put(Lang.FRA, getWords(" Dimensions du mitigeur thermostatique à trois prises de douche: 10.25 W x 7 H x 3 P (po) / 26 W x 18 H x 7.5 P (cm)")).build());
		translates
				.put(getCode("Install your Aquatica freestanding bath tub in few minutes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Установите свою автономную ванну Aquatica за несколько минут"))
						.put(Lang.ITA, getWords(" Installa la tua vasca da bagno indipendente Aquatica in pochi minuti"))
						.put(Lang.ESP, getWords(" Instale su bañera independiente Aquatica en pocos minutos"))
						.put(Lang.DEU, getWords(" Installieren Sie Ihre Aquatica freistehende Badewanne in wenigen Minuten"))
						.put(Lang.FRA, getWords(" Installez votre baignoire autoportante Aquatica en quelques minutes")).build());
		translates
				.put(getCode("Capacious 15.7 (400 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Вместимость 15,7 (400 мм) душевой кабины"))
						.put(Lang.ITA, getWords(" Soffione spazioso da 15,7 (400 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha de 15.7 (400 mm)"))
						.put(Lang.DEU, getWords(" Capacious 15,7 (400 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche de 15,7 po (400 mm)")).build());
		translates
				.put(getCode("Solid surface resin body design with silky smooth, soft and warm to touch surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Конструкция корпуса из твердой поверхности с шелковистой гладкой, мягкой и теплой поверхностью"))
						.put(Lang.ITA, getWords(" Corpo in resina a superficie solida con superficie liscia, morbida e calda al tatto"))
						.put(Lang.ESP, getWords(" Diseño de cuerpo de resina de superficie sólida con superficie sedosa suave, suave y cálida al tacto"))
						.put(Lang.DEU, getWords(" Solid-Surface-Harz-Körper-Design mit seidenweich, weich und warm zu berühren Oberfläche"))
						.put(Lang.FRA, getWords(" Conception de corps de résine de surface solide avec la surface soyeuse, molle et chaude au toucher")).build());
		translates
				.put(getCode("Requires 2 drain hole"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Требуется 2 дренажное отверстие"))
						.put(Lang.ITA, getWords(" Richiede 2 foro di scarico"))
						.put(Lang.ESP, getWords(" Requiere orificio de drenaje de 2 "))
						.put(Lang.DEU, getWords(" Benötigt 2 Ablaufloch"))
						.put(Lang.FRA, getWords(" Nécessite un trou de vidange de 2 ")).build());
		translates
				.put(getCode("Waterproof Wireless Remote control with beautiful capacitive glass interface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Водонепроницаемый беспроводной пульт дистанционного управления с красивым емкостным стеклянным интерфейсом"))
						.put(Lang.ITA, getWords(" Telecomando senza fili impermeabile con una bella interfaccia capacitiva in vetro"))
						.put(Lang.ESP, getWords(" Control remoto inalámbrico a prueba de agua con hermosa interfaz de vidrio capacitivo"))
						.put(Lang.DEU, getWords(" Wasserdichte drahtlose Fernbedienung mit schöner kapazitiver Glasschnittstelle"))
						.put(Lang.FRA, getWords(" Télécommande sans fil étanche avec une belle interface de verre capacitif")).build());
		translates
				.put(getCode("Aged oak veneer cabinet with matt white lacquered top and side panels - 1600 x 500 x 350 mm / 63 x 19.75 x 13.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Шкаф из дубового шпона с матовым белым лакированным верхним и боковым панелями - 1600 x 500 x 350 мм / 63 x 19,75 x 13,75 дюйма"))
						.put(Lang.ITA, getWords(" Mobile impiallacciato rovere invecchiato con piano e pannelli laterali laccati bianco opaco - 1600 x 500 x 350 mm / 63 x 19,75 x 13,75 in"))
						.put(Lang.ESP, getWords(" Mueble de chapa de roble envejecido con tapa lacada en blanco mate y paneles laterales - 1600 x 500 x 350 mm / 63 x 19.75 x 13.75 in"))
						.put(Lang.DEU, getWords(" Gealterter Eiche Furnier Schrank mit matt weiß lackierten oberen und Seitenplatten - 1600 x 500 x 350 mm / 63 x 19,75 x 13,75 in"))
						.put(Lang.FRA, getWords(" Meuble placage en chêne vieilli avec plateau et panneaux latéraux laqués blanc mat - 1600 x 500 x 350 mm / 63 x 19,75 x 13,75 po")).build());
		translates
				.put(getCode("Visual controls for temperature"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Визуальный контроль температуры"))
						.put(Lang.ITA, getWords(" Controlli visivi per la temperatura"))
						.put(Lang.ESP, getWords(" Controles visuales para la temperatura"))
						.put(Lang.DEU, getWords(" Visuelle Kontrollen für die Temperatur"))
						.put(Lang.FRA, getWords(" Contrôles visuels pour la température")).build());
		translates
				.put(getCode("5 year limited warranty on the furniture"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя ограниченная гарантия на мебель"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 5 anni sui mobili"))
						.put(Lang.ESP, getWords(" 5 años de garantía limitada en los muebles"))
						.put(Lang.DEU, getWords(" 5 Jahre Garantie auf die Möbel"))
						.put(Lang.FRA, getWords(" Garantie limitée de 5 ans sur les meubles")).build());
		translates
				.put(getCode("Colour Sense: 2 LEDs and 24 mini LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чувствительность цвета: 2 светодиода и 24 мини-светодиода"))
						.put(Lang.ITA, getWords(" Senso del colore: 2 LED e 24 mini LED"))
						.put(Lang.ESP, getWords(" Color Sense: 2 LED y 24 mini LED"))
						.put(Lang.DEU, getWords(" Farbsensor: 2 LEDs und 24 Mini-LEDs"))
						.put(Lang.FRA, getWords(" Sens de la couleur: 2 LED et 24 mini LED")).build());
		translates
				.put(getCode("Tested and found compliant with CSA B45 and ANSI Z124 (American National Standards Institute), Code compliant with American standard 1.5 waste outlets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Протестировано и найдено в соответствии с CSA B45 и ANSI Z124 (Американский национальный институт стандартов), код соответствует американским стандартным 1,5-дюймовым отходам"))
						.put(Lang.ITA, getWords(" Testato e trovato conforme a CSA B45 e ANSI Z124 (American National Standards Institute), codice conforme alle prese standard americano da 1,5 "))
						.put(Lang.ESP, getWords(" Probado y encontrado conforme con CSA B45 y ANSI Z124 (Instituto Nacional de Estándares Americanos), Código que cumple con los estándares estadounidenses de desperdicios de 1.5 "))
						.put(Lang.DEU, getWords(" Getestet und als konform mit CSA B45 und ANSI Z124 (American National Standards Institute), Code-konform mit 1,5-Zoll-Abfallsteckdosen nach amerikanischem Standard"))
						.put(Lang.FRA, getWords(" Testé et trouvé conforme aux normes CSA B45 et ANSI Z124 (American National Standards Institute), conforme aux normes américaines de 1,5 ")).build());
		translates
				.put(getCode("5 Year Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("5-летняя гарантия"))
						.put(Lang.ITA, getWords(" 5 anni di garanzia"))
						.put(Lang.ESP, getWords(" 5 años de garantía"))
						.put(Lang.DEU, getWords(" 5 Jahre Garantie"))
						.put(Lang.FRA, getWords(" Garantie de 5 ans")).build());
		translates
				.put(getCode("Square wall-mounted shower arm to match your shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Квадратная настенная душевая подставка, соответствующая душевой головке"))
						.put(Lang.ITA, getWords(" Braccio doccia quadrato a parete per abbinare il tuo soffione"))
						.put(Lang.ESP, getWords(" Brazo de ducha cuadrado montado en la pared para que coincida con tu cabezal de ducha"))
						.put(Lang.DEU, getWords(" Quadratischer wandmontierter Duscharm passend zu Ihrem Duschkopf"))
						.put(Lang.FRA, getWords(" Bras de douche mural pour votre tête de douche")).build());
		translates
				.put(getCode("Ceiling mounting system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Потолочная система крепления"))
						.put(Lang.ITA, getWords(" Sistema di montaggio a soffitto"))
						.put(Lang.ESP, getWords(" Sistema de montaje en el techo"))
						.put(Lang.DEU, getWords(" Deckenmontagesystem"))
						.put(Lang.FRA, getWords(" Système de montage au plafond")).build());
		translates
				.put(getCode("Max flow rate of 3.17 GPM (12 LMP) at 5 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,17 GPM (12 LMP) при давлении воды 5 бар"))
						.put(Lang.ITA, getWords(" Portata massima di 3,17 GPM (12 LMP) a 5 bar di pressione dell'acqua"))
						.put(Lang.ESP, getWords(" Caudal máximo de 3.17 GPM (12 LMP) a 5 bar de presión de agua"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 3,17 GPM (12 LMP) bei 5 bar Wasserdruck"))
						.put(Lang.FRA, getWords(" Débit maximum de 3,17 GPM (12 LMP) à une pression d'eau de 5 bars")).build());
		translates
				.put(getCode("Iroko wood frame"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Деревянная рамка Ироко"))
						.put(Lang.ITA, getWords(" Cornice in legno Iroko"))
						.put(Lang.ESP, getWords(" Marco de madera Iroko"))
						.put(Lang.DEU, getWords(" Iroko Holzrahmen"))
						.put(Lang.FRA, getWords(" Cadre en bois d'Iroko")).build());
		translates
				.put(getCode("220/240V 50Hz or 60Hz AC power with 25A RCD (GFI) protected fuse"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("220/240 В 50 Гц или 60 Гц переменного тока с защищенным предохранителем 25 А RCD (GFI)"))
						.put(Lang.ITA, getWords(" Alimentazione 220 / 240V 50Hz o 60Hz CA con fusibile protetto da 25 A RCD (GFI)"))
						.put(Lang.ESP, getWords(" 220 / 240V 50Hz o 60Hz de corriente alterna con fusible protegido de 25A RCD (GFI)"))
						.put(Lang.DEU, getWords(" 220 / 240V 50Hz oder 60Hz Wechselstrom mit 25A RCD (GFI) Sicherung"))
						.put(Lang.FRA, getWords(" Alimentation CA 220 / 240V 50Hz ou 60Hz avec fusible protégé RCD 25 A (GFI)")).build());
		translates
				.put(getCode("Discreet styling in chrome-finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сдержанный стиль в хромированном исполнении"))
						.put(Lang.ITA, getWords(" Stile discreto in finitura cromata"))
						.put(Lang.ESP, getWords(" Discreto estilo en acabado cromado"))
						.put(Lang.DEU, getWords(" Dezentes Styling in Chrom-Finish"))
						.put(Lang.FRA, getWords(" Style discret en finition chromée")).build());
		translates
				.put(getCode("Anti-lime shower system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Анти-лимонная душевая система"))
						.put(Lang.ITA, getWords(" Sistema doccia anticalcare"))
						.put(Lang.ESP, getWords(" Sistema de ducha antical"))
						.put(Lang.DEU, getWords(" Anti-Kalk-Duschsystem"))
						.put(Lang.FRA, getWords(" Système de douche anti-calcaire")).build());
		translates
				.put(getCode("n°1 amplifier 3 channel 2X25Wrms+1x50Wrms"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("n ° 1 усилитель 3 канала 2X25Wrms + 1x50Wrms"))
						.put(Lang.ITA, getWords(" n ° 1 amplificatore 3 canali 2X25Wrms + 1x50Wrms"))
						.put(Lang.ESP, getWords(" n ° 1 amplificador 3 canales 2X25Wrms + 1x50Wrms"))
						.put(Lang.DEU, getWords(" n ° 1 Verstärker 3 Kanal 2X25Wrms + 1x50Wrms"))
						.put(Lang.FRA, getWords(" n ° 1 amplificateur 3 canaux 2X25Wrms + 1x50Wrms")).build());
		translates
				.put(getCode("Square wall-mounted shower arm 12 (31 cm) in length to match your shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Квадратный настенный душ 12 (31 см) в длину, соответствующий вашей душевой головке"))
						.put(Lang.ITA, getWords(" Braccio doccia quadrato a parete da 12 (31 cm) di lunghezza per abbinare il tuo soffione"))
						.put(Lang.ESP, getWords(" Brazo de ducha cuadrado montado en la pared 12 (31 cm) de largo para que coincida con tu cabezal de ducha"))
						.put(Lang.DEU, getWords(" Quadratischer wandmontierter Duscharm mit einer Länge von 31 cm (31 cm) passend zu Ihrem Duschkopf"))
						.put(Lang.FRA, getWords(" Bras de douche carré mural 12 (31 cm) de longueur pour s'adapter à votre pomme de douche")).build());
		translates
				.put(getCode("Strikingly modern, gentle and very feminine design – a true architectural masterpiece"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удивительно современный, нежный и очень женственный дизайн - настоящий архитектурный шедевр"))
						.put(Lang.ITA, getWords(" Design sorprendentemente moderno, gentile e molto femminile - un vero capolavoro architettonico"))
						.put(Lang.ESP, getWords(" Diseño sorprendentemente moderno, gentil y muy femenino: una verdadera obra maestra arquitectónica"))
						.put(Lang.DEU, getWords(" Auffallend modernes, sanftes und sehr feminines Design - ein wahres architektonisches Meisterwerk"))
						.put(Lang.FRA, getWords(" Design étonnamment moderne, doux et très féminin - un véritable chef-d'œuvre architectural")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 3 outlets to function all concurrently and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 3 розетками для работы одновременно и с металлической пластиной"))
						.put(Lang.ITA, getWords(" Miscelatore doccia a parete con 3 uscite per funzionare contemporaneamente e piastra metallica"))
						.put(Lang.ESP, getWords(" Mezclador de ducha de pared con 3 salidas para funcionar todas al mismo tiempo y placa de metal"))
						.put(Lang.DEU, getWords(" Wand-Brausebatterie mit 3 Ausgängen für gleichzeitige Funktion und Metallplatte"))
						.put(Lang.FRA, getWords(" Mitigeur de douche mural avec 3 sorties pour fonctionner simultanément et plaque métallique")).build());
		translates
				.put(getCode("Should be used when the water outlet is higher than 86.6in."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Следует использовать, когда выход воды выше 86,6 дюймов."))
						.put(Lang.ITA, getWords(" Dovrebbe essere usato quando l'uscita dell'acqua è superiore a 86,6 pollici."))
						.put(Lang.ESP, getWords(" Se debe usar cuando la salida de agua sea superior a 86.6 in."))
						.put(Lang.DEU, getWords(" Sollte verwendet werden, wenn der Wasserauslass höher als 86,6 Zoll ist."))
						.put(Lang.FRA, getWords(" Devrait être utilisé lorsque la sortie d'eau est supérieure à 86.6in.")).build());
		translates
				.put(getCode("Streamlined, wall-mounted design for seamless fitting in any style of shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Упорядоченный, настенный дизайн для бесшовной установки в любом стиле душа"))
						.put(Lang.ITA, getWords(" Design aerodinamico a parete per un montaggio senza cuciture in qualsiasi tipo di doccia"))
						.put(Lang.ESP, getWords(" Diseño aerodinámico montado en la pared para un ajuste perfecto en cualquier estilo de ducha"))
						.put(Lang.DEU, getWords(" Stromlinienförmiges, an der Wand befestigtes Design für nahtlose Anpassung in jeder Art von Dusche"))
						.put(Lang.FRA, getWords(" Conception profilée et murale pour un ajustement parfait dans n'importe quel style de douche")).build());
		translates
				.put(getCode("Shower mixer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Смеситель для душа"))
						.put(Lang.ITA, getWords(" Miscelatore doccia"))
						.put(Lang.ESP, getWords(" Mezclador de la ducha"))
						.put(Lang.DEU, getWords(" Dusche-Mischer"))
						.put(Lang.FRA, getWords(" Mitigeur de douche")).build());
	}

	public static void part4() {
		translates
				.put(getCode("Two supporting dark brown veneer drawer cabinets – 1200 x 500 x 250 mm / 47.25 x 19.75 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Два поддерживающих темно-коричневых шкафа для шпона - 1200 x 500 x 250 мм / 47,25 x 19,75 x 10 дюймов"))
						.put(Lang.ITA, getWords(" Due cassettiere impiallacciate in legno marrone scuro - 1200 x 500 x 250 mm / 47,25 x 19,75 x 10 pollici"))
						.put(Lang.ESP, getWords(" Dos gabinetes de cajones de chapa de color marrón oscuro de apoyo - 1200 x 500 x 250 mm / 47.25 x 19.75 x 10 in"))
						.put(Lang.DEU, getWords(" Zwei tragende Schubladenschränke aus dunkelbraunem Furnier - 1200 x 500 x 250 mm / 47,25 x 19,75 x 10 in"))
						.put(Lang.FRA, getWords(" Deux armoires à tiroirs en placage brun foncé - 1200 x 500 x 250 mm / 47,25 x 19,75 x 10 po")).build());
		translates
				.put(getCode("Max flow rate of 3.2 GPM (12.1 LPM) at 5 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,2 GPM (12,1 LPM) при давлении воды 5 бар"))
						.put(Lang.ITA, getWords(" Portata massima di 3,2 GPM (12,1 LPM) a 5 bar di pressione dell'acqua"))
						.put(Lang.ESP, getWords(" Caudal máximo de 3.2 GPM (12.1 LPM) a 5 bar de presión de agua"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 3,2 GPM (12,1 LPM) bei 5 bar Wasserdruck"))
						.put(Lang.FRA, getWords(" Débit maximum de 3,2 GPM (12,1 LPM) à une pression d'eau de 5 bars")).build());
		translates
				.put(getCode("9.25x5.5 (235x140 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("9.25x5.5 (235x140 мм) душевая головка"))
						.put(Lang.ITA, getWords(" Soffione 9,25x5,5 (235x140 mm)"))
						.put(Lang.ESP, getWords(" Cabeza de ducha de 9.25x5.5 (235x140 mm)"))
						.put(Lang.DEU, getWords(" 9.25x5.5 (235x140 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche de 9,25 x 5,5 po (235 x 140 mm)")).build());
		translates
				.put(getCode("Designed for extra deep bathing experience. 16 to overflow line!"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для дополнительного глубокого купания.  16 до линии перелива!"))
						.put(Lang.ITA, getWords(" Progettato per un'esperienza di bagno extra profonda.  16 per troppopieno linea!"))
						.put(Lang.ESP, getWords(" Diseñado para una experiencia de baño extra profunda.  16 para desbordar la línea!"))
						.put(Lang.DEU, getWords(" Entworfen für extra tiefe Badeerfahrung.  16 bis zur Überlaufleitung!"))
						.put(Lang.FRA, getWords(" Conçu pour une expérience de bain extra profonde.  16 à déborder!")).build());
		translates
				.put(getCode("One of Aquatica’s largest tubs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Одна из самых больших ванн Aquatica"))
						.put(Lang.ITA, getWords(" Una delle vasche più grandi di Aquatica"))
						.put(Lang.ESP, getWords(" Una de las bañeras más grandes de Aquatica"))
						.put(Lang.DEU, getWords(" Eine der größten Wannen von Aquatica"))
						.put(Lang.FRA, getWords(" L'un des plus grands bassins d'Aquatica")).build());
		translates
				.put(getCode("Progressive shower mixer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прогрессивный смеситель для душа"))
						.put(Lang.ITA, getWords(" Miscelatore doccia progressivo"))
						.put(Lang.ESP, getWords(" Batidora de ducha progresiva"))
						.put(Lang.DEU, getWords(" Progressiver Duschmischer"))
						.put(Lang.FRA, getWords(" Mitigeur de douche progressif")).build());
		translates
				.put(getCode("Purchasable in a Bathroom Furniture Set"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Покупка в наборе мебели для ванной комнаты"))
						.put(Lang.ITA, getWords(" Acquistabile in un set di mobili da bagno"))
						.put(Lang.ESP, getWords(" Comprable en un conjunto de muebles de baño"))
						.put(Lang.DEU, getWords(" Gekauft in einem Badezimmer-Möbel-Set"))
						.put(Lang.FRA, getWords(" Achetable dans un ensemble de meubles de salle de bain")).build());
		translates
				.put(getCode("Flow rate 2.37 GPM (9 LPM) for best performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Скорость потока 2,37 GPM (9 LPM) для лучшей производительности"))
						.put(Lang.ITA, getWords(" Portata 2.37 GPM (9 LPM) per prestazioni ottimali"))
						.put(Lang.ESP, getWords(" Velocidad de flujo 2.37 GPM (9 LPM) para un mejor rendimiento"))
						.put(Lang.DEU, getWords(" Durchflussrate 2.37 GPM (9 LPM) für beste Leistung"))
						.put(Lang.FRA, getWords(" Débit 2,37 GPM (9 LPM) pour de meilleures performances")).build());
		translates
				.put(getCode("Large matching pairs of drawers – each 700 x 500 x 250 mm / 27.5 x 19.75 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большие совпадающие пары ящиков - каждый 700 х 500 х 250 мм / 27,5 х 19,75 х 10 дюймов"))
						.put(Lang.ITA, getWords(" Grandi coppie di cassetti abbinati: 700 x 500 x 250 mm / 27,5 x 19,75 x 10 pollici"))
						.put(Lang.ESP, getWords(" Gran juego de cajones, cada uno de 700 x 500 x 250 mm / 27.5 x 19.75 x 10 in"))
						.put(Lang.DEU, getWords(" Große zusammenpassende Schubladenpaare - jeweils 700 x 500 x 250 mm / 27,5 x 19,75 x 10 in"))
						.put(Lang.FRA, getWords(" Grandes paires de tiroirs - 700 x 500 x 250 mm / 27.5 x 19.75 x 10 po")).build());
		translates
				.put(getCode("Chromotherapy LED spotlight - 1 LED"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Светодиодный прожектор хромотерапии - 1 светодиод"))
						.put(Lang.ITA, getWords(" Faretto a LED per cromoterapia - 1 LED"))
						.put(Lang.ESP, getWords(" Proyector LED de cromoterapia - 1 LED"))
						.put(Lang.DEU, getWords(" Chromotherapie LED-Strahler - 1 LED"))
						.put(Lang.FRA, getWords(" Spot à LED pour chromothérapie - 1 LED")).build());
		translates
				.put(getCode("Fingertip control: pull left handle for cold water, right - for hot"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Управление пальцем: потяните левую ручку для холодной воды, вправо - для горячего"))
						.put(Lang.ITA, getWords(" Controllo a portata di mano: tirare la maniglia sinistra per l'acqua fredda, a destra - per il caldo"))
						.put(Lang.ESP, getWords(" Control con la yema del dedo: tire del asa izquierda para obtener agua fría, derecha - para el calor"))
						.put(Lang.DEU, getWords(" Fingerspitzenkontrolle: Ziehen Sie den linken Griff für kaltes Wasser, rechts - für heiß"))
						.put(Lang.FRA, getWords(" Contrôle du bout des doigts: tirer la poignée gauche pour l'eau froide, droite - pour chaud")).build());
		translates
				.put(getCode("Handmade in Italy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ручная работа в Италии"))
						.put(Lang.ITA, getWords(" Fatto a mano in Italia"))
						.put(Lang.ESP, getWords(" Hecho a mano en Italia"))
						.put(Lang.DEU, getWords(" Handgefertigt in Italien"))
						.put(Lang.FRA, getWords(" Fait à la main en Italie")).build());
		translates
				.put(getCode("Toned grey safety glass 6 mm (0.25 in)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Тонированное серое безопасное стекло 6 мм (0,25 дюйма)"))
						.put(Lang.ITA, getWords(" Vetro di sicurezza grigio tinto 6 mm (0,25 in)"))
						.put(Lang.ESP, getWords(" Vidrio de seguridad gris entonado 6 mm (0.25 in)"))
						.put(Lang.DEU, getWords(" Getöntes graues Sicherheitsglas 6 mm"))
						.put(Lang.FRA, getWords(" Verre de sécurité gris tonique 6 mm (0.25 in)")).build());
		translates
				.put(getCode("Premium design, one of the kind freestanding bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-дизайн, одна из которых отдельно стоящая ванна"))
						.put(Lang.ITA, getWords(" Design premium, una vasca da bagno indipendente di tipo"))
						.put(Lang.ESP, getWords(" Diseño premium, uno de los tipos de bañera independiente"))
						.put(Lang.DEU, getWords(" Premium-Design, eine der freistehenden Badewannen"))
						.put(Lang.FRA, getWords(" Un design haut de gamme, une baignoire autoportante")).build());
		translates
				.put(getCode("Minimalistic keypad with LCD temperature display"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минималистичная клавиатура с жидкокристаллическим дисплеем"))
						.put(Lang.ITA, getWords(" Tastiera minimalista con display LCD della temperatura"))
						.put(Lang.ESP, getWords(" Teclado minimalista con pantalla LCD de temperatura"))
						.put(Lang.DEU, getWords(" Minimalistische Tastatur mit LCD-Temperaturanzeige"))
						.put(Lang.FRA, getWords(" Clavier minimaliste avec affichage de la température LCD")).build());
		translates
				.put(getCode("3 kW heater with temperature control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3 кВт с контролем температуры"))
						.put(Lang.ITA, getWords(" Riscaldatore da 3 kW con controllo della temperatura"))
						.put(Lang.ESP, getWords(" Calentador de 3 kW con control de temperatura"))
						.put(Lang.DEU, getWords(" 3 kW Heizung mit Temperaturregelung"))
						.put(Lang.FRA, getWords(" Réchauffeur de 3 kW avec contrôle de la température")).build());
		translates
				.put(getCode("Constructed of thick 100% heavy gauge sanitary grade precision acrylic"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построенный из толстой 100% тяжелой калибровки санитарного класса точности акрил"))
						.put(Lang.ITA, getWords(" Realizzato in acrilico di precisione di spessore sanitario di grosso spessore al 100%"))
						.put(Lang.ESP, getWords(" Construido de acrílico grueso de la precisión del grado sanitario grueso del 100%"))
						.put(Lang.DEU, getWords(" Hergestellt aus dickem, 100% schwerem Sanitär-Präzisions-Acryl"))
						.put(Lang.FRA, getWords(" Construit de 100% épais acrylique sanitaire de précision de calibre lourd")).build());
		translates
				.put(getCode("25 Year Limited Warranty on the bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия 25 лет на ванну"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 25 anni sulla vasca"))
						.put(Lang.ESP, getWords(" Garantía limitada de 25 años en la bañera"))
						.put(Lang.DEU, getWords(" 25 Jahre eingeschränkte Garantie auf die Badewanne"))
						.put(Lang.FRA, getWords(" Garantie limitée de 25 ans sur la baignoire")).build());
		translates
				.put(getCode("Suitable for a wide range of one or two function shower heads"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходит для широкого диапазона одной или двух функциональных душевых голов"))
						.put(Lang.ITA, getWords(" Adatto per un'ampia gamma di soffioni a una o due funzioni"))
						.put(Lang.ESP, getWords(" Adecuado para una amplia gama de cabezales de ducha de una o dos funciones"))
						.put(Lang.DEU, getWords(" Geeignet für eine Vielzahl von Duschbrausen mit einer oder zwei Funktionen"))
						.put(Lang.FRA, getWords(" Convient pour une large gamme de pommes de douche à une ou deux fonctions")).build());
		translates
				.put(getCode("1600 mm x 500 mm (63 x 19.75 in) frameless wall mirror with built-in LED strip lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1600 мм х 500 мм (63 х 19,75 дюйма) бескаркасное настенное зеркало со встроенным светодиодным освещением"))
						.put(Lang.ITA, getWords(" Specchio da parete senza cornice da 1600 mm x 500 mm (63 x 19,75 in) con illuminazione a strisce LED incorporata"))
						.put(Lang.ESP, getWords(" Espejo de pared sin marco de 1600 mm x 500 mm (63 x 19.75 in) con iluminación incorporada en tira de LED"))
						.put(Lang.DEU, getWords(" 1600 mm x 500 mm (63 x 19,75 Zoll) rahmenloser Wandspiegel mit eingebauter LED-Streifenbeleuchtung"))
						.put(Lang.FRA, getWords(" Rétroviseur mural sans cadre de 1600 mm x 500 mm (63 x 19,75 po) avec éclairage intégré à DEL")).build());
		translates
				.put(getCode("Maximum water capacity - 520 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 520 литров"))
						.put(Lang.ITA, getWords(" Capacità massima d'acqua - 520 litri"))
						.put(Lang.ESP, getWords(" Capacidad máxima de agua - 520 Litros"))
						.put(Lang.DEU, getWords(" Maximale Wasserkapazität - 520 Liter"))
						.put(Lang.FRA, getWords(" Capacité maximale en eau - 520 Litres")).build());
		translates
				.put(getCode("15 (380 mm) round built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("15 (380 мм) круглая встроенная душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione da incasso rotondo da 380 mm (15 )"))
						.put(Lang.ESP, getWords(" Cabezal de ducha redondo redondo de 15 (380 mm)"))
						.put(Lang.DEU, getWords(" 15 (380 mm) runde Einbaubrause"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée ronde de 15 (380 mm)")).build());
		translates
				.put(getCode("Anti-condensation plastic cistern with double flow system is included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("В комплект входит антиконденсационная пластиковая цистерна с системой двойного потока"))
						.put(Lang.ITA, getWords(" La cassetta di plastica anti-condensa con doppio flusso è inclusa"))
						.put(Lang.ESP, getWords(" Cisterna de plástico anticondensación con sistema de doble flujo incluido"))
						.put(Lang.DEU, getWords(" Anti-Kondensat-Zisterne mit Doppel-Flow-System ist enthalten"))
						.put(Lang.FRA, getWords(" Réservoir en plastique anti-condensation avec système double flux inclus")).build());
		translates
				.put(getCode("Homogeneous throughout"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Однородно"))
						.put(Lang.ITA, getWords(" Omogeneo in tutto"))
						.put(Lang.ESP, getWords(" Homogéneo a lo largo"))
						.put(Lang.DEU, getWords(" Durchgängig homogen"))
						.put(Lang.FRA, getWords(" Homogène partout")).build());
		translates
				.put(getCode("220V power with 16A RCD protected"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мощность 220 В с защитой 16 А"))
						.put(Lang.ITA, getWords(" Alimentazione a 220 V con protezione RCD da 16 A protetta"))
						.put(Lang.ESP, getWords(" 220 V de potencia con 16 A protegido en RCD"))
						.put(Lang.DEU, getWords(" 220V Leistung mit 16A RCD geschützt"))
						.put(Lang.FRA, getWords(" Alimentation 220V avec protection RCD 16A")).build());
		translates
				.put(getCode("1 Year limited warranty on all electrical components"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1-летняя ограниченная гарантия на все электрические компоненты"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 1 anno su tutti i componenti elettrici"))
						.put(Lang.ESP, getWords(" 1 año de garantía limitada en todos los componentes eléctricos"))
						.put(Lang.DEU, getWords(" 1 Jahr beschränkte Garantie auf alle elektrischen Komponenten"))
						.put(Lang.FRA, getWords(" Garantie limitée de 1 an sur tous les composants électriques")).build());
		translates
				.put(getCode("EcoMarmor™ Lite  cutting edge light-weight cast stone composite in matte finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("EcoMarmor ™ Lite режущая кромка из легкого литого каменного композита с матовой отделкой"))
						.put(Lang.ITA, getWords(" EcoMarmor ™ Lite in composito leggero in pietra fuso all'avanguardia in finitura opaca"))
						.put(Lang.ESP, getWords(" Compuesto de piedra fundida liviana de vanguardia EcoMarmor ™ Lite en acabado mate"))
						.put(Lang.DEU, getWords(" EcoMarmor ™ Lite Leichtbau-Gussstein in mattem Finish"))
						.put(Lang.FRA, getWords(" EcoMarmor ™ Lite Composite en pierre coulée légère de pointe en finition mate")).build());
		translates
				.put(getCode("Designed for comfortable one-person bathing or two-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного купания одного человека или купания двух человек"))
						.put(Lang.ITA, getWords(" Progettato per una balneazione individuale da una sola persona o una balneazione per due persone"))
						.put(Lang.ESP, getWords(" Diseñado para un baño cómodo para una persona o para dos personas"))
						.put(Lang.DEU, getWords(" Entworfen für komfortables Ein-Personen-Baden oder Zwei-Personen-Baden"))
						.put(Lang.FRA, getWords(" Conçu pour la baignade confortable d'une personne ou la baignade de deux personnes")).build());
		translates
				.put(getCode("Handy, vertical open-fronted, two shelf wall unit in matt yellow lacquer 500 mm high x 150 mm wide by 210 mm deep / 19.75 x 6 x 8.25 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобный вертикальный открытый пол, двухслойный стеновой блок в матовом желтом лакере 500 мм в высоту х 150 мм в ширину на 210 мм в глубину / 19,75 х 6 х 8,25 дюйма"))
						.put(Lang.ITA, getWords(" Comodo mobile verticale a due ante con due mensole in laccato giallo opaco da 500 mm di altezza x 150 mm di larghezza per 210 mm di profondità / 19,75 x 6 x 8,25 pollici"))
						.put(Lang.ESP, getWords(" Práctica, vertical, de fachada abierta, dos estantes de pared de lama en lacado amarillo mate de 500 mm de alto x 150 mm de ancho por 210 mm de profundidad / 19,75 x 6 x 8,25 pulgadas"))
						.put(Lang.DEU, getWords(" Handliche, vertikal offene, zweiregalige Wandeinheit in mattgelbem Lack 500 mm hoch x 150 mm breit und 210 mm tief / 19.75 x 6 x 8.25 in"))
						.put(Lang.FRA, getWords(" Unité murale pratique, à façade ouverte, à deux étagères, en laque jaune mat, 500 mm de haut x 150 mm de large sur 210 mm de profondeur / 19,75 x 6 x 8,25 po")).build());
		translates
				.put(getCode("Preinstalled cable drive waste-overflow fitting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предварительно установленный кабель для перекачки отходов"))
						.put(Lang.ITA, getWords(" Raccordo del troppo-troppopieno"))
						.put(Lang.ESP, getWords(" Accesorio de desbordamiento de desagüe de la unidad de cable preinstalado"))
						.put(Lang.DEU, getWords(" Vorinstallierte Abflussüberlaufgarnitur für Kabelantrieb"))
						.put(Lang.FRA, getWords(" Raccord de vidange de trop-plein à entraînement par câble préinstallé")).build());
		translates
				.put(getCode("Stylish metal griff handle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Стильная металлическая ручка"))
						.put(Lang.ITA, getWords(" Elegante impugnatura in metallo"))
						.put(Lang.ESP, getWords(" Elegante mango Griff de metal"))
						.put(Lang.DEU, getWords(" Stilvoller Griff aus Metallgriff"))
						.put(Lang.FRA, getWords(" Poignée griffée en métal élégante")).build());
		translates
				.put(getCode("1 ½  brass tube for female threaded stone bath wastes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 ½ латунная трубка для ванных резьбовых каменных ванн"))
						.put(Lang.ITA, getWords(" Tubo da 1 ½ in ottone per rifiuti di bagni con filettatura femmina"))
						.put(Lang.ESP, getWords(" Tubo de latón de 1 ½ para desechos de baño de piedra con rosca hembra"))
						.put(Lang.DEU, getWords(" 1 ½ Messingrohr für weibliche Gewinde-Steinbadabfälle"))
						.put(Lang.FRA, getWords(" Tube en laiton de 1 ½ pour les déchets de bain en pierre filetés femelles")).build());
		translates
				.put(getCode("Low voltage transformer (12V)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Трансформатор низкого напряжения (12 В)"))
						.put(Lang.ITA, getWords(" Trasformatore a bassa tensione (12V)"))
						.put(Lang.ESP, getWords(" Transformador de bajo voltaje (12V)"))
						.put(Lang.DEU, getWords(" Niederspannungstransformator (12V)"))
						.put(Lang.FRA, getWords(" Transformateur basse tension (12V)")).build());
		translates
				.put(getCode("Matching chrome-plated hand shower with flexible hose and shower control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствующий хромированный ручной душ с гибким шлангом и душем"))
						.put(Lang.ITA, getWords(" Doccetta cromata abbinata con tubo flessibile e comando doccia"))
						.put(Lang.ESP, getWords(" Cojín de ducha cromado a juego con manguera flexible y control de ducha"))
						.put(Lang.DEU, getWords(" Passende verchromte Handbrause mit flexiblem Schlauch- und Duschregler"))
						.put(Lang.FRA, getWords(" Douchette à main chromée assortie avec flexible et commande de douche")).build());
		translates
				.put(getCode("Comes complete with all the installation equipment"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поставляется со всем оборудованием для установки"))
						.put(Lang.ITA, getWords(" Viene fornito completo di tutte le attrezzature di installazione"))
						.put(Lang.ESP, getWords(" Viene completo con todo el equipo de instalación"))
						.put(Lang.DEU, getWords(" Kommt komplett mit allen Installationsgeräten"))
						.put(Lang.FRA, getWords(" Livré complet avec tout l'équipement d'installation")).build());
		translates
				.put(getCode("Waste joint is factory sealed and tested; no solvent weld"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мусорное соединение запечатывается и проверяется на заводе;  нет сварки растворителем"))
						.put(Lang.ITA, getWords(" Il giunto dei rifiuti è sigillato e testato in fabbrica;  nessuna saldatura di solventi"))
						.put(Lang.ESP, getWords(" La junta de desechos se sella y prueba en fábrica;  sin soldaduras solventes"))
						.put(Lang.DEU, getWords(" Überlauf wird werksseitig versiegelt und geprüft;  keine Lösungsmittelschweißung"))
						.put(Lang.FRA, getWords(" Le joint de rebut est scellé et testé en usine;  pas de soudure au solvant")).build());
		translates
				.put(getCode("Safe and reliable - the overflow is always open"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Безопасный и надежный - переполнение всегда открыто"))
						.put(Lang.ITA, getWords(" Sicuro e affidabile: l'overflow è sempre aperto"))
						.put(Lang.ESP, getWords(" Seguro y confiable: el desbordamiento siempre está abierto"))
						.put(Lang.DEU, getWords(" Sicher und zuverlässig - der Überlauf ist immer offen"))
						.put(Lang.FRA, getWords(" Sûr et fiable - le débordement est toujours ouvert")).build());
		translates
				.put(getCode("White cushions with brushed gold frames"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Белые подушки с матовыми золотыми рамами"))
						.put(Lang.ITA, getWords(" Cuscini bianchi con montatura in oro spazzolato"))
						.put(Lang.ESP, getWords(" Cojines blancos con marcos dorados cepillados"))
						.put(Lang.DEU, getWords(" Weiße Kissen mit gebürsteten Goldrahmen"))
						.put(Lang.FRA, getWords(" Coussins blancs avec des cadres en or brossé")).build());
		translates
				.put(getCode("Large 17.7x7.9 (450x200 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая насадка 17.7x7.9 (450x200 мм)"))
						.put(Lang.ITA, getWords(" Grande soffione 17.7x7.9 (450x200 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 17.7x7.9 (450x200 mm)"))
						.put(Lang.DEU, getWords(" Großer Duschkopf (17.7x7.9 (450x200 mm))"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 17,7 x 7,9 po (450 x 200 mm)")).build());
		translates
				.put(getCode("Mélange dark grey cushions with white powder-coated frames"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Меланж темно-серые подушки с белыми раковинами с порошковым покрытием"))
						.put(Lang.ITA, getWords(" Cuscini Mélange grigio scuro con cornici verniciate a polvere bianca"))
						.put(Lang.ESP, getWords(" Cojines Mélange gris oscuro con marcos blancos con recubrimiento de polvo"))
						.put(Lang.DEU, getWords(" Melange dunkelgraue Kissen mit weißen pulverbeschichteten Rahmen"))
						.put(Lang.FRA, getWords(" Coussins gris foncé mélangés avec des cadres blancs enduits de poudre")).build());
		translates
				.put(getCode("Energy Saving Cover"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Энергосберегающая крышка"))
						.put(Lang.ITA, getWords(" Copertura a risparmio energetico"))
						.put(Lang.ESP, getWords(" Cubierta de ahorro de energía"))
						.put(Lang.DEU, getWords(" Energiesparende Abdeckung"))
						.put(Lang.FRA, getWords(" Couverture d'économie d'énergie")).build());
		translates
				.put(getCode("LED wall mounted overhead lamp"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Светодиодная настенная лампа"))
						.put(Lang.ITA, getWords(" Lampada da soffitto a LED a parete"))
						.put(Lang.ESP, getWords(" Lámpara de techo LED montada en la pared"))
						.put(Lang.DEU, getWords(" LED-Deckenlampe"))
						.put(Lang.FRA, getWords(" Lampe plafonnier à LED")).build());
		translates
				.put(getCode("Designed to coordinate beautifully with a selection of Aquatica's baths"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для красивой координации с выбором ванн Aquatica"))
						.put(Lang.ITA, getWords(" Progettato per coordinarsi magnificamente con una selezione di bagni Aquatica"))
						.put(Lang.ESP, getWords(" Diseñado para coordinar maravillosamente con una selección de baños de Aquatica"))
						.put(Lang.DEU, getWords(" Entworfen, um sich wunderbar mit einer Auswahl von Aquatica-Bädern zu koordinieren"))
						.put(Lang.FRA, getWords(" Conçu pour coordonner magnifiquement avec une sélection de bains Aquatica")).build());
		translates
				.put(getCode("Stain resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Пятностойкое"))
						.put(Lang.ITA, getWords(" Resistente alle macchie"))
						.put(Lang.ESP, getWords(" Resistente a las manchas"))
						.put(Lang.DEU, getWords(" Schmutzabweisend"))
						.put(Lang.FRA, getWords(" Résistant aux taches")).build());
		translates
				.put(getCode("Easy to clean"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Легко очистить"))
						.put(Lang.ITA, getWords(" Facile da pulire"))
						.put(Lang.ESP, getWords(" Fácil de limpiar"))
						.put(Lang.DEU, getWords(" Einfach zu säubern"))
						.put(Lang.FRA, getWords(" Facile à nettoyer")).build());
		translates
				.put(getCode("A consistent, non-painted color throughout the tubs thickness which won't fade or lose its brilliance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Совместимый, непокрашенный цвет по толщине ванны, который не будет исчезать или потерять свой блеск"))
						.put(Lang.ITA, getWords(" Un colore consistente e non verniciato su tutto lo spessore della vasca che non sbiadirà o perderà la sua brillantezza"))
						.put(Lang.ESP, getWords(" Un color uniforme y no pintado en todo el espesor de las bañeras que no se desvanecerá ni perderá su brillo"))
						.put(Lang.DEU, getWords(" Eine gleichbleibende, nicht gestrichene Farbe in der ganzen Wannenstärke, die nicht verblasst oder ihre Brillanz verliert"))
						.put(Lang.FRA, getWords(" Une couleur uniforme et non peinte dans toute l'épaisseur de la cuve qui ne pâlira pas et ne perdra pas sa brillance")).build());
		translates
				.put(getCode("Unique to acrylic bathtubs – a slot style overflow opening for deeper bathing experience"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникально для акриловых ванн - открытие переполнения слота для более глубокого купания"))
						.put(Lang.ITA, getWords(" Vasche da bagno in acrilico esclusive: un'apertura a sfioro con scanalatura per un'esperienza di bagno più profonda"))
						.put(Lang.ESP, getWords(" Único en bañeras acrílicas: una abertura de desbordamiento estilo ranura para una experiencia de baño más profunda"))
						.put(Lang.DEU, getWords(" Einzigartig in Acrylbadewannen - eine Schlitz-Überlauföffnung für ein tieferes Badeerlebnis"))
						.put(Lang.FRA, getWords(" Unique aux baignoires en acrylique - une ouverture de débordement de style fente pour une expérience de baignade plus profonde")).build());
		translates
				.put(getCode("Extra large 19.7x19.7 (500x500 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра большая 19.7x19.7 (500x500 мм) душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione extra large 19.7x19.7 (500x500 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 19.7x19.7 (500x500 mm)"))
						.put(Lang.DEU, getWords(" Extra großer 19.7x19.7 (500x500 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 19,7 x 19,7 po (500 x 500 mm)")).build());
		translates
				.put(getCode("Frequency response: 35-20000 hz"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Частотная характеристика: 35-20000 Гц"))
						.put(Lang.ITA, getWords(" Risposta in frequenza: 35-20000 hz"))
						.put(Lang.ESP, getWords(" Respuesta de frecuencia: 35-20000 hz"))
						.put(Lang.DEU, getWords(" Frequenzgang: 35-20000 Hz"))
						.put(Lang.FRA, getWords(" Réponse en fréquence: 35-20000 hz")).build());
		translates
				.put(getCode("Arm length 13.75 (35 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукоятки 13,75 (35 см)"))
						.put(Lang.ITA, getWords(" Braccio lunghezza 13,75 (35 cm)"))
						.put(Lang.ESP, getWords(" Longitud del brazo 13.75 (35 cm)"))
						.put(Lang.DEU, getWords(" Armlänge 13,75 (35 cm)"))
						.put(Lang.FRA, getWords(" Longueur de bras 13.75 (35 cm)")).build());
		translates
				.put(getCode("Powder-coated stainless-steel frame and arms"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Порошковая рама из нержавеющей стали и рукава"))
						.put(Lang.ITA, getWords(" Telaio e braccioli in acciaio inossidabile verniciato a polvere"))
						.put(Lang.ESP, getWords(" Armazón y brazos de acero inoxidable recubiertos de polvo"))
						.put(Lang.DEU, getWords(" Gestell und Arme aus pulverbeschichtetem Edelstahl"))
						.put(Lang.FRA, getWords(" Structure et bras en acier inoxydable enduits de poudre")).build());
		translates
				.put(getCode("Water repelent"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Водоотталкивающая"))
						.put(Lang.ITA, getWords(" Acqua repellente"))
						.put(Lang.ESP, getWords(" Repelente al agua"))
						.put(Lang.DEU, getWords(" Wasserabweisend"))
						.put(Lang.FRA, getWords(" Répulsif d'eau")).build());
		translates
				.put(getCode("Constructed of AquateX™ Matte in fine matte finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен из AquateX ™ Matte в матовой матовой отделке"))
						.put(Lang.ITA, getWords(" Realizzato in AquateX ™ Matte con finitura opaca"))
						.put(Lang.ESP, getWords(" Construido de AquateX ™ mate en fino acabado mate"))
						.put(Lang.DEU, getWords(" Aus AquateX ™ Matte in feinem Matt-Finish"))
						.put(Lang.FRA, getWords(" Construit de AquateX ™ Matte dans la finition mate fine")).build());
		translates
				.put(getCode("Striking upscale and modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удивительный высококлассный и современный дизайн"))
						.put(Lang.ITA, getWords(" Striking di alto livello e design moderno"))
						.put(Lang.ESP, getWords(" Sorprendente diseño exclusivo y moderno"))
						.put(Lang.DEU, getWords(" Auffallendes gehobenes und modernes Design"))
						.put(Lang.FRA, getWords(" Design haut de gamme et moderne")).build());
		translates
				.put(getCode("Suggested installation height -  50-70cm above the user’s head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемая высота установки - 50-70 см над головой пользователя"))
						.put(Lang.ITA, getWords(" Altezza di installazione suggerita: 50-70 cm sopra la testa dell'utente"))
						.put(Lang.ESP, getWords(" Altura sugerida de instalación: 50-70 cm por encima de la cabeza del usuario"))
						.put(Lang.DEU, getWords(" Empfohlene Installationshöhe - 50-70 cm über dem Kopf des Benutzers"))
						.put(Lang.FRA, getWords(" Hauteur d'installation suggérée - 50-70 cm au-dessus de la tête de l'utilisateur")).build());
		translates
				.put(getCode("Ergonomically shaped hand grip"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичная рукоятка"))
						.put(Lang.ITA, getWords(" Impugnatura ergonomica"))
						.put(Lang.ESP, getWords(" Empuñadura ergonómica"))
						.put(Lang.DEU, getWords(" Ergonomisch geformter Handgriff"))
						.put(Lang.FRA, getWords(" Poignée ergonomique")).build());
		translates
				.put(getCode("Velvety, warm, smooth and pleasant to the touch hypoallergenic surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Бархатистая, теплая, гладкая и приятная на ощупь гипоаллергенная поверхность"))
						.put(Lang.ITA, getWords(" Superficie ipoallergenica vellutata, calda, liscia e piacevole al tatto"))
						.put(Lang.ESP, getWords(" Aterciopelado, cálido, suave y agradable al tacto superficie hipoalergénica"))
						.put(Lang.DEU, getWords(" Samtig, warm, glatt und angenehm im Griff hypoallergene Oberfläche"))
						.put(Lang.FRA, getWords(" Velouté, chaud, lisse et agréable au toucher surface hypoallergénique")).build());
		translates
				.put(getCode("Dark brown wood-grain composite veneer base – 900 x 500 x 250 mm / 35.5 x 19.75 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Темно-коричневая композитная шпонная древесина - 900 x 500 x 250 мм / 35,5 x 19,75 x 10 дюймов"))
						.put(Lang.ITA, getWords(" Base impiallacciata in legno composito venato marrone scuro - 900 x 500 x 250 mm / 35,5 x 19,75 x 10 in"))
						.put(Lang.ESP, getWords(" Base de chapa compuesta de grano de madera marrón oscuro - 900 x 500 x 250 mm / 35.5 x 19.75 x 10 in"))
						.put(Lang.DEU, getWords(" Dunkelbrauner Holzfaserverbund-Furnierboden - 900 x 500 x 250 mm / 35,5 x 19,75 x 10 in"))
						.put(Lang.FRA, getWords(" Base de placage en composite bois brun foncé - 900 x 500 x 250 mm / 35,5 x 19,75 x 10 po")).build());
		translates
				.put(getCode("Optional aromatherapy package"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительный пакет ароматерапии"))
						.put(Lang.ITA, getWords(" Pacchetto aromaterapia opzionale"))
						.put(Lang.ESP, getWords(" Paquete de aromaterapia opcional"))
						.put(Lang.DEU, getWords(" Optionales Aromatherapie-Paket"))
						.put(Lang.FRA, getWords(" Paquet d'aromathérapie en option")).build());
		translates
				.put(getCode("Wall-mounted built-in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный встроенный"))
						.put(Lang.ITA, getWords(" Built-in a parete"))
						.put(Lang.ESP, getWords(" Montado en la pared"))
						.put(Lang.DEU, getWords(" Wandeinbau"))
						.put(Lang.FRA, getWords(" Mural encastré")).build());
		translates
				.put(getCode("No filters or chemicals"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Нет фильтров или химикатов"))
						.put(Lang.ITA, getWords(" Nessun filtro o prodotto chimico"))
						.put(Lang.ESP, getWords(" Sin filtros ni productos químicos"))
						.put(Lang.DEU, getWords(" Keine Filter oder Chemikalien"))
						.put(Lang.FRA, getWords(" Pas de filtres ou de produits chimiques")).build());
		translates
				.put(getCode("1” Automatic hidden filling valve"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 Автоматический скрытый заправочный клапан"))
						.put(Lang.ITA, getWords(" 1 valvola di riempimento automatica nascosta"))
						.put(Lang.ESP, getWords(" 1 válvula de llenado oculta automática"))
						.put(Lang.DEU, getWords(" 1 automatisches verdecktes Füllventil"))
						.put(Lang.FRA, getWords(" 1 soupape de remplissage cachée automatique")).build());
		translates
				.put(getCode("36 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("36 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords(" 36 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords(" 36 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords(" 36 flache Luftdüsen"))
						.put(Lang.FRA, getWords(" 36 jets d'air à profil bas")).build());
		translates
				.put(getCode("Matt white laminate side panels"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовые белые ламинированные боковые панели"))
						.put(Lang.ITA, getWords(" Pannelli laterali in laminato bianco opaco"))
						.put(Lang.ESP, getWords(" Paneles laterales laminados en blanco mate"))
						.put(Lang.DEU, getWords(" Mattweiß Laminat Seitenwände"))
						.put(Lang.FRA, getWords(" Panneaux latéraux en stratifié blanc mat")).build());
		translates
				.put(getCode("Rich organic colour incorporating ‘oxidised metal effect"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Богатый органический цвет, включающий «окисленный металлический эффект»"))
						.put(Lang.ITA, getWords(" Colore organico ricco che incorpora l'effetto metallo ossidato"))
						.put(Lang.ESP, getWords(" Color orgánico intenso que incorpora el efecto de metal oxidado"))
						.put(Lang.DEU, getWords(" Rich organische Farbe mit oxidiertem Metall-Effekt"))
						.put(Lang.FRA, getWords(" Couleur organique riche incorporant 'effet de métal oxydé")).build());
		translates
				.put(getCode("Oiled oak countertop - 1630 x 505 x 60 mm / 64.25 x 20 x 2.25 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сальниковая столешница из дуба - 1630 x 505 x 60 мм / 64,25 x 20 x 2,25 дюйма"))
						.put(Lang.ITA, getWords(" Controsoffitto rovere oliato - 1630 x 505 x 60 mm / 64,25 x 20 x 2,25 pollici"))
						.put(Lang.ESP, getWords(" Encimera de roble engrasado - 1630 x 505 x 60 mm / 64.25 x 20 x 2.25 in"))
						.put(Lang.DEU, getWords(" Geölte Eiche Arbeitsplatte - 1630 x 505 x 60 mm / 64,25 x 20 x 2,25 in"))
						.put(Lang.FRA, getWords(" Plan de travail en chêne huilé - 1630 x 505 x 60 mm / 64,25 x 20 x 2,25 po")).build());
		translates
				.put(getCode("LED light over mirror"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Светодиодный свет над зеркалом"))
						.put(Lang.ITA, getWords(" LED su specchio"))
						.put(Lang.ESP, getWords(" Luz LED sobre el espejo"))
						.put(Lang.DEU, getWords(" LED-Licht über Spiegel"))
						.put(Lang.FRA, getWords(" Lumière LED sur le miroir")).build());
		translates
				.put(getCode("Bluetooth Audio system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Аудиосистема Bluetooth"))
						.put(Lang.ITA, getWords(" Sistema audio Bluetooth"))
						.put(Lang.ESP, getWords(" Sistema de audio Bluetooth"))
						.put(Lang.DEU, getWords(" Bluetooth Audiosystem"))
						.put(Lang.FRA, getWords(" Système audio Bluetooth")).build());
		translates
				.put(getCode("Touch activated on/off sensor to prevent over spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сенсорный датчик включения / выключения для предотвращения перелива"))
						.put(Lang.ITA, getWords(" Tocca il sensore on / off attivato per prevenire spruzzi eccessivi"))
						.put(Lang.ESP, getWords(" Tocar el sensor activado / desactivado para evitar el exceso de pulverización"))
						.put(Lang.DEU, getWords(" Berühren Sie den aktivierten Ein / Aus-Sensor, um ein Übersprühen zu verhindern"))
						.put(Lang.FRA, getWords(" Touchez le capteur marche / arrêt activé pour éviter les pulvérisations excessives")).build());
		translates
				.put(getCode("Preinstalled pop up waste and integrated overflow channel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предустановленные всплывающие отходы и интегрированный канал переполнения"))
						.put(Lang.ITA, getWords(" Scarico pop up preinstallato e canale di sfioro integrato"))
						.put(Lang.ESP, getWords(" Desagüe automático preinstalado y canal de desbordamiento integrado"))
						.put(Lang.DEU, getWords(" Vorinstallierter Pop-Up-Abfall und integrierter Überlaufkanal"))
						.put(Lang.FRA, getWords(" Dévidoir préinstallé et canal de débordement intégré")).build());
		translates
				.put(getCode("2-years electronic and electric component warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя гарантия на электронные и электрические компоненты"))
						.put(Lang.ITA, getWords(" Garanzia per i componenti elettronici ed elettrici di 2 anni"))
						.put(Lang.ESP, getWords(" 2 años de garantía para componentes electrónicos y eléctricos"))
						.put(Lang.DEU, getWords(" 2 Jahre Garantie auf elektronische und elektrische Komponenten"))
						.put(Lang.FRA, getWords(" Garantie de 2 ans sur les composants électroniques et électriques")).build());
		translates
				.put(getCode("Wall-mounted at minimum of 1.6 metres height or human's height plus 7 (18 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенные на высоте не менее 1,6 метра или высота человека плюс 7 (18 см)"))
						.put(Lang.ITA, getWords(" Montaggio a parete ad un'altezza minima di 1,6 metri o altezza umana più 7 (18 cm)"))
						.put(Lang.ESP, getWords(" Montado en la pared a una altura mínima de 1.6 metros o la altura del ser humano más 7 (18 cm)"))
						.put(Lang.DEU, getWords(" Wandmontage bei mindestens 1,6 Meter Höhe oder Menschenhöhe plus 7 (18 cm)"))
						.put(Lang.FRA, getWords(" Mural d'au moins 1,6 mètre de hauteur ou de hauteur humaine plus 7 (18 cm)")).build());
		translates
				.put(getCode("Minimalist modern design and superior Northern European workmanship"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минималистский современный дизайн и превосходное североевропейское мастерство"))
						.put(Lang.ITA, getWords(" Design moderno e minimalista e lavorazione del nord Europa superiore"))
						.put(Lang.ESP, getWords(" Diseño moderno minimalista y mano de obra superior del norte de Europa"))
						.put(Lang.DEU, getWords(" Minimalistisches, modernes Design und hochwertige nordeuropäische Verarbeitung"))
						.put(Lang.FRA, getWords(" Design moderne minimaliste et fabrication supérieure en Europe du Nord")).build());
		translates
				.put(getCode("Low surface moisture absorbency"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Низкая поверхностная влажность"))
						.put(Lang.ITA, getWords(" Assorbimento dell'umidità superficiale ridotto"))
						.put(Lang.ESP, getWords(" Baja absorbencia de humedad superficial"))
						.put(Lang.DEU, getWords(" Geringe Oberflächenfeuchtigkeitsaufnahme"))
						.put(Lang.FRA, getWords(" Absorption d'humidité de surface faible")).build());
		translates
				.put(getCode("Can be plumbed to existing stub-outs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Может быть отнесен к существующим"))
						.put(Lang.ITA, getWords(" Può essere scandagliato da estensioni esistenti"))
						.put(Lang.ESP, getWords(" Se puede conectar a los stub-outs existentes"))
						.put(Lang.DEU, getWords(" Kann zu vorhandenen Stubouts ausgelotet werden"))
						.put(Lang.FRA, getWords(" Peut être raccordé aux embouchures existantes")).build());
		translates
				.put(getCode("Wave jet from +15° to -15° forwards angle"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Волновая струя от + 15 ° до -15 ° переднего угла"))
						.put(Lang.ITA, getWords(" Getto d'onda da + 15 ° a -15 ° in avanti"))
						.put(Lang.ESP, getWords(" Chorro de olas desde + 15 ° hasta -15 ° en ángulo hacia adelante"))
						.put(Lang.DEU, getWords(" Wellenstrahl von + 15 ° bis -15 ° nach vorne"))
						.put(Lang.FRA, getWords(" Jet d'onde de + 15 ° à -15 ° vers l'avant")).build());
		translates
				.put(getCode("Premium Lucite® acrylic with high gloss white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум Lucite® акрил с белой поверхностью с высоким блеском"))
						.put(Lang.ITA, getWords(" Acrilico Premium Lucite® con superficie bianca lucida"))
						.put(Lang.ESP, getWords(" Acrílico Lucite® Premium con superficie blanca de alto brillo"))
						.put(Lang.DEU, getWords(" Premium Lucite® Acryl mit hochglänzender weißer Oberfläche"))
						.put(Lang.FRA, getWords(" Acrylique Premium Lucite® avec surface blanche très brillante")).build());
		translates
				.put(getCode("Constructed of stainless steel AISI 316 L"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из нержавеющей стали AISI 316 L"))
						.put(Lang.ITA, getWords(" Costruito in acciaio inossidabile AISI 316 L"))
						.put(Lang.ESP, getWords(" Construido de acero inoxidable AISI 316 L"))
						.put(Lang.DEU, getWords(" Hergestellt aus Edelstahl AISI 316 L"))
						.put(Lang.FRA, getWords(" Construit en acier inoxydable AISI 316 L")).build());
		translates
				.put(getCode("Hard-wearing fabric that is removable and easy to clean"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Жесткая ткань, которая является съемной и легко очищаемой"))
						.put(Lang.ITA, getWords(" Tessuto resistente che è rimovibile e facile da pulire"))
						.put(Lang.ESP, getWords(" Tejido resistente que es extraíble y fácil de limpiar"))
						.put(Lang.DEU, getWords(" Strapazierfähiges Gewebe, das abnehmbar und leicht zu reinigen ist"))
						.put(Lang.FRA, getWords(" Tissu résistant, amovible et facile à nettoyer")).build());
		translates
				.put(getCode("Save on plumbers time and costs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экономия на санях время и затраты"))
						.put(Lang.ITA, getWords(" Risparmia tempo e costi sugli idraulici"))
						.put(Lang.ESP, getWords(" Ahorre tiempo y costos de plomeros"))
						.put(Lang.DEU, getWords(" Sparen Sie Zeit und Kosten"))
						.put(Lang.FRA, getWords(" Économisez sur le temps et les coûts des plombiers")).build());
		translates
				.put(getCode("Modern interpretation of traditional Japanese Ofuro heated bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современная интерпретация традиционной японской подогреваемой ванны Ofuro"))
						.put(Lang.ITA, getWords(" Interpretazione moderna della tradizionale vasca da bagno riscaldata Ofuro giapponese"))
						.put(Lang.ESP, getWords(" Interpretación moderna de la bañera tradicional japonesa Ofuro con calefacción"))
						.put(Lang.DEU, getWords(" Moderne Interpretation der traditionellen japanischen beheizten Ofuro-Badewanne"))
						.put(Lang.FRA, getWords(" Interprétation moderne de la baignoire chauffée traditionnelle japonaise Ofuro")).build());
		translates
				.put(getCode("Feminine cleansing and kids function"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Женская очистка и дети"))
						.put(Lang.ITA, getWords(" Pulizia femminile e funzione bambini"))
						.put(Lang.ESP, getWords(" La limpieza femenina y la función de los niños"))
						.put(Lang.DEU, getWords(" Feminine Reinigung und Kinderfunktion"))
						.put(Lang.FRA, getWords(" Le nettoyage féminin et la fonction des enfants")).build());
		translates
				.put(getCode("Curve shaped glass"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Криволинейное стекло"))
						.put(Lang.ITA, getWords(" Vetro a forma di curva"))
						.put(Lang.ESP, getWords(" Vidrio en forma de curva"))
						.put(Lang.DEU, getWords(" Kurvenförmiges Glas"))
						.put(Lang.FRA, getWords(" Verre en forme de courbe")).build());
		translates
				.put(getCode("Single function rainhead water flow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Одиночный поток воды с дождевой водой"))
						.put(Lang.ITA, getWords(" Flusso d'acqua a singola testa di pioggia"))
						.put(Lang.ESP, getWords(" Flujo de agua de lluvia única de función única"))
						.put(Lang.DEU, getWords(" Einzelfunktion Regenkopf Wasserfluss"))
						.put(Lang.FRA, getWords(" Flux d'eau de tête de pluie à fonction unique")).build());
		translates
				.put(getCode("Very hard-wearing, durable, longlife material"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень прочный, долговечный, долговечный материал"))
						.put(Lang.ITA, getWords(" Materiale molto resistente, durevole e di lunga durata"))
						.put(Lang.ESP, getWords(" Material muy resistente, duradero y duradero"))
						.put(Lang.DEU, getWords(" Sehr strapazierfähiges, langlebiges und langlebiges Material"))
						.put(Lang.FRA, getWords(" Matériau durable, très durable")).build());
		translates
				.put(getCode("Arm heght 11.8 (30 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высота рукоятки 11,8 (30 см)"))
						.put(Lang.ITA, getWords(" Arm heght 11,8 (30 cm)"))
						.put(Lang.ESP, getWords(" Brazo de 11.8 (30 cm)"))
						.put(Lang.DEU, getWords(" Armhöhe 11,8 (30 cm)"))
						.put(Lang.FRA, getWords(" Bras de 11,8 (30 cm)")).build());
		translates
				.put(getCode("Chrome-plated drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромированный слив"))
						.put(Lang.ITA, getWords(" Scarico cromato"))
						.put(Lang.ESP, getWords(" Drenaje cromado"))
						.put(Lang.DEU, getWords(" Chromierter Ablauf"))
						.put(Lang.FRA, getWords(" Drain chromé")).build());
		translates
				.put(getCode("Cushions with removable covers, easy to wash and weather resistant"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подушки со съемными крышками, легко моются и устойчивы к атмосферным воздействиям"))
						.put(Lang.ITA, getWords(" Cuscini sfoderabili, facili da lavare e resistenti agli agenti atmosferici"))
						.put(Lang.ESP, getWords(" Cojines con fundas extraíbles, fáciles de lavar y resistentes a la intemperie"))
						.put(Lang.DEU, getWords(" Kissen mit abnehmbaren Bezügen, einfach zu waschen und witterungsbeständig"))
						.put(Lang.FRA, getWords(" Coussins avec housses amovibles, faciles à laver et résistants aux intempéries")).build());
		translates
				.put(getCode("Posterior cleansing with a variable spray (5 settings); aerated spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Задняя очистка с переменным спреем (5 настроек);  аэрозольный аэрозоль"))
						.put(Lang.ITA, getWords(" Pulizia posteriore con spray variabile (5 impostazioni);  spray aerato"))
						.put(Lang.ESP, getWords(" Limpieza posterior con un spray variable (5 configuraciones);  chorro aireado"))
						.put(Lang.DEU, getWords(" Hintere Reinigung mit einem variablen Spray (5 Einstellungen);  belüfteter Spray"))
						.put(Lang.FRA, getWords(" Nettoyage postérieur avec un spray variable (5 réglages);  aérosol")).build());
		translates
				.put(getCode("1 Year Limited Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 1 год"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 1 anno"))
						.put(Lang.ESP, getWords(" Garantía limitada de 1 año"))
						.put(Lang.DEU, getWords(" 1 Jahr beschränkte Garantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 1 an")).build());
		translates
				.put(getCode("Innovative and truly distinctive design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Инновационный и по-настоящему отличительный дизайн"))
						.put(Lang.ITA, getWords(" Design innovativo e davvero distintivo"))
						.put(Lang.ESP, getWords(" Diseño innovador y verdaderamente distintivo"))
						.put(Lang.DEU, getWords(" Innovatives und unverwechselbares Design"))
						.put(Lang.FRA, getWords(" Design innovant et vraiment distinctif")).build());
		translates
				.put(getCode("Included handheld shower attachment allows for a more targeted cleaning"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("В комплект поставки переносного душа входит более целенаправленная очистка"))
						.put(Lang.ITA, getWords(" L'accessorio per doccia palmare incluso consente una pulizia più mirata"))
						.put(Lang.ESP, getWords(" El accesorio de ducha de mano incluido permite una limpieza más específica"))
						.put(Lang.DEU, getWords(" Die mitgelieferte Handbrause ermöglicht eine gezieltere Reinigung"))
						.put(Lang.FRA, getWords(" L'accessoire de douche à main inclus permet un nettoyage plus ciblé")).build());
		translates
				.put(getCode("Made to be emptied after each use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изготавливается для опорожнения после каждого использования"))
						.put(Lang.ITA, getWords(" Fatto per essere svuotato dopo ogni uso"))
						.put(Lang.ESP, getWords(" Hecho para ser vaciado después de cada uso"))
						.put(Lang.DEU, getWords(" Nach jedem Gebrauch entleert werden"))
						.put(Lang.FRA, getWords(" Fabriqué pour être vidé après chaque utilisation")).build());
		translates
				.put(getCode("Matching anti-lime chrome-plated hand shower with slide rail and long life flexible hose"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствующий анти-липовый хромированный ручной душ с направляющей и гибким шлангом на долгий срок"))
						.put(Lang.ITA, getWords(" Doccia manuale cromata anticalcare con guida di scorrimento e tubo flessibile a lunga durata"))
						.put(Lang.ESP, getWords(" Cojín de ducha cromado anti-lima a juego con raíl deslizante y manguera flexible de larga duración"))
						.put(Lang.DEU, getWords(" Passende anti-Kalk-verchromte Handbrause mit Gleitschiene und langlebigem flexiblem Schlauch"))
						.put(Lang.FRA, getWords(" Douche à main chromée antidérapante assortie avec glissière et flexible longue durée")).build());
		translates
				.put(getCode("A total of 5 electrical outlets required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("В общей сложности требуется 5 электрических выходов"))
						.put(Lang.ITA, getWords(" Sono richieste in totale 5 prese elettriche"))
						.put(Lang.ESP, getWords(" Se requieren un total de 5 tomas de corriente"))
						.put(Lang.DEU, getWords(" Insgesamt sind 5 Steckdosen erforderlich"))
						.put(Lang.FRA, getWords(" Un total de 5 prises électriques requises")).build());
		translates
				.put(getCode("Geometrical, upscale and modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Геометрический, высококлассный и современный дизайн"))
						.put(Lang.ITA, getWords(" Design geometrico, raffinato e moderno"))
						.put(Lang.ESP, getWords(" Diseño geométrico, exclusivo y moderno"))
						.put(Lang.DEU, getWords(" Geometrisches, gehobenes und modernes Design"))
						.put(Lang.FRA, getWords(" Design géométrique, haut de gamme et moderne")).build());
		translates
				.put(getCode("Large 11 3/4 (300 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая головка 11 3/4 (300 мм)"))
						.put(Lang.ITA, getWords(" Grande soffione 11 3/4 (300 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 11 3/4 (300 mm)"))
						.put(Lang.DEU, getWords(" Großer 11 3/4 (300 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 11 3/4 po (300 mm)")).build());
		translates
				.put(getCode("Built-in cleaning and self-check capabilities"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная функция очистки и самоконтроля"))
						.put(Lang.ITA, getWords(" Funzionalità integrate di pulizia e autocontrollo"))
						.put(Lang.ESP, getWords(" Capacidad incorporada de limpieza y autocomprobación"))
						.put(Lang.DEU, getWords(" Integrierte Reinigungs- und Selbstüberprüfungsfunktionen"))
						.put(Lang.FRA, getWords(" Fonctions de nettoyage et d'autocontrôle intégrées")).build());
		translates
				.put(getCode("Available in white and black finishes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно в белом и черном исполнении"))
						.put(Lang.ITA, getWords(" Disponibile nelle finiture bianco e nero"))
						.put(Lang.ESP, getWords(" Disponible en acabados blanco y negro"))
						.put(Lang.DEU, getWords(" Erhältlich in weiß und schwarz"))
						.put(Lang.FRA, getWords(" Disponible en finitions blanc et noir")).build());
		translates
				.put(getCode("Max flow rate of 3.25 GPM (12.3 LPM) at 5 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,25 GPM (12,3 LPM) при давлении воды 5 бар"))
						.put(Lang.ITA, getWords(" Portata massima di 3,25 GPM (12,3 LPM) a 5 bar di pressione dell'acqua"))
						.put(Lang.ESP, getWords(" Caudal máximo de 3.25 GPM (12.3 LPM) a 5 bar de presión de agua"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 3,25 GPM (12,3 LPM) bei 5 bar Wasserdruck"))
						.put(Lang.FRA, getWords(" Débit maximal de 3,25 GPM (12,3 LPM) à une pression d'eau de 5 bars")).build());
		translates
				.put(getCode("Extremely robust, heavy duty constructions – made to last!"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чрезвычайно прочные, сверхмощные конструкции - сделаны дольше!"))
						.put(Lang.ITA, getWords(" Costruzioni estremamente robuste e resistenti, fatte per durare!"))
						.put(Lang.ESP, getWords(" Construcciones extremadamente robustas y resistentes, ¡hechas para durar!"))
						.put(Lang.DEU, getWords(" Extrem robuste, schwere Konstruktionen - langlebig!"))
						.put(Lang.FRA, getWords(" Constructions extrêmement robustes et robustes - faites pour durer!")).build());
		translates
				.put(getCode("Modern interpretation of traditional Japanese Ofuro bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Современная интерпретация традиционной японской ванной Ofuro"))
						.put(Lang.ITA, getWords(" Interpretazione moderna della tradizionale vasca giapponese Ofuro"))
						.put(Lang.ESP, getWords(" Interpretación moderna de la bañera tradicional japonesa Ofuro"))
						.put(Lang.DEU, getWords(" Moderne Interpretation der traditionellen japanischen Ofuro Badewanne"))
						.put(Lang.FRA, getWords(" Interprétation moderne de la baignoire traditionnelle japonaise Ofuro")).build());
		translates
				.put(getCode("Premium glass shower wall"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум стеклянная душевая стена"))
						.put(Lang.ITA, getWords(" Parete doccia in vetro Premium"))
						.put(Lang.ESP, getWords(" Pared de ducha de vidrio premium"))
						.put(Lang.DEU, getWords(" Premium Glas Duschwand"))
						.put(Lang.FRA, getWords(" Mur de douche en verre Premium")).build());
		translates
				.put(getCode("Ceramic disc cartridge"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Керамический дисковый картридж"))
						.put(Lang.ITA, getWords(" Cartuccia a dischi ceramici"))
						.put(Lang.ESP, getWords(" Cartucho de disco de cerámica"))
						.put(Lang.DEU, getWords(" Keramik-Disc-Kassette"))
						.put(Lang.FRA, getWords(" Cartouche de disque en céramique")).build());
		translates
				.put(getCode("2 year limited warranty for all electrical components and other parts, except drains; 2 year overall limited air massage warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя ограниченная гарантия на все электрические компоненты и другие детали, кроме стоков;  2-летняя ограниченная воздушная массажная гарантия"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 2 anni per tutti i componenti elettrici e le altre parti, ad eccezione degli scarichi;  Garanzia di 2 anni per un massaggio d'aria limitato"))
						.put(Lang.ESP, getWords(" Garantía limitada de 2 años para todos los componentes eléctricos y otras partes, excepto drenajes;  Garantía de masaje de aire limitado general de 2 años"))
						.put(Lang.DEU, getWords(" 2 Jahre beschränkte Garantie für alle elektrischen Komponenten und andere Teile, ausgenommen Abflüsse;  2 Jahre eingeschränkte Luftmassage Garantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 2 ans pour tous les composants électriques et autres pièces, sauf les drains;  Garantie limitée de 2 ans sur le massage de l'air")).build());
		translates
				.put(getCode("1 year limited warranty on the basin"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 год ограниченной гарантии на бассейн"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 1 anno sul bacino"))
						.put(Lang.ESP, getWords(" 1 año de garantía limitada en el lavabo"))
						.put(Lang.DEU, getWords(" 1 Jahr Garantie auf das Becken"))
						.put(Lang.FRA, getWords(" Garantie limitée de 1 an sur le bassin")).build());
		translates
				.put(getCode("Made of a solid surface material AquateX™ resulting in stylish appearance and pleasant to touch surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изготовлен из твердого поверхностного материала AquateX ™, что обеспечивает стильный внешний вид и приятную сенсорную поверхность"))
						.put(Lang.ITA, getWords(" Realizzato in un materiale di superficie solido AquateX ™ che conferisce un aspetto elegante e una superficie piacevole al tatto"))
						.put(Lang.ESP, getWords(" Hecho de un material de superficie sólida AquateX ™ que da como resultado una apariencia elegante y una superficie agradable al tacto"))
						.put(Lang.DEU, getWords(" AquateX ™ besteht aus einem soliden Oberflächenmaterial, das für ein elegantes Aussehen und eine angenehme Oberfläche sorgt"))
						.put(Lang.FRA, getWords(" Fait d'un matériau de surface solide AquateX ™ résultant en un aspect élégant et une surface agréable au toucher")).build());
	}

	public static void part3() {
		translates
				.put(getCode("Round wall-mounted shower arm to match your shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Круглая настенная душевая консоль, соответствующая душевой головке"))
						.put(Lang.ITA, getWords(" Braccio doccia tondo da parete per abbinare il tuo soffione"))
						.put(Lang.ESP, getWords(" Brazo de ducha redondo de pared para que coincida con tu cabezal de ducha"))
						.put(Lang.DEU, getWords(" Runder wandmontierter Duscharm passend zu Ihrem Duschkopf"))
						.put(Lang.FRA, getWords(" Bras de douche mural rond pour correspondre à votre pomme de douche")).build());
		translates
				.put(getCode("White LED light"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Белый светодиод"))
						.put(Lang.ITA, getWords(" LED bianco"))
						.put(Lang.ESP, getWords(" Luz LED blanca"))
						.put(Lang.DEU, getWords(" Weißes LED-Licht"))
						.put(Lang.FRA, getWords(" Lumière LED blanche")).build());
		translates
				.put(getCode("About 28 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Около 28 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords(" Circa 28 LED a basso profilo"))
						.put(Lang.ESP, getWords(" Alrededor de 28 LED de bajo perfil"))
						.put(Lang.DEU, getWords(" Etwa 28 Low-Profile-LEDs"))
						.put(Lang.FRA, getWords(" Environ 28 LEDs à profil bas")).build());
		translates
				.put(getCode("Premium acrylic and tub thickness provides for excellent heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум акрил и толщина ванны обеспечивают отличное удержание тепла"))
						.put(Lang.ITA, getWords(" Lo spessore acrilico e tubolare premium garantisce un'eccellente ritenzione del calore"))
						.put(Lang.ESP, getWords(" El espesor de acrílico y de tina superior proporciona una excelente retención de calor"))
						.put(Lang.DEU, getWords(" Die hochwertige Acryl- und Wannenstärke sorgt für eine hervorragende Wärmespeicherung"))
						.put(Lang.FRA, getWords(" L'épaisseur supérieure de l'acrylique et de la baignoire assure une excellente rétention de la chaleur")).build());
		translates
				.put(getCode("Variable Speed air massage with wave & pulse modes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Переменный воздушный массаж с волновыми и импульсными режимами"))
						.put(Lang.ITA, getWords(" Massaggio ad aria a velocità variabile con modalità onda e impulso"))
						.put(Lang.ESP, getWords(" Masaje de aire de velocidad variable con modos de onda y pulso"))
						.put(Lang.DEU, getWords(" Luftmassage mit variabler Geschwindigkeit mit Wellen- und Pulsmodi"))
						.put(Lang.FRA, getWords(" Massage à air à vitesse variable avec modes d'onde et d'impulsion")).build());
		translates
				.put(getCode("Capacious 11.8x15.7 (300x400 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Вместительная душевая головка 11.8x15.7 (300x400 мм)"))
						.put(Lang.ITA, getWords(" Soffione estensibile 11.8x15.7 (300x400 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha de 11.8x15.7 (300x400 mm)"))
						.put(Lang.DEU, getWords(" Geräumiger 11.8x15.7 (300x400 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche de 11,8 x 15,7 po (300 x 400 mm)")).build());
		translates
				.put(getCode("Easy installation on existing toilets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Простая установка на существующих туалетах"))
						.put(Lang.ITA, getWords(" Facile installazione su servizi igienici esistenti"))
						.put(Lang.ESP, getWords(" Instalación fácil en inodoros existentes"))
						.put(Lang.DEU, getWords(" Einfache Installation auf vorhandenen Toiletten"))
						.put(Lang.FRA, getWords(" Installation facile sur les toilettes existantes")).build());
		translates
				.put(getCode("Built in book shelf on the side of tub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен в книжной полке со стороны ванны"))
						.put(Lang.ITA, getWords(" Mensola incorporata sul lato della vasca"))
						.put(Lang.ESP, getWords(" Construido en el estante de libros en el lado de la tina"))
						.put(Lang.DEU, getWords(" Built in Bücherregal auf der Seite der Wanne"))
						.put(Lang.FRA, getWords(" Étagère à livres intégrée sur le côté de la baignoire")).build());
		translates
				.put(getCode("The optional Bluetooth Audio system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дополнительная аудиосистема Bluetooth"))
						.put(Lang.ITA, getWords(" Il sistema audio Bluetooth opzionale"))
						.put(Lang.ESP, getWords(" El sistema de audio Bluetooth opcional"))
						.put(Lang.DEU, getWords(" Das optionale Bluetooth-Audiosystem"))
						.put(Lang.FRA, getWords(" Le système audio Bluetooth en option")).build());
		translates
				.put(getCode("Exquisite Aquatica trim finishes resist corrosion and tarnishing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изысканная отделка отделки Aquatica противостоит коррозии и потускнению"))
						.put(Lang.ITA, getWords(" Le raffinate rifiniture Aquatica resistono alla corrosione e all'appannamento"))
						.put(Lang.ESP, getWords(" Los acabados exquisitos de Aquatica resisten la corrosión y el deslustre"))
						.put(Lang.DEU, getWords(" Exquisite Aquatica-Zierteile sind korrosions- und anlaufbeständig"))
						.put(Lang.FRA, getWords(" Les garnitures Aquatica exquises résistent à la corrosion et au ternissement")).build());
		translates
				.put(getCode("ø2.5 (ø60 mm) freestanding outdoor shower with ground fixing and water supply system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("ø2,5 (ø60 мм) автономный наружный душ с заземлением и системой водоснабжения"))
						.put(Lang.ITA, getWords(" Doccia da esterno ø 2,5 mm (ø60 mm) con fissaggio a terra e sistema di alimentazione dell'acqua"))
						.put(Lang.ESP, getWords(" Ducha exterior de ø2.5 (ø60 mm) libre con fijación al suelo y sistema de suministro de agua"))
						.put(Lang.DEU, getWords(" ø2.5 (ø60 mm) freistehende Außendusche mit Bodenbefestigung und Wasserversorgungssystem"))
						.put(Lang.FRA, getWords(" Douche d'extérieur autoportante ø2.5 (ø60 mm) avec fixation au sol et système d'alimentation en eau")).build());
		translates
				.put(getCode("Bold form in stylish, Chrome finished solid brass"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Жирная форма в стильной, хромированной готовой латуни"))
						.put(Lang.ITA, getWords(" Forma audace in elegante ottone massiccio cromato"))
						.put(Lang.ESP, getWords(" Audaz forma en elegante, acabado en cromo de latón macizo"))
						.put(Lang.DEU, getWords(" Bold Form in stilvollem, verchromtem massivem Messing"))
						.put(Lang.FRA, getWords(" Forme audacieuse dans le style, laiton massif fini par chrome")).build());
		translates
				.put(getCode("Freestanding generous circular design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Свободно щедрый круговой дизайн"))
						.put(Lang.ITA, getWords(" Design circolare generoso e indipendente"))
						.put(Lang.ESP, getWords(" Diseño circular generoso independiente"))
						.put(Lang.DEU, getWords(" Freistehendes großzügiges kreisförmiges Design"))
						.put(Lang.FRA, getWords(" Conception circulaire généreuse autoportante")).build());
		translates
				.put(getCode("Monochrome LED lights"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Монохромные светодиодные фонари"))
						.put(Lang.ITA, getWords(" Luci a LED monocromatiche"))
						.put(Lang.ESP, getWords(" Luces LED monocromáticas"))
						.put(Lang.DEU, getWords(" Monochrome LED-Leuchten"))
						.put(Lang.FRA, getWords(" Lumières LED monochromes")).build());
		translates
				.put(getCode("Ergonomic design forms to the body’s shape for ultimate comfort"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичный дизайн формирует форму тела для максимального комфорта"))
						.put(Lang.ITA, getWords(" Il design ergonomico si adatta alla forma del corpo per il massimo comfort"))
						.put(Lang.ESP, getWords(" El diseño ergonómico se adapta a la forma del cuerpo para una máxima comodidad"))
						.put(Lang.DEU, getWords(" Ergonomisches Design passt sich der Körperform an und sorgt für ultimativen Komfort"))
						.put(Lang.FRA, getWords(" Le design ergonomique épouse la forme du corps pour un confort ultime")).build());
		translates
				.put(getCode("2 year overall limited heated therapy and air massage warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2 года ограниченной ограниченной терапии с подогревом и воздушным массажем"))
						.put(Lang.ITA, getWords(" 2 anni di terapia riscaldata e garanzia di massaggio ad aria limitata"))
						.put(Lang.ESP, getWords(" 2 años de garantía global limitada de masaje con terapia de aire y calefacción"))
						.put(Lang.DEU, getWords(" 2 Jahre limitierte, beheizte Therapie- und Luftmassagegarantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 2 ans sur le traitement thermique et le massage par air")).build());
		translates
				.put(getCode("Over 20 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более 20 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords(" Oltre 20 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords(" Más de 20 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords(" Über 20 Low-Profile-Luftstrahlen"))
						.put(Lang.FRA, getWords(" Plus de 20 jets d'air à profil bas")).build());
		translates
				.put(getCode("Spa with cartridge filter"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Спа с картриджным фильтром"))
						.put(Lang.ITA, getWords(" Spa con filtro a cartuccia"))
						.put(Lang.ESP, getWords(" Spa con filtro de cartucho"))
						.put(Lang.DEU, getWords(" Spa mit Patronenfilter"))
						.put(Lang.FRA, getWords(" Spa avec filtre à cartouche")).build());
		translates
				.put(getCode("European manufacture"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Европейское производство"))
						.put(Lang.ITA, getWords(" Fabbricazione europea"))
						.put(Lang.ESP, getWords(" Fabricación europea"))
						.put(Lang.DEU, getWords(" Europäische Herstellung"))
						.put(Lang.FRA, getWords(" Fabrication européenne")).build());
		translates
				.put(getCode("Self check"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Самостоятельная проверка"))
						.put(Lang.ITA, getWords(" Autoverifica"))
						.put(Lang.ESP, getWords(" Autochequeo"))
						.put(Lang.DEU, getWords(" Selbstüberprüfung"))
						.put(Lang.FRA, getWords(" Auto contrôle")).build());
		translates
				.put(getCode("Made in Europe of genuine European materials"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано в Европе подлинных европейских материалов"))
						.put(Lang.ITA, getWords(" Made in Europe di autentici materiali europei"))
						.put(Lang.ESP, getWords(" Hecho en Europa de materiales europeos genuinos"))
						.put(Lang.DEU, getWords(" Hergestellt in Europa aus echten europäischen Materialien"))
						.put(Lang.FRA, getWords(" Fabriqué en Europe de véritables matériaux européens")).build());
		translates
				.put(getCode("False ceiling mounted at the recommended height of 50-70cm above the user’s height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подвесной потолок, установленный на рекомендуемой высоте 50-70 см выше высоты пользователя"))
						.put(Lang.ITA, getWords(" Controsoffitto montato all'altezza consigliata di 50-70 cm sopra l'altezza dell'utente"))
						.put(Lang.ESP, getWords(" Falso techo montado a la altura recomendada de 50-70cm sobre la altura del usuario"))
						.put(Lang.DEU, getWords(" Zwischendecke in der empfohlenen Höhe von 50-70 cm über der Benutzerhöhe montiert"))
						.put(Lang.FRA, getWords(" Faux plafond monté à la hauteur recommandée de 50-70cm au-dessus de la hauteur de l'utilisateur")).build());
		translates
				.put(getCode("Made in Europe"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано в Европе"))
						.put(Lang.ITA, getWords(" Made in Europe"))
						.put(Lang.ESP, getWords(" Hecho en Europa"))
						.put(Lang.DEU, getWords(" Hergestellt in Europa"))
						.put(Lang.FRA, getWords(" Fabriqué en Europe")).build());
		translates
				.put(getCode("Weather resistant, polyester belts for seta and back support"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Устойчивые к атмосферным воздействиям, полиэфирные ленты для поддержки сетки и спины"))
						.put(Lang.ITA, getWords(" Cinghie in poliestere resistente agli agenti atmosferici per seta e supporto per la schiena"))
						.put(Lang.ESP, getWords(" Resistente a la intemperie, cinturones de poliéster para seta y respaldo"))
						.put(Lang.DEU, getWords(" Witterungsbeständige Polyestergurte für die Sitz- und Rückenunterstützung"))
						.put(Lang.FRA, getWords(" Ceintures de polyester résistantes aux intempéries pour le support de seta et de dos")).build());
		translates
				.put(getCode("10 Year Limited Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 10 лет"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 10 anni"))
						.put(Lang.ESP, getWords(" Garantía limitada de 10 años"))
						.put(Lang.DEU, getWords(" 10 Jahre beschränkte Garantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 10 ans")).build());
		translates
				.put(getCode("Will be a perfect addition to Celine collection"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Будет отличным дополнением к коллекции Celine"))
						.put(Lang.ITA, getWords(" Sarà un'aggiunta perfetta alla collezione Celine"))
						.put(Lang.ESP, getWords(" Será un complemento perfecto para la colección Celine"))
						.put(Lang.DEU, getWords(" Wird eine perfekte Ergänzung zu Celine Sammlung sein"))
						.put(Lang.FRA, getWords(" Sera un ajout parfait à la collection de Céline")).build());
		translates
				.put(getCode("Constructed of solid AquateX™ composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен из твердого композита AquateX ™"))
						.put(Lang.ITA, getWords(" Realizzato in solido composito AquateX ™"))
						.put(Lang.ESP, getWords(" Construido de compuesto AquateX ™ sólido"))
						.put(Lang.DEU, getWords(" Hergestellt aus festem AquateX ™ Composite"))
						.put(Lang.FRA, getWords(" Construit de composite solide AquateX ™")).build());
		translates
				.put(getCode("Lavatory with integrated basin dimensions: 55 L x 20.75 W x 9 H (in) / 140 L x 53 W x 23 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Умывальник со встроенными размерами бассейна: 55 L x 20,75 W x 9 H (дюйм) / 140 L x 53 Вт x 23 H (см)"))
						.put(Lang.ITA, getWords(" Lavabo con dimensioni lavabo integrato: 55 L x 20,75 W x 9 H (in) / 140 L x 53 W x 23 H (cm)"))
						.put(Lang.ESP, getWords(" Lavabo con dimensiones de lavabo integrados: 55 L x 20.75 W x 9 H (in) / 140 L x 53 W x 23 H (cm)"))
						.put(Lang.DEU, getWords(" Toilette mit integriertem Becken Abmessungen: 55 L x 20,75 W x 9 H (in) / 140 L x 53 B x 23 H (cm)"))
						.put(Lang.FRA, getWords(" Lavabo avec des dimensions de bassin intégré: 55 L x 20,75 W x 9 H (pouces) / 140 L x 53 W x 23 H (cm)")).build());
		translates
				.put(getCode("Made with Lucite Care hygienic acrylic sheets from Lucite International"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано с Lucite Care гигиеническими акриловыми листами от Lucite International"))
						.put(Lang.ITA, getWords(" Fatto con le lastre acriliche igieniche Lucite Care di Lucite International"))
						.put(Lang.ESP, getWords(" Hecho con hojas acrílicas higiénicas Lucite Care de Lucite International"))
						.put(Lang.DEU, getWords(" Hergestellt mit Lucite Care hygienischen Acrylplatten von Lucite International"))
						.put(Lang.FRA, getWords(" Fabriqué avec des feuilles d'acrylique hygiénique Lucite Care de Lucite International")).build());
		translates
				.put(getCode("Tobacco Sherwood wood-grain composite veneer base (1050 mm x 500 mm x 350 mm deep / 41.25 x 19.75 x 13.75 in)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Табак Шервуд из древесно-стружечного композитного шпона (1050 мм х 500 мм х 350 мм в глубину / 41,25 х 19,75 х 13,75 дюйма)"))
						.put(Lang.ITA, getWords(" Base per impiallacciatura in composito legno-graniglia di tabacco Sherwood (1050 mm x 500 mm x 350 mm di profondità / 41,25 x 19,75 x 13,75 pollici)"))
						.put(Lang.ESP, getWords(" Base de chapa de madera compuesta de grano de tabaco Sherwood (1050 mm x 500 mm x 350 mm de profundidad / 41.25 x 19.75 x 13.75 in)"))
						.put(Lang.DEU, getWords(" Tabak Sherwood Holz-Korn-Composite-Furnier-Basis (1050 mm x 500 mm x 350 mm tief / 41,25 x 19,75 x 13,75 Zoll)"))
						.put(Lang.FRA, getWords(" Tabac à base de bois composite Sherwood (1050 mm x 500 mm x 350 mm de profondeur / 41,25 x 19,75 x 13,75 po)")).build());
		translates
				.put(getCode("20 low-profile air jets and 16 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("20 низкопрофильных воздушных струй и 16 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords(" 20 getti d'aria a basso profilo e 16 LED a basso profilo"))
						.put(Lang.ESP, getWords(" 20 chorros de aire de perfil bajo y 16 LED de bajo perfil"))
						.put(Lang.DEU, getWords(" 20 Low-Profile-Luftdüsen und 16 Low-Profile-LEDs"))
						.put(Lang.FRA, getWords(" 20 jets d'air à profil bas et 16 LED à profil bas")).build());
		translates
				.put(getCode("Durable, stain resistant under-surface coating protected by the translucent acrylic top panel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочное, устойчивое к пятнам покрытие под поверхностью, защищенное полупрозрачной акриловой верхней панелью"))
						.put(Lang.ITA, getWords(" Rivestimento duraturo e antimacchia sotto la superficie protetto dal pannello superiore in acrilico traslucido"))
						.put(Lang.ESP, getWords(" Recubrimiento de superficie resistente a las manchas y duradero protegido por el panel superior acrílico translúcido"))
						.put(Lang.DEU, getWords(" Strapazierfähige, schmutzabweisende Unterbodenbeschichtung, geschützt durch die transluzente Acrylglasplatte"))
						.put(Lang.FRA, getWords(" Revêtement sous la surface durable et résistant aux taches, protégé par le panneau supérieur en acrylique translucide")).build());
		translates
				.put(getCode("Removable decorative front apron for easy access to plumbing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Съемный декоративный передний фартук для легкого доступа к сантехнике"))
						.put(Lang.ITA, getWords(" Grembiule anteriore decorativo rimovibile per un facile accesso all'impianto idraulico"))
						.put(Lang.ESP, getWords(" Delantal frontal decorativo extraíble para facilitar el acceso a la plomería"))
						.put(Lang.DEU, getWords(" Abnehmbare dekorative Frontschürze für einfachen Zugang zu den Rohrleitungen"))
						.put(Lang.FRA, getWords(" Tablier avant décoratif amovible pour un accès facile à la plomberie")).build());
		translates
				.put(getCode("Built-in overflow drain and preinstalled pop up waste drain included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенный сливной слив и предустановленный сливной шланг"))
						.put(Lang.ITA, getWords(" Scarico troppopieno incorporato e scarico scarico preinstallato incluso"))
						.put(Lang.ESP, getWords(" Incluye drenaje de desbordamiento integrado y drenaje de residuos preinstalado."))
						.put(Lang.DEU, getWords(" Eingebauter Überlaufabfluss und vorinstallierter Pop-Up-Ablauf inklusive"))
						.put(Lang.FRA, getWords(" Drain de trop-plein intégré et vidage automatique préinstallé inclus")).build());
		translates
				.put(getCode("Male adapter Spg x MIPT for Aquatica bath wastes with Sch40 hub outlet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Мужской адаптер Spg x MIPT для отходов ванны Aquatica с выходом ступицы Sch40"))
						.put(Lang.ITA, getWords(" Adattatore maschio Spg x MIPT per vasca da bagno Aquatica spreca con uscita mozzo Sch40"))
						.put(Lang.ESP, getWords(" Adaptador macho Spg x MIPT para residuos de baño Aquatica con salida de cubo Sch40"))
						.put(Lang.DEU, getWords(" Steckeradapter Spg x MIPT für Aquatica Badeabfälle mit Schovennabenabgang"))
						.put(Lang.FRA, getWords(" Adaptateur mâle Spg x MIPT pour les déchets de bain Aquatica avec sortie de moyeu Sch40")).build());
		translates
				.put(getCode("Maximum hot water temperature: 80 °C"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная температура горячей воды: 80 ° C"))
						.put(Lang.ITA, getWords(" Temperatura massima dell'acqua calda: 80 ° C"))
						.put(Lang.ESP, getWords(" Temperatura máxima del agua caliente: 80 ° C"))
						.put(Lang.DEU, getWords(" Maximale Heißwassertemperatur: 80 ° C"))
						.put(Lang.FRA, getWords(" Température maximale de l'eau chaude: 80 ° C")).build());
		translates
				.put(getCode("n°4 MSPK 34x34 mm"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("n ° 4 MSPK 34x34 мм"))
						.put(Lang.ITA, getWords(" n ° 4 MSPK 34x34 mm"))
						.put(Lang.ESP, getWords(" n ° 4 MSPK 34x34 mm"))
						.put(Lang.DEU, getWords(" Nr. 4 MSPK 34x34 mm"))
						.put(Lang.FRA, getWords(" n ° 4 MSPK 34x34 mm")).build());
		translates
				.put(getCode("Galvanized steel frame with adjustable feet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Оцинкованная стальная рама с регулируемыми ножками"))
						.put(Lang.ITA, getWords(" Struttura in acciaio zincato con piedini regolabili"))
						.put(Lang.ESP, getWords(" Estructura de acero galvanizado con patas ajustables"))
						.put(Lang.DEU, getWords(" Rahmen aus verzinktem Stahl mit verstellbaren Füßen"))
						.put(Lang.FRA, getWords(" Cadre en acier galvanisé avec pieds réglables")).build());
		translates
				.put(getCode("The pipe head features a cap with a filter that helps to prevent any damage to the water turbine"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("На головке трубопровода имеется колпачок с фильтром, который помогает предотвратить любой ущерб для гидротурбины"))
						.put(Lang.ITA, getWords(" La testa del tubo è dotata di un cappuccio con un filtro che aiuta a prevenire eventuali danni alla turbina dell'acqua"))
						.put(Lang.ESP, getWords(" La cabeza de la tubería tiene una tapa con un filtro que ayuda a prevenir cualquier daño a la turbina de agua"))
						.put(Lang.DEU, getWords(" Der Rohrkopf verfügt über eine Kappe mit einem Filter, der Schäden an der Wasserturbine verhindert"))
						.put(Lang.FRA, getWords(" La tête de tuyau comporte un chapeau avec un filtre qui aide à empêcher n'importe quels dommages à la turbine de l'eau")).build());
		translates
				.put(getCode("Striking upscale modern design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удивительный высококлассный современный дизайн"))
						.put(Lang.ITA, getWords(" Striking design moderno di alto livello"))
						.put(Lang.ESP, getWords(" Impresionante diseño moderno exclusivo"))
						.put(Lang.DEU, getWords(" Auffallendes gehobenes modernes Design"))
						.put(Lang.FRA, getWords(" Design moderne haut de gamme")).build());
		translates
				.put(getCode("Cone material: polypropylene and rubber"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Материал конуса: полипропилен и резина"))
						.put(Lang.ITA, getWords(" Materiale del cono: polipropilene e gomma"))
						.put(Lang.ESP, getWords(" Material del cono: polipropileno y caucho"))
						.put(Lang.DEU, getWords(" Konusmaterial: Polypropylen und Gummi"))
						.put(Lang.FRA, getWords(" Matériau du cône: polypropylène et caoutchouc")).build());
		translates
				.put(getCode("2-1/2 Long drain body"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-1 / 2 Длинный сливной корпус"))
						.put(Lang.ITA, getWords(" 2-1 / 2 Corpo di scarico lungo"))
						.put(Lang.ESP, getWords(" Cuerpo de drenaje largo de 2-1 / 2 "))
						.put(Lang.DEU, getWords(" 2-1 / 2 Lange Ablaufkörper"))
						.put(Lang.FRA, getWords(" 2-1 / 2 Corps de drain long")).build());
		translates
				.put(getCode("No more bumping with your back into the overflow handle!"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Больше не натыкайтесь спиной в ручку переполнения!"))
						.put(Lang.ITA, getWords(" Basta sbattere con la schiena nella maniglia del troppo pieno!"))
						.put(Lang.ESP, getWords(" ¡No más golpes con la espalda en el mango de desbordamiento!"))
						.put(Lang.DEU, getWords(" Nicht mehr mit dem Rücken in den Überlauf Griff stoßen!"))
						.put(Lang.FRA, getWords(" Plus besoin de vous cogner le dos dans la poignée de débordement!")).build());
		translates
				.put(getCode("Color is consistent throughout in thickness – not painted on"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет согласован по толщине - не нарисован на"))
						.put(Lang.ITA, getWords(" Il colore è coerente per tutto lo spessore - non dipinto"))
						.put(Lang.ESP, getWords(" El color es uniforme en todo su espesor, no está pintado"))
						.put(Lang.DEU, getWords(" Die Farbe ist gleichmäßig in der Dicke - nicht aufgemalt"))
						.put(Lang.FRA, getWords(" La couleur est constante dans toute l'épaisseur - non peinte sur")).build());
		translates
				.put(getCode("Avaliable in two colors: white, black"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступны в двух цветах: белый, черный"))
						.put(Lang.ITA, getWords(" Disponibile in due colori: bianco, nero"))
						.put(Lang.ESP, getWords(" Disponible en dos colores: blanco, negro"))
						.put(Lang.DEU, getWords(" Erhältlich in zwei Farben: weiß, schwarz"))
						.put(Lang.FRA, getWords(" Disponible en deux couleurs: blanc, noir")).build());
		translates
				.put(getCode("25 Year Limited Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 25 лет"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 25 anni"))
						.put(Lang.ESP, getWords(" Garantía limitada de 25 años"))
						.put(Lang.DEU, getWords(" 25 Jahre beschränkte Garantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 25 ans")).build());
		translates
				.put(getCode("Fully integrated with freestanding bathtub - no external equipment required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полностью интегрирована с отдельно стоящей ванной - нет необходимости в внешнем оборудовании"))
						.put(Lang.ITA, getWords(" Completamente integrato con vasca da bagno indipendente - nessun equipaggiamento esterno richiesto"))
						.put(Lang.ESP, getWords(" Totalmente integrado con bañera independiente: no se requiere equipo externo"))
						.put(Lang.DEU, getWords(" Vollintegriert mit freistehender Badewanne - keine externe Ausrüstung erforderlich"))
						.put(Lang.FRA, getWords(" Entièrement intégré avec baignoire autoportante - aucun équipement externe requis")).build());
		translates
				.put(getCode("5 Year Limited Warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия 5 лет"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 5 anni"))
						.put(Lang.ESP, getWords(" Garantía limitada de 5 años"))
						.put(Lang.DEU, getWords(" 5 Jahre beschränkte Garantie"))
						.put(Lang.FRA, getWords(" Garantie limitée de 5 ans")).build());
		translates
				.put(getCode("Rock solid design and sturdy construction with galvanized steel frame – will not wobble upon use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочная конструкция из камня и прочная конструкция с оцинкованной стальной рамой - не будут качаться при использовании"))
						.put(Lang.ITA, getWords(" Design solido e robusta con telaio in acciaio zincato - non oscillerà al momento dell'uso"))
						.put(Lang.ESP, getWords(" Diseño sólido como una roca y construcción robusta con marco de acero galvanizado: no se tambaleará con el uso"))
						.put(Lang.DEU, getWords(" Rock solid Design und robuste Konstruktion mit verzinktem Stahlrahmen - wackelt nicht bei der Verwendung"))
						.put(Lang.FRA, getWords(" Conception solide et construction robuste avec cadre en acier galvanisé - ne chancèlera pas lors de l'utilisation")).build());
		translates
				.put(getCode("Tested and works perfectly with Aquatica Bath Installer to further simplify freestanding bath installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Протестировано и отлично работает с Aquatica Bath Installer для дальнейшего упрощения установки автономной ванны"))
						.put(Lang.ITA, getWords(" Testato e funziona perfettamente con Aquatica Bath Installer per semplificare ulteriormente l'installazione in vasca da bagno freestanding"))
						.put(Lang.ESP, getWords(" Probado y funciona perfectamente con Aquatica Bath Installer para simplificar aún más la instalación de baño independiente"))
						.put(Lang.DEU, getWords(" Getestet und funktioniert perfekt mit Aquatica Bath Installer, um die freistehende Installation von Badewannen weiter zu vereinfachen"))
						.put(Lang.FRA, getWords(" Testé et fonctionne parfaitement avec Aquatica Bath Installer pour simplifier davantage l'installation de baignoires autoportantes")).build());
		translates
				.put(getCode("Matching Tobacco Sherwood laminate console  - 1630 x 505 x 130 mm / 64.25 x 20 x 5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подходящая ламинатная консоль для табака Sherwood - 1630 x 505 x 130 мм / 64,25 x 20 x 5 дюймов"))
						.put(Lang.ITA, getWords(" Consolle in laminato Sherwood per tabacco coordinato - 1630 x 505 x 130 mm / 64,25 x 20 x 5 pollici"))
						.put(Lang.ESP, getWords(" Cojín laminado Sherwood Matching Tobacco - 1630 x 505 x 130 mm / 64.25 x 20 x 5 in"))
						.put(Lang.DEU, getWords(" Passende Tabak Sherwood Laminat Konsole - 1630 x 505 x 130 mm / 64,25 x 20 x 5 in"))
						.put(Lang.FRA, getWords(" Console en stratifié Sherwood de tabac - 1630 x 505 x 130 mm / 64,25 x 20 x 5 po")).build());
		translates
				.put(getCode("Made from germ-resistant plastic for maximum hygiene"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изготовлен из зародышевого пластика для максимальной гигиены"))
						.put(Lang.ITA, getWords(" Realizzato in plastica resistente ai germi per la massima igiene"))
						.put(Lang.ESP, getWords(" Hecho de plástico resistente a los gérmenes para la máxima higiene"))
						.put(Lang.DEU, getWords(" Aus keimbeständigem Kunststoff für maximale Hygiene"))
						.put(Lang.FRA, getWords(" Fabriqué en plastique résistant aux germes pour une hygiène maximale")).build());
		translates
				.put(getCode("Color will not fade or lose its brilliance over time"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет не будет исчезать или потерять блеск с течением времени"))
						.put(Lang.ITA, getWords(" Il colore non sbiadirà o perderà la sua brillantezza nel tempo"))
						.put(Lang.ESP, getWords(" El color no se desvanecerá o perderá su brillo con el tiempo"))
						.put(Lang.DEU, getWords(" Die Farbe wird nicht verblassen oder ihre Brillanz im Laufe der Zeit verlieren"))
						.put(Lang.FRA, getWords(" La couleur ne s'efface pas et ne perd pas son éclat au fil du temps")).build());
		translates
				.put(getCode("Understated chrome finish, solid brass construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Заднее хромированное покрытие, сплошная латунная конструкция"))
						.put(Lang.ITA, getWords(" Finitura cromata, costruzione in ottone massiccio"))
						.put(Lang.ESP, getWords(" Con acabado de cromo discreto, construcción de latón macizo"))
						.put(Lang.DEU, getWords(" Dezentes Chrom-Finish, massive Messingkonstruktion"))
						.put(Lang.FRA, getWords(" Finition chromée sobre, construction en laiton massif")).build());
		translates
				.put(getCode("Telescopic pipe extends from 14.96” min to 15.75” max"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Телескопическая труба простирается от 14,96 мин до 15,75 макс."))
						.put(Lang.ITA, getWords(" Il tubo telescopico si estende da 14,96 min a 15,75 max"))
						.put(Lang.ESP, getWords(" El tubo telescópico se extiende desde 14.96 min. Hasta 15.75 máx."))
						.put(Lang.DEU, getWords(" Teleskoprohr erstreckt sich von 14,96 min bis 15,75 max"))
						.put(Lang.FRA, getWords(" Le tube télescopique s'étend de 14,96 min à 15,75 max")).build());
		translates
				.put(getCode("Classical design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классический дизайн"))
						.put(Lang.ITA, getWords(" Design classico"))
						.put(Lang.ESP, getWords(" Diseño clásico"))
						.put(Lang.DEU, getWords(" Klassisches Design"))
						.put(Lang.FRA, getWords(" Design classique")).build());
		translates
				.put(getCode("Large 13.4x13.4 (340x340 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 13.4x13.4 (340x340 мм) душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione grande 13.4x13.4 (340x340 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 13.4x13.4 (340x340 mm)"))
						.put(Lang.DEU, getWords(" Großer 13,4 x 13,4 (340 x 340 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 13,4 x 13,4 po (340 x 340 mm)")).build());
		translates
				.put(getCode("3 fully adjustable stainless steel body spray massage jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3 полностью регулируемых струйных распылителя из нержавеющей стали"))
						.put(Lang.ITA, getWords(" 3 getti massaggianti spray per il corpo in acciaio inossidabile completamente regolabili"))
						.put(Lang.ESP, getWords(" 3 jets de masaje en aerosol para cuerpo de acero inoxidable totalmente ajustables"))
						.put(Lang.DEU, getWords(" 3 voll einstellbare Edelstahl Körperspray Massagedüsen"))
						.put(Lang.FRA, getWords(" 3 jets de massage en aérosol entièrement réglables en acier inoxydable")).build());
		translates
				.put(getCode("Clean modern design and Northern European workmanship"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чистый современный дизайн и североевропейское мастерство"))
						.put(Lang.ITA, getWords(" Pulizia del design moderno e lavorazione nordeuropea"))
						.put(Lang.ESP, getWords(" Diseño moderno y limpio y mano de obra del norte de Europa"))
						.put(Lang.DEU, getWords(" Sauberes modernes Design und nordeuropäische Verarbeitung"))
						.put(Lang.FRA, getWords(" Design moderne et propre et fabrication nord-européenne")).build());
		translates
				.put(getCode("10 years warranty on the acrylic surfaces"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("10-летняя гарантия на акриловые поверхности"))
						.put(Lang.ITA, getWords(" 10 anni di garanzia sulle superfici acriliche"))
						.put(Lang.ESP, getWords(" 10 años de garantía en las superficies acrílicas"))
						.put(Lang.DEU, getWords(" 10 Jahre Garantie auf die Acryloberflächen"))
						.put(Lang.FRA, getWords(" 10 ans de garantie sur les surfaces en acrylique")).build());
		translates
				.put(getCode("Bold and contemporary bowl shape"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Жирная и современная форма чаши"))
						.put(Lang.ITA, getWords(" Forma della ciotola audace e contemporanea"))
						.put(Lang.ESP, getWords(" Forma de tazón negrita y contemporánea"))
						.put(Lang.DEU, getWords(" Fett und zeitgenössische Schüsselform"))
						.put(Lang.FRA, getWords(" Forme de cuvette audacieuse et contemporaine")).build());
		translates
				.put(getCode("Square top-mounted shower arm to match your shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Квадратный верхний душ для душа, соответствующий вашей душевой головке"))
						.put(Lang.ITA, getWords(" Braccio doccia quadrato montato in alto per abbinare il tuo soffione"))
						.put(Lang.ESP, getWords(" Brazo de ducha cuadrado montado en la parte superior para combinar con su cabezal de ducha"))
						.put(Lang.DEU, getWords(" Quadratischer Aufsatzarm für die Dusche passend zu Ihrem Duschkopf"))
						.put(Lang.FRA, getWords(" Bras de douche carré supérieur à votre pomme de douche")).build());
		translates
				.put(getCode("Could be installed on certain bath rims or tiled walls"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Может устанавливаться на некоторых оправах для ванных комнат или черепичных стенах"))
						.put(Lang.ITA, getWords(" Potrebbe essere installato su determinati bordi del bagno o pareti piastrellate"))
						.put(Lang.ESP, getWords(" Podría instalarse en ciertas llantas de baño o muros de azulejos"))
						.put(Lang.DEU, getWords(" Kann auf bestimmten Badewannenrändern oder gefliesten Wänden installiert werden"))
						.put(Lang.FRA, getWords(" Peut être installé sur certaines jantes de bain ou murs carrelés")).build());
		translates
				.put(getCode("All plumbing components supplied by major Italian brands"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Все сантехнические компоненты, поставляемые крупными итальянскими брендами"))
						.put(Lang.ITA, getWords(" Tutti i componenti idraulici forniti dai principali marchi italiani"))
						.put(Lang.ESP, getWords(" Todos los componentes de fontanería suministrados por las principales marcas italianas"))
						.put(Lang.DEU, getWords(" Alle Sanitärkomponenten von großen italienischen Marken"))
						.put(Lang.FRA, getWords(" Tous les composants de plomberie fournis par les grandes marques italiennes")).build());
		translates
				.put(getCode("False ceiling recessed installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Установка в потолочный подвесной потолок"))
						.put(Lang.ITA, getWords(" Installazione da incasso a controsoffitto"))
						.put(Lang.ESP, getWords(" Instalación empotrada en falso techo"))
						.put(Lang.DEU, getWords(" Zwischendeckeneinbau"))
						.put(Lang.FRA, getWords(" Installation encastrée au faux plafond")).build());
		translates
				.put(getCode("Pleasant to touch natural Lebanese cedar slab warms your most loved pieces of apparel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Приятно прикоснуться к естественной ливанской кедровой плите, согревает ваши любимые предметы одежды"))
						.put(Lang.ITA, getWords(" Piacevole toccare la lastra di cedro naturale libanese riscalda i tuoi capi d'abbigliamento più amati"))
						.put(Lang.ESP, getWords(" Agradable tocar la losa de cedro libanés natural calienta tus prendas más queridas"))
						.put(Lang.DEU, getWords(" Angenehm zu berühren natürliche libanesische Zedernplatte wärmt Ihre beliebtesten Stücke von Bekleidung"))
						.put(Lang.FRA, getWords(" Agréable pour toucher la dalle de cèdre libanaise naturelle réchauffe vos pièces les plus aimées de l'habillement")).build());
		translates
				.put(getCode("3 jets: rainfall/double cascade/mist spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("3 струи: ливень / двойной каскад / туман спрей"))
						.put(Lang.ITA, getWords(" 3 getti: pioggia / doppia cascata / nebulizzazione"))
						.put(Lang.ESP, getWords(" 3 chorros: lluvia / doble cascada / neblina"))
						.put(Lang.DEU, getWords(" 3 Düsen: Regen / Doppelkaskade / Nebelspray"))
						.put(Lang.FRA, getWords(" 3 jets: pluie / double cascade / brouillard")).build());
		translates
				.put(getCode("19.75” (500 mm) square built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("19,75 (500 мм) встроенная душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione da incasso quadrato da 500 mm (19,75 )"))
						.put(Lang.ESP, getWords(" Cabezales de ducha empotrados cuadrados de 19.75 (500 mm)"))
						.put(Lang.DEU, getWords(" 19.75 (500 mm) quadratischer Einbauduschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée carrée de 19,75 po (500 mm)")).build());
		translates
				.put(getCode("Absolute comfort with 3 included spa headrests"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Абсолютный комфорт с 3 включенными подголовниками"))
						.put(Lang.ITA, getWords(" Comfort assoluto con 3 poggiatesta spa inclusi"))
						.put(Lang.ESP, getWords(" Comodidad absoluta con 3 reposacabezas de spa incluidos"))
						.put(Lang.DEU, getWords(" Absoluter Komfort mit 3 integrierten Spa Kopfstützen"))
						.put(Lang.FRA, getWords(" Confort absolu avec 3 appuie-tête spa inclus")).build());
		translates
				.put(getCode("Quick and easy installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Быстрая и простая установка"))
						.put(Lang.ITA, getWords(" Installazione semplice e veloce"))
						.put(Lang.ESP, getWords(" Instalación rápida y fácil"))
						.put(Lang.DEU, getWords(" Schnelle und einfache Installation"))
						.put(Lang.FRA, getWords(" Installation rapide et facile")).build());
		translates
				.put(getCode("Over 12 low-profile air jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более 12 низкопрофильных воздушных струй"))
						.put(Lang.ITA, getWords(" Oltre 12 getti d'aria a basso profilo"))
						.put(Lang.ESP, getWords(" Más de 12 chorros de aire de perfil bajo"))
						.put(Lang.DEU, getWords(" Über 12 Low-Profile-Luftdüsen"))
						.put(Lang.FRA, getWords(" Plus de 12 jets d'air à profil bas")).build());
		translates
				.put(getCode("Hot water recirculation system with Tranquility heating system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система рециркуляции горячей воды с системой отопления Tranquility"))
						.put(Lang.ITA, getWords(" Sistema di ricircolo dell'acqua calda con sistema di riscaldamento Tranquility"))
						.put(Lang.ESP, getWords(" Sistema de recirculación de agua caliente con sistema de calefacción Tranquility"))
						.put(Lang.DEU, getWords(" Warmwasser-Umwälzsystem mit Tranquility Heizsystem"))
						.put(Lang.FRA, getWords(" Système de recirculation d'eau chaude avec système de chauffage Tranquility")).build());
		translates
				.put(getCode("Metal frame in dark brown mokka, brushed gold or white to match your set"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Металлический каркас в темно-коричневой мокке, матовое золото или белый, чтобы соответствовать вашему набору"))
						.put(Lang.ITA, getWords(" Montatura in metallo in moka marrone scuro, oro spazzolato o bianco per abbinarlo al tuo set"))
						.put(Lang.ESP, getWords(" Marco de metal en mokka marrón oscuro, cepillado dorado o blanco para combinar con su conjunto"))
						.put(Lang.DEU, getWords(" Metallrahmen in dunkelbraunem Mokka, gebürstetem Gold oder Weiß passend zu Ihrem Set"))
						.put(Lang.FRA, getWords(" Structure en métal mokka brun foncé, or brossé ou blanc pour assortir votre ensemble")).build());
		translates
				.put(getCode("Constructed of EcoMarmor™ Lite - a combination of heavy gauge sanitary grade precision acrylic and synthetic stone composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из EcoMarmor ™ Lite - комбинация высокоточного высокоточного акрилового и синтетического каменного композитного материала"))
						.put(Lang.ITA, getWords(" Costruito con EcoMarmor ™ Lite - una combinazione di acrilico di precisione di alta qualità e composito di pietra sintetica"))
						.put(Lang.ESP, getWords(" Construido de EcoMarmor ™ Lite - una combinación de compuesto de piedra sintética y acrílico de precisión de grado sanitario de gran calibre"))
						.put(Lang.DEU, getWords(" Hergestellt aus EcoMarmor ™ Lite - einer Kombination aus hochpräzisem Präzisions-Acryl- und Synthetikstein-Verbundmaterial"))
						.put(Lang.FRA, getWords(" Construit avec EcoMarmor ™ Lite - une combinaison d'acrylique de précision sanitaire de haute qualité et de composite de pierre synthétique")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 2 outlet shut-off valve and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 2 выпускными запорными клапанами и металлической пластиной"))
						.put(Lang.ITA, getWords(" Miscelatore doccia esterno a parete con valvola di intercettazione a 2 uscite e piastra metallica"))
						.put(Lang.ESP, getWords(" Mezclador de ducha de pared con válvula de cierre de 2 salidas y placa de metal"))
						.put(Lang.DEU, getWords(" Wand-Brausebatterie mit 2 Auslauf-Absperrventilen und Metallplatte"))
						.put(Lang.FRA, getWords(" Mitigeur de douche mural avec 2 vannes d'arrêt et plaque métallique")).build());
		translates
				.put(getCode("Feminine cleansing, kids function"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Женская очистка, дети"))
						.put(Lang.ITA, getWords(" Pulizia femminile, funzione dei bambini"))
						.put(Lang.ESP, getWords(" Limpieza femenina, función de niños"))
						.put(Lang.DEU, getWords(" Feminine Reinigung, Kinderfunktion"))
						.put(Lang.FRA, getWords(" Nettoyage féminin, fonction enfants")).build());
		translates
				.put(getCode("High gloss white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высокая глянцевая белая поверхность"))
						.put(Lang.ITA, getWords(" Superficie bianca lucida"))
						.put(Lang.ESP, getWords(" Superficie blanca de alto brillo"))
						.put(Lang.DEU, getWords(" Hochglänzende weiße Oberfläche"))
						.put(Lang.FRA, getWords(" Surface blanche haute brillance")).build());
		translates
				.put(getCode("Designed to coordinate beautifully with a selection of Aquatica's washbasins"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для красивой координации с выбором умывальников Aquatica"))
						.put(Lang.ITA, getWords(" Progettato per coordinarsi magnificamente con una selezione di lavabi Aquatica"))
						.put(Lang.ESP, getWords(" Diseñado para coordinar maravillosamente con una selección de lavabos de Aquatica"))
						.put(Lang.DEU, getWords(" Entwickelt, um sich perfekt mit einer Auswahl von Aquaticas Waschbecken zu koordinieren"))
						.put(Lang.FRA, getWords(" Conçu pour se coordonner magnifiquement avec une sélection de lavabos Aquatica")).build());
		translates
				.put(getCode("Electronic control panel."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Электронная панель управления."))
						.put(Lang.ITA, getWords(" Pannello di controllo elettronico."))
						.put(Lang.ESP, getWords(" Panel de control electrónico"))
						.put(Lang.DEU, getWords(" Elektronisches Bedienfeld"))
						.put(Lang.FRA, getWords(" Panneau de contrôle électronique.")).build());
		translates
				.put(getCode("4 different installations available"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступны 4 различных установки"))
						.put(Lang.ITA, getWords(" 4 diverse installazioni disponibili"))
						.put(Lang.ESP, getWords(" 4 instalaciones diferentes disponibles"))
						.put(Lang.DEU, getWords(" 4 verschiedene Installationen verfügbar"))
						.put(Lang.FRA, getWords(" 4 installations différentes disponibles")).build());
		translates
				.put(getCode("Drain will screw directly into a standard drain shoe with 1 112 NPSM (IPS) thread"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Слив будет ввинчиваться непосредственно в стандартный сливной башмак с резьбой 1 112 NPSM (IPS)"))
						.put(Lang.ITA, getWords(" Lo scarico si avviterà direttamente in una scarpa di scarico standard con filettatura 1 112 NPSM (IPS)"))
						.put(Lang.ESP, getWords(" El desagüe se atornillará directamente en una zapata de drenaje estándar con 1 hilo de 112 NPSM (IPS)"))
						.put(Lang.DEU, getWords(" Der Ablauf wird direkt in einen Standard-Ablaufschuh mit 1 112 NPSM (IPS) Gewinde eingeschraubt"))
						.put(Lang.FRA, getWords(" Le drain vissera directement dans un sabot de vidange standard avec un filetage de 1 112 NPSM (IPS)")).build());
		translates
				.put(getCode("Gentle closing seat and lid"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Нежное закрывающее сиденье и крышка"))
						.put(Lang.ITA, getWords(" Sede e coperchio delicati di chiusura"))
						.put(Lang.ESP, getWords(" Asiento y tapa de cierre suave"))
						.put(Lang.DEU, getWords(" Leicht schließender Sitz und Deckel"))
						.put(Lang.FRA, getWords(" Siège de fermeture en douceur et couvercle")).build());
		translates
				.put(getCode("Crossover cut: 200-3200 hz"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Кроссовер: 200-3200 Гц"))
						.put(Lang.ITA, getWords(" Taglio trasversale: 200-3200 hz"))
						.put(Lang.ESP, getWords(" Corte de cruce: 200-3200 hz"))
						.put(Lang.DEU, getWords(" Crossover Schnitt: 200-3200 Hz"))
						.put(Lang.FRA, getWords(" Coupe transversale: 200-3200 hz")).build());
		translates
				.put(getCode("Designed to be installed in an 8” joist space"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для установки в 8-дюймовом пространстве"))
						.put(Lang.ITA, getWords(" Progettato per essere installato in uno spazio di travetto da 8 "))
						.put(Lang.ESP, getWords(" Diseñado para ser instalado en un espacio de viguetas de 8 "))
						.put(Lang.DEU, getWords(" Entworfen für die Installation in einem 8 Fachwerkraum"))
						.put(Lang.FRA, getWords(" Conçu pour être installé dans un espace de solive de 8 ")).build());
		translates
				.put(getCode("Can be made in black mat, white mat finish upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("«Может быть сделано в черном коврике, белая матовая отделка по запросу"))
						.put(Lang.ITA, getWords(" Può essere realizzato in nero opaco, finitura opaca bianca su richiesta"))
						.put(Lang.ESP, getWords(" Se puede hacer en una estera negra, acabado mate blanco a pedido"))
						.put(Lang.DEU, getWords(" Kann in schwarz matt, weiß matt auf Anfrage hergestellt werden"))
						.put(Lang.FRA, getWords(" Peut être fait en natte noire, finition natte blanche sur demande")).build());
		translates
				.put(getCode("Wooden table-shelf  is not included in the price and can be ordered additionally"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Деревянная столовая полка не включена в цену и может быть заказана дополнительно"))
						.put(Lang.ITA, getWords(" La mensola in legno non è inclusa nel prezzo e può essere ordinata in aggiunta"))
						.put(Lang.ESP, getWords(" El estante de madera no está incluido en el precio y se puede pedir adicionalmente"))
						.put(Lang.DEU, getWords(" Holzregal ist nicht im Preis inbegriffen und kann zusätzlich bestellt werden"))
						.put(Lang.FRA, getWords(" L'étagère en bois n'est pas incluse dans le prix et peut être commandée en supplément")).build());
		translates
				.put(getCode("Frameless wall mirror with its built-in LED strip light – 1800 x 500 mm / 70.75 x 19.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Безрамное зеркало на стене со встроенной светодиодной лентой - 1800 x 500 мм / 70,75 x 19,75 дюйма"))
						.put(Lang.ITA, getWords(" Specchio da parete senza cornice con luce a strisce LED integrata - 1800 x 500 mm / 70,75 x 19,75 in"))
						.put(Lang.ESP, getWords(" Espejo de pared sin marco con luz LED incorporada - 1800 x 500 mm / 70,75 x 19,75 pulgadas"))
						.put(Lang.DEU, getWords(" Rahmenloser Wandspiegel mit eingebauter LED-Leiste - 1800 x 500 mm / 70,75 x 19,75 in"))
						.put(Lang.FRA, getWords(" Miroir mural sans cadre avec sa bande LED intégrée - 1800 x 500 mm / 70.75 x 19.75 in")).build());
		translates
				.put(getCode("Wall-mounted frameless mirror with a smart, understated cube LED lamp – 1550 x 500 mm / 61 x 19.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенное бескаркасное зеркало с умной, заниженной светодиодной лампой куба - 1550 x 500 мм / 61 x 19,75 дюйма"))
						.put(Lang.ITA, getWords(" Specchio a parete senza cornice con una lampada a LED cubo intelligente e sobria - 1550 x 500 mm / 61 x 19,75 pollici"))
						.put(Lang.ESP, getWords(" Espejo sin marco montado en la pared con una elegante lámpara de cubos LED discreta - 1550 x 500 mm / 61 x 19.75 in"))
						.put(Lang.DEU, getWords(" Wandmontierter rahmenloser Spiegel mit einer intelligenten, unaufdringlichen Würfel-LED-Lampe - 1550 x 500 Millimeter / 61 x 19.75 Zoll"))
						.put(Lang.FRA, getWords(" Miroir mural sans cadre avec une lampe LED cube discrète - 1550 x 500 mm / 61 x 19,75 po")).build());
		translates
				.put(getCode("Spring SQ-500-C shower head operates with minimum pressure of 0.5 to 1.5 bar for a full cascade shower that saves water"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Пружинная душевая головка Spring SQ-500-C работает с минимальным давлением от 0,5 до 1,5 бар для полного каскадного душа, который экономит воду"))
						.put(Lang.ITA, getWords(" Il soffione a molla SQ-500-C funziona con una pressione minima di 0,5 - 1,5 bar per una doccia a cascata completa che consente di risparmiare acqua"))
						.put(Lang.ESP, getWords(" El cabezal de ducha Spring SQ-500-C funciona con una presión mínima de 0.5 a 1.5 bar para una ducha de cascada completa que ahorra agua"))
						.put(Lang.DEU, getWords(" Der Duschkopf Spring SQ-500-C arbeitet mit einem Mindestdruck von 0,5 bis 1,5 bar für eine vollständige Kaskadendusche, die Wasser spart"))
						.put(Lang.FRA, getWords(" Le pommeau de douche Spring SQ-500-C fonctionne avec une pression minimale de 0,5 à 1,5 bar pour une douche en cascade complète qui économise l'eau")).build());
		translates
				.put(getCode("Constructed of EcoMarmor Light - A combination of heavy gauge sanitary grade precision acrylic and synthetic stone composite"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из EcoMarmor Light - комбинация сложного высокоточного акрилового и синтетического каменного композитного материала"))
						.put(Lang.ITA, getWords(" Costruito con EcoMarmor Light - Una combinazione di acrilico di precisione di alta qualità e composito di pietra sintetica"))
						.put(Lang.ESP, getWords(" Construido de EcoMarmor Light - Una combinación de compuesto de piedra sintética y acrílica de precisión de grado sanitario de gran calibre"))
						.put(Lang.DEU, getWords(" Hergestellt aus EcoMarmor Light - Eine Kombination aus hochwertigem Acryl und synthetischem Stein"))
						.put(Lang.FRA, getWords(" Construit avec EcoMarmor Light - Une combinaison d'acrylique de précision sanitaire de haute qualité et de composite de pierre synthétique")).build());
		translates
				.put(getCode("Maximum water capacity - 660 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 660 литров"))
						.put(Lang.ITA, getWords(" Capacità massima dell'acqua - 660 litri"))
						.put(Lang.ESP, getWords(" Capacidad máxima de agua - 660 Litros"))
						.put(Lang.DEU, getWords(" Maximale Wasserkapazität - 660 Liter"))
						.put(Lang.FRA, getWords(" Capacité d'eau maximale - 660 Litres")).build());
		translates
				.put(getCode("4-Hole installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Установка с 4 отверстиями"))
						.put(Lang.ITA, getWords(" Installazione a 4 fori"))
						.put(Lang.ESP, getWords(" Instalación de 4 agujeros"))
						.put(Lang.DEU, getWords(" 4-Loch-Installation"))
						.put(Lang.FRA, getWords(" Installation à 4 trous")).build());
		translates
				.put(getCode("Minimum flow rate 12 L/min"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальный расход 12 л / мин"))
						.put(Lang.ITA, getWords(" Portata minima 12 L / min"))
						.put(Lang.ESP, getWords(" Velocidad de flujo mínima 12 L / min"))
						.put(Lang.DEU, getWords(" Mindestdurchflussrate 12 L / min"))
						.put(Lang.FRA, getWords(" Débit minimal 12 L / min")).build());
		translates
				.put(getCode("Maximum water capacity - 1000 Litres"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная емкость воды - 1000 литров"))
						.put(Lang.ITA, getWords(" Capacità massima dell'acqua: 1000 litri"))
						.put(Lang.ESP, getWords(" Capacidad máxima de agua - 1000 Litros"))
						.put(Lang.DEU, getWords(" Maximale Wasserkapazität - 1000 Liter"))
						.put(Lang.FRA, getWords(" Capacité maximale d'eau - 1000 litres")).build());
		translates
				.put(getCode("Concealed overflow fitting allows for comfortable reclining surfaces at both ends"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Скрытый перепускной штуцер позволяет удобные откидные поверхности с обоих концов"))
						.put(Lang.ITA, getWords(" Il montaggio del troppopieno nascosto consente superfici di appoggio reclinabili su entrambe le estremità"))
						.put(Lang.ESP, getWords(" El accesorio de desbordamiento oculto permite superficies reclinables cómodas en ambos extremos"))
						.put(Lang.DEU, getWords(" Die verdeckte Überlaufgarnitur ermöglicht komfortable Liegeflächen an beiden Enden"))
						.put(Lang.FRA, getWords(" Le raccord de trop-plein dissimulé permet des surfaces inclinables confortables aux deux extrémités")).build());
		translates
				.put(getCode("Classic design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классический дизайн"))
						.put(Lang.ITA, getWords(" Design classico"))
						.put(Lang.ESP, getWords(" Diseño clásico"))
						.put(Lang.DEU, getWords(" Klassisches Design"))
						.put(Lang.FRA, getWords(" Design classique")).build());
		translates
				.put(getCode("Elegant, timeless design with roomy interiors"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Элегантный, безвременный дизайн с просторными интерьерами"))
						.put(Lang.ITA, getWords(" Design elegante e senza tempo con interni spaziosi"))
						.put(Lang.ESP, getWords(" Diseño elegante y atemporal con espaciosos interiores"))
						.put(Lang.DEU, getWords(" Elegantes, zeitloses Design mit geräumigen Innenräumen"))
						.put(Lang.FRA, getWords(" Design élégant et intemporel avec des intérieurs spacieux")).build());
		translates
				.put(getCode("Dual function water flow – rainfall, waterfall"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Двойной поток воды - осадки, водопад"))
						.put(Lang.ITA, getWords(" Flusso dell'acqua a doppia funzione - precipitazioni, cascata"))
						.put(Lang.ESP, getWords(" Flujo de agua de doble función: lluvia, cascada"))
						.put(Lang.DEU, getWords(" Wasserdurchfluss mit zwei Funktionen - Niederschlag, Wasserfall"))
						.put(Lang.FRA, getWords(" Flux d'eau à double fonction - précipitations, chute d'eau")).build());
		translates
				.put(getCode("Self-powered system - a built-in microturbine produces electrical power from the water flow inside the shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Самонаводящаяся система - встроенная микротурбина производит электроэнергию от потока воды внутри душевой головки"))
						.put(Lang.ITA, getWords(" Sistema autoalimentato: una microturbina integrata produce energia elettrica dal flusso d'acqua all'interno del soffione"))
						.put(Lang.ESP, getWords(" Sistema autoalimentado: una microturbina incorporada produce energía eléctrica a partir del flujo de agua dentro del cabezal de la ducha"))
						.put(Lang.DEU, getWords(" Self-powered System - eine eingebaute Mikroturbine erzeugt elektrische Energie aus dem Wasserfluss im Duschkopf"))
						.put(Lang.FRA, getWords(" Système auto-alimenté - une microturbine intégrée produit de l'énergie électrique à partir du flux d'eau à l'intérieur de la pomme de douche")).build());
		translates
				.put(getCode("Available with CE/TUV certified Europea/International 220V or UL certified USA/American 110V systems"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно с сертифицированными CE / TUV системами Europea / International 220V или UL, сертифицированными системами США и Америки 110V"))
						.put(Lang.ITA, getWords(" Disponibile con certificazione Europea / TUV Europea / International 220V o UL USA / American 110V"))
						.put(Lang.ESP, getWords(" Disponible con CE / TUV con certificación Europea / Internacional 220V o UL con certificación USA / American 110V"))
						.put(Lang.DEU, getWords(" Erhältlich mit CE / TÜV-zertifizierten Europea / International 220V- oder UL-zertifizierten USA / American 110V-Systemen"))
						.put(Lang.FRA, getWords(" Disponible avec les systèmes certifiés CE / TUV Europea / International 220V ou UL / USA 110V")).build());
		translates
				.put(getCode("EcoMarmor™ Lite  cutting edge light-weight cast stone composite variant of the Gloria bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("EcoMarmor ™ Lite режущая кромка из легкого литого каменного композитного варианта ванны Gloria"))
						.put(Lang.ITA, getWords(" EcoMarmor ™ Lite, la variante in composito di pietra da taglio leggera della vasca Gloria"))
						.put(Lang.ESP, getWords(" Variante compuesta de piedra fundida liviana de vanguardia EcoMarmor ™ Lite de la bañera Gloria"))
						.put(Lang.DEU, getWords(" EcoMarmor ™ Lite moderne Leichtbeton-Verbundkonstruktion der Gloria-Badewanne"))
						.put(Lang.FRA, getWords(" EcoMarmor ™ Lite Variante composite légère en pierre coulée de la baignoire Gloria")).build());
		translates
				.put(getCode("Fitted using  a false ceiling to achieve a strong industrial design flavour"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Устанавливается с использованием подвесного потолка для достижения прочного промышленного дизайна"))
						.put(Lang.ITA, getWords(" Montato utilizzando un controsoffitto per ottenere un forte sapore di design industriale"))
						.put(Lang.ESP, getWords(" Equipado con un falso techo para lograr un fuerte sabor de diseño industrial"))
						.put(Lang.DEU, getWords(" Ausgestattet mit einer Zwischendecke, um einen starken Industriedesign-Geschmack zu erzielen"))
						.put(Lang.FRA, getWords(" Équipé d'un faux plafond pour obtenir une forte saveur de design industriel")).build());
		translates
				.put(getCode("Classic romantically styled freestanding tub – one of a kind architectural masterpiece"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Классическая романтично оформленная автономная ванна - единственный в своем роде архитектурный шедевр"))
						.put(Lang.ITA, getWords(" Classica vasca freestanding in stile romantico - un capolavoro architettonico unico nel suo genere"))
						.put(Lang.ESP, getWords(" Bañera exenta clásica de estilo romántico - obra maestra arquitectónica única"))
						.put(Lang.DEU, getWords(" Klassische, freistehende Badewanne im romantischen Stil - ein einzigartiges architektonisches Meisterstück"))
						.put(Lang.FRA, getWords(" Baignoire autoportante de style romantique classique - un chef-d'œuvre architectural unique")).build());
		translates
				.put(getCode("27.5 x 15 (700 x 380 mm) rectangular built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("27,5 x 15 (700 x 380 мм) прямоугольная встроенная душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione da incasso rettangolare da 27,5 x 15 (700 x 380 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha empotrable rectangular de 27.5 x 15 (700 x 380 mm)"))
						.put(Lang.DEU, getWords(" 27,5 x 15 (700 x 380 mm) rechteckiger Einbauduschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée rectangulaire de 27,5 po x 15 po (700 x 380 mm)")).build());
		translates
				.put(getCode("Fireproof - safe to use for both residential and commercial applications"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Огнеупорный - безопасный для использования как в жилых, так и в коммерческих целях"))
						.put(Lang.ITA, getWords(" A prova di fuoco - sicuro da usare sia per applicazioni residenziali che commerciali"))
						.put(Lang.ESP, getWords(" Incombustible: seguro de usar tanto para aplicaciones residenciales como comerciales"))
						.put(Lang.DEU, getWords(" Feuerfest - sicher für private und gewerbliche Anwendungen"))
						.put(Lang.FRA, getWords(" Ignifuge - sûr à utiliser pour les applications résidentielles et commerciales")).build());
		translates
				.put(getCode("Premium acrylic with high gloss surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум акрил с высокой поверхностью блеска"))
						.put(Lang.ITA, getWords(" Acrilico Premium con superficie lucida"))
						.put(Lang.ESP, getWords(" Acrílico premium con superficie de alto brillo"))
						.put(Lang.DEU, getWords(" Premium Acryl mit hochglänzender Oberfläche"))
						.put(Lang.FRA, getWords(" Acrylique Premium avec surface brillante")).build());
	}

	public static void part2() {
		translates
				.put(getCode("Generous 23.6” (600 mm) square shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Щедрая душевая головка размером 23,6 дюйма (600 мм)"))
						.put(Lang.ITA, getWords(" Generoso soffione quadrato da 23.6 (600 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha cuadrado generoso de 23.6 (600 mm)"))
						.put(Lang.DEU, getWords(" Großzügiger quadratischer Duschkopf mit einem Durchmesser von 600 mm"))
						.put(Lang.FRA, getWords(" Généreuse pomme de douche carrée de 23,6 po (600 mm)")).build());
		translates
				.put(getCode("Precision engineering for superior waterflow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прецизионная техника для превосходного потока воды"))
						.put(Lang.ITA, getWords(" Ingegneria di precisione per un flusso d'acqua superiore"))
						.put(Lang.ESP, getWords(" Ingeniería de precisión para un flujo de agua superior"))
						.put(Lang.DEU, getWords(" Feinmechanik für überlegenen Wasserdurchfluss"))
						.put(Lang.FRA, getWords(" Ingénierie de précision pour un débit d'eau supérieur")).build());
		translates
				.put(getCode("Large 11.8x15.7 (300x400 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая головка 11.8x15.7 (300x400 мм)"))
						.put(Lang.ITA, getWords(" Soffione grande 11.8x15.7 (300x400 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 11.8x15.7 (300x400 mm)"))
						.put(Lang.DEU, getWords(" Große 11,8 x 15,7 (300 x 400 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 11,8 x 15,7 po (300 x 400 mm)")).build());
		translates
				.put(getCode("Intelligent energy saving function"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Интеллектуальная функция энергосбережения"))
						.put(Lang.ITA, getWords(" Funzione di risparmio energetico intelligente"))
						.put(Lang.ESP, getWords(" Función inteligente de ahorro de energía"))
						.put(Lang.DEU, getWords(" Intelligente Energiesparfunktion"))
						.put(Lang.FRA, getWords(" Fonction intelligente d'économie d'énergie")).build());
		translates
				.put(getCode("Minimum of 3 bar water pressure recommended for best performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальное давление воды 3 бар рекомендуется для наилучшей производительности"))
						.put(Lang.ITA, getWords(" È consigliata una pressione dell'acqua minima di 3 bar per le migliori prestazioni"))
						.put(Lang.ESP, getWords(" Se recomienda un mínimo de 3 bares de presión de agua para un mejor rendimiento"))
						.put(Lang.DEU, getWords(" Mindestens 3 bar Wasserdruck empfohlen für beste Leistung"))
						.put(Lang.FRA, getWords(" Une pression d'eau minimale de 3 bars est recommandée pour de meilleures performances")).build());
		translates
				.put(getCode("External thermostatic shower mixer with swivel column pipe, checkable anti-lime overhead shower"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Внешний термостатический смеситель для душа с трубкой с поворотной колонной, регулируемый противоизносный ливень"))
						.put(Lang.ITA, getWords(" Miscelatore termostatico esterno per doccia con colonna girevole, soffione anticalcare controllabile"))
						.put(Lang.ESP, getWords(" Mezclador de ducha termostático externo con tubo de columna giratorio, ducha fija anti-cal"))
						.put(Lang.DEU, getWords(" Externe Thermostat-Brausebatterie mit schwenkbarem Säulenrohr, überprüfbarer Kalk-Kalk-Kopfbrause"))
						.put(Lang.FRA, getWords(" Mitigeur de douche thermostatique externe avec tuyau de colonne pivotant, douche anti-calcaire réglable")).build());
		translates
				.put(getCode("Minimum pressure recommended 0.5 to 1.5 bar"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемое минимальное давление: от 0,5 до 1,5 бар"))
						.put(Lang.ITA, getWords(" Pressione minima consigliata da 0,5 a 1,5 bar"))
						.put(Lang.ESP, getWords(" Presión mínima recomendada de 0.5 a 1.5 bar"))
						.put(Lang.DEU, getWords(" Minimaler Druck empfohlen 0,5 bis 1,5 bar"))
						.put(Lang.FRA, getWords(" Pression minimale recommandée de 0,5 à 1,5 bar")).build());
		translates
				.put(getCode("Adjustable wands position"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Регулируемое положение палочек"))
						.put(Lang.ITA, getWords(" Posizione delle bacchette regolabili"))
						.put(Lang.ESP, getWords(" Posición de varillas ajustables"))
						.put(Lang.DEU, getWords(" Einstellbare Zauberstabposition"))
						.put(Lang.FRA, getWords(" Position des baguettes ajustables")).build());
		translates
				.put(getCode("Deep - almost a 18.5-inch (470 mm) water depth for full-body soaks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Глубокая - почти 18,5-дюймовая (470 мм) глубина воды для полного погружения в тело"))
						.put(Lang.ITA, getWords(" Profondo: quasi una profondità d'acqua di 18,5 pollici (470 mm) per il lavaggio di tutto il corpo"))
						.put(Lang.ESP, getWords(" Profundo: casi una profundidad de agua de 18.5 pulgadas (470 mm) para baños de cuerpo completo"))
						.put(Lang.DEU, getWords(" Tief - fast eine Wassertiefe von 475 mm (18,5 inch) für Ganzkörpertränke"))
						.put(Lang.FRA, getWords(" Profondeur - une profondeur d'eau de près de 47,5 mm (18,5 po) pour les bains de tout le corps")).build());
		translates
				.put(getCode("2 year limited warranty for all electrical components and other parts, except drains"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 2 года для всех электрических компонентов и других деталей, кроме стоков"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 2 anni per tutti i componenti elettrici e le altre parti, ad eccezione degli scarichi"))
						.put(Lang.ESP, getWords(" Garantía limitada de 2 años para todos los componentes eléctricos y otras partes, excepto desagües"))
						.put(Lang.DEU, getWords(" 2 Jahre beschränkte Garantie für alle elektrischen Komponenten und andere Teile, ausgenommen Abflüsse"))
						.put(Lang.FRA, getWords(" Garantie limitée de 2 ans pour tous les composants électriques et autres pièces, sauf les drains")).build());
		translates
				.put(getCode("A hypoallergenic, non-porous and inert surface for easy cleaning and sanitizing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная, непористая и инертная поверхность для легкой очистки и дезинфекции"))
						.put(Lang.ITA, getWords(" Una superficie ipoallergenica, non porosa e inerte per una facile pulizia e sanificazione"))
						.put(Lang.ESP, getWords(" Una superficie hipoalergénica, no porosa e inerte para facilitar la limpieza y la desinfección"))
						.put(Lang.DEU, getWords(" Eine hypoallergene, porenfreie und inerte Oberfläche für einfache Reinigung und Desinfektion"))
						.put(Lang.FRA, getWords(" Une surface hypoallergénique, non poreuse et inerte pour un nettoyage et une désinfection faciles")).build());
		translates
				.put(getCode("45 low-profile air jets and 24 low profile LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("45 низкопрофильных воздушных струй и 24 низкопрофильных светодиодов"))
						.put(Lang.ITA, getWords(" 45 getti d'aria a basso profilo e 24 LED a basso profilo"))
						.put(Lang.ESP, getWords(" 45 chorros de aire de perfil bajo y 24 LED de bajo perfil"))
						.put(Lang.DEU, getWords(" 45 Low-Profile-Luftdüsen und 24 Low-Profile-LEDs"))
						.put(Lang.FRA, getWords(" 45 jets d'air à profil bas et 24 LED à profil bas")).build());
		translates
				.put(getCode("10 year limited warranty on the bathtub shell"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("10-летняя ограниченная гарантия на корпус ванны"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 10 anni sulla scocca della vasca"))
						.put(Lang.ESP, getWords(" Garantía limitada de 10 años en la carcasa de la bañera"))
						.put(Lang.DEU, getWords(" 10 Jahre Garantie auf die Badewannenschale"))
						.put(Lang.FRA, getWords(" Garantie limitée de 10 ans sur la coque de la baignoire")).build());
		translates
				.put(getCode("Does not include faucet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Не включает кран"))
						.put(Lang.ITA, getWords(" Non include il rubinetto"))
						.put(Lang.ESP, getWords(" No incluye el grifo"))
						.put(Lang.DEU, getWords(" Enthält keinen Wasserhahn"))
						.put(Lang.FRA, getWords(" Ne comprend pas le robinet")).build());
		translates
				.put(getCode("Built-in rectangular sink with corner inset"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Встроенная прямоугольная раковина с угловой вставкой"))
						.put(Lang.ITA, getWords(" Lavabo rettangolare integrato con inserto angolare"))
						.put(Lang.ESP, getWords(" Fregadero rectangular incorporado con inserción de esquina"))
						.put(Lang.DEU, getWords(" Eingebaute rechteckige Spüle mit Eckeinsatz"))
						.put(Lang.FRA, getWords(" Évier rectangulaire intégré avec coin encastré")).build());
		translates
				.put(getCode("8” x 4.75” (200 x 120 mm) metal wall plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("8 x 4.75 (200 x 120 мм) металлическая настенная плита"))
						.put(Lang.ITA, getWords(" 8 x 4,75 (200 x 120 mm) piastra a muro in metallo"))
						.put(Lang.ESP, getWords(" Placa de pared de metal de 8 x 4.75 (200 x 120 mm)"))
						.put(Lang.DEU, getWords(" 8 x 4,75 (200 x 120 mm) Metallwandplatte"))
						.put(Lang.FRA, getWords(" Plaque murale en métal de 8 x 4.75 (200 x 120 mm)")).build());
		translates
				.put(getCode("Sleek profile, slimline shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гладкий профиль, тонкая душевая головка"))
						.put(Lang.ITA, getWords(" Profilo sottile, soffione sottile"))
						.put(Lang.ESP, getWords(" Perfil elegante, cabezal de ducha delgado"))
						.put(Lang.DEU, getWords(" Schlankes Profil, schmaler Duschkopf"))
						.put(Lang.FRA, getWords(" Profil élégant, pomme de douche slimline")).build());
		translates
				.put(getCode("Designed for comfortable two-person soaking"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного впитывания двух человек"))
						.put(Lang.ITA, getWords(" Progettato per un comodo ammollo per due persone"))
						.put(Lang.ESP, getWords(" Diseñado para un cómodo remojo de dos personas"))
						.put(Lang.DEU, getWords(" Konzipiert für komfortables Einweichen für zwei Personen"))
						.put(Lang.FRA, getWords(" Conçu pour un trempage confortable à deux personnes")).build());
		translates
				.put(getCode("Connects to plastic or metal piping (adapters not supplied)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подключается к пластиковым или металлическим трубопроводам (адаптеры не поставляются)"))
						.put(Lang.ITA, getWords(" Si collega a tubazioni in plastica o metallo (adattatori non in dotazione)"))
						.put(Lang.ESP, getWords(" Se conecta a tuberías de plástico o metal (adaptadores no incluidos)"))
						.put(Lang.DEU, getWords(" Anschluss an Kunststoff- oder Metallrohrleitungen (Adapter nicht im Lieferumfang enthalten)"))
						.put(Lang.FRA, getWords(" Se connecte à une tuyauterie en plastique ou en métal (adaptateurs non fournis)")).build());
		translates
				.put(getCode("Robust freestanding construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прочная автономная конструкция"))
						.put(Lang.ITA, getWords(" Robusta costruzione indipendente"))
						.put(Lang.ESP, getWords(" Construcción robusta e independiente"))
						.put(Lang.DEU, getWords(" Robuste freistehende Konstruktion"))
						.put(Lang.FRA, getWords(" Construction autoportante robuste")).build());
		translates
				.put(getCode("Mirror 900w x 700h mm / 35.5 x 27.5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Зеркало 900w x 700h мм / 35,5 x 27,5 дюймов"))
						.put(Lang.ITA, getWords(" Specchio 900w x 700h mm / 35,5 x 27,5 pollici"))
						.put(Lang.ESP, getWords(" Espejo 900w x 700h mm / 35.5 x 27.5 in"))
						.put(Lang.DEU, getWords(" Spiegel 900 x 700 mm / 35,5 x 27,5 Zoll"))
						.put(Lang.FRA, getWords(" Miroir 900w x 700h mm / 35.5 x 27.5 po")).build());
		translates
				.put(getCode("Max flow rate of 3.28 GPM (14 LPM) at 5 bar water pressure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 3,28 GPM (14 LPM) при давлении воды 5 бар"))
						.put(Lang.ITA, getWords(" Portata massima di 3,28 GPM (14 LPM) a 5 bar di pressione dell'acqua"))
						.put(Lang.ESP, getWords(" Caudal máximo de 3.28 GPM (14 LPM) a 5 bar de presión de agua"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 3,28 GPM (14 LPM) bei 5 bar Wasserdruck"))
						.put(Lang.FRA, getWords(" Débit maximum de 3.28 GPM (14 LPM) à une pression d'eau de 5 bar")).build());
		translates
				.put(getCode("Hygienic acrylic sheets from Lucite International ensure superior hygiene conditions."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гигиенические акриловые листы от Lucite International обеспечивают превосходные гигиенические условия."))
						.put(Lang.ITA, getWords(" I fogli acrilici igienici di Lucite International garantiscono condizioni igieniche superiori."))
						.put(Lang.ESP, getWords(" Las láminas acrílicas higiénicas de Lucite International garantizan condiciones de higiene superiores."))
						.put(Lang.DEU, getWords(" Hygienische Acrylplatten von Lucite International sorgen für hervorragende hygienische Bedingungen."))
						.put(Lang.FRA, getWords(" Les feuilles acryliques hygiéniques de Lucite International assurent des conditions d'hygiène supérieures.")).build());
		translates
				.put(getCode("Power supply - 12 V"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Электропитание - 12 В"))
						.put(Lang.ITA, getWords(" Alimentazione - 12 V"))
						.put(Lang.ESP, getWords(" Fuente de alimentación - 12 V"))
						.put(Lang.DEU, getWords(" Stromversorgung - 12 V"))
						.put(Lang.FRA, getWords(" Alimentation - 12 V")).build());
		translates
				.put(getCode("High-quality durable construction with teflon cable that will last years"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высококачественная прочная конструкция с тефлоновым кабелем, которая будет длиться годами"))
						.put(Lang.ITA, getWords(" Costruzione durevole di alta qualità con cavo in teflon che durerà anni"))
						.put(Lang.ESP, getWords(" Construcción duradera de alta calidad con cable de teflón que durará años"))
						.put(Lang.DEU, getWords(" Hochwertige langlebige Konstruktion mit Teflon-Kabel, die Jahre dauern wird"))
						.put(Lang.FRA, getWords(" Construction durable de haute qualité avec câble en téflon qui durera des années")).build());
		translates
				.put(getCode("Backlit frameless mirror 700 x 900 mm / 27.5 x 35.5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подсветка без рамки без зеркала 700 x 900 мм / 27,5 x 35,5 дюймов"))
						.put(Lang.ITA, getWords(" Specchio retroilluminato senza cornice 700 x 900 mm / 27,5 x 35,5 pollici"))
						.put(Lang.ESP, getWords(" Espejo retroiluminado sin marco 700 x 900 mm / 27.5 x 35.5 in"))
						.put(Lang.DEU, getWords(" Hintergrundbeleuchteter rahmenloser Spiegel 700 x 900 mm / 27.5 x 35.5 in"))
						.put(Lang.FRA, getWords(" Rétroviseur sans cadre rétro-éclairé 700 x 900 mm / 27.5 x 35.5 po")).build());
		translates
				.put(getCode("Thicker walls for superior heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более толстые стенки для превосходного удержания тепла"))
						.put(Lang.ITA, getWords(" Pareti più spesse per una maggiore ritenzione del calore"))
						.put(Lang.ESP, getWords(" Paredes más gruesas para una retención de calor superior"))
						.put(Lang.DEU, getWords(" Dickere Wände für überlegene Wärmespeicherung"))
						.put(Lang.FRA, getWords(" Murs plus épais pour une rétention supérieure de la chaleur")).build());
		translates
				.put(getCode("Bottom Mounted Air Massage"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Воздушный массаж с нижним контуром"))
						.put(Lang.ITA, getWords(" Massaggio ad aria inferiore montato"))
						.put(Lang.ESP, getWords(" Masaje de aire montado en la parte inferior"))
						.put(Lang.DEU, getWords(" Bodenmontierte Luftmassage"))
						.put(Lang.FRA, getWords(" Massage à air monté par le bas")).build());
		translates
				.put(getCode("Hypoallergenic surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная поверхность"))
						.put(Lang.ITA, getWords(" Superficie ipoallergenica"))
						.put(Lang.ESP, getWords(" Superficie hipoalergénica"))
						.put(Lang.DEU, getWords(" Hypoallergene Oberfläche"))
						.put(Lang.FRA, getWords(" Surface hypoallergénique")).build());
		translates
				.put(getCode("Designed for comfortable five-person soaking"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для комфортного увлажнения на пять человек"))
						.put(Lang.ITA, getWords(" Progettato per un comodo ammollo da cinque persone"))
						.put(Lang.ESP, getWords(" Diseñado para un cómodo remojo de cinco personas"))
						.put(Lang.DEU, getWords(" Konzipiert für komfortables Einweichen für fünf Personen"))
						.put(Lang.FRA, getWords(" Conçu pour le trempage confortable de cinq personnes")).build());
		translates
				.put(getCode("Very deep bathtub- almost 20-inch (500 mm) water depth for full-body soaks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень глубокая ванна - почти 20-дюймовая (500 мм) глубина воды для полного увлажнения"))
						.put(Lang.ITA, getWords(" Vasca molto profonda: profondità d'acqua di quasi 20 pollici (500 mm) per bagni con tutto il corpo"))
						.put(Lang.ESP, getWords(" Bañera muy profunda: casi 20 pulgadas (500 mm) de profundidad de agua para todo el cuerpo."))
						.put(Lang.DEU, getWords(" Sehr tiefe Badewanne - fast 20 Zoll (500 mm) Wassertiefe für Ganzkörper-Einweichungen"))
						.put(Lang.FRA, getWords(" Baignoire très profonde - profondeur d'eau de près de 20 po (500 mm) pour les bains de corps entiers")).build());
		translates
				.put(getCode("Works with iPhone/iPad App - simply and quickly install the app to operate your bath/shower set and set to your needs, from any location!"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Работает с iPhone / iPad App - просто и быстро установите приложение, чтобы управлять вашим набором ванны / душа и соответствовать вашим потребностям, из любого места!"))
						.put(Lang.ITA, getWords(" Funziona con l'app per iPhone / iPad: installa l'app in modo semplice e rapido per utilizzare il set da bagno / doccia e adattarlo alle tue esigenze, da qualsiasi posizione!"))
						.put(Lang.ESP, getWords(" Funciona con la aplicación iPhone / iPad: ¡simplemente y rápidamente instale la aplicación para operar su juego de baño / ducha y ajústelo a sus necesidades, desde cualquier ubicación!"))
						.put(Lang.DEU, getWords(" Funktioniert mit iPhone / iPad App - einfach und schnell installieren Sie die App, um Ihr Bad / Dusche-Set zu bedienen und von jedem Ort aus auf Ihre Bedürfnisse einzustellen!"))
						.put(Lang.FRA, getWords(" Fonctionne avec l'application iPhone / iPad - installez simplement et rapidement l'application pour faire fonctionner votre ensemble bain / douche et régler selon vos besoins, de n'importe quel endroit!")).build());
		translates
				.put(getCode("Intended for corner installations"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для угловых установок"))
						.put(Lang.ITA, getWords(" Destinato alle installazioni angolari"))
						.put(Lang.ESP, getWords(" Destinado a instalaciones de esquina"))
						.put(Lang.DEU, getWords(" Vorgesehen für Eckinstallationen"))
						.put(Lang.FRA, getWords(" Destiné aux installations d'angle")).build());
		translates
				.put(getCode("Blue LED powered lighting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Синее светодиодное питание"))
						.put(Lang.ITA, getWords(" Illuminazione a LED blu"))
						.put(Lang.ESP, getWords(" Iluminación LED azul"))
						.put(Lang.DEU, getWords(" Blaue LED-Beleuchtung"))
						.put(Lang.FRA, getWords(" Éclairage à DEL bleu")).build());
		translates
				.put(getCode("Uses only 48 Gallons of Water - the lowest water consumption in the class"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Использует только 48 галлонов воды - самое низкое потребление воды в классе"))
						.put(Lang.ITA, getWords(" Utilizza solo 48 galloni d'acqua - il consumo d'acqua più basso della classe"))
						.put(Lang.ESP, getWords(" Utiliza solo 48 galones de agua: el consumo de agua más bajo de la clase"))
						.put(Lang.DEU, getWords(" Verwendet nur 48 Gallonen Wasser - der niedrigste Wasserverbrauch in der Klasse"))
						.put(Lang.FRA, getWords(" Utilise seulement 48 gallons d'eau - la plus faible consommation d'eau de la classe")).build());
		translates
				.put(getCode("Shipped complete with all necessary mounting hardware"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поставляется в комплекте со всеми необходимыми монтажными устройствами"))
						.put(Lang.ITA, getWords(" Spedito completo di tutto il necessario hardware di montaggio"))
						.put(Lang.ESP, getWords(" Enviado completo con todo el hardware de montaje necesario"))
						.put(Lang.DEU, getWords(" Wird komplett mit allen notwendigen Montageteilen geliefert"))
						.put(Lang.FRA, getWords(" Expédié complet avec tout le matériel de montage nécessaire")).build());
		translates
				.put(getCode("No external power source required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Не требуется внешний источник питания"))
						.put(Lang.ITA, getWords(" Non è richiesta alcuna fonte di alimentazione esterna"))
						.put(Lang.ESP, getWords(" No se requiere fuente de alimentación externa"))
						.put(Lang.DEU, getWords(" Keine externe Stromquelle erforderlich"))
						.put(Lang.FRA, getWords(" Aucune source d'alimentation externe requise")).build());
		translates
				.put(getCode("Max flow rate of 4.73 GPM (17.9 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 4,73 GPM (17,9 LPM)"))
						.put(Lang.ITA, getWords(" Portata massima di 4,73 GPM (17,9 LPM)"))
						.put(Lang.ESP, getWords(" Caudal máximo de 4.73 GPM (17.9 LPM)"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 4,73 GPM (17,9 LPM)"))
						.put(Lang.FRA, getWords(" Débit maximum de 4,73 GPM (17,9 LPM)")).build());
		translates
				.put(getCode("40 x whirlpool jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("40 x джакузи"))
						.put(Lang.ITA, getWords(" Getti idromassaggio 40 x"))
						.put(Lang.ESP, getWords(" 40 x chorros de hidromasaje"))
						.put(Lang.DEU, getWords(" 40 x Whirlpooldüsen"))
						.put(Lang.FRA, getWords(" 40 jets d'hydromassage")).build());
		translates
				.put(getCode("Aesthetically appealing and minimalistic"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эстетически привлекательный и минималистический"))
						.put(Lang.ITA, getWords(" Esteticamente attraente e minimalista"))
						.put(Lang.ESP, getWords(" Estéticamente atractivo y minimalista"))
						.put(Lang.DEU, getWords(" Ästhetisch ansprechend und minimalistisch"))
						.put(Lang.FRA, getWords(" Esthétiquement attrayant et minimaliste")).build());
		translates
				.put(getCode("18 Gauge Epoxy Coated Metal Deck Flange"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("18 фланец с эпоксидным покрытием с металлической палубой"))
						.put(Lang.ITA, getWords(" Flangia in metallo con rivestimento epossidico calibro 18"))
						.put(Lang.ESP, getWords(" Reborde de cubierta de metal revestido de epoxi calibre 18"))
						.put(Lang.DEU, getWords(" 18 Gauge Epoxy Coated Metall Deck Flansch"))
						.put(Lang.FRA, getWords(" Bride de plancher en métal enduit d'époxy de calibre 18")).build());
		translates
				.put(getCode("All of above solutions feature a wider pipe (1.18in Ø) which guarantees a high flow-rate."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Все вышеперечисленные решения имеют более широкую трубу (1,18 дюйма), что гарантирует высокую скорость потока."))
						.put(Lang.ITA, getWords(" Tutte le soluzioni di cui sopra dispongono di un tubo più largo (1.18in Ø) che garantisce un'alta portata."))
						.put(Lang.ESP, getWords(" Todas las soluciones anteriores cuentan con un tubo más ancho (1.18in Ø) que garantiza un alto índice de flujo."))
						.put(Lang.DEU, getWords(" Alle oben genannten Lösungen verfügen über ein breiteres Rohr (1,18 Zoll Ø), das eine hohe Durchflussrate garantiert."))
						.put(Lang.FRA, getWords(" Toutes les solutions ci-dessus disposent d'un tuyau plus large (1,18 po Ø) qui garantit un débit élevé.")).build());
		translates
				.put(getCode("Rectangular Black Oxide console and integrated basin – 1206 x 505 x 130 mm / 47.5 x 20 x 5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прямоугольная консоль черного оксида и встроенный бассейн - 1206 x 505 x 130 мм / 47,5 x 20 x 5 дюймов"))
						.put(Lang.ITA, getWords(" Consolle rettangolare in ossido nero e vasca integrata - 1206 x 505 x 130 mm / 47,5 x 20 x 5 pollici"))
						.put(Lang.ESP, getWords(" Consola rectangular de óxido negro y lavabo integrado - 1206 x 505 x 130 mm / 47.5 x 20 x 5 in"))
						.put(Lang.DEU, getWords(" Rechteckige Black Oxide Konsole und integriertes Becken - 1206 x 505 x 130 mm / 47,5 x 20 x 5 in"))
						.put(Lang.FRA, getWords(" Console d'oxyde noir rectangulaire et bassin intégré - 1206 x 505 x 130 mm / 47.5 x 20 x 5 po")).build());
		translates
				.put(getCode("100~240V AC/ 50~60 Hz"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("100 ~ 240 В переменного тока / 50 ~ 60 Гц"))
						.put(Lang.ITA, getWords(" 100 ~ 240 V CA / 50 ~ 60 Hz"))
						.put(Lang.ESP, getWords(" 100 ~ 240 V AC / 50 ~ 60 Hz"))
						.put(Lang.DEU, getWords(" 100 ~ 240 V Wechselstrom / 50 ~ 60 Hz"))
						.put(Lang.FRA, getWords(" 100 ~ 240V AC / 50 ~ 60 Hz")).build());
		translates
				.put(getCode("Beautiful solid wood pedestal base"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прекрасная деревянная пьедестал"))
						.put(Lang.ITA, getWords(" Bellissimo piedistallo in legno massello"))
						.put(Lang.ESP, getWords(" Hermosa base de pedestal de madera maciza"))
						.put(Lang.DEU, getWords(" Schöner Sockel aus massivem Holz"))
						.put(Lang.FRA, getWords(" Belle base de socle en bois massif")).build());
		translates
				.put(getCode("Generous 19.75” (500 mm) square shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Щедрая квадратная душевая головка размером 19,75 дюйма (500 мм)"))
						.put(Lang.ITA, getWords(" Generoso soffione quadrato da 19,75 (500 mm)"))
						.put(Lang.ESP, getWords(" Generoso ducha cuadrada de 19.75 (500 mm)"))
						.put(Lang.DEU, getWords(" Großzügiger quadratischer Duschkopf mit einer Breite von 19,75 (500 mm)"))
						.put(Lang.FRA, getWords(" Généreuse pomme de douche carrée de 19,75 po (500 mm)")).build());
		translates
				.put(getCode("Minimum flow rate 3.4 GPM (13 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальный расход 3,4 GPM (13 LPM)"))
						.put(Lang.ITA, getWords(" Portata minima 3,4 GPM (13 LPM)"))
						.put(Lang.ESP, getWords(" Velocidad de flujo mínima 3.4 GPM (13 LPM)"))
						.put(Lang.DEU, getWords(" Mindestdurchflussrate 3,4 GPM (13 LPM)"))
						.put(Lang.FRA, getWords(" Débit minimum 3.4 GPM (13 LPM)")).build());
		translates
				.put(getCode("Fully made and assembled in Europe from genuine parts"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полностью изготовлены и собраны в Европе из оригинальных деталей"))
						.put(Lang.ITA, getWords(" Completamente realizzato e assemblato in Europa da parti originali"))
						.put(Lang.ESP, getWords(" Completamente fabricado y ensamblado en Europa a partir de piezas originales"))
						.put(Lang.DEU, getWords(" Hergestellt und montiert in Europa aus Originalteilen"))
						.put(Lang.FRA, getWords(" Entièrement fabriqué et assemblé en Europe à partir de pièces d'origine")).build());
		translates
				.put(getCode("Made in Italy"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Сделано в Италии"))
						.put(Lang.ITA, getWords(" Fatto in Italia"))
						.put(Lang.ESP, getWords(" Hecho en Italia"))
						.put(Lang.DEU, getWords(" Hergestellt in Italien"))
						.put(Lang.FRA, getWords(" Fabriqué en Italie")).build());
		translates
				.put(getCode("Beautifully crafted chrome plated brass legs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Красиво обработанные хромированные латунные ножки"))
						.put(Lang.ITA, getWords(" Gambe in ottone cromato splendidamente lavorate"))
						.put(Lang.ESP, getWords(" Piernas de bronce cromado bellamente diseñadas"))
						.put(Lang.DEU, getWords(" Wunderschön gearbeitete Beine aus verchromtem Messing"))
						.put(Lang.FRA, getWords(" Jambes en laiton chromé")).build());
		translates
				.put(getCode("Variable spray (5 settings)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Переменный спрей (5 настроек)"))
						.put(Lang.ITA, getWords(" Spray variabile (5 impostazioni)"))
						.put(Lang.ESP, getWords(" Spray variable (5 configuraciones)"))
						.put(Lang.DEU, getWords(" Variabler Spray (5 Einstellungen)"))
						.put(Lang.FRA, getWords(" Pulvérisation variable (5 réglages)")).build());
		translates
				.put(getCode("Premium VelveX™ acrylic with matte white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум VelveX ™ акрил с матовой белой поверхностью"))
						.put(Lang.ITA, getWords(" Acrilico VelveX ™ Premium con superficie bianca opaca"))
						.put(Lang.ESP, getWords(" Acrílico Premium VelveX ™ con superficie blanca mate"))
						.put(Lang.DEU, getWords(" Premium VelveX ™ Acryl mit mattweißer Oberfläche"))
						.put(Lang.FRA, getWords(" Acrylique Premium VelveX ™ avec surface blanche mate")).build());
		translates
				.put(getCode("Drain is not included in the price and can be ordered additionally"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Дренаж не включен в цену и может быть заказан дополнительно"))
						.put(Lang.ITA, getWords(" Lo scarico non è incluso nel prezzo e può essere ordinato in aggiunta"))
						.put(Lang.ESP, getWords(" El desagüe no está incluido en el precio y puede solicitarse adicionalmente"))
						.put(Lang.DEU, getWords(" Abfluss ist nicht im Preis inbegriffen und kann zusätzlich bestellt werden"))
						.put(Lang.FRA, getWords(" Le drain n'est pas inclus dans le prix et peut être commandé en supplément")).build());
		translates
				.put(getCode("40 x air massage jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("40 воздушных струй воздуха"))
						.put(Lang.ITA, getWords(" 40 x getti d'aria"))
						.put(Lang.ESP, getWords(" 40 x jets de masaje de aire"))
						.put(Lang.DEU, getWords(" 40 x Luftmassagedüsen"))
						.put(Lang.FRA, getWords(" 40 jets de massage à air")).build());
		translates
				.put(getCode("No callbacks; eliminates alignment problems and leaks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Нет обратных вызовов;  устраняет проблемы выравнивания и утечки"))
						.put(Lang.ITA, getWords(" Nessun callback;  elimina problemi di allineamento e perdite"))
						.put(Lang.ESP, getWords(" Sin devoluciones de llamada;  elimina problemas de alineación y fugas"))
						.put(Lang.DEU, getWords(" Keine Rückrufe;  beseitigt Ausrichtungsprobleme und Lecks"))
						.put(Lang.FRA, getWords(" Pas de callbacks  élimine les problèmes d'alignement et les fuites")).build());
		translates
				.put(getCode("Large 15.7x19.7 (400x500 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая 15,7x19,7 (400x500 мм) душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione grande 15.7x19.7 (400x500 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 15.7x19.7 (400x500 mm)"))
						.put(Lang.DEU, getWords(" Großer 15,7x19,7 (400x500 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 15,7 x 19,7 po (400 x 500 mm)")).build());
		translates
				.put(getCode("Ceiling Mounted Small Shower Arm is included. It is compatible with similar Aquatica shower arm model"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("В комплект входит небольшая душевая лейка с потолочным креплением.  Он совместим с аналогичной моделью Aquatica для душа"))
						.put(Lang.ITA, getWords(" Braccio doccia a soffitto piccolo incluso.  È compatibile con un modello di braccio doccia Aquatica simile"))
						.put(Lang.ESP, getWords(" Se incluye el brazo de ducha pequeño montado en el techo.  Es compatible con el modelo de brazo de ducha Aquatica similar"))
						.put(Lang.DEU, getWords(" Deckenmontierter kleiner Duscharm ist im Lieferumfang enthalten.  Es ist kompatibel mit ähnlichen Aquatica Duscharm Modell"))
						.put(Lang.FRA, getWords(" Le petit bras de douche fixé au plafond est inclus.  Il est compatible avec un modèle de bras de douche Aquatica similaire")).build());
		translates
				.put(getCode("Matt white lacquer sides and countertop"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Матовая белая лаковая сторона и столешница"))
						.put(Lang.ITA, getWords(" Lati e controsoffitto in laccato bianco opaco"))
						.put(Lang.ESP, getWords(" Laca blanca mate y encimera"))
						.put(Lang.DEU, getWords(" Matt weiße Lackseiten und Arbeitsplatte"))
						.put(Lang.FRA, getWords(" Côtés en laque blanc mat et comptoir")).build());
		translates
				.put(getCode("Honey Sherwood veneer offset two drawer cabinet – 1400 x 500 x 250 mm / 55 x 19.75 x 9.75 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Медный шервудский шпон смещен на два ящика - 1400 x 500 x 250 мм / 55 x 19,75 x 9,75 дюйма"))
						.put(Lang.ITA, getWords(" Impiallacciatura in legno a due cassetti sfoderabile Honey Sherwood - 1400 x 500 x 250 mm / 55 x 19,75 x 9,75 pollici"))
						.put(Lang.ESP, getWords(" Chapa de madera Honey Sherwood compensada con dos gabinetes de cajones: 1400 x 500 x 250 mm / 55 x 19,75 x 9,75 in"))
						.put(Lang.DEU, getWords(" Honey Sherwood Furnier Offset zwei Schublade Schrank - 1400 x 500 x 250 mm / 55 x 19,75 x 9,75 in"))
						.put(Lang.FRA, getWords(" Meuble à tiroirs Honey Sherwood avec placage à deux tiroirs - 1400 x 500 x 250 mm / 55 x 19.75 x 9.75 po")).build());
		translates
				.put(getCode("Matching chrome-plated hand shower, outlet cover, hose and bracket"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствующий хромированный ручной душ, выпускная крышка, шланг и кронштейн"))
						.put(Lang.ITA, getWords(" Doccetta cromata, copertura d'uscita, tubo flessibile e supporto abbinati"))
						.put(Lang.ESP, getWords(" Juego de ducha de mano cromada, tapa de salida, manguera y soporte"))
						.put(Lang.DEU, getWords(" Passende verchromte Handbrause, Auslaufabdeckung, Schlauch und Halterung"))
						.put(Lang.FRA, getWords(" Douchette à main chromée assortie, couvercle de sortie, tuyau et support")).build());
		translates
				.put(getCode("Independently Operating Water heating with temperature control up to 40C (104F)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Независимо работающий нагрев воды с контролем температуры до 40 ° C (104F)"))
						.put(Lang.ITA, getWords(" Funzionamento autonomo del riscaldamento dell'acqua con controllo della temperatura fino a 40 ° C (104 ° F)"))
						.put(Lang.ESP, getWords(" Calentamiento de agua de funcionamiento independiente con control de temperatura de hasta 40 ° C (104 ° F)"))
						.put(Lang.DEU, getWords(" Unabhängig Betrieb Wasserheizung mit Temperaturregelung bis zu 40C (104F)"))
						.put(Lang.FRA, getWords(" Chauffage autonome de l'eau avec contrôle de la température jusqu'à 40C (104F)")).build());
		translates
				.put(getCode("Independently Operating Water heating with temperature control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Независимо работающий нагрев воды с контролем температуры"))
						.put(Lang.ITA, getWords(" Riscaldamento autonomo dell'acqua funzionante con controllo della temperatura"))
						.put(Lang.ESP, getWords(" Calefacción de agua de funcionamiento independiente con control de temperatura"))
						.put(Lang.DEU, getWords(" Unabhängig Betrieb Wasserheizung mit Temperaturregelung"))
						.put(Lang.FRA, getWords(" Fonctionnement indépendant du chauffage de l'eau avec contrôle de la température")).build());
		translates
				.put(getCode("Very comfortable, premium design corner bathtub that takes only 55” corner space"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень удобная, угловая ванна с премиальным дизайном, которая занимает всего 55 дюймов"))
						.put(Lang.ITA, getWords(" Vasca da bagno angolare di design molto confortevole che occupa solo 55 di spazio angolare"))
						.put(Lang.ESP, getWords(" Bañera de esquina de diseño premium muy cómoda que ocupa solo 55 de espacio en la esquina"))
						.put(Lang.DEU, getWords(" Sehr komfortable Premium-Eckbadewanne, die nur 55 Platz in der Ecke einnimmt"))
						.put(Lang.FRA, getWords(" Très confortable, baignoire d'angle de conception premium qui ne prend que 55 coin")).build());
		translates
				.put(getCode("Wide and useful side rims for handy accessories like soap, shampoo, candles and even drinks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Широкие и полезные боковые диски для удобных аксессуаров, таких как мыло, шампунь, свечи и даже напитки"))
						.put(Lang.ITA, getWords(" Cerchi laterali ampi e utili per accessori utili come sapone, shampoo, candele e persino bevande"))
						.put(Lang.ESP, getWords(" Llantas laterales anchas y útiles para accesorios prácticos como jabón, champú, velas e incluso bebidas"))
						.put(Lang.DEU, getWords(" Breite und nützliche Seitenränder für handliches Zubehör wie Seife, Shampoo, Kerzen und sogar Getränke"))
						.put(Lang.FRA, getWords(" Jantes latérales larges et utiles pour des accessoires pratiques comme du savon, du shampoing, des bougies et même des boissons")).build());
		translates
				.put(getCode("Hot water recirculation system (just like the traditional Japanese oidaki) with Tranquility heating system and ozone disinfection system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система рециркуляции горячей воды (как и традиционные японские ойдаки) с системой отопления Tranquility и системой дезинфекции озона"))
						.put(Lang.ITA, getWords(" Sistema di ricircolo dell'acqua calda (come il tradizionale oidaki giapponese) con sistema di riscaldamento Tranquility e sistema di disinfezione dell'ozono"))
						.put(Lang.ESP, getWords(" Sistema de recirculación de agua caliente (al igual que el oidaki tradicional japonés) con sistema de calefacción Tranquility y sistema de desinfección de ozono"))
						.put(Lang.DEU, getWords(" Heißwasser-Umwälzsystem (wie das traditionelle japanische Oidaki) mit Tranquility-Heizsystem und Ozon-Desinfektionssystem"))
						.put(Lang.FRA, getWords(" Système de recirculation d'eau chaude (comme l'oïdaki traditionnel japonais) avec système de chauffage Tranquility et système de désinfection à l'ozone")).build());
		translates
				.put(getCode("Underwater ?hromotherapy lighting with various lighting shows"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Подводное освещение с хромотерапией с различными освещенными шоу"))
						.put(Lang.ITA, getWords(" Illuminazione subacquea per la cromoterapia con vari programmi di illuminazione"))
						.put(Lang.ESP, getWords(" Iluminación subacuática de hromoterapia con varios espectáculos de iluminación"))
						.put(Lang.DEU, getWords(" Unterwassertherapie mit verschiedenen Lichtshows"))
						.put(Lang.FRA, getWords(" Éclairage sous-marin? Hromotherapy avec divers spectacles d'éclairage")).build());
		translates
				.put(getCode("Color is consistent throughout in thickness – will not fade or lose its brilliance over time"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет согласован по всей толщине - не будет исчезать или потерять блеск во времени"))
						.put(Lang.ITA, getWords(" Il colore è costante per tutto lo spessore - non sbiadirà o perderà la sua brillantezza nel tempo"))
						.put(Lang.ESP, getWords(" El color es consistente en todo su espesor: no se desvanecerá ni perderá su brillo con el tiempo"))
						.put(Lang.DEU, getWords(" Die Farbe ist gleichmäßig in der Dicke - verblassen nicht oder verlieren ihre Brillanz im Laufe der Zeit"))
						.put(Lang.FRA, getWords(" La couleur est uniforme dans toute l'épaisseur - ne s'efface pas et ne perd pas son éclat au fil du temps")).build());
		translates
				.put(getCode("Dual action UV/O3 water sanitization system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система обеззараживания воды двойного действия UV / O3"))
						.put(Lang.ITA, getWords(" Sistema di sanificazione dell'acqua UV / O3 a doppia azione"))
						.put(Lang.ESP, getWords(" Sistema de desinfección de agua de doble acción UV / O3"))
						.put(Lang.DEU, getWords(" Dual-Action-UV / O3-Wasserdesinfektionssystem"))
						.put(Lang.FRA, getWords(" Système d'assainissement de l'eau à double action UV / O3")).build());
		translates
				.put(getCode("ø4 (ø100 mm) freestanding outdoor shower with ground fixing and water supply system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("ø4 (ø100 мм) автономный наружный душ с заземлением и системой водоснабжения"))
						.put(Lang.ITA, getWords(" Doccia esterna da terra ø4 (ø100 mm) con fissaggio a terra e sistema di alimentazione dell'acqua"))
						.put(Lang.ESP, getWords(" Ducha exterior de ø4 (ø100 mm) libre con fijación al suelo y sistema de suministro de agua"))
						.put(Lang.DEU, getWords(" ø4 (ø100 mm) freistehende Außendusche mit Bodenbefestigung und Wasserversorgungssystem"))
						.put(Lang.FRA, getWords(" Douche d'extérieur autoportante ø4 (ø100 mm) avec fixation au sol et système d'alimentation en eau")).build());
		translates
				.put(getCode("Rectangular frameless wall mirror – 750 x 700 mm / 29.5 x 27.5 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прямоугольное бескаркасное настенное зеркало - 750 x 700 мм / 29,5 x 27,5 дюймов"))
						.put(Lang.ITA, getWords(" Specchio rettangolare a parete senza cornice - 750 x 700 mm / 29,5 x 27,5 in"))
						.put(Lang.ESP, getWords(" Espejo de pared rectangular sin marco - 750 x 700 mm / 29.5 x 27.5 in"))
						.put(Lang.DEU, getWords(" Rechteckiger rahmenloser Wandspiegel - 750 x 700 mm / 29,5 x 27,5 in"))
						.put(Lang.FRA, getWords(" Miroir mural sans cadre rectangulaire - 750 x 700 mm / 29,5 x 27,5 po")).build());
		translates
				.put(getCode("Enhanced hygiene conditions compared to most competing acrylic bathtubs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Улучшенные гигиенические условия по сравнению с большинством конкурирующих акриловых ванн"))
						.put(Lang.ITA, getWords(" Migliori condizioni igieniche rispetto alla maggior parte delle vasche acriliche concorrenti"))
						.put(Lang.ESP, getWords(" Mejora de las condiciones de higiene en comparación con la mayoría de las bañeras de acrílico de la competencia"))
						.put(Lang.DEU, getWords(" Verbesserte Hygienebedingungen im Vergleich zu den meisten konkurrierenden Acrylbadewannen"))
						.put(Lang.FRA, getWords(" Conditions d'hygiène améliorées par rapport à la plupart des baignoires acryliques concurrentes")).build());
		translates
				.put(getCode("Easy to operate and maintain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Простота в эксплуатации и обслуживании"))
						.put(Lang.ITA, getWords(" Facile da usare e da mantenere"))
						.put(Lang.ESP, getWords(" Fácil de operar y mantener"))
						.put(Lang.DEU, getWords(" Einfach zu bedienen und zu warten"))
						.put(Lang.FRA, getWords(" Facile à utiliser et à entretenir")).build());
		translates
				.put(getCode("Fully integrated with bathtub - no external equipment required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Полностью интегрирована с ванной - нет необходимости в внешнем оборудовании"))
						.put(Lang.ITA, getWords(" Completamente integrato con vasca da bagno - non è richiesto alcun equipaggiamento esterno"))
						.put(Lang.ESP, getWords(" Totalmente integrado con la bañera: no se requiere equipo externo"))
						.put(Lang.DEU, getWords(" Komplett mit Badewanne integriert - keine externe Ausrüstung erforderlich"))
						.put(Lang.FRA, getWords(" Entièrement intégré avec baignoire - aucun équipement externe requis")).build());
		translates
				.put(getCode("Very clean lines and smart chrome finish fits into any shower environment"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень чистые линии и элегантная хромированная отделка вписываются в любую душевую среду"))
						.put(Lang.ITA, getWords(" Linee molto pulite e finiture cromate intelligenti si adattano a qualsiasi ambiente doccia"))
						.put(Lang.ESP, getWords(" Las líneas muy limpias y el acabado cromado inteligente se adaptan a cualquier entorno de ducha"))
						.put(Lang.DEU, getWords(" Sehr klare Linien und cleveres Chrom-Finish passen in jede Duschumgebung"))
						.put(Lang.FRA, getWords(" Les lignes très propres et la finition chromée intelligente s'intègrent dans n'importe quel environnement de douche")).build());
		translates
				.put(getCode("Hypoallergenic white surface"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная белая поверхность"))
						.put(Lang.ITA, getWords(" Superficie bianca ipoallergenica"))
						.put(Lang.ESP, getWords(" Superficie blanca hipoalergénica"))
						.put(Lang.DEU, getWords(" Hypoallergene weiße Oberfläche"))
						.put(Lang.FRA, getWords(" Surface blanche hypoallergénique")).build());
		translates
				.put(getCode("FOLLOW-ME System"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Система FOLLOW-ME"))
						.put(Lang.ITA, getWords(" Sistema FOLLOW-ME"))
						.put(Lang.ESP, getWords(" Sistema FOLLOW-ME"))
						.put(Lang.DEU, getWords(" FOLLOW-ME-System"))
						.put(Lang.FRA, getWords(" Système FOLLOW-ME")).build());
		translates
				.put(getCode("18 x whirlpool jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("18 x джакузи"))
						.put(Lang.ITA, getWords(" 18 getti idromassaggio"))
						.put(Lang.ESP, getWords(" 18 x jets de hidromasaje"))
						.put(Lang.DEU, getWords(" 18 x Whirlpooldüsen"))
						.put(Lang.FRA, getWords(" 18 jets d'hydromassage")).build());
		translates
				.put(getCode("Designed for one-person bathing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для купания одного человека"))
						.put(Lang.ITA, getWords(" Progettato per la balneazione di una persona"))
						.put(Lang.ESP, getWords(" Diseñado para el baño de una persona"))
						.put(Lang.DEU, getWords(" Entworfen für das Ein-Personen-Baden"))
						.put(Lang.FRA, getWords(" Conçu pour la baignade d'une personne")).build());
		translates
				.put(getCode("25 year limited warranty on the bathtub shell"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("25-летняя ограниченная гарантия на корпус ванны"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 25 anni sul guscio della vasca"))
						.put(Lang.ESP, getWords(" Garantía limitada de 25 años en la carcasa de la bañera"))
						.put(Lang.DEU, getWords(" 25 Jahre Garantie auf die Badewannenschale"))
						.put(Lang.FRA, getWords(" Garantie limitée de 25 ans sur la coque de la baignoire")).build());
		translates
				.put(getCode("Arm length 12 (31 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рукава 12 (31 см)"))
						.put(Lang.ITA, getWords(" Lunghezza braccio 12 (31 cm)"))
						.put(Lang.ESP, getWords(" Longitud del brazo 12 (31 cm)"))
						.put(Lang.DEU, getWords(" Armlänge 12 (31 cm)"))
						.put(Lang.FRA, getWords(" Longueur de bras 12 (31 cm)")).build());
		translates
				.put(getCode("2 year limited warranty for Wellness System components"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия на 2 года для компонентов системы Wellness"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 2 anni per i componenti del sistema Wellness"))
						.put(Lang.ESP, getWords(" Garantía limitada de 2 años para los componentes del sistema de bienestar"))
						.put(Lang.DEU, getWords(" 2 Jahre eingeschränkte Garantie für Wellness System-Komponenten"))
						.put(Lang.FRA, getWords(" Garantie limitée de 2 ans pour les composants du système Wellness")).build());
		translates
				.put(getCode("Recommended 3 bar water pressure for best performance"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемое давление воды 3 бар для лучшей производительности"))
						.put(Lang.ITA, getWords(" Pressione dell'acqua consigliata di 3 bar per prestazioni ottimali"))
						.put(Lang.ESP, getWords(" Presión de agua recomendada de 3 bares para un mejor rendimiento"))
						.put(Lang.DEU, getWords(" Empfohlener Wasserdruck von 3 bar für beste Leistung"))
						.put(Lang.FRA, getWords(" Pression d'eau recommandée de 3 bars pour de meilleures performances")).build());
		translates
				.put(getCode("Available for Europe/International 220V and/or USA/Americas 110V"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступно для Европы / Международного 220V и / или США / Северной и Южной Америки 110V"))
						.put(Lang.ITA, getWords(" Disponibile per Europa / International 220V e / o USA / Americas 110V"))
						.put(Lang.ESP, getWords(" Disponible para Europa / Internacional 220V y / o EE. UU. / Américas 110V"))
						.put(Lang.DEU, getWords(" Verfügbar für Europa / International 220V und / oder USA / Americas 110V"))
						.put(Lang.FRA, getWords(" Disponible pour l'Europe / International 220V et / ou les Etats-Unis / Amériques 110V")).build());
		translates
				.put(getCode("Library bathtub with built in book shelf – 4 different installations available"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Библиотечная ванна со встроенной книжной полкой - доступно 4 различных установки"))
						.put(Lang.ITA, getWords(" Vasca da bagno libreria con libreria integrata - 4 diverse installazioni disponibili"))
						.put(Lang.ESP, getWords(" Bañera de biblioteca con estante para libros incorporado - 4 instalaciones diferentes disponibles"))
						.put(Lang.DEU, getWords(" Bibliotheksbadewanne mit eingebautem Bücherregal - 4 verschiedene Installationen verfügbar"))
						.put(Lang.FRA, getWords(" Baignoire bibliothèque avec étagère intégrée - 4 différentes installations disponibles")).build());
		translates
				.put(getCode("Two large drawers with push&pull soft closing mechanism"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Два больших выдвижных ящика с мягким закрывающим механизмом push & pull"))
						.put(Lang.ITA, getWords(" Due grandi cassetti con meccanismo di chiusura soft push & pull"))
						.put(Lang.ESP, getWords(" Dos cajones grandes con mecanismo de cierre suave push & pull"))
						.put(Lang.DEU, getWords(" Zwei große Schubladen mit Push & Pull-Soft-Schließmechanismus"))
						.put(Lang.FRA, getWords(" Deux grands tiroirs avec mécanisme de fermeture en douceur")).build());
		translates
				.put(getCode("1-1/2 to 11-1/2 NPSM Thread"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1-1 / 2 до 11-1 / 2 NPSM Thread"))
						.put(Lang.ITA, getWords(" Filettatura NPSM da 1-1 / 2 a 11-1 / 2"))
						.put(Lang.ESP, getWords(" Rosca NPSM de 1-1 / 2 a 11-1 / 2"))
						.put(Lang.DEU, getWords(" 1-1 / 2 bis 11-1 / 2 NPSM Gewinde"))
						.put(Lang.FRA, getWords(" Filetage NPSM 1-1 / 2 à 11-1 / 2")).build());
		translates
				.put(getCode("Arm length 14 (35 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Длина рычага 14 (35 см)"))
						.put(Lang.ITA, getWords(" Lunghezza del braccio 14 (35 cm)"))
						.put(Lang.ESP, getWords(" Longitud del brazo 14 (35 cm)"))
						.put(Lang.DEU, getWords(" Armlänge 14 (35 cm)"))
						.put(Lang.FRA, getWords(" Longueur de bras 14 (35 cm)")).build());
		translates
				.put(getCode("Double skinned for added heat retention"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Двойная кожа для дополнительного удержания тепла"))
						.put(Lang.ITA, getWords(" Doppia pelle per maggiore ritenzione di calore"))
						.put(Lang.ESP, getWords(" Doble piel para mayor retención de calor"))
						.put(Lang.DEU, getWords(" Doppelhäutig für zusätzliche Wärmespeicherung"))
						.put(Lang.FRA, getWords(" Double peau pour une rétention de chaleur supplémentaire")).build());
		translates
				.put(getCode("Very deep bathing experience"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень глубокий опыт купания"))
						.put(Lang.ITA, getWords(" Esperienza di bagno molto profonda"))
						.put(Lang.ESP, getWords(" Experiencia de baño muy profunda"))
						.put(Lang.DEU, getWords(" Sehr tiefe Badeerfahrung"))
						.put(Lang.FRA, getWords(" Expérience de bain très profonde")).build());
		translates
				.put(getCode("Solid brass finished in chrome."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Твердая латунь из хрома."))
						.put(Lang.ITA, getWords(" Ottone massiccio rifinito in cromo."))
						.put(Lang.ESP, getWords(" Latón macizo acabado en cromo."))
						.put(Lang.DEU, getWords(" Massives Messing, verchromt."))
						.put(Lang.FRA, getWords(" Laiton massif fini en chrome.")).build());
		translates
				.put(getCode("Constructed of solid AquateX™ Matte"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построен из твердого AquateX ™ Matte"))
						.put(Lang.ITA, getWords(" Realizzato in solido AquateX ™ Matte"))
						.put(Lang.ESP, getWords(" Construido de sólido AquateX ™ mate"))
						.put(Lang.DEU, getWords(" Hergestellt aus solidem AquateX ™ Matte"))
						.put(Lang.FRA, getWords(" Construit de solide AquateX ™ Matte")).build());
		translates
				.put(getCode("Can only be installed on select Aquatica bathtubs - See Compatibility Sheet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Может устанавливаться только на ванну Aquatica - см. Лист совместимости"))
						.put(Lang.ITA, getWords(" Può essere installato solo su vasche da bagno Aquatica selezionate - Consultare la scheda di compatibilità"))
						.put(Lang.ESP, getWords(" Solo se puede instalar en ciertas bañeras Aquatica - Ver la Hoja de Compatibilidad"))
						.put(Lang.DEU, getWords(" Kann nur bei ausgewählten Aquatica Badewannen installiert werden - Siehe Kompatibilitätsseite"))
						.put(Lang.FRA, getWords(" Ne peut être installé que sur certaines baignoires Aquatica - Voir la fiche de compatibilité")).build());
		translates
				.put(getCode("2-year overall system warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя общая системная гарантия"))
						.put(Lang.ITA, getWords(" Garanzia di sistema complessiva di 2 anni"))
						.put(Lang.ESP, getWords(" 2 años de garantía total del sistema"))
						.put(Lang.DEU, getWords(" 2 Jahre Gesamtsystemgarantie"))
						.put(Lang.FRA, getWords(" Garantie globale du système de 2 ans")).build());
		translates
				.put(getCode("Colour Sense - 5 LEDs + illuminated accessories"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Color Sense - 5 светодиодов + подсветка"))
						.put(Lang.ITA, getWords(" Senso del colore - 5 LED + accessori illuminati"))
						.put(Lang.ESP, getWords(" Color Sense - 5 LEDs + accesorios iluminados"))
						.put(Lang.DEU, getWords(" Colour Sense - 5 LEDs + beleuchtetes Zubehör"))
						.put(Lang.FRA, getWords(" Color Sense - 5 LEDs + accessoires lumineux")).build());
		translates
				.put(getCode("Arms trimmed with Iroko wood or silver travertine or titanium travertine slats"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Руки, обрезанные древесиной Ироко или серебряным травертином или титановыми травертинами"))
						.put(Lang.ITA, getWords(" Armi rifinite con legno di Iroko o travertino argento o doghe in travertino di titanio"))
						.put(Lang.ESP, getWords(" Brazos adornados con madera de Iroko o travertino de plata o tablillas de travertino de titanio"))
						.put(Lang.DEU, getWords(" Die Arme sind mit Iroko-Holz oder Silber Travertin oder Titan Travertin Lamellen getrimmt"))
						.put(Lang.FRA, getWords(" Bras garnis de lattes de travertin en bois d'Iroko ou en travertin d'argent ou en travertin de titane")).build());
		translates
				.put(getCode("Unparalleled heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Непревзойденная удержание тепла и долговечность"))
						.put(Lang.ITA, getWords(" Ritenzione di calore e durata senza pari"))
						.put(Lang.ESP, getWords(" Retención de calor y durabilidad sin igual"))
						.put(Lang.DEU, getWords(" Beispiellose Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords(" Rétention de chaleur inégalée et durabilité")).build());
		translates
				.put(getCode("Minimalistic, freestanding, bowl-shaped tub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минималистичная, автономная, чашеобразная ванна"))
						.put(Lang.ITA, getWords(" Minimalista, autoportante, vasca a forma di vasca"))
						.put(Lang.ESP, getWords(" Minimalista, independiente, bañera en forma de tazón"))
						.put(Lang.DEU, getWords(" Minimalistische, freistehende, schüsselförmige Wanne"))
						.put(Lang.FRA, getWords(" Baignoire minimaliste, autoportante, en forme de cuvette")).build());
		translates
				.put(getCode("Cabinet finished in dark matt beige lacquer - 2100 x 500 x 250 mm / 82.75 x 19.75 x 10 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Шкаф выполнен из темного матового бежевого лака - 2100 x 500 x 250 мм / 82,75 x 19,75 x 10 дюймов"))
						.put(Lang.ITA, getWords(" Armadio rifinito in laccato beige scuro opaco - 2100 x 500 x 250 mm / 82,75 x 19,75 x 10 in"))
						.put(Lang.ESP, getWords(" Armario acabado en laca beige mate oscura - 2100 x 500 x 250 mm / 82.75 x 19.75 x 10 in"))
						.put(Lang.DEU, getWords(" Schrank in dunkel mattbeige lackiert - 2100 x 500 x 250 mm / 82,75 x 19,75 x 10 in"))
						.put(Lang.FRA, getWords(" Cabinet fini en laque beige foncé mat - 2100 x 500 x 250 mm / 82.75 x 19.75 x 10 po")).build());
		translates
				.put(getCode("Special purpose heat insulating ThermoMate™ foam layer on the underside"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Специальный слой теплоизоляции ThermoMate ™ на нижней стороне"))
						.put(Lang.ITA, getWords(" Strato termoisolante ThermoMate ™ a scopo speciale termoisolante sul lato inferiore"))
						.put(Lang.ESP, getWords(" Capa de espuma ThermoMate ™ aislante térmica especial en la parte inferior"))
						.put(Lang.DEU, getWords(" Wärmeisolierende ThermoMate ™ Spezialschaumstoffschicht auf der Unterseite"))
						.put(Lang.FRA, getWords(" Couche de mousse ThermoMate ™ thermo-isolante spéciale sur le dessous")).build());
		translates
				.put(getCode("20.9” x 20.5 (530 x 520 mm ) square built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("20,9 x 20,5 (530 x 520 мм) квадратная встроенная душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione da incasso quadrato da 20,9 x 20,5 (530 x 520 mm)"))
						.put(Lang.ESP, getWords(" Cabeza de ducha empotrable cuadrada de 20.9 x 20.5 (530 x 520 mm)"))
						.put(Lang.DEU, getWords(" 20,9 x 20,5 (530 x 520 mm) quadratischer Einbauduschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée carrée de 20,9 po x 20,5 po (530 x 520 mm)")).build());
		translates
				.put(getCode("Extra large 23.2x12.2 (590x310 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень большая 23,2x12,2 (590x310 мм) душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione extra large 23.2x12.2 (590x310 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 23.2x12.2 (590x310 mm)"))
						.put(Lang.DEU, getWords(" Extra großer 23,2x12,2 (590x310 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 23,2 x 12,2 po (590 x 310 mm)")).build());
		translates
				.put(getCode("Triple function water flow – rainfall, waterfall, spray"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Тройной поток воды - осадки, водопад, спрей"))
						.put(Lang.ITA, getWords(" Flusso dell'acqua a tripla funzione: pioggia, cascata, spruzzo"))
						.put(Lang.ESP, getWords(" Flujo de agua de triple función: lluvia, cascada, rocío"))
						.put(Lang.DEU, getWords(" Dreifachfunktion Wasserfluss - Regen, Wasserfall, Spray"))
						.put(Lang.FRA, getWords(" Flux d'eau à triple fonction - précipitations, chute d'eau, pulvérisation")).build());
		translates
				.put(getCode("Handy, vertical open fronted wall unit in matt cobalt blue lacquer 700 mm high x 150 mm wide by 210 mm deep (27.5 x 6 x 8.25 in)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Удобный вертикальный открытый настенный блок в матовом кобальтовом синем лаке 700 мм в высоту х 150 мм в ширину на 210 мм в глубину (27,5 х 6 х 8,25 дюйма)"))
						.put(Lang.ITA, getWords(" Comodo mobile verticale a parete aperta in laccato blu cobalto opaco altezza 700 mm x larghezza 150 mm per profondità 210 mm (27,5 x 6 x 8,25 pollici)"))
						.put(Lang.ESP, getWords(" Práctica, unidad de pared con fachada abierta vertical en laca azul cobalto mate 700 mm de alto x 150 mm de ancho por 210 mm de profundidad (27.5 x 6 x 8.25 in)"))
						.put(Lang.DEU, getWords(" Handliche, senkrechte Vorbauwand in matter kobaltblauer Lackierung 700 mm hoch x 150 mm breit und 210 mm tief (27,5 x 6 x 8,25 in)"))
						.put(Lang.FRA, getWords(" Unité murale à façade ouverte pratique en laque bleue cobalt mat de 700 mm de haut x 150 mm de large sur 210 mm de profondeur (27,5 x 6 x 8,25 po)")).build());
		translates
				.put(getCode("Available with optional air massage system and in matte finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с дополнительной системой воздушного массажа и матовой отделкой"))
						.put(Lang.ITA, getWords(" Disponibile con sistema opzionale di massaggio ad aria e finitura opaca"))
						.put(Lang.ESP, getWords(" Disponible con sistema de masaje de aire opcional y en acabado mate"))
						.put(Lang.DEU, getWords(" Erhältlich mit optionalem Luftmassagesystem und in mattem Finish"))
						.put(Lang.FRA, getWords(" Disponible avec système de massage à air en option et finition mate")).build());
		translates
				.put(getCode("15 years warranty on the spa structure"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("15 лет гарантии на спа-структуру"))
						.put(Lang.ITA, getWords(" 15 anni di garanzia sulla struttura della spa"))
						.put(Lang.ESP, getWords(" 15 años de garantía en la estructura del spa"))
						.put(Lang.DEU, getWords(" 15 Jahre Garantie auf die Spa-Struktur"))
						.put(Lang.FRA, getWords(" 15 ans de garantie sur la structure du spa")).build());
		translates
				.put(getCode("Quick and easy installation due to easily accessible exposed plumbing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Быстрая и простая установка благодаря легкодоступной открытой сантехнике"))
						.put(Lang.ITA, getWords(" Installazione semplice e veloce grazie a tubature esposte facilmente accessibili"))
						.put(Lang.ESP, getWords(" Instalación rápida y sencilla gracias a la plomería expuesta de fácil acceso"))
						.put(Lang.DEU, getWords(" Schnelle und einfache Installation durch leicht zugängliche, offene Rohrleitungen"))
						.put(Lang.FRA, getWords(" Installation rapide et facile en raison de la plomberie exposée facilement accessible")).build());
		translates
				.put(getCode("1 x 3kW heater"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("1 х 3 кВт нагреватель"))
						.put(Lang.ITA, getWords(" 1 riscaldatore 3kW"))
						.put(Lang.ESP, getWords(" 1 x 3kW calentador"))
						.put(Lang.DEU, getWords(" 1 x 3kW Heizung"))
						.put(Lang.FRA, getWords(" 1 radiateur de 3 kW")).build());
	}

	public static void part1() {
		translates
				.put(getCode("Made of AquateX™ Matte in fine matte finish – a homogeneous solid surface material with zero risks of delamination or discoloring"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Изготовлен из AquateX ™ Matte в матовой матовой отделке - однородный твердый поверхностный материал с нулевыми рисками расслоения или обесцвечивания"))
						.put(Lang.ITA, getWords("Realizzato in AquateX ™ Matte con finitura opaca - un materiale di superficie solido omogeneo con zero rischi di delaminazione o scolorimento"))
						.put(Lang.ESP, getWords("Hecho de AquateX ™ mate en acabado mate fino: un material homogéneo de superficie sólida con cero riesgos de delaminación o decoloración"))
						.put(Lang.DEU, getWords(" Hergestellt aus AquateX ™ Matte in feinem Matt-Finish - ein homogenes, festes Oberflächenmaterial ohne Delaminations- oder Verfärbungsgefahr"))
						.put(Lang.FRA, getWords(" Fabriqué en AquateX ™ Matte en finition mate fine - un matériau de surface solide homogène sans risque de décollement ou de décoloration")).build());
		translates
				.put(getCode("Large 11.8x11.8 (300x300 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая головка 11.8x11.8 (300x300 мм)"))
						.put(Lang.ITA, getWords(" Soffione grande 11.8x11.8 (300x300 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 11.8x11.8 (300x300 mm)"))
						.put(Lang.DEU, getWords(" Große 11,8 x 11,8 (300 x 300 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 11,8 x 11,8 po (300 x 300 mm)")).build());
		translates
				.put(getCode("900 x 500 x 500 mm (35.5 x 19.75 x 19.75 in) wide base in hard-wearing, easy clean Tobacco Sherwood composite veneer"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("900 x 500 x 500 мм (35,5 x 19,75 x 19,75 дюйма) широкая основа из износостойкой, легкой очистки Компонентный шпон из табака Sherwood"))
						.put(Lang.ITA, getWords(" Base larga 900 x 500 x 500 mm (35,5 x 19,75 x 19,75 pollici) in impiallacciato composito Tobacco Sherwood resistente e facile da pulire"))
						.put(Lang.ESP, getWords(" Base amplia de 900 x 500 x 500 mm (35,5 x 19,75 x 19,75 pulg.) En chapa de compuesto Sherwood de tabaco resistente y fácil de limpiar"))
						.put(Lang.DEU, getWords(" 900 x 500 x 500 mm breite Basis aus strapazierfähigem, pflegeleichtem Tobacco Sherwood Composite-Furnier"))
						.put(Lang.FRA, getWords(" Base large de 900 x 500 x 500 mm (35,5 x 19,75 x 19,75 po) en placage composite de tabac Sherwood, résistant à l'usure et facile à nettoyer")).build());
		translates
				.put(getCode("Clean and practical Scandinavian design with attractive and sculpted appeal"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чистый и практичный скандинавский дизайн с привлекательной и скульптурной привлекательностью"))
						.put(Lang.ITA, getWords(" Design scandinavo pulito e pratico con appeal accattivante e scolpito"))
						.put(Lang.ESP, getWords(" Diseño escandinavo limpio y práctico con atractivo y esculpido atractivo"))
						.put(Lang.DEU, getWords(" Sauberes und praktisches skandinavisches Design mit attraktiver und plastischer Ausstrahlung"))
						.put(Lang.FRA, getWords(" Design scandinave propre et pratique avec un attrait attrayant et sculpté")).build());
		translates
				.put(getCode("16 light programs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("16 легких программ"))
						.put(Lang.ITA, getWords(" 16 programmi di luce"))
						.put(Lang.ESP, getWords(" 16 programas de luz"))
						.put(Lang.DEU, getWords(" 16 Lichtprogramme"))
						.put(Lang.FRA, getWords(" 16 programmes légers")).build());
		translates
				.put(getCode("Clean lines, simple design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Чистые линии, простой дизайн"))
						.put(Lang.ITA, getWords(" Linee pulite, design semplice"))
						.put(Lang.ESP, getWords(" Líneas limpias, diseño simple"))
						.put(Lang.DEU, getWords(" Klare Linien, einfaches Design"))
						.put(Lang.FRA, getWords(" Des lignes épurées, un design simple")).build());
		translates
				.put(getCode("Matching white stone coated drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствующий слив из белого камня"))
						.put(Lang.ITA, getWords(" Scolo rivestito in pietra bianca abbinato"))
						.put(Lang.ESP, getWords(" Drenaje revestido de piedra blanca a juego"))
						.put(Lang.DEU, getWords(" Passender weißer steinbeschichteter Ablauf"))
						.put(Lang.FRA, getWords(" Drain assorti en pierre blanche assorti")).build());
		translates
				.put(getCode("Minimum installation height: 2250 mm (88.5 in)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Минимальная монтажная высота: 2250 мм (88,5 дюйма)"))
						.put(Lang.ITA, getWords(" Altezza minima di installazione: 2250 mm (88,5 pollici)"))
						.put(Lang.ESP, getWords(" Altura mínima de instalación: 2250 mm (88.5 in)"))
						.put(Lang.DEU, getWords(" Mindesteinbauhöhe: 2250 mm (88,5 in)"))
						.put(Lang.FRA, getWords(" Hauteur d'installation minimale: 2250 mm (88.5 in)")).build());
		translates
				.put(getCode("Weather-resistant, removable easy to wash covers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Устойчивые к атмосферным воздействиям, съемные легко моющиеся крышки"))
						.put(Lang.ITA, getWords(" Coperture facili da lavare e resistenti agli agenti atmosferici"))
						.put(Lang.ESP, getWords(" Resistente a la intemperie, extraíble fácil de lavar cubiertas"))
						.put(Lang.DEU, getWords(" Wetterbeständige, abnehmbare, leicht zu waschende Bezüge"))
						.put(Lang.FRA, getWords(" Résistant aux intempéries, amovible, facile à laver")).build());
		translates
				.put(getCode("28 x 27.4 (710 x 695 mm) square built-in shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("28 x 27.4 (710 x 695 мм) квадратная встроенная душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione da incasso quadrato da 28 x 27,4 (710 x 695 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha incorporado cuadrado de 28 x 27,4 (710 x 695 mm)"))
						.put(Lang.DEU, getWords(" 28 x 27,4 (710 x 695 mm) quadratischer integrierter Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée carrée de 28 x 27,4 (710 x 695 mm)")).build());
		translates
				.put(getCode("Round profile, slimline shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Круглый профиль, тонкая душевая головка"))
						.put(Lang.ITA, getWords(" Profilo rotondo, soffione sottile"))
						.put(Lang.ESP, getWords(" Perfil redondo, cabezal de ducha delgado"))
						.put(Lang.DEU, getWords(" Rundprofil, schmaler Duschkopf"))
						.put(Lang.FRA, getWords(" Profil rond, pomme de douche slimline")).build());
		translates
				.put(getCode("Easily to clean - use the ScotchBright sponge and detergent, just like with your dishes"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Легко чистить - используйте губку ScotchBright и моющее средство, как и ваши блюда"))
						.put(Lang.ITA, getWords(" Facile da pulire: usa la spugna ScotchBright e il detergente, proprio come i tuoi piatti"))
						.put(Lang.ESP, getWords(" Fácil de limpiar: use la esponja y el detergente ScotchBright, al igual que con sus platos"))
						.put(Lang.DEU, getWords(" Leicht zu reinigen - verwenden Sie den ScotchBright-Schwamm und das Reinigungsmittel, genau wie bei Ihrem Geschirr"))
						.put(Lang.FRA, getWords(" Facilement nettoyable - utilisez l'éponge et le détergent ScotchBright, comme avec votre vaisselle")).build());
		translates
				.put(getCode("Ergonomic interior including built-in seat step"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичный интерьер, включая встроенный стул"))
						.put(Lang.ITA, getWords(" Interno ergonomico con scalino integrato"))
						.put(Lang.ESP, getWords(" Interior ergonómico que incluye un asiento incorporado"))
						.put(Lang.DEU, getWords(" Ergonomischer Innenraum einschließlich eingebauter Sitzstufe"))
						.put(Lang.FRA, getWords(" Intérieur ergonomique avec marche intégrée")).build());
		translates
				.put(getCode("More elegant than a trip lever, this drain is operated by gently twisting the control head, which is mounted over the overflow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Более элегантный, чем рычаг отключения, этот дренаж управляется аккуратно скручиванием контрольной головки, которая монтируется поверх переполнения"))
						.put(Lang.ITA, getWords(" Più elegante di una leva di scatto, questo scarico viene azionato ruotando delicatamente la testina di controllo, che è montata sul troppo pieno"))
						.put(Lang.ESP, getWords(" Más elegante que una palanca de disparo, este desagüe se acciona girando suavemente el cabezal de control, que está montado sobre el rebosadero"))
						.put(Lang.DEU, getWords(" Eleganter als ein Auslösehebel, wird dieser Abfluss durch leichtes Drehen des Steuerkopfes betätigt, der über dem Überlauf montiert ist"))
						.put(Lang.FRA, getWords(" Plus élégant qu'un levier de déclenchement, ce drain est actionné en tordant doucement la tête de commande, qui est montée sur le trop-plein")).build());
		translates
				.put(getCode("6 RGB led"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("6 светодиодов RGB"))
						.put(Lang.ITA, getWords(" 6 led RGB"))
						.put(Lang.ESP, getWords(" 6 led RGB"))
						.put(Lang.DEU, getWords(" 6 RGB geführt"))
						.put(Lang.FRA, getWords(" 6 RVB a mené")).build());
		translates
				.put(getCode("Preinstalled pop-up waste fitting"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предустановленная установка для удаления всплывающих окон"))
						.put(Lang.ITA, getWords(" Scarico pop-up preinstallato"))
						.put(Lang.ESP, getWords(" Montaje de desecho automático preinstalado"))
						.put(Lang.DEU, getWords(" Vorinstallierte Ablaufgarnitur"))
						.put(Lang.FRA, getWords(" Garniture de vidage automatique préinstallée")).build());
		translates
				.put(getCode("Chromotherapy with multiple perimeter LEDs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромотерапия с несколькими светодиодами периметра"))
						.put(Lang.ITA, getWords(" Cromoterapia con LED perimetrali multipli"))
						.put(Lang.ESP, getWords(" Cromoterapia con múltiples LED perimetrales"))
						.put(Lang.DEU, getWords(" Chromotherapie mit mehreren Perimeter-LEDs"))
						.put(Lang.FRA, getWords(" Chromothérapie avec plusieurs LED de périmètre")).build());
		translates
				.put(getCode("This item set includes pre-assembled universal thermostatic concealed body with 3 independent 1/2” stopcocks"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Этот набор предметов включает предварительно собранный универсальный термостатический скрытый корпус с 3 независимыми 1/2 кранами"))
						.put(Lang.ITA, getWords(" Questo set di articoli comprende un corpo termostatico universale a scomparsa premontato con 3 rubinetti indipendenti da 1/2 "))
						.put(Lang.ESP, getWords(" Este conjunto de elementos incluye cuerpo oculto termostático universal premontado con 3 llaves de paso independientes de 1/2 "))
						.put(Lang.DEU, getWords(" Dieses Set beinhaltet einen vormontierten universellen Thermostat-Unterputzkörper mit 3 unabhängigen 1/2 Absperrhähnen"))
						.put(Lang.FRA, getWords(" Cet ensemble d'articles inclut le corps caché universel thermostatique pré-assemblé avec 3 robinets d'arrêt indépendants de 1/2 ")).build());
		translates
				.put(getCode("Plugs into a regular household electrical outlet"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Заглушки в обычную электрическую розетку"))
						.put(Lang.ITA, getWords(" Si inserisce in una normale presa di corrente domestica"))
						.put(Lang.ESP, getWords(" Se conecta a una toma de corriente doméstica normal"))
						.put(Lang.DEU, getWords(" Wird in eine normale Haushaltssteckdose gesteckt"))
						.put(Lang.FRA, getWords(" Se branche dans une prise électrique domestique ordinaire")).build());
		translates
				.put(getCode("Increases water depth by full 2"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Увеличивает глубину воды на 2 "))
						.put(Lang.ITA, getWords(" Aumenta la profondità dell'acqua di 2 "))
						.put(Lang.ESP, getWords(" Aumenta la profundidad del agua en 2 "))
						.put(Lang.DEU, getWords(" Erhöht die Wassertiefe um volle 2 "))
						.put(Lang.FRA, getWords(" Augmente la profondeur de l'eau de 2 ")).build());
		translates
				.put(getCode("Extra large 21.7x12.6 (550x320 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Очень большая душевая насадка 21,7x12,6 (550x320 мм)"))
						.put(Lang.ITA, getWords(" Soffione extra large 21,7x12,6 (550x320 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 21.7x12.6 (550x320 mm)"))
						.put(Lang.DEU, getWords(" Extra großer 21,7x12,6 (550x320 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 21,7 x 12,6 po (550 x 320 mm)")).build());
		translates
				.put(getCode("A handheld shower attachment is included, allowing for a more targeted clean"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("В комплект входит переносная душевая кабина, позволяющая использовать более целенаправленную очистку"))
						.put(Lang.ITA, getWords(" Un accessorio doccia palmare è incluso, consentendo una pulizia più mirata"))
						.put(Lang.ESP, getWords(" Se incluye un accesorio de ducha de mano, lo que permite una limpieza más específica"))
						.put(Lang.DEU, getWords(" Eine Handbrause ist im Lieferumfang enthalten und ermöglicht eine gezieltere Reinigung"))
						.put(Lang.FRA, getWords(" Un accessoire de douche à main est inclus, permettant un nettoyage plus ciblé")).build());
		translates
				.put(getCode("White LED illumination"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Белая светодиодная подсветка"))
						.put(Lang.ITA, getWords(" Illuminazione a LED bianchi"))
						.put(Lang.ESP, getWords(" Iluminación LED blanca"))
						.put(Lang.DEU, getWords(" Weiße LED-Beleuchtung"))
						.put(Lang.FRA, getWords(" Éclairage LED blanc")).build());
		translates
				.put(getCode("Anti-lime feature"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Анти-известь"))
						.put(Lang.ITA, getWords(" Caratteristica anticalcare"))
						.put(Lang.ESP, getWords(" Función anti-lima"))
						.put(Lang.DEU, getWords(" Antikalk-Funktion"))
						.put(Lang.FRA, getWords(" Fonction anti-calcaire")).build());
		translates
				.put(getCode("Large and very comfortable, premium design corner bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая и очень удобная, высококачественная угловая ванна"))
						.put(Lang.ITA, getWords(" Vasca da bagno angolare di design di grandi dimensioni e molto confortevole"))
						.put(Lang.ESP, getWords(" Bañera de esquina grande y muy cómoda, de diseño superior"))
						.put(Lang.DEU, getWords(" Große und sehr komfortable Eckbadewanne im Premium-Design"))
						.put(Lang.FRA, getWords(" Grande et très confortable baignoire d'angle design")).build());
		translates
				.put(getCode("Unique anti-bacterial characteristics"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальные антибактериальные характеристики"))
						.put(Lang.ITA, getWords(" Caratteristiche anti-batteriche uniche"))
						.put(Lang.ESP, getWords(" Características antibacterianas únicas"))
						.put(Lang.DEU, getWords(" Einzigartige antibakterielle Eigenschaften"))
						.put(Lang.FRA, getWords(" Caractéristiques anti-bactériennes uniques")).build());
		translates
				.put(getCode("Flat wall or corner installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Плоская стенная или угловая установка"))
						.put(Lang.ITA, getWords(" Installazione a parete piana o ad angolo"))
						.put(Lang.ESP, getWords(" Instalación de pared plana o esquina"))
						.put(Lang.DEU, getWords(" Flache Wand- oder Eckinstallation"))
						.put(Lang.FRA, getWords(" Installation de paroi plate ou d'angle")).build());
		translates
				.put(getCode("Max flow rate of the waterfall shower: 4.7 GPM (17.9 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока водопада: 4.7 GPM (17.9 LPM)"))
						.put(Lang.ITA, getWords(" Portata massima della doccia a cascata: 4,7 GPM (17,9 LPM)"))
						.put(Lang.ESP, getWords(" Caudal máximo de la cascada de ducha: 4.7 GPM (17.9 LPM)"))
						.put(Lang.DEU, getWords(" Max Flussrate der Wasserfall Dusche: 4,7 GPM (17,9 LPM)"))
						.put(Lang.FRA, getWords(" Débit maximum de la douche cascade: 4,7 GPM (17,9 LPM)")).build());
		translates
				.put(getCode("Should be used if the existing wall outlet is set at a height lower than 86.6in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Следует использовать, если существующая настенная розетка установлена ​​на высоте ниже 86,6 дюймов"))
						.put(Lang.ITA, getWords(" Dovrebbe essere usato se la presa a muro esistente è impostata ad un'altezza inferiore a 86,6 pollici"))
						.put(Lang.ESP, getWords(" Se debe utilizar si la toma de corriente existente se establece a una altura inferior a 86.6in"))
						.put(Lang.DEU, getWords(" Sollte verwendet werden, wenn die vorhandene Wandsteckdose auf eine Höhe von weniger als 86,6 Zoll eingestellt ist"))
						.put(Lang.FRA, getWords(" Doit être utilisé si la prise murale existante est réglée à une hauteur inférieure à 86,6 po")).build());
		translates
				.put(getCode("Sliding door design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Конструкция раздвижных дверей"))
						.put(Lang.ITA, getWords(" Design porta scorrevole"))
						.put(Lang.ESP, getWords(" Diseño de puerta corredera"))
						.put(Lang.DEU, getWords(" Schiebetür Design"))
						.put(Lang.FRA, getWords(" Conception de porte coulissante")).build());
		translates
				.put(getCode("Premium height of 120cm designed specifically for the True Ofuro bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум высотой 120 см, разработанный специально для ванны True Ofuro"))
						.put(Lang.ITA, getWords(" Altezza Premium di 120 cm progettata specificamente per la vasca True Ofuro"))
						.put(Lang.ESP, getWords(" Altura superior de 120 cm diseñada específicamente para la bañera True Ofuro"))
						.put(Lang.DEU, getWords(" Premium-Höhe von 120 cm speziell für die True Ofuro Badewanne konzipiert"))
						.put(Lang.FRA, getWords(" Hauteur de 120 cm conçue spécialement pour la baignoire True Ofuro")).build());
		translates
				.put(getCode("Two outlet thermostatic shower control mixer dimensions: 10.25 W x 6.75 H x 3 D (in) / 26 W x 17 H x 7.5 D (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Два выходных термостатических смесителя для душа: 10,25 Вт x 6,75 H x 3 D (дюйм) / 26 Вт x 17 H x 7,5 D (см)"))
						.put(Lang.ITA, getWords(" Miscelatore termostatico per doccia a due uscite dimensioni: 10,25 W x 6,75 H x 3 D (pollici) / 26 W x 17 H x 7,5 D (cm)"))
						.put(Lang.ESP, getWords(" Dimensiones del mezclador de control de la ducha termostática de dos salidas: 10.25 W x 6.75 H x 3 D (in) / 26 W x 17 H x 7.5 D (cm)"))
						.put(Lang.DEU, getWords(" Zwei Auslass-Thermostat-Brausebatterie-Mischer Abmessungen: 10,25 W x 6,75 H x 3 D (Zoll) / 26 W x 17 H x 7,5 D (cm)"))
						.put(Lang.FRA, getWords(" Dimensions du mélangeur de douche thermostatique à deux prises: 10,25 W x 6,75 H x 3 D (in) / 26 W x 17 H x 7,5 D (cm)")).build());
		translates
				.put(getCode("Easy installation on existing toilets; Adaptable to most standard fixtures"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Простая установка на существующих туалетах;  Адаптируется к большинству стандартных светильников"))
						.put(Lang.ITA, getWords(" Facile installazione su servizi igienici esistenti;  Adattabile alla maggior parte dei proiettori standard"))
						.put(Lang.ESP, getWords(" Instalación fácil en baños existentes;  Adaptable a la mayoría de los accesorios estándar"))
						.put(Lang.DEU, getWords(" Einfache Installation auf vorhandenen Toiletten;  Anpassbar an die meisten Standard-Geräte"))
						.put(Lang.FRA, getWords(" Installation facile sur les toilettes existantes;  Adaptable à la plupart des appareils standard")).build());
		translates
				.put(getCode("Solid, one-piece construction for safety and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Твердая, цельная конструкция для обеспечения безопасности и долговечности"))
						.put(Lang.ITA, getWords(" Solida costruzione monopezzo per sicurezza e durata"))
						.put(Lang.ESP, getWords(" Construcción sólida de una sola pieza para mayor seguridad y durabilidad"))
						.put(Lang.DEU, getWords(" Solide, einteilige Konstruktion für Sicherheit und Langlebigkeit"))
						.put(Lang.FRA, getWords(" Construction monobloc solide pour la sécurité et la durabilité")).build());
		translates
				.put(getCode("1 Year Limited Warranty on the drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия 1 года на слив"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 1 anno sullo scarico"))
						.put(Lang.ESP, getWords(" Garantía limitada de 1 año en el desagüe"))
						.put(Lang.DEU, getWords(" 1 Jahr beschränkte Garantie auf den Abfluss"))
						.put(Lang.FRA, getWords(" Garantie limitée d'un an sur le drain")).build());
		translates
				.put(getCode("High temperature, vacuum coated decorative exterior panel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Декоративная наружная панель с высокой температурой, с вакуумным покрытием"))
						.put(Lang.ITA, getWords(" Pannello esterno decorativo verniciato ad alta temperatura"))
						.put(Lang.ESP, getWords(" Panel exterior decorativo de alta temperatura, revestido al vacío"))
						.put(Lang.DEU, getWords(" Hochtemperatur-, vakuumbeschichtete dekorative Außenverkleidung"))
						.put(Lang.FRA, getWords(" Panneau extérieur décoratif enduit sous vide à haute température")).build());
		translates
				.put(getCode("IQ Smart Shower Control is available in two colors: white or black"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("IQ Smart Shower Control доступен в двух цветах: белый или черный"))
						.put(Lang.ITA, getWords(" IQ Smart Shower Control è disponibile in due colori: bianco o nero"))
						.put(Lang.ESP, getWords(" IQ Smart Shower Control está disponible en dos colores: blanco o negro"))
						.put(Lang.DEU, getWords(" IQ Smart Shower Control ist in zwei Farben erhältlich: weiß oder schwarz"))
						.put(Lang.FRA, getWords(" IQ Smart Shower Control est disponible en deux couleurs: blanc ou noir")).build());
		translates
				.put(getCode("Available with optional hydro-massage and/or air-massage system"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с дополнительным гидромассажем и / или системой воздушного массажа"))
						.put(Lang.ITA, getWords(" Disponibile con idromassaggio opzionale e / o sistema di massaggio ad aria"))
						.put(Lang.ESP, getWords(" Disponible con sistema opcional de hidromasaje y / o masaje de aire"))
						.put(Lang.DEU, getWords(" Erhältlich mit optionalem Hydromassage- und / oder Luftmassagesystem"))
						.put(Lang.FRA, getWords(" Disponible avec un système d'hydromassage et / ou de massage à air en option")).build());
		translates
				.put(getCode("Power supply - 12V"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Электропитание - 12 В"))
						.put(Lang.ITA, getWords(" Alimentazione elettrica 12V"))
						.put(Lang.ESP, getWords(" Fuente de alimentación - 12V"))
						.put(Lang.DEU, getWords(" Stromversorgung - 12V"))
						.put(Lang.FRA, getWords(" Alimentation - 12V")).build());
		translates
				.put(getCode("Soft touch remote control and side mounted supplementary buttons"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Пульт дистанционного управления с мягким касанием и дополнительные дополнительные кнопки"))
						.put(Lang.ITA, getWords(" Telecomando soft touch e pulsanti supplementari montati lateralmente"))
						.put(Lang.ESP, getWords(" Control remoto táctil suave y botones suplementarios montados lateralmente"))
						.put(Lang.DEU, getWords(" Soft-Touch-Fernbedienung und seitlich angebrachte Zusatztasten"))
						.put(Lang.FRA, getWords(" Télécommande tactile et boutons supplémentaires latéraux")).build());
		translates
				.put(getCode("Bathtub and integrated washbasin constructed from AquateX™"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ванна и встроенный умывальник, построенный из AquateX ™"))
						.put(Lang.ITA, getWords(" Vasca e lavabo integrato realizzati in AquateX ™"))
						.put(Lang.ESP, getWords(" Bañera y lavabo integrado construidos con AquateX ™"))
						.put(Lang.DEU, getWords(" Badewanne und integriertes Waschbecken aus AquateX ™"))
						.put(Lang.FRA, getWords(" Baignoire et vasque intégrée en AquateX ™")).build());
		translates
				.put(getCode("Premium design, one of a kind freestanding status bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-дизайн, единственная в своем роде автономная ванна"))
						.put(Lang.ITA, getWords(" Design di alta qualità, vasca da bagno indipendente di livello"))
						.put(Lang.ESP, getWords(" Diseño premium, una de una bañera de estado independiente"))
						.put(Lang.DEU, getWords(" Premium-Design, einzigartige freistehende Badewanne"))
						.put(Lang.FRA, getWords(" Un design haut de gamme, une baignoire autoportante unique en son genre")).build());
		translates
				.put(getCode("Ergonomic design that follows human body’s shape for ultimate comfort with built-in sculptured headrest"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичный дизайн, который следует за формой человеческого тела для максимального комфорта со встроенным скульптурным подголовником"))
						.put(Lang.ITA, getWords(" Design ergonomico che segue la forma del corpo umano per il massimo comfort con poggiatesta scolpito incorporato"))
						.put(Lang.ESP, getWords(" Diseño ergonómico que sigue la forma del cuerpo humano para la máxima comodidad con reposacabezas esculpido incorporado"))
						.put(Lang.DEU, getWords(" Ergonomisches Design, das der Form des menschlichen Körpers nachempfunden ist und dank der integrierten skulpturierten Kopfstütze höchsten Komfort bietet"))
						.put(Lang.FRA, getWords(" Conception ergonomique qui suit la forme du corps humain pour un confort ultime avec appuie-tête sculpté intégré")).build());
		translates
				.put(getCode("Shower head with 81 jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Душевая головка с 81 форсункой"))
						.put(Lang.ITA, getWords(" Soffione doccia con 81 getti"))
						.put(Lang.ESP, getWords(" Cabezal de ducha con 81 chorros"))
						.put(Lang.DEU, getWords(" Duschkopf mit 81 Düsen"))
						.put(Lang.FRA, getWords(" Pomme de douche avec 81 jets")).build());
		translates
				.put(getCode("Constructed of stainless steel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Построено из нержавеющей стали"))
						.put(Lang.ITA, getWords(" Costruito in acciaio inossidabile"))
						.put(Lang.ESP, getWords(" Construido de acero inoxidable"))
						.put(Lang.DEU, getWords(" Hergestellt aus Edelstahl"))
						.put(Lang.FRA, getWords(" Construit en acier inoxydable")).build());
		translates
				.put(getCode("A unique bathing experience that is great for the body and mind"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Уникальный опыт купания, который отлично подходит для тела и ума"))
						.put(Lang.ITA, getWords(" Un'esperienza balneare unica per il corpo e la mente"))
						.put(Lang.ESP, getWords(" Una experiencia de baño única que es genial para el cuerpo y la mente"))
						.put(Lang.DEU, getWords(" Ein einzigartiges Badeerlebnis, das Körper und Geist gut tut"))
						.put(Lang.FRA, getWords(" Une expérience de bain unique, idéale pour le corps et l'esprit")).build());
		translates
				.put(getCode("2 year overall limited heated therapy warranty"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("2-летняя ограниченная гарантия на нагретую терапию"))
						.put(Lang.ITA, getWords(" 2 anni di garanzia di terapia riscaldata limitata complessiva"))
						.put(Lang.ESP, getWords(" Garantía general limitada de terapia de 2 años"))
						.put(Lang.DEU, getWords(" 2 Jahre eingeschränkte Beheizungsgarantie"))
						.put(Lang.FRA, getWords(" Garantie générale limitée de 2 ans sur les thérapies chauffantes")).build());
		translates
				.put(getCode("AquateX™ material provides excellent heat retention and durability"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Материал AquateX ™ обеспечивает отличное удержание тепла и долговечность"))
						.put(Lang.ITA, getWords(" Il materiale AquateX ™ offre un'eccellente ritenzione di calore e durata"))
						.put(Lang.ESP, getWords(" El material AquateX ™ proporciona una excelente retención de calor y durabilidad"))
						.put(Lang.DEU, getWords(" AquateX ™ -Material bietet hervorragende Wärmespeicherung und Haltbarkeit"))
						.put(Lang.FRA, getWords(" Le matériau AquateX ™ offre une excellente rétention de chaleur et une durabilité")).build());
		translates
				.put(getCode("Also available with a powder-coated aluminum frame in white (A 12) or graphite (A 14)."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Также поставляется с алюминиевой рамой с порошковым покрытием в белом (A 12) или графитом (A 14)."))
						.put(Lang.ITA, getWords(" Disponibile anche con telaio in alluminio verniciato a polvere in bianco (A 12) o grafite (A 14)."))
						.put(Lang.ESP, getWords(" También disponible con un marco de aluminio con recubrimiento de polvo en blanco (A 12) o grafito (A 14)."))
						.put(Lang.DEU, getWords(" Auch mit pulverbeschichtetem Aluminiumrahmen in Weiß (A 12) oder Graphit (A 14) erhältlich."))
						.put(Lang.FRA, getWords(" En outre disponible avec un cadre en aluminium enduit de poudre en blanc (A 12) ou en graphite (A 14).")).build());
		translates
				.put(getCode("Ergonomic design forms to the body’s shape for ultimate comfort with built-in sculptured headrest"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Эргономичный дизайн формирует форму тела для максимального комфорта со встроенным скульптурным подголовником"))
						.put(Lang.ITA, getWords(" Design ergonomico per la forma del corpo per il massimo comfort con poggiatesta scolpito incorporato"))
						.put(Lang.ESP, getWords(" El diseño ergonómico se adapta a la forma del cuerpo para una mayor comodidad con reposacabezas esculpido incorporado"))
						.put(Lang.DEU, getWords(" Das ergonomische Design passt sich der Körperform an und sorgt dank der integrierten Kopfstütze für ultimativen Komfort"))
						.put(Lang.FRA, getWords(" Le design ergonomique épouse la forme du corps pour un confort ultime avec un appui-tête sculpté intégré")).build());
		translates
				.put(getCode("100% recyclable"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("100% подлежащая вторичной переработке"))
						.put(Lang.ITA, getWords(" 100% riciclabile"))
						.put(Lang.ESP, getWords(" 100% reciclable"))
						.put(Lang.DEU, getWords(" 100% recycelbar"))
						.put(Lang.FRA, getWords(" 100% recyclable")).build());
		translates
				.put(getCode("Extra large 23.2x12.2 (59x31 cm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра большая душевая головка 23,2x12,2 (59x31 см)"))
						.put(Lang.ITA, getWords(" Soffione extra large 23.2x12.2 (59x31 cm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 23.2x12.2 (59x31 cm)"))
						.put(Lang.DEU, getWords(" Extra großer 23.2x12.2 (59x31 cm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 23,2 x 12,2 po (59 x 31 cm)")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 1 outlet shut-off valve and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 1 выпускным запорным клапаном и металлической пластиной"))
						.put(Lang.ITA, getWords(" Miscelatore doccia esterno a parete con 1 valvola di intercettazione in uscita e piastra metallica"))
						.put(Lang.ESP, getWords(" Mezclador de ducha de pared con 1 válvula de cierre de salida y placa de metal"))
						.put(Lang.DEU, getWords(" Wand-Brausebatterie mit 1 Auslauf-Absperrventil und Metallplatte"))
						.put(Lang.FRA, getWords(" Mitigeur de douche mural avec 1 vanne d'arrêt et plaque métallique")).build());
		translates
				.put(getCode("Mirror dimensions: 55 L x 28.25 W (in) / 140 L x 72 W (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Размеры зеркала: 55 L x 28,25 Вт (дюйм) / 140 л x 72 Вт (см)"))
						.put(Lang.ITA, getWords(" Dimensioni dello specchio: 55 L x 28,25 W (pollici) / 140 L x 72 W (cm)"))
						.put(Lang.ESP, getWords(" Dimensiones del espejo: 55 L x 28.25 W (in) / 140 L x 72 W (cm)"))
						.put(Lang.DEU, getWords(" Spiegelmaße: 55 L x 28,25 W (Zoll) / 140 L x 72 W (cm)"))
						.put(Lang.FRA, getWords(" Dimensions du miroir: 55 L x 28,25 W (po) / 140 L x 72 W (cm)")).build());
		translates
				.put(getCode("One seamless unit - eliminates much of the messy installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Один бесшовный блок - исключает большую часть беспорядочной установки"))
						.put(Lang.ITA, getWords(" Un'unità senza soluzione di continuità: elimina gran parte dell'installazione disordinata"))
						.put(Lang.ESP, getWords(" Una unidad sin costura: elimina gran parte de la instalación desordenada"))
						.put(Lang.DEU, getWords(" Eine nahtlose Einheit - eliminiert viel von der unordentlichen Installation"))
						.put(Lang.FRA, getWords(" Une unité sans couture - élimine une grande partie de l'installation désordonnée")).build());
		translates
				.put(getCode("Large wall mounted mirror with 4 LED lights and a PVC Loom"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большое настенное зеркало с 4 светодиодными фонарями и ПВХ-трубой"))
						.put(Lang.ITA, getWords(" Grande specchio a parete con 4 luci a LED e telaio in PVC"))
						.put(Lang.ESP, getWords(" Gran espejo montado en la pared con 4 luces LED y un telar de PVC"))
						.put(Lang.DEU, getWords(" Großer Wandspiegel mit 4 LED-Leuchten und einem PVC-Loom"))
						.put(Lang.FRA, getWords(" Grand miroir mural avec 4 lumières LED et un métier à tisser en PVC")).build());
		translates
				.put(getCode("Also available with a powder-coated aluminum frame in white (A 12) with grey fabric or graphite (A 14) with dark grey fabric (C81)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Также поставляется с алюминиевой рамой с порошковым покрытием в белом (A 12) с серой тканью или графитом (A 14) с темно-серой тканью (C81)"))
						.put(Lang.ITA, getWords(" Disponibile anche con telaio in alluminio verniciato a polvere bianco (A 12) con tessuto grigio o grafite (A 14) con tessuto grigio scuro (C81)"))
						.put(Lang.ESP, getWords(" También disponible con un marco de aluminio con recubrimiento de polvo en blanco (A 12) con tela gris o grafito (A 14) con tela gris oscuro (C81)"))
						.put(Lang.DEU, getWords(" Auch erhältlich mit einem pulverbeschichteten Aluminiumrahmen in Weiß (A 12) mit grauem Stoff oder Graphit (A 14) mit dunkelgrauem Stoff (C81)"))
						.put(Lang.FRA, getWords(" En outre disponible avec un cadre en aluminium enduit de poudre en blanc (A 12) avec le tissu gris ou le graphite (A 14) avec le tissu gris foncé (C81)")).build());
		translates
				.put(getCode("A hypoallergenic, non-porous surface for easy cleaning and sanitizing"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Гипоаллергенная, непористая поверхность для легкой очистки и дезинфекции"))
						.put(Lang.ITA, getWords(" Una superficie ipoallergenica e non porosa per una facile pulizia e sanificazione"))
						.put(Lang.ESP, getWords(" Una superficie hipoalergénica, no porosa para facilitar la limpieza y la desinfección"))
						.put(Lang.DEU, getWords(" Eine hypoallergene, porenfreie Oberfläche zur einfachen Reinigung und Desinfektion"))
						.put(Lang.FRA, getWords(" Une surface hypoallergénique et non poreuse pour un nettoyage et une désinfection faciles")).build());
		translates
				.put(getCode("Designed to coordinate beautifully with any of Aquatica's freestanding bathtubs"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для красивой координации с любой из автономных ванн Aquatica"))
						.put(Lang.ITA, getWords(" Progettato per coordinarsi magnificamente con qualsiasi delle vasche da bagno indipendenti Aquatica"))
						.put(Lang.ESP, getWords(" Diseñado para coordinar maravillosamente con cualquiera de las bañeras independientes de Aquatica"))
						.put(Lang.DEU, getWords(" Entwickelt für die perfekte Abstimmung mit den freistehenden Badewannen von Aquatica"))
						.put(Lang.FRA, getWords(" Conçu pour se coordonner magnifiquement avec l'une des baignoires autoportantes Aquatica")).build());
		translates
				.put(getCode("Extra large 19.7 (500 mm) shower head"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра большая 19,7 (500 мм) душевая насадка"))
						.put(Lang.ITA, getWords(" Soffione extra large da 19,7 (500 mm)"))
						.put(Lang.ESP, getWords(" Cabezal de ducha extra grande de 19.7 (500 mm)"))
						.put(Lang.DEU, getWords(" Extra großer 19.7 (500 mm) Duschkopf"))
						.put(Lang.FRA, getWords(" Pomme de douche extra large de 19,7 po (500 mm)")).build());
		translates
				.put(getCode("Hard, modern lines and a crisp, clean rectangular shape suited to either traditional or contemporary bathroom"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Жесткие, современные линии и четкая, чистая прямоугольная форма, подходящая как для традиционной, так и для современной ванной комнаты"))
						.put(Lang.ITA, getWords(" Linee dure e moderne e una forma rettangolare nitida e pulita adatta al bagno tradizionale o contemporaneo"))
						.put(Lang.ESP, getWords(" Líneas duras y modernas y una forma rectangular nítida y limpia adecuada para baño tradicional o contemporáneo"))
						.put(Lang.DEU, getWords(" Harte, moderne Linien und eine klare, saubere rechteckige Form, die sowohl für traditionelle als auch für moderne Badezimmer geeignet ist"))
						.put(Lang.FRA, getWords(" Des lignes dures et modernes et une forme rectangulaire propre et nette adaptée à une salle de bain traditionnelle ou contemporaine")).build());
		translates
				.put(getCode("Translucent decorative acrylic top panel"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прозрачная декоративная акриловая верхняя панель"))
						.put(Lang.ITA, getWords(" Pannello superiore acrilico decorativo traslucido"))
						.put(Lang.ESP, getWords(" Panel superior acrílico decorativo translúcido"))
						.put(Lang.DEU, getWords(" Transluzente dekorative Acrylplatte"))
						.put(Lang.FRA, getWords(" Panneau supérieur décoratif en acrylique translucide")).build());
		translates
				.put(getCode("Arm heght 7.9 (20 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высота рукоятки 7,9 (20 см)"))
						.put(Lang.ITA, getWords(" Arm heght 7,9 (20 cm)"))
						.put(Lang.ESP, getWords(" Brazo de 7.9 (20 cm)"))
						.put(Lang.DEU, getWords(" Armhöhe 7,9 (20 cm)"))
						.put(Lang.FRA, getWords(" Bras de 7,9 po (20 cm)")).build());
		translates
				.put(getCode("Other RAL colors available upon demand"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Другие цвета RAL по запросу"))
						.put(Lang.ITA, getWords(" Altri colori RAL disponibili su richiesta"))
						.put(Lang.ESP, getWords(" Otros colores RAL disponibles bajo demanda"))
						.put(Lang.DEU, getWords(" Andere RAL-Farben auf Anfrage erhältlich"))
						.put(Lang.FRA, getWords(" D'autres couleurs RAL sont disponibles sur demande")).build());
		translates
				.put(getCode("Tested and works well with Aquatica Freestanding Bath Installer for an easy installation."), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Протестировано и хорошо работает с Aquatica Freestanding Bath Installer для легкой установки."))
						.put(Lang.ITA, getWords(" Testato e funziona bene con Aquatica Freestanding Bath Installer per una facile installazione."))
						.put(Lang.ESP, getWords(" Probado y funciona bien con el instalador de baño independiente Aquatica para una instalación fácil."))
						.put(Lang.DEU, getWords(" Getestet und funktioniert gut mit Aquatica Freistehende Badewanne Installer für eine einfache Installation."))
						.put(Lang.FRA, getWords(" Testé et fonctionne bien avec l'installateur de baignoire autoportante Aquatica pour une installation facile.")).build());
		translates
				.put(getCode("Highest quality materials and manufacture"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Высококачественные материалы и производство"))
						.put(Lang.ITA, getWords(" Materiali e produzione di altissima qualità"))
						.put(Lang.ESP, getWords(" Materiales y fabricación de la más alta calidad"))
						.put(Lang.DEU, getWords(" Höchste Qualität der Materialien und Herstellung"))
						.put(Lang.FRA, getWords(" La plus haute qualité des matériaux et la fabrication")).build());
		translates
				.put(getCode("Available with black gloss or white gloss fascia"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Доступен с черным блеском или белой глянцевой панелью"))
						.put(Lang.ITA, getWords(" Disponibile con fascia nera lucida o bianca lucida"))
						.put(Lang.ESP, getWords(" Disponible con brillo negro o fascia de brillo blanco"))
						.put(Lang.DEU, getWords(" Erhältlich mit schwarzer Glanz- oder weißer Glanzblende"))
						.put(Lang.FRA, getWords(" Disponible avec le fascia de lustre noir ou de lustre blanc")).build());
		translates
				.put(getCode("Second 500 x 350 mm (19.75 in x 13.75 in) frameless wall mirror"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Второе 500 x 350 мм (19,75 дюйма x 13,75 дюйма) бескаркасное настенное зеркало"))
						.put(Lang.ITA, getWords(" Secondo specchio da parete senza cornice da 500 x 350 mm (19,75 in x 13,75 in)"))
						.put(Lang.ESP, getWords(" Segundo espejo de pared sin marco de 500 x 350 mm (19.75 in x 13.75 in)"))
						.put(Lang.DEU, getWords(" Zweiter rahmenloser Wandspiegel, 500 x 350 mm (19,75 Zoll x 13,75 Zoll)"))
						.put(Lang.FRA, getWords(" Deuxième miroir mural sans cadre de 500 x 350 mm (19,75 po x 13,75 po)")).build());
		translates
				.put(getCode("Surface scratches, wear and tear will not affect the color"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Поверхностные царапины, износ не повлияют на цвет"))
						.put(Lang.ITA, getWords(" I graffi superficiali, l'usura non influenzano il colore"))
						.put(Lang.ESP, getWords(" Arañazos en la superficie, el desgaste no afectará el color"))
						.put(Lang.DEU, getWords(" Oberflächenkratzer, Abnutzung und Abnutzung beeinflussen die Farbe nicht"))
						.put(Lang.FRA, getWords(" Rayures de surface, l'usure et la déchirure n'affectera pas la couleur")).build());
		translates
				.put(getCode("Bluetooth Audio system with stereo speakers"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Аудиосистема Bluetooth со стереодинамиками"))
						.put(Lang.ITA, getWords(" Sistema audio Bluetooth con altoparlanti stereo"))
						.put(Lang.ESP, getWords(" Sistema de audio Bluetooth con parlantes estéreo"))
						.put(Lang.DEU, getWords(" Bluetooth-Audiosystem mit Stereo-Lautsprechern"))
						.put(Lang.FRA, getWords(" Système audio Bluetooth avec haut-parleurs stéréo")).build());
		translates
				.put(getCode("Wall-mounted Tobacco Sherwood laminate base unit 1200 x 500 x 365 mm / 47.25 x 19.75 x 14.25 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный базовый блок ламината для табака Sherwood 1200 x 500 x 365 мм / 47,25 x 19,75 x 14,25 дюйма"))
						.put(Lang.ITA, getWords(" Base a parete in laminato Sherwood Tabacco 1200 x 500 x 365 mm / 47.25 x 19.75 x 14.25 in"))
						.put(Lang.ESP, getWords(" Unidad de base de laminado de Sherwood de tabaco Tabacco 1200 x 500 x 365 mm / 47.25 x 19.75 x 14.25 in"))
						.put(Lang.DEU, getWords(" An der Wand befestigtes Tabak-Sherwood-Laminatunterteil 1200 x 500 x 365 Millimeter / 47.25 x 19.75 x 14.25 in"))
						.put(Lang.FRA, getWords(" Élément de base stratifié en bois Sherwood tabac 1200 x 500 x 365 mm / 47,25 x 19,75 x 14,25 po")).build());
		translates
				.put(getCode("24 x air massage jets"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("24 струй воздуха для воздуха"))
						.put(Lang.ITA, getWords(" 24 x getti d'aria"))
						.put(Lang.ESP, getWords(" 24 x jets de masaje de aire"))
						.put(Lang.DEU, getWords(" 24 x Luftmassagedüsen"))
						.put(Lang.FRA, getWords(" 24 jets de massage à air")).build());
		translates
				.put(getCode("Recommended temperature: 50-60 °C"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Рекомендуемая температура: 50-60 ° C"))
						.put(Lang.ITA, getWords(" Temperatura consigliata: 50-60 ° C"))
						.put(Lang.ESP, getWords(" Temperatura recomendada: 50-60 ° C"))
						.put(Lang.DEU, getWords(" Empfohlene Temperatur: 50-60 ° C"))
						.put(Lang.FRA, getWords(" Température recommandée: 50-60 ° C")).build());
		translates
				.put(getCode("5 Year Limited Warranty on the door"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Ограниченная гарантия 5 лет на двери"))
						.put(Lang.ITA, getWords(" Garanzia limitata di 5 anni sulla porta"))
						.put(Lang.ESP, getWords(" Garantía limitada de 5 años en la puerta"))
						.put(Lang.DEU, getWords(" 5 Jahre eingeschränkte Garantie auf die Tür"))
						.put(Lang.FRA, getWords(" Garantie limitée de 5 ans sur la porte")).build());
		translates
				.put(getCode("Frame Color: Mokka - Cushion Color: Grey Light / Mélange grey / White-beige"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Цвет рамки: Mokka - Подушка Цвет: Серый Свет / Меланж серый / Бело-бежевый"))
						.put(Lang.ITA, getWords(" Colore telaio: Mokka - Cuscino Colore: Grigio chiaro / Grigio melange / Bianco-beige"))
						.put(Lang.ESP, getWords(" Color del marco: Mokka - Cojín Color: Gris claro / Mélange gris / Blanco-beige"))
						.put(Lang.DEU, getWords(" Gestellfarbe: Mokka - Kissen Farbe: Grau Hell / Melange Grau / Weiß-Beige"))
						.put(Lang.FRA, getWords(" Couleur de la monture: Mokka - Cushion Couleur: Grey Light / Mélange grey / White-beige")).build());
		translates
				.put(getCode("Square built-in shower head - 9.75x9.75 (25x25 cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Квадратная встроенная душевая головка - 9,75 x9,75 (25x25 см)"))
						.put(Lang.ITA, getWords(" Soffione a incasso quadrato - 9,75 x9,75 (25x25 cm)"))
						.put(Lang.ESP, getWords(" Cabeza de ducha cuadrada incorporada - 9.75 x9.75 (25x25 cm)"))
						.put(Lang.DEU, getWords(" Square Einbauduschkopf - 9,75 x9.75 (25x25 cm)"))
						.put(Lang.FRA, getWords(" Pomme de douche encastrée carrée - 9.75 x9.75 (25x25 cm)")).build());
		translates
				.put(getCode("Control key for switch on/off light required"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Контрольная клавиша для включения / выключения подсветки"))
						.put(Lang.ITA, getWords(" Chiave di controllo per accensione / spegnimento luce richiesta"))
						.put(Lang.ESP, getWords(" Se requiere la tecla de control para encender / apagar la luz"))
						.put(Lang.DEU, getWords(" Steuertaste zum Ein- / Ausschalten des Lichts erforderlich"))
						.put(Lang.FRA, getWords(" Touche de commande pour allumer / éteindre la lumière requise")).build());
		translates
				.put(getCode("Extra deep and ergonomic design for ultimate comfort"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Экстра глубокий и эргономичный дизайн для максимального комфорта"))
						.put(Lang.ITA, getWords(" Design estremamente profondo ed ergonomico per il massimo comfort"))
						.put(Lang.ESP, getWords(" Diseño extra profundo y ergonómico para la máxima comodidad"))
						.put(Lang.DEU, getWords(" Extra tiefes und ergonomisches Design für ultimativen Komfort"))
						.put(Lang.FRA, getWords(" Conception extra profonde et ergonomique pour un confort ultime")).build());
		translates
				.put(getCode("Precision engineering for steady stream of water"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Прецизионная техника для устойчивого потока воды"))
						.put(Lang.ITA, getWords(" Ingegneria di precisione per flusso costante di acqua"))
						.put(Lang.ESP, getWords(" Ingeniería de precisión para un flujo constante de agua"))
						.put(Lang.DEU, getWords(" Feinmechanik für stetigen Wasserstrahl"))
						.put(Lang.FRA, getWords(" Ingénierie de précision pour un flux d'eau constant")).build());
		translates
				.put(getCode("Adaptable to most standard fixtures"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Адаптируется к большинству стандартных светильников"))
						.put(Lang.ITA, getWords(" Adattabile alla maggior parte dei proiettori standard"))
						.put(Lang.ESP, getWords(" Adaptable a la mayoría de los accesorios estándar"))
						.put(Lang.DEU, getWords(" Anpassbar an die meisten Standard-Geräte"))
						.put(Lang.FRA, getWords(" Adaptable à la plupart des appareils standard")).build());
		translates
				.put(getCode("Thermostatic valve included"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Термостатический клапан"))
						.put(Lang.ITA, getWords(" Valvola termostatica inclusa"))
						.put(Lang.ESP, getWords(" Válvula termostática incluida"))
						.put(Lang.DEU, getWords(" Thermostatventil enthalten"))
						.put(Lang.FRA, getWords(" Valve thermostatique incluse")).build());
		translates
				.put(getCode("Controls mounted on the side of the seat"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Элементы управления, установленные сбоку от сиденья"))
						.put(Lang.ITA, getWords(" Comandi montati sul lato del sedile"))
						.put(Lang.ESP, getWords(" Controles montados en el costado del asiento"))
						.put(Lang.DEU, getWords(" Bedienelemente an der Seite des Sitzes montiert"))
						.put(Lang.FRA, getWords(" Commandes montées sur le côté du siège")).build());
		translates
				.put(getCode("Wall-mounted for flexibility in height installation"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенная установка для гибкой установки высоты"))
						.put(Lang.ITA, getWords(" Installazione a parete per flessibilità nell'installazione in altezza"))
						.put(Lang.ESP, getWords(" Montado en la pared para una instalación flexible en altura"))
						.put(Lang.DEU, getWords(" An der Wand befestigt für Flexibilität in der Höheninstallation"))
						.put(Lang.FRA, getWords(" Mural pour une installation flexible en hauteur")).build());
		translates
				.put(getCode("Chrome Finish"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромированная отделка"))
						.put(Lang.ITA, getWords(" Finitura cromata"))
						.put(Lang.ESP, getWords(" Acabado cromado"))
						.put(Lang.DEU, getWords(" Chrom-Finish"))
						.put(Lang.FRA, getWords(" Finition chromée")).build());
		translates
				.put(getCode("Can be made in different finishes upon request"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("«Может быть сделано в разных вариантах по запросу"))
						.put(Lang.ITA, getWords(" Può essere realizzato in diverse finiture su richiesta"))
						.put(Lang.ESP, getWords(" Se puede hacer en diferentes acabados bajo pedido"))
						.put(Lang.DEU, getWords(" Kann in verschiedenen Ausführungen auf Anfrage hergestellt werden"))
						.put(Lang.FRA, getWords(" Peut être fait dans différentes finitions sur demande")).build());
		translates
				.put(getCode("Max water consumption - 3.9 GPM (15 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальное потребление воды - 3,9 GPM (15 LPM)"))
						.put(Lang.ITA, getWords(" Consumo massimo di acqua: 3,9 GPM (15 LPM)"))
						.put(Lang.ESP, getWords(" Consumo máximo de agua: 3.9 GPM (15 LPM)"))
						.put(Lang.DEU, getWords(" Max Wasserverbrauch - 3,9 GPM (15 LPM)"))
						.put(Lang.FRA, getWords(" Consommation maximale d'eau - 3,9 GPM (15 LPM)")).build());
		translates
				.put(getCode("Silence requires less use of water with its exclusive water-saving design and low interior height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Тишина требует меньше использования воды с ее исключительной водосберегающей конструкцией и низкой внутренней высотой"))
						.put(Lang.ITA, getWords(" Il silenzio richiede meno uso di acqua con il suo design esclusivo per il risparmio idrico e l'altezza interna ridotta"))
						.put(Lang.ESP, getWords(" El silencio requiere menos uso de agua con su exclusivo diseño de ahorro de agua y baja altura interior"))
						.put(Lang.DEU, getWords(" Stille erfordert weniger Wasserverbrauch mit seinem exklusiven wassersparenden Design und niedriger Innenhöhe"))
						.put(Lang.FRA, getWords(" Silence nécessite moins d'eau avec son design exclusif d'économie d'eau et sa faible hauteur intérieure")).build());
		translates
				.put(getCode("Max flow rate of 4.76 GPM (18 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 4,76 GPM (18 LPM)"))
						.put(Lang.ITA, getWords(" Portata massima di 4,76 GPM (18 LPM)"))
						.put(Lang.ESP, getWords(" Caudal máximo de 4.76 GPM (18 LPM)"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 4,76 GPM (18 LPM)"))
						.put(Lang.FRA, getWords(" Débit maximum de 4,76 GPM (18 LPM)")).build());
		translates
				.put(getCode("240V 60Hz power with 20A GFCI Protected"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("240V 60Hz с защитой от 20A GFCI"))
						.put(Lang.ITA, getWords(" Alimentazione 240V 60Hz con GFCI 20A protetta"))
						.put(Lang.ESP, getWords(" Potencia de 240V 60Hz con 20 GFCI protegido"))
						.put(Lang.DEU, getWords(" 240V 60Hz Leistung mit 20A GFCI geschützt"))
						.put(Lang.FRA, getWords(" Puissance 240V 60Hz avec GFCI 20A Protégé")).build());
		translates
				.put(getCode("Wall-mounted at recommended installation of 50cm  to 70cm above user’s height"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный монтаж при рекомендуемой установке на 50 см до 70 см выше высоты пользователя"))
						.put(Lang.ITA, getWords(" Installazione a parete con installazione consigliata da 50 cm a 70 cm sopra l'altezza dell'utente"))
						.put(Lang.ESP, getWords(" Montado en la pared en la instalación recomendada de 50 cm a 70 cm por encima de la altura del usuario"))
						.put(Lang.DEU, getWords(" Wandmontage bei empfohlener Installation von 50 cm bis 70 cm über der Benutzerhöhe"))
						.put(Lang.FRA, getWords(" Mural à l'installation recommandée de 50 à 70 cm au-dessus de la hauteur de l'utilisateur")).build());
		translates
				.put(getCode("Generous 100 x 95 x 46 cm (39.5 x 37.5 x 18 in) cushion top"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Щедрая высота 100 x 95 x 46 см (39,5 x 37,5 x 18 дюймов)"))
						.put(Lang.ITA, getWords(" Parte superiore del cuscino generosa da 100 x 95 x 46 cm (39,5 x 37,5 x 18 pollici)"))
						.put(Lang.ESP, getWords(" Cojín superior generoso de 100 x 95 x 46 cm (39.5 x 37.5 x 18 in)"))
						.put(Lang.DEU, getWords(" Großzügige Kissenauflage von 100 x 95 x 46 cm (39,5 x 37,5 x 18 in)"))
						.put(Lang.FRA, getWords(" Généreux 100 x 95 x 46 cm (39,5 x 37,5 x 18 po) coussin haut")).build());
		translates
				.put(getCode("Bathtub dimensions: 69.75 L x 39.25 W x 21.25 H (in) / 177 L x 100 W x 54 H (cm)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Размеры ванны: 69,75 L x 39,25 W x 21,25 H (дюйм) / 177 L x 100 Вт x 54 H (см)"))
						.put(Lang.ITA, getWords(" Dimensioni vasca: 69,75 L x 39,25 W x 21,25 H (in) / 177 L x 100 W x 54 H (cm)"))
						.put(Lang.ESP, getWords(" Dimensiones de la bañera: 69.75 L x 39.25 W x 21.25 H (in) / 177 L x 100 W x 54 H (cm)"))
						.put(Lang.DEU, getWords(" Badewannenabmessungen: 69,75 L x 39,25 W x 21,25 H (Zoll) / 177 L x 100 W x 54 H (cm)"))
						.put(Lang.FRA, getWords(" Dimensions de la baignoire: 69,75 L x 39,25 W x 21,25 H (pouces) / 177 L x 100 L x 54 H (cm)")).build());
		translates
				.put(getCode("All massage system components are made in the USA, and UL certified"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Все компоненты системы массажа производятся в США и сертифицированы UL"))
						.put(Lang.ITA, getWords(" Tutti i componenti del sistema di massaggio sono realizzati negli Stati Uniti e certificati UL"))
						.put(Lang.ESP, getWords(" Todos los componentes del sistema de masaje están fabricados en los EE. UU. Y cuentan con la certificación UL"))
						.put(Lang.DEU, getWords(" Alle Massagesystemkomponenten werden in den USA hergestellt und UL-zertifiziert"))
						.put(Lang.FRA, getWords(" Tous les composants du système de massage sont fabriqués aux États-Unis et certifiés UL")).build());
		translates
				.put(getCode("Max flow rate of 4.7 GPM (17.9 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Максимальная скорость потока 4,7 GPM (17,9 LPM)"))
						.put(Lang.ITA, getWords(" Portata massima di 4,7 GPM (17,9 LPM)"))
						.put(Lang.ESP, getWords(" Caudal máximo de 4.7 GPM (17.9 LPM)"))
						.put(Lang.DEU, getWords(" Max. Durchflussrate von 4,7 GPM (17,9 LPM)"))
						.put(Lang.FRA, getWords(" Débit maximum de 4,7 GPM (17,9 LPM)")).build());
		translates
				.put(getCode("Matching stone-plated drain"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Соответствие каменному сливу"))
						.put(Lang.ITA, getWords(" Scarico in pietra abbinato"))
						.put(Lang.ESP, getWords(" Dren de piedra a juego"))
						.put(Lang.DEU, getWords(" Passender Ablauf mit Steinsilber"))
						.put(Lang.FRA, getWords(" Drain de pierre assorti")).build());
		translates
				.put(getCode("The second frameless wall mirror – 500 x 1100 mm / 19.75 x 43.25 in"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Второе бескаркасное зеркало - 500 x 1100 мм / 19,75 x 43,25 дюйма"))
						.put(Lang.ITA, getWords(" Il secondo specchio a parete senza cornice - 500 x 1100 mm / 19,75 x 43,25 pollici"))
						.put(Lang.ESP, getWords(" El segundo espejo de pared sin marco - 500 x 1100 mm / 19.75 x 43.25 in"))
						.put(Lang.DEU, getWords(" Der zweite rahmenlose Wandspiegel - 500 x 1100 mm / 19.75 x 43.25 in"))
						.put(Lang.FRA, getWords(" Le deuxième miroir mural sans cadre - 500 x 1100 mm / 19,75 x 43,25 po")).build());
		translates
				.put(getCode("Chrome finished solid brass construction"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Хромированная конструкция из цельной латуни"))
						.put(Lang.ITA, getWords(" Costruzione in ottone massiccio cromato"))
						.put(Lang.ESP, getWords(" Construcción de latón macizo con acabado cromado"))
						.put(Lang.DEU, getWords(" Verchromte Messingkonstruktion"))
						.put(Lang.FRA, getWords(" Construction en laiton massif fini chrome")).build());
		translates
				.put(getCode("Premium design and very comfortable freestanding bathtub"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Премиум-дизайн и очень удобная автономная ванна"))
						.put(Lang.ITA, getWords(" Design premium e vasca da bagno indipendente molto confortevole"))
						.put(Lang.ESP, getWords(" Diseño premium y bañera independiente muy confortable"))
						.put(Lang.DEU, getWords(" Premium-Design und sehr komfortable freistehende Badewanne"))
						.put(Lang.FRA, getWords(" Design haut de gamme et baignoire autoportante très confortable")).build());
		translates
				.put(getCode("Large 11.8 (300 mm) shower head for generous water flow"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Большая душевая головка 11,8 (300 мм) для большого потока воды"))
						.put(Lang.ITA, getWords(" Grande soffione da 11,8 (300 mm) per un abbondante flusso d'acqua"))
						.put(Lang.ESP, getWords(" Cabezal de ducha grande de 11.8 (300 mm) para un flujo de agua generoso"))
						.put(Lang.DEU, getWords(" Großer 11,8 (300 mm) -Duschkopf für großzügigen Wasserfluss"))
						.put(Lang.FRA, getWords(" Grande pomme de douche de 11,8 po (300 mm) pour un débit d'eau généreux")).build());
		translates
				.put(getCode("Anti-limestone nozzles"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Анти-известковые форсунки"))
						.put(Lang.ITA, getWords(" Ugelli anticalcare"))
						.put(Lang.ESP, getWords(" Boquillas anticalcáreas"))
						.put(Lang.DEU, getWords(" Antikalk-Düsen"))
						.put(Lang.FRA, getWords(" Buses anti-calcaire")).build());
		translates
				.put(getCode("Designed for outdoor/indoor use"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Предназначен для наружного и внутреннего использования"))
						.put(Lang.ITA, getWords(" Progettato per uso esterno / interno"))
						.put(Lang.ESP, getWords(" Diseñado para uso exterior / interior"))
						.put(Lang.DEU, getWords(" Entwickelt für den Außen- und Innenbereich"))
						.put(Lang.FRA, getWords(" Conçu pour une utilisation extérieure / intérieure")).build());
		translates
				.put(getCode("Korean technology, Italian design"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Корейская технология, итальянский дизайн"))
						.put(Lang.ITA, getWords(" Tecnologia coreana, design italiano"))
						.put(Lang.ESP, getWords(" Tecnología coreana, diseño italiano"))
						.put(Lang.DEU, getWords(" Koreanische Technologie, italienisches Design"))
						.put(Lang.FRA, getWords(" Technologie coréenne, design italien")).build());
		translates
				.put(getCode("Wall-mounted shower mixer with 4 outlets to function all concurrently and metal plate"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Настенный смеситель для душа с 4 выходами для работы одновременно и с металлической пластиной"))
						.put(Lang.ITA, getWords(" Miscelatore doccia a parete con 4 uscite per funzionare contemporaneamente e piastra metallica"))
						.put(Lang.ESP, getWords(" Mezclador de ducha de pared con 4 salidas para funcionar todas al mismo tiempo y placa de metal"))
						.put(Lang.DEU, getWords(" Wand-Brausemischer mit 4 Ausgängen für gleichzeitige Funktion und Metallplatte"))
						.put(Lang.FRA, getWords(" Mitigeur de douche mural avec 4 sorties pour fonctionner simultanément et plaque métallique")).build());
		translates
				.put(getCode("Includes 2 drawers –700 x 365 x 250 mm / 27.5 x 14.25 x 10 in and 500 x 365 x 250 mm / 19.75 x 14.25 x 10 in clip handles"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Включает 2 выдвижных ящика -700 x 365 x 250 мм / 27,5 x 14,25 x 10 дюймов и 500 x 365 x 250 мм / 19,75 x 14,25 x 10 в ручках клипов"))
						.put(Lang.ITA, getWords(" Include 2 cassetti -700 x 365 x 250 mm / 27,5 x 14,25 x 10 pollici e 500 x 365 x 250 mm / 19,75 x 14,25 x 10 nei manici a clip"))
						.put(Lang.ESP, getWords(" Incluye 2 gavetas -700 x 365 x 250 mm / 27.5 x 14.25 x 10 in y 500 x 365 x 250 mm / 19.75 x 14.25 x 10 en mangos de clip"))
						.put(Lang.DEU, getWords(" Enthält 2 Schubladen -700 x 365 x 250 mm / 27,5 x 14,25 x 10 Zoll und 500 x 365 x 250 mm / 19,75 x 14,25 x 10 in Clip-Griffe"))
						.put(Lang.FRA, getWords(" Comprend 2 tiroirs -700 x 365 x 250 mm / 27,5 x 14,25 x 10 po et 500 x 365 x 250 mm / 19,75 x 14,25 x 10 po")).build());
		translates
				.put(getCode("Included piezoelectrical remote control"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Включено пьезоэлектрическое дистанционное управление"))
						.put(Lang.ITA, getWords(" Telecomando piezoelettrico incluso"))
						.put(Lang.ESP, getWords(" Control remoto piezoeléctrico incluido"))
						.put(Lang.DEU, getWords(" Piezoelektrische Fernbedienung inklusive"))
						.put(Lang.FRA, getWords(" Télécommande piézoélectrique incluse")).build());
		translates
				.put(getCode("Overhead LED Eco lamp - one per mirror"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Светодиодная лампа Eco с подсветкой - по одному на зеркало"))
						.put(Lang.ITA, getWords(" Lampada ambientale a LED Eco - una per specchio"))
						.put(Lang.ESP, getWords(" Lámpara LED Eco de arriba, una por espejo"))
						.put(Lang.DEU, getWords(" Overhead LED Eco Lampe - eine pro Spiegel"))
						.put(Lang.FRA, getWords(" Lampe plafonnier LED Eco - une par miroir")).build());
		translates
				.put(getCode("Water flow rate - 4.75 GPM (18 LPM)"), ImmutableMap.<Lang, Translate> builder()
						.put(Lang.RUS, getWords("Расход воды - 4,75 GPM (18 LPM)"))
						.put(Lang.ITA, getWords(" Portata d'acqua - 4,75 GPM (18 LPM)"))
						.put(Lang.ESP, getWords(" Caudal de agua: 4.75 GPM (18 LPM)"))
						.put(Lang.DEU, getWords(" Wasserdurchflussrate - 4,75 GPM (18 LPM)"))
						.put(Lang.FRA, getWords(" Débit d'eau - 4,75 GPM (18 LPM)")).build());
	}
	
	private static String getCode(String bullet) {
		bullet = bullet.trim().toLowerCase().replace("\"", "").replaceAll("[^\\d\\w\\s\\p{L}]", " ").replaceAll("\\s+", " ");
		
		return bullet;
	}

	private static Translate getWords(String text) {
		Set<String> words = Arrays.asList(getCode(text).split("\\s+")).stream().filter(new Predicate<String>() {

			@Override
			public boolean test(String t) {
				return t.length() > 3;
			}
		}).collect(Collectors.toSet());
		return new Translate(text, words);
	}
}
