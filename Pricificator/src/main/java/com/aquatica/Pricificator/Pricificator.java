package com.aquatica.Pricificator;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Pricificator {

	public static class Product implements Comparable<Product> {
		public final String title;
		public final String upc;
		public final String sku;
		public final String slug;
		public final String baseCategory;
		public final String model;
		public final double price;
		public final String currency;
		public final Collection<Modification> modifications = new ArrayList<>();

		public Product(JSONObject attributes) {
			title = get("title", attributes);
			upc = get("upc", attributes);
			sku = get("sku", attributes);
			slug = get("slug", attributes);
			baseCategory = get("baseCategory", attributes);
			String m = get("collection", attributes).toLowerCase().trim();
			if (m.isEmpty()) {
				m = get("model", attributes).toLowerCase().trim();
			}
			if (m.isEmpty()) {
				m = get("title", attributes).toLowerCase().trim();
			}
			model = m;
			JSONObject priceObj = attributes.getJSONObject("price");
			price = priceRound(Double.parseDouble(priceObj.getString("value")));
			currency = priceObj.getString("currency");
		}

		void addModification(Modification modification) {
			modifications.add(modification);
		}

		@Override
		public String toString() {
			return upc + " - " + title + "   " + String.format("%.2f", price) + " " + currency
					+ (modifications.size() == 0 ? "" : " MODS: " + modifications);
		}

		public void packModifications() {
			boolean hasRAL = false;
			Collection<Modification> newModifications = new ArrayList<>();
			for (Modification modification : modifications) {
				if (modification.name.equals("None") || modification.cost < Double.MIN_VALUE) {
					continue;
				}
				if (modification.style.contains("wizard")) {
					continue;
				}
				if (modification.name.contains("RAL")) {
					hasRAL = true;
					continue;
				}
				newModifications.add(modification);
			}
			modifications.clear();
			modifications.addAll(newModifications);
			if (hasRAL) {
				modifications.add(RAL_COLOR);
			}
		}

		@Override
		public int compareTo(Product o) {
			int res = model.compareTo(o.model);
			if (res != 0)
				return res;
			return title.compareTo(o.title);
		}
	}

	private static final Modification RAL_COLOR = new Modification("RAL Colors", "", "RAL_COLOR", 3000);

	public static class Modification implements Comparable<Modification> {
		public final String name;
		public final String group;
		public final String code;
		public final String style;
		public final double cost;
		public int uses = 0;

		public Modification(JSONObject attributes) {
			name = get("name", attributes);
			group = get("group", attributes);
			code = get("code", attributes);
			style = get("style", attributes);
			cost = priceRound(attributes.getDouble("cost"));
		}

		public Modification(String name, String group, String code, double cost) {
			this.name = name;
			this.group = group;
			this.code = code;
			this.cost = cost;
			this.style = "";
		}

		public String getKey() {
			return group.trim() + " " + name.trim();
		}

		@Override
		public String toString() {
			return getKey();
		}

		@Override
		public int compareTo(Modification o) {
			int res = o.uses - uses;
			if (res != 0)
				return res;
			return (group + " " + name).compareTo((o.group + " " + o.name));
		}
	}

	private static String get(String key, JSONObject attributes) {
		if (attributes.isNull(key))
			return "";
		Object value = attributes.get(key);
		if (value instanceof String)
			return ((String) value).trim();
		return String.valueOf(value);
	}

	private static double priceRound(double value) {
		return Math.round(value);
	};

	private JFrame frame;
	private Map<String, List<Product>> tabs = new LinkedHashMap<>();
	private Map<String, List<Modification>> mods = new LinkedHashMap<>();
	private Map<String, DefaultTableModel> uiModels = new LinkedHashMap<>();
	private JTabbedPane tabbedPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				LOG.init();
			}
		});
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Pricificator window = new Pricificator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public Pricificator() {
		try {
			initialize();
		} catch (Exception e) {
			new RuntimeException(e);
		}
	}

	private void initialize() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(10, 0));
		JLabel lblChoiceSite = new JLabel("   Choice Site ");
		panel.add(lblChoiceSite, BorderLayout.WEST);

		final JComboBox<String> comboBox = new JComboBox<>();
		comboBox.setModel(new DefaultComboBoxModel<>(new String[] { "https://www.aquaticausa.com/",
				"https://dev.aquaticaplumbing.com/", "https://www.aquaticabath.ca/", "https://www.aquaticabath.eu/",
				"https://www.aquaticabath.co.uk/", "https://www.aquaticabano.com.mx/", "https://www.aquaticabagno.it/",
				"https://www.vivalusso.ru/" }));
		panel.add(comboBox, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.EAST);

		JButton btnCreateFile = new JButton("Create File");
		btnCreateFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							registerCert();
							String site = (String) comboBox.getSelectedItem();
							if (tabs.isEmpty()) {
								proccessSite(site);
							}
							File file = selectFile(site);
							XlsExporter.doExport(tabs, mods, file);
							openFile(file);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				t.start();
			}

		});
		panel_1.add(btnCreateFile);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

	}

	private void proccessSite(String site) throws Exception {
		tabs.clear();
		mods.clear();
		uiModels.clear();
		tabbedPane.removeAll();
		addCategory("bathtub");
		addCategory("sink");
		LOG.log("Get all products");
		JSONObject allProducts = getAllProducts(site);
		JSONArray catalog = allProducts.getJSONArray("included");
		LOG.log("Fetch " + catalog.length() + " products");
		for (int p = 0; p < catalog.length(); p++) {
			JSONObject attributes = catalog.getJSONObject(p).getJSONObject("attributes");
			Product product = new Product(attributes);
			LogValue log = LOG.log((p + 1) + "/" + catalog.length() + " Get Details: " + product.toString());
			JSONObject productDetails = getProductDetails(site, product);
			JSONArray modifications = productDetails.getJSONObject("modifications").getJSONArray("data");
			if (modifications == null) {
				continue;
			}
			for (int m = 0; m < modifications.length(); m++) {
				Modification modification = new Modification(modifications.getJSONObject(m));
				product.addModification(modification);
			}
			product.packModifications();
			log.setValue((p + 1) + "/" + catalog.length() + " Fetch : " + product.toString());

			String baseCategory = product.baseCategory.toLowerCase().trim();
			if (baseCategory.isEmpty()) {
				baseCategory = "Empty";
			}
			Collection<Product> products = tabs.get(baseCategory);
			if (products == null) {
				products = addCategory(baseCategory);
			}
			products.add(product);
			DefaultTableModel model = uiModels.get(baseCategory);
			model.addRow(new Object[] { product.upc, product.sku, product.title, product.price,
					product.modifications.toString() });
		}

		for (Entry<String, List<Product>> entry : tabs.entrySet()) {
			List<Product> products = entry.getValue();
			Collections.sort(products);
			Map<String, Modification> modMap = new HashMap<>();
			for (Product product : products) {
				for (Modification mod : product.modifications) {
					String key = mod.getKey().toLowerCase().trim();
					Modification modification = modMap.get(key);
					if (modification != null) {
						modification.uses = modification.uses + 1;
					} else {
						Modification newMod = new Modification(mod.name, mod.group, mod.code, mod.cost);
						newMod.uses = 1;
						modMap.put(key, newMod);
					}
				}
			}

			List<Modification> columns = new ArrayList<>(modMap.values());
			Collections.sort(columns);
			mods.put(entry.getKey(), columns);
		}
	}

	private Collection<Product> addCategory(String baseCategory) {
		List<Product> products = new ArrayList<>();
		tabs.put(baseCategory, products);
		mods.put(baseCategory, new ArrayList<>());
		JList<String> list = new JList<>();
		list.setModel(new DefaultListModel<String>());

		JTable table = new JTable();
		table.setFillsViewportHeight(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultTableModel model = new DefaultTableModel(new Object[0][5],
				new String[] { "UPC", "SKU", "TITLE", "PRICE", "MODs" }) {
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, Double.class, String.class };

			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		};
		table.setModel(model);
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(0).setMaxWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setMaxWidth(150);

		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(3).setMaxWidth(100);
		uiModels.put(baseCategory, model);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);

		tabbedPane.addTab(baseCategory, null, scrollPane);
		return products;
	}

	private File selectFile(String site) {
		FileDialog fileDialog = new FileDialog(frame, "Select file to export", FileDialog.SAVE);
		String pref = "Pricificator" + site.replace("https://www", "").replace(".", "_").replace("/", "");
		fileDialog.setFile(pref + "_" + new DateTime().toString("yyyy-MM-dd") + ".xls");
		fileDialog.setVisible(true);
		String filename = fileDialog.getFile();
		fileDialog.setVisible(false);

		File file = new File(fileDialog.getDirectory(), filename);
		if (file.exists()) {
			file.delete();
		}
		return file;
	}

	private void openFile(File file) {
		int result = JOptionPane.showConfirmDialog(null, "Export complete. Open file?", "Export complete.",
				JOptionPane.YES_NO_OPTION);
		if (result == JOptionPane.YES_OPTION) {
			try {
				Desktop.getDesktop().open(file);
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with open file " + file.getAbsolutePath());
			}
		}
	}

	private static JSONObject getAllProducts(String site) throws UnirestException {
		Map<String, Object> queryString = new HashMap<>();
		queryString.put("locale", "en");
		queryString.put("filter[category]", "all products");
		queryString.put("limit", "1000");
		HttpResponse<JsonNode> catalogResponse = Unirest.get(site + "api/filters/products").queryString(queryString)
				.asJson();
		return catalogResponse.getBody().getObject();
	}

	private static JSONObject getProductDetails(String site, Product product) throws UnirestException {
		HttpResponse<JsonNode> productResponse = Unirest.get(site + "api/products/" + product.slug)
				.queryString("include", "modifications").asJson();
		return productResponse.getBody().getObject();
	}

	private static void registerCert() {
		LOG.logWarning("REGISTER WHORE CERTIFICATE");
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}

					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}

					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			SSLContext sslcontext = SSLContexts.custom()
					.loadTrustMaterial(null, new TrustSelfSignedStrategy())
					.build();

			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
			CloseableHttpClient httpclient = HttpClients.custom()
					.setSSLSocketFactory(sslsf)
					.build();
			Unirest.setHttpClient(httpclient);
		} catch (GeneralSecurityException e) {
		}
	}
}
