package com.aquatica.Pricificator;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import com.aquatica.Pricificator.Pricificator.Modification;
import com.aquatica.Pricificator.Pricificator.Product;

public class XlsExporter {
	
	public static void doExport(Map<String, List<Product>> tabs, Map<String, List<Modification>> columns, File file) throws Exception {
		
		Workbook wb = new HSSFWorkbook();
		
		addCategorySheets(tabs, columns, wb);
		
		FileOutputStream fileOut = new FileOutputStream(file);
		wb.write(fileOut);
		fileOut.close();
	}
	
	public static void addCategorySheets(Map<String, List<Product>> tabs, Map<String, List<Modification>> columns, Workbook wb) throws ExecutionException {
		for (Entry<String, List<Product>> entry : tabs.entrySet()) {
			String tabName = entry.getKey();
			List<Modification> modsColumns = columns.get(tabName);
			List<Product> products = entry.getValue();
			int rowCount = products.size();
			if (rowCount < 1) {
				continue;
			}
			String currency = products.get(0).currency.toUpperCase();
			
			Sheet sheet = wb.createSheet(tabName);
			int startI = 0;
			
			Row row = sheet.createRow(1);
			header(wb, row, startI++, "UPC");
			header(wb, row, startI++, "SKU");
			header(wb, row, startI++, "Product Name");
			header(wb, row, startI++, "Price \n(" + currency + ")");
			
			for (int c = 0; c < modsColumns.size(); c++) {
				sheet.setColumnWidth(startI, 4000);
				Modification column = modsColumns.get(c);
				String name = column.name;
				if (!column.group.trim().isEmpty()) {
					name = column.group + ": \n" + name;
				}
				header(wb, row, startI++, name);
			}
			
			for (int r = 0; r < rowCount; r++) {
				startI = 0;
				row = sheet.createRow(r + 2);
				StringBuilder commentBuilder = new StringBuilder();
				Product productInfo = products.get(r);
				Collection<Modification> productModifications = productInfo.modifications;
				createCell(row, startI++, productInfo.upc, "");
				createCell(row, startI++, productInfo.sku, "");
				createCell(row, startI++, productInfo.title, "");
				createCell(row, startI++, productInfo.price, 0.0);
				for (int m = 0; m < modsColumns.size(); m++) {
					for (Modification modification : productModifications) {
						if (modification.getKey().equalsIgnoreCase(modsColumns.get(m).getKey())) {
							createCell(row, startI, modification.cost, "");
							String mName = modification.name;
							if (!modification.group.trim().isEmpty()) {
								mName = modification.group + ": " + mName;
							}
							String mPrice = Double.toString(Math.round(modification.cost + productInfo.price));
							commentBuilder.append(mPrice).append(" ").append(mName).append("\r\n");
						}
					}
					startI++;
				}
				int countModification = productModifications.size();
				String comment = commentBuilder.toString();
				if (!comment.isEmpty()) {
					setComment(wb, sheet, row, comment, countModification);
				}
			}
			
			for (int i = 0; i < 4; i++) {
				sheet.autoSizeColumn(i);
			}
		}
	}
	
	private static void setComment(Workbook wb, Sheet sheet, Row row, String commentText, int countModification) {
		CreationHelper helper = wb.getCreationHelper();
		Cell cellI = row.getCell(3);
		Drawing drawing = sheet.createDrawingPatriarch();
		
		ClientAnchor anchor = helper.createClientAnchor();
		anchor.setCol1(cellI.getColumnIndex());
		anchor.setCol2(cellI.getColumnIndex() + 6);
		anchor.setRow1(cellI.getRowIndex());
		anchor.setRow2(cellI.getRowIndex() + countModification + 3);
		
		Comment comment = drawing.createCellComment(anchor);
		comment.setString(helper.createRichTextString(commentText));
		cellI.setCellComment(comment);
	}
	
	private static void createCell(Row row, int i, Object value, Object defaultValue) {
		if (value == null) {
			createCell(row, i, defaultValue, "");
			return;
		}
		if (value instanceof String) {
			row.createCell(i).setCellValue((String) value);
		} else if (value instanceof Number) {
			row.createCell(i).setCellValue(((Number) value).doubleValue());
		} else if (value instanceof Boolean) {
			row.createCell(i).setCellValue((Boolean) value);
		} else {
			row.createCell(i).setCellValue(String.valueOf(value));
		}
	}
	
	private static void header(Workbook wb, Row row, int i, String caption) throws ExecutionException {
		Cell cell = row.createCell(i);
		cell.setCellStyle(getStyle(wb));
		cell.setCellValue(caption);
	}
	
	private static CellStyle style;
	
	private static CellStyle getStyle(Workbook wb) {
		if (style != null)
			return style;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBold(true);
		style.setFont(font);
		style.setWrapText(true);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		return style;
	}
}
