import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JProgressBar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.common.base.Splitter;

@SuppressWarnings("unused")
public class CRMLoader<T extends WebDriver & FindsByClassName> {

	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final NumberFormat formatter = new DecimalFormat("#0");

	public static void load(String home, int minI, boolean needImg, boolean needPdf, JProgressBar progressBar_1) throws Exception {
		int i = 0;
		CrmDriver driver = null;
		try {
			driver = createInnerDriver();
			String mainWindow = driver.getWindowHandle();
			LOG.logInfo("Open CRM and AUTH");
			driver.get("https://crm.aquaticagroup.com/AquaticaGroup/main.aspx");
			Thread.sleep(1000);
			LOG.logInfo("Open products");
			driver.get("https://crm.aquaticagroup.com/AquaticaGroup/_root/homepage.aspx?etc=1024&pagemode=iframe&sitemappath=SFA|SFA|nav_products");
			Thread.sleep(1000);
			List<WebElement> aa = driver.findElementsByXPath("//a[contains(@id,'primary')][@class='ms-crm-List-Link']");
			Collection<String> ids = new ArrayList<String>();
			for (WebElement a : aa) {
				String id = a.getAttribute("id");
				id = id.substring(id.indexOf('{') + 1, id.indexOf('}'));
				ids.add(id);
			}
			progressBar_1.setMaximum(ids.size());
			for (String id : ids) {
				i++;
				progressBar_1.setValue(i);
				if (i < minI) {
					continue;
				}
				String href = "https://crm.aquaticagroup.com/AquaticaGroup/userdefined/edit.aspx?etc=1024&id={" + id + "}&pagemode=iframe"; // / ONLY DATA
				// String href = "https://crm.aquaticagroup.com/AquaticaGroup/main.aspx?etn=product&id={" + id + "}&pagetype=entityrecord";
				driver.get(href);
				// driver.switchTo().frame("contentIFrame");
				String upc = driver.findElementById("productnumber").getAttribute("value");
				LOG.log("Scan " + i + "\t" + upc);
				saveImages(home, driver, upc, needImg, needPdf);
				// driver.switchTo().defaultContent();
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Product " + i);
		} finally {
			LOG.log("Loading finish");
			if (driver != null) {
				driver.close();
			}
		}
	}
	
	public static CrmDriver createInnerDriver() {
		CrmDriver driver = new CrmDriver();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		return driver;
	}

	public static RemoteWebDriver createFFDriver() {
		FirefoxProfile ffProfile = new FirefoxProfile(new File("C:/Users/Annenkov/AppData/Roaming/Mozilla/Firefox/Profiles/n0xgr86r.default"));
		RemoteWebDriver driver = new FirefoxDriver(ffProfile);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		return driver;
	}

	public static void saveImages(String home, CrmDriver driver, String upc, boolean needImg, boolean needPdf) throws Exception {
		String SKU = driver.findElementById("vendorpartnumber").getAttribute("value").replace('\\', '_');
		WebElement notes = driver.findElementById("notescontrol");
		String notesUrl = "https://crm.aquaticagroup.com" + notes.getAttribute("url");
		String base = home + "\\" + SKU;
		File fol = new File(base);
		fol.mkdirs();
		driver.get(notesUrl);
		String source = driver.getPageSource();
		List<WebElement> urls = driver.findElementsByXPath("//span[@class='attachment']");
		int imgNum = 0;
		int pdfNum = 0;
		for (int i = 0; i < urls.size(); i++) {
			WebElement urlE = driver.findElementByXPath("(//span[@class='attachment'])[" + (i + 1) + "]");
			driver.executeScript("arguments[0].scrollIntoView();", urlE);
			String name = urlE.getAttribute("title");
			name = name.substring(0, name.lastIndexOf("(", name.length() - 5));
			name = name.trim();
			String ext = name.substring(name.lastIndexOf(".")).toLowerCase();
			boolean isPDF = ext.equalsIgnoreCase(".pdf");
			boolean isImg = ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".jpeg");
			if (!isPDF && !isImg) {
				continue;
			}
			if (isImg && !needImg) {
				continue;
			}
			if (isPDF && !needPdf) {
				continue;
			}
			int num = 0;
			if (isImg) {
				imgNum++;
				num = imgNum;
				ext = ".jpg";
			}
			if (isPDF) {
				pdfNum++;
				num = pdfNum;
				ext = ".pdf";
			}
			File newFile = new File(fol, upc + "-" + (num < 10 ? "0" : "") + num + ext);
			driver.setClosesNewFile(newFile);
			urlE.click();
			driver.getSilent(notesUrl);
		}
	}
}
