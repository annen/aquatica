import javax.swing.DefaultListModel;

public class LogValue {
	public static LogValue NULL = new LogValue(null) {
		@Override
		public void setValue(String value) {
		};
	};

	String value;
	private DefaultListModel<LogValue> model;
	private int index;

	public LogValue(String value) {
		this.value = value;

	}

	public void setValue(String value) {
		this.value = LogFrame.prepareValue(value);
		if (model != null) {
			model.setElementAt(this, index);
		}
	}

	public void addTo(DefaultListModel<LogValue> model) {
		this.model = model;
		index = model.size();
		model.addElement(this);
	}

	@Override
	public String toString() {
		return value;
	}

}