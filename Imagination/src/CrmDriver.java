
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;

public class CrmDriver extends HtmlUnitDriver {
	
	private File closesNewFile;
	
	public CrmDriver() {
		super(BrowserVersion.FIREFOX_38, true);
		// setProxy("127.0.0.1", 9999);
		WebClient webClient = getWebClient();
		setSillentListener();
	}
	
	private void setSillentListener() {
		WebClient webClient = getWebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.setIncorrectnessListener(new IncorrectnessListener() {
			@Override
			public void notify(String arg0, Object arg1) {
			}
		});
		webClient.setCssErrorHandler(new org.w3c.css.sac.ErrorHandler() {
			
			@Override
			public void warning(CSSParseException arg0) throws CSSException {
			
			}
			
			@Override
			public void fatalError(CSSParseException arg0) throws CSSException {
			}
			
			@Override
			public void error(CSSParseException arg0) throws CSSException {
			}
		});
		webClient.setJavaScriptErrorListener(new JavaScriptErrorListener() {
			
			@Override
			public void loadScriptError(InteractivePage arg0, URL arg1, Exception arg2) {
			}
			
			@Override
			public void malformedScriptURL(InteractivePage arg0, String arg1, MalformedURLException arg2) {
			}
			
			@Override
			public void scriptException(InteractivePage arg0, ScriptException arg1) {
			}
			
			@Override
			public void timeoutError(InteractivePage arg0, long arg1, long arg2) {
			}
		});
		webClient.setHTMLParserListener(new HTMLParserListener() {
			
			@Override
			public void error(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
			
			@Override
			public void warning(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
		});
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
	}
	
	@Override
	public WebClient getWebClient() {
		return super.getWebClient();
	}
	
	@Override
	public void get(String url) {
		if (url.equalsIgnoreCase("about:blank")) {
			super.get(url);
			LOG.logInfo("Bot browser loaded");
			return;
		}
		LogValue logValue = LOG.logInfo("Loading " + url);
		long time = System.currentTimeMillis();
		super.get(url);
		time = System.currentTimeMillis() - time;
		logValue.setValue("INFO: Loaded at " + time / 1000.0 + " sec. " + url);
	}

	public void closeSources() {
		WebClient webClient = getWebClient();
		Collection<WebWindow> windows = webClient.getWebWindows();
		for (WebWindow ww : windows) {
			if (ww != getCurrentWindow()) {
				if (ww instanceof FrameWindow) {
					try {
						((FrameWindow) ww).close();
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with close bots window");
					}
				}
			} else {
				ww.getJobManager().removeAllJobs();
				ww.getJobManager().shutdown();
			}
		}
	}

	// This is the magic. Keep a reference to the client instance
	@Override
	protected WebClient modifyWebClient(WebClient client) {
		
		ConfirmHandler okHandler = new ConfirmHandler() {
			@Override
			public boolean handleConfirm(Page page, String message) {
				return true;
			}
		};
		client.setConfirmHandler(okHandler);
		
		client.addWebWindowListener(new WebWindowListener() {
			
			@Override
			public void webWindowOpened(WebWindowEvent event) {
			}
			
			@Override
			public void webWindowContentChanged(WebWindowEvent event) {
				
				WebResponse response = event.getWebWindow().getEnclosedPage().getWebResponse();
				int statusCode = response.getStatusCode();
				String contentType = response.getContentType();
				if (statusCode == 200 && contentType.equalsIgnoreCase("application/octet-stream")) {
					if (closesNewFile != null) {
						getFileResponse(response);
						closesNewFile = null;
					} else {
						LOG.logError("Unexpected web file");
					}
				}
			}
			
			@Override
			public void webWindowClosed(WebWindowEvent event) {
			}
		});
		return client;
	}
	
	private void getFileResponse(WebResponse response) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = response.getContentAsStream();
			outputStream = new FileOutputStream(closesNewFile);
			int read = 0;
			byte[] bytes = new byte[1024];
			
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			LOG.logSuccess("Loaded " + closesNewFile.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setClosesNewFile(File closesNewFile) {
		this.closesNewFile = closesNewFile;
	}
	
	public void getSilent(String url) {
		super.get(url);
	}
}
