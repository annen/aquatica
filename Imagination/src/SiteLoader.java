import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JProgressBar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

@SuppressWarnings("unused")
public class SiteLoader<T extends WebDriver & FindsByClassName> {
	
	public static class Config {
		
		private boolean isUpc;
		private boolean isSku;
		private boolean isName;
		private boolean isList;
		private Map<String, String> map = new HashMap<>();
		
		public Config(boolean isUpc, boolean isSku, boolean isName, boolean isList, Map<String, String> map) {
			this.isUpc = isUpc;
			this.isSku = isSku;
			this.isName = isName;
			this.isList = isList;
			this.map = map;
		}
		
	}
	
	private static final String CHROME_WEBDRIVER_PATH = "webdriver.chrome.driver";
	private static final String DEFAULT_DRIVER_PATH = "D:/Aquatica/IMAGES/chromedriver.exe";
	
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final NumberFormat formatter = new DecimalFormat("#0");
	private static final Joiner tabJoiner = Joiner.on("\t");
	
	public static void load(String home, String host, int minI, boolean needImg, boolean needPdf, JProgressBar progressBar_1,
			Config folderConfig, Config fileConfig, boolean needWB) throws Exception {
		if (host == null || host.isEmpty())
			return;
		if (!host.endsWith("/")) {
			host += "/";
		}
		registerCert(host);
		checkSaving(host + "images/b2t.png");
		
		int i = 0;
		ChromeDriver driver = null;
		try {
			driver = createDriver(host);
			LOG.logInfo("Open browser");
			LOG.logInfo("Open site");
			driver.get(host + "category/all");
			int sec = 1;
			List<WebElement> aa = driver.findElementsByCssSelector(".catalog-product .info");
			
			while (aa.isEmpty()) {
				LOG.logInfo("Wait " + sec + " seconds");
				Thread.sleep(1000);
				sec++;
				aa = driver.findElementsByCssSelector(".catalog-product .info");
				if (sec > 60) {
					break;
				}
			}
			Thread.sleep(1000);
			aa = driver.findElementsByCssSelector(".catalog-product .info");
			Map<String, Collection<String>> urls = new LinkedHashMap<>();
			LogValue log = LOG.log("Scan urls: " + urls.size());
			for (WebElement a : aa) {
				String href = a.getAttribute("href");
				urls.put(host + href.substring(1), new ArrayList<String>());
				log.setValue("Scan urls: " + urls.size());
			}
			progressBar_1.setMaximum(urls.size());
			for (String url : urls.keySet()) {
				i++;
				progressBar_1.setValue(i);
				log = LOG.log("Processed " + url);
				if (i < minI) {
					continue;
				}
				driver.get(url);
				
				sec = 0;
				List<WebElement> img = driver.findElementsByCssSelector("img.fotorama__img");
				while (img.isEmpty()) {
					LOG.logInfo("Wait " + sec + " seconds");
					Thread.sleep(1000);
					sec++;
					img = driver.findElementsByCssSelector("img.fotorama__img");
					if (sec > 30) {
						break;
					}
				}
				
				String upc = "";
				String sku = "";
				String name = "";
				List<WebElement> metaTags = driver.findElementsByXPath("//meta[contains(@property,':upc')]");
				if (metaTags.isEmpty()) {
					log.setValue("WARNING: Not found UPC on page " + url);
					continue;
				}
				upc = metaTags.get(0).getAttribute("content");
				metaTags = driver.findElementsByXPath("//meta[contains(@property,':mfr_part_no')]");
				if (metaTags.isEmpty()) {
					log.setValue("WARNING: Not found SKU on page " + url);
					sku = upc;
				} else {
					sku = metaTags.get(0).getAttribute("content");
				}
				
				metaTags = driver.findElementsByXPath("//h1[contains(@class,'productTitle')]");
				if (metaTags.isEmpty()) {
					log.setValue("WARNING: Product title not found SKU on page " + url);
					name = sku;
				} else {
					name = metaTags.get(0).getText();
				}
				upc = getValidName(upc);
				sku = getValidName(sku);
				name = getValidName(name);
				if (upc.isEmpty()) {
					upc = "unknown";
				}
				if (sku.isEmpty()) {
					sku = "unknown";
				}
				if (name.isEmpty()) {
					name = "unknown";
				}
				Collection<String> files = saveFiles(home, driver, upc, sku, name, needImg, needPdf, folderConfig, fileConfig,
						needWB);
				urls.get(url).addAll(files);
				log.setValue("SUCCESS: " + url);
			}
			StringBuilder info = new StringBuilder();
			for (Entry<String, Collection<String>> entry : urls.entrySet()) {
				info.append(entry.getKey()).append("\t").append(tabJoiner.join(entry.getValue())).append("\r\n");
				
			}
			TextViewFrame textViewFrame = new TextViewFrame();
			textViewFrame.textArea.setText(info.toString());
			textViewFrame.setVisible(true);
		} catch (Exception ex) {
			LOG.logError(ex, "Product " + i);
		} finally {
			LOG.log("Loading finish");
			if (driver != null) {
				driver.close();
			}
		}
	}
	
	private static boolean checkSaving(String url) {
		try (InputStream imgUrl = new URL(url).openStream()) {
			File tempFile = File.createTempFile("aq_", String.valueOf(System.currentTimeMillis()));
			tempFile.delete();
			Files.copy(imgUrl, tempFile.toPath());
			tempFile.delete();
			return true;
		} catch (Exception e) {
			LOG.logError(e, "Save Failed");
			return false;
		}
	}
	
	private static void registerCert(String host) {
		LOG.logWarning("REGISTER WHORE CERTIFICATE");
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}
					
					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					
					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};
		
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
		}
	}
	
	public static ChromeDriver createDriver(String host) {
		String driverPath = System.getProperty(CHROME_WEBDRIVER_PATH);
		File localDriver = new File("./chromedriver.exe");
		if (localDriver.exists()) {
			driverPath = localDriver.getAbsolutePath();
		}
		if (driverPath == null || driverPath.isEmpty()) {
			LOG.logInfo("Set driverPath as " + DEFAULT_DRIVER_PATH);
			driverPath = DEFAULT_DRIVER_PATH;
		}
		System.setProperty(CHROME_WEBDRIVER_PATH, driverPath);
		LOG.logInfo("Used webdriver.chrome.driver as " + driverPath);
		DesiredCapabilities cap = new DesiredCapabilities();
		if (host.contains("vivaluso")) {
			String PROXY = "95.85.6.74:1234";
			LOG.logInfo("Apply PROXY " + PROXY);

			org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
			proxy.setHttpProxy(PROXY)
					.setFtpProxy(PROXY)
					.setSslProxy(PROXY);
			cap.setCapability(CapabilityType.PROXY, proxy);
		}
		ChromeDriver driver = new ChromeDriver(cap);
		driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		return driver;
	}
	
	public static Collection<String> saveFiles(String home, ChromeDriver driver, String upc, String sku, String name,
			boolean needImg, boolean needPdf, Config folderConfig, Config fileConfig, boolean needWB) throws Exception {
		String base = home + "\\" + generatePrefix(upc, sku, name, folderConfig);
		File folder = new File(base);
		folder.mkdirs();
		
		Collection<String> result = new ArrayList<>();
		if (needImg) {
			result.addAll(getImg(driver, folder, upc, sku, name, fileConfig, home, needWB));
		}
		
		if (needPdf) {
			result.addAll(getPdf(driver, folder, upc, sku, name, fileConfig));
		}
		return result;
	}
	
	public static Collection<String> getImg(ChromeDriver driver, File folder, String upc, String sku, String name, Config fileConfig,
			String home, boolean needWB) {
		prepareImages(driver);
		Collection<String> result = new ArrayList<>();
		int imgNum = 1;
		String ext = ".jpg";
		String filePrefix = generatePrefix(upc, sku, name, fileConfig);
		
		if (needWB) {
			File whiteFile = new File(new File(home).getParentFile(), "\\white\\" + upc + ".jpg");
			if (whiteFile.exists()) {
				try {
					String fileName = createFileName(filePrefix, imgNum, ext);
					FileUtils.copyFile(whiteFile, new File(folder.getPath() + "\\" + fileName));
					LOG.log("Copied WB image " + fileName);
					imgNum++;
				} catch (Exception e) {
					LOG.logError(e, "Error while copying WB image");
				}
			} else {
				LOG.logInfo("WB image is not found");
			}
		}
		
		List<WebElement> imgHolderElements = driver.findElementsByCssSelector(".fotorama__thumb");
		List<WebElement> imgElements = driver.findElementsByCssSelector(".fotorama__thumb img");
		if (imgHolderElements.size() > imgElements.size()) {
			prepareImages(driver);
			imgElements = driver.findElementsByCssSelector(".fotorama__thumb img");
			if (imgHolderElements.size() > imgElements.size()) {
				LOG.logWarning("Not all images");
			}
		}
		if (imgElements.isEmpty()) {
			imgElements = driver.findElementsByCssSelector(".fotorama__img");
		}
		HashSet<String> imgUrls = new HashSet<>();
		for (WebElement imgElement : imgElements) {
			String url = imgElement.getAttribute("src");
			url = url.replaceAll("/cache/images/[^/]+/aquatica", "/image/aquatica");
			int extIndex = url.lastIndexOf('.');
			if (extIndex > 0) {
				ext = url.substring(extIndex).toLowerCase();
			}
			if (!imgUrls.contains(url)) {
				try (InputStream imgUrl = new URL(url).openStream()) {
					String fileName = createFileName(filePrefix, imgNum, ext);
					new File(folder + "/" + fileName).delete();
					Path imgPath = Paths.get(folder + "/" + fileName);
					Files.deleteIfExists(imgPath);
					Files.copy(imgUrl, imgPath);
					LOG.log("Saved image " + fileName);
					imgUrls.add(url);
					imgNum++;
					result.add(url);
				} catch (Exception e) {
					LOG.logError(e, "Error while saving " + url);
				}
			}
		}
		return result;
	}
	
	private static void prepareImages(ChromeDriver driver) {
		try {
			driver.executeScript("$('#loader').remove(); return 1;");
			driver.executeScript("$('.modal-header button').click(); return 1;");
			Actions builder = new Actions(driver);
			builder.moveToElement(driver.findElementByCssSelector(".fotorama__stage")).perform();
			sleep1s();
			int i = 0;
			List<WebElement> nextArrow = driver.findElementsByCssSelector(".fotorama__arr--next:not(.fotorama__arr--disabled)");
			while (nextArrow.size() > 0) {
				nextArrow.get(0).click();
				i++;
				nextArrow = driver.findElementsByCssSelector(".fotorama__arr--next:not(.fotorama__arr--disabled)");
				if (i % 8 == 0) {
					sleep1s();
					sleep1s();
					sleep1s();
				}
			}
		} catch (Exception ex) {
			prepareImages(driver);
		}
		
	}
	
	private static void sleep1s() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
		}
	}
	
	public static Collection<String> getPdf(ChromeDriver driver, File fol, String upc, String sku, String name, Config fileConfig) {
		Collection<String> result = new ArrayList<>();
		int pdfNum = 1;
		String filePrefix = generatePrefix(upc, sku, name, fileConfig);
		String ext = ".pdf";
		List<WebElement> pdfElements = driver.findElementsByXPath("//div[contains(@class,'documentationRow')]//a[contains(@class,'downld')]");
		if (pdfElements.size() == 0) {
			LOG.logInfo("Pdf files is not exist");
		}
		for (WebElement pdfElement : pdfElements) {
			if (pdfElement != null) {
				String url = pdfElement.getAttribute("href");
				boolean isPDF = url.contains(".pdf");
				if (isPDF) {
					try (InputStream pdfUrl = new URL(url).openStream()) {
						String fileName = createFileName(filePrefix, pdfNum, ext);
						Files.copy(pdfUrl, Paths.get(fol + "/" + fileName));
						LOG.log("Saved pdf file " + fileName);
						pdfNum++;
						result.add(url);
					} catch (Exception e) {
						LOG.logError(e, "Error while saving " + url);
					}
				}
			}
		}
		return result;
	}
	
	private static String generatePrefix(String upc, String sku, String name, Config config) {
		if (config.isUpc)
			return upc;

		if (config.isSku)
			return sku;

		if (config.isName)
			return name;

		if (config.isList) {
			if (config.map == null || !config.map.containsKey(upc))
				return upc;
			return config.map.get(upc);
		}
		return upc;
	}
	
	private static String getValidName(String value) {
		value = value.trim().replaceAll("\\s+", " ").replaceAll("-+", "-").trim();
		StringBuilder sb = new StringBuilder(value);
		for (int i = 0; i < sb.length();) {
			if (wrongLetter(sb.charAt(i))) {
				sb.deleteCharAt(i);
			} else {
				i++;
			}
		}
		return sb.toString();
	}
	
	private static boolean wrongLetter(char letter) {
		if (letter == 32 || // (
				letter == 40 || // (
				letter == 41 || // )
				letter == 45 || // -
				letter == 46 || // .
				letter == 95 || // _
				(letter >= 48 && letter <= 57) || // 0-9
				(letter >= 65 && letter <= 90) || // A-Z
				(letter >= 97 && letter <= 122) || // a-z
				(letter >= 'А' && letter <= 'Я') ||
				(letter >= 'а' && letter <= 'я'))
			return false;
		return true;
	}
	
	private static String createFileName(String filePrefix, int imgNum, String ext) {
		return filePrefix + "-" + (imgNum < 10 ? "0" : "") + imgNum + ext;
	}
}
