import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ImportConfig extends JFrame {
	
	private JPanel contentPane;
	public JTextArea folderSetupTA;
	public JTextArea fileSetupTA;
	public JComboBox folderBox;
	public JComboBox fileBox;
	private JComboBox<String> txtPath;
	
	/**
	 * Create the frame.
	 */
	public ImportConfig(final String home, final int minI, final JProgressBar progressBar) {
		setTitle("Rebuild Config");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		String[] setups = { "By UPC", "By SKU", "By Name", "By MatchList" };
		setBounds(100, 100, 340, 470);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		ActionListener actionListenerForFolderSetup = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isList((String) folderBox.getSelectedItem(), folderSetupTA);
			}
		};
		
		ActionListener actionListenerForFileSetup = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isList((String) fileBox.getSelectedItem(), fileSetupTA);
			}
		};
		
		final JCheckBox chckbxImg = new JCheckBox("images");
		chckbxImg.setSelected(true);
		
		final JCheckBox chckbxPdf = new JCheckBox("pdf");
		chckbxPdf.setSelected(true);
		
		final JCheckBox chckbxWB = new JCheckBox("Add White Background  Images");
		chckbxWB.setSelected(false);
		
		txtPath = new JComboBox<>();
		txtPath.setModel(new DefaultComboBoxModel<>(new String[] { "https://www.aquaticausa.com/",
				"https://dev.aquaticaplumbing.com/", "https://www.aquaticabath.ca/", "https://www.aquaticabath.eu/",
				"https://www.aquaticabath.co.uk/", "https://www.aquaticabano.com.mx/", "https://www.aquaticabagno.it/",
				"https://www.vivalusso.ru/" }));
		
		JLabel dirLabel = new JLabel("Site Host:");
		dirLabel.setVerticalAlignment(JLabel.TOP);
		dirLabel.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel resourceTypeLabel = new JLabel("Resource Type:");
		resourceTypeLabel.setVerticalAlignment(JLabel.TOP);
		resourceTypeLabel.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel folderLabel = new JLabel("Folder setup:");
		resourceTypeLabel.setVerticalAlignment(JLabel.TOP);
		resourceTypeLabel.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel fileLabel = new JLabel("Files  setup:");
		resourceTypeLabel.setVerticalAlignment(JLabel.TOP);
		resourceTypeLabel.setHorizontalAlignment(JLabel.CENTER);
		resourceTypeLabel.setVerticalAlignment(JLabel.TOP);
		resourceTypeLabel.setHorizontalAlignment(JLabel.CENTER);
		folderBox = new JComboBox(setups);
		fileBox = new JComboBox(setups);
		folderBox.setMaximumSize(new Dimension(50, 20));
		fileBox.setMaximumSize(new Dimension(50, 20));
		folderSetupTA = new JTextArea(null, 10, 60);
		fileSetupTA = new JTextArea(null, 10, 60);
		folderSetupTA.setVisible(false);
		fileSetupTA.setVisible(false);
		folderBox.addActionListener(actionListenerForFolderSetup);
		fileBox.addActionListener(actionListenerForFileSetup);

		final JButton btnStart = new JButton("START");
		
		btnStart.setBackground(Color.CYAN);
		
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnStart.setEnabled(false);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							SiteLoader.Config folderConfig = getSetup((String) folderBox.getSelectedItem(), folderSetupTA);
							SiteLoader.Config fileConfig = getSetup((String) fileBox.getSelectedItem(), fileSetupTA);
							
							SiteLoader.load(home, (String) txtPath.getSelectedItem(), minI, chckbxImg.isSelected(), chckbxPdf.isSelected(), progressBar, folderConfig, fileConfig, chckbxWB.isSelected());
							btnStart.setEnabled(true);
						} catch (Exception e) {
							LOG.logError(e, "Problem with get CRM Images");
							btnStart.setEnabled(true);
						}
					}
				});
				t.setDaemon(true);
				t.start();
			}
		});
		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(320, 420));
		// panel_1.setSize(500, 1000);
		scrollPane.setViewportView(panel_1);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1
				.setHorizontalGroup(
						gl_panel_1
								.createParallelGroup(
										Alignment.LEADING)
								.addGroup(
										gl_panel_1.createSequentialGroup()
												.addGroup(
														gl_panel_1.createParallelGroup(Alignment.LEADING)
																.addGroup(gl_panel_1.createSequentialGroup().addComponent(resourceTypeLabel).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(chckbxImg).addGap(18).addComponent(chckbxPdf))
																.addGroup(
																		gl_panel_1.createSequentialGroup().addComponent(folderLabel).addGap(18).addComponent(folderBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																.addGroup(gl_panel_1.createSequentialGroup().addComponent(fileLabel).addGap(18).addComponent(fileBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																.addGroup(gl_panel_1.createSequentialGroup().addContainerGap().addComponent(chckbxWB))
																.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING).addComponent(btnStart, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(fileSetupTA, Alignment.LEADING, GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																.addGroup(
																		gl_panel_1
																				.createParallelGroup(Alignment.TRAILING, false).addGroup(gl_panel_1.createSequentialGroup().addComponent(dirLabel).addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																						.addComponent(txtPath, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE))
																				.addComponent(folderSetupTA, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
												.addGap(326)));
		gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup().addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE).addComponent(dirLabel).addComponent(txtPath, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.CENTER).addComponent(resourceTypeLabel).addComponent(chckbxImg).addComponent(chckbxPdf))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE).addComponent(folderLabel).addComponent(folderBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addGap(6)
						.addComponent(folderSetupTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE).addComponent(fileLabel).addComponent(fileBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addGap(6)
						.addComponent(fileSetupTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(3).addComponent(chckbxWB).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnStart, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(573)));
		gl_panel_1.setAutoCreateGaps(true);
		gl_panel_1.setAutoCreateContainerGaps(true);
		
		panel_1.setLayout(gl_panel_1);
		
	}
	
	private static SiteLoader.Config getSetup(String setup, JTextArea textArea) {
		Map<String, String> map = new HashMap<>();
		if (setup == "By UPC")
			return new SiteLoader.Config(true, false, false, false, null);

		if (setup == "By SKU")
			return new SiteLoader.Config(false, true, false, false, null);

		if (setup == "By Name")
			return new SiteLoader.Config(false, false, true, false, null);

		if (setup == "By MatchList") {
			String[] text = textArea.getText().split("\n");
			for (String element : text) {
				String[] keys = element.split("\t");
				map.put(keys[0], keys[1]);
			}
			return new SiteLoader.Config(false, false, false, true, map);
		}
		
		return new SiteLoader.Config(true, false, false, false, null);
	}
	
	private static void isList(String setup, JTextArea textArea) {
		if (setup == "By MatchList") {
			textArea.setVisible(true);
		} else {
			textArea.setVisible(false);
		}
	}
}
