import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.imgscalr.Scalr;

import com.google.common.io.Files;

@SuppressWarnings("rawtypes")
public class Imagination extends JFrame {
	
	private ImageList imageList;
	private JTextField txtPath;
	private JProgressBar progressBar;
	private JList<String> foldersList;
	private TextViewFrame textViewFrame = new TextViewFrame();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Imagination frame = new Imagination();
					frame.setVisible(true);
					LOG.init();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Imagination() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setTitle("Imagination");
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		imageList = new ImageList();
		JScrollPane scrollPane = new JScrollPane(imageList);
		getContentPane().add(scrollPane);
		
		progressBar = new JProgressBar();
		getContentPane().add(progressBar, BorderLayout.SOUTH);
		
		JScrollPane scrollPane_folders = new JScrollPane();
		getContentPane().add(scrollPane_folders, BorderLayout.WEST);
		foldersList = new JList<String>();
		foldersList.setFont(new Font("Serif", Font.PLAIN, 14));
		foldersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		foldersList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int index = foldersList.getSelectedIndex();
				if (index < 0)
					return;
				imageList.setBackground(Color.LIGHT_GRAY);
				imageList.clearSelection();
				DefaultListModel model = (DefaultListModel) foldersList.getModel();
				String folder = (String) model.getElementAt(index);
				File[] files = new File(txtPath.getText() + "\\" + folder).listFiles();
				imageList.rebuildModel(files);
			}
		});
		scrollPane_folders.setViewportView(foldersList);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setSize(1000, 50);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		txtPath = new JTextField();
		txtPath.setText("D:\\AQUATICA\\IMAGES\\WORK");
		panel.add(txtPath);
		txtPath.setColumns(30);
		
		JButton btnBuild = new JButton("Scan");
		btnBuild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				buildList(foldersList);
			}
		});
		panel.add(btnBuild);
		
		JLabel lblDiv = new JLabel("        ");
		panel.add(lblDiv);
		
		JButton btnRename = new JButton("RENAME");
		btnRename.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				renameAll();
				final ListModel<ListElement> model = imageList.getModel();
				imageList.setModel(new DefaultListModel<ListElement>());
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
						}
						imageList.setModel(model);
					}
				});
				t.start();
			}
		});
		panel.add(btnRename);
		
		JLabel label = new JLabel("        ");
		panel.add(label);
		
		JButton btnTabview = new JButton("TabView");
		btnTabview.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Map<String, List<File>> grImages = new LinkedHashMap<String, List<File>>();
				Map<String, List<File>> grPdf = new LinkedHashMap<String, List<File>>();
				List<File> listFolders = new ArrayList<File>();
				listFolders.add(new File(txtPath.getText()));
				while (!listFolders.isEmpty()) {
					File home = listFolders.get(0);
					listFolders.remove(0);
					File[] listFiles = home.listFiles();
					Arrays.sort(listFiles, new Comparator<File>() {
						
						@Override
						public int compare(File arg0, File arg1) {
							return arg0.getName().compareTo(arg1.getName());
						}
					});
					Collection<File> tempFiles = new ArrayList<File>();
					int i_img = 0;
					int i_pdf = 0;
					for (File file : listFiles) {
						if (file.isDirectory()) {
							listFolders.add(file);
							continue;
						}
						if (file.getName().toLowerCase().contains("thumbs")) {
							continue;
						}
						String name = file.getName();
						String ext = name.substring(name.lastIndexOf('.'));
						boolean isIMG = ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".png") || ext.equalsIgnoreCase(".jpeg") || ext.equalsIgnoreCase(".gif");
						name = name.substring(0, name.length() - ext.length());
						String upc = name;
						if (name.contains("-")) {
							upc = name.substring(0, name.lastIndexOf("-"));
						}
						if (!upc.startsWith("6277") && !!upc.startsWith("6477")) {
							upc = findUpc(listFiles, upc);
						}
						List<File> list = grImages.get(upc);
						if (list == null) {
							list = new ArrayList<File>();
							grImages.put(upc, list);
						}
						list = grPdf.get(upc);
						if (list == null) {
							list = new ArrayList<File>();
							grPdf.put(upc, list);
						}
						int needIndex = 0;
						if (isIMG) {
							needIndex = ++i_img;
						} else {
							needIndex = ++i_pdf;
							
						}
						
						File needFile = new File(file.getParentFile(), upc + "-" + (needIndex < 10 ? "0" + needIndex : needIndex) + ext);
						if (!file.getName().equalsIgnoreCase(needFile.getName())) {
							File tempFile = new File(file.getParentFile(), "_" + needFile.getName());
							tempFiles.add(tempFile);
							try {
								Files.move(file, tempFile);
								file = needFile;
							} catch (IOException e) {
								LOG.logError(e, "Problem with rename " + file.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
								return;
							}
						}
						Map<String, List<File>> map = isIMG ? grImages : grPdf;
						list = map.get(upc);
						list.add(file);
					}
					
					for (File tempFile : tempFiles) {
						File needFile = new File(tempFile.getParentFile(), tempFile.getName().substring(1));
						try {
							Files.move(tempFile, needFile);
						} catch (IOException e) {
							LOG.logError(e, "Problem with rename " + tempFile.getAbsolutePath() + " to " + needFile.getAbsolutePath());
						}
					}
				}
				int maxImages = 0;
				for (Entry<String, List<File>> g : grImages.entrySet()) {
					if (maxImages < g.getValue().size()) {
						maxImages = g.getValue().size();
					}
				}
				
				StringBuilder sb = new StringBuilder();
				for (Entry<String, List<File>> g : grImages.entrySet()) {
					sb = sb.append(g.getKey()).append('\t');
					int i = 0;
					for (File file : g.getValue()) {
						sb = sb.append(file.getName()).append('\t');
						i++;
					}
					for (int j = i; j <= maxImages; j++) {
						sb = sb.append('\t');
					}
					for (File file : grPdf.get(g.getKey())) {
						sb = sb.append(file.getName()).append('\t');
						i++;
					}
					sb = sb.append("\r\n");
				}
				textViewFrame.textArea.setText(sb.toString());
				textViewFrame.setVisible(true);
			}
		});
		panel.add(btnTabview);
		
		JLabel label_1 = new JLabel("        ");
		panel.add(label_1);
		
		final JButton btnReduild = new JButton("REBUILD");
		
		btnReduild.setBackground(Color.CYAN);
		panel.add(btnReduild);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		final JSpinner spinner = new JSpinner();
		
		final JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setStringPainted(true);
		progressBar_1.setMaximum(1);
		progressBar_1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				spinner.setValue(progressBar_1.getValue());
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(progressBar_1, GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
								.addContainerGap()));
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(progressBar_1, GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
										.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addContainerGap()));
		panel_1.setLayout(gl_panel_1);
		btnReduild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnReduild.setEnabled(false);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							ImportConfig configFrame = new ImportConfig(txtPath.getText(), (int) spinner.getValue(), progressBar_1);
							configFrame.setVisible(true);
							// SiteLoader.load(txtPath.getText(), (int) spinner.getValue(), chckbxImg.isSelected(), chckbxPdf.isSelected(), progressBar_1,
							// new SiteLoader.Config(false, true, false, null),
							// new SiteLoader.Config(true, false, false, null));
							btnReduild.setEnabled(true);
						} catch (Exception e) {
							LOG.logError(e, "Problem with get CRM Images");
							btnReduild.setEnabled(true);
						}
					}
				});
				t.setDaemon(true);
				t.start();
			}
		});
		buildList(foldersList);
	}
	
	public void buildList(JList<String> foldersList) {
		final DefaultListModel<String> model = new DefaultListModel<String>();
		((DefaultListModel) imageList.getModel()).clear();
		File home = new File(txtPath.getText());
		if (!home.exists() || !home.isDirectory()) {
			int result = JOptionPane.showConfirmDialog(null, "Invalid Home folder " + home.getAbsolutePath() + ".\r\n I am create it?", "Invalid Home folder", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.CANCEL_OPTION)
				return;
			home.mkdirs();
		}
		File[] listFiles = home.listFiles();

		for (File file : listFiles) {
			if (file.isDirectory()) {
				model.addElement(file.getName());
			}
		}
		foldersList.setModel(model);
		if (!model.isEmpty()) {
			foldersList.setSelectedIndex(0);
		}
	}
	
	public void renameAll() {
		final DefaultListModel model = (DefaultListModel) imageList.getModel();
		int i = 1;
		Collection<ListElement> tempFiles = new ArrayList<ListElement>();
		for (Enumeration elements = model.elements(); elements.hasMoreElements();) {
			ListElement item = (ListElement) elements.nextElement();
			File file = item.file;
			String name = file.getName();
			String ext = name.substring(name.lastIndexOf('.'));
			name = name.substring(0, name.length() - ext.length());
			if (name.charAt(name.length() - 3) == '-') {
				name = name.substring(0, name.length() - 3);
			}
			if (!name.startsWith("6277")) {
				name = findUpc(file.getParentFile().listFiles(), name);
			}
			File tempFile = new File(file.getParentFile(), "_" + name + "-" + (i < 10 ? "0" + i : i) + ext);
			try {
				Files.move(file, tempFile);
			} catch (IOException e) {
				LOG.logError(e, "Problem with rename " + file.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
			}
			item.file = tempFile;
			tempFiles.add(item);
			i++;
		}
		for (ListElement item : tempFiles) {
			File tempFile = new File(item.file.getParentFile(), item.file.getName().substring(1));
			try {
				Files.move(item.file, tempFile);
			} catch (IOException e) {
				LOG.logError(e, "Problem with rename " + item.file.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
			}
			item.file = tempFile;
		}
	}
	
	private String findUpc(File[] listFiles, String fileName) {
		Map<String, Integer> upcList = new HashMap<String, Integer>();
		for (File file : listFiles) {
			String name = file.getName();
			int index = name.lastIndexOf('.');
			if (index > 0) {
				String ext = name.substring(index);
				name = name.substring(0, name.length() - ext.length());
			}
			index = name.lastIndexOf('-');
			if (name.length() - index == 3) {
				name = name.substring(0, name.length() - 3);
			}
			if (name.startsWith("6277") || name.startsWith("647")) {
				Integer upcCount = upcList.get(name);
				if (upcCount == null) {
					upcCount = 0;
				}
				upcCount++;
				upcList.put(name, upcCount);
			}
		}
		if (upcList.size() == 0) {
			LOG.logError("Not found UPC in files " + Arrays.toString(listFiles));
			return fileName;
		}
		int max = 0;
		String maxUpc = "";
		for (Entry<String, Integer> upc : upcList.entrySet()) {
			Integer upcCount = upc.getValue();
			if (upcCount > max) {
				max = upcCount;
				maxUpc = upc.getKey();
			}
		}
		LOG.log("Rename " + fileName + " to " + maxUpc);
		return maxUpc;
	}
	
	private void moveNext() {
		int index = foldersList.getSelectedIndex();
		if (index < foldersList.getModel().getSize()) {
			foldersList.setSelectedIndex(index + 1);
		}
	}
	
	class ImageList extends JList<ListElement> {
		public ImageList() {
			setModel(new DefaultListModel<ListElement>());
			setFixedCellWidth(155);
			setVisibleRowCount(0);
			setLayoutOrientation(JList.HORIZONTAL_WRAP);
			setDragEnabled(true);
			setDropMode(DropMode.ON_OR_INSERT);
			setTransferHandler(new ImageListTransferHandler());
			
			addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
				
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyChar() == '' && e.getModifiers() == 2) {
						int index = getSelectedIndex();
						if (index >= 0) {
							ListElement le = getModel().getElementAt(index);
							le.file.delete();
							((DefaultListModel) getModel()).remove(index);
						}
					}
					if (e.getKeyCode() == 10 && e.getModifiers() == 2) {
						renameAll();
						moveNext();
					}
					if (e.getKeyCode() == 39 && e.getModifiers() == 2) {
						moveNext();
					}
					if (e.getKeyCode() == 37 && e.getModifiers() == 2) {
						int index = foldersList.getSelectedIndex();
						if (index > 0) {
							foldersList.setSelectedIndex(index - 1);
						}
					}
				}
			});
		}
		
		public void rebuildModel(final File[] imageFiles) {
			ImageList.this.setBackground(Color.LIGHT_GRAY);
			Arrays.sort(imageFiles, new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			final DefaultListModel<ListElement> model = new DefaultListModel<ListElement>();
			progressBar.setMaximum(imageFiles.length);
			progressBar.setValue(0);
			progressBar.setVisible(true);
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					for (File f : imageFiles) {
						try {
							if (f.getName().toLowerCase().endsWith(".jpg") || f.getName().toLowerCase().endsWith(".jpeg")) {
								// BufferedInputStream fis = new BufferedInputStream(new FileInputStream(f));
								BufferedImage image = ImageIO.read(f);
								if (image == null) {
									JOptionPane.showMessageDialog(null, "Problem with file " + f);
								}
								BufferedImage resizeImage = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, 150, 100, Scalr.OP_ANTIALIAS);
								ListElement imageIcon = new ListElement(resizeImage, f);
								model.addElement(imageIcon);
								// fis.close();
							}
							progressBar.setValue(progressBar.getValue() + 1);
						} catch (Exception e) {
							LOG.logError(e, "Problem with file " + f);
						}
					}
					progressBar.setVisible(false);
					setModel(model);
					ImageList.this.setBackground(Color.WHITE);
				}
			});
			t.setDaemon(true);
			t.start();
		}
	}
	
	class ListElement extends ImageIcon {
		public File file;
		
		public ListElement(Image resizedImage, File file) {
			super(resizedImage);
			this.file = file;
		}
		
		@Override
		public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
			super.paintIcon(c, g, x, y);
			
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, getWidth(), 20);
			g.setColor(Color.BLACK);
			g.drawString(file.getName(), 5, 15);
		}
	}
	
	class ImageListTransferHandler extends TransferHandler {
		// Support for drag
		
		@Override
		public int getSourceActions(JComponent source) {
			return COPY_OR_MOVE;
		}
		
		@Override
		protected Transferable createTransferable(JComponent source) {
			JList list = (JList) source;
			int index = list.getSelectedIndex();
			if (index < 0)
				return null;
			ImageIcon icon = (ImageIcon) list.getModel().getElementAt(index);
			return new ImageTransferable(icon);
		}
		
		@Override
		protected void exportDone(JComponent source, Transferable data, int action) {
			if (action == MOVE) {
				JList list = (JList) source;
				int index = list.getSelectedIndex();
				if (index < 0)
					return;
				DefaultListModel model = (DefaultListModel) list.getModel();
				model.remove(index);
			}
		}
		
		// Support for drop
		
		@Override
		public boolean canImport(TransferSupport support) {
			if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
				return false;
			return ((JList.DropLocation) support.getDropLocation()).isInsert();
		}
		
		@Override
		public boolean importData(TransferSupport support) {
			ImageList list = (ImageList) support.getComponent();
			DefaultListModel<ListElement> model = (DefaultListModel<ListElement>) list.getModel();
			
			Transferable transferable = support.getTransferable();
			List<DataFlavor> flavors = Arrays.asList(transferable.getTransferDataFlavors());
			
			List<ListElement> images = new ArrayList<ListElement>();
			
			try {
				if (flavors.contains(DataFlavor.imageFlavor)) {
					images.add((ListElement) transferable.getTransferData(DataFlavor.imageFlavor));
				}
				
				int index;
				if (support.isDrop()) {
					JList.DropLocation location = (JList.DropLocation) support.getDropLocation();
					index = location.getIndex();
					if (!location.isInsert()) {
						model.remove(index); // replace location
					}
				} else {
					index = model.size();
				}
				for (ListElement image : images) {
					model.add(index, image);
					index++;
				}
				return true;
			} catch (IOException ex) {
				return false;
			} catch (UnsupportedFlavorException ex) {
				return false;
			}
		}
	}
	
	/*
	 * This program is a part of the companion code for Core Java 8th ed. (http://horstmann.com/corejava) This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
	 * option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General
	 * Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
	 */
	
	/**
	 * This class is a wrapper for the data transfer of image objects.
	 */
	class ImageTransferable implements Transferable {
		/**
		 * Constructs the selection.
		 *
		 * @param image
		 *           an image
		 */
		private ImageIcon theImage;
		
		public ImageTransferable(ImageIcon image) {
			theImage = image;
		}
		
		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { DataFlavor.imageFlavor };
		}
		
		@Override
		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return flavor.equals(DataFlavor.imageFlavor);
		}
		
		@Override
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
			if (flavor.equals(DataFlavor.imageFlavor))
				return theImage;
			else
				throw new UnsupportedFlavorException(flavor);
		}
	}
}
