package com.Aquatica.drivers;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;

import com.Aquatica.window.Settings;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;

public class CrmDataDriver extends AquaticaDriver {
	
	public CrmDataDriver() {
		DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) getWebClient().getCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new NTCredentials(Settings.get(Settings.CRM_USER), Settings.get(Settings.CRM_PASSWORD), "localhost", Settings.get(Settings.CRM_DOMAIN)));
	}
}
