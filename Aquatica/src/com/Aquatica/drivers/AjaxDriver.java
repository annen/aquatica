package com.Aquatica.drivers;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;

public class AjaxDriver extends AquaticaDriver {

	private final WebClient webClient;

	public AjaxDriver() {
		webClient = getWebClient();
		webClient.addRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
	}

	@Override
	protected void setConnectionWrapper() {
	}

	public String getAjax(String url) {
		getSilent(url);
		return getPageSource();
	}

	public byte[] download(String url) throws Exception {
		Page page = webClient.getPage(url);
		WebResponse webResponse = page.getWebResponse();
		InputStream iStream = webResponse.getContentAsStream();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[1024];

		while ((nRead = iStream.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
}
