package com.Aquatica.drivers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;

import com.Aquatica.window.LOG;
import com.Aquatica.window.LogFrame.LogValue;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.IncorrectnessListener;
import com.gargoylesoftware.htmlunit.InteractivePage;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.StringWebResponse;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import com.gargoylesoftware.htmlunit.util.WebConnectionWrapper;

public class AquaticaDriver extends HtmlUnitDriver {
	
	public AquaticaDriver() {
		super(BrowserVersion.FIREFOX_38, true);
		//setProxy("127.0.0.1", 8888);
		setSillentListener();
		setConnectionWrapper();
	}
	
	protected void setConnectionWrapper() {
		getWebClient().setWebConnection(new WebConnectionWrapper(getWebClient()) {
			
			@Override
			public WebResponse getResponse(WebRequest request) throws IOException {
				URL url = request.getUrl();
				String urlString = url.toString().toLowerCase();
				if (urlString.startsWith("http://www.hayneedle.com/ga") && urlString.endsWith(".js")) {
					// return new StringWebResponse("", url);
				}
				if (url.getFile().endsWith(".jpg") || url.getFile().endsWith(".jpeg") || url.getFile().endsWith(".gif") || url.getFile().endsWith(".png"))
					return new StringWebResponse("", url);
				return super.getResponse(request);
			}
		});
	}
	
	private void setSillentListener() {
		WebClient webClient = getWebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.setIncorrectnessListener(new IncorrectnessListener() {
			@Override
			public void notify(String arg0, Object arg1) {
			}
		});
		webClient.setCssErrorHandler(new org.w3c.css.sac.ErrorHandler() {
			
			@Override
			public void warning(CSSParseException arg0) throws CSSException {

			}
			
			@Override
			public void fatalError(CSSParseException arg0) throws CSSException {
			}
			
			@Override
			public void error(CSSParseException arg0) throws CSSException {
			}
		});
		webClient.setJavaScriptErrorListener(new JavaScriptErrorListener() {
			
			@Override
			public void loadScriptError(InteractivePage arg0, URL arg1, Exception arg2) {
			}
			
			@Override
			public void malformedScriptURL(InteractivePage arg0, String arg1, MalformedURLException arg2) {
			}
			
			@Override
			public void scriptException(InteractivePage arg0, ScriptException arg1) {
			}
			
			@Override
			public void timeoutError(InteractivePage arg0, long arg1, long arg2) {
			}
		});
		webClient.setHTMLParserListener(new HTMLParserListener() {
			
			@Override
			public void error(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
			
			@Override
			public void warning(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
		});
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
	}
	
	@Override
	public WebClient getWebClient() {
		return super.getWebClient();
	}
	
	@Override
	public void get(String url) {
		closeSources();
		if (url.equalsIgnoreCase("about:blank")) {
			super.get(url);
			LOG.logInfo("Bot browser loaded");
			return;
		}
		LogValue logValue = LOG.logInfo("Loading " + url);
		long time = System.currentTimeMillis();
		super.get(url);
		time = System.currentTimeMillis() - time;
		logValue.setValue("INFO: Loaded at " + time / 1000.0 + " sec. " + url);
	}
	
	public void closeSources() {
		WebClient webClient = getWebClient();
		Collection<WebWindow> windows = webClient.getWebWindows();
		for (WebWindow ww : windows) {
			if (ww != getCurrentWindow()) {
				if (ww instanceof FrameWindow) {
					try {
						((FrameWindow) ww).close();
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with close bots window");
					}
				}
			} else {
				ww.getJobManager().removeAllJobs();
				ww.getJobManager().shutdown();
			}
		}
	}
	
	public void getSilent(String url) {
		super.get(url);
	}
}
