package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class TubzSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.tubz.com/aquatica-bath.htm";
	
	private static final Pattern sizePattern = Pattern.compile("x\\s*([\\d\\.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern pricePattern = Pattern.compile("[a-z]* \\$([\\d.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern paramsPattern = Pattern.compile("Bathtub Size: (.*?)<br>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public TubzSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			
			List<WebElement> divs = mainDriver.findElementsByXPath("//ul[contains(@class, 'bathtub')]//li//..//a");
			
			for (WebElement div : divs) {
				String href = div.getAttribute("href");
				String title = div.getText();
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
				// if (result.foundItems.size() < 48) {
				// continue;
				// }
				for (Item item : getDetails(href, detailsDriver)) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			int numOptionBlock = -1;
			int numOption = -1;
			boolean multiPage = false;
			List<WebElement> options = detailsDriver.findElementsByXPath("//div[contains(@class, 'border3')]/ul[contains(@class, 'bath')]//li");
			List<WebElement> multiProductPage = detailsDriver.findElementsByXPath("//div[contains(@class,'border3')]");
			
			if (options.isEmpty()) {
				Item item = createItem(href, detailsDriver, numOptionBlock, multiPage, numOption);
				result.add(item);
				return result;
			}
			
			if (!options.isEmpty() && multiProductPage.size() < 2) {
				for (numOptionBlock = 0; numOptionBlock < options.size(); numOptionBlock++) {
					Item item = createItem(href, detailsDriver, numOptionBlock, multiPage, numOption);
					result.add(item);
				}
			}
			
			if (multiProductPage.size() > 1) {
				multiPage = true;
				for (numOptionBlock = 0; numOptionBlock < multiProductPage.size(); numOptionBlock++) {
					List<WebElement> blockOptions = detailsDriver.findElements(By.xpath("//div[contains(@class,'border3')][" + (numOptionBlock + 1) + "]//ul[contains(@class, 'bath')]//li"));
					if (blockOptions.size() > 0) {
						for (numOption = 0; numOption < blockOptions.size(); numOption++) {
							Item item = createItem(href, detailsDriver, numOptionBlock, multiPage, numOption);
							result.add(item);
						}
						numOption = -1;
					} else {
						Item item = createItem(href, detailsDriver, numOptionBlock, multiPage, numOption);
						result.add(item);
					}
				}
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver, int numOptionBlock, boolean multiPage, int numOption) throws Exception {
		WebElement div = null;
		String realTitle = "";
		String infoHtml = "";
		if (numOptionBlock == -1) {
			div = detailsDriver.findElement(By.xpath("//div[contains(@class,'content')]"));
			infoHtml = (String) detailsDriver.executeScript("return document.getElementsByClassName('content')[0].innerHTML;");
		} else if (!multiPage) {
			div = detailsDriver.findElements(By.xpath("//div[contains(@class, 'border3')]/ul[contains(@class, 'bath')]//li")).get(numOptionBlock);
			infoHtml = div.getText();
		} else if (multiPage && numOption > -1) {
			div = detailsDriver.findElements(By.xpath("//div[contains(@class,'border3')][" + (numOptionBlock + 1) + "]//ul[contains(@class, 'bath')]//li")).get(numOption);
			infoHtml = div.getText();
		} else if (multiPage && numOption == -1) {
			div = detailsDriver.findElement(By.xpath("//div[contains(@class,'border3')][" + (numOptionBlock + 1) + "]"));
			infoHtml = div.getText();
		} else {
			div = detailsDriver.findElements(By.xpath("//div[contains(@class,'border2')]")).get(numOptionBlock);
			infoHtml = div.getText();
		}
		if (!multiPage) {
			realTitle = detailsDriver.findElement(By.xpath("//div[contains(@class,'content')]//h2")).getText().trim();
		} else {
			realTitle = detailsDriver.findElement(By.xpath("//div[contains(@class,'border3')][" + (numOptionBlock + 1) + "]//h3")).getText().trim();
		}
		String testPrice = null;
		Matcher matcherInfo = pricePattern.matcher(infoHtml.replace(",", ""));
		if (matcherInfo.find()) {
			testPrice = matcherInfo.group(1);
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		if (numOptionBlock == -1) {
			item.SKU = div.findElement(By.xpath("//div[contains(@class, 'border3')]//strong[contains(@class,'blue')]")).getText().trim();
		} else {
			List<WebElement> skuElements = div.findElements(By.className("blue"));
			if(!skuElements.isEmpty()){
				item.SKU = skuElements.get(0).getText().trim();
			} else {
				item.SKU = div.findElements(By.className("large6")).get(0).getText().trim();
			}
		}
		item.qty = 0;
		if (numOptionBlock == -1) {
			List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'sp-wrap')]//img"));
			if (!imgs.isEmpty()) {
				for (WebElement img : imgs) {
					item.images.add(img.getAttribute("src"));
				}
			}
		} else if (!multiPage || numOption > -1) {
			String image = div.findElement(By.xpath("//div[contains(@class,'sp-wrap')]//img")).getAttribute("src");
			List<WebElement> currentImg = div.findElements(By.tagName("img"));
			if (!currentImg.isEmpty()) {
				String imgAlt = div.findElement(By.tagName("img")).getAttribute("alt");
				if (!imgAlt.equals("PDF Download")) {
					image = div.findElement(By.tagName("img")).getAttribute("src");
				}
			}
			item.images.add(image);
		} else {
			List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'border3')][" + (numOptionBlock + 1) + "]//div[contains(@class,'sp-wrap')]//img"));
			if (!imgs.isEmpty()) {
				for (WebElement img : imgs) {
					item.images.add(img.getAttribute("src"));
				}
			}
		}
		
		item.UPC = getUPC(item, detailsDriver, item.SKU);
		
		String paramHtml = "";
		if (!multiPage) {
			paramHtml = (String) detailsDriver.executeScript("return document.getElementsByClassName('row')[0].innerHTML;");
			if (paramHtml == null) {
				paramHtml = (String) detailsDriver.executeScript("return document.getElementsByClassName('bathtub')[0].innerHTML;");
			}
		} else {
			paramHtml = (String) detailsDriver.executeScript("return document.getElementsByClassName('border3')[" + numOptionBlock + "].innerHTML;");
		}
		if (paramHtml.isEmpty()) {
			LOG.log("size is empty");
			item.length = tryParseDouble(0.0, null);
			item.width = tryParseDouble(0.0, null);
			item.height = tryParseDouble(0.0, null);
		} else {
			String size = "";
			Matcher matcher = paramsPattern.matcher(paramHtml);
			if (matcher.find()) {
				size = matcher.group(1);
			}
			// for (String param : params) {
			// if (param.contains("Bathtub Size:")) {
			// size = param.replace("\"", "").replace("Bathtub Size:",
			// "").replace("<p>", "").trim();
			// break;
			// }
			// }
			if (size.isEmpty()) {
				item.length = tryParseDouble(0.0, null);
				item.width = tryParseDouble(0.0, null);
				item.height = tryParseDouble(0.0, null);
			} else {
				size = Utils.replaceSizeAdditions("x " + size);
				Matcher matcherSize = sizePattern.matcher(size);
				List<String> sizeParams = new ArrayList<String>();
				while (matcherSize.find()) {
					sizeParams.add(matcherSize.group(1));
				}
				item.length = tryParseDouble(sizeParams.get(0), null);
				item.width = tryParseDouble(sizeParams.get(1), null);
				item.height = tryParseDouble(sizeParams.get(2), null);
			}
		}
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver, String sku) {
		String upcs = sku;
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "Tubz";
	}
}
