package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.Aquatica.Utils.sleep3Sec;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;
import com.google.common.base.Strings;

public class BluebathSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.bluebath.com/bath.html#/?Manufacturers=Aquatica";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public BluebathSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		Map<String, String> productList = new LinkedHashMap<String, String>();
		
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> selectionsBlock = newMainDriver.findElements(By.xpath("//div[contains(@class,'nxt-refine-selection')]"));
				int lastCount = 0;
				while (selectionsBlock.isEmpty()) {
					if (lastCount == 20) {
						break;
					}
					LOG.logInfo("Waiting products load");
					sleep3Sec();
					lastCount++;
					selectionsBlock = newMainDriver.findElements(By.xpath("//div[contains(@class,'nxt-refine-selection')]"));
				}
				
				List<WebElement> divs = newMainDriver.findElements(By.xpath(".//li[contains(@class,'item last')]"));
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//h2[contains(@class,'product-name')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					productList.put(href, title);
				}
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath(".//a[contains(@class,'next i-next')]"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(1);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					String nextpage = nextElement.getAttribute("href").trim().replace("https://www.bluebath.com/bath.html?Manufacturers=Aquatica", "");
					urlNextPage = MAIN_PAGE + nextpage;
					newMainDriver.get(urlNextPage);
					sleep3Sec();
				}
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			result.foundItems.add(product.getValue());
			LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
			// if (result.foundItems.size() < 73) {
			// continue;
			// }
			
			Collection<Item> itemsAq = getDetails(product.getKey(), newMainDriver);
			for (Item item : itemsAq) {
				result.addItem(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			
			String title = getTextIfExist(detailsDriver, "//span[contains(@class,'h1')]");
			if (title.isEmpty()) {
				LOG.logWarning("Product not found");
				return result;
			}
			
			List<WebElement> options = detailsDriver.findElements(By.xpath("//select[contains(@class,'required-entry super-attribute-select')]/option"));
			Double finishPrice = 0.0;
			String finish = "";
			if (options.isEmpty()) {
				Item item = createItem(href, "", 0.0, detailsDriver);
				result.add(item);
				
				return result;
			}
			String[] valuesMas;
			for (WebElement option : options) {
				String text = option.getText();
				String value = option.getAttribute("value");
				if (Strings.isNullOrEmpty(value)) {
					continue;
				}
				text = text.substring(0, text.indexOf("(")).trim();
				valuesMas = text.split("\\$");
				finish = valuesMas[0].trim();
				finishPrice = Utils.tryParseDouble(valuesMas[1], 0.0);
				Item item = createItem(href, finish, finishPrice, detailsDriver);
				result.add(item);
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String finish, double finishPrice, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'main')]"));
		String realTitle = div.findElement(By.xpath("//span[contains(@class,'h1')]")).getText().trim();
		if (!Strings.isNullOrEmpty(finish)) {
			realTitle = realTitle + " -> " + finish;
		}
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'sale-price-box')]/div"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		if (finishPrice == 0.0) {
			item.price = tryParseDouble(testPrice, null);
		} else {
			item.price = tryParseDouble(finishPrice, null);
		}
		item.SKU = div.findElement(By.xpath("//li[contains(@class,'ourId')]")).getText().trim().replace("OUR ID: ", "");
		
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//a[contains(@class,'thumb-link lytebox')]/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		
		item.UPC = getUPC(item, detailsDriver, finish);
		
		String l = "0", h = "0", w = "0";
		List<WebElement> elementL = div.findElements(By.xpath("//tr[contains(.,'Length')]"));
		List<WebElement> elementW = div.findElements(By.xpath("//tr[contains(.,'Width')]"));
		List<WebElement> elementH = div.findElements(By.xpath("//tr[contains(.,'Height')]"));
		
		for (WebElement element : elementL) {
			if (!element.getText().contains("Ship") && !element.getText().contains("Inner")) {
				l = element.getText();
			}
		}
		for (WebElement element : elementW) {
			if (!element.getText().contains("Ship") && !element.getText().contains("Inner")) {
				w = element.getText();
			}
		}
		for (WebElement element : elementH) {
			if (!element.getText().contains("Ship") && !element.getText().contains("to Overflow") && !element.getText().contains("Inner")) {
				h = element.getText();
			}
		}
		
		String size = "x " + l.trim().replace("Length ", "").replace("Exterior ", "") + " L x " + w.trim().replace("Width ", "").replace("Exterior ", "") + " W x " + h.trim().replace("Height ", "").replace("Exterior ", "") + " H";
		size = size.toLowerCase().trim().replace(" 1/4", ".25").replace(" 3/4", ".75").replace(" 1/2", ".5").replace(" 1/3", ".3").replace(" 1/5", ".2").replace(" 2/5", ".4").replace(" 3/5", ".6").replace(" 4/5", ".8").replace(" 2/3", ".6").replace(" 5/8", ".62").replace(" 8/9", ".9").replace(" 7/9", ".8").replace(" 1/10", ".1").replace(" 7/8", ".87").replace(" 3/8", ".37").replace(" 1/8", ".13");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver, String finish) {
		String upcs = "";
		List<WebElement> counts = detailsDriver.findElements(By.xpath("//p[contains(.,'" + finish + "')]//span[contains(@class,'upc')]"));
		if (counts.size() == 1) {
			upcs = getTextIfExist(detailsDriver, "//li[contains(@class,'upc')]").replace("upc:", "").trim();
			if (!Strings.isNullOrEmpty(finish)) {
				upcs = getTextIfExist(detailsDriver, "//p[contains(.,'" + finish + "')]//span[contains(@class,'upc')]").replace("upc:", "").trim();
			}
		} else {
			upcs = getTextIfExist(detailsDriver, "//div[contains(@class,'tab-content')]//ul//li[contains(@class, 'upc')]").replace("upc:", "").trim();
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "Bluebath";
	}
}
