package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class ModernBathroomSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.modernbathroom.com/search.aspx?keyword=aquatica";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public ModernBathroomSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		Map<String, String> productList = new LinkedHashMap<String, String>();
		
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			String urlNextPage;
			do {
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//div[contains(@class,'SingleProductDisplayPanel')]"));
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//div[contains(@class,'SingleProductDisplayName')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					productList.put(href, title);
				}
				
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//li[contains(@id,'NextListItem')]/a"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(1);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
					sleep3Sec();
				}
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			result.foundItems.add(product.getValue());
			LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
			// if (result.foundItems.size() < 74) {
			// continue;
			// }
			Collection<Item> itemsAq = getDetails(product.getKey(), newMainDriver);
			for (Item item : itemsAq) {
				result.addItem(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
			WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id,'productpage')]"));
			// String brand =
			// div.findElement(By.xpath(("//a[contains(@id,'hplBrand')]"))).getText();
			// if(!brand.contains("Aquatica")){
			// LOG.log("Brand not Aquatica");
			// return result;
			// }
			List<WebElement> options = detailsDriver.findElements(By.xpath("//select[contains(@id,'ChoiceList')]/option"));
			
			if (options.isEmpty()) {
				Item item = createItem(href, "", detailsDriver);
				result.add(item);
				return result;
			}
			
			Collection<String> masSizes = new ArrayList<String>();
			List<WebElement> masElementsSize = div.findElements(By.xpath("//span[contains(@itemprop,'description')]/ul/li[contains(.,'Dimensions:')]"));
			if (masElementsSize.size() > 1) {
				for (WebElement element : masElementsSize) {
					masSizes.add(element.getText());
				}
			}
			Iterator<String> iter = masSizes.iterator();
			
			Collection<String> values = new ArrayList<String>();
			for (WebElement option : options) {
				if (!option.getText().equals("- select -")) {
					String value = option.getAttribute("value");
					values.add(value);
				}
			}
			
			for (String value : values) {
				String oldSku = detailsDriver.findElement(By.xpath("//span[contains(@itemprop,'sku')]")).getText();
				String newSku = oldSku;
				WebElement selectBlock = detailsDriver.findElement(By.xpath("//select[contains(@id,'ChoiceList')]"));
				// detailsDriver.executeScript("$(arguments[0]).fireEvent('change');",
				// selectBlock.get(0));
				// detailsDriver.findElementByXPath("//option[@value='" + value
				// + "']").click();
				Select select = new Select(selectBlock);
				select.selectByValue(value);
				int i = 0;
				do {
					sleep1Sec();
					i++;
					if (i == 20) {
						LOG.logError("Problem with page " + href);
						break;
					}
					newSku = detailsDriver.findElement(By.xpath("//span[contains(@itemprop,'sku')]")).getText();
				} while (oldSku.equals(newSku));
				String finishSize = "";
				if (iter.hasNext())
					finishSize = iter.next();
				Item item = createItem(href, finishSize, detailsDriver);
				result.add(item);
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String finishSize, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id,'productpage')]"));
		String realTitle = div.findElement(By.xpath("//span[contains(@itemprop,'name')]")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//span[contains(@class,'SitePrice')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.xpath("//span[contains(@itemprop,'sku')]")).getText();
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//a[contains(@id,'thumbAnchor')]/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		
		item.UPC = getUPC(item, detailsDriver);
		
		if (!finishSize.isEmpty()) {
			String size = finishSize.toLowerCase().trim().replace("inches", "").replace("Overall Dimensions:", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "").replace(":", " x ").replace("Dimensions:", "").replace("Length", "l");
			size = "x " + size;
			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		} else {
			String size = getTextIfExist(detailsDriver, "//span[contains(@itemprop,'description')]/ul/li[contains(.,'Dimensions:')]");
			if (size.isEmpty()) {
				LOG.logWarning("Not found size");
			} else {
				size = size.toLowerCase().trim().replace("inches", "").replace("Overall Dimensions:", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "").replace(":", " x ").replace("Dimensions:", "").replace("Length", "l");
				size = "x " + size;
				item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
				item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
				item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
			}
		}
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver) {
		String info = getTextIfExist(detailsDriver, "//li/span[1][contains(.,'UPC')]/../span[2]");
		return item.resolveUPC(info, null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "ModernBathroom";
	}
}
