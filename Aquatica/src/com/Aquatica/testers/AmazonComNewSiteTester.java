package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class AmazonComNewSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.amazon.com/s/?rh=n:228013,p_89:Aquatica";
	
	private static final Pattern qtyPattern = Pattern.compile("Only\\s+(\\d+)\\s+left\\s+in\\s+stock", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*[H|D]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private static final Pattern WFSizeInfoPattern = Pattern.compile("initialize[^\\{]+\\{\\s+var\\s+iframeContent\\s+=\\s+\"([^\"]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern WFSizePattern = Pattern.compile("Length.+Side: -([\\d\\.]+)\".+Width.*Back: -([\\d\\.]+)\".*Depth.*Bottom: -([\\d\\.]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public AmazonComNewSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		setCookie(detailsDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElements(By.xpath("//li[contains(@class,'s-result-item')][not(contains(@class,'placeholder'))]"));
				if (divs.isEmpty()) {
					LOG.logError("Sorry I am Blocked :( UA:" + mainDriver.getWebClient().getBrowserVersion().getUserAgent());
					if (listener != null) {
						listener.allFinish(result);
					}
					return result;
				}
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//a[contains(@class,'s-access-detail-page')]"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 7) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsById("pagnNextLink");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			Collection<String> sellerTitle = new ArrayList<String>();
			Map<String, String> sellerPrice = new HashMap<String, String>();
			detailsDriver.get(href);
			WebElement newLink = detailsDriver.findElement(By.xpath("//div[contains(@id,'olp_feature_div')]//a"));
			String newHref = newLink.getAttribute("href");
			
			WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
			List<WebElement> options = div.findElements(By.xpath(".//ul[contains(@class,'swatches')]/li"));
			
			if (options.isEmpty() || options.get(0).getText().trim().contains("Without expert installation") || options.get(0).getText().trim().contains("Without expert setup")) {
				detailsDriver.get(newHref);
				Thread.sleep(2000);
				List<WebElement> sellerBlocks = detailsDriver.findElements(By.xpath(".//div[contains(@class,'a-row a-spacing-mini olpOffer')]"));
				for (WebElement seller : sellerBlocks) {
					List<WebElement> imageTitle = seller.findElements(By.xpath(".//h3[contains(@class,'olpSellerName')]//img"));
					if (!imageTitle.isEmpty()) {
						sellerTitle.add(imageTitle.get(0).getAttribute("alt"));
						sellerPrice.put(imageTitle.get(0).getAttribute("alt"), seller.findElement(By.xpath(".//div[contains(@class,'olpPriceColumn')]/span")).getText());
					} else {
						sellerTitle.add(seller.findElement(By.xpath(".//h3[contains(@class,'olpSellerName')]//a")).getText());
						sellerPrice.put(seller.findElement(By.xpath(".//h3[contains(@class,'olpSellerName')]//a")).getText(), seller.findElement(By.xpath(".//div[contains(@class,'olpPriceColumn')]/span")).getText());
					}
				}
				detailsDriver.get(href);
				Thread.sleep(2000);
				for (String title : sellerTitle) {
					Item item = createItem(href, detailsDriver, title, sellerPrice.get(title));
					result.add(item);
				}
				
			} else {
				Collection<String> selectedAsins = new ArrayList<String>();
				for (WebElement option : options) {
					String value = option.getAttribute("data-defaultasin");
					selectedAsins.add(value);
				}
				
				for (String asin : selectedAsins) {
					detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
					String nameButton = detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button//..//span").getText();
					int i = 0;
					do {
						Thread.sleep(1000);
						String sku = div.findElement(By.id("ASIN")).getAttribute("value");
						if (asin.toLowerCase().trim().equalsIgnoreCase(sku.toLowerCase().trim())) {
							break;
						}
						if (i % 25 == 0) {
							// detailsDriver.get(href);
							LOG.log("Container loading");
							div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
						}
						if (i % 45 == 0) {
							detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
						}
						i++;
					} while (i < 105);
					if (i == 105) {
						LOG.logError("Problem with page " + href + " change to asin " + asin);
					}
					detailsDriver.get(newHref);
					Thread.sleep(1000);
					detailsDriver.findElement(By.xpath("//li[contains(@class,'a-spacing-mini')]//..//a[contains(.,'" + nameButton + "')]")).click();
					Thread.sleep(1000);
					List<WebElement> sellerBlocks = detailsDriver.findElements(By.xpath(".//div[contains(@class,'a-row a-spacing-mini olpOffer')]"));
					for (WebElement seller : sellerBlocks) {
						List<WebElement> imageTitle = seller.findElements(By.xpath(".//h3[contains(@class,'olpSellerName')]//img"));
						if (!imageTitle.isEmpty()) {
							sellerTitle.add(imageTitle.get(0).getAttribute("alt"));
							sellerPrice.put(imageTitle.get(0).getAttribute("alt"), seller.findElement(By.xpath(".//div[contains(@class,'olpPriceColumn')]/span")).getText());
						} else {
							sellerTitle.add(seller.findElement(By.xpath(".//h3[contains(@class,'olpSellerName')]//a")).getText());
							sellerPrice.put(seller.findElement(By.xpath(".//h3[contains(@class,'olpSellerName')]//a")).getText(), seller.findElement(By.xpath(".//div[contains(@class,'olpPriceColumn')]/span")).getText());
						}
					}
					detailsDriver.get(href);
					Thread.sleep(500);
					detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
					Thread.sleep(500);
					div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
					i = 0;
					do {
						Thread.sleep(1000);
						String sku = div.findElement(By.id("ASIN")).getAttribute("value");
						if (asin.toLowerCase().trim().equalsIgnoreCase(sku.toLowerCase().trim())) {
							break;
						}
						if (i % 25 == 0) {
							// detailsDriver.get(href);
							LOG.log("Container loaded");
							div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
						}
						if (i % 45 == 0) {
							detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
						}
						i++;
					} while (i < 105);
					if (i == 105) {
						LOG.logError("Problem with page " + href + " change to asin " + asin);
					}
					
					for (String title : sellerTitle) {
						Item item = createItem(href, detailsDriver, title, sellerPrice.get(title));
						result.add(item);
					}
					sellerTitle.clear();
					sellerPrice.clear();
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver, String sellerTitle, String sellerPrice) throws Exception {
		Thread.sleep(2000);
		WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
		String realTitle = sellerTitle + " -> " + div.findElement(By.id("productTitle")).getText().trim();
		String testPrice = "";
		if (!sellerPrice.isEmpty()) {
			testPrice = sellerPrice;
		} else {
			List<WebElement> priceblock = div.findElements(By.id("priceblock_ourprice"));
			if (!priceblock.isEmpty()) {
				testPrice = priceblock.get(0).getText();
			} else {
				priceblock = div.findElements(By.xpath("//span[@class='a-color-price']"));
				if (!priceblock.isEmpty()) {
					testPrice = priceblock.get(0).getText();
				}
			}
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = div.findElement(By.id("ASIN")).getAttribute("value");
		if (!div.findElements(By.id("quantity")).isEmpty()) {
			item.qty = div.findElement(By.id("quantity")).findElements(By.tagName("option")).size();
		} else {
			List<WebElement> count = div.findElements(By.xpath("//span[contains(@class,'a-color-success')][contains(.,'left in stock')]"));
			if (!count.isEmpty()) {
				Matcher matcher = qtyPattern.matcher(count.get(0).getText());
				if (matcher.find()) {
					item.qty = Utils.tryParseInt(matcher.group(1), 0);
				} else {
					LOG.logWarning("Cant parse QTY info for " + count.get(0).getText());
				}
			}
		}
		String source = detailsDriver.getPageSource();
		List<WebElement> imgs = detailsDriver.findElementsByXPath("//span[contains(@class,'thumbnail')]//img");
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);
		
		String size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Size')]/../td");
		if (size.isEmpty() || !size.contains(" x ")) {
			size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Dimensions')]/../td");
			if (!size.isEmpty()) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			} else {
				Matcher matcher = WFSizeInfoPattern.matcher(source);
				if (matcher.find()) {
					String content = matcher.group(1);
					content = URLDecoder.decode(content, "UTF-8");
					matcher = WFSizePattern.matcher(content);
					if (matcher.find()) {
						size = matcher.group(1) + " L x " + matcher.group(2) + " W x " + matcher.group(3) + " H";
					}
				}
			}
		} else {
			if (!size.toLowerCase().contains("l ") && !size.toLowerCase().contains("w ") && !size.toLowerCase().contains("h ")) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			}
		}
		size = " x " + size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		
		return item;
		
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		StringBuilder info = new StringBuilder();
		List<WebElement> tds = detailsDriver.findElements(By.xpath("//table[@id='productDetails_techSpec_section_1']//td"));
		for (WebElement td : tds) {
			info.append(td.getText()).append(" ");
		}
		return item.resolveUPC(info.toString(), null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.amazon.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-1", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA1)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-2", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA2)));
	}
	
	@Override
	public String toString() {
		return "Amazon.com(Retailers)";
	}
	
}
