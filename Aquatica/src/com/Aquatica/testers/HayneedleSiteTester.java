package com.Aquatica.testers;

import static com.Aquatica.Utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.google.common.base.Strings;

public class HayneedleSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://search.hayneedle.com/search/index.cfm?categoryId=0&selectedFacets=Brand%7CAquatica~%5E&page=1";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*h", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*w", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*l", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public HayneedleSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.hayneedle.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_SID", Settings.get(Settings.BOTS_HAYNEEDLE_COOKIE_SID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_PID", Settings.get(Settings.BOTS_HAYNEEDLE_COOKIE_PID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_IID", Settings.get(Settings.BOTS_HAYNEEDLE_COOKIE_IID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_UID", Settings.get(Settings.BOTS_HAYNEEDLE_COOKIE_UID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_HID", Settings.get(Settings.BOTS_HAYNEEDLE_COOKIE_HID)));
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		WebDriver newMainDriver = DRIVER.getPlatformDriverWithProxy();
		createNewTab(newMainDriver);
		changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			List<WebElement> divs = newMainDriver.findElements(By.xpath("//a[contains(@class,'individual-card')]"));
			if (divs.isEmpty()) {
				break;
			}
			int divCount = divs.size();
			for (int divI = 0; divI < divCount; divI++) {
				WebElement div = newMainDriver.findElement(By.xpath("(//a[contains(@class,'individual-card')])[" + (divI + 1) + "]"));
				String href = div.getAttribute("href");
				String title = div.findElement(By.xpath(".//span[@id='name']")).getText();
				/*
				 * String t = div.getText(); t = t.replace("QUICK VIEW",
				 * "").trim(); String title = t.substring(0,
				 * t.indexOf("$")).trim();
				 */
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
				// if (result.foundItems.size() < 4) {
				// continue;
				// }
				changeToTab2(newMainDriver);
				Collection<Item> items = getDetails(href, newMainDriver);
				changeToTab1(newMainDriver);
				for (Item item : items) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
			
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
			checkCaptcha(detailsDriver);
			int step = 0;
			List<WebElement> checks = detailsDriver.findElements(By.xpath("//div[contains(@class,'pdp-redesign')]"));
			while (checks.isEmpty()) {
				step++;
				if (step % 20 == 0) {
					detailsDriver.get(href);
				}
				sleep1Sec();
				checks = detailsDriver.findElements(By.xpath("//div[contains(@class,'pdp-redesign')]"));
				if (step > 60) {
					LOG.logError("Problem when load page " + href);
					Item item = new Item();
					item.UPC = "000000000000";
					item.title = "BOT BLOCKED";
					item.href = href;
					result.add(item);
					return result;
				}
			}
			Boolean optionsBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'options-container')]//..//div[contains(@class,'option-select')]")).isEmpty();
			if (optionsBlock) {
				Item item = createItem(href, "", 0.0, detailsDriver);
				result.add(item);
				return result;
			}
			WebElement divSelect = detailsDriver.findElement(By.xpath("//div[contains(@class,'options-container')]//..//div[contains(@class,'option-select')]"));
			// ((RemoteWebDriver) detailsDriver).executeScript("window.scroll("
			// + 0 + "," + (divSelect.getLocation().y - 250) + ")" , divSelect);
			divSelect.click();
			Utils.sleep1Sec();
			List<WebElement> options = detailsDriver.findElements(By.xpath("//div[contains(@class,'options-container')]//..//div[contains(@class,'option-value-name')]"));
			Collection<String> optionTitle = new ArrayList<String>();
			for (WebElement option : options) {
				if (!option.getText().isEmpty())
					optionTitle.add(option.getText());
			}
			divSelect.click();
			Utils.sleep1Sec();
			for (String option : optionTitle) {
				divSelect = detailsDriver.findElement(By.xpath("//div[contains(@class,'options-container')]//..//div[contains(@class,'option-select')]"));
				// ((RemoteWebDriver)
				// detailsDriver).executeScript("arguments[0].style.display='block';",
				// divSelect);
				// Utils.sleep1Sec();
				// ((RemoteWebDriver)
				// detailsDriver).executeScript("arguments[0].click();",
				// option);
				divSelect.click();
				Utils.sleep1Sec();
				detailsDriver.findElement(By.xpath("//div[contains(@class,'options-container')]//..//div[contains(@class,'option-value-container')]//..//div[contains(.,'" + option + "')]")).click();
				Utils.sleep3Sec();
				Item item = createItem(href, option.trim(), 0.0, detailsDriver);
				result.add(item);
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
		
	}
	
	public Item createItem(String href, String finish, double finishPrice, WebDriver detailsDriver) throws Exception {
		List<WebElement> checks = detailsDriver.findElements(By.xpath("//div[contains(@class,'pdp-redesign')]"));
		if (checks.isEmpty()) {
			Item item = new Item();
			item.UPC = "000000000000";
			item.title = "BOT BLOCKED";
			item.href = href;
			return item;
		}
		WebElement div = checks.get(0);
		String realTitle = div.findElement(By.xpath("//div[contains(@class,'pdp-title')]//..//h1")).getText().trim();
		if (!Strings.isNullOrEmpty(finish)) {
			realTitle = realTitle + " -> " + finish;
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		if (finishPrice == 0.0) {
			String testPrice = null;
			List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'pdp-price')]//..//span[contains(@class,'pdp-dollar-price')]"));
			if (!priceblock.isEmpty()) {
				String centblock = div.findElements(By.xpath("//div[contains(@class,'pdp-price')]//..//span[contains(@class,'pdp-cent-price')]")).get(0).getText();
				testPrice = (priceblock.get(0).getText()).trim() + "." + centblock;
				item.price = tryParseDouble(testPrice, null);
			} else {
				LOG.logWarning("Not found price block");
			}
		} else {
			item.price = tryParseDouble(finishPrice, null);
		}
		item.SKU = detailsDriver.findElement(By.xpath("//span[contains(@class,'breadcrumb-sku ')]")).getText().replace("#", "").replace("Item", "").trim();
		item.qty = 0;
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'pdp-scroll-image')]//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		} else {
			WebElement img = div.findElement(By.xpath("//div[contains(@class,'image-magnifier')]//img"));
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);
		
		List<WebElement> blockDimensions = div.findElements(By.xpath("//div[@class='pdp-tech-specifications']//..//div[contains(@class,'pdp-data-table-row')]//div[1][contains(.,'Dimensions')]/../div[2]//div"));
		String size = "";
		if (blockDimensions.size() == 1) {
			size = blockDimensions.get(0).getText();
		} else {
			String sizeInFinish = finish.substring(0, finish.indexOf("in")).trim();
			for (WebElement dimensions : blockDimensions) {
				if (dimensions.getText().startsWith(sizeInFinish)) {
					size = dimensions.getText();
					break;
				}
			}
		}
		if (!size.isEmpty()) {
			size = "x " + size.toLowerCase().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").trim();
			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		}
		
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver) {
		return item.resolveUPC("", null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//div[contains(@id,'recaptcha_image')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//div[contains(@id,'recaptcha_image')]")).isEmpty();
	}
	
	@Override
	public String toString() {
		return "Hayneedle";
	}
}
