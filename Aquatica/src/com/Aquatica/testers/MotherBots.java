package com.Aquatica.testers;

import static com.Aquatica.Utils.changeToTab1;
import static com.Aquatica.Utils.changeToTab2;
import static com.Aquatica.Utils.createNewTab;
import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.core.Is;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

import mx4j.log.Log;
import mx4j.log.Log4JLogger;

public class MotherBots implements SiteTester {
	public static final String MAIN_PAGE = "https://shopee.ph/CKK-V7-Plus-1GB-RAM-8GB-ROM-(Rose-Gold)-i.35580456.1115162517";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	private Map<String, Integer> checkProxy = new HashMap<String, Integer>();
	private Map<String, Integer> successProxy = new HashMap<String, Integer>();
	private ArrayList<String> proxyList = new ArrayList<String>();
	private ArrayList<String> userAgentList = new ArrayList<String>();
	private ArrayList<String> userText = new ArrayList<String>();
	
	public MotherBots(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
		
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		initUsers();
		
		String proxy, userAgent;
		for (String mainPage : mainPages) {
		WebDriver newMainDrivers = DRIVER.getPlatformDriver();
		newMainDrivers.get(mainPage);
		String source = newMainDrivers.getPageSource();
		List<WebElement> divForms = newMainDrivers.findElements(By.xpath("//h1[contains(@class,'shopee-product-info')]"));
		String tests = divForms.get(0).getText();
		}
		
		int iterations = 20;
		int iterationsFailed = 0;
		
		outer: for (int a = 0; a < iterations; a++) {
			int leftIteration = iterations - a;
			LOG.log(leftIteration + " iterations left");
			int chooseProxy, chooseUserAgent, chooseText;
			do {
				chooseProxy = (int) (Math.random() * proxyList.size());
				chooseUserAgent = (int) (Math.random() * userAgentList.size());
				proxy = proxyList.get(chooseProxy);
				userAgent = userAgentList.get(chooseUserAgent);
			} while (checkProxy.get(proxy) > 3);
			LOG.logSuccess("Pair: proxy - " + chooseProxy + ", userAgent - " + chooseUserAgent);
			WebDriver newMainDriver = DRIVER.getPlatformForMothersBot(proxy, userAgent);
			// createNewTab(newMainDriver);
			// changeToTab1(newMainDriver);
			
			for (String mainPage : mainPages) {
				LOG.log("Load main page " + mainPage);
				List<WebElement> divForm;
				try{
				newMainDriver.get(mainPage);
				sleep3Sec();
				divForm = newMainDriver.findElements(By.xpath("//div[@class='office-form-body']"));
				} catch(Exception ex){
					LOG.logError(ex, "Problem with this proxy: " + chooseProxy + " - " + proxyList.get(chooseProxy));
					int check = checkProxy.get(proxy) + 1;
					checkProxy.put(proxy, check);
					iterationsFailed++;
					newMainDriver.quit();
					DRIVER.freePlatformDriver(newMainDriver);
					continue outer;
				}
				int i = 0;
				while (divForm.isEmpty()) {
					i++;
					sleep1Sec();
					divForm = newMainDriver.findElements(By.xpath("//div[@class='office-form-body']"));
					if (i > 70) {
						LOG.logError("This proxy is bad (" + chooseProxy + " - " + proxy + ")");
						int check = checkProxy.get(proxy) + 1;
						checkProxy.put(proxy, check);
						iterationsFailed++;
						newMainDriver.quit();
						DRIVER.freePlatformDriver(newMainDriver);
						continue outer;
					}
					if (i % 10 == 0) {
						divForm = newMainDriver.findElements(By.xpath("//div[@class='office-form-body']"));
						LOG.logInfo("Waiting Searching Forms");
					}
					if (i % 20 == 0) {
						LOG.logInfo("Waiting Page load");
						newMainDriver.get(mainPage);
						divForm = newMainDriver.findElements(By.xpath("//div[@class='office-form-body']"));
					}
				}
				LOG.logOracle("Form found");
				sleep3Sec();
				List<WebElement> selects = newMainDriver.findElements(By.xpath("//div[@class='select-placeholder']"));
				selects.get(0).click();
				sleep3Sec();
				List<WebElement> divSelect = newMainDriver.findElements(By.xpath("//ul[@class='select-option-menu-container']//li[contains(.,'Тищенко С. Є.')]"));
				if(!divSelect.isEmpty()){
					divSelect.get(0).click();
				} else{
					selects.get(1).click();
					sleep3Sec();
					divSelect = newMainDriver.findElements(By.xpath("//ul[@class='select-option-menu-container']//li[contains(.,'Тищенко С. Є.')]"));
					divSelect.get(0).click();
				}
				sleep3Sec();
				chooseText = (int) (Math.random() * userText.size());
				List <WebElement> textField = newMainDriver.findElements(By.xpath("//div[contains(@class, 'student-feedback')]//input[contains(@class, 'office-form-textfield-input')]"));
				if(!textField.isEmpty()){
					textField.get(0).sendKeys(userText.get(chooseText));
				}
				sleep3Sec();
				List <WebElement> radioField = newMainDriver.findElements(By.xpath("//label[contains(., 'Ознайомлен')]//input[contains(@role, 'radio')]"));
				if(!radioField.isEmpty()){
					radioField.get(0).click();
				}
				sleep1Sec();
				WebElement button = newMainDriver.findElement(By.xpath("//div[contains(@class, 'submit-button-div')]//button"));
				button.click();
				sleep3Sec();
				List <WebElement> divSuccess = newMainDriver.findElements(By.xpath("//div[@class='office-form-thankyou-page-container']"));
				i = 0;
				boolean isSuccessPage = true;
				while (divSuccess.isEmpty()) {
					i++;
					sleep1Sec();
					divSuccess = newMainDriver.findElements(By.xpath("//div[@class='office-form-thankyou-page-container']"));
					if (i % 15 == 0) {
						LOG.logInfo("Waiting success Page load");
					}
					
					if(i > 60){
						LOG.logError("Page success is not load");
						isSuccessPage = false;
						break;
					}
				}
				if(isSuccessPage){
					LOG.logSuccess("Vote was sent! Gratz!");
					int checksuccess = successProxy.get(proxy) + 1;
					successProxy.put(proxy, checksuccess);
				}
			}
			newMainDriver.quit();
			DRIVER.freePlatformDriver(newMainDriver);
			
			int chooseSleep = (int) (Math.random() * 50) + 50;
			if (leftIteration != 1) {
				LOG.log("Sleep " + chooseSleep*10 + " seconds");
				int sleep = 0;
				while (sleep < chooseSleep) {
					sleep++;
					Thread.sleep(10000);
					LOG.logInfo(((chooseSleep - sleep) * 10) + " seconds left");
				}
				LOG.logInfo("Lets Go!");
				sleep3Sec();
			}
		}
		LOG.logError("Iterations failed = " + iterationsFailed);
		LOG.logSuccess("Iterations successful = " + (iterations - iterationsFailed));
		for (Map.Entry entry : checkProxy.entrySet()) {
		    LOG.logWarning("Proxy: " + entry.getKey() + " Check: "
		        + entry.getValue());
		}
		for (Map.Entry entry : successProxy.entrySet()) {
		    LOG.logOracle("Proxy: " + entry.getKey() + " Check: "
		        + entry.getValue());
		}
		return result;
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "MotherBots";
	}
	
	private void initUsers() {
		proxyList.add("37.57.241.120:8888");
		proxyList.add("46.219.14.43:8080");
		proxyList.add("77.222.139.57:8080");
		proxyList.add("80.91.177.78:3128");
		proxyList.add("81.163.119.84:53281");
		proxyList.add("82.117.229.20:3128");
		proxyList.add("91.193.128.76:8080");
		proxyList.add("91.206.30.218:3128");
		proxyList.add("91.214.128.88:8080");
		proxyList.add("91.237.240.5:8080");
		proxyList.add("92.112.243.60:3128");//
		proxyList.add("94.45.49.170:8080");
		proxyList.add("109.86.227.250:8080");
		proxyList.add("109.254.211.233:3128");
		proxyList.add("109.200.155.196:8080");
		proxyList.add("109.254.93.195:8080");
		proxyList.add("109.87.156.99:8080");
		proxyList.add("159.224.83.100:8080");
		proxyList.add("176.37.121.85:8080");
		proxyList.add("178.151.34.43:8080");
		proxyList.add("185.8.41.109:8080");
		proxyList.add("185.154.13.2:3128");
		proxyList.add("193.16.101.146:8080");
		proxyList.add("193.19.243.225:8080");
		proxyList.add("194.169.206.141:8080");
		proxyList.add("195.66.141.152:8080");
		proxyList.add("195.69.197.7:8080");
		proxyList.add("195.80.140.212:8081");//
		proxyList.add("195.211.174.202:8080");//
		proxyList.add("195.69.197.7:8080");
		proxyList.add("195.138.75.189:8081");
		proxyList.add("213.160.157.159:8080");
		proxyList.add("46.219.116.2:8081");
		proxyList.add("46.98.251.161:53281");
		proxyList.add("80.91.176.240:8080");
		proxyList.add("82.117.245.110:3128");
		proxyList.add("91.214.179.4:8080");
		proxyList.add("91.219.198.42:8080");
		proxyList.add("91.237.240.1:8080");
		proxyList.add("91.193.128.76:8080");
		proxyList.add("91.197.220.51:3128");
		proxyList.add("176.119.110.217:53281");//
		proxyList.add("176.241.129.179:8080");
		proxyList.add("193.34.219.216:8088");//
		proxyList.add("193.93.228.242:5328");
		proxyList.add("193.110.114.26:8080");
		proxyList.add("194.44.228.94:8181");//
		proxyList.add("194.44.246.82:8080");
		proxyList.add("194.79.63.134:8080");
		proxyList.add("195.177.75.106:8081");
		proxyList.add("212.115.238.222:8080");
		
		
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/604.5.6 (KHTML, like Gecko) Version/11.0.3 Safari/604.5.6");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.58");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/63.0.3239.84 Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:58.0) Gecko/20100101 Firefox/58.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0;  Trident/5.0)");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.5.6 (KHTML, like Gecko) Version/11.0.3 Safari/604.5.6");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  Trident/5.0)");
		userAgentList.add("	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36");
		userAgentList.add("	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 5.1; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("	Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0");
		userAgentList.add("Mozilla/5.0 (iPad; CPU OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0 Mobile/15C202 Safari/604.1");
		userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");
		userAgentList.add("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko");
		
		for (String proxy : proxyList) {
			checkProxy.put(proxy, 0);
		}
		
		for (String proxy : proxyList) {
			successProxy.put(proxy, 0);
		}
		
		userText.add("Види діяльності, які вчитель впроваджую у своїй роботі, допомагають відійти від стандарту.");
		userText.add("Робота показує, що вчитель розвиває інтелектуальні здібності учнів та ініціативність. Дякую.");
		userText.add("Робота відповідає сучасним вимогам. Формування   ключових  компетентностей найважливіше завдання вчителя.");
		userText.add("Види роботи, які вчитель впроваджує у своїй роботі, підштовхують учня до власного, індивідуального, особливого погляду на життя.");
		userText.add("Дякуємо за роботу. Вчитель вчить учнів вмінню презентувати себе.");
		userText.add("Сподобалась робота. Сподобалось відношення вчителя до свої учнів, в яких вона бачить особистості.");
		userText.add("Відмітила для себе вправи диференційованої роботи.");
		userText.add("Цікаві вправи ігрового характеру.");
		userText.add("Діти люблять казки, люблять все фантастичне,  вживаючись у роль дитина розкривається. Дякую за поради.");
		userText.add("Цікаві наробки. Корисні поради щодо розкриття творчого потенціалу учня дитини.");
		userText.add("Проблема розвитку творчих здібностей особистості є однією з пріоритетних в педагогіці.");
		userText.add("Вчитель розкриває, як вчить учня працювати в групі, в колективі, сміливо демонструвати свої здібності.");
		userText.add("На сучасному етапі все більше уваги приділяється пошуку перспективних підходів до процесу навчання і виховання, стимулювання пізнавальної активності,  творчої і креативної діяльності.");
		userText.add("Театрально-ігрові ситуації  допомагають дітям глибше пізнати себе, свій внутрішній світ. Дякую за працю.");
		userText.add("Згодна з вчителем, що навчання іноземній мові не повинно обмежуватися урочним часом.");
		userText.add("Позакласна робота з предмету  сприяє більш глибокому оволодінню предметом. Дякую.");
		userText.add("Вчитель має гарні результати успішності учнів і охоплення гуртковою роботою.");
		userText.add("Завдяки різноманітним позакласним заходам з’являється у учнів мотивація щодо необхідності навчання іноземної мови для майбутнього та бажання вчитися.");
		userText.add("Вистави німецькою мовою – це практика живого спілкування для учнів.");
		userText.add("Участь у театралізованих виставах це уміння учнів встановлювати і підтримувати контакт із співрозмовником.");
		userText.add("Участь учнів у заходах по предмету це вміння будувати стратегію, мовленнєву поведінку залежно від комунікативної ситуації.");
		userText.add("Вчитель вчить учнів вмінню висловлюванню на іноземній мові в різних жанрах, в різних ситуаціях");
		userText.add("Робота про дослідження театрально-ігрових методів при навчанні німецької мови цікава.");
		userText.add("Шкільний театр іноземною мовою – це дивовижно. Кожен відповідає за власне висловлювання.");
		userText.add("Театральні прийоми виховують у дитини креативність, вміння слухати, застосовувати різну інтонацію тощо.");
		userText.add("Театральні прийоми виховують у школярів креативність, громадську активність, прагнення і потребу в творчій віддачі отриманих знань і умінь.");
		userText.add("Доцільна та ефективна методика навчання німецькій мові.");
		userText.add("Сподобалося, що вчитель стимулює учнів до висловлювання свого ставлення до тієї чи іншої життєвої або навчальної реальності.");
		userText.add("Театр може не тільки розкрити особистість дитини, але також пробудити в ньому гордість за себе і бажання вчитися ділі.");
		userText.add("Виконуючи роль, учень стає організатором самого себе, і опора для інших в загальній роботі, таким чином він навчається іноземній мові  і навчає.");
		userText.add("Театральне творчість є оптимальним варіантом розвитку зацікавленості до вивчення іноземної мови.");
		userText.add("Новизна у тому, що процес спілкування характеризується постійною зміною предмета розмови, умов, завдань.");
		userText.add("Сподобався задум вчителя: ситуативність відтворює  комунікативну реальність і бажання відповідати.");
		userText.add("Робота вчителя сподобалась, особливо конспекти уроків.");
		userText.add("Робота актуальна. В школах не вистачає виховних заходів на іноземній мові.");
		userText.add("Особливо сподобались казки німецькою мовою. Такі методи розвивають комунікативні здібності учня.");
		userText.add("Згодна з вчителем. У наш час спостерігається зростання інтересу до процесу творчості. Дякую за Вашу працю.");
		userText.add("Дякуємо за роботу. Це дуже важливо допомагати учням знайти себе у житті.");
		userText.add("Сподобалось що вчитель не байдуже ставиться до особистості учня. Дякуємо.");
		userText.add("Допомогти учнівській молоді у життєвому самовизначенні, самореалізації  це найважливіші завдання сучасної освіти.");
		userText.add("Робота показує, що вчитель своєю працею виховує свідому особистість із громадянською позицією, здатну до толерантності у спілкуванні.");
		userText.add("Робота сподобалась, особливо провадження інтерактивних методів в позаурочний час");
		userText.add("Здатність до спілкування у різних ситуаціях вважаю головним завданням при вивченні іноземної мови.");
		userText.add("Здатність до пристосування у різних ситуаціях  дуже гарно розвивається творчими процесами. Дякую вчителю.");
		userText.add("Сподобалися методи в роботі як вчити використовувати мову творчо, цілеспрямовано, нормативно, у взаємодії зі співрозмовниками.");
	}
	
}
