package com.Aquatica.testers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class ArchiProductsSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.archiproducts.com/en/products-aquatica-plumbing-group-a162597.html";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public ArchiProductsSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			
			List<WebElement> divs = mainDriver.findElementsByXPath("//li[contains(@class,'_productbox')]");
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//h3/a"));
				String href = a.getAttribute("href");
				String title = a.getText();
				String sku = div.getAttribute("pid");
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//				 if (result.foundItems.size() < 15) {
//				 continue;
//				 }
				for (Item item : getDetails(href, detailsDriver, sku)) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver, String sku) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			String description = detailsDriver.findElement(By.xpath("//div[contains(@id,'ProductGeneralDescription')]")).getText();
			
			if (description.contains("Avaliable item:")) {
				int index = description.indexOf("Avaliable item:");
				int brIndex = description.indexOf("\n",index);
				if (brIndex<0){
					brIndex = description.length();
				}
				String finish = description.substring(index, brIndex).replace("Avaliable item:", "").trim();
				Item item = createItem(href, finish, detailsDriver, sku);
				result.add(item);
				return result;
			}
			
			if (description.contains("Avaliable items:")) {
				int index = description.indexOf("Avaliable items:");
				int brIndex = description.indexOf("\n",index);
				if (brIndex<0){
					brIndex = description.length();
				}
				String finish = description.substring(index, brIndex).replace("Avaliable items:", "").trim();
				String finishes[];
				finishes = finish.split(",");
				for (String presentFinish : finishes) {
					Item item = createItem(href, presentFinish.trim(), detailsDriver, sku);
					result.add(item);
					
				}
			} else {
				Item item = createItem(href, "", detailsDriver, sku);
				result.add(item);
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String finish, AquaticaDriver detailsDriver, String sku) throws Exception {
		String realTitle = detailsDriver.findElement(By.xpath("//h1[contains(@class,'ProductName')]")).getText().replace("Aquatica Plumbing Group", "").trim();
		if (!finish.isEmpty()) {
			realTitle += " -> " + finish.replace(".", "").replace(";", "").trim();
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = 0.0;
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//li[contains(@class,'thumbs')]//..//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, finish, detailsDriver);
		item.SKU = sku;
		
		String length = getTextIfExist(detailsDriver, "//span[text()[contains(.,'Length')]][text()[contains(.,'Exterior')]]/../../../td[2]");
		String width = getTextIfExist(detailsDriver, "//span[text()[contains(.,'Width')]][text()[contains(.,'Exterior')]]/../../../td[2]");
		String height = getTextIfExist(detailsDriver, "//span[text()[contains(.,'Height')]][text()[contains(.,'Exterior')]]/../../../td[2]");
		
		if (length.isEmpty() && width.isEmpty() && height.isEmpty()) {
			length = getTextIfExist(detailsDriver, "//td[text()[contains(.,'Length')]][text()[contains(.,'Exterior')]]/../td[2]");
			width = getTextIfExist(detailsDriver, "//td[text()[contains(.,'Width')]][text()[contains(.,'Exterior')]]/../td[2]");
			height = getTextIfExist(detailsDriver, "//td[text()[contains(.,'Height')]][text()[contains(.,'Exterior')]]/../td[2]");
		}
		
		if (length.isEmpty() && width.isEmpty()) {
			String diametr = getTextIfExist(detailsDriver, "//td[text()[contains(.,'Outside')]][text()[contains(.,'Diameter')]]/../td[2]");
			length = diametr;
			width = diametr;
		}
		
		// String sizeBlock =
		// detailsDriver.findElement(By.xpath("//div[contains(@class,'description
		// left dimensions')]")).getText();
		// String size = sizeBlock.substring(sizeBlock.indexOf("Exterior
		// Parameters"), sizeBlock.indexOf("H")).replace("Exterior Parameters",
		// "");
		//
		// size = size.toLowerCase().trim().replace("inches",
		// "").replace("inch", "").replace("in.", "").replace("in",
		// "").replace("-", "").replace("\"", "");
		// size = "x "+size +" H";
		
		item.length = Utils.tryParseDouble(length, 0.0);
		item.width = Utils.tryParseDouble(width, 0.0);
		item.height = Utils.tryParseDouble(height, 0.0);
		return item;
	}
	
	private String getUPC(Item item, String finish, AquaticaDriver detailsDriver) {
		return item.resolveUPC(finish, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "ArchiProducts";
	}
}
