package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class KitchenSourceSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.kitchensource.com/aquatica/brand/";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*D", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern paramsPattern = Pattern.compile("<b>(.*?)</b>(.*?)<br>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public KitchenSourceSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@id,'number_3')]//..//td//a[contains(@class, 'navlgbw')]");
				
				for (WebElement div : divs) {
					String href = div.getAttribute("href");
					String title = div.getText();
					if (productList.containsKey(href)) {
						LOG.log("Dublicate link - " + href);
						continue;
					}
					productList.put(href, title);
				}
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = mainDriver.findElementsByXPath(".//a[contains(.,'Next')]");
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(0);
					urlNextPage = nextElement.getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			result.foundItems.add(product.getValue());
			LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
//			 if (result.foundItems.size() < 35) {
//			 continue;
//			 }
			for (Item item : getDetails(product.getKey(), detailsDriver)) {
				result.addItem(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			boolean hasOption = false;
			boolean hasAdditionalOption = false;
			List<WebElement> options = detailsDriver.findElementsByXPath("//div[contains(@id,'drop_down')]//select[contains(@name, 'attribute1')]//option");
			if (options.isEmpty()) {
				Item item = createItem(href, detailsDriver, hasOption);
				result.add(item);
				return result;
			}
			
			hasOption = true;
			List<WebElement> additionalOptions = detailsDriver.findElementsByXPath("//div[contains(@id,'drop_down')]//select[contains(@name, 'attribute2')]//option");
			hasAdditionalOption = !additionalOptions.isEmpty();
			Collection<String> values = new ArrayList<String>();
			Collection<String> additionalValues = new ArrayList<String>();
			for (WebElement option : options) {
				String value = option.getAttribute("value");
				if (!value.isEmpty() && !value.contains("0")) {
					values.add(value);
				}
			}
			
			if (hasAdditionalOption) {
				for (WebElement option : additionalOptions) {
					String additionalValue = option.getAttribute("value");
					if (!additionalValue.isEmpty() && !additionalValue.contains("0")) {
						additionalValues.add(additionalValue);
					}
				}
			}
			
			for (String value : values) {
				detailsDriver.findElementByXPath("//div[contains(@id,'drop_down')]//select[contains(@name, 'attribute1')]//option[@value='" + value + "']").click();
				Thread.sleep(2000);
				if (hasAdditionalOption) {
					for (String additionalValue : additionalValues) {
						detailsDriver.findElementByXPath("//div[contains(@id,'drop_down')]//select[contains(@name, 'attribute2')]//option[@value='" + additionalValue + "']").click();
						Thread.sleep(2000);
						Item item = createItem(href, detailsDriver, hasOption);
						result.add(item);
					}
				} else {
					Item item = createItem(href, detailsDriver, hasOption);
					result.add(item);
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver, boolean hasOption) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id,'number_3')]"));
		String realTitle = div.findElement(By.xpath("//div[contains(@id, 'body_page_header')]//h1")).getText().trim();
		String testPrice = "0.0";
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@id, 'price')]//div[contains(@class,'price_spec')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText().replace("Our Price:", "");
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.xpath("//div[@id='kau_id']")).getText().replace("AQU-", "").trim();
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@id,'rollover_images')]//..//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		}
		
		item.UPC = getUPC(item, detailsDriver, item.SKU);
		
		String paramHtml = (String) detailsDriver.executeScript("return document.getElementById('body_size_information').innerHTML;");
		Matcher matcher = paramsPattern.matcher(paramHtml);
		Map<String, String> params = new HashMap<String, String>();
		while (matcher.find()) {
			params.put(matcher.group(1), matcher.group(2));
		}
		String size = "";
		if (params.containsKey("Exterior Dimensions:")) {
			size = params.get("Exterior Dimensions:").replace("\"", "").trim();
		} else if (params.containsKey("Showerhead Dimensions:")) {
			size = params.get("Showerhead Dimensions:").replace("\"", "").trim();
		} else if (params.containsKey("Faucet Dimensions:")) {
			size = params.get("Faucet Dimensions:").replace("\"", "").trim();
		}
		
		size = Utils.replaceSizeAdditions("x " + size);
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver, String sku) {
		String upcs = sku;
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "KitchenSource";
	}
}
