package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class BravoBusinessMediaSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://products.actionsupply.com/Aquatica-v1-Page-1-300.htm";
	
	//private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public BravoBusinessMediaSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@id,'product-search-attributes')]");
				for (WebElement div : divs) {
					List<WebElement> blockInfo = div.findElements(By.xpath("./div/ul[contains(@class, 'mbottom')]/li"));
					String href = blockInfo.get(1).findElement(By.tagName("a")).getAttribute("href");
					String title = blockInfo.get(3).getText().trim();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 18) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath("//a[contains(@title, 'Next Page')]");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 2) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(1).getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'container mbot10')]"));
		List<WebElement> realTitleBlock = div.findElements(By.tagName("h1"));
		if(realTitleBlock.contains("WE'RE SORRY"))
			return null;
			
		String realTitle = "";
		if(!realTitleBlock.isEmpty()){
			realTitle = realTitleBlock.get(0).getText().trim();
		}
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//h3[contains(@class, 'priceColor')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText().replace("Price:", "").trim();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.xpath("//div[contains(@class,'tab-pane active')]//h4")).getText().replace("Aquatica -", "").trim();
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'wrapper')]//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs)
				item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);
		
		String l = "0", h = "0", w = "0";
		l = getTextIfExist(detailsDriver, "//table/tbody/tr[contains(.,'Length')]/td[2]").replace("\"", "");
		w = getTextIfExist(detailsDriver, "//table/tbody/tr[contains(.,'Width')]/td[2]").replace("\"", "");
		h = getTextIfExist(detailsDriver, "//table/tbody/tr[contains(.,'Height')]/td[2]").replace("\"", "");
		
		item.length = Utils.tryParseDouble(l, 0.0);
		item.width = Utils.tryParseDouble(w, 0.0);
		item.height = Utils.tryParseDouble(h, 0.0);
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		String upc = "";
		upc = getTextIfExist(detailsDriver, "//table/tbody/tr[contains(.,'UPC')]/td[2]");
		return item.resolveUPC(upc, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "BravoBusinessMedia";
	}
}
