package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class CloudShowersSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://cloud9showers.com/search?type=product&q=Aquatica";
	
	// private static final Pattern heightPattern =
	// Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE |
	// Pattern.DOTALL);
	// private static final Pattern wigthPattern =
	// Pattern.compile("x\\s*([\\d\\.]+)\\s*D", Pattern.CASE_INSENSITIVE |
	// Pattern.DOTALL);
	// private static final Pattern lengthPattern =
	// Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE |
	// Pattern.DOTALL);
	private static final Pattern upcPattern = Pattern.compile("\"barcode\":\"(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public CloudShowersSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@class, 'grid-uniform')]//div[contains(@class, 'grid-item')]");
				
				for (WebElement div : divs) {
					String href = div.findElement(By.xpath("./a[contains(@class, 'product-grid-item')]")).getAttribute("href");
					String title = div.findElement(By.xpath("./a[contains(@class, 'product-grid-item')]/p")).getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 9) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath(".//a[contains(.,'Next')]");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 2) {
						System.err.println("\t WARNING: EXPECTED TWO NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//main[contains(@class,'main-content')]"));
		String realTitle = div.findElement(By.xpath("//h2[contains(@itemprop, 'name')]")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//span[contains(@id, 'productPrice')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
			testPrice = testPrice.substring(0, testPrice.length()-2).concat("." + testPrice.substring(testPrice.length()-2));
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		List<WebElement> variantsBlock = div.findElements(By.xpath("//select[@class = 'product-variants']/option"));
		if (!variantsBlock.isEmpty()) {
			item.SKU = variantsBlock.get(0).getAttribute("data-sku");
		} else {
			item.SKU = "";
			LOG.logInfo("Not Found sku");
		}
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class, 'MagicToolboxContainer')]//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		}
		
		item.UPC = getUPC(item, detailsDriver.getPageSource(), detailsDriver, item.SKU);
		
		String length = "0.0";
		String width = "0.0";
		String height = "0.0";
		
		item.length = Utils.tryParseDouble(length, 0.0);
		item.width = Utils.tryParseDouble(width, 0.0);
		item.height = Utils.tryParseDouble(height, 0.0);
		
		return item;
	}
	
	private String getUPC(Item item, String source, AquaticaDriver detailsDriver, String sku) {
		String upcs = "";
		Matcher matcher = upcPattern.matcher(source);
		if (matcher.find()) {
			upcs = matcher.group(1);
		} else {
			LOG.logInfo("Not Found upc");
			upcs = sku;
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "Cloud9Showers";
	}
}
