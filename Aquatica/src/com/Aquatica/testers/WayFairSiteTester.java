package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.util.Strings;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class WayFairSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.wayfair.com/Aquatica-Group-C474080.html?itemsperpage=200&sortby=2";
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*[H|D]");
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W");
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L");
	private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public WayFairSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			String urlNextPage = "";
			
			do {
				
				List<WebElement> items = newMainDriver.findElements(By.xpath("//div[contains(@id,'sbprodgrid')]//..//a[contains(@class,'ProductCard')]"));
				
				for (WebElement a : items) {
					String href = a.getAttribute("href");
					String title = a.findElement(By.xpath(".//h2[contains(@class, 'ProductCard-name')]")).getText();
					productList.put(href, title);
				}
				
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//a[contains(@aria-label,'Next')]"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(0);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
					LOG.log("Next page loaded");
					sleep1Sec();
					if (!checkCaptcha(newMainDriver)) {
						break;
					}
					sleep3Sec();
				}
			} while (urlNextPage != null);
		}
		
		for (Map.Entry<String, String> product : productList.entrySet()) {
			try {
				result.foundItems.add(product.getValue());
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
				// if (result.foundItems.size() < 140) {
				// continue;
				// }
				Collection<Item> itemsAq = getDetails(product.getKey(), newMainDriver);
				for (Item item : itemsAq) {
					result.addItem(item);
					if (result.foundItems.size() >= MAX_ITEMS) {
						break;
					}
				}
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with scan main page page ", true, newMainDriver.getPageSource());
			}
		}
		
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.wayfair.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_SID", Settings.get(Settings.BOTS_WAYFAIR_COOKIE_SID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_PID", Settings.get(Settings.BOTS_WAYFAIR_COOKIE_PID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_IID", Settings.get(Settings.BOTS_WAYFAIR_COOKIE_IID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_UID", Settings.get(Settings.BOTS_WAYFAIR_COOKIE_UID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_HID", Settings.get(Settings.BOTS_WAYFAIR_COOKIE_HID)));
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
			checkCaptcha(detailsDriver);
			int step = 0;
			WebElement specBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'section-product-overview')]"));
			List<WebElement> divBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'PdpLayout')]"));
			while (divBlock.isEmpty()) {
				step++;
				if (step % 20 == 0) {
					detailsDriver.get(href);
				}
				Thread.sleep(500);
				divBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'PdpLayout')]"));
				if (step > 60) {
					LOG.logError("Problem when load page " + href);
					return result;
				}
			}
			WebElement div = divBlock.get(0);
			String realTitle = "";
			String testPrice = "";
			String partNum = getTextIfExist(detailsDriver.findElement(By.xpath("//body")), ".//li[contains(.,'SKU:')]").replace("SKU: ", "").replace("/", "").trim();
			boolean hasOptions = false;
			
			List<WebElement> optionsBlock = div.findElements(By.xpath("//button[contains(@class,'PdpOptionSelect')]"));
			if (!optionsBlock.isEmpty()) {
				hasOptions = true;
			}
			
			List<WebElement> visualOptionsBlock = div.findElements(By.xpath("//div[contains(@class,'VisualOptionSelector')]"));
			if (!visualOptionsBlock.isEmpty()) {
				hasOptions = true;
			}
			
			if (!hasOptions) {
				realTitle = div.findElement(By.xpath(".//header[contains(@class,'PageTitle')]//h1")).getText();
				testPrice = div.findElement(By.xpath("//div[@class='BasePriceBlock']")).getText();
				WebElement moreInfo = detailsDriver.findElement(By.xpath("//div[contains(@id,'section-product-overview')]//..//button[contains(.,'Specifications')]"));
				((RemoteWebDriver) detailsDriver).executeScript("window.scroll(" + 0 + "," + (moreInfo.getLocation().y - 150) + ")", moreInfo);
				sleep1Sec();
				((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", moreInfo);
				sleep1Sec();
				String finish = getTextIfExist(moreInfo, "//table[@class='Specifications-table']//tr//td[1][contains(.,'Finish')]/../td[2]");
				List<WebElement> partNumberBlock = detailsDriver.findElements(By.xpath("//div[@class='MoreAboutThisProduct']"));
				
				String specText = "";
				if (!partNumberBlock.isEmpty()) {
					specText = partNumberBlock.get(0).getAttribute("innerText");
				}
				String upcs = "";
				if (specText.contains("Part #:")) {
					Matcher matcher = upcPattern.matcher(specText);
					if (matcher.find()) {
						upcs = matcher.group(1);
					} else {
						LOG.logInfo("Not Found upc");
					}
				} else {
					List<WebElement> navNumberBlock = detailsDriver.findElements(By.xpath("//nav[contains(@class,'Breadcrumbs')]//..//li[contains(., 'Part #')]"));
					if (!navNumberBlock.isEmpty()) {
						upcs = navNumberBlock.get(0).getText().replace("Part #:", "").trim();
					}
				}
				
				Item item = createItem(href, realTitle, finish, upcs, testPrice, partNum, getTextSize(specBlock, detailsDriver), false, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			
			if (hasOptions) {
				Collection<String> selectedStrings = null;
				if (!optionsBlock.isEmpty()) {
					selectedStrings = extractOptionsBlock(detailsDriver, optionsBlock);
				} else {
					selectedStrings = extractVisualOptions(detailsDriver, optionsBlock);
				}
				WebElement moreInfo = detailsDriver.findElement(By.xpath("//div[contains(@id,'section-product-overview')]//..//button[contains(.,'Specifications')]"));
				((RemoteWebDriver) detailsDriver).executeScript("window.scroll(" + 0 + "," + (moreInfo.getLocation().y - 150) + ")", moreInfo);
				sleep1Sec();
				((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", moreInfo);
				sleep1Sec();
				for (String piid : selectedStrings) {
					String finish = "";
					if (!optionsBlock.isEmpty()) {
						selectOption(detailsDriver, div, piid);
						finish = piid;
					} else {
						selectVisualOption(detailsDriver, href, piid);
						finish = detailsDriver.findElement(By.xpath("//span[contains(@class,'VisualOptionSelector-optionName')]")).getText().trim();
					}
					div = detailsDriver.findElement(By.xpath("//div[contains(@class,'PdpLayout')]"));
					specBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'section-product-overview')]"));
					realTitle = div.findElement(By.xpath(".//header[contains(@class,'PageTitle')]//h1")).getText();
					testPrice = div.findElement(By.xpath("//div[@class='BasePriceBlock']")).getText();
					
					String upcs = "";
					List<WebElement> tableUpcs = div.findElements(By.xpath("//div[contains(@id,'section-product-overview')]//..//ul[contains(.,'Specifications')]//div[contains(.,'Part Numbers')]/table[@class='Specifications-table']"));
					if (!tableUpcs.isEmpty()) {
						upcs = div.findElement(By.xpath("//div[contains(@id,'section-product-overview')]//..//ul[contains(.,'Specifications')]//div[contains(.,'Part Numbers')]/table[@class='Specifications-table']//..//td[2][contains(.,'" + finish + "')]/../td[1]")).getText();
					} else {
						List<WebElement> navNumberBlock = detailsDriver.findElements(By.xpath("//nav[contains(@class,'Breadcrumbs')]//..//li[contains(., 'Part #')]"));
						if (!navNumberBlock.isEmpty()) {
							upcs = navNumberBlock.get(0).getText().replace("Part #:", "").trim();
						}
					}
					Item item = createItem(href, realTitle, finish, upcs, testPrice, piid, getTextSize(div, detailsDriver), true, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	private Collection<String> extractOptionsBlock(WebDriver detailsDriver, List<WebElement> optionsBlock) {
		Collection<String> selectedStrings = new ArrayList<String>();
		optionsBlock.get(0).click();
		sleep1Sec();
		List<WebElement> options = detailsDriver.findElements(By.xpath("//div[contains(@class,'PdpOptionDrawer')]//li//p[contains(@class, 'PdpOptionDrawer-option-details-title')]"));
		for (WebElement option : options) {
			if (!option.equals("")) {
				selectedStrings.add(option.getText().trim());
			}
		}
		WebElement backButton = detailsDriver.findElement(By.xpath("//div[contains(@class, 'Veil has-overlay')]"));
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", backButton);
		sleep1Sec();
		return selectedStrings;
	}
	
	private Collection<String> extractVisualOptions(WebDriver detailsDriver, List<WebElement> optionsBlock) {
		Collection<String> selectedStrings = new ArrayList<String>();
		List<WebElement> options = detailsDriver.findElements(By.xpath("//div[contains(@class,'VisualOptionSelector')]//.. //a"));
		for (WebElement option : options) {
			selectedStrings.add(option.getAttribute("href").trim());
		}
		return selectedStrings;
	}
	
	private void selectOption(WebDriver detailsDriver, WebElement div, String piid) {
		List<WebElement> optionsBlock = div.findElements(By.xpath("//button[contains(@class,'PdpOptionSelect')]"));
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", optionsBlock.get(0));
		sleep1Sec();
		WebElement divOption = detailsDriver.findElement(By.xpath("//div[contains(@class,'PdpOptionDrawer')]//li//p[contains(., '" + piid + "')]"));
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", divOption);
		sleep3Sec();
	}
	
	private void selectVisualOption(WebDriver detailsDriver, String href, String piid) {
		detailsDriver.get(href + piid);
		sleep3Sec();
	}
	
	public String getTextSize(WebElement div, WebDriver detailsDriver) {
		WebElement moreInfo = detailsDriver.findElement(By.xpath("//button[contains(@id,'CollapseToggle-1')]"));
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", moreInfo);
		String size = "";
		String l = "";
		String w = "";
		String h = "";
		
		l = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Overall Length - Side to Side')]//dd").replace("\''", "").trim();
		w = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Overall Width - Front to Back')]//dd").replace("\''", "").trim();
		h = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Overall Depth - Top to Bottom')]//dd").replace("\''", "").trim();
		
		if (!l.isEmpty() || !w.isEmpty() || !h.isEmpty()) {
			size = " x " + l + " L x " + w + " W x " + h + " H ";
			return size;
		}
		
		size = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Overall Faucet Height')]//dd").replace("\''", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size + "H";
			return size;
		}
		size = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Overall')]//dd").replace("\''", "").replace("Inches Overall Height- Top to Bottom", "H").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size;
			return size;
		}
		size = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//div//dl[contains(.,'Shower Head')]//dd").replace("\''", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size;
			return size;
		}
		size = getTextIfExist(div, "//div[@class='ProductWeightsDimensions']//li[contains(@class,'ProductWeightsDimensions')]").replace("\''", "").replace("Overall:", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size;
			return size;
		}
		
		return "";
	}
	
	public String getTextIfExist(WebElement div, String xpath) {
		List<WebElement> parts = div.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim();
	}
	
	public Item createItem(String href, String realTitle, String finish, String upcs, String testPrice, String optKey, String size, boolean multiItem, WebDriver detailsDriver) throws Exception {
		if (size.contains(" D") && size.contains(" H")) {
			size = size.replace(" H", " L").replace(" D", " H");
		}
		
		Item item = new Item();
		item.href = href;
		if (!finish.isEmpty()) {
			item.title = realTitle + " -> " + finish;
		} else {
			item.title = realTitle;
		}
		item.price = tryParseDouble(testPrice, null);
		item.SKU = getTextIfExist(detailsDriver.findElement(By.xpath("//body")), ".//li[contains(.,'SKU: ')]").replace("SKU: ", "").replace("/", "").trim();
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		item.UPC = getUPC(item, finish, upcs, multiItem, detailsDriver);
		item.qty = 0;
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'ProductDetailImageCarouselVariantB-thumbnails')]//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		return item;
	}
	
	private String getUPC(Item item, String finish, String upcs, boolean multiItem, WebDriver detailsDriver) {
		if (upcs.contains("6277")) {
			String upc = item.resolveUPC(upcs, null);
			return upc;
		}
		String part = "";
		if (upcs.equals("627722004")) {
			part = "627722004286";
			String upc = item.resolveUPC(part.trim(), null);
			return upc;
		}
		if (upcs.equals("6277220032")) {
			part = "WCRD-300" + finish;
			String upc = item.resolveUPC(part.trim(), null);
			return upc;
		}
		
		if (upcs.equals("Elise-Show-Blck")) {
			upcs = "627722003135";
			String upc = item.resolveUPC(upcs, null);
			return upc;
		}
		
		if (upcs.equals("Karolina-Wht-Matte")) {
			upcs = "627722002961";
			String upc = item.resolveUPC(upcs, null);
			return upc;
		}
		
		part = upcs + " " + finish;
		String upc = item.resolveUPC(part.trim(), null);
		return upc;
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//form[contains(@id,'bd')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//form[contains(@id,'bd')]")).isEmpty();
	}
	
	@Override
	public String toString() {
		return "WayFair";
	}
}