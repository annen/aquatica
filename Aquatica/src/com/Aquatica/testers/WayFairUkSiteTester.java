package com.Aquatica.testers;

import static com.Aquatica.Utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.util.Strings;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AjaxDriver;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class WayFairUkSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.wayfair.co.uk/Aquatica-C1791989.html?itemsperpage=200";
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*[H|D]");
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W");
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L");
	//private static final Pattern qtyPattern = Pattern.compile("\"available_quantity\"\\s*:\\s*(\\d+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern CurrencyGBPPattern = Pattern.compile("\"USDGBP\"\\s*:\\s*([\\d\\.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern CurrencyEURPattern = Pattern.compile("\"USDEUR\"\\s*:\\s*([\\d\\.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private static final String currencyUrlGBP = "http://apilayer.net/api/live?access_key=9cc9b91d7041874faa2fb84526cd4238&currencies=GBP";
	private static final String currencyUrlEUR = "http://apilayer.net/api/live?access_key=9cc9b91d7041874faa2fb84526cd4238&currencies=EUR";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	private Double currentCourseGBP = 1 / 0.804;
	private Double currentCourseEUR = 1 / 0.931;
	private Double currentCourse = 1 / 0.864;

	public WayFairUkSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		setCurrency();
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		createNewTab(newMainDriver);
		changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			WebElement resultsElement = newMainDriver.findElement(By.xpath("//div[contains(@id,'sbprodgrid')]"));
	
			List<WebElement> items = resultsElement.findElements(By.xpath("//a[contains(@id,'productbox')]"));
			for (WebElement a : items) {
				try {
					String href = a.getAttribute("href");
					String title = a.findElement(By.xpath(".//p[contains(@class, 'sb_prod_name')]")).getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//					 if (result.foundItems.size() < 9) {
//					 continue;
//					 }
					changeToTab2(newMainDriver);
					Collection<Item> itemsAq = getDetails(href, newMainDriver);
					changeToTab1(newMainDriver);
					for (Item item : itemsAq) {
						result.addItem(item);
						if (result.foundItems.size() >= MAX_ITEMS) {
							break;
						}
					}
				} catch (Exception ex) {
					LOG.logError(ex, "Problem with scan page " + mainPage, true, newMainDriver.getPageSource());
				}
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}

	public void setCookie(AquaticaDriver driver) {
		String domain = "www.wayfairuk.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_SID", Settings.get(Settings.BOTS_WAYFAIRUK_COOKIE_SID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_PID", Settings.get(Settings.BOTS_WAYFAIRUK_COOKIE_PID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_IID", Settings.get(Settings.BOTS_WAYFAIRUK_COOKIE_IID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_UID", Settings.get(Settings.BOTS_WAYFAIRUK_COOKIE_UID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_HID", Settings.get(Settings.BOTS_WAYFAIRUK_COOKIE_HID)));
	}

	
	public void setCurrency() {
		AjaxDriver ajaxDriver = DRIVER.getAjaxDriver();
		String courseGBPSource = ajaxDriver.getAjax(currencyUrlGBP);
		String courseEURSource = ajaxDriver.getAjax(currencyUrlEUR);
		Matcher matcherGBP = CurrencyGBPPattern.matcher(courseGBPSource);
		Matcher matcherEUR = CurrencyEURPattern.matcher(courseEURSource);
		if (matcherGBP.find()) {
			currentCourseGBP = tryParseDouble(matcherGBP.group(1), currentCourseGBP);
		}
		currentCourseGBP = 1.0 / currentCourseGBP;
		if (matcherEUR.find()) {
			currentCourseEUR = tryParseDouble(matcherEUR.group(1), currentCourseEUR);
		}
		currentCourseEUR = 1.0 / currentCourseEUR;
		currentCourse = currentCourseGBP/currentCourseEUR;
		
		DRIVER.freeAjaxDriver(ajaxDriver);
	}

	private Collection<Item> getDetails(String href, WebDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			//setCookie(detailsDriver);
			detailsDriver.get(href);
			sleep3Sec();
			checkCaptcha(detailsDriver);
			int step = 0;
			WebElement specBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'product_specifications')]"));
			List<WebElement> divBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'ProductDetailInfoBlock')]"));
			while (divBlock.isEmpty()) {
				step++;
				if (step % 20 == 0) {
					detailsDriver.get(href);
				}
				Thread.sleep(500);
				divBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'ProductDetailInfoBlock')]"));
				if (step > 60) {
					LOG.logError("Problem when load page " + href);
					return result;
				}
			}
			WebElement div = divBlock.get(0);
			String realTitle = div.findElement(By.xpath(".//header[contains(@class,'PageTitle')]//h1")).getText();
			String testPrice = div.findElement(By.xpath(".//div[contains(@class,'pricing-amount')]")).getText();
			String partNum = getTextIfExist(detailsDriver.findElement(By.xpath("//body")), ".//span[contains(.,'Part #:')]").replace("Part #: ", "");
			String finish = getTextIfExist(div, ".//div[contains(@class,'forcedOption')]");
			if (!finish.isEmpty()) {
				List<WebElement> parts = detailsDriver.findElements(By.xpath("//table[contains(@class,'ProductDetailSpecifications-table')]//td[contains(@class,'right')][contains(.,'" + finish + "')]/../td[contains(@class,'left')]"));
				if (!parts.isEmpty()) {
					partNum = parts.get(0).getText();
				}
			}
			boolean mustBeMorePrice = partNum.contains(" / ") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null;
			boolean hasOptions = false;
			boolean isSelect = false;

			List<WebElement> options = div.findElements(By.xpath(".//div[contains(@class,'option-scrollbox')]/a"));
			if (!options.isEmpty()) {
				hasOptions = true;
				if (options.size() == 1) {
					LOG.logError("Expected many select " + href);
					return result;
				}
			} else {
				options = div.findElements(By.xpath("//select[contains(@class,'js-main-product-option-dropdown')]"));
				if (!options.isEmpty()) {
					hasOptions = true;
					isSelect = true;
					if (options.size() != 1) {
						LOG.logError("Expected one select " + href);
						return result;
					}
				}
			}

			if (!mustBeMorePrice && !hasOptions) {
				String text = getTextIfExist(detailsDriver.findElement(By.xpath("//div[contains(@class,'ProductDetail-content')]")), ".//p[contains(.,'Part Number')]/following-sibling::p[1]");
				if (!text.contains("About") && !partNum.equalsIgnoreCase(text)) {
					partNum = partNum + " " + text;
				}
				Item item = createItem(href, realTitle, testPrice, null, partNum, getTextSize(specBlock, detailsDriver), false, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			if (isSelect) {
				Collection<String> selectedStrings = new ArrayList<String>();
				Collection<String> selectedValues = new ArrayList<String>();
				Select select = new Select(options.get(0));
				for (WebElement option : select.getOptions()) {
					String value = option.getAttribute("value");
					if (value == null || value.trim().length() == 0 || value.equalsIgnoreCase("XXXXXXXXXX")) {
						value = "0";
					}
					if ("0".equals(value.trim())) {
						continue;
					}
					selectedStrings.add(option.getText());
					selectedValues.add(value);
				}
				Iterator<String> iterator = selectedValues.iterator();
				for (String text : selectedStrings) {
					String piid = iterator.next();
					String itemUrl = href + "?piid[]=" + piid;
					//setCookie(detailsDriver);
					detailsDriver.get(itemUrl);
					div = detailsDriver.findElement(By.xpath("//div[contains(@class,'ProductDetailInfoBlock')]"));
					specBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'product_specifications')]"));
					testPrice = div.findElement(By.xpath(".//div[contains(@class,'pricing-amount')]")).getText();
					String size = text.trim();
					if (size.contains("+")) {
						size = size.substring(0, size.indexOf('+')).trim();
					}
					Item item = createItem(href, realTitle + " -> " + size, testPrice, piid, size, "x "+ size  + getTextSize(specBlock, detailsDriver), true, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
				}
			} else if (options.size() > 0) {
				Collection<String> selectedStrings = new ArrayList<String>();
				for (WebElement option : options) {
					String value = option.getAttribute("data-pi-id");
					selectedStrings.add(value);
				}
				for (String piid : selectedStrings) {
					String itemUrl = href + "?piid[]=" + piid;
					//setCookie(detailsDriver);
					detailsDriver.get(itemUrl);
					div = detailsDriver.findElement(By.xpath("//div[contains(@class,'ProductDetailInfoBlock')]"));
					specBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'product_specifications')]"));
					String text = div.findElement(By.xpath("//a[contains(@class,'visual-option-selected')]")).getAttribute("data-option-name").trim();
					testPrice = div.findElement(By.xpath(".//div[contains(@class,'pricing-amount')]")).getText();
					Item item = createItem(itemUrl, realTitle + " -> " + text, testPrice, piid, text, getTextSize(specBlock, detailsDriver), true, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
				}
			} else {
				String text = getTextIfExist(detailsDriver.findElement(By.xpath("//div[contains(@class,'ProductDetail-content')]")), ".//p[contains(.,'Part Number')]/following-sibling::p[1]");
				if (text.contains("About")) {
					System.err.println("CHECK THIS");
				}
				testPrice = div.findElement(By.xpath(".//div[contains(@class,'pricing-amount')]")).getText();
				Item item = createItem(href, realTitle + " -> " + text, testPrice, "", text, getTextSize(specBlock, detailsDriver), true, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}

	public String getTextSize(WebElement div, WebDriver detailsDriver) {
		WebElement specView = detailsDriver.findElement(By.xpath("//div[contains(@id,'product_specifications')]/div"));
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].setAttribute('style', 'max-height: none;')", specView);
		String size = "";
		String part1 = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Overall Widtht')]/../td[2]").replace("cm", "").trim();
		String part2 = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Overall Depth')]/../td[2]").replace("cm", "").trim();
		String part3 = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Overall Height')]/../td[2]").replace("cm", "").trim();
		if (!Strings.isNullOrEmpty(part1) || !Strings.isNullOrEmpty(part2) || !Strings.isNullOrEmpty(part3)) {
			size = " x " + part1 + "L x " + part2 + "W x " + part3 + "D ";
			return size;
		}
		size = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Overall Faucet Height')]/../td[2]").replace("cm", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size + "H";
			return size;
		}
		size = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Overall')]/../td[2]").replace("\''", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			size = " x " + size;
			return size;
		}
		size = getTextIfExist(div, "//table[@class='ProductDetailSpecifications-table']//tr//td[1][contains(.,'Shower Head')]/../td[2]").replace("cm", "").trim();
		if (!Strings.isNullOrEmpty(size)){
			size = " x " + size;
			return size;
		}
		return "";
	}

	public String getTextIfExist(WebElement div, String xpath) {
		List<WebElement> parts = div.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim();
	}

	public Item createItem(String href, String realTitle, String testPrice, String optKey, String finish, String size, boolean multiItem, WebDriver detailsDriver) throws Exception {
		if (size.contains(" D") && size.contains(" H")) {
			size = size.replace(" H", " L").replace(" D", " H");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = round(tryParseDouble(testPrice, null) * currentCourse);
		item.SKU = getTextIfExist(detailsDriver.findElement(By.xpath("//body")), ".//span[contains(.,'SKU: ')]").replace("SKU: ", "");
		// detailsDriver.findElementByXPath("//span[@class='ProductDetailBreadcrumbs']").getText().substring(5).trim();
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		item.UPC = getUPC(item, finish, multiItem, detailsDriver);
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//img[contains(@class,'js-carousel-item')]"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
//		AjaxDriver ajaxDriver = DRIVER.getAjaxDriver();
//		setCookie(ajaxDriver);
//		String ajax = ajaxDriver.getAjax("http://www.wayfair.com/a/product/get_liteship_and_inventory_data?product_data[0][sku]=" + item.SKU + (optKey != null ? "&product_data[0][option_ids][]=" + optKey : ""));
//		DRIVER.freeAjaxDriver(ajaxDriver);
//		Matcher matcher = qtyPattern.matcher(ajax);
//		if (matcher.find()) {
//			item.qty = Utils.tryParseInt(matcher.group(1), 0);
//		} else {
//			LOG.logWarning("Cant parse QTY info for " + realTitle);
//		}
		item.qty = 0;
		return item;
	}

	private String getUPC(Item item, String finish, boolean multiItem, WebDriver detailsDriver) {
		String headSKU = getTextIfExist(detailsDriver.findElement(By.xpath("//body")), ".//span[contains(.,'Part #:')]").replace("Part #: ", "");
		if (headSKU.contains(" / ") || headSKU.equalsIgnoreCase(finish) || (multiItem && headSKU.contains("6277"))) {
			headSKU = "";
		}
		List<WebElement> parts = detailsDriver.findElements(By.xpath("//table[contains(@class,'ProductDetailSpecifications-table')]//td[contains(@class,'right')][contains(.,'" + finish + "')]/../td[contains(@class,'left')]"));
		String part = parts.isEmpty() ? headSKU : parts.get(0).getAttribute("textContent");
		part += " " + finish;
		if(part.contains("Shower Arm Finish")){
			part = parts.isEmpty() ? headSKU : parts.get(1).getAttribute("textContent");   //hardcode for Dynamo GPM Shower
		}
		if(part.equals("627722001094 White"))      //hardcode
			part = "627722001162" + " " + finish;
		String sku = item.SKU;
		String jsPN = ((RemoteWebDriver) detailsDriver).executeScript("return wf.appData[arguments[0]].part_number", "product_data_" + sku).toString();
		if (part.contains(jsPN) || jsPN.contains("/") || (multiItem && jsPN.contains("6277"))) {
			jsPN = "";
		}
		part += " " + jsPN;
		String upc = item.resolveUPC(part, null);
		return upc;
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//form[contains(@id,'bd')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//form[contains(@id,'bd')]")).isEmpty();
	}

	@Override
	public String toString() {
		return "WayFairUk";
	}
}
