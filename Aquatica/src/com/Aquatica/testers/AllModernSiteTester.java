package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.util.Strings;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AjaxDriver;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class AllModernSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.allmodern.com/Aquatica-C482981.html?itemsperpage=300";
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*\"\\s*[H|D]");
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*\"\\s*W");
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*\"\\s*L");
	private static final Pattern qtyPattern = Pattern.compile("\"available_quantity\"\\s*:\\s*(\\d+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public AllModernSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			Thread.sleep(1000);
			LOG.log("Parse main page products");
			WebElement resultsElement = mainDriver.findElementById("sbprodgrid");
			List<WebElement> items = resultsElement.findElements(By.xpath("//a[contains(@class,'productbox')]"));
			for (WebElement a : items) {
				try {
					String href = a.getAttribute("href");
					String title = a.findElement(By.xpath(".//p[contains(@class, 'sb_prod_name')]")).getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 45) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (result.foundItems.size() >= MAX_ITEMS) {
							break;
						}
					}
				} catch (Exception ex) {
					LOG.logError(ex, "Problem with scan page " + mainPage, true, mainDriver.getPageSource());
				}
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.allmodern.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_SID", Settings.get(Settings.BOTS_ALLMODERN_COOKIE_SID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_PID", Settings.get(Settings.BOTS_ALLMODERN_COOKIE_PID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_IID", Settings.get(Settings.BOTS_ALLMODERN_COOKIE_IID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_UID", Settings.get(Settings.BOTS_ALLMODERN_COOKIE_UID)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "D_HID", Settings.get(Settings.BOTS_ALLMODERN_COOKIE_HID)));
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			setCookie(detailsDriver);
			detailsDriver.get(href);
			WebElement div = detailsDriver.findElementByXPath("//div[contains(@class,'ProductDetailInfoBlock')]");
			String realTitle = div.findElement(By.xpath(".//span[contains(@class,'title')]")).getText();
			String testPrice = div.findElement(By.xpath(".//span[contains(@data-id,'dynamic-sku-price')]")).getText();
			String partNum = "";
			// = getTextIfExist(detailsDriver.findElementByTagName("body"),
			// ".//span[contains(.,'Part #:')]").replace("Part #: ", "");
			String finish = getTextIfExist(div, ".//div[contains(@class,'forcedOption')]");
			if (!finish.isEmpty()) {
				List<WebElement> parts = detailsDriver.findElementsByXPath("//table[contains(@class,'ProductDetailSpecifications-table')]//td[contains(@class,'right')][contains(.,'" + finish + "')]/../td[contains(@class,'left')]");
				if (!parts.isEmpty()) {
					partNum = parts.get(0).getText();
				}
			}
			boolean mustBeMorePrice = partNum.contains(" / ") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null;
			boolean hasOptions = false;
			boolean isSelect = false;
			
			List<WebElement> options = div.findElements(By.xpath(".//div[contains(@class,'option-scrollbox')]/a"));
			if (!options.isEmpty()) {
				hasOptions = true;
				if (options.size() == 1) {
					LOG.logError("Expected many select " + href);
					return result;
				}
			} else {
				options = div.findElements(By.xpath("//select[contains(@class,'js-main-product-option-dropdown')]"));
				if (!options.isEmpty()) {
					hasOptions = true;
					isSelect = true;
					if (options.size() != 1) {
						LOG.logError("Expected one select " + href);
						return result;
					}
				}
			}
			
			if (!mustBeMorePrice && !hasOptions) {
				String text = getTextIfExist(detailsDriver.findElementByXPath("//div[contains(@class,'ProductDetail-content')]"), ".//p[contains(.,'Part Number')]/following-sibling::p[1]");
				if (!text.contains("About") && !partNum.equalsIgnoreCase(text)) {
					partNum = partNum + " " + text;
				}
				Item item = createItem(href, realTitle, testPrice, null, partNum, getTextSize(div), false, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			if (isSelect) {
				Collection<String> selectedStrings = new ArrayList<String>();
				Collection<String> selectedValues = new ArrayList<String>();
				Select select = new Select(options.get(0));
				for (WebElement option : select.getOptions()) {
					String value = option.getAttribute("value");
					if (value == null || value.trim().length() == 0 || value.equalsIgnoreCase("XXXXXXXXXX")) {
						value = "0";
					}
					if ("0".equals(value.trim())) {
						continue;
					}
					selectedStrings.add(option.getText());
					selectedValues.add(value);
				}
				Iterator<String> iterator = selectedValues.iterator();
				for (String text : selectedStrings) {
					String piid = iterator.next();
					String itemUrl = href + "?piid[]=" + piid;
					setCookie(detailsDriver);
					detailsDriver.get(itemUrl);
					div = detailsDriver.findElementByXPath("//div[contains(@class,'ProductDetailInfoBlock')]");
					testPrice = div.findElement(By.xpath("//span[contains(@data-id,'dynamic-sku-price')]")).getText();
					String size = text;
					if (size.contains("+")) {
						size = size.substring(0, size.indexOf('+')).trim();
					}
					Item item = createItem(href, realTitle + " -> " + size, testPrice, piid, size, "x " + size + getTextSize(div), true, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
				}
			} else if (options.size() > 0) {
				Collection<String> selectedStrings = new ArrayList<String>();
				for (WebElement option : options) {
					String value = option.getAttribute("data-pi-id");
					selectedStrings.add(value);
				}
				for (String piid : selectedStrings) {
					String itemUrl = href + "?piid[]=" + piid;
					setCookie(detailsDriver);
					detailsDriver.get(itemUrl);
					div = detailsDriver.findElementByXPath("//div[contains(@class,'ProductDetailInfoBlock')]");
					String text = div.findElement(By.xpath("//a[contains(@class,'visual-option-selected')]")).getAttribute("data-option-name");
					testPrice = div.findElement(By.xpath(".//span[contains(@data-id,'dynamic-sku-price')]")).getText();
					Item item = createItem(itemUrl, realTitle + " -> " + text, testPrice, piid, text, getTextSize(div), true, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
				}
			} else {
				String text = getTextIfExist(detailsDriver.findElementByXPath("//div[contains(@class,'ProductDetail-content')]"), ".//p[contains(.,'Part Number')]/following-sibling::p[1]");
				if (text.contains("About")) {
					System.err.println("CHECK THIS");
				}
				testPrice = div.findElement(By.xpath(".//span[contains(@data-id,'dynamic-sku-price')]")).getText();
				Item item = createItem(href, realTitle + " -> " + text, testPrice, "", text, getTextSize(div), true, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public String getTextSize(WebElement div) {
		String size = getTextIfExist(div, "//div[contains(@class,'ProductDetailSpecifications')]//li[contains(.,'Overall:')]").replace("Overall:", " x ");
		if (!Strings.isNullOrEmpty(size))
			return size;
		size = getTextIfExist(div, "//div[contains(@class,'ProductDetailSpecifications')]//li[contains(.,'Overall Faucet Height:')]").replace("Height:", " x ");
		if (!Strings.isNullOrEmpty(size)) {
			size = size + "H";
			return size;
		}
		String part1 = getTextIfExist(div, "//div[contains(@class,'ProductDetailSpecifications')]//li[contains(.,'Overall Length')]").replace(":", " x ");
		String part2 = getTextIfExist(div, "//div[contains(@class,'ProductDetailSpecifications')]//li[contains(.,'Overall Width')]").replace(":", " x ");
		String part3 = getTextIfExist(div, "//div[contains(@class,'ProductDetailSpecifications')]//li[contains(.,'Overall Depth')]").replace(":", " x ");
		if (!Strings.isNullOrEmpty(part1) || !Strings.isNullOrEmpty(part2) || !Strings.isNullOrEmpty(part3)) {
			size = part1 + "L " + part2 + "W " + part3 + "H ";
			return size;
		}
		return "";
	}
	
	public String getTextIfExist(WebElement div, String xpath) {
		List<WebElement> parts = div.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim();
	}
	
	public Item createItem(String href, String realTitle, String testPrice, String optKey, String finish, String size, boolean multiItem, AquaticaDriver detailsDriver) throws Exception {
		if (size.contains(" D") && size.contains(" H")) {
			size = size.replace(" H", " L").replace(" D", " H");
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = getTextIfExist(detailsDriver.findElementByTagName("body"), ".//span[contains(.,'SKU: ')]").replace("SKU: ", "");
		// detailsDriver.findElementByXPath("//span[@class='ProductDetailBreadcrumbs']").getText().substring(5).trim();
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		item.UPC = getUPC(item, finish, multiItem, detailsDriver);
		List<WebElement> imgs = detailsDriver.findElementsByXPath("//img[contains(@class,'js-carousel-item')]");
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		AjaxDriver ajaxDriver = DRIVER.getAjaxDriver();
		setCookie(ajaxDriver);
		String ajax = ajaxDriver.getAjax("http://www.allmodern.com/a/product/get_liteship_and_inventory_data?product_data[0][sku]=" + item.SKU + (optKey != null ? "&product_data[0][option_ids][]=" + optKey : ""));
		DRIVER.freeAjaxDriver(ajaxDriver);
		Matcher matcher = qtyPattern.matcher(ajax);
		if (matcher.find()) {
			item.qty = Utils.tryParseInt(matcher.group(1), 0);
		} else {
			LOG.logWarning("Cant parse QTY info for " + realTitle);
		}
		return item;
	}
	
	private String getUPC(Item item, String finish, boolean multiItem, AquaticaDriver detailsDriver) {
		String headSKU = getTextIfExist(detailsDriver.findElementByTagName("body"), ".//span[contains(.,'Part #:')]").replace("Part #: ", "");
		if (headSKU.contains(" / ") || headSKU.equalsIgnoreCase(finish) || (multiItem && headSKU.contains("6277"))) {
			headSKU = "";
		}
		List<WebElement> parts = detailsDriver.findElementsByXPath("//table[contains(@class,'ProductDetailSpecifications-table')]//td[contains(@class,'right')][contains(.,' " + finish + "')]/../td[contains(@class,'left')]");
		String part = parts.isEmpty() ? headSKU : parts.get(0).getText();
		part += " " + finish;
		String upc = item.resolveUPC(part, null);
		return upc;
	}
	
	@Override
	public String toString() {
		return "AllModern";
	}
}
