package com.Aquatica.testers;

import static com.Aquatica.Utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class GoEdekersSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.goedekers.com/catalogsearch/result/index/?form_key=k0Nf7qp0Otlry4Vw&limit=80&q=aquatica+-";
	
	private static final Pattern upcPattern = Pattern.compile("og:upc[^>]+(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern upcPattern2 = Pattern.compile("(627722\\d{6})[^>]+og:upc", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public GoEdekersSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		WebDriver newMainDriver = DRIVER.getPlatformDriverWithProxy();
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			// ((RemoteWebDriver)
			// newMainDriver).executeScript("window.scrollTo(0,
			// document.body.scrollHeight)");
			// Thread.sleep(5000);
			String urlNextPage;
			do {
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//div[contains(@class,'category-products')]/ul[contains(@class,'products')]/li"));
				int divCount = divs.size();
				for (int divI = 1; divI < divCount + 1; divI++) {
					WebElement div = newMainDriver.findElement(By.xpath("(//div[contains(@class,'category-products')]/ul[contains(@class,'products')]/li)[" + (divI) + "]"));
					WebElement a = div.findElement(By.xpath(".//div[contains(@class,'product-name')]/a"));
					String href = a.getAttribute("href");
					String title = a.getAttribute("title");
					String visibleTitle = a.getAttribute("outerText");
					if (!visibleTitle.contains("Aquatica")) {
						LOG.log("Skip item #" + divI);
						continue;
					}
					productList.put(href, title);
				}
				
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//a[contains(@class,'next i-next')]"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(2);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
					sleep1Sec();
					if (!checkCaptcha(newMainDriver)) {
						break;
					}
					sleep3Sec();
				}
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			result.foundItems.add(product.getValue());
			LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
			Collection<Item> items = getDetails(product.getKey(), newMainDriver);
			for (Item item : items) {
				result.addItem(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
			checkCaptcha(detailsDriver);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//div[contains(@id,'recaptcha_image')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//div[contains(@id,'recaptcha_image')]")).isEmpty();
	}
	
	public Item createItem(String href, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'product-view')]"));
		String realTitle = div.findElements(By.xpath("//span[contains(@class,'full-prod-name')]")).get(0).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'product-price-area')]/div[contains(@class,'product-price')]"));
		if (!priceblock.isEmpty()) {
			if (priceblock.get(0).getText().contains("See Price in Cart")) {
				((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", div.findElement(By.xpath("//div[contains(@class,'product-price-area')]/div[contains(@class,'product-price')]//a")));
				sleep3Sec();
				testPrice = div.findElement(By.xpath("//div[contains(@class, 'priceText')]//div")).getText();
				((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", div.findElement(By.xpath("//button[@id = 'fancybox-close']")));
			} else {
				testPrice = priceblock.get(0).getText();
			}
		} else {
			List<WebElement> notAvailableBlock = div.findElements(By.xpath(".//div[@class='title']"));
			if (notAvailableBlock.isEmpty() || !notAvailableBlock.get(0).getText().contains("Product Not Available")) {
				LOG.logWarning("Not found price block");
			}
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = div.findElement(By.xpath("//div[contains(@class,'product-shop')]//div[contains(@class,'product-sku')]")).getText().replace("Model #", "");
		String source = detailsDriver.getPageSource();
		
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[@class='product-image-thumbs']/div/a/img"));
		
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		
		item.UPC = getUPC(item, detailsDriver, source);
		List<WebElement> specBlock = detailsDriver.findElements(By.xpath("//dd[contains(@class,'tab-container specs')]"));
		// if (!specBlock.isEmpty()) {
		// int tablePosition =
		// detailsDriver.findElement(By.xpath("//div[contains(@class,'product-collateral')]")).getLocation().getY();
		// ((RemoteWebDriver) detailsDriver).executeScript("window.scrollTo(0,"
		// + (tablePosition) + ")");
		// sleep1Sec();
		((RemoteWebDriver) detailsDriver).executeScript("arguments[0].style.display='block';", specBlock.get(0));
		// sleep1Sec();
		// }
		String width = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[1][contains(.,'Width')]/../td[2]");
		if (width.isEmpty()) {
			width = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[3][contains(.,'Width')]/../td[4]");
		}
		String length = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[1][contains(.,'Depth')]/../td[2]");
		if (length.isEmpty()) {
			length = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[3][contains(.,'Depth')]/../td[4]");
		}
		String height = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[1][contains(.,'Height')]/../td[2]");
		if (height.isEmpty()) {
			height = getTextIfExist(detailsDriver, "//table[@class='product-specs-table']//tr//td[3][contains(.,'Height')]/../td[4]");
		}
		item.length = Utils.tryParseDouble(length, 0.0);
		item.width = Utils.tryParseDouble(width, 0.0);
		item.height = Utils.tryParseDouble(height, 0.0);
		
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver, String source) {
		String upcs = "";
		Matcher matcher = upcPattern.matcher(source);
		if (matcher.find()) {
			upcs = matcher.group(1);
		} else {
			matcher = upcPattern2.matcher(source);
			if (matcher.find()) {
				upcs = matcher.group(1);
			} else {
				LOG.logWarning("Not Fount og:upc meta tag");
			}
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "GoEdekers";
	}
}
