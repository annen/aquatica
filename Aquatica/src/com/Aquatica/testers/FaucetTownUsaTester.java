package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class FaucetTownUsaTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.faucettownusa.com/search.php?search_query_adv=aquatica%20&section=product";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	//private static final Pattern qtyPattern = Pattern.compile("VarInventory\\[\\d+\\]\\s*=\\s*(\\d+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern upcPattern = Pattern.compile("og:image[^>]+(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public FaucetTownUsaTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//main[contains(@id,'product-listing-container')]//form//ul[contains(@class,'productGrid')]//li[contains(@class,'product')]");

				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//article[contains(@class,'card')]//div[contains(@class,'card-body')]/h4[contains(@class,'card-title')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//					 if (result.foundItems.size() < 22) {
//					 continue;
//					 }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath(".//a[contains(.,'Next')]");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 2) {
						System.err.println("\t WARNING: EXPECTED TWO NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'productView')]"));
		String realTitle = div.findElement(By.xpath("//h1[contains(@itemprop,'name')]")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//span[contains(@class,'price--withoutTax')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.xpath("//ul[contains(@class,'top-details')]/li[contains(.,'Item #')]")).getText().replace("Item #:", "").trim();
		String source = detailsDriver.getPageSource();
		item.qty = 0;
//		Matcher matcher = qtyPattern.matcher(source);
//		if (matcher.find()) {
//			item.qty = Utils.tryParseInt(matcher.group(1), 0);
//		} else {
//			LOG.logWarning("QTY pattern not found");
//		}
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//li[contains(@class,'productView-thumbnail')]/a/img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		} 
			
		
		item.UPC = getUPC(item, detailsDriver, source);
		
		String size = getTextIfExist(detailsDriver, "//li[contains(.,'External Size:')]");
		if (!size.isEmpty()) {
			size = size.toLowerCase().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "").replace(":", " x ").replace("External Size:", "").trim();
			size = "x " + size;
			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		} 
//		else {
//			size = getTextIfExist(detailsDriver, "//li[contains(.,'Exterior Dimensions:')]");
//			size = size.toLowerCase().replace("exterior dimensions:", "");
//			size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
//			String[] parts = size.split(" x ");
//			if (parts.length == 3) {
//				size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
//			}
//			
//			size = "x " + size;
//			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
//			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
//			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
//		}
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver, String source) {
		String upcs = "";
		Matcher matcher = upcPattern.matcher(source);
		if (matcher.find()) {
			upcs = upcs + " " + matcher.group(1);
		}
		if (upcs.contains("627722002398") || upcs.contains("627722002923")) {
			upcs = ""; // Wrong UPC
							// http://www.faucettownusa.com/p/aquatica-daydreamer-c-by-massimo-farinatti-bathtub-70-87-x-31-5/
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "FaucetTownUsa";
	}
}
