package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class QualityBathSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.qualitybath.com/brands/1719/aquatica?perpage=500";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public QualityBathSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		// createNewTab(newMainDriver);
		// changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			Map<String, String> productList = new LinkedHashMap<String, String>();
			LOG.log("Searching products");
			List<WebElement> divs = newMainDriver.findElements(By.xpath("//section[contains(@class,'e1vnq5ar0')]/div"));
			for (WebElement div : divs) {
				checkModal(newMainDriver);
				List<WebElement> a = div.findElements(By.xpath(".//a[contains(@class,'css-1vzx9hi')]"));
				if (a.isEmpty()) {
					continue;
				}
				List<WebElement> b = a.get(0).findElements(By.xpath(".//h2[contains(@class,'css-r33409')]"));
				if (b.isEmpty()) {
					continue;
				}
				String href = a.get(0).getAttribute("href");
				String title = b.get(0).getText();
				productList.put(href, title);
			}
			
			LOG.log("Finish searching products - " + productList.size());
			
			for (Map.Entry<String, String> product : productList.entrySet()) {
				result.foundItems.add(product.getValue());
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
				LOG.logInfo("Url: " + product.getKey());
				// if (result.foundItems.size() < 51) {
				// continue;
				// }
				Collection<Item> itemsAq = getDetails(product.getKey(), newMainDriver);
				for (Item item : itemsAq) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private void checkModal(WebDriver newMainDriver) {
		List<WebElement> modal = newMainDriver.findElements(By.id("exit-modal"));
		if (!modal.isEmpty()) {
			((RemoteWebDriver) newMainDriver).executeScript("document.getElementsByClassName('qb-remove')[0].click()", modal);
		}
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			checkCaptcha(detailsDriver);
			sleep3Sec();
			
			// checkCaptcha(detailsDriver);
			LOG.logInfo("Searching options");
			List<WebElement> options = detailsDriver.findElements(By.xpath("//div[contains(@class,'css-4qerui')]//select//option"));
			if (options.isEmpty()) {
				LOG.log("Additional options not found");
				Item item = createItem(href, false, detailsDriver);
				result.add(item);
				return result;
			}
			LOG.log("Additional Options found");
			Collection<String> values = new ArrayList<String>();
			for (WebElement option : options) {
				String value = option.getAttribute("value");
				if (!value.equals("0") && !value.isEmpty() && !option.getText().toLowerCase().contains("overflow kit")) {
					values.add(value);
				}
			}
			if (values.size() > 0) {
				for (String value : values) {
					WebElement option = detailsDriver.findElement(By.xpath("//option[@value='" + value + "']"));
					WebElement divSelect = detailsDriver.findElement(By.xpath("//div[contains(@class,'css-4qerui')]//select"));
					((RemoteWebDriver) detailsDriver).executeScript("window.scroll(" + 0 + "," + (divSelect.getLocation().y - 150) + ")", divSelect);
					option.click();
					LOG.log("Select option:" + value);
					sleep3Sec();
					int i = 0;
					do {
						Thread.sleep(500);
						List<WebElement> divBlock = detailsDriver.findElements(By.xpath("//div[contains(@class,'css-qksh47')]"));
						if (!divBlock.isEmpty()) {
							LOG.logInfo("Found main block");
							break;
						}
						if (i % 10 == 0) {
							divSelect = detailsDriver.findElement(By.xpath("//option[@value='" + value + "']"));
							divSelect.click();
							LOG.logInfo("Loading main block");
						}
						i++;
					} while (i < 40);
					if (i == 40) {
						LOG.logError("Problem with page " + href + " Not found main block");
					}
					Item item = createItem(href, true, detailsDriver);
					result.add(item);
				}
			} else {
				LOG.log("Only Overflow Kit");
				Item item = createItem(href, false, detailsDriver);
				result.add(item);
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, boolean hasSelect, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'css-qksh47')]"));
		String realTitle = div.findElements(By.xpath("//h1[contains(@class,'css-13lsco5')]//span")).get(1).getText();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//b[contains(@class,'css-4dgzct')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText().trim();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = ((RemoteWebDriver) detailsDriver).executeScript("return document.getElementsByClassName('css-1a2rv3d')[0].textContent", div).toString().trim();
		item.qty = 0;
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'css-117eau0')]//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		
		String source = detailsDriver.getPageSource();
		item.UPC = getUPC(item, hasSelect, source);
		
		String l = "0", h = "0", w = "0";
		l = getTextIfExist(detailsDriver, "//table[contains(@class,'css-k0105j')]/tbody/tr/td[1][contains(.,'Overall Length')]/../td[2]");
		w = getTextIfExist(detailsDriver, "//table[contains(@class,'css-k0105j')]/tbody/tr/td[1][contains(.,'Overall Width')]/../td[2]");
		h = getTextIfExist(detailsDriver, "//table[contains(@class,'css-k0105j')]/tbody/tr/td[1][contains(.,'Height')]/../td[2]");
		
		if (w.isEmpty() && !l.isEmpty()) {
			w = l;
		}
		
		String size = "x " + l.trim() + " L x " + w.trim() + " W x " + h.trim() + " H";
		size = size.toLowerCase().replace("\"", "").trim();
		size = Utils.replaceSizeAdditions(size);
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}
	
	private String getUPC(Item item, boolean hasSelect, String source) {
		String upcs = "";
		if (!hasSelect) {
			Matcher matcher = upcPattern.matcher(source);
			if (matcher.find()) {
				upcs = matcher.group(1);
			} else {
				LOG.logInfo("Not Found upc");
			}
		}
		upcs = item.SKU;
		return item.resolveUPC(upcs, null);
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//div[contains(@class,'cf-captcha-container')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//div[contains(@class,'cf-captcha-container')]")).isEmpty();
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "QualityBath";
	}
}
