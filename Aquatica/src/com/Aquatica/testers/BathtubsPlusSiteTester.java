package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;
import com.google.common.base.Strings;

public class BathtubsPlusSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.bathtubsplus.com/aquatica.html";

	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;

	public BathtubsPlusSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//li[contains(@class,'item')]");
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//h2[contains(@class,'product-name')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 47) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath(".//a[contains(@class,'next i-next')]");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 2) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					String nextpage = nextElement.get(1).getAttribute("href");
					urlNextPage = MAIN_PAGE + nextpage;
					mainDriver.get(urlNextPage);
					Thread.sleep(6000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}

	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> options = detailsDriver.findElementsByXPath(("//select[contains(@class,'required-entry product-custom-option')]/option"));
			Double finishPrice = 0.0;
			String finish = "";
			if (options.isEmpty()) {
				String sku = detailsDriver.findElementByXPath("//input[@name='product']").getAttribute("value");
				Item item = createItem(href, "", 0.0, sku, detailsDriver);
				result.add(item);
				return result;
			}
			String[] valuesMas;

			for (WebElement option : options) {
				String text = option.getText();
				String value = option.getAttribute("value");
				if (Strings.isNullOrEmpty(value)) {
					continue;
				}
				List<WebElement> finishPriceblock = detailsDriver.findElements(By.xpath("//div[contains(@class,'add-to-cart-wrapper')]//p[contains(@class, 'special-price')]//span[@class='price']"));
				if (text.contains("+")) {
					valuesMas = text.split("\\+");
					finish = valuesMas[0].trim();
					finishPrice = Utils.tryParseDouble(valuesMas[1], 0.0) + Utils.tryParseDouble(finishPriceblock.get(0).getText(), 0.0);
				} else {
					finish = text;
					finishPrice = Utils.tryParseDouble(finishPriceblock.get(0).getText(), 0.0);
				}
				Item item = createItem(href, finish, finishPrice, value, detailsDriver);
				result.add(item);
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}

	public Item createItem(String href, String finish, double finishPrice, String sku, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'product-view')]"));
		String realTitle = div.findElement(By.xpath("//div[contains(@class,'product-name')]/h1")).getText().trim();
		if (!Strings.isNullOrEmpty(finish)) {
			realTitle = realTitle + " -> " + finish;
		}
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'add-to-cart-wrapper')]//p[contains(@class, 'special-price')]//span[@class='price']"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			if (finishPrice == 0.0) {
				LOG.logWarning("Not found price block");
			}
		}

		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		if (finishPrice == 0.0) {
			item.price = tryParseDouble(testPrice, null);
		} else {
			item.price = tryParseDouble(finishPrice, null);
		}

		item.SKU = sku;
		item.qty = 0;

		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'more-views')]//..//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}

		item.UPC = getUPC(item, detailsDriver, finish);

		String size = getTextIfExist(detailsDriver, "//div[contains(@id,'product_tabs_description_tabbed_contents')]//..//li[contains(.,'Overall Dimensions:')]");
		if (!size.isEmpty()) {
			size = size.toLowerCase().trim().replace("overall dimensions:", "").replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
			size = "x " + size;

			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		} else {
			String l = "0", h = "0", w = "0", d = "0";
			l = getTextIfExist(detailsDriver, "//div[contains(@class,'prod_dimensions')]/ul/li[contains(.,'Length')]").replace("overall length - end to end:", "").replace("inches", "").trim();
			w = getTextIfExist(detailsDriver, "//div[contains(@class,'prod_dimensions')]/ul/li[contains(.,'Width')]").replace("overall width - side to side:", "").replace("inches", "").trim();
			h = getTextIfExist(detailsDriver, "//div[contains(@class,'prod_dimensions')]/ul/li[contains(.,'Depth')]").replace("overall depth - top to bottom:", "").replace("inches", "").trim();
			d = getTextIfExist(detailsDriver, "//div[contains(@class,'prod_dimensions')]/ul/li[contains(.,'Height')]").replace("overall faucet height:", "").replace("inches", "").trim();

			if (h.isEmpty() && !d.isEmpty()) {
				h = d;
			}

			item.length = Utils.tryParseDouble(l, 0.0);
			item.width = Utils.tryParseDouble(w, 0.0);
			item.height = Utils.tryParseDouble(h, 0.0);
		}
		return item;
	}

	private String getUPC(Item item, AquaticaDriver detailsDriver, String finish) {
		String upcs = finish;
		String sku = detailsDriver.findElement(By.xpath("//table[contains(@class,'data-table')]//..//tr[contains(.,'SKU')]/td")).getText().trim();
		if (!sku.contains("/")) {
			upcs += " " + sku;
		}

		return item.resolveUPC(upcs, null);
	}

	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}

	@Override
	public String toString() {
		return "BathtubsPlus";
	}
}
