package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class LowesSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.lowes.com/search?searchTerm=aquatica&refinement=4294526228";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public LowesSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@class,'js-product-list')]");
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//div[contains(@class,'product-details')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText().trim();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 22) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath(".//a[contains(.,'Next')]");
				if (!nextElement.isEmpty()) {
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//section[contains(@class,'pd-holder met-product')]"));
		List<WebElement> locationInput = div.findElements(By.xpath("//input[contains(@id, 'zipcode-input')]"));
		if (!locationInput.isEmpty()) {
			locationInput.get(0).sendKeys("33101");
			WebElement locationButton = div.findElement(By.xpath("//button[contains(@class, 'met-zip-submit')]"));
			locationButton.click();
			Thread.sleep(5000);
			List<WebElement> chooseStore = div.findElements(By.xpath("//button[contains(@class, 'js-store-locator-select-store')]"));
			if (!chooseStore.isEmpty()) {
				chooseStore.get(0).click();
			}
			Thread.sleep(5000);
			div = detailsDriver.findElement(By.xpath("//section[contains(@class,'pd-holder met-product')]"));
		}
		String realTitle = div.findElement(By.tagName("h1")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'pd-price')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText().trim();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.xpath("//span[contains(@class,'met-product-item-number')]")).getText().trim();
		item.qty = 0;
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'pd-epc-holder')]//li//a//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		}
		item.UPC = getUPC(item, detailsDriver);
		
		String width = getTextIfExist(detailsDriver, "//div[contains(@id,'collapseSpecs')]//table//tr//th[contains(.,'Actual Width')]/../td/span");
		String length = getTextIfExist(detailsDriver, "//div[contains(@id,'collapseSpecs')]//table//tr//th[contains(.,'Actual Length')]/../td/span");
		String height = getTextIfExist(detailsDriver, "//div[contains(@id,'collapseSpecs')]//table//tr//th[contains(.,'Actual Height')]/../td/span");
		item.length = Utils.tryParseDouble(length, 0.0);
		item.width = Utils.tryParseDouble(width, 0.0);
		item.height = Utils.tryParseDouble(height, 0.0);
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		String upcs = "";
		List<WebElement> modelBlock = detailsDriver.findElements(By.xpath("//span[contains(@class, 'met-product-model')]"));
		if (!modelBlock.isEmpty()) {
			upcs = modelBlock.get(0).getText();
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "Lowes";
	}
}
