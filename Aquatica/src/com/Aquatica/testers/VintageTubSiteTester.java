package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;
import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class VintageTubSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.vintagetub.com/search?q=aquatica#/?_=1";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern widthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public VintageTubSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			
			WebElement urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//li[contains(@class,'item last')]");
				int lastCount = -1;
				while (divs.size() != lastCount || divs.isEmpty()) {
					LOG.logInfo("Waiting page load");
					Thread.sleep(3000);
					lastCount = divs.size();
					divs = mainDriver.findElementsByXPath("//li[contains(@class,'item last')]");
				}
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//h2[contains(@class,'product-name')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 99) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElements(By.xpath("//table[contains(@class,'pagination bottom')]//td[contains(@class, 'next')]"));
				if (!nextElement.isEmpty()) {
					urlNextPage = nextElement.get(0);
					if (urlNextPage.getCssValue("display").equals("none")) {
						break;
					}
					urlNextPage.click();
				}
				
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> selectBlocks = detailsDriver.findElementsByXPath("//select[contains(@class,'super-attribute-select')]");
			if (selectBlocks.isEmpty()) {
				Item item = createItem(href, detailsDriver);
				result.add(item);
				return result;
			}
			
			List<WebElement> firstOptions = selectBlocks.get(0).findElements(By.tagName("option"));
			Collection<String> mainValues = new ArrayList<String>();
			for (WebElement option : firstOptions) {
				String value = option.getAttribute("value");
				if (!value.isEmpty()) {
					mainValues.add(value);
				}
			}
			
			for (String mainValue : mainValues) {
				selectBlocks = detailsDriver.findElementsByXPath("//select[contains(@class,'super-attribute-select')]");
				selectBlocks.get(0).click();
				sleep1Sec();
				detailsDriver.findElementByXPath("//option[@value='" + mainValue + "']").click();
				sleep3Sec();
				
				if (selectBlocks.size() > 1) {
					List<WebElement> secondOptions = new ArrayList<WebElement>();
					Collection<String> additionalValues = new ArrayList<String>();
					secondOptions = selectBlocks.get(1).findElements(By.tagName("option"));
					for (WebElement option : secondOptions) {
						String value = option.getAttribute("value");
						if (!value.isEmpty()) {
							additionalValues.add(value);
						}
					}
					
					for (String additionalValue : additionalValues) {
						selectBlocks = detailsDriver.findElementsByXPath("//select[contains(@class,'super-attribute-select')]");
						selectBlocks.get(1).click();
						sleep1Sec();
						detailsDriver.findElementByXPath("//option[@value='" + additionalValue + "']").click();
						sleep3Sec();
						
						Item item = createItem(href, detailsDriver);
						result.add(item);
					}
				} else {
					Item item = createItem(href, detailsDriver);
					result.add(item);
				}
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'product-view')]"));
		String realTitle = div.findElement(By.xpath("//div[contains(@class,'product-name')]")).getText().trim();
		String testPrice = null;
		testPrice = div.findElement(By.xpath("//div[contains(@class,'product-shop')]//..//span[contains(@id,'product-price')]/span")).getText();
		// List<WebElement> priceblock =
		// div.findElements(By.xpath("//div[contains(@class,'product-shop')]//..//span[contains(@id,'product-price')]/span"));
		// if (!priceblock.isEmpty()) {
		// testPrice = priceblock.get(0).getText();
		// } else {
		// LOG.logWarning("Not found price block");
		// }
		// return document.querySelector('.product-view .product-shop
		// .price-info .price-box .regular-price .price').innerHTML
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = div.findElements(By.xpath("//div[contains(@class,'product-sku')]")).get(0).getText().replace("SKU:", "").trim();
		item.qty = 0;
		String source = detailsDriver.getPageSource();
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'thumbnail-image__gallery-wrapper')]//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, source);
		
		String l = "0", h = "0", w = "0", d = "0", di = "0";
		l = getTextIfExist(detailsDriver, "//ul[contains(@class,'spec-tab')]/li[contains(.,'Length')]").replace("overall length:", "").replace("exterior length:", "").replace("\"", "");
		w = getTextIfExist(detailsDriver, "//ul[contains(@class,'spec-tab')]/li[contains(.,'Width')]").replace("overall width:", "").replace("exterior width:", "").replace("\"", "");
		h = getTextIfExist(detailsDriver, "//ul[contains(@class,'spec-tab')]/li[contains(.,'Height')]").replace("overall height:", "").replace("exterior height:", "").replace("faucet height:", "").replace("\"", "");
		d = getTextIfExist(detailsDriver, "//ul[contains(@class,'spec-tab')]/li[contains(.,'Depth')]").replace("depth:", "").replace("\"", "");
		di = getTextIfExist(detailsDriver, "//ul[contains(@class,'spec-tab')]/li[contains(.,'Diameter:')]").replace("diameter:", "").replace("\"", "");
		
		if (h.isEmpty() && !d.isEmpty()) {
			h = d;
		}
		
		if (l.isEmpty() && w.isEmpty() && !di.isEmpty()) {
			l = di;
			w = di;
		}
		
		String size = "x " + l.trim().replace("length:", "") + " L x " + w.trim().replace("width: ", "") + " W x " + h.trim().replace("height: ", "") + " H";
		size = size.toLowerCase().trim();
		size = Utils.replaceSizeAdditions(size);
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, widthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}
	
	private String getUPC(Item item, String source) {
		String upcs = "";
		
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return detailsDriver.findElement(By.xpath(xpath)).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "VintageTub";
	}
}
