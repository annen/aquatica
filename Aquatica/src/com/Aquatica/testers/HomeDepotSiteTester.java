package com.Aquatica.testers;

import static com.Aquatica.Utils.getProxyUrl;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;

import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class HomeDepotSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	private static final String AQUATICA = "Aquatica";
	public static final String MAIN_PAGE = "http://www.homedepot.com/b/Bath/N-5yc1vZbzb3/Ntk-brandsearch/Ntt-Aquatica";
//	private static final Pattern heightPattern = Pattern.compile("height\\s*\\(in\\.\\)?\\s*([\\d\\.]*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
//	private static final Pattern wigthPattern = Pattern.compile("width\\s*\\(in\\.\\)?\\s*([\\d\\.]*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
//	private static final Pattern lengthPattern = Pattern.compile("length\\s*\\(in\\.\\)?\\s*([\\d\\.]*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public HomeDepotSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			String urlNextPage = null;
			LOG.log("Load main page " + mainPage);
			mainDriver.get(getProxyUrl(mainPage));
			Thread.sleep(3000);
			do {
				LOG.log("Parse main page products");
				WebElement resultsElement = mainDriver.findElementById("products");
				List<WebElement> divs = resultsElement.findElements(By.xpath("//div[@data-section='gridview']//div[@data-component='productpod']"));
				for (WebElement div : divs) {
					try {
						WebElement a = div.findElement(By.xpath(".//div[contains(@class,'description')]//a"));
						String href = a.getAttribute("href");
						String title = a.getText();
						if (!title.toLowerCase().contains(AQUATICA.toLowerCase())) {
							LOG.logWarning("Bad brand -> " + title);
							continue;
						}
						result.foundItems.add(title);
						LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//						 if (result.foundItems.size() < 32) {
//						 continue;
//						 }
						for (Item item : getDetails(href, detailsDriver)) {
							result.addItem(item);
							if (result.foundItems.size() == MAX_ITEMS) {
								break;
							}
						}
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with scan page " + mainPage);
					}
				}
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElements(By.xpath("//a[@title='Next']"));
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						LOG.logError("Expected one next page button");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(getProxyUrl(urlNextPage));
				}
				
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Thread.sleep(1500);
			WebElement div = detailsDriver.findElementById("productinfo_ctn");
			String realTitle = div.findElement(By.xpath("//h1[contains(@class,'product-title__title')]")).getText();
			String testPrice = div.findElement(By.xpath(".//span[@id='ajaxPrice']//span[contains(@class, 'price__dollars')]")).getText();
			boolean mustBeMorePrice = testPrice.contains("-") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null;
			boolean hasOptions = false;
			boolean hasSelect = false;
			if (!mustBeMorePrice && !hasOptions) {
				Item item = createItem(href, realTitle, testPrice, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			if (!hasSelect) {
				LOG.logError("Not found select " + href);
				return result;
			}
			throw new RuntimeException("Problem with code - Not Implement yet");
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href);
		}
		return result;
	}
	
	public Item createItem(String href, String realTitle, String testPrice, AquaticaDriver detailsDriver) {
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = detailsDriver.findElementById("product_internet_number").getText().replaceAll("[^0-9.-]", "").trim();
		String length = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Basin Length')]//..//div[2]");
		if(length.isEmpty()){
			length = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Left to Right Length')]//..//div[2]");
		}
		String width = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Basin Width')]//..//div[2]");
		if(width.isEmpty()){
			width = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Front to Back Width')]//..//div[2]");
		}
		String height = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Basin Height')]//..//div[2]");
		if(height.isEmpty()){
			height = getTextIfExist(detailsDriver, "//div[@id='specifications']//div[contains(@class, 'specs__group')]//div[contains(.,'Top to Bottom Depth')]//..//div[2]");
		}
		item.length = Utils.tryParseDouble(length,  0.0);
		item.width = Utils.tryParseDouble(width,  0.0);
		item.height = Utils.tryParseDouble(height,  0.0);
		item.UPC = getUPC(item, detailsDriver);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
		}
		List<WebElement> imgs;
		int i = 0;
		do {
			imgs = detailsDriver.findElementsByXPath("//div[contains(@id,'thumbnails')]//..//a[contains(@class, 'media__thumbnail')]/img");
			if (imgs.isEmpty()){
				imgs = detailsDriver.findElementsByXPath("//div[contains(@class,'main-image')]/img");
			}
			if (!imgs.isEmpty()) {
				break;
			}
			i++;
			try {
				Thread.sleep(500);
			} catch (InterruptedException ex) {
			}
		} while (i < 10);
		
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
//		String upc = (String) detailsDriver.executeScript("return CI_ItemUPC");
//		if (upc == null || !upc.startsWith("627722")) {
//			LOG.logWarning("In product invalid upc (" + upc + ") " + item.href);
//			return "";
//		}
//		String info = detailsDriver.findElementByXPath("//h2[contains(@class,'modelNo')]").getText();
//		item.resolveUPC(info, Arrays.asList(upc));
		String model = getTextIfExist(detailsDriver, "//h2[contains(@class,'product_details modelNo')]").replace("Model #", "").replace(":", "").trim();
		String upcTag = detailsDriver.findElement(By.xpath("//upc")).getText();
		String upc = "";
		Matcher matcher = upcPattern.matcher(upcTag);
		if (matcher.find()) {
			upc = upc + " " + matcher.group(1);
		} else {
			LOG.logWarning("In product invalid upc (" + upc + ") " + item.href);
			upc = model;
		}
		if(upc == "627722001032"){
			upc = "627722002961";    //Wrong upc    http://o53xo.nbxw2zlemvyg65bomnxw2.nblz.ru/p/Aquatica-Karolina-5-91-ft-AquateX-Classic-Flatbottom-Non-Whirlpool-Bathtub-in-White-Karolina-Wht-Matte/205022479
		}
		upc = upc + " " + model; 
		
		return item.resolveUPC(upc, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElementsByXPath(xpath);
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim();
	}
	
	@Override
	public String toString() {
		return "HomeDepot.com";
	}
}
