package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class QualityBathSiteTesterWithoutCaptcha implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.qualitybath.com/brands/1719/aquatica?perpage=500";

	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;

	public QualityBathSiteTesterWithoutCaptcha(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);

			List<WebElement> divs = mainDriver.findElements(By.xpath("//section[contains(@class,'test-product-grid')]/div"));
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//div[contains(@class,'ecz5KTg card')]/a"));
				WebElement b = a.findElement(By.xpath(".//h2[contains(@class,'f-5UMjJ')]"));
				String href = a.getAttribute("href");
				String title = b.getText();
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
				// if (result.foundItems.size() < 26) {
				// continue;
				// }
				for (Item item : getDetails(href, detailsDriver)) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}

	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> options = detailsDriver.findElementsByXPath("//div[contains(@class,'dropdown')]/select[contains(@class,'hasCustomSelect')]/option");
			if (options.isEmpty()) {
				Item item = createItem(href, false, detailsDriver);
				result.add(item);
				return result;
			}
			Collection<String> values = new ArrayList<String>();
			for (WebElement option : options) {
				String value = option.getAttribute("value");
				if (!value.equals("0")) {
					values.add(value);
				}
			}
			for (String value : values) {
				detailsDriver.findElementByXPath("//option[@value='" + value + "']").click();
				Thread.sleep(500);
				Item item = createItem(href, true, detailsDriver);
				result.add(item);
			}

		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}

	public Item createItem(String href, boolean hasSelect, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'group content-area')]"));
		String realTitle = div.findElement(By.xpath("//span[contains(@class,'js-title')]")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//b[@id='price']"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(1).getText();
		} else {
			priceblock = div.findElements(By.xpath("//b[contains(@class,'product-price')]"));
			if (!priceblock.isEmpty()) {
				testPrice = priceblock.get(0).getText();
			} else {
				LOG.logWarning("Not found price block");
			}
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = div.findElement(By.xpath("//span[contains(@class,'skuPlaceholder')]")).getText().trim();
		item.qty = 0;

		List<WebElement> imgs = div.findElements(By.xpath("//li[contains(@class,'product-slide')]/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, hasSelect, detailsDriver);

		String l = "0", h = "0", w = "0";
		l = getTextIfExist(detailsDriver, "//table[contains(@class,'_1SiKhCs')]/tbody/tr/td[1][contains(.,'Overall Length')]/../td[2]");
		w = getTextIfExist(detailsDriver, "//table[contains(@class,'_1SiKhCs')]/tbody/tr/td[1][contains(.,'Overall Width')]/../td[2]");
		h = getTextIfExist(detailsDriver, "//table[contains(@class,'_1SiKhCs')]/tbody/tr/td[1][contains(.,'Height')]/../td[2]");

		if (w.isEmpty() && !l.isEmpty()) {
			w = l;
		}
		
		String size = "x " + l.trim() + " L x " + w.trim() + " W x " + h.trim() + " H";
		size = size.toLowerCase().replace("\"", "").trim();
		size = Utils.replaceSizeAdditions(size);
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}

	private String getUPC(Item item, boolean hasSelect, AquaticaDriver detailsDriver) {
		String upcs = "";
		// detailsDriver.executeScript("if (window.jshare.generatedSkus.length>0) return window.jshare.generatedSkus[arguments[0]].upc; else return '';", itemNumber).toString();
		if (!hasSelect) {
			Matcher matcher = upcPattern.matcher(detailsDriver.getPageSource());
			if (matcher.find()) {
				upcs = matcher.group(1);
			} else {
				LOG.logInfo("Not Found upc");
			}
		}
		return item.resolveUPC(upcs, null);
	}

	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}

	@Override
	public String toString() {
		return "QualityBath(no Captcha)";
	}
}
