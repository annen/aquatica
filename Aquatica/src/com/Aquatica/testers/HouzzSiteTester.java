package com.Aquatica.testers;

import static com.Aquatica.Utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class HouzzSiteTester implements SiteTester {
	
	private static class Holder {
		private final Item item;
		
		public Holder(Item item) {
			this.item = item;
		}
		
		@Override
		public int hashCode() {
			return item.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			return item.equalsAnother(((Holder) obj).item);
		}
		
	}
	
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.houzz.com/photos/products/manufacturer--sales_aquatica/query/Aquatica/ls=2";
	private static final Pattern heightPattern = Pattern.compile("D\\s*([\\d\\.]*)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	private static final Pattern wigthPattern = Pattern.compile("H\\s*([\\d\\.]*)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	private static final Pattern lengthPattern = Pattern.compile("W\\s*([\\d\\.]*)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	// private static final Pattern upcPattern =
	// Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE |
	// Pattern.DOTALL);
	private static final Pattern sellerPattern = Pattern.compile("AquaBlu Mosaics", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern idPattern = Pattern.compile("ID#: (\\d+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public HouzzSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		setCookie(mainDriver);
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			String urlNextPage = null;
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			String url = newMainDriver.getCurrentUrl();
			if (url.contains("www.houzz.ru")) {
				List<WebElement> selectCountry = newMainDriver.findElements(By.xpath("//select[contains(@id,'siteSelector')]"));
				if (selectCountry.isEmpty())
					continue;
				selectCountry.get(0).click();
				WebElement usaCountry = newMainDriver.findElement(By.xpath("//select[contains(@id,'siteSelector')]/option[@value='us']"));
				usaCountry.click();
				sleep1Sec();
				WebElement searchField = newMainDriver.findElement(By.xpath("//input[contains(@id,'navSearchInput')]"));
				searchField.sendKeys("aquatica");
				WebElement searchButton = newMainDriver.findElement(By.xpath("//button[contains(@compid,'searchSubmit')]"));
				searchButton.click();
			}
			
			sleep1Sec();
			
			do {
				LOG.log("Parse main page products");
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//div[contains(@class,'hz-card')]//..//div[contains(@class,'hz-product-card hz-track-me')]"));
				int i = 0;
				do {
					sleep3Sec();
					i++;
					if (i % 10 == 0) {
						newMainDriver.get(mainPage);
					}
					divs = newMainDriver.findElements(By.xpath("//div[contains(@class,'hz-card')]//..//div[contains(@class,'hz-product-card hz-track-me')]"));
					if (i > 30) {
						LOG.logError("Problem with page");
						continue;
					}
				} while (divs.size() == 0);
				LOG.log("Products on page = " + divs.size());
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//a[contains(@class, 'hz-product-card')]"));
					String href = a.getAttribute("href");
					String title = a.findElement(By.xpath(".//span[contains(@class, 'hz-color-link__text')]")).getText();
					productList.put(href, title);
				}
				LOG.log("Searching products - " + productList.size());
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//a[contains(@class,' hz-pagination-link--next')]"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(0);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
				}
			} while (urlNextPage != null);
		}
		
		Set<Holder> exist = new HashSet<Holder>();
		for (Map.Entry<String, String> product : productList.entrySet()) {
			try {
				result.foundItems.add(product.getValue());
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
				// if (result.foundItems.size() < 7) {
				// continue;
				// }
				Collection<Item> items = getDetails(product.getKey(), newMainDriver);
				for (Item item : items) {
					Holder t = new Holder(item);
					if (exist.contains(t)) {
						LOG.log("Skip duplicate item " + item);
						continue;
					}
					result.addItem(item);
					exist.add(t);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS) {
						break;
					}
				}
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with scan main page ", true, newMainDriver.getPageSource());
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.houzz.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utma", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMA)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utmb", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMB)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utmc", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMC)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utmli", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMLI)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utmt", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMT)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "__utmz", Settings.get(Settings.BOTS_HOUZZ_COOKIE_UTMZ)));
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			Thread.sleep(3000);
			WebElement sellerInfoBlock = detailsDriver.findElement(By.xpath("//div[contains(@id,'productSellers')]/div"));
			((RemoteWebDriver) detailsDriver).executeScript("arguments[0].style.height='auto';", sellerInfoBlock);
			String sellerInfo = detailsDriver.findElement(By.xpath("//section[contains(@class,'product-main-seller')]")).getText();
			Matcher matcher = sellerPattern.matcher(sellerInfo);
			if (matcher.find()) {
				LOG.logInfo("Mosaic page");
				return result;
			}
			WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id, 'hz-page-content-wrapper')]"));
			String realTitle = div.findElement(By.xpath("//h1[contains(@class,'view-product-title')]")).getText();
			String testPrice = getTextIfExist(detailsDriver, ("//span[contains(@class, 'product-final-price')]"));
			boolean mustBeMorePrice = testPrice.contains("-") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null || !(div.findElements(By.xpath("//*[contains(@id,'hzProductInfo')]//div[contains(@class,'variationSelectors')]"))).isEmpty();
			boolean hasOptions = false;
			boolean hasSelect = false;
			List<WebElement> options = new ArrayList<WebElement>();
			List<WebElement> optionsBlock = div.findElements(By.xpath("//div[contains(@class, 'product-variations-theme-option')]"));
			if (!optionsBlock.isEmpty()) {
				((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", optionsBlock.get(0));
				Thread.sleep(3000);
				options = detailsDriver.findElements(By.xpath("//div[contains(@class,'product-variations-theme-options')]//ul//li"));
				
			}
			
			if (!options.isEmpty()) {
				hasOptions = true;
				hasSelect = true;
			}
			LOG.log("hasOptions - " + hasOptions);
			LOG.log("hasSelect - " + hasSelect);
			LOG.log("optionSize - " + options.size());
			if (!mustBeMorePrice && !hasOptions) {
				String idBlock = detailsDriver.findElement(By.xpath("//div[contains(@class, 'pre-sales-phone')]")).getText().trim();
				String lid = "";
				matcher = idPattern.matcher(idBlock);
				if (matcher.find()) {
					lid = matcher.group(1);
				} else {
					LOG.logInfo("Not Found id");
				}
				Item item = createItem(href, realTitle, testPrice, lid, detailsDriver);
				result.add(item);
				return result;
			}
			if (!hasSelect) {
				LOG.logError("Not found select " + href);
				return result;
			}
			
			Collection<String> optionValues = new ArrayList<String>();
			for (WebElement opt : options) {
				String value = opt.getText();
				String[] values = value.split("\n");
				optionValues.add(values[0]);
			}
			// optionsBlock = div.findElements(By.xpath("//div[contains(@class,
			// 'variationSelectors')]//..//div[contains(@class,'theme-option--cover')]"));
			// optionsBlock.get(0).click();
			((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", div);
			Thread.sleep(4000);
			for (String optValue : optionValues) {
				optionsBlock = div.findElements(By.xpath("//div[contains(@class, 'product-variations-theme-option')]"));
				optionsBlock.get(0).click();
				WebElement opt = detailsDriver.findElements(By.xpath("//div[contains(@class,'product-variations-theme-options')]//ul//li[contains(., '" + optValue + "')]")).get(0);
				opt.click();
				Thread.sleep(4000);
				String idBlock = detailsDriver.findElement(By.xpath("//div[contains(@class, 'pre-sales-phone')]")).getText().trim();
				String lid = "";
				matcher = idPattern.matcher(idBlock);
				if (matcher.find()) {
					lid = matcher.group(1);
				} else {
					LOG.logInfo("Not Found id");
				}
				testPrice = div.findElement(By.xpath("//span[contains(@class, 'product-final-price')]")).getText();
				String currectTitle = div.findElement(By.xpath("//h1[contains(@class,'view-product-title')]")).getText();
				Item item = createItem(href, currectTitle, testPrice, lid, detailsDriver);
				result.add(item);
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String realTitle, String testPrice, String iid, WebDriver detailsDriver) {
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		String idBlock = detailsDriver.findElement(By.xpath("//div[contains(@class, 'pre-sales-phone')]")).getText().trim();
		String lid = "";
		Matcher matcher = idPattern.matcher(idBlock);
		if (matcher.find()) {
			lid = matcher.group(1);
		} else {
			LOG.logInfo("Not Found id");
		}
		item.SKU = lid;
		item.qty = detailsDriver.findElements(By.xpath("//select[@class='hz-dropdown__select']/option")).size();
		// String testUPC = ((RemoteWebDriver)
		// detailsDriver).executeScript("return HZ.data.Spaces.get(" + iid +
		// ").l").toString();
		item.UPC = getUPC(item, detailsDriver, iid, realTitle);
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'view-product-image')]//div[contains(@class, 'alt-images__thumb')]//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		WebElement modificationBlock = detailsDriver.findElement(By.xpath("//section[contains(@class, 'product-specification')]//button"));
		modificationBlock.click();
		List<WebElement> sizes = detailsDriver.findElements(By.xpath("//div[contains(@id, 'productSpecification')]//dl[contains(@class, 'view-product-specs__list')]//dt[contains(., 'Size/Weight')]/following-sibling::dd[1]"));
		if (!sizes.isEmpty()) {
			String size = sizes.get(0).getText();
			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		}
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver, String iid, String realTitle) {
		String newIid = "";
		String upc = "";
		// Matcher matcher = upcPattern.matcher(testUPC);
		// if (matcher.find()) {
		// upc = upc + " " + matcher.group(1);
		// } else{
		newIid = ((RemoteWebDriver) detailsDriver).executeScript("return Object.keys(HZ.ctx.data.stores.data.VendorListingsStore.data[" + iid + "])[0]").toString(); // HZ.data.SpaceListingIds.get("
																																										// +
																																										// iid
																																										// +
																																										// ")[0]
		upc = ((RemoteWebDriver) detailsDriver).executeScript("return HZ.ctx.data.stores.data.VendorListingsStore.data[" + iid + "][" + newIid + "].productCode").toString(); // HZ.data.ListingsInfo.get("
																																												// +
																																												// newIid
																																												// +
																																												// ").sku
		if (upc.isEmpty()) {
			upc = iid;
		}
		// }
		return item.resolveUPC(upc, null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "Houzz.com";
	}
}
