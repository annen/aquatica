package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class MenardsSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.menards.com/main/search.html?sf_brandName=Aquatica&ipp=500";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern upcPattern = Pattern.compile("(627722\\d{6})", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern imagePattern = Pattern.compile("(.*main/).*(items/.*)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public MenardsSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		WebDriver newMainDriver = DRIVER.getPlatformDriverWithProxy(false);
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			if (!checkCaptcha(newMainDriver)) {
				break;
			}
			String urlNextPage;
			do {
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//div[@class='search-item']"));
				if (divs.isEmpty()) {
					break;
				}
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//div[contains(@class,'details')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					productList.put(href, title);
				}
				
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//ul[contains(@class,'paginationList')]//li//a[@class='fa fa-chevron-right ']"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(0);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
					LOG.log("Next page loaded");
					sleep1Sec();
					if (!checkCaptcha(newMainDriver)) {
						break;
					}
					sleep3Sec();
				}
				
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			result.foundItems.add(product.getValue());
			LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
			// if (result.foundItems.size() < 57) {
			// continue;
			// }
			Collection<Item> items = getDetails(product.getKey(), newMainDriver);
			for (Item item : items) {
				result.addItem(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
			checkCaptcha(detailsDriver);
			Item item = createItem(href, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id,'content')]"));
		String realTitle = div.findElements(By.xpath("//h3[contains(@data-at-id,'itemTitle')]")).get(0).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[@id='priceContainer']//span[contains(@class, 'pr-md-5')]//span[contains(@class, 'h1')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		List<WebElement> skuBlock = div.findElements(By.xpath("//h4[@class='modelSKU']/span"));
		item.SKU = skuBlock.get(0).getText().replace("Model Number:", "").replace("|  Variation: White", "").trim();
		String source = detailsDriver.getPageSource();
		
		item.qty = 0;
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class, 'itemImageSlider')]//div[contains(@class, 'slick-slide')]//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				String src = img.getAttribute("src");
				Matcher m = imagePattern.matcher(src);
				if (m.find()) {
					item.images.add(m.group(1) + m.group(2));
				}
			}
		} else {
			WebElement img = div.findElement(By.xpath("//div[contains(@id, 'mainPic')]//img"));
			item.images.add(img.getAttribute("src"));
			
		}
		item.UPC = getUPC(item, detailsDriver, source);
		
		String size = getTextIfExist(detailsDriver, "//div[contains(@id, 'descriptDocs')]/p[contains(.,'Dimensions:')]");
		size = size.toLowerCase().trim().replace("inches", "").replace("Dimensions:", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "").replace(":", " x ");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver, String source) {
		String upc = "";
		Matcher matcher = upcPattern.matcher(source);
		if (matcher.find()) {
			upc = matcher.group(1);
		}
		
		return item.resolveUPC(upc, null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	public boolean checkCaptcha(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//iframe[contains(.,'Request unsuccessful')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CAPTCHA COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CAPTCHA");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//iframe[contains(.,'Request unsuccessful')]")).isEmpty();
	}
	
	@Override
	public String toString() {
		return "Menards";
	}
}
