﻿package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep1Sec;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.util.Strings;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class OverStockSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.overstock.com/Aquatica,/brand,/results.html";
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public OverStockSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		Map<String, String> productList = new LinkedHashMap<String, String>();
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			sleep3Sec();
			LOG.log("Parse main page products");
			int i = 0;
			
			String urlNextPage;
			do {
				
				List<WebElement> resultsElements = newMainDriver.findElements(By.xpath("//div[contains(@id,'product-container')]"));
				while (resultsElements.isEmpty()) {
					i++;
					sleep1Sec();
					if (i > 60) {
						LOG.logError("Not found products");
						if (listener != null) {
							listener.allFinish(result);
						}
						return result;
					}
					if (i % 10 == 0) {
						LOG.logInfo("Waiting Page load");
					}
				}
				
				WebElement resultsElement = resultsElements.get(0);
				List<WebElement> items = resultsElement.findElements(By.xpath(".//div[contains(@class, 'product-tile')]"));
				for (WebElement a : items) {
					String title = a.findElement(By.xpath(".//div[contains(@class, 'product-title')]")).getText();
					String href = a.findElement(By.xpath(".//a[contains(@class, 'product-link')]")).getAttribute("href");
					productList.put(href, title);
				}
				
				LOG.log("Finish searching products - " + productList.size());
				
				LOG.log("Searching next page");
				
				urlNextPage = null;
				List<WebElement> nextElements = newMainDriver.findElements(By.xpath("//div[contains(@class,'next-wrapper')]//a"));
				if (!nextElements.isEmpty()) {
					WebElement nextElement = nextElements.get(0);
					((RemoteWebDriver) newMainDriver).executeScript("window.scroll(" + 0 + "," + (nextElement.getLocation().y - 150) + ")", nextElement);
					sleep3Sec();
					urlNextPage = nextElement.getAttribute("href");
					newMainDriver.get(urlNextPage);
					sleep1Sec();
				}
			} while (urlNextPage != null);
		}
		for (Map.Entry<String, String> product : productList.entrySet()) {
			try {
				result.foundItems.add(product.getValue());
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + product.getValue());
				// if (result.foundItems.size() < 26) {
				// continue;
				// }
				Collection<Item> itemsAq = getDetails(product.getKey(), newMainDriver);
				for (Item item : itemsAq) {
					result.addItem(item);
					if (result.foundItems.size() >= MAX_ITEMS) {
						break;
					}
				}
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with scan main page ", true, mainDriver.getPageSource());
			}
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			WebElement div = detailsDriver.findElement(By.id("mainContent"));
			String realTitle = div.findElement(By.xpath(".//div[contains(@class, 'product-title')]/h1")).getText();
			String testPrice = getTextIfExist(detailsDriver, ".//div[@id='pricing-container']//span[contains(@itemprop, 'price')]//..//span[contains(@class, 'dollars')]");
			String selectSize = "";
			if (testPrice.isEmpty()) {
				testPrice = "0.0";
			}
			boolean mustBeMorePrice = testPrice.contains("-") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null || ((RemoteWebDriver) detailsDriver).executeScript("return os.productpage.hasMultipleOptions").toString().equalsIgnoreCase("true");
			
			boolean hasOptionSelections = false;
			boolean hasOptions = false;
			boolean hasSelect = false;
			List<WebElement> options = div.findElements(By.xpath("//div[contains(@class,'control')]//select[contains(@class,'options-dropdown')]"));
			if (!options.isEmpty()) {
				hasOptions = true;
				hasSelect = true;
			}
			List<WebElement> optionSelections = div.findElements(By.xpath("//div[contains(@class,'option-breakout')]//..//div[contains(@class,'facets')]//*[contains(@class,'facet')]"));
			if (!optionSelections.isEmpty()) {
				hasOptionSelections = true;
				hasSelect = true;
			}
			
			if (!mustBeMorePrice && !hasOptions) {
				String sku = getTextIfExist(detailsDriver, "//h2[contains(.,'Specs')]/../div//table//tr//td[1][contains(.,'Model Number')]/../td[2]");
				Item item = createItem(href, realTitle, testPrice, sku, "", "", detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			
			int i = 0;
			if (mustBeMorePrice && hasOptionSelections) {
				Collection<String> values = new ArrayList<String>();
				for (WebElement option : optionSelections) {
					String value = option.findElement(By.tagName("span")).getAttribute("textContent");
					if (!value.equals("0") && !value.isEmpty()) {
						values.add(value);
					}
				}
				
				for (String value : values) {
					WebElement option = detailsDriver.findElement(By.xpath("//div[contains(@class,'option-breakout')]//..//div[contains(@class,'facets')]//*[contains(@class,'facet')]//span[contains(.,'" + value + "')]"));
					option.click();
					List<WebElement> attributesField = detailsDriver.findElements(By.xpath("//div[contains(@class,'facet-group')]//..//div[contains(@class,'title')]"));
					String selectedSize = "";
					String selectedFinish = "";
					if (attributesField.size() > 0) {
						if (attributesField.get(0).getText().contains("Size")) {
							selectedSize = value;
						} else {
							selectedFinish = value;
						}
					}
					realTitle = getTextIfExist(detailsDriver, ".//div[contains(@class, 'product-title')]/h1");
					testPrice = getTextIfExist(detailsDriver, ".//div[@id='pricing-container']//span[contains(@itemprop, 'price')]//..//span[contains(@class, 'dollars')]");
					if (testPrice.isEmpty()) {
						testPrice = "0.0";
					}
					String sku = ((RemoteWebDriver) detailsDriver).executeScript("return  os.productpage.dropDownOptions[" + i + "].vendorSku;").toString().trim();
					Item item = createItem(href, realTitle, testPrice, sku, selectedSize, selectedFinish, detailsDriver);
					result.add(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					i++;
				}
				return result;
			}
			
			if (!hasSelect) {
				LOG.logError("Not found select " + href);
				return result;
			}
			if (options.size() != 1) {
				LOG.logError("Expected one select " + href);
				return result;
			}
			
			// detailsDriver.executeScript("arguments[0].setAttribute('style',
			// 'display:block')",
			// options.get(0).findElement(By.xpath("..")));
			Map<String, String> selectedStrings = new HashMap<String, String>();
			Select select = new Select(options.get(0));
			for (WebElement option : select.getOptions()) {
				String value = option.getAttribute("value");
				if (value == null || value.trim().length() == 0) {
					value = "0";
				}
				if ("0".equals(value.trim())) {
					continue;
				}
				selectedStrings.put(value, option.getText());
			}
			for (Entry<String, String> entry : selectedStrings.entrySet()) {
				String value = entry.getValue();
				select.selectByVisibleText(value);
				Thread.sleep(1000);
				if (value.contains("$")) {
					testPrice = value.substring(value.indexOf("$")).trim();
				} else {
					testPrice = getTextIfExist(detailsDriver, ".//div[@id='pricing-container']//span[contains(@itemprop, 'price')]//..//span[contains(@class, 'dollars')]");
				}
				if (testPrice.isEmpty()) {
					testPrice = "0.0";
				}
				String sku = ((RemoteWebDriver) detailsDriver).executeScript("return  os.productpage.dropDownOptions[" + i + "].vendorSku;").toString().trim();
				if (value.contains("in.")) {
					selectSize = "x " + value.replace("-", "").replace(testPrice, "").replace("Price in Cart", "").replace("Dimensions:", "").trim();
				}
				Item item = createItem(href, realTitle + " -> " + value, testPrice, sku, selectSize, "", detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				select.deselectByVisibleText(value);
				i++;
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String realTitle, String testPrice, String key, String selectSize, String finish, WebDriver detailsDriver) {
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = detailsDriver.findElement(By.xpath("//div[contains(@class,'item-number')]")).getText().replaceAll("[^0-9.-]", "").trim();
		
		item.qty = detailsDriver.findElements(By.xpath("//select[contains(@name,'addqty')]//option")).size();
		String size = "";
		if (!selectSize.isEmpty()) {
			size = selectSize;
		} else {
			if (realTitle.contains("Dimensions:")) {
				size = realTitle.replace("Dimensions:", " x ");
			} else {
				size = getTextSize(detailsDriver);
			}
		}
		size = size.toLowerCase().trim().replace("inches", "").replace("in.", "").replace("in", "").replace("\"", "");
		
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		item.UPC = getUPC(item, detailsDriver, key, finish, selectSize);
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'image-gallery')]//..//div[contains(@class, 'thumb-frame')]//ul//li//img")); // �����
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		return item;
	}
	
	public String getTextSize(WebDriver detailsDriver) {
		String size = getTextIfExist(detailsDriver, "//td[contains(.,'Dimensions')]/../td[2]").replace("See Details", "").replace("See Description", "").trim();
		if (!Strings.isNullOrEmpty(size)) {
			if (size.contains("L") || size.contains("W") || size.contains("H"))
				return " x " + size;
			size = size.toLowerCase();
			size = size.replaceFirst("in.", "L");
			size = size.replaceFirst("in.", "W");
			size = size.replaceFirst("in.", "H");
			return " x " + size;
		}
		size = getTextIfExist(detailsDriver, "//span[contains(.,'External Dimensions:')]").replace("Dimensions:", " x ");
		if (!Strings.isNullOrEmpty(size))
			return size;
		size = getTextIfExist(detailsDriver, "//li[contains(.,'Faucet height')]").replace(":", " x ");
		if (!Strings.isNullOrEmpty(size)) {
			size = size + " H";
			return size;
		}
		size = getTextIfExist(detailsDriver, "//li[contains(.,'Dimensions')]").replace(":", " x ");
		if (size.isEmpty()) {
			size = getTextIfExist(detailsDriver, "//li[contains(.,'dimension')]").replace(":", " x ");
		}
		
		if (!Strings.isNullOrEmpty(size)) {
			size = size.replace("long long", "long").replace("long", "L").replace("deep", "L").replace("wide ", "W").replace("high ", "H").replace(" 3/4", ".75");
			size = size + " H";
			return size;
		}
		return "";
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim();
	}
	
	private String getUPC(Item item, WebDriver detailsDriver, String key, String finish, String selectSize) {
		// String text = "";
		// List<WebElement> tds =
		// detailsDriver.findElements(By.xpath("//h2[contains(.,'Specs')]/../div//table//tr//td[1][contains(.,'Model
		// Number')]/../td[2]"));
		// if (tds.size() > 0)
		// text = (tds.get(0).getText() + " " + finish + " " +
		// selectSize).trim();
		// if (text.equals("Inflection-A-Rect-Wht")) { // Wrong SKU
		// //
		// https://www.overstock.com/Home-Garden/Aquatica-Inflection-Freestanding-Cast-Stone-Bathtub/9378512/product.html?keywords=16568939&searchtype=Header
		// text = "Infl-A-F-Wht";
		// }
		// if (text.equals("PureScape 204A-M")) { // Wrong SKU
		// //
		// https://www.overstock.com/Home-Garden/Aquatica-Spoon-2-Purescape-204AM-Egg-Shaped-Freestanding-Solid-Surface-Bathtub/7263406/product.html
		// text = "PS204C-M";
		// }
		// if (key.equals("14180859")) { // Wrong SKU
		// //
		// https://www.overstock.com/Home-Garden/Aquatica-Pamela-Wht-Acrylic-Bathtub/9542340/product.html?refccid=USVUEJBK72CLJVKKJ5AXIPAHVM&searchidx=42
		// text = "Pamela-Wht-Relax";
		// }
		//
		// if (key.equals("24898937")) { // Wrong SKU
		// //
		// https://www.overstock.com/Home-Garden/Aquatica-Spoon-2-Egg-Shaped-Solid-Surface-Bathtub/14948656/product.html?refccid=RKE3KRKWREB3FDKG24QJMN7BYA&searchidx=44&keywords=&refinement=brand%3AAquatica
		// item.SKU = text;
		// text = "Spoon2M-Blck";
		// }
		//
		// if (key.equals("24898938")) { // Wrong SKU
		// //
		// https://www.overstock.com/Home-Garden/Aquatica-Spoon-2-Egg-Shaped-Solid-Surface-Bathtub/14948656/product.html?refccid=RKE3KRKWREB3FDKG24QJMN7BYA&searchidx=44&keywords=&refinement=brand%3AAquatica
		// item.SKU = text;
		// text = "Spoon2M-Blck-Wht";
		// }
		//
		// text = text + " " + key;
		return item.resolveUPC(key, null);
	}
	
	@Override
	public String toString() {
		return "OverStock.com";
	}
}

// if (mustBeMorePrice && hasOptionSelections) {
// LOG.log("Additional Options found");
// Collection<String> values = new ArrayList<String>();
// for (WebElement option : options) {
// String value = option.getAttribute("data-name");
// if (!value.equals("0")) {
// values.add(value);
// }
// }
// for (String value : values) {
// WebElement divSelect =
// detailsDriver.findElement(By.xpath("//section[contains(@id,'optionSelection')]//a[@data-name='"
// + value + "']"));
// divSelect.click();
// sleep1Sec();
// realTitle = div.findElement(By.xpath(".//div[contains(@itemprop,
// 'name')]/h1")).getText();
// testPrice = getTextIfExist(detailsDriver,
// ".//div[@id='pricing-container']//span[contains(@itemprop,
// 'price')]//..//span[contains(@class, 'dollars')]");
// selectSize = "";
// if (testPrice.isEmpty()) {
// testPrice = "0.0";
// }
// String sku = detailsDriver.executeScript("return
// os.productpage.productId;").toString().replaceAll("[^0-9.-]", "").trim();
// Item item = createItem(href, realTitle, testPrice, sku, "", detailsDriver);
// result.add(item);
// if (listener != null) {
// listener.itemFinish(item);
// }
// return result;
// }
// }
