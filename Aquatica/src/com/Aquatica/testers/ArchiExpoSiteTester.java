package com.Aquatica.testers;

import static com.Aquatica.Utils.getProxyUrl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class ArchiExpoSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.archiexpo.com/prod/aquatica-plumbing-group-68046.html";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public ArchiExpoSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(getProxyUrl(mainPage));
			
			List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@class,'group-product-list-item')]");
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//span[contains(@class,'product-thumb-title')]/.."));
				String href = a.getAttribute("href");
				String title = a.getText();
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
				// if (result.foundItems.size() < 6) {
				// continue;
				// }
				for (Item item : getDetails(href, detailsDriver)) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(getProxyUrl(href));
			String description = detailsDriver.findElement(By.xpath("//span[contains(@id,'product-description')]")).getText();
			
			if (description.contains("Avaliable item:")) {
				int index = description.indexOf("Avaliable item:");
				int brIndex = description.indexOf("\n", index);
				if (brIndex < 0) {
					brIndex = description.length();
				}
				String finish = description.substring(index, brIndex).replace("Avaliable item:", "").trim();
				Item item = createItem(href, finish, detailsDriver);
				result.add(item);
				return result;
			}
			
			if (description.contains("Avaliable items:")) {
				int index = description.indexOf("Avaliable items:");
				int brIndex = description.indexOf("\n", index);
				if (brIndex < 0) {
					brIndex = description.length();
				}
				String finish = description.substring(index, brIndex).replace("Avaliable items:", "").trim();
				String finishes[];
				finishes = finish.split(",");
				for (String presentFinish : finishes) {
					Item item = createItem(href, presentFinish.trim(), detailsDriver);
					result.add(item);
					
				}
			} else {
				Item item = createItem(href, "", detailsDriver);
				result.add(item);
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String finish, AquaticaDriver detailsDriver) throws Exception {
		String realTitle = detailsDriver.findElement(By.xpath("//h1[contains(@class,'stand-description-title')]/span")).getText().trim();
		if (!finish.isEmpty()) {
			realTitle += " -> " + finish.replace(".", "").replace(";", "").trim();
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = 0.0;
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//li[contains(@class,'picture-thumbnail')]/a/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, finish, detailsDriver);
		item.SKU = detailsDriver.findElement(By.xpath("//div[contains(@class,'add-bookmark-button')]")).getAttribute("data-entity-id");
		return item;
	}
	
	private String getUPC(Item item, String finish, AquaticaDriver detailsDriver) {
		return item.resolveUPC(finish, null);
	}
	
	@Override
	public String toString() {
		return "ArchiExpo";
	}
}
