package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;
import com.google.common.base.Strings;

public class HomeClickSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.homeclick.com/products/aquatica.aspx?n=109+20002251";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public HomeClickSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			// Thread.sleep(2000);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//a[contains(@class,'box')]");
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//span[contains(@class,'title')]"));
					String href = div.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 45) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsById("_ctl0__ctl0_bodyPlaceHolder_mainContent_lnkNext");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> options = detailsDriver.findElementsByXPath("//select[contains(@class,'options')]/option");
			
			if (options.size() < 2) {
				Item item = createItem(href, "", 0.0, detailsDriver);
				result.add(item);
				
				return result;
			}
			
			if (!options.isEmpty()) {
				String[] valuesMas;
				String finish = "";
				Double finishPrice = 0.0;
				for (WebElement option : options) {
					String text = option.getText();
					String value = option.getAttribute("value");
					if (Strings.isNullOrEmpty(value)) {
						continue;
					}
					
					if (text.contains("+")) {
						valuesMas = text.split("\\+");
						finish = valuesMas[0].trim();
						finishPrice = Utils.tryParseDouble(valuesMas[1], 0.0);
						Item item = createItem(href, finish, finishPrice, detailsDriver);
						result.add(item);
					} else {
						finish = text.trim();
						Item item = createItem(href, finish, 0.0, detailsDriver);
						result.add(item);
					}
					
				}
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String finish, Double finishPrice, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@id,'vp-main')]"));
		WebElement divbottom = detailsDriver.findElement(By.xpath("//div[contains(@id,'vp-bottom-left-container')]"));
		String realTitle = div.findElement(By.xpath("//span[contains(@id,'lblProductTitle')]")).getText().trim();
		if (!Strings.isNullOrEmpty(finish)) {
			realTitle = realTitle + " -> " + finish;
		}
		
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//div[contains(@class,'vp-price-total')]/span"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0) + finishPrice;
		item.SKU = detailsDriver.executeScript("return pr_page_id").toString();
		
		List<WebElement> sizeSelect = div.findElements(By.id("availabilityMessageCnt"));
		if (!sizeSelect.isEmpty()) {
			item.qty = Utils.tryParseInt(sizeSelect.get(0).getText().replace("In Stock", ""), 0);
		} else {
			item.qty = 0;
			
		}
		
		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'tnGImg')]/img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("data-blzsrc"));
			}
		} else {
			WebElement img = div.findElement(By.xpath("//img[contains(@id,'ProductImage')]"));
			item.images.add(img.getAttribute("data-blzsrc"));
		}
		item.UPC = getUPC(item, detailsDriver, finish);
		
		String l = "0", h = "0", w = "0";
		List<WebElement> elementL = divbottom.findElements(By.xpath("//td[contains(.,'Length:')]"));
		List<WebElement> elementW = divbottom.findElements(By.xpath("//td[contains(.,'Width:')]"));
		List<WebElement> elementH = divbottom.findElements(By.xpath("//td[contains(.,'Height:')]"));
		
		for (WebElement element : elementL) {
			if (!element.getText().contains("Interior")) {
				l = element.getText();
			}
		}
		for (WebElement element : elementW) {
			if (!element.getText().contains("Interior")) {
				w = element.getText();
			}
		}
		for (WebElement element : elementH) {
			if (!element.getText().contains("Interior") && !element.getText().contains("Faucet")) {
				h = element.getText();
			}
		}
		
		String size = "x " + l.trim().replace("Length: ", "").replace("Exterior ", "") + " L x " + w.trim().replace("Width: ", "").replace("Exterior ", "") + " W x " + h.trim().replace("Height: ", "").replace("Exterior ", "") + " H";
		size = size.toLowerCase().trim().replace("-1/4", ".25").replace("-3/4", ".75").replace("-1/2", ".5").replace("-1/3", ".3").replace("-1/5", ".2").replace("-2/5", ".4").replace("-3/5", ".6").replace("-4/5", ".8").replace("-2/3", ".6").replace("-5/8", ".62").replace("-8/9", ".9").replace("-7/9", ".8").replace("-1/10", ".1");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver, String finish) {
		String realtitle = getTextIfExist(detailsDriver, "//div[contains(@id,'divSpecs')]//h2").replace("specifications", "");
		if (!Strings.isNullOrEmpty(finish)) {
			realtitle = getTextIfExist(detailsDriver, "//td[@class='vp-sku-item'][contains(.,'" + finish + "')]");
		}
		return item.resolveUPC(realtitle, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "HomeClick";
	}
}
