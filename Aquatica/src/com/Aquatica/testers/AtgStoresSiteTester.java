package com.Aquatica.testers;

import static com.Aquatica.Utils.changeToTab1;
import static com.Aquatica.Utils.changeToTab2;
import static com.Aquatica.Utils.createNewTab;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class AtgStoresSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.atgstores.com/search/default.aspx?term=Aquatica";
	private static final Pattern heightPattern = Pattern.compile("\\|Height\\s*:\\s*([^|]+|)");
	private static final Pattern wigthPattern = Pattern.compile("\\|Width\\s*:\\s*([^|]+|)");
	private static final Pattern lengthPattern = Pattern.compile("\\|Length\\s*:\\s*([^|]+|)");
	//private static final Pattern proxyPattern = Pattern.compile("\"result\"\\s*:\\s*\"([^\"]+)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public AtgStoresSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		WebDriver newMainDriver = DRIVER.getPlatformDriverWithProxy();
		createNewTab(newMainDriver);
		changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(getProxyUrl(mainPage));
			// Thread.sleep(1000);
			// newMainDriver.findElement(By.xpath("//*[@id='grid']")).click();
			// Thread.sleep(1000);
			LOG.log("Parse main page products");
			List<WebElement> nextElement;
			do {
				// WebElement resultsElement =
				// newMainDriver.findElement(By.xpath("//*[@id='divProdDisplay1']"));
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//div[contains(@class, 'products')]//div[contains(@class, 'product')]"));
				for (WebElement div : divs) {
					try {
						WebElement a = div.findElement(By.xpath(".//div[contains(@class, 'prod-name')]//a"));
						String href = a.getAttribute("href");
						String title = a.getText();
						// if (!AQUATICA.equalsIgnoreCase(brand)) {
						// LOG.logWarning("Bad brand -> " + brand + " " +
						// title);
						// continue;
						// }
						result.foundItems.add(title);
						LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//						if (result.foundItems.size() < 39) {
//							continue;
//						}
						changeToTab2(newMainDriver);
						Collection<Item> items = getDetails(href, newMainDriver);
						changeToTab1(newMainDriver);
						for (Item item : items) {
							result.addItem(item);
							if (result.foundItems.size() >= MAX_ITEMS) {
								break;
							}
						}
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with scan page " + mainPage);
					}
				}
				nextElement = newMainDriver.findElements(By.xpath("//li[@class = 'ng-scope']//.//i[contains(@class, 'icon-arrow-right')]"));
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					nextElement.get(0).click();
					LOG.log("Loaded new page");
					Thread.sleep(5000);
				}
			} while (!nextElement.isEmpty());
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			WebElement div = null;
			int step = 0;
			while (div == null) {
				step++;
				if (step % 20 == 0) {
					detailsDriver.get(href);
				}
				Thread.sleep(500);
				List<WebElement> tests = detailsDriver.findElements(By.xpath("//*[@id='divPriceBlock']"));
				if (!tests.isEmpty()) {
					div = tests.get(0);
				}
				if (step > 60) {
					LOG.logError("Problem when load page " + href);
					return result;
				}
			}
			
			String realTitle = detailsDriver.findElement(By.xpath("//div[@id='prodTitle']/h1[@id='prodName']")).getText();
			String testPrice = div.findElement(By.xpath(".//div[@id='divPrice']")).getText();
			boolean mustBeMorePrice = testPrice.contains("-") || testPrice.contains("to") || tryParseDouble(testPrice, null) == null;
			boolean hasOptions = false;
			boolean hasSelect = false;
			
			WebElement divOption = div.findElement(By.xpath(".//div[@id='tblOptions']"));
			List<WebElement> options = divOption.findElements(By.xpath(".//select[contains(@id,'selOpt0')]"));
			if (!options.isEmpty()) {
				hasOptions = true;
				hasSelect = true;
			}
			
			if (!mustBeMorePrice && !hasOptions) {
				Item item = createItem(href, realTitle, testPrice, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				return result;
			}
			
			if (!hasSelect) {
				LOG.logError("Not found select " + href);
				return result;
			}
			if (options.size() != 1) {
				LOG.logError("Expected one select " + href);
				return result;
			}
			((RemoteWebDriver) detailsDriver).executeScript("arguments[0].setAttribute('style', 'display:block')", options.get(0).findElement(By.xpath("..")));
			
			Collection<String> selectedStrings = new ArrayList<String>();
			Select select = new Select(options.get(0));
			for (WebElement option : select.getOptions()) {
				String value = option.getAttribute("value");
				if (value == null || value.trim().length() == 0) {
					value = "0";
				}
				if ("0".equals(value.trim())) {
					continue;
				}
				selectedStrings.add(value);
			}
			String sku = detailsDriver.findElement(By.xpath("//*[@id='spnPartNumber']")).getText().replaceAll("[^0-9.-]", "").trim();
			String newSku = "";
			String oldSku = "";
			String strKey = "";
			String oldStrKey = "";
			for (String text : selectedStrings) {
				select.selectByValue(text);
				boolean check;
				int i = 0;
				do {
					Thread.sleep(300);
					i++;
					if (i % 15 == 0) {
						select.selectByValue(text);
					}
					if (i == 60) {
						LOG.logError("Problem with page " + href);
						break;
					}
					newSku = detailsDriver.findElement(By.xpath("//*[@id='spnPartNumber']")).getText().replaceAll("[^0-9.-]", "").trim();
					boolean priceNotExist = div.findElements(By.xpath(".//div[@id='divPrice']")).isEmpty();
					strKey = (String) ((RemoteWebDriver) detailsDriver).executeScript("return strKey");
					check = priceNotExist || sku.equals(newSku) || strKey.equals("0") || newSku.equals(oldSku) || (!strKey.isEmpty() && strKey.equals(oldStrKey));
				} while (check);
				oldSku = newSku;
				oldStrKey = strKey;
				// Thread.sleep(260);
				
				testPrice = div.findElement(By.xpath(".//div[@id='divPrice']")).getText();
				Item item = createItem(href, realTitle + " -> " + select.getFirstSelectedOption().getText(), testPrice, detailsDriver);
				result.add(item);
				if (listener != null) {
					listener.itemFinish(item);
				}
				select.deselectByValue(text);
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href);
		}
		return result;
	}
	
	public Item createItem(String href, String realTitle, String testPrice, WebDriver detailsDriver) {
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = detailsDriver.findElement(By.xpath("//*[@id='spnPartNumber']")).getText().replaceAll("[^0-9.-]", "").trim();
		String optKey = String.valueOf(((RemoteWebDriver) detailsDriver).executeScript("return arrOptions[arguments[0]].optionKey;", item.SKU));
		item.qty = Utils.tryParseInt(((RemoteWebDriver) detailsDriver).executeScript("return arrOpt[arguments[0]].mfrStk;", optKey), 0);
		String size = "|" + String.valueOf(((RemoteWebDriver) detailsDriver).executeScript("return arrOpt[arguments[0]].d;", optKey));
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		item.UPC = getUPC(item, detailsDriver);
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@id,'altImg')][not(contains(@class,'altVideoImg'))]/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		return item;
	}
	
	private String getUPC(Item item, WebDriver detailsDriver) {
		String upc = (String) ((RemoteWebDriver) detailsDriver).executeScript("return arrOpt[strKey].upc");
		if (upc == null || !upc.startsWith("627722")) {
			LOG.logWarning("In product invalid upc (" + upc + ") " + item.href);
			return "";
		}
		
		String partNo = String.valueOf(((RemoteWebDriver) detailsDriver).executeScript("return arrOpt[arrOptions[arguments[0]].optionKey].partNo;", item.SKU));
		item.resolveUPC(partNo, Arrays.asList(upc));
		
		return upc;
	}
	
	private static String getProxyUrl(String url) {
		// try {
		// String newUrl = IOUtils.toString(new
		// URI("http://noblockme.ru/api/anonymize?url=" + URLEncoder.encode(url,
		// Charsets.UTF_8.name())));
		// Matcher m = proxyPattern.matcher(newUrl);
		// if (!m.find())
		// throw new RuntimeException("Not found new proxy url");
		// return m.group(1);
		// } catch (Exception ex) {
		// LOG.logError(ex, "Problem with get proxy url " + url);
		// return "";
		// }
		return url;
	}
	
	@Override
	public String toString() {
		return "ATG Stores";
	}
}
