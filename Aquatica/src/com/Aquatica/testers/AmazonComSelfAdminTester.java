package com.Aquatica.testers;

import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;

import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.Aquatica.Utils.changeToTab1;
import static com.Aquatica.Utils.changeToTab2;
import static com.Aquatica.Utils.createNewTab;
import static com.Aquatica.Utils.sleep1Sec;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class AmazonComSelfAdminTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://sellercentral.amazon.com/inventory/ref=ag_invmgr_dnav_xx";
	
	private static final Pattern qtyPattern = Pattern.compile("Only\\s+(\\d+)\\s+left\\s+in\\s+stock", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*[H|D]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private static final Pattern pricePattern = Pattern.compile("\\$(\\d.)+", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern WFSizeInfoPattern = Pattern.compile("initialize[^\\{]+\\{\\s+var\\s+iframeContent\\s+=\\s+\"([^\"]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern WFSizePattern = Pattern.compile("Length.+Side: -([\\d\\.]+)\".+Width.*Back: -([\\d\\.]+)\".*Depth.*Bottom: -([\\d\\.]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public AmazonComSelfAdminTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		setCookie(detailsDriver);
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		createNewTab(newMainDriver);
		changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			
			List<WebElement> authorizationBlock = newMainDriver.findElements(By.xpath("//div[contains(@id,'authportal-center-section')]"));
			if (!authorizationBlock.isEmpty()) {
				WebElement inputBlock = authorizationBlock.get(0);
				WebElement loginField = inputBlock.findElement(By.id("ap_email"));
				WebElement passwordField = inputBlock.findElement(By.id("ap_password"));
				loginField.clear();
				loginField.sendKeys(Settings.get(Settings.AMAZONADMIN_LOGIN));
				passwordField.clear();
				passwordField.sendKeys(Settings.get(Settings.AMAZONADMIN_PASSWORD));
				sleep1Sec();
				WebElement buttonSubmit = inputBlock.findElement(By.id("signInSubmit"));
				buttonSubmit.click();
				sleep3Sec();
				sleep3Sec();
				if (!checkAuthorizationCode(newMainDriver)) {
					break;
				}
			}
			List<WebElement> nextElement = null;
			do {
				int i = 0;
				String countProducts = newMainDriver.findElements(By.xpath("//span[contains(@id,'count-value')]")).get(0).getText();
				LOG.log("Products on page = " + countProducts);
				List<WebElement> divs = newMainDriver.findElements(By.xpath("//tr[contains(@class,'mt-row')]/td[contains(@id,'title')]"));
				do {
					LOG.log("Loaded products = " + divs.size());
					LOG.logInfo("Loading products");
					sleep3Sec();
					divs = newMainDriver.findElements(By.xpath("//tr[contains(@class,'mt-row')]"));
					i++;
					if (i == 40) {
						LOG.logError("Sorry I am Blocked :( ");
						if (listener != null) {
							listener.allFinish(result);
						}
					}
				} while (!tryParseDouble(divs.size(), 0.0).equals(tryParseDouble(countProducts, 0.0)));
				
				if (divs.isEmpty()) {
					LOG.logError("Sorry I am Blocked :( ");
					if (listener != null) {
						listener.allFinish(result);
					}
					return result;
				}
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//td[contains(@data-column,'title')]//a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 45) {
					// continue;
					// }
					changeToTab2(newMainDriver);
					Collection<Item> itemsAq = getDetails(href, newMainDriver);
					changeToTab1(newMainDriver);
					for (Item item : itemsAq) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				nextElement = newMainDriver.findElements(By.xpath("//li[contains(@class,'a-last')]/a"));
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED NEXT PAGE BUTTON");
					}
					nextElement.get(0).click();
					sleep3Sec();
					
				}
			} while (!nextElement.isEmpty());
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			sleep3Sec();
//			WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
//			List<WebElement> options = div.findElements(By.xpath(".//ul[contains(@class,'swatches')]/li"));
//			if (options.isEmpty() || options.get(0).getText().trim().contains("Without expert installation") || options.get(0).getText().trim().contains("Without expert setup")) {
				String availability = detailsDriver.findElement(By.xpath(".//div[contains(@id,'availability')]/span")).getText().replace(".", "");
				if (availability.contains("Currently unavailable")) {
					Item item = createItem(href, detailsDriver, "0.0", availability);
					result.add(item);
					return result;
				}
				WebElement newLink = detailsDriver.findElement(By.xpath("//div[contains(@id,'olp_feature_div')]//a"));
				String newHref = newLink.getAttribute("href");
				detailsDriver.get(newHref);
				sleep3Sec();
				List<WebElement> aquaticaSeller = detailsDriver.findElements(By.xpath("//div[contains(@class,'olpOffer')]/div[contains(@class,'olpSellerColumn')]/h3[contains(.,'Aquatica')]//..//..//div[1]/span[contains(@class,'olpOfferPrice')]"));
				String price = "";
				if (aquaticaSeller.isEmpty()) {
					availability = "Currently unavailable";
				} else {
					price = aquaticaSeller.get(0).getText();
					availability = "";
				}
				detailsDriver.get(href);
				sleep3Sec();
				Item item = createItem(href, detailsDriver, price, availability);
				result.add(item);
//			} 
//		else {
//				Collection<String> selectedAsins = new ArrayList<String>();
//				for (WebElement option : options) {
//					String value = option.getAttribute("data-defaultasin");
//					selectedAsins.add(value);
//				}
//				
//				for (String asin : selectedAsins) {
//					div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
//					detailsDriver.findElement(By.xpath("//li[@data-defaultasin='" + asin + "']//button")).click();
//					int i = 0;
//					do {
//						sleep1Sec();
//						String sku = div.findElement(By.id("ASIN")).getAttribute("value");
//						if (asin.toLowerCase().trim().equalsIgnoreCase(sku.toLowerCase().trim())) {
//							break;
//						}
//						if (i % 25 == 0) {
//							detailsDriver.get(href);
//							div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
//						}
//						if (i % 5 == 0) {
//							detailsDriver.findElement(By.xpath("//li[@data-defaultasin='" + asin + "']//button")).click();
//						}
//						i++;
//					} while (i < 60);
//					if (i == 60) {
//						LOG.logError("Problem with page " + href + " change to asin " + asin);
//					}
//					
//					String availability = detailsDriver.findElement(By.xpath(".//div[contains(@id,'availability')]/span")).getText().replace(".", "");
//					if (availability.contains("Currently unavailable")) {
//						Item item = createItem(href, detailsDriver, "0.0", availability);
//						result.add(item);
//					}
//					WebElement newLink = detailsDriver.findElement(By.xpath("//div[contains(@id,'olp_feature_div')]//a"));
//					String newHref = newLink.getAttribute("href");
//					detailsDriver.get(newHref);
//					sleep3Sec();
//					List<WebElement> aquaticaSeller = detailsDriver.findElements(By.xpath("//div[contains(@class,'olpOffer')]/div[contains(@class,'olpSellerColumn')]/h3[contains(.,'Aquatica')]//..//..//div[1]/span[contains(@class,'olpOfferPrice')]"));
//					String price = "";
//					if (aquaticaSeller.isEmpty()) {
//						availability = "Currently unavailable";
//					} else {
//						price = aquaticaSeller.get(0).getText();
//						availability = "";
//					}
//					detailsDriver.get(href);
//					sleep3Sec();
//					detailsDriver.findElement(By.xpath("//li[@data-defaultasin='" + asin + "']//button")).click();
//					Thread.sleep(500);
//					div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
//					i = 0;
//					do {
//						sleep1Sec();
//						String sku = div.findElement(By.id("ASIN")).getAttribute("value");
//						if (asin.toLowerCase().trim().equalsIgnoreCase(sku.toLowerCase().trim())) {
//							break;
//						}
//						if (i % 25 == 0) {
//							// detailsDriver.get(href);
//							LOG.log("Container loading");
//							div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
//						}
//						if (i % 45 == 0) {
//							detailsDriver.findElement(By.xpath("//li[@data-defaultasin='" + asin + "']//button")).click();
//						}
//						i++;
//					} while (i < 105);
//					if (i == 105) {
//						LOG.logError("Problem with page " + href + " change to asin " + asin);
//					}
//					Item item = createItem(href, detailsDriver, price, availability);
//					result.add(item);
//				}
//			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, WebDriver detailsDriver, String sellerPrice, String availability) throws Exception {
		sleep3Sec();
		WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
		String realTitle = null;
		if (availability.isEmpty()) {
			realTitle = div.findElement(By.id("productTitle")).getText().trim();
		} else {
			realTitle = availability + " -> " + div.findElement(By.id("productTitle")).getText().trim();
		}
		
		String testPrice = "";
		if (!sellerPrice.isEmpty()) {
			testPrice = sellerPrice;
		} else {
			List<WebElement> priceblock = div.findElements(By.id("priceblock_ourprice"));
			if (!priceblock.isEmpty()) {
				testPrice = priceblock.get(0).getText();
			} else {
				priceblock = div.findElements(By.xpath("//div[@id='olp_feature_div']"));
				if (!priceblock.isEmpty()) {
					String testPriceBlock = priceblock.get(0).getText();
					Matcher matcher = pricePattern.matcher(testPriceBlock);
					if (matcher.find()) {
						testPrice = matcher.group();
					} else {
						LOG.logWarning("Cant find price");
					}
				}
			}
		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, 0.0);
		item.SKU = div.findElement(By.id("ASIN")).getAttribute("value");
		if (!div.findElements(By.id("quantity")).isEmpty()) {
			item.qty = div.findElement(By.id("quantity")).findElements(By.tagName("option")).size();
		} else {
			List<WebElement> count = div.findElements(By.xpath("//span[contains(@class,'a-color-success')][contains(.,'left in stock')]"));
			if (!count.isEmpty()) {
				Matcher matcher = qtyPattern.matcher(count.get(0).getText());
				if (matcher.find()) {
					item.qty = Utils.tryParseInt(matcher.group(1), 0);
				} else {
					LOG.logWarning("Cant parse QTY info for " + count.get(0).getText());
				}
			}
		}
		String source = detailsDriver.getPageSource();
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//span[contains(@class,'thumbnail')]//img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		
		if (item.title.equals("Bath Pillows")) {
			item.UPC = "627722003630"; // not enough information about Bath
										// Pillows
		} else {
			item.UPC = getUPC(item, detailsDriver);
		}
		
		String size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Size')]/../td");
		if (size.isEmpty() || !size.contains(" x ")) {
			size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Dimensions')]/../td");
			if (!size.isEmpty()) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			} else {
				Matcher matcher = WFSizeInfoPattern.matcher(source);
				if (matcher.find()) {
					String content = matcher.group(1);
					content = URLDecoder.decode(content, "UTF-8");
					matcher = WFSizePattern.matcher(content);
					if (matcher.find()) {
						size = matcher.group(1) + " L x " + matcher.group(2) + " W x " + matcher.group(3) + " H";
					}
				}
			}
		} else {
			if (!size.toLowerCase().contains("l ") && !size.toLowerCase().contains("w ") && !size.toLowerCase().contains("h ")) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			}
		}
		size = " x " + size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		
		return item;
		
	}
	
	private String getUPC(Item item, WebDriver detailsDriver) {
		StringBuilder info = new StringBuilder();
		List<WebElement> tds = detailsDriver.findElements(By.xpath("//table[@id='productDetails_techSpec_section_1']//td"));
		for (WebElement td : tds) {
			info.append(td.getText()).append(" ");
		}
		return item.resolveUPC(info.toString(), null);
	}
	
	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.amazon.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-1", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA1)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-2", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA2)));
	}
	
	public boolean checkAuthorizationCode(WebDriver detailsDriver) throws InterruptedException {
		int attemps = 300;
		do {
			if (detailsDriver.findElements(By.xpath("//form[contains(@id,'auth-mfa-form')]")).isEmpty()) {
				if (attemps < 300) {
					LOG.logSuccess("CODE COMPLETE");
				}
				return true;
			}
			java.awt.Toolkit.getDefaultToolkit().beep();
			LOG.logWarning("ENTER CODE");
			for (int i = 0; i < 10; i++) {
				LOG.logInfo("Sleep " + attemps + " sec");
				attemps--;
				Thread.sleep(1000);
			}
		} while (attemps > 0);
		return detailsDriver.findElements(By.xpath("//table[contains(@id,'ap_captcha_table')]")).isEmpty();
	}
	
	@Override
	public String toString() {
		return "Amazon.com(Admin)";
	}
	
}
