package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;

import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class UltraBathRoomVanitiesSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.ultrabathroomvanities.com/collections/aquatica-summer-sale";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public UltraBathRoomVanitiesSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
			List<WebElement> divs = mainDriver.findElementsByXPath("//a[contains(@class,'grid-link')]");
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//p[contains(@class,'grid-link__title')]"));
				String href = div.getAttribute("href");
				String title = a.getText();
				result.foundItems.add(title);
				LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
				// if (result.foundItems.size() < 18) {
				// continue;
				// }
				for (Item item : getDetails(href, detailsDriver)) {
					result.addItem(item);
					if (listener != null) {
						listener.itemFinish(item);
					}
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
			urlNextPage = null;
			List<WebElement> nextElement = mainDriver.findElementsByXPath("//a[contains(@title, 'Next')]");
			if (!nextElement.isEmpty()) {
				if (nextElement.size() != 1) {
					System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
				}
				
				urlNextPage = nextElement.get(0).getAttribute("href");
				mainDriver.get(urlNextPage);
				Thread.sleep(1000);
			}
		} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			String sku = detailsDriver.findElement(By.xpath("//form[contains(@id,'AddToCartForm')]")).getAttribute("data-product-id");
			Item item = createItem(href, sku, detailsDriver);
			result.add(item);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, String sku, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[contains(@class,'grid product-single__hero')]"));
		String realTitle = div.findElement(By.xpath("//h1[contains(@itemprop,'name')]")).getText().trim();
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.xpath("//span[contains(@itemprop,'price')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = sku;
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//img[contains(@id,'ProductPhoto')]"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		}
		
		item.UPC = getUPC(item, detailsDriver);
		String l = "0", h = "0", w = "0";
		
		l = getTextIfExist(detailsDriver, "//table[contains(@class,'attribute')]/../tr/td[1][contains(.,'Exterior Product Length')]/../td[2]").replace(",", ".");
		w = getTextIfExist(detailsDriver, "//table[contains(@class,'attribute')]/tbody/tr/td[1][contains(.,'Exterior Product Width')]/../td[2]").replace(",", ".");
		h = getTextIfExist(detailsDriver, "//table[contains(@class,'attribute')]/tbody/tr/td[1][contains(.,'Exterior Product Height')]/../td[2]").replace(",", ".");
		
		item.length = tryParseDouble(l, 0.0);
		item.width = tryParseDouble(w, 0.0);
		item.height = tryParseDouble(h, 0.0);
		
		return item;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		String upcs = detailsDriver.findElement(By.xpath("//select[contains(@class,'product-single__variants')]/option")).getAttribute("data-sku");
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "UltraBathRoom";
	}
}
