package com.Aquatica.testers;

import static com.Aquatica.Utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AjaxDriver;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;

public class PremiumBathsTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.premiumbaths.co.uk/search?type=product&q=aquatica";
	
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern CurrencyGBPPattern = Pattern.compile("\"USDGBP\"\\s*:\\s*([\\d\\.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern CurrencyEURPattern = Pattern.compile("\"USDEUR\"\\s*:\\s*([\\d\\.]+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private static final String currencyUrlGBP = "http://apilayer.net/api/live?access_key=9cc9b91d7041874faa2fb84526cd4238&currencies=GBP";
	private static final String currencyUrlEUR = "http://apilayer.net/api/live?access_key=9cc9b91d7041874faa2fb84526cd4238&currencies=EUR";
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	private Double currentCourseGBP = 1 / 0.804;
	private Double currentCourseEUR = 1 / 0.931;
	private Double currentCourse = 1 / 0.864;
	
	public PremiumBathsTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		setCurrency();
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElementsByXPath("//div[contains(@class,'grid-uniform')]//div[contains(@class,'grid-item')]");
				
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.tagName("a"));
					String href = a.getAttribute("href");
					String title = a.findElement(By.tagName("p")).getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
					// if (result.foundItems.size() < 76) {
					// continue;
					// }
					for (Item item : getDetails(href, detailsDriver)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsByXPath(".//a[contains(@title,'Next')]");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
					Thread.sleep(2000);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	public void setCurrency() {
		AjaxDriver ajaxDriver = DRIVER.getAjaxDriver();
		String courseGBPSource = ajaxDriver.getAjax(currencyUrlGBP);
		String courseEURSource = ajaxDriver.getAjax(currencyUrlEUR);
		Matcher matcherGBP = CurrencyGBPPattern.matcher(courseGBPSource);
		Matcher matcherEUR = CurrencyEURPattern.matcher(courseEURSource);
		if (matcherGBP.find()) {
			currentCourseGBP = tryParseDouble(matcherGBP.group(1), currentCourseGBP);
		}
		currentCourseGBP = 1.0 / currentCourseGBP;
		if (matcherEUR.find()) {
			currentCourseEUR = tryParseDouble(matcherEUR.group(1), currentCourseEUR);
		}
		currentCourseEUR = 1.0 / currentCourseEUR;
		currentCourse = currentCourseGBP / currentCourseEUR;
		
		DRIVER.freeAjaxDriver(ajaxDriver);
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> options = detailsDriver.findElementsByXPath("//div[contains(@class, 'selector-wrapper')]//select[contains(@id, 'productSelect')]//option");
			if (options.isEmpty()) {
				Item item = createItem(href, detailsDriver);
				result.add(item);
				return result;
			}
			
			Collection<String> values = new ArrayList<String>();
			for (WebElement option : options) {
				String value = option.getAttribute("value");
				if (!value.isEmpty()) {
					values.add(value);
				}
			}
			
			for (String value : values) {
				detailsDriver.findElementByXPath("//option[@value='" + value + "']").click();
				Thread.sleep(2000);
				Item item = createItem(href, detailsDriver);
				result.add(item);
			}
			
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//main[contains(@class,'main-content')]"));
		String realTitle = div.findElement(By.xpath("//h2[contains(@itemprop,'name')]")).getText().trim();
		String testPrice = null;
		double finishPrice = 0.0;
		List<WebElement> priceblock = div.findElements(By.xpath("//span[contains(@id,'productPrice')]"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
			finishPrice = tryParseDouble(testPrice, null) / 100;
		} else {
			LOG.logWarning("Not found price block");
		}
		
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = round(finishPrice * currentCourse);
		item.SKU = div.findElement(By.xpath("//p[contains(@class,'product-meta')]//span[contains(@class, 'variant-sku')]")).getText().trim();
		item.qty = 0;
		
		List<WebElement> imgs = detailsDriver.findElements(By.xpath("//div[contains(@class,'flexslider')]//ul[contains(@id, 'productThumbs')]//li//..//img"));
		if (!imgs.isEmpty()) {
			for (WebElement img : imgs) {
				item.images.add(img.getAttribute("src"));
			}
		} else {
			imgs = detailsDriver.findElements(By.xpath("//div[contains(@id,'productPhoto')]/a"));
			if (!imgs.isEmpty()) {
				item.images.add(imgs.get(0).getAttribute("src"));
			}
		}
		
		item.UPC = getUPC(item, realTitle, item.SKU);
		
		String size = getTextIfExist(detailsDriver, "//tr//td[1][contains(.,'Exterior dimensions')]/../td[2]");
		if (!size.isEmpty()) {
			size = size.toLowerCase().replace("\"", "").replace(":", "").trim();
			size = "x " + size;
			item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
			item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
			item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		}
		
		return item;
	}
	
	private String getUPC(Item item, String title, String sku) {
		String upcs = "";
		if (!sku.isEmpty()) {
			upcs = sku;
		} else {
			upcs = title;
		}
		return item.resolveUPC(upcs, null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	@Override
	public String toString() {
		return "PremiumBaths";
	}
}
