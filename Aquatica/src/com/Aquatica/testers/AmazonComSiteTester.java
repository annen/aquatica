package com.Aquatica.testers;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.Settings;
import com.Aquatica.window.ValueProvider;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class AmazonComSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "http://www.amazon.com/s/?rh=n:228013,p_89:Aquatica";
	
	private static final Pattern qtyPattern = Pattern.compile("Only\\s+(\\d+)\\s+left\\s+in\\s+stock", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*[H|D]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private static final Pattern WFSizeInfoPattern = Pattern.compile("initialize[^\\{]+\\{\\s+var\\s+iframeContent\\s+=\\s+\"([^\"]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern WFSizePattern = Pattern.compile("Length.+Side: -([\\d\\.]+)\".+Width.*Back: -([\\d\\.]+)\".*Depth.*Bottom: -([\\d\\.]+)\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;
	
	public AmazonComSiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		setCookie(mainDriver);
		setCookie(detailsDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			mainDriver.get(mainPage);
			String urlNextPage;
			do {
				List<WebElement> divs = mainDriver.findElements(By.xpath("//li[contains(@class,'s-result-item')][not(contains(@class,'aok-hidden'))]"));
				if (divs.isEmpty()) {
					LOG.logError("Sorry I am Blocked :( UA:" + mainDriver.getWebClient().getBrowserVersion().getUserAgent());
					if (listener != null) {
						listener.allFinish(result);
					}
					return result;
				}
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//a[contains(@class,'s-access-detail-page')]"));
					String href = a.getAttribute("href");
					String title = a.getText();
					String price = "";
					List<WebElement> priceBlock = div.findElements(By.xpath(".//span[contains(@class,'a-size-base a-color-base')]"));
					if(!priceBlock.isEmpty()){
						price = priceBlock.get(0).getText();
					} else {
						priceBlock = div.findElements(By.xpath(".//span[contains(@class,'sx-price-whole')]"));
						if(!priceBlock.isEmpty()){
							price = priceBlock.get(0).getText();
						} else {
							LOG.logWarning("Not found price");
						}
					}
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//					 if (result.foundItems.size() < 108) {
//					 continue;
//					 }
					for (Item item : getDetails(href, detailsDriver, price)) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}
				urlNextPage = null;
				List<WebElement> nextElement = mainDriver.findElementsById("pagnNextLink");
				if (!nextElement.isEmpty()) {
					if (nextElement.size() != 1) {
						System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
					}
					urlNextPage = nextElement.get(0).getAttribute("href");
					mainDriver.get(urlNextPage);
				}
			} while (urlNextPage != null);
		}
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}
	
	private Collection<Item> getDetails(String href, AquaticaDriver detailsDriver, String price) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
			List<WebElement> options = div.findElements(By.xpath(".//ul[contains(@class,'swatches')]/li"));
			if (options.isEmpty() || options.get(0).getText().trim().contains("Without expert installation") ||options.get(0).getText().trim().contains("Without expert setup")) {
				Item item = createItem(href, detailsDriver, price);
				result.add(item);
			} else {
				Collection<String> selectedAsins = new ArrayList<String>();
				for (WebElement option : options) {
					String value = option.getAttribute("data-defaultasin");
					selectedAsins.add(value);
				}
				
				for (String asin : selectedAsins) {
					detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
					int i = 0;
					do {
						Thread.sleep(1000);
						String sku = div.findElement(By.id("ASIN")).getAttribute("value");
						if (asin.toLowerCase().trim().equalsIgnoreCase(sku.toLowerCase().trim())) {
							break;
						}
						if (i % 25 == 0) {
							detailsDriver.get(href);
							div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
						}
						if (i % 5 == 0) {
							detailsDriver.findElementByXPath("//li[@data-defaultasin='" + asin + "']//button").click();
						}
						i++;
					} while (i < 60);
					if (i == 60) {
						LOG.logError("Problem with page " + href + " change to asin " + asin);
					}
					Item item = createItem(href, detailsDriver, price);
					result.add(item);
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}
	
	public Item createItem(String href, AquaticaDriver detailsDriver, String price) throws Exception {
		Thread.sleep(2000);
		WebElement div = detailsDriver.findElement(By.xpath("//div[@class='a-container']"));
		String realTitle = div.findElement(By.id("productTitle")).getText().trim();
		WebElement merchant = div.findElement(By.id("merchant-info"));
		if (merchant.isDisplayed() && !merchant.getText().trim().isEmpty()) {
			realTitle = realTitle + " |" + merchant.getText().trim().replace("Ships from and sold by", "");
		} else {
			List<WebElement> merch = div.findElements(By.xpath("//a[contains(@class,'padsMerchantName')]"));
			if (!merch.isEmpty() && merch.get(0).isDisplayed()) {
				realTitle = realTitle + " |" + merch.get(0).getText().trim();
			} else {
				realTitle = realTitle + " |" + "Unknown";
			}
		}
//		String testPrice = "";
//		List<WebElement> priceblock = div.findElements(By.id("priceblock_ourprice"));
//		if (!priceblock.isEmpty()) {
//			testPrice = priceblock.get(0).getText();
//		} else {
//			priceblock = div.findElements(By.xpath("//span[@class='a-color-price']"));
//			if (!priceblock.isEmpty()) {
//				testPrice = priceblock.get(0).getText();
//			}
//		}
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(price, 0.0);
		item.SKU = div.findElement(By.id("ASIN")).getAttribute("value");
		if (!div.findElements(By.id("quantity")).isEmpty()) {
			item.qty = div.findElement(By.id("quantity")).findElements(By.tagName("option")).size();
		} else {
			List<WebElement> count = div.findElements(By.xpath("//span[contains(@class,'a-color-success')][contains(.,'left in stock')]"));
			if (!count.isEmpty()) {
				Matcher matcher = qtyPattern.matcher(count.get(0).getText());
				if (matcher.find()) {
					item.qty = Utils.tryParseInt(matcher.group(1), 0);
				} else {
					LOG.logWarning("Cant parse QTY info for " + count.get(0).getText());
				}
			}
		}
		String source = detailsDriver.getPageSource();
		List<WebElement> imgs = detailsDriver.findElementsByXPath("//span[contains(@class,'thumbnail')]//img");
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);

		String size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Size')]/../td");
		if (size.isEmpty() || !size.contains(" x ")) {
			size = getTextIfExist(detailsDriver, "//table[@id='productDetails_techSpec_section_1']//th[contains(.,'Dimensions')]/../td");
			if (!size.isEmpty()) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			} else {
				Matcher matcher = WFSizeInfoPattern.matcher(source);
				if (matcher.find()) {
					String content = matcher.group(1);
					content = URLDecoder.decode(content, "UTF-8");
					matcher = WFSizePattern.matcher(content);
					if (matcher.find()) {
						size = matcher.group(1) + " L x " + matcher.group(2) + " W x " + matcher.group(3) + " H";
					}
				}
			}
		} else {
			if (!size.toLowerCase().contains("l ") && !size.toLowerCase().contains("w ") && !size.toLowerCase().contains("h ")) {
				size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
				String[] parts = size.split(" x ");
				if (parts.length == 3) {
					size = parts[0] + " L x " + parts[1] + " W x " + parts[2] + " H";
				}
			}
		}
		size = " x " + size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;

	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		StringBuilder info = new StringBuilder();
		List<WebElement> tds = detailsDriver.findElements(By.xpath("//table[@id='productDetails_techSpec_section_1']//td"));
		for (WebElement td : tds) {
			info.append(td.getText()).append(" ");
		}
		return item.resolveUPC(info.toString(), null);
	}
	
	public String getTextIfExist(AquaticaDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}
	
	public void setCookie(AquaticaDriver driver) {
		String domain = "www.amazon.com";
		driver.getWebClient().getCookieManager().clearCookies();
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-1", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA1)));
		driver.getWebClient().getCookieManager().addCookie(new Cookie(domain, "x-amz-captcha-2", Settings.get(Settings.BOTS_AMAZONCOM_COOKIE_CAPTHCA2)));
	}
	
	@Override
	public String toString() {
		return "Amazon.com";
	}
	
}
