package com.Aquatica.testers;

import static com.Aquatica.Utils.changeToTab1;
import static com.Aquatica.Utils.changeToTab2;
import static com.Aquatica.Utils.createNewTab;
import static com.Aquatica.Utils.tryParseDouble;
import static com.Aquatica.Utils.sleep3Sec;
import static com.Aquatica.Utils.sleep1Sec;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;
import com.Aquatica.window.ValueProvider;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

public class OpenSkySiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	public static final String MAIN_PAGE = "https://www.opensky.com/aquatica";

	private static final Pattern heightPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*H.", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern wigthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*W.", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern lengthPattern = Pattern.compile("x\\s*([\\d\\.]+)\\s*L.", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	private List<String> mainPages;
	private ValueProvider<Boolean> loadIfExist;
	private ProcessListener listener;

	public OpenSkySiteTester(List<String> mainPages, ValueProvider<Boolean> loadIfExist) {
		this.mainPages = mainPages;
		this.loadIfExist = loadIfExist;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		this.listener = listener;
	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (loadIfExist.getValue() && f.exists()) {
			result = result.loadFrom(f);
			if (listener != null) {
				listener.allFinish(result);
			}
			return result;
		}
		
		WebDriver newMainDriver = DRIVER.getPlatformDriver();
		createNewTab(newMainDriver);
		changeToTab1(newMainDriver);
		for (String mainPage : mainPages) {
			LOG.log("Load main page " + mainPage);
			newMainDriver.get(mainPage);
			List<WebElement> divs = newMainDriver.findElements(By.xpath("//li[contains(@id,'product-grid-')]"));
			sleep3Sec();
			int i = 0;
			do{
				if(i == 5){
					divs = newMainDriver.findElements(By.xpath("//li[contains(@id,'product-grid-')]"));
					LOG.logInfo("Searching products");
				}
				sleep1Sec();
				i++;
				if (i == 40) {
					LOG.logError("Sorry I am Blocked :( ");
					if (listener != null) {
						listener.allFinish(result);
					}
				}
			} while(divs.isEmpty());
			do {
				if (divs.isEmpty()) {
					break;
				}
				for (WebElement div : divs) {
					WebElement a = div.findElement(By.xpath(".//div[contains(@class,'product-grid-info-name')]/a"));
					String href = a.getAttribute("href");
					String title = a.getText();
					result.foundItems.add(title);
					LOG.log("Parse item  [" + result.foundItems.size() + "] -> " + title);
//					 if (result.foundItems.size() < 130) {
//					 continue;
//					 }
					
					changeToTab2(newMainDriver);
					Collection<Item> itemsAq = getDetails(href, newMainDriver);
					changeToTab1(newMainDriver);
					for (Item item : itemsAq) {
						result.addItem(item);
						if (listener != null) {
							listener.itemFinish(item);
						}
						if (result.foundItems.size() == MAX_ITEMS)
							return result;
					}
				}

				List<WebElement> aNext = newMainDriver.findElements(By.xpath("//a[contains(@class,'show-more')]"));
				if (aNext.isEmpty()) {
					break;
				}
				String nextHref = aNext.get(0).getAttribute("href");
				newMainDriver.get(nextHref);
				i++;
			} while (i < 20);
		}
		DRIVER.freePlatformDriver(newMainDriver);
		if (loadIfExist.getValue()) {
			result.saveTo(f);
		}
		if (listener != null) {
			listener.allFinish(result);
		}
		return result;
	}

	private Collection<Item> getDetails(String href, WebDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		try {
			detailsDriver.get(href);
			List<WebElement> selects = detailsDriver.findElements(By.xpath("//div[contains(@class,'select-container')]"));
			boolean mustBeMorePrice = !selects.isEmpty() || !((RemoteWebDriver) detailsDriver).executeScript("var i=0; for(var x in Osky.App.pageOptions.sellable.detailsMap){ i=i+1; } return i;").toString().equals("1");
			if (!mustBeMorePrice) {
				Item item = createItem(href, "", detailsDriver);
				result.add(item);
				return result;
			}
			List<WebElement> options = detailsDriver.findElements(By.xpath("//div[contains(@class,'select-container')]//fieldSet/*"));
			if (!options.isEmpty()) {
				Collection<String> selectedColor = new ArrayList<String>();
				for (WebElement option : options) {
					String value = option.getAttribute("class");
					value = value.replace("selected", "").trim();
					selectedColor.add(value);
				}

				for (String color : selectedColor) {
					WebElement element = detailsDriver.findElement(By.xpath("//fieldSet/*[@class='" + color + "']/*"));

					WebElement finishblock = element.findElement(By.xpath(".//input"));
					String finish = finishblock.getAttribute("data-color-name");
					String id = finishblock.getAttribute("value");
					Item item = createItem(href, finish, detailsDriver);
					String price = ((RemoteWebDriver) detailsDriver).executeScript("var A = Osky.App.pageOptions.sellable.detailsMap; for (var n in A) if (A.hasOwnProperty(n)) { var r = A[n]; if (r.attributes['color']==arguments[0]) return r.price; } return null;", id).toString();
					item.price = tryParseDouble(price, null);
					result.add(item);
				}
			} else {
				String skus = ((RemoteWebDriver) detailsDriver).executeScript("var s=''; for(var x in Osky.App.pageOptions.sellable.detailsMap){ s=s+' '+x; } return s;").toString();
				List<String> skuList = Splitter.on(' ').omitEmptyStrings().trimResults().splitToList(skus);
				for (String sku : skuList) {
					Item item = createItemFromJS(href, sku, detailsDriver);
					result.add(item);
				}
			}
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with scan item " + href, true, detailsDriver.getPageSource());
		}
		return result;
	}

	public Item createItem(String href, String finish, WebDriver detailsDriver) throws Exception {
		WebElement div = detailsDriver.findElement(By.xpath("//div[@class='main-module-body']"));
		String realTitle = div.findElement(By.xpath("//h1[contains(@class,'sellable-name')]")).getText().trim();
		realTitle = realTitle.replace("&trade;", " ");
		if (!Strings.isNullOrEmpty(finish)) {
			realTitle = realTitle + " -> " + finish;
		}
		String testPrice = null;
		List<WebElement> priceblock = div.findElements(By.id("current-price"));
		if (!priceblock.isEmpty()) {
			testPrice = priceblock.get(0).getText();
		} else {
			LOG.logWarning("Not found price block");
		}

		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.id("buy-button")).getAttribute("data-sellable-slug"); // data-sellable-id
		// ???
		List<WebElement> sizeSelect = div.findElements(By.id("select_product_quantity"));
		if (!sizeSelect.isEmpty()) {
			item.qty = sizeSelect.get(0).findElements(By.tagName("option")).size();
		} else {
			if (getTextIfExist(detailsDriver, "//span[contains(.,'Sold Out')]").isEmpty()) {
				LOG.logWarning("QTY control not found");
			}
		}

		List<WebElement> imgs = div.findElements(By.xpath("//nav[contains(@id,'sellable-images-thumbs')]//img"));
		if(!imgs.isEmpty()){
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		} else {
			imgs = div.findElements(By.xpath("//ul[contains(@class,'orbit-container')]//img"));
			item.images.add(imgs.get(0).getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);
		List<WebElement> showMoreButton = div.findElements(By.xpath("//a[contains(@class,'show-more-less')]"));
		if (!showMoreButton.isEmpty()){
			((RemoteWebDriver) detailsDriver).executeScript("arguments[0].click()", showMoreButton.get(0));	
		}
		String size = getTextIfExist(detailsDriver, "//li[contains(.,'Dimension:')]");
		if(size.isEmpty()){
			size = getTextIfExist(detailsDriver, "//div[contains(@class,'product-description')]//p[contains(.,'External Dimensions:')]");
		}
		size = size.toLowerCase().trim().replace("inches", "").replace("inch", "").replace("in.", "").replace("in", "").replace("-", "").replace("\"", "").replace(":", " x ");
		item.length = Utils.tryParseDouble(size, lengthPattern, 0.0);
		item.width = Utils.tryParseDouble(size, wigthPattern, 0.0);
		item.height = Utils.tryParseDouble(size, heightPattern, 0.0);
		return item;
	}

	private Item createItemFromJS(String href, String sku, WebDriver detailsDriver) {
		WebElement div = detailsDriver.findElement(By.xpath("//div[@class='main-module-body']"));
		String realTitle = ((RemoteWebDriver) detailsDriver).executeScript("return Osky.App.pageOptions.sellable.detailsMap['" + sku + "'].name").toString();
		String testPrice = ((RemoteWebDriver) detailsDriver).executeScript("return Osky.App.pageOptions.sellable.detailsMap['" + sku + "'].price").toString();
		Item item = new Item();
		item.href = href;
		item.title = realTitle;
		item.price = tryParseDouble(testPrice, null);
		item.SKU = div.findElement(By.id("buy-button")).getAttribute("data-sellable-slug"); // data-sellable-id
		// ???
		item.qty = Utils.tryParseInt(((RemoteWebDriver) detailsDriver).executeScript("return Osky.App.pageOptions.sellable.detailsMap['" + sku + "'].inventory").toString(), 0);

		List<WebElement> imgs = div.findElements(By.xpath("//div[contains(@class,'thumbnail')]/img"));
		for (WebElement img : imgs) {
			item.images.add(img.getAttribute("src"));
		}
		item.UPC = getUPC(item, detailsDriver);
		return item;
	}

	private String getUPC(Item item, WebDriver detailsDriver) {
		String upc = getTextIfExist(detailsDriver, "//li[contains(@class,'upc')]").replace("UPC", "");
		return item.resolveUPC(upc, null);
	}

	public String getTextIfExist(WebDriver detailsDriver, String xpath) {
		List<WebElement> parts = detailsDriver.findElements(By.xpath(xpath));
		if (parts.isEmpty())
			return "";
		return parts.get(0).getText().trim().toLowerCase();
	}

	@Override
	public String toString() {
		return "OpenSky";
	}
}

/*
 * HtmlUnitWebElement input = (HtmlUnitWebElement) detailsDriver.findElementByXPath("//fieldSet/*[@class='" + color + "']//input"); detailsDriver.executeScript("jQuery(arguments[0]).trigger('click')", input); input.click(); Thread.sleep(1000);
 * System.out.println(detailsDriver.findElement(By.id("current-price")).getText()); detailsDriver.executeScript(" var evt = document.createEvent('HTMLEvents'); evt.initEvent('change', true, true);  arguments[0].dispatchEvent(evt);", input); detailsDriver.executeScript(
 * " var evt = document.createEvent('HTMLEvents'); evt.initEvent('click', true, true);  arguments[0].dispatchEvent(evt);", input); // detailsDriver.executeScript("arguments[0].onchange();", input); detailsDriver.executeScript("arguments[0].click();", input); input.click(); detailsDriver.executeScript(
 * " var evt = document.createEvent('HTMLEvents'); evt.initEvent('change', true, true);  arguments[0].dispatchEvent(evt);", input); detailsDriver.executeScript(" var evt = document.createEvent('HTMLEvents'); evt.initEvent('click', true, true);  arguments[0].dispatchEvent(evt);", input); //
 * detailsDriver.executeScript("arguments[0].onchange();", input); detailsDriver.executeScript("arguments[0].click();", input); input.click(); Thread.sleep(1000); input = (HtmlUnitWebElement) detailsDriver.findElementByXPath("//fieldSet/*[@class='" + color + "']/*"); detailsDriver.executeScript(
 * " var evt = document.createEvent('HTMLEvents'); evt.initEvent('change', true, true);  arguments[0].dispatchEvent(evt);", input); detailsDriver.executeScript(" var evt = document.createEvent('HTMLEvents'); evt.initEvent('click', true, true);  arguments[0].dispatchEvent(evt);", input); //
 * detailsDriver.executeScript("arguments[0].onchange();", input); detailsDriver.executeScript("arguments[0].click();", input); input.click(); detailsDriver.executeScript(" var evt = document.createEvent('HTMLEvents'); evt.initEvent('change', true, true);  arguments[0].dispatchEvent(evt);", input);
 * detailsDriver.executeScript(" var evt = document.createEvent('HTMLEvents'); evt.initEvent('click', true, true);  arguments[0].dispatchEvent(evt);", input); // detailsDriver.executeScript("arguments[0].onchange();", input); detailsDriver.executeScript("arguments[0].click();", input); input.click();
 * Thread.sleep(1000); System.out.println(detailsDriver.findElement(By.id("current-price")).getText());
 */