package com.Aquatica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.Aquatica.window.LOG;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;

public class UPCResolver {
	public static class Resolver {
		private static final Map<String, Integer> freq = new HashMap<String, Integer>();
		private static final double KEY_P = 10.0;
		private static final double PRICE_P = 2.0;
		private static final double SIZE_P = 4.0;
		private static final double SKU_P = 6.0;
		private static final double WEB_TITLE_P = 3.0;
		private final String upc;
		private String key;
		private Double price;
		private String sizeW;
		private String sizeH;
		private String sizeL;
		private String[] skus = new String[0];
		private String[] titles = new String[0];
		
		public Resolver(String upc) {
			this.upc = upc;
		}
		
		public Double test(String title, Double price, String info, String productSKU) {
			Double p = 0.0;
			title = " " + title.toLowerCase() + " ";
			info = " " + info.toLowerCase() + " ";
			String prepTitle = prepareString(title);
			String prepInfo = prepareString(info);
			String prepSKU = prepareString(productSKU);
			price = price == null ? 0.0 : price;
			if (!upc.isEmpty() && (title.contains(upc) || info.contains(upc))) {
				p = p + KEY_P * 2;
			}
			if (key != null) {
				if (title.contains(" " + key + " ") || info.contains(" " + key + " ")) {
					p = p + KEY_P;
				} else if (title.contains(key) || info.contains(key)) {
					p = p + KEY_P * 0.7;
				}
			}
			if (this.price != null && this.price > 1 && price != null && price > 0) {
				double delta = Math.abs(price - this.price) / 1000.0;
				if (delta < 1) {
					p = p + PRICE_P * (1 - delta);
				}
			}
			if (skus != null) {
				for (String sku : skus) {
					sku = prepareString(sku);
					if (prepSKU.equalsIgnoreCase(sku)) {
						p = p + SKU_P * 2;
					}
					if (prepTitle.contains(sku) || prepInfo.contains(sku)) {
						p = p + SKU_P;
					}
				}
			}
			if (sizeL != null && !sizeL.isEmpty() && ((title.contains(" " + sizeL + " ") || info.contains(" " + sizeL + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (sizeW != null && !sizeW.isEmpty() && ((title.contains(" " + sizeW + " ") || info.contains(" " + sizeW + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (sizeH != null && !sizeH.isEmpty() && ((title.contains(" " + sizeH + " ") || info.contains(" " + sizeH + " ")))) {
				p = p + SIZE_P / 3.0;
			}
			if (titles != null && titles.length > 0) {
				
				for (String web_title : titles) {
					Integer f = freq.get(web_title);
					f = f == null ? 1 : f;
					if (prepTitle.contains(web_title) || prepInfo.contains(web_title)) {
						p = p + (WEB_TITLE_P / f.doubleValue());
					} else {
						p = p - (WEB_TITLE_P / 20.0);
					}
				}
			}
			return p;
		}
		
		public String getUpc() {
			return upc;
		}
		
		public String getKey() {
			return key;
		}
		
		public String[] getSkus() {
			return skus;
		}
		
		public Double getPrice() {
			return price;
		}
		
		public String getSizeL() {
			return sizeL;
		}
		
		public String getSizeW() {
			return sizeW;
		}
		
		public String getSizeH() {
			return sizeH;
		}
		
		public String[] getTitles() {
			return titles;
		}
		
		public Resolver key(String key) {
			this.key = key.toLowerCase();
			return this;
		}
		
		public Resolver price(Double price) {
			this.price = price;
			return this;
		}
		
		public Resolver size(String sizeL, String sizeW, String sizeH) {
			this.sizeL = sizeL;
			this.sizeW = sizeW;
			this.sizeH = sizeH;
			return this;
		}
		
		public Resolver sku(String... skus) {
			Collection<String> valid = new ArrayList<String>();
			for (String sku : skus) {
				if (sku != null && !sku.trim().isEmpty()) {
					valid.add(sku.trim().toLowerCase());
				}
			}
			this.skus = valid.toArray(new String[valid.size()]);
			return this;
		}
		
		public Resolver titles(String... titles) {
			Collection<String> valid = new ArrayList<String>();
			for (String title : titles) {
				if (title != null && !title.trim().isEmpty()) {
					valid.add(title.trim());
				}
			}
			this.titles = valid.toArray(new String[valid.size()]);
			for (int i = 0; i < this.titles.length; i++) {
				this.titles[i] = prepareString(this.titles[i]);
				Integer f = freq.get(this.titles[i]);
				if (f == null) {
					freq.put(this.titles[i], 1);
				} else {
					freq.put(this.titles[i], f + 1);
				}
			}
			return this;
		}
		
		private String prepareString(String title) {
			if (title == null)
				return "";
			return title.trim().toLowerCase().replace(",", " ").replace(".", " ").replace("$", " ").replace("x", " ").replace("\t", " ").replace("  ", " ").trim();
		}
		
		@Override
		public String toString() {
			return "[" + upc + "] KEY:" + key + " -> " + Arrays.toString(ObjectArrays.concat(skus, titles, String.class));
		}
		
		private static final Splitter splitter = Splitter.on('\t').trimResults();
		private static final Joiner joiner = Joiner.on("---").useForNull("");
		
		public static Resolver parse(String row) {
			if (Strings.isNullOrEmpty(row))
				return DEFAULT;
			List<String> parts = splitter.splitToList(row);
			String upc = parts.get(0);
			String sku = parts.get(1);
			String key = parts.get(2);
			String price = parts.get(3);
			String sizeL = parts.get(4);
			String sizeW = parts.get(5);
			String sizeH = parts.get(6);
			String titles = parts.get(7);
			return buildResolver(upc, sku, key, price, sizeL, sizeW, sizeH, titles);
		}
		
		public static Resolver buildResolver(String upc, String sku, String key, String price, String sizeL, String sizeW, String sizeH, String titles) {
			Resolver resolver = new Resolver(upc);
			if (!Strings.isNullOrEmpty(sku)) {
				resolver.sku(sku);
			}
			if (!Strings.isNullOrEmpty(key)) {
				resolver.key(key);
			}
			if (!Strings.isNullOrEmpty(price)) {
				resolver.price(parseDouble(price));
			}
			resolver.size(Strings.emptyToNull(sizeL), Strings.emptyToNull(sizeW), Strings.emptyToNull(sizeH));
			if (!Strings.isNullOrEmpty(titles)) {
				String[] splitTitles;
				if (titles.contains("---")) {
					splitTitles = titles.split("---");
				} else {
					splitTitles = titles.split("\n");
				}
				resolver.titles(splitTitles);
			}
			return resolver;
		}
		
		public String serialize() {
			if (this == DEFAULT)
				return "";
			StringBuilder sb = new StringBuilder();
			sb.append(upc).append("\t");
			sb.append(joiner.join(skus)).append("\t");
			sb.append(Strings.nullToEmpty(key)).append("\t");
			sb.append(price == null ? "" : Double.toString(price)).append("\t");
			sb.append(Strings.nullToEmpty(sizeL)).append("\t");
			sb.append(Strings.nullToEmpty(sizeW)).append("\t");
			sb.append(Strings.nullToEmpty(sizeH)).append("\t");
			sb.append(joiner.join(titles));
			return sb.toString();
		}
	}
	
	private static Double parseDouble(Object o) {
		if (o == null)
			return 0.0;
		String value = String.valueOf(o).replaceAll("[^0-9.-]", "").trim();
		if (Strings.isNullOrEmpty(value))
			return 0.0;
		try {
			double v = Double.parseDouble(value);
			return v;
		} catch (Exception ex) {
			return 0.0;
		}
	}
	
	public static final Resolver DEFAULT = new Resolver("").price(0.0).key("Default");
	private static final UPCResolver INSTANCE = new UPCResolver();
	
	public static final UPCResolver instance() {
		return INSTANCE;
	}
	
	private final Collection<Resolver> resolvers = new ArrayList<Resolver>();
	
	private UPCResolver() {
		buildDefault();
	}
	
	public Collection<Resolver> getResolvers() {
		return resolvers;
	}
	
	public void resetResolvers(Collection<String> newResolvers) {
		resolvers.clear();
		resolvers.add(DEFAULT);
		for (String row : newResolvers) {
			resolvers.add(Resolver.parse(row));
		}
	}
	
	public void resetResolvers(List<Resolver> newResolvers) {
		resolvers.clear();
		resolvers.add(DEFAULT);
		for (Resolver resolver : newResolvers) {
			resolvers.add(resolver);
		}
	}
	
	public Map<String, String> serialize() {
		Map<String, String> result = new LinkedHashMap<String, String>();
		for (Resolver resolver : resolvers) {
			if (resolver == DEFAULT || resolver == null) {
				continue;
			}
			String serialize = resolver.serialize();
			if (Strings.isNullOrEmpty(serialize)) {
				continue;
			}
			result.put(resolver.upc, serialize);
		}
		return result;
	}
	
	public void buildDefault() {
		resolvers.add(DEFAULT);
		
		// --------Bath and Sink Faucets---------
		
		resolvers.add(new Resolver("627722001735").key("700").titles("B700", "BN", "Brushed", "Nickel").sku("B700-BN").price(1366.20).size("4.75", "33.5", "4.75"));
		resolvers.add(new Resolver("627722001711").key("700").titles("B700", "CP", "Chrome").sku("B700-CP").price(2300.00).size("4.75", "33.5", "4.75"));
		resolvers.add(new Resolver("627722003647").key("Bollicine").titles("3.5", "Sink").sku("Bollicine-S-220").price(850.0).size("", "6.1", "5.7"));
		resolvers.add(new Resolver("627722003654").key("Bollicine").titles("8.5", "Sink").sku("Bollicine-S-228").price(1250.0).size("", "7.5", "11.0"));
		resolvers.add(new Resolver("627722005832").key("Bollicine").titles("2.6", "Sink", "Chrome").sku("Bollicine-B-320").price(850.0).size("2.25", "5.0", "4.5"));
		resolvers.add(new Resolver("627722005825").key("Bollicine").titles("3.5", "Sink", "Chrome").sku("Bollicine-S-205").price(1199.0).size("15.75", "9.25", "5.0"));
		resolvers.add(new Resolver("627722003227").key("Bollicine").titles("2-Hole", "Deck").sku("Bollicine-D-121").price(1399.0).size("", "6.7", "9.5"));
		resolvers.add(new Resolver("627722003487").key("Bollicine").titles("4-Hole", "Deck").sku("Bollicine-D-126").price(1599.0).size("", "6.7", "9.5"));
		resolvers.add(new Resolver("627722003173").key("Bollicine").titles("Floor").sku("F-190", "Bollicine-F-190-CP").price(3588.0).size("", "9.5", "36.25"));
		resolvers.add(new Resolver("627722005924").key("Bollicine").titles("Bollicine-108", "Wall", "Mounted", "Chrome").sku("W-108", "Bollicine-W-108").price(1650.0).size("12.75", "7.75", "3.75"));
		resolvers.add(new Resolver("627722005931").key("Bollicine").titles("Bollicine-110", "Wall", "Mounted", "Chrome").sku("W-110", "Bollicine-W-110").price(1690.0).size("12.75", "7.75", "3.75"));
		resolvers.add(new Resolver("627722005894").key("Bollicine").titles("Bollicine-111", "Wall", "Mounted", "Chrome").sku("W-111", "Bollicine-W-111").price(1750.0).size("17.25", "7.5", ""));
		resolvers.add(new Resolver("627722005849").key("Bollicine").titles("Bollicine-242", "Wall", "Mounted", "Chrome").sku("W-242", "Bollicine-W-242").price(1350.0).size("10.25", "6.75", "2.25"));
		resolvers.add(new Resolver("627722002695").key("Caesar").titles("Chrome").sku("Caesar-Chrome", "Caesar-CP", "Chrome").price(1745.70).size("34.5", "4.75", "11"));
		resolvers.add(new Resolver("627722002701").key("Caesar").titles("Bronze").sku("Caesar-Bronze", "Caesar-BP", "Bronze").price(1952.70).size("34.5", "4.75", "11"));
		
		resolvers.add(new Resolver("627722006679").key("Celine").titles("Celine", "B-320", "Chrome").sku("Celine-B-320").price(750.0).size("5.5", "2.0", "7.75"));
		resolvers.add(new Resolver("627722006686").key("Celine").titles("Celine", "B-326", "Chrome").sku("Celine-B-326").price(799.0).size("", "2.0", "6.75"));
		resolvers.add(new Resolver("627722006693").key("Celine").titles("Celine", "D-126", "Chrome").sku("Celine-D-126").price(1299.0).size("", "7.75", "10.75"));
		resolvers.add(new Resolver("627722006389").key("Celine").titles("Celine", "S-220", "Chrome").sku("Celine-S-220").price(750.0).size("6.0", "2.0", "7.75"));
		resolvers.add(new Resolver("627722006396").key("Celine").titles("Celine", "S-222", "Chrome").sku("Celine-S-222").price(899.0).size("8.75", "2.0", "16.0"));
		resolvers.add(new Resolver("627722006402").key("Celine").titles("Celine", "S-225", "Chrome").sku("Celine-S-225").price(850.0).size("7.25", "2.0", "12.5"));
		resolvers.add(new Resolver("627722006372").key("Celine").titles("Celine", "S-226", "Chrome").sku("Celine-S-226").price(799.0).size("6.5", "2.0", "10.75"));
		resolvers.add(new Resolver("627722006365").key("Celine").titles("Celine", "WS-242").sku("Celine-WS-242").price(1050.0).size("", "8.75", "2.0"));
		resolvers.add(new Resolver("627722008468").key("Celine").titles("Celine", "B-326", "Blck", "Black").sku("Celine-B-326-Blck").price(999.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008475").key("Celine").titles("Celine", "D-126", "Blck", "Black").sku("Celine-D-126-Blck").price(1999.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008420").key("Celine").titles("Celine", "F-108", "Blck", "Black").sku("Celine-F-108-Blck").price(4750.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008444").key("Celine").titles("Celine", "S-222", "Blck", "Black").sku("Celine-S-222-Blck").price(1250.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008451").key("Celine").titles("Celine", "S-226", "Blck", "Black").sku("Celine-S-226-Blck").price(999.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008482").key("Celine").titles("Celine", "WS-242", "Blck", "Black").sku("Celine-WS-242-Blck").price(1300.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722006709").key("Celine").titles("108", "F-108-CP", "Chrome").sku("Celine-F-108-CP").price(3200.0).size("46.5", "12.0", "0.0"));
		resolvers.add(new Resolver("627722007195").key("Celine").titles("157", "W-157", "Chrome").sku("Celine-W-157").price(1199.0).size("14.25", "7.5", "2.5"));
		
		resolvers.add(new Resolver("627722002664").key("Colonna").titles("Chrome").sku("Colonna-CP").price(1607.70).size("37.5", "7.25", "31"));
		resolvers.add(new Resolver("627722002671").key("Colonna").titles("Brushed", "Nickel").sku("Colonna-BN", "Brushed Nickel").price(1814.70).size("37.5", "7.25", "31"));
		resolvers.add(new Resolver("627722002954").key("Colonna").titles("WS", "Wide", "Chrome", "WS-CP").sku("Colonna-WS-CP").price(2016.0).size("", "11.5", "37.2"));
		resolvers.add(new Resolver("627722004323").key("Colonna").titles("WS", "Wide", "Nickel", "WS-BN").sku("Colonna-WS-BN").price(2304.0).size("0.0", "10.75", "37.25"));
		resolvers.add(new Resolver("627722004262").key("Colonna").titles("CP", "Colonna-120-CP", "Colonna-120", "Chrome", "Chrome Finish", "120").sku("Colonna-120-CP").price(2152.0).size("", "10.75", "47.0"));
		resolvers.add(new Resolver("627722004279").key("Colonna").titles("BN", "Colonna-120-BN", "Colonna-120", "Brushed", "Nickel", "120").sku("Colonna-120-BN").price(2346.0).size("0.0", "10.75", "47.0"));
		resolvers.add(new Resolver("627722008437").key("Delta").titles("Delta", "F-190", "Blck", "Black").sku("Delta-F-190-Blck").price(3650.00).size("35.75", "8.75", "0.0"));
		resolvers.add(new Resolver("627722006747").key("Delta").titles("Delta", "F-190", "CP", "Chrome").sku("Delta-F-190-CP").price(2600.00).size("35.75", "8.75", "0.0"));
		resolvers.add(new Resolver("627722002688").key("Italia").titles("Faucet").sku("Italia-D").price(1152.00).size("8.75", "5.5", "4"));
		resolvers.add(new Resolver("627722004330").key("Italia").titles("BN", " Waterfall", "4-Hole", "Brushed Nickel").sku("Italia-Waterfall-BN").price(1439.0).size("18.5", "5.75", "8.75"));
		resolvers.add(new Resolver("627722004286").key("Italia").titles("CP", " Waterfall", "4-Hole", "Chrome").sku("Italia-Waterfall-CP").price(1295.0).size("18.5", "5.75", "8.75"));
		
		resolvers.add(new Resolver("627722008086").key("Lorena").titles("B-307", "307", "Bidet Faucet").sku("Lorena-B-307").price(1350.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008079").key("Lorena").titles("B-310", "310", "Bidet Faucet").sku("Lorena-B-310").price(850.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008130").key("Lorena").titles("D-120", "120", "Bath Filler").sku("Lorena-D-120").price(1500.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008147").key("Lorena").titles("D-121", "121", "Bath Filler").sku("Lorena-D-121").price(2350.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008154").key("Lorena").titles("F-189", "189", "CP", "Bath Filler").sku("Lorena-F-189-CP").price(4150.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008093").key("Lorena").titles("F-280", "280", "CP", "Sink Faucet").sku("Lorena-F-280-CP").price(3150.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008062").key("Lorena").titles("F-290", "290", "CP", "Sink Faucet").sku("Lorena-F-290-CP").price(3150.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008017").key("Lorena").titles("S-205", "205", "Sink Faucet").sku("Lorena-S-205").price(1350.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008024").key("Lorena").titles("S-210", "210", "Sink Faucet").sku("Lorena-S-210").price(850.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008031").key("Lorena").titles("S-217", "217", "Sink Faucet").sku("Lorena-S-217").price(1050.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008116").key("Lorena").titles("W-108", "217", "Lorena-217", "Bath Filler").sku("Lorena-W-108").price(2500.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008123").key("Lorena").titles("W-110", "110", "Lorena-110", "Bath Filler").sku("Lorena-W-110").price(2550.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008109").key("Lorena").titles("W-165", "165", "Lorena-165", "Bath Filler").sku("Lorena-W-165").price(2850.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008048").key("Lorena").titles("WS-242", "242", "Lorena-242", "Sink Faucet").sku("Lorena-WS-242").price(1700.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008055").key("Lorena").titles("WS-243", "243", "Lorena-243", "Sink Faucet").sku("Lorena-WS-243").price(1800.00).size("0.0", "0.0", "0.0"));
		
		resolvers.add(new Resolver("627722004101").key("MD107").titles("MD107", "Modul-107").sku("Modul-MD107").price(1550.0).size("10.0", "6.5", "3.25"));
		resolvers.add(new Resolver("627722004088").key("MD121").titles("MD121", "Modul-121").sku("Modul-MD121").price(1499.0).size("8.75", "", ""));
		resolvers.add(new Resolver("627722004033").key("MD190").titles("MD190", "Modul-190").sku("Modul-MD190").price(2799.33).size("9.75", "33.25", "33.00"));
		resolvers.add(new Resolver("627722004071").key("MD220").titles("MD220", "Modul-220").sku("Modul-MD220").price(1050.0).size("", "3.5", "8.25"));
		resolvers.add(new Resolver("627722004118").key("MD222").titles("MD222", "Modul-222").sku("Modul-MD222").price(1450.0).size("7.0", "3.5", "14.0"));
		resolvers.add(new Resolver("627722004125").key("MD223").titles("MD223", "Modul-223").sku("Modul-MD223").price(1399.00).size("7.0", "3.5", "11.25"));
		resolvers.add(new Resolver("627722004583").key("Multiplex").titles("Electronic", "Multiplex").sku("Multiplex-Trio").price(3995.00).size("", "", ""));
		resolvers.add(new Resolver("627722004576").key("Multiplex").titles("Electronic", "Multiplex", "Duo", "E-Duo").sku("Multiplex-E-Duo", "Multiplex-Duo").price(2950.00).size("", "", ""));
		
		resolvers.add(new Resolver("627722002541").key("Matrix-D").titles("Faucet").price(999.81).size("8", "7.5", "6.5"));
		resolvers.add(new Resolver("627722002558").key("Pure-D").titles("Faucet").price(999.81).size("8.85", "8.25", "6.1"));
		resolvers.add(new Resolver("627722002534").key("Juno-F").titles("Faucet").price(1104.00).size("32", "9", "8.6"));
		resolvers.add(new Resolver("627722002527").key("T4-D").titles("Faucet").price(1104.00).size("12.2", "11.8", "5.2"));
		resolvers.add(new Resolver("627722002497").key("Inno").titles("Istanbul", "Faucet").price(1242.00).size("10", "8.85", "5.2"));
		resolvers.add(new Resolver("627722002503").key("Inspir").titles("Istanbul", "Faucet").price(1242.00).size("26.25", "11.8", "7"));
		resolvers.add(new Resolver("627722002510").key("Memoria").titles("Faucet").price(1242.00).size("13.6", "6.6", "3.7"));
		resolvers.add(new Resolver("634392345298").key("Spring").titles("W-1004", "1004").sku("Spring-W-1004-US").price(235.00).size("", "", ""));
		resolvers.add(new Resolver("716894069848").key("Spring").titles("D-2300", "2300").sku("Spring-D-2300-US").price(830.00).size("", "", ""));
		resolvers.add(new Resolver("634392345229").key("Spring").titles("W-2350", "2350").sku("Spring-W-2350-US").price(795.00).size("", "", ""));
		
		// ---------Bathroom Accessories----------
		
		resolvers.add(new Resolver("627722003081").key("Beatrice").titles("Hdrst", "Headrest", "Blck", "Black").sku("Beatrice-Hdrst").price(199.0).size("12.0", "3.0", ""));
		resolvers.add(new Resolver("627722000813").key("Beatrice").titles("Hdrst-Wht", "Headrest", "Wht", "White").sku("Beatrice-Hdrst-Wht").price(199.0).size("12.0", "3.0", ""));
		resolvers.add(new Resolver("627722003524").key("Brackets").titles("Brackets-", "455", "Bravado").sku("Brackets-455", "Washbasin").price(325.0).size("", "18.0", ""));
		resolvers.add(new Resolver("627722003098").key("Comfort").titles("Hdrst", "Headrest", "Blck", "Black").sku("Comfort-Hdrst").price(179.0).size("11.75", "2.75", "0.75"));
		resolvers.add(new Resolver("627722004057").key("Comfort").titles("Hdrst-Wht", "Headrest", "Wht", "White").sku("Comfort-Hdrst-Wht").price(179.0).size("11.75", "2.75", "0.75"));
		resolvers.add(new Resolver("627722007300").key("DJ").titles("DJ", "DJ-Aquatica", "BAS", "DJ-BAS").sku("DJ-Aquatica-BAS").price(3200.0).size("21.75", "21.75", "21.75"));
		resolvers.add(new Resolver("627722005368").key("Infinity").titles("Hdrst", "Headrest", "R1-Hdrst", "R1-Headrest").sku("Infinity-R1-Hdrst").price(599.0).size("19.75", "4.0", "10.25"));
		
		resolvers.add(new Resolver("627722007287").key("Onde").titles("Onde Iroko", "Iroko-Floor", "Mat").sku("Onde-Iroko-Floor-Mat").price(699.0).size("33.5", "19.75", "1.5"));
		resolvers.add(new Resolver("627722007324").key("Onde").titles("Onde Iroko", "Iroko-Tray", "Tray").sku("Onde-Iroko-Tray").price(549.0).size("34.75", "9.5", "1.5"));
		resolvers.add(new Resolver("627722007881").key("Onde").titles("Onde-S", "S-Iroko-Tray", "Tray").sku("Onde-S-Iroko-Tray").price(549.0).size("34.75", "9.5", "1.5"));
		resolvers.add(new Resolver("627722007898").key("Onde").titles("Onde-S", "S-Iroko-Tray", "Tray").sku("Onde-S-Teak-Tray").price(599.0).size("34.75", "9.5", "1.5"));
		resolvers.add(new Resolver("627722007478").key("Onde").titles("Onde Teak", "Teak-Floor", "Mat").sku("Onde-Teak-Floor-Mat").price(799.0).size("33.5", "19.75", "1.5"));
		resolvers.add(new Resolver("627722007485").key("Onde").titles("Onde Teak", "Teak-Tray", "Tray").sku("Onde-Teak-Tray").price(599.0).size("34.75", "9.5", "1.5"));
		
		resolvers.add(new Resolver("627722007430").key("Tidal").titles("Iroko-Tray", "Tray", "Iroko").sku("Tidal-Iroko-Tray").price(649.0).size("36.25", "8.25", "1.25"));
		resolvers.add(new Resolver("627722007447").key("Tidal").titles("Teak-Tray", "Tray", "Teak").sku("Tidal-Teak-Tray").price(699.0).size("36.25", "8.25", "1.25"));
		resolvers.add(new Resolver("627722000042").key("TrueOfuro").titles("Teak-Step", "Teak").sku("True Ofuro-Teak-Step", "True-Ofuro-Teak-Step", "TrueOfuro-Teak-Step").price(599.0).size("21.75", "11.5", "7.5"));
		
		resolvers.add(new Resolver("627722008802").key("Universal").titles("Iroko-Tray", "Wood", "Iroko").sku("Universal-Iroko-Tray").price(649.0).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722008796").key("Universal").titles("Teak-Tray", "Teak", "Teak").sku("Universal-Teak-Tray").price(699.0).size("0.0", "0.0", "0.0"));
		
		resolvers.add(new Resolver("627722003630").key("Vanilla").titles("Hdrst", "Headrest", "Black", "Blck").sku("Vanilla-Hdrst").price(149.0).size("12.0", "3.0", ""));
		resolvers.add(new Resolver("627722004040").key("Vanilla").titles("Hdrst-Wht", "Headrest", "White", "Wht").sku("Vanilla-Hdrst-Wht").price(149.0).size("12.25", "4.75", "1.25"));
		
		// ---------Bathroom Furniture----------
		
		resolvers.add(new Resolver("627722007263").key("Alabama").titles("Alabama-Iro", "Armchair", "Outdoor").sku("Alabama-Armchair-Iro").price(3000.0).size("41.25", "39.25", "25.25"));
		resolvers.add(new Resolver("627722000998").key("Alabama").titles("Alabama-G", "Pillow", "Outdoor").sku("Alabama-G-Pillow").price(210.0).size("23.5", "15.75", "0.0"));
		resolvers.add(new Resolver("627722001001").key("Alabama").titles("Alabama-P", "Pillow", "Outdoor").sku("Alabama-P-Pillow").price(140.0).size("17.75", "9.75", "0.0"));
		resolvers.add(new Resolver("627722007256").key("Alabama").titles("Alabama-Sofa", "Sofa", "Outdoor").sku("Alabama-Sofa-Iro").price(4350.0).size("153.5", "78.75", "25.25"));
		resolvers.add(new Resolver("627722007270").key("Alabama").titles("Alabama-Table", "Table", "Coffee").sku("Alabama-Table-Iro").price(2000.0).size("39.25", "39.25", "6.75"));
		resolvers.add(new Resolver("627722006198").key("Breez").titles("Breez-1", "Sunbed", "Breez").sku("Breez-1-Sunbed").price(1399.0).size("67.0", "25.25", "26.0"));
		resolvers.add(new Resolver("627722006204").key("Breez").titles("Breez-2", "Sunbed", "Breez").sku("Breez-2-Sunbed").price(1599.0).size("68.5", "26.75", "27.5"));
		resolvers.add(new Resolver("627722006334").key("Casilda").titles("Casilda", "Armchair").sku("Casilda-Armchair").price(3400.0).size("41.25", "39.25", "25.25"));
		resolvers.add(new Resolver("627722006327").key("Casilda").titles("Casilda", "Pouf").sku("Casilda-Pouf").price(1850.0).size("39.25", "37.5", "18.0"));
		resolvers.add(new Resolver("627722006303").key("Casilda").titles("Casilda", "Sofa", "5M").sku("Casilda-Sofa-5M").price(15000.0).size("153.5", "78.75", "25.25"));
		resolvers.add(new Resolver("627722006211").key("Casilda").titles("Casilda", "Sunbed").sku("Casilda-Sunbed").price(2200.0).size("79.5", "30.75", "13.5"));
		resolvers.add(new Resolver("627722006310").key("Casilda").titles("Casilda", "Table-SQ-100", "SQ-110").sku("Casilda-Table-SQ-100").price(1950.0).size("39.25", "39.25", "6.75"));
		
		resolvers.add(new Resolver("627722006044").key("Furniture").titles("Furniture", "Comp-06").sku("Furniture-Comp-06").price(5100.0).size("118.0", "22.0", "74.75"));
		resolvers.add(new Resolver("627722006051").key("Furniture").titles("Furniture", "Comp-16.1").sku("Furniture-Comp-16.1").price(8500.0).size("67.25", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006068").key("Furniture").titles("Furniture", "Comp-23").sku("Furniture-Comp-23").price(3750.0).size("107.0", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006082").key("Furniture").titles("Furniture", "Comp-26").sku("Furniture-Comp-26").price(3950.0).size("55.0", "31.5", "74.75"));
		resolvers.add(new Resolver("627722006129").key("Furniture").titles("Furniture", "Comp-31").sku("Furniture-Comp-31").price(3260.0).size("63.0", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006136").key("Furniture").titles("Furniture", "Comp-33.1").sku("Furniture-Comp-33.1").price(4150.0).size("55.0", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006143").key("Furniture").titles("Furniture", "Comp-37").sku("Furniture-Comp-37").price(3150.0).size("48.25", "14.5", "74.75"));
		resolvers.add(new Resolver("627722006150").key("Furniture").titles("Furniture", "Comp-39").sku("Furniture-Comp-39").price(4850.0).size("76.0", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006167").key("Furniture").titles("Furniture", "Comp-43").sku("Furniture-Comp-43").price(2400.0).size("35.5", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006174").key("Furniture").titles("Furniture", "Comp-44").sku("Furniture-Comp-44").price(1850.0).size("41.25", "19.75", "74.75"));
		resolvers.add(new Resolver("627722003975").key("MyBag").sku("MyBag-Wht").price(5799.0).size("73.2", "29.5", "22.4"));
		resolvers.add(new Resolver("627722006075").key("Sophia").titles("Sophia-Furniture", "Comp-25").sku("Sophia-Furniture-Comp-25", "Furniture-Comp-25").price(5600.0).size("72.0", "19.75", "74.75"));
		resolvers.add(new Resolver("627722006457").key("Sophia").titles("Sophia-H", "H-EL", "HEATER").sku("Sophia-H-EL-HEATER").price(1499.0).size("31.5", "15.75", "3.25"));
		resolvers.add(new Resolver("627722006440").key("Sophia").titles("Sophia-V", "V-EL", "HEATER").sku("Sophia-V-EL-HEATER").price(1499.0).size("31.5", "15.75", "3.25"));
		resolvers.add(new Resolver("627722004255").key("Storage-Lovers").sku("Storage-Lovers-Set").price(13248.00));
		
		// ---------Bathtubs----------
		
		resolvers.add(new Resolver("627722PS015A").key("015").titles("015A").sku("PureScape 015A", "PS015A").price(2484.00).size("59", "29.5", "24.75"));
		resolvers.add(new Resolver("627722PS015B").key("015").titles("015B").sku("PureScape 015B", "PS015B").price(2469.16).size("63", "29.5", "24.75"));
		resolvers.add(new Resolver("627722PS015C").key("015").titles("015C").sku("PureScape 015C", "PS015C").price(2469.16).size("67", "29.5", "24.5"));
		resolvers.add(new Resolver("627722PS015D").key("015").titles("015D").sku("PureScape 015D", "PS015D").price(2469.16).size("71", "31.5", "24.5"));
		resolvers.add(new Resolver("627722000172").key("014").titles("014A").sku("PureScape 014A", "PS014A").price(2600.00).size("59", "29.5", "23.25"));
		resolvers.add(new Resolver("627722000196").key("014").titles("014B", "014B+").sku("PureScape 014B", "PS014B").price(2484.00).size("65", "31.5", "23.75"));
		resolvers.add(new Resolver("627722000202").key("014").titles("014C").sku("PureScape 014C", "PS014C").price(2484.00).size("67", "31.5", "23.75"));
		resolvers.add(new Resolver("627722000219").key("014").titles("014D").sku("PureScape 014D", "PS014D").price(2484.00).size("70.75", "33.5", "23.75"));
		resolvers.add(new Resolver("627722000226").key("014").titles("014E").sku("PureScape 014E", "PS014E", "72.75 in. L x 33.50 in. W x 23.50 in. H").price(3174.00).size("74.75", "35.5", "23.75"));
		resolvers.add(new Resolver("627722000233").key("014").titles("014M", "63-in L x 29.5-in W", "63 in. L X 29.5 in. W").sku("PureScape 014M", "PS014M").price(1999.00).size("63", "27.5", "22.75"));
		resolvers.add(new Resolver("627722000332").key("026").titles("026A", "PureScape 026A").sku("10372017", "PS026A").price(2756.55).size("63", "31.5", "22.8"));
		resolvers.add(new Resolver("627722000349").key("026").titles("026B", "PureScape 026B").sku("10372018", "B00V7SPKUO", "PS026B").price(2691.00).size("67", "31.9", "23.5"));
		resolvers.add(new Resolver("627722000356").key("026").titles("026C", "PureScape 026C").sku("10372019", "PS026C").price(2898.00).size("70.75", "33.5", "23.5"));
		resolvers.add(new Resolver("627722000394").key("028").titles("028A", "25.5", "PureScape 028A").sku("12780852", "PS028A").price(2484.00).size("47.25", "29.5", "29.13"));
		resolvers.add(new Resolver("627722000400").key("028").titles("028B", "51", "51.1", "25.5", "PureScape 028B").sku("10359706", "PS028B").price(2484.00).size("51.2", "29.5", "26"));
		// "PureScape 028B" - Failed in OS
		resolvers.add(new Resolver("627722000417").key("028").titles("028C", "55", "26.0", "25.5", "PureScape 028C").sku("10359708", "PS028C").price(2484.00).size("55.1", "29.5", "26"));
		resolvers.add(new Resolver("627722000424").key("028").titles("028D", "58.75", "29.25", "24", "PureScape 028D").sku("PS028D").price(2484.00).size("58.6", "29.2", "24"));
		resolvers.add(new Resolver("627722000448").key("034").sku("PureScape 034", "PS034").price(2622.00).size("", "", "26"));
		resolvers.add(new Resolver("627722000455").key("036").sku("PureScape 036", "PS036").price(2760.00).size("70.3", "34", "31.5"));
		resolvers.add(new Resolver("627722000462").key("040").sku("PureScape 040", "PS040").price(2998.00).size("70.75", "33.5", "23"));
		resolvers.add(new Resolver("627722000509").key("060").sku("PureScape 060", "PS060").price(2759.00).size("63", "31.5", "31.5"));
		resolvers.add(new Resolver("627722000516").key("103").sku("PureScape 103", "PS103").price(2484.00).size("64.57", "31.5", "23.62"));
		resolvers.add(new Resolver("627722007232").key("107").titles("Purescape 107", "HydroRlx", "Relax").sku("PS107-HydroRlx-Int", "PS107").price(3200.00).size("74.5", "35.5", "23.5"));
		resolvers.add(new Resolver("627722000547").key("114").sku("PureScape 114", "PS114").price(2484.00).size("62.6", "31.5", "23.62"));
		resolvers.add(new Resolver("627722000561").key("141").sku("PureScape 141", "PS141").price(3449.31).size("", "", "23.25"));
		resolvers.add(new Resolver("627722000592").key("148").sku("PureScape 148", "PS148").price(1999.00).size("65", "30", "25.5"));
		resolvers.add(new Resolver("627722000622").key("169").sku("PureScape 169", "PS169").price(2484.31).size("56.5", "29.5", "23.6"));
		resolvers.add(new Resolver("627722000639").key("170").sku("PureScape 170", "PS170").price(3099.00).size("74.4", "37.6", "26.8"));
		resolvers.add(new Resolver("627722004095").key("171").titles("Purescape 171", "PS171", "White").sku("PS171M-Wht").price(5995.0).size("72.5", "38.5", "27.0"));
		resolvers.add(new Resolver("627722005092").key("171").titles("Purescape 171", "PS171-Mini", "White", "Mini").sku("PS171M-Mini-Wht").price(3950.0).size("63.0", "38.75", "31.25"));
		resolvers.add(new Resolver("627722004408").key("171").titles("Purescape 171", "PS171", "Black").sku("PS171M-Blck").price(8280.0).size("72.0", "39.5", "31.0"));
		resolvers.add(new Resolver("627722008390").key("171").titles("Purescape 171", "PS171", "Blck-Wht").sku("PS171M-Blck-Wht").price(7495.0).size("72.0", "39.25", "31.0"));
		resolvers.add(new Resolver("627722004781").key("171").titles("Purescape 171", "PS171-Mini-Wht", "Mini").sku("PS171-Mini-Wht").price(3950.0).size("63.0", "38.75", "31.25"));
		resolvers.add(new Resolver("627722002817").key("174").titles("174A").sku("PureScape 174A", "PS174A").price(2990.00).size("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722004477").key("174").titles("Wht-Tranq-Rlx", "Tranq-Rlx", "174B-Wht").sku("PureScape 174B-Wht-Tranq-Rlx", "PS174B-Wht-Tranq-Rlx").price(6950.00).size("68.75", "32.75", "23.5"));
		resolvers.add(new Resolver("627722003067").key("174").titles("174A", "Blck-Wht", "Blck-Wht").sku("174A-Blck-Wht", "PS174A-Blck-Wht").price(3950.00).size("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722003203").key("174").titles("174A", "Rlx", "Relax", "Blck-Wht", "Blck-Wht-Rlx").sku("Purescape 174A-Blck-Wht Relax", "PS174A-Blck-Wht-Rlx", "PS174A-BLK-WHT-RLX").price(5950.0).size("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722003197").key("174").titles("174A", "Rlx", "Relax").sku("Purescape 174A-Wht Relax", "PS174A-Wht-Rlx", "PS174A-Rlx").price(5490.0).size("63.0", "30.0", "23.6"));
		resolvers.add(new Resolver("627722000653").key("174").titles("174B", "PureScape 174 Freestanding Acrylic Bathtub").sku("PS174B", "PureScape 174B").price(2990.00).size("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722003074").key("174").titles("174B", "Blck-Wht", "Blck-Wht").sku("174B-Blck-Wht", "PS174B-Blck-Wht", "PS174B-Black-Wht").price(3950.00).size("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722003180").key("174").titles("174B", "Rlx", "Relax").sku("Purescape 174B-Wht Relax", "PS174B-Wht-Rlx", "PS174B-Rlx").price(5490.0).size("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722003210").key("174").titles("174B", "Rlx", "Relax", "Blck-Wht", "PS174B-Blck-Wht-Rlx").sku("Purescape 174B-Blck-Wht Relax", "PS174B-Blck-Wht-Rlx").price(5950.0).size("68.7", "32.7", "23.5"));
		resolvers.add(new Resolver("627722000714").key("271").sku("PureScape 271", "PS271").price(2622.00).size("70.3", "33", "23.6"));
		resolvers.add(new Resolver("627722000721").key("272").sku("PureScape 272", "PS272").price(2994.60).size("69", "33.5", "30.3"));
		resolvers.add(new Resolver("627722000738").key("302").sku("PureScape 302", "PS302").price(2208.00).size("59", "27.5", "22.8"));
		resolvers.add(new Resolver("6277220PS306").key("306").sku("PureScape 306", "PS306").price(915.00).size("68", "34.65", "23.25"));
		resolvers.add(new Resolver("627722000776").key("308").titles("308A", "PureScape 308A").sku("11511692", "PS308A").price(3312.00).size("54.25", "54.25", "23.5"));
		resolvers.add(new Resolver("627722000783").key("308").titles("308B", "PureScape 308B").sku("11511694", "PS308B").price(3415.50).size("59", "59", "23.625"));
		resolvers.add(new Resolver("627722000790").key("308").titles("308C", "PureScape 308C").sku("11511695", "PS308C").price(3995.10).size("72", "72", "23.5"));
		resolvers.add(new Resolver("627722000806").key("309").titles("309A", "PureScape 309A").sku("PS309A").price(2484.00).size("59", "32", "23.75"));
		resolvers.add(new Resolver("6277PS309Old").key("309").titles("309B", "PureScape 309B").sku("PS309B").price(125.00).size("63", "33", "23.75"));
		resolvers.add(new Resolver("627722PS309C").key("309").titles("309C", "PureScape 309C").sku("PS309C").price(1450.00).size("67", "33.5", "23.75"));
		resolvers.add(new Resolver("627722000882").key("314").sku("PureScape 314", "PS314").price(2898.00).size("57", "57", "27"));
		resolvers.add(new Resolver("627722001971").key("315").sku("PureScape 315", "PS315").price(2898.00).size("62.2", "62.2", "26"));
		resolvers.add(new Resolver("627722000899").key("316").sku("PureScape 316", "PS316").price(3001.50).size("70.8", "37.8", "25.6"));
		resolvers.add(new Resolver("627722000905").key("317").sku("PureScape 317", "PS317").price(2725.50).size("64.6", "33", "23.6"));
		resolvers.add(new Resolver("627722000943").key("324").sku("PureScape 324", "PS324").price(3001.50).size("52.76", "52.76", "22.83"));
		resolvers.add(new Resolver("627722000950").key("325").sku("PureScape 325", "PS325").price(3588.00).size("", "", "23.62"));
		resolvers.add(new Resolver("627722000936").key("323").titles("323A").sku("PureScape 323A", "PS323A").price(2622.00).size("67", "33.5", "22.8"));
		resolvers.add(new Resolver("6277PS327Old").key("327").titles("PureScape 327 Modern Acrylic Freestanding Oval Two Piece Soaker Tub").sku("B0058QQR8A").price(2537.68).size("56.5", "31", "24.3"));
		resolvers.add(new Resolver("627722000967").key("327").titles("327B").sku("PureScape 327B", "PS327B").price(2484.00).size("56.6", "30.7", "23.2"));
		resolvers.add(new Resolver("627722000981").key("400").sku("PureScape 400", "PS400").price(4692.00).size("73.25", "59", "22"));
		resolvers.add(new Resolver("627722001162").key("602").titles("602M", "Lullaby", "Wht").sku("Lullaby-Wht", "PS602M-Wht").price(3600.00).size("61.81", "26.97", "25.59"));
		resolvers.add(new Resolver("627722001094").key("602").titles("602M", "Lullaby", "Blck-Wht", "Blck").sku("PS602M-Blck-Wht", "Lullaby-Blck-Wht").price(4600.00).size("61.81", "26.97", "25.59"));
		resolvers.add(new Resolver("627722003111").key("602").titles("602M", "Lullaby", "Mini", "Mini-Wht").sku("PS602M-Mini-Wht").price(3600.00).size("55", "27.56", "23.78"));
		resolvers.add(new Resolver("627722003128").key("602").titles("602M", "Lullaby", "Mini", "Mini-Blck-Wht").sku("PS602M-Mini-Blck-Wht").price(4600.00).size("55", "27.56", "23.78"));
		resolvers.add(new Resolver("627722004392").key("602").titles("602M", "Lullaby", "Nano", "Nano-Wht").sku("Lullaby-Nano-Wht").price(3600.00).size("51.25", "27.5", "23.5"));
		resolvers.add(new Resolver("627722007904").key("602").titles("602M", "Lullaby", "Nano", "Nano-Blck-Wht").sku("Lullaby-Nano-Blck-Wht").price(4600.00).size("51.25", "27.5", "23.5"));
		resolvers.add(new Resolver("627722PS610M").key("610").titles("610M").sku("PureScape 610M", "PS610M").price(3152.26).size("60.25", "32", "23.5"));
		// resolvers.add(new
		// Resolver("627722001322").key("617").titles("A-G").sku("PureScape
		// 617AG", "PS617AG").price(3312.00).size("63", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001339").key("617").titles("A-M").sku("PureScape 617AM", "PS617AM").price(3174.00).size("63", "31.5", "21.25"));
		// resolvers.add(new
		// Resolver("627722001346").key("617").titles("B-G").sku("PureScape
		// 617BG", "PS617BG").price(3450.00).size("70.75", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001049").key("617").titles("Wht", "White").sku("PS617M-Wht").price(3995.00).size("66.25", "28.5", "24.75"));
		resolvers.add(new Resolver("627722001056").key("617").titles("Blck-Wht").sku("PS617M-Blck-Wht").price(4995.00).size("66.25", "28.5", "24.75"));
		resolvers.add(new Resolver("627722001353").key("617").titles("B-M").sku("PureScape 617M", "PS617M").price(3995.00).size("70.75", "31.5", "21.25"));
		resolvers.add(new Resolver("627722001438").key("621").titles("621M").sku("PureScape 621M", "PS621M").price(3995.00).size("73.62", "33.25", "23.2"));
		resolvers.add(new Resolver("627722001421").key("621").titles("621G", "Glossy").sku("PureScape 621G", "PS621G").price(3995.1).size("76.75", "33.0", "28.0"));
		resolvers.add(new Resolver("627722PS623M").key("623").titles("623M").sku("PureScape 623M", "PS623M").price(1989.01).size("67", "33.5", "22.5"));
		resolvers.add(new Resolver("627722PS701M").key("701").titles("PureScape 701M").sku("PS701M", "701M").price(2334.72).size("69", "29.5", "26.75"));
		resolvers.add(new Resolver("627722001520").key("714").titles("714M").sku("PureScape 714M", "PS714M").price(4999.00).size("70.75", "31.5", "24"));
		resolvers.add(new Resolver("627722001544").key("720").titles("720M", "PS720M-Wht").sku("PureScape 720M", "PS720M", "PS720M-Wht").price(5200.0).size("53", "24", ""));
		resolvers.add(new Resolver("627722004385").key("720").titles("720M", "PS720M-Blck-Wht").sku("PureScape 720M", "PS720M", "PS720M-Blck-Wht").price(6210.0).size("53.25", "53.25", "24.0"));
		resolvers.add(new Resolver("627722001568").key("748").titles("748M", "26830").sku("PureScape 748M", "PS748M", "PS748M-Wht", "26830").price(3600.00).size("63", "33.5", "28.3"));
		resolvers.add(new Resolver("627722002794").key("748").titles("748G", "Glossy", "26829", "Glossy White").sku("PureScape 748G", "PS748G", "PS748G-Wht", "26829").price(4299.00).size("63", "33.5", "28.3"));
		resolvers.add(new Resolver("627722002756").key("748").titles("748M", "Graphite", "Black").sku("PS748M-Blck").price(6950.00).size("63", "33.5", "28.3"));
		resolvers.add(new Resolver("627722007454").key("748").titles("748M", "Blck-Wht").sku("PS748M-Blck-Wht").price(5100.00).size("61.25", "34.75", "28.75"));
		resolvers.add(new Resolver("627722008338").key("748").titles("748M", "Grey", "Brown-Wht").sku("PS748M-Grey-Brown-Wht").price(6600.00).size("61.25", "34.75", "28.75"));
		resolvers.add(new Resolver("627722001353").key("PS617M").titles("Wht", "Corelia").sku("Corelia-Wht", "PS617M").price(3588.00).size("66.35", "28.55", "24.8"));
		resolvers.add(new Resolver("627722001766").key("AdoreMe").titles("White").sku("AdoreMe-Wht").price(3795.00).size("74.8", "35.75", "26.5"));
		resolvers.add(new Resolver("627722002336").key("Allegra").titles("Freestanding", "-> Freestanding").sku("13846865", "Allegra-Wht", "Allegra-FS").price(5200.00).size("", "", "26.75"));
		resolvers.add(new Resolver("627722002343").key("Allegra").titles("Built-in", "Blt-in", "-> Drop-in").sku("13846866", "Allegra-Blt-in-Wht", "Allegra-Blt", "Allegra-Wht Built-In").price(4800.0).size("", "", "26.75"));
		resolvers.add(new Resolver("627722003661").key("Allegra").titles("Built-in", "Relax", "Rlx").sku("Allegra-Wht Built-In Relax", "Allegra-Blt-Rlx").price(7495.0).size("74.8", "74.8", "26.75"));
		resolvers.add(new Resolver("6277AlleGran").key("Allegra").titles("Granite", "Gran", "Pearl Grey").sku("Allegra-G").price(3995.10).size("74.75", "74.75", "26.75"));
		resolvers.add(new Resolver("627722003678").key("Allegra").titles("Freestanding", "Relax", "Rlx").sku("Allegra-FS-Rlx", "Allegra-Wht Freestanding Relax").price(7995.00).size("74.8", "74.8", "26.75"));
		resolvers.add(new Resolver("6277AllePear").key("Allegra").titles("Pearl Bark").price(3995.10).size("74.75", "74.75", "26.75"));
		resolvers.add(new Resolver("627722004521").key("Allegra").titles("FS-Spa", "Spa", "Jetted").sku("Allegra-FS-Spa").price(11950.0).size("74.75", "74.75", "26.75"));
		resolvers.add(new Resolver("627722004538").key("Allegra").titles("FS-Spa-Int", "Spa-Int", "Jetted").sku("Allegra-FS-Spa-Int").price(11950.0).size("74.75", "74.75", "26.75"));
		resolvers.add(new Resolver("627722002374").key("Alex").sku("Alex-Wht").price(2898.00).size("67", "35.25", "25.2"));
		resolvers.add(new Resolver("627722001759").key("AdmireMe").titles("White").sku("AdmireMe-Wht", "AdmireMe-Wht-Std").price(7038.00).size("78.75", "59", "30.75"));
		resolvers.add(new Resolver("6277AdmMeRED").key("AdmireMe").titles("Red").sku("AdmireMe-Red").price(8950.00).size("78.75", "59", "33"));
		resolvers.add(new Resolver("627722002718").key("AdmireMe").titles("Relax", "AdmireMe-Wht-Rlx").sku("AdmireMe-Relax", "AdmireMe-Wht-Rlx", "AdmireMe-Wht Relax").price(11005.50).size("78.75", "59", "33.25"));
		resolvers.add(new Resolver("627722004194").key("Anette").titles("A-L", "Wht", "Corner", "Bathtub", "Anette-A").sku("Anette-A-L").price(2484.0).size("59.0", "37.5", "24.75"));
		resolvers.add(new Resolver("627722004187").key("Anette").titles("A-R", "Wht", "Corner", "Bathtub", "Anette-A").sku("Anette-A-R").price(2484.0).size("59.0", "37.5", "24.75"));
		resolvers.add(new Resolver("627722004156").key("Anette").titles("B-L", "Wht", "Corner", "Bathtub", "Anette-B").sku("Anette-B-L").price(2484.0).size("63.0", "37.5", "24.75"));
		resolvers.add(new Resolver("627722004149").key("Anette").titles("B-R", "Wht", "Corner", "Bathtub", "Anette-B").sku("Anette-B-R").price(2484.0).size("63.0", "37.5", "24.75"));
		resolvers.add(new Resolver("627722005382").key("Anette").titles("C-L", "Wht", "Corner", "Bathtub", "Anette-C").sku("Anette-C-L").price(2484.0).size("66.5", "38.25", "25.5"));
		resolvers.add(new Resolver("627722005375").key("Anette").titles("C-R", "Wht", "Corner", "Bathtub", "Anette-C").sku("Anette-C-R").price(2484.0).size("66.5", "38.25", "25.5"));
		resolvers.add(new Resolver("627722002060").key("Arab").titles("Arabella", "White").sku("Arab-Wht", "Arab-F-Wht").price(3995.00).size("68.5", "30.3", "24.4"));
		resolvers.add(new Resolver("627722002572").key("Arab").titles("Arabella", "Left", "Corner", "Arabella-L", "Aquastone White Bathtub with Rounded Left Corner -> With Rounded Left Corner").sku("Arab-L-Wht").price(3995.00).size("72", "33.85", "24.4"));
		resolvers.add(new Resolver("627722002565").key("Arab").titles("Arabella", "Wall", "Aquastone White Bathtub with Rounded Left Corner -> WithWall Ledge").sku("Arab-W-Wht", "Arabella-Wall", "Arab-Wall").price(3995.00).size("68.5", "33.85", "24.4"));
		resolvers.add(new Resolver("627722004354").key("Arab").titles("Arabella", "Black-Wht", "Arabella-Black-Wht").sku("Arab-Blck-Wht", "Arabella-Blck-Wht").price(4968.0).size("68.5", "30.25", "24.5"));
		resolvers.add(new Resolver("6277Atlantic").key("Atlantica").price(3152.26).size("60.25", "41.75", "24.25"));
		
		resolvers.add(new Resolver("627722005559").key("Baby-Boomer").titles("Jetted", "Oxygen", "Spa", "Left", "Baby-Boomer-L").sku("Baby-Boomer-L-OxySpa").price(8800.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722005498").key("Baby-Boomer").titles("Jetted", "Left", "Spa", "Baby-Boomer-L").sku("Baby-Boomer-L-Spa").price(6999.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722005504").key("Baby-Boomer").titles("Jetted", "Left", "Tranquility", "Baby-Boomer-L").sku("Baby-Boomer-L-Tranq").price(5999.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722005481").key("Baby-Boomer").titles("Jetted", "Left", "Soaking", "Baby-Boomer-L").sku("Baby-Boomer-L-Wht").price(4999.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722005566").key("Baby-Boomer").titles("Jetted", "Oxygen", "Spa", "Right", "Baby-Boomer-R").sku("Baby-Boomer-R-OxySpa").price(8800.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722004910").key("Baby-Boomer").titles("Jetted", "Right", "Spa", "Baby-Boomer-R").sku("Baby-Boomer-R-Spa").price(6999.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722005511").key("Baby-Boomer").titles("Jetted", "Right", "Tranquility", "Baby-Boomer-R").sku("Baby-Boomer-R-Tranq").price(5999.00).size("51.25", "29.5", "41.25"));
		resolvers.add(new Resolver("627722004903").key("Baby-Boomer").titles("Jetted", "Right", "Soaking", "Baby-Boomer-R").sku("Baby-Boomer-R-Wht").price(4999.00).size("51.25", "29.5", "41.25"));
		
		resolvers.add(new Resolver("627722001629").key("Carol").sku("Carol-Wht").sku("Carol-Wht-Std").price(2599.0).size("74.8", "35.8", "26.4"));
		resolvers.add(new Resolver("627722002107").key("Cleo").titles("Cleopatra", "Cleopatra-Wht").sku("Cleo-Wht").price(2990.00).size("61", "61", "25.25"));
		resolvers.add(new Resolver("627722005184").key("Cleo").titles("Cleopatra", "Cleo-Wht-HydroRlx", "HydroRlx").sku("Cleo-Wht-HydroRlx-Pro-Int", "Cleo-Wht-HydroRlx").price(8990.00).size("61", "61", "25.25"));
		resolvers.add(new Resolver("627722000011").key("Cocoon").titles("White Corner Lucite with Microban").sku("Cocoon-Wht").price(1499.00).size("63", "31.5", "24.4"));
		resolvers.add(new Resolver("627722004774").key("Coletta").titles("Sandstn", "Sandstone").sku("Coletta-Sandstn").price(12000.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722005528").key("Coletta").titles("Concrete").sku("Coletta-Concrete").price(12000.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722004811").key("Coletta").titles("Wht", "White").sku("Coletta-Wht").price(5200.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722005542").key("Coletta").titles("Blck", "Black").sku("Coletta-Blck").price(8280.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722008369").key("Coletta").titles("Blck-Wht", "Black-Wht", "Blck-White").sku("Coletta-Blck-Wht").price(6700.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722001650").key("Corelia").titles("Black-Wht").sku("Corelia-Black-Wht").size("66.35", "28.55", "24.8").price(4995.0));
		resolvers.add(new Resolver("627722007225").key("Corelia").titles("Black", "Corelia-Black").sku("Corelia-M-Blck").size("66.5", "34.25", "20.75").price(6600.0));
		resolvers.add(new Resolver("627722008239").key("Corelia").titles("Green-Wht", "Moss", "Moss-Green-Wht").sku("Corelia-Moss-Green-Wht").size("66.5", "34.25", "20.75").price(6995.0));
		resolvers.add(new Resolver("627722007874").key("Coletta").titles("Blue", "Jaffa", "Blue-Wht").sku("Coletta-Jaffa-Blue-Wht").price(8200.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722007928").key("Coletta").titles("Red", "Oxide", "Red-Wht").sku("Coletta-Oxide-Red-Wht").price(8200.0).size("70.75", "35.5", "19.75"));
		resolvers.add(new Resolver("627722007140").key("Crystal").sku("Crystal", "Spa").sku("Crystal-Spa").price(39990.0).size("91.0", "91.0", "30.75"));
		
		resolvers.add(new Resolver("627722001384").key("Daydr").titles("Daydreamer-A", "Daydr-A").sku("Daydr-A-Wht-Int").price(6893.10).size("63", "31.5", "23.62"));
		resolvers.add(new Resolver("627722001391").key("Daydr").titles("Daydreamer-B", "Daydr-B").sku("Daydr-B-Wht-Int").price(6893.10).size("66.9", "31.5", "23.62"));
		resolvers.add(new Resolver("627722002923").key("Daydr").titles("Daydreamer-C", "Daydr-C", "Daydr-Wht-Int").sku("Daydr-C-Wht-Int").price(6893.10).size("70.87", "31.5", "23.62"));
		resolvers.add(new Resolver("627722001582").key("Daydr").titles("Daydreamer-D", "Daydr-D").sku("Daydr-D-Wht-Int").price(6893.10).size("74.8", "31.5", "23.62"));
		resolvers.add(new Resolver("627722003104").key("Daydr").titles("Daydreamer-Mini, Mini").sku("Daydr-Mini-Wht-Int").price(6893.10).size("59", "31.5", "23.62"));
		resolvers.add(new Resolver("627722006228").key("Dream").titles("Dream-A", "Jetted").sku("Dream-A-SpaBath").price(5499.0).size("67.0", "27.5", "25.5"));
		resolvers.add(new Resolver("627722006235").key("Dream").titles("Dream-B", "Jetted").sku("Dream-B-SpaBath").price(5499.0).size("70.75", "31.5", "26.5"));
		resolvers.add(new Resolver("627722006242").key("Dream").titles("Dream-C", "Jetted").sku("Dream-C-SpaBath").price(5999.0).size("70.75", "39.25", "26.5"));
		resolvers.add(new Resolver("627722006273").key("Dream").titles("Dream-D", "Jetted").sku("Dream-D-SpaBath").price(5999.0).size("74.75", "35.5", "26.5"));
		resolvers.add(new Resolver("627722006105").key("Dream").titles("Ovatus", "Dream", "Jetted").sku("Dream-Ovatus-Spa-Out", "Dream-Ovatus-SpaBath").price(7999.00).size("74.75", "43.25", "25.25"));
		resolvers.add(new Resolver("627722006099").key("Dream").titles("Rondo", "Dream", "Jetted").sku("Dream-Rondo-Spa-Out", "Dream-Rondo-SpaBath").price(7999.00).size("63.0", "63.0", "25.25"));
		resolvers.add(new Resolver("627722006648").key("Dream").titles("Ovatus", "Dream").sku("Dream-Ovatus-Basic").price(3995.00).size("74.75", "43.25", "25.25"));
		resolvers.add(new Resolver("627722006631").key("Dream").titles("Rondo", "Dream").sku("Dream-Rondo-Basic").price(3995.00).size("63.0", "63.0", "25.25"));
		resolvers.add(new Resolver("627722006112").key("Dream").titles("Cube", "Dream", "Jetted").sku("Cube-Ovatus-Spa-Out", "Cube-Ovatus-SpaBath", "Dream-Cube-SpaBath").price(7999.00).size("78.75", "63.0", "26.0"));
		resolvers.add(new Resolver("627722007249").key("Duet").titles("Wht", "White").sku("Duet-Wht").price(3174.00).size("70.5", "47.25", "26.75"));
		
		resolvers.add(new Resolver("627722002114").key("Elise").titles("White").sku("Elise-Wht").price(3995.00).size("67", "32", "24.5"));
		resolvers.add(new Resolver("627722002046").key("Emma").titles("Emmanuelle-Wht", "White").sku("Emma-Wht").price(2800.00).size("71.75", "33.75", "31"));
		resolvers.add(new Resolver("627722004927").key("Emma").titles("Emmanuelle-Wht-2", "White").sku("Emma-Wht-2").price(5495.00).size("66.0", "35.0", "32.75"));
		resolvers.add(new Resolver("627722005665").key("Emma").titles("Emmanuelle-2-Wht-Rlx", "White", "Relax").sku("Emma-2-Wht-Rlx").price(8950.00).size("66.25", "35.0", "32.75"));
		resolvers.add(new Resolver("627722005658").key("Emma").titles("Emmanuelle-2-Blck", "Black").sku("Emma-2-Blck").price(8280.00).size("66.25", "35.0", "32.75"));
		resolvers.add(new Resolver("627722008376").key("Emma").titles("Emmanuelle-2-Blck-Wht", "Emmanuelle-Blck-Wht-2", "Black-Wht").sku("Emma-Blck-Wht-2", "Emma-2-Blck-Wht").price(6995.00).size("66.25", "35.0", "32.75"));
		resolvers.add(new Resolver("627722002404").key("Evol").titles("Evolution-Wht", "Evolution-White", "White").price(3312.00).size("74.8", "68.9", "30.71"));
		resolvers.add(new Resolver("627722002619").key("Evol").titles("Evolution-Wht", "Relax", "Built-in").sku("Evo-Wht-Rlx").price(7935.00).size("74.8", "68.9", "30.71"));
		resolvers.add(new Resolver("627722002053").key("Fido").titles("White", "Wht").sku("Fido-Wht").price(3950.00).size("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002596").key("Fido").titles("Black").sku("Fido-Blck", "Fido-Black").price(4950.00).size("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002169").key("Fido").titles("Red").sku("Fido-Red").price(4950.00).size("65.75", "29", "26.4"));
		resolvers.add(new Resolver("627722002602").key("Fido").titles("Blue").sku("Fido-Blue").price(4950.00).size("65.75", "29.25", "26.5"));
		
		resolvers.add(new Resolver("627722004897").key("Fusion").titles("Cube", "Fusion", "Jetted").sku("Fusion-Cube-Spa-Out", "Fusion-Cube-SpaBath").price(19990.00).size("90.5", "70.75", "26.0"));
		resolvers.add(new Resolver("627722004873").key("Fusion").titles("Ovatus", "Fusion", "Jetted").sku("Fusion-Ovatus-Spa-Out", "Fusion-Ovatus-SpaBath").price(19990.00).size("90.5", "59.0", "26.25"));
		resolvers.add(new Resolver("627722004866").key("Fusion").titles("Rondo", "Fusion", "Jetted").sku("Fusion-Rondo-Spa-Out", "Fusion-Rondo-SpaBath").price(19990.00).size("79.0", "79.0", "26.25"));
		resolvers.add(new Resolver("627722004880").key("Fusion").titles("Lineare", "Fusion", "Jetted").sku("Fusion-Lineare-Spa-Out", "Fusion-Lineare-SpaBath").price(15490.00).size("86.5", "47.25", "25.5"));
		resolvers.add(new Resolver("627722006037").key("Fusion").titles("Cube", "Fusion").sku("Fusion-Cube-Spa").price(21990.00).size("90.5", "70.75", "26.0"));
		resolvers.add(new Resolver("627722006020").key("Fusion").titles("Ovatus", "Fusion").sku("Fusion-Ovatus-Spa").price(21990.00).size("90.5", "59.0", "26.25"));
		resolvers.add(new Resolver("627722006013").key("Fusion").titles("Rondo", "Fusion").sku("Fusion-Rondo-Spa").price(21990.00).size("79.0", "79.0", "26.25"));
		resolvers.add(new Resolver("627722006280").key("Fusion").titles("Lineare", "Fusion").sku("Fusion-Lineare-Spa").price(19990.00).size("86.5", "47.25", "25.5"));
		resolvers.add(new Resolver("627722002299").key("Gemina").titles("Gemn", "Gemn-Rect", "Rect", "Freestanding").sku("Gemn-Rect-Wht").price(3415.00).size("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002312").key("Gemina").titles("Gemn", "Gemn-R", "Right").sku("Gemn-R-Wht").price(3415.50).size("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002305").key("Gemina").titles("Gemn", "Gemn-L", "Left").sku("Gemn-L-Wht").price(3415.55).size("67", "29.5", "24.6"));
		resolvers.add(new Resolver("627722002329").key("Gemina").titles("Gemn", "Gemn-W", "Wall").sku("Gemina-W-Wht", "Gemn-Wall-Wht", "4551").price(3415.55).size("67", "29.5", "24.61"));
		resolvers.add(new Resolver("627722001827").key("Gemina").titles("Gemn", "Esp", "Gemina-Espresso", "Wall").sku("Gemn-W-Esp").price(4485.0).size("67.0", "29.5", "24.61"));
		resolvers.add(new Resolver("627722002640").key("Harmony").titles("Duo").sku("Harmony-Duo").price(2208.00).size("70.85", "31.5", "22.25"));
		resolvers.add(new Resolver("6277HarmSolo").key("Harmony Solo-Wht").titles("Solo").price(2484.00).size("63", "31.5", "22.25"));
		resolvers.add(new Resolver("627722002091").key("Idea").titles("Idea-L", "Left").sku("Idea-L-Wht").price(2622.00).size("59", "35.75", "25.25"));
		resolvers.add(new Resolver("627722002190").key("Idea").titles("Idea-R", "Right").sku("Idea-R-Wht").price(2622.00).size("59", "35.75", "25.25"));
		
		resolvers.add(new Resolver("627722006617").key("Iliad").titles("Iliad", "Art").sku("Iliad-Art").price(5995.00).size("67.0", "31.5", "28.25"));
		resolvers.add(new Resolver("627722006600").key("Iliad").titles("Iliad", "Wht", "White").sku("Iliad-Wht").price(3995.00).size("67.0", "31.5", "28.25"));
		resolvers.add(new Resolver("627722002398").key("Imagin").titles("Istanbul").sku("Imagination-Wht").price(2499.00).size("", "", "22"));
		resolvers.add(new Resolver("627722006624").key("Impero").titles("Impero", "Wht", "White").sku("Impero-Wht").price(3995.00).size("69.25", "32.25", "29.5"));
		resolvers.add(new Resolver("627722002411").key("inno").titles("Istanbul", "White").sku("Istan-Innov-Wht", "Innovation-Wht").price(1999.00).size("74.8", "35.5", "22"));
		resolvers.add(new Resolver("627722000028").key("Inspir").titles("Istanbul", "White", "Wht").price(8273.10).size("", "", "22"));
		resolvers.add(new Resolver("627722005344").key("Infinity").titles("Therapy", "R1-Tranq-Rlx").sku("Infinity-R1-Tranq-Rlx").price(9990.0).size("74.75", "74.75", "25.5"));
		resolvers.add(new Resolver("627722005351").key("Infinity").titles("Therapy", "R1-Tranq-Rlx-Int").sku("Infinity-R1-Tranq-Rlx-Int").price(9990.0).size("74.75", "74.75", "25.5"));
		resolvers.add(new Resolver("627722002206").key("Inflection").titles("A-L", "Left", "Corner").sku("Infl-A-L-Wht").price(2725.50).size("59.5", "29.53", "22.25"));
		resolvers.add(new Resolver("627722002237").key("Inflection").titles("A-F", "Infl-A-F", "Without Wall", "Freestanding").sku("Infl-A-F-Wht").price(2726.0).size("61.42", "29.5", "22.25"));
		resolvers.add(new Resolver("627722001872").key("Inflection").titles("A-Rect", "Rectangular", "Rect").sku("Infl-A-Rect").price(2725.50).size("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002213").key("Inflection").titles("A-R", "Right", "Corner").sku("Infl-A-R-Wht").price(2725.50).size("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002220").key("Inflection").titles("A-W", "With Wall", "Wall").sku("Infl-A-W-Wht").price(2725.50).size("59.5", "29.53", "22.24"));
		resolvers.add(new Resolver("627722002282").key("Inflection").titles("B-F", "Freestanding").sku("Infl-B-F-Wht").price(2898.00).size("68.9", "31.5", "25.75"));
		resolvers.add(new Resolver("627722002251").key("Inflection").titles("B-L", "Left", "Corner", "69 X 31.5 X 25.75", "Inflection-B EcoMarmor Stone Bathtub").sku("Infl-B-L-Wht").price(2898.00).size("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002244").key("Inflection").titles("B-Rect", "Rectangular", "Rect").sku("Infl-B-Rect-Wht").price(2898.00).size("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002268").key("Inflection").titles("B-R", "Right", "Corner").sku("Infl-B-R-Wht").price(2898.00).size("68.9", "31.5", "25.79"));
		resolvers.add(new Resolver("627722002275").key("Inflection").titles("B-W", "With Wall", "Wall").sku("Infl-B-W-Wht").price(2898.00).size("68.9", "31.5", "25.79"));
		
		resolvers.add(new Resolver("627722001667").key("Jane").titles("Wht-Std", "Corner").sku("Jane-Wht-Std").price(3449.0).size("59.0", "33.5", "25.2"));
		// resolvers.add(new
		// Resolver("627722001032").key("Karolina").titles("Fine Matte",
		// "Matte", "Wht-Matte", "White").sku("PS503M-Wht",
		// "karolina-freestanding-stone-bathtub").price(4278.00).size("70.75",
		// "35.5", "25.5"));
		resolvers.add(new Resolver("627722002961").key("Karolina").titles("Fine Matte", "Matte", "Wht-Matte", "White").sku("PS503M-Wht", "karolina-freestanding-stone-bathtub", "627722001032", "Karolina-M-Wht-USA").price(5200.00).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722001025").key("Karolina").titles("Hand-Polished", "Glossy").price(4795.00).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002749").key("Karolina").titles("Graphite", "Black").sku("PS503M-Blck", "Karolina-Blck-Matte").price(8280.00).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722008383").key("Karolina").titles("Blck-Wht").sku("Karolina-Blck-Wht").price(6700.00).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002732").key("Karolina").titles("Relax", "air", "Matte", "With", "rlx").sku("Karolina-Wht-Matte-Rlx", "karolina-relax-stone-air-massage-bathtub-fine-matte", "PS503M-Wht-Rlx").price(8935.50).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002725").key("Karolina").titles("Relax", "air", "Hand-Polished", "Glossy", "rlx").sku("Karolina-Wht-Glossy-Rlx").price(8935.50).size("70.75", "35.5", "25.5"));
		resolvers.add(new Resolver("627722002077").key("Lacus").titles("Lacus-Wht", "White").sku("Lacus-Wht-Std").price(3499.00).size("70", "70", "28.25"));
		resolvers.add(new Resolver("627722002633").key("Lacus").titles("Lacus-Wht", "Relax").sku("Lacus-Wht-Rlx").price(6499.00).size("70", "70", "28.25"));
		resolvers.add(new Resolver("627722005283").key("Lacus").titles("Lacus-Wht", "Outdoor").sku("Lacus-Wht-Out").price(4995.00).size("70", "70", "28.25"));
		resolvers.add(new Resolver("627722004569").key("Lacus").titles("Lacus-Wht-Spa", "White").sku("Lacus-Wht-Spa-Int").price(9990.00).size("70", "70", "28.25"));
		resolvers.add(new Resolver("627722007133").key("Laguna").titles("Laguna", "Spa").sku("Laguna-Spa").price(39990.00).size("91.75", "91.75", "35.5"));
		resolvers.add(new Resolver("627722000035").key("Liquid").titles("Space").sku("Liq-Spc-Wht").price(1999.00).size("55.1", "55.1", "22.5"));
		resolvers.add(new Resolver("627722001834").key("LoveMe").titles("LoveMe-Wht", "White").sku("LoveMe-Wht").price(2898.00).size("71", "33.5", "29"));
		resolvers.add(new Resolver("627722002589").key("LoveMe").titles("LoveMe-Blck", "Black").sku("LoveMe-Blck-Wht").price(3726.00).size("71", "33.5", "29"));
		
		resolvers.add(new Resolver("627722007980").key("Maya").titles("Spa-Pro", "Spa", "Maya-Spa").sku("Maya-Spa-Pro").price(25990.00).size("102.25", "80.25", "37.5"));
		resolvers.add(new Resolver("627722007621").key("Moonlight").titles("Moonlight", "Spa").sku("Moonlight-Spa").price(17000.00).size("80.75", "80.75", "34.5"));
		resolvers.add(new Resolver("627722007973").key("Muse").titles("Muse", "Spa").sku("Muse-Spa-Pro").price(25990.00).size("110.25", "92.5", "38.5"));
		resolvers.add(new Resolver("627722008567").key("Nexus").titles("Nexus", "Spa").sku("Nexus-Spa").price(35990.00).size("84.75", "67.25", "29.5"));
		resolvers.add(new Resolver("627722000097").key("Noa 1").titles("Noa", "Semi-Freestanding").price(3999.00).size("70.75", "31.5", "23"));
		resolvers.add(new Resolver("627722000103").key("Noa 2").titles("Noa", "Semi-Freestanding").price(4499.00).size("78.75", "35.5", "23"));
		resolvers.add(new Resolver("627722001858").key("Nostalgia").titles("Stn-Legs", "Satin").sku("B00ARE8A1U", "Nosta-Wht-Stn-Legs").price(3415.50).size("67", "32.75", "29.75"));
		resolvers.add(new Resolver("627722001865").key("Nostalgia").titles("Ash-Legs", "Ash").sku("B00ARE89ZM", "Nosta-Wht-Ash-Legs").price(3726.00).size("67", "32.75", "29.75"));
		resolvers.add(new Resolver("627722002084").key("Oliv").titles("Olivia", "White").sku("Oliv-Wht").price(2899.00).size("55", "55", "26.75"));
		resolvers.add(new Resolver("627722002800").key("Oliv").titles("Olivia", "White", "Relax", "Oliv-Wht-Rlx").sku("Oliv-Wht-Rlx", "Olivia-Wht-Relax", "Olivia-Wht-Rlx").price(5899.00).size("55", "55", "26.75"));
		resolvers.add(new Resolver("627722004828").key("Oliv").titles("Olivia", "Spa", "Wht-Spa").sku("Olivia-Wht-Spa").price(6950.00).size("55", "55", "26.75"));
		resolvers.add(new Resolver("627722004835").key("Oliv").titles("Olivia", "Spa-Int", "Wht-Spa-Int").sku("Olivia-Wht-Spa-Int").price(7499.00).size("55", "55", "26.75"));
		resolvers.add(new Resolver("627722005412").key("Oliv").titles("Olivia", "White", "Corner").sku("Oliv-B-Wht", "Olivia-B-Wht").price(2999.00).size("58.75", "58.75", "26.75"));
		resolvers.add(new Resolver("627722005429").key("Oliv").titles("Olivia", "White", "Relax", "Oliv-B-Wht-Rlx", "B-Wht-Rlx").sku("Oliv-B-Wht-Rlx", "Olivia-B-Wht-Relax", "Olivia-B-Wht-Rlx").price(5999.00).size("58.75", "58.75", "26.75"));
		resolvers.add(new Resolver("627722005436").key("Oliv").titles("Olivia", "Spa", "B-Wht-Spa").sku("Olivia-B-Wht-Spa").price(6995.00).size("58.75", "58.75", "26.75"));
		resolvers.add(new Resolver("627722005443").key("Oliv").titles("Olivia", "Spa-Int", "B-Wht-Spa-Int").sku("Olivia-B-Wht-Spa-Int").price(7499.00).size("58.75", "58.75", "26.75"));
		resolvers.add(new Resolver("627722004347").key("Olympian").titles("Olympian White").sku("Olympian-Wht").price(6210.0).size("70.75", "37.5", "22.75"));
		resolvers.add(new Resolver("627722001810").key("Organic").titles("Organic-Bath-Coffee", "Coffee").sku("Org-Coffee-Std").price(4968.00).size("91.25", "53.1", "28.75"));
		resolvers.add(new Resolver("627722001803").key("Organic").titles("Organic-Bath-Wht", "White", "Wht").sku("Org-Wht-Std").price(4795.50).size("91.25", "53.1", "28.75"));
		resolvers.add(new Resolver("627722002381").key("Pamela").titles("Pamela-Wht", "White").sku("Pam-Wht-Std").price(5200.00).size("68.0", "68.0", "29.5"));
		resolvers.add(new Resolver("627722005306").key("Pamela").titles("Pamela-Wht-Out", "White").sku("Pam-Wht-Out", "Pamela-Wht-Out").price(5995.0).size("68.0", "68.0", "29.5"));
		resolvers.add(new Resolver("627722002626").key("Pamela").titles("Relax", "White", "With").sku("Pamela-Wht-Relax", "Pamela-Wht-Rlx", "Pam-Wht-Rlx").price(7995.00).size("68.0", "68.0", "29.5"));
		resolvers.add(new Resolver("627722004507").key("Pamela").titles("Usa", "White", "Spa").sku("Pamela-Wht-Spa-Int", "Pam-Wht-Spa-Int").price(11950.00).size("68.0", "68.0", "29.5"));
		resolvers.add(new Resolver("627722004514").key("Pamela").titles("Int", "White", "Spa").sku("Pamela-Wht-Spa", "Pam-Wht-Spa").price(11950.00).size("68.0", "68.0", "29.5"));
		resolvers.add(new Resolver("627722007102").key("Pearl").titles("Pearl", "Spa").sku("Pearl-Spa").price(14990.00).size("78.75", "65.25", "27.5"));
		resolvers.add(new Resolver("627722008680").key("Phantom").titles("Pro", "Spa").sku("Phantom-Spa-Pro").price(25990.00).size("0.0", "0.0", "0.0"));
		resolvers.add(new Resolver("627722005191").key("Piccolo").titles("White", "Wht").sku("Piccolo-Wht").price(2950.00).size("58.75", "33.5", "33.25"));
		resolvers.add(new Resolver("627722002428").key("Pool").titles("Pool-Wht", "White").price(3995.10).size("63", "63", "27.5"));
		resolvers.add(new Resolver("62772200PSol").key("Pool").titles("Solo-Wht", "Solo").price(240.00).size("74.8", "51.2", "23.4"));
		resolvers.add(new Resolver("62772200PDuo").key("Pool").titles("Duo-Wht", "Duo").price(240.00).size("74.8", "55.1", "23.4"));
		resolvers.add(new Resolver("627722002930").key("Pure-1").titles("Pure 1", "1L", "LOak", "Light", "Light Oak").sku("Pure-1-Wht-LOak").price(4950.00).size("67", "31.5", "24"));
		resolvers.add(new Resolver("627722003043").key("Pure-1").titles("Pure 1", "1D", "DOak", "Dark", "Dark Oak").sku("Pure-1-Wht-DOak").price(4950.00).size("67", "31.5", "24"));
		resolvers.add(new Resolver("627722002947").key("Pure-2").titles("Pure 2", "2D", "DOak", "Dark", "Dark Oak").sku("XAI1407", "Pure-2-Wht-DOak").price(5950.00).size("82.75", "31.5", "24"));
		resolvers.add(new Resolver("627722003050").key("Pure-2").titles("Pure 2", "2L", "LOak", "Light", "Light Oak").sku("Pure-2-Wht-LOak").price(5950.00).size("82.75", "31.5", "24"));
		resolvers.add(new Resolver("627722006181").key("Rest").titles("Rest", "Spa").sku("Rest-Spa-Pro").price(23990.31).size("94.5", "76.75", "33.5"));
		
		resolvers.add(new Resolver("627722007355").key("Santosa").titles("Santosa-1", "Santosa 1", "Spa").sku("Santosa-1-Spa").price(32000.00).size("73.25", "73.25", "33.5"));
		resolvers.add(new Resolver("627722007362").key("Santosa").titles("Santosa-2", "Santosa 2", "Spa").sku("Santosa-2-Spa").price(39990.00).size("92.25", "91.0", "45.25"));
		resolvers.add(new Resolver("627722007379").key("Santosa").titles("Santosa-3", "Santosa 3", "Spa").sku("Santosa-3-Spa").price(62990.00).size("73.25", "69.0", "33.75"));
		resolvers.add(new Resolver("627722007386").key("Santosa").titles("Santosa-4", "Santosa 4", "Spa").sku("Santosa-4-Spa").price(72990.00).size("94.0", "91.0", "45.25"));
		resolvers.add(new Resolver("627722007393").key("Santosa").titles("Santosa-5", "Santosa 5", "Spa").sku("Santosa-5-Spa").price(77990.00).size("94.0", "78.75", "45.25"));
		resolvers.add(new Resolver("627722007409").key("Santosa").titles("Santosa-6", "Santosa 6", "Spa").sku("Santosa-6-Spa").price(29990.00).size("73.25", "73.25", "33.5"));
		resolvers.add(new Resolver("627722007416").key("Santosa").titles("Santosa-7", "Santosa 7", "Spa").sku("Santosa-7-Spa").price(38100.00).size("91.0", "91.0", "33.75"));
		
		resolvers.add(new Resolver("627722001698").key("Silence").sku("Silence-Wht").price(1499.00).size("67", "35.5", "27"));
		resolvers.add(new Resolver("627722002916").key("Sincera").titles("Sinc").sku("SNCR-Wll-Wht").price(3950.00).size("63", "28", "24"));
		resolvers.add(new Resolver("627722001988").key("Sincera").titles("Sinc", "Blck-Wht", "Wall", "Surface", "Black"));
		resolvers.add(new Resolver("627722002350").key("Sensuality").titles("Mini", "Mini-F-Wht").sku("13846862", "SensMini-Free-Wht", "SensMini-F-Wht").price(3950.00).size("66.5", "33.0", "26.75"));
		resolvers.add(new Resolver("627722002367").key("Sensuality").titles("Mini", "Mini-Wall").sku("13846863", "SensMini-Wll-Wht").price(4995.00).size("66.5", "37.0", "26.75"));
		resolvers.add(new Resolver("627722003166").key("Sensuality").titles("Mini", "Mini-F-Black-Wht", "Black-Wht").sku("Sens-Mini-F-Black-Wht").price(4995.0).size("66.5", "33.0", "26.75"));
		resolvers.add(new Resolver("627722001940").key("Sensuality").titles("Wht", "Sensuality-White", "Sensuality-Wht").sku("Sens-Wht").price(4495.00).size("70", "35", "25.5"));
		resolvers.add(new Resolver("627722003159").key("Sensuality").titles("Blck-Wht").sku("Sens-Blck-Wht").price(5495.0).size("70.0", "35.0", "25.5"));
		resolvers.add(new Resolver("627722004934").key("Sensuality").titles("Mini", "Mini-F-Wht-Rlx", "Relax", "Wht").sku("Sens-Mini-F-Wht-Rlx", "Sens-Mini-Free-Wht-Rlx").price(7299.00).size("66.5", "33", "26.75"));
		resolvers.add(new Resolver("627722004941").key("Sensuality").titles("Mini", "Mini-F-Blck-Wht-Rlx", "Rlx", "Blck-Wht").sku("Sens-Mini-F-Black-Wht-Rlx", "Sens-Mini-Free-Black-Wht-Rlx").price(8299.00).size("66.5", "33", "26.75"));
		resolvers.add(new Resolver("627722008413").key("Sensuality").titles("Mini", "Blck-Wht", "Back-to-Wall").sku("SensMini-Wll-Blck-Wht").price(6495.0).size("66.5", "37.0", "26.75"));
		
		resolvers.add(new Resolver("627722005146").key("Sophia").titles("Sophia-M-Wht", "Matte").sku("Sophia-M-Wht", "Lavatory").price(4800.0).size("56.0", "34.5", "27.25"));
		resolvers.add(new Resolver("627722005207").key("Sophia").titles("Sophia-Wht", "Gloss").sku("Sophia-Wht", "Lavatory").price(4800.0).size("56.0", "34.5", "27.25"));
		resolvers.add(new Resolver("627722007218").key("Sophia").titles("Sophia-M-Blck", "Matte").sku("Sophia-M-Blck").price(6950.0).size("57.0", "34.75", "27.25"));
		resolvers.add(new Resolver("627722008406").key("Sophia").titles("Sophia-M-Blck-Wht", "Matte").sku("Sophia-M-Blck-Wht").price(6300.0).size("57.0", "34.75", "27.25"));
		resolvers.add(new Resolver("62772PS204AG").key("Spoon").titles("A-G").sku("PureScape 204A-G", "PS204A-G").price(1880.00).size("67", "32.75", "21"));
		resolvers.add(new Resolver("627722000677").key("Spoon").titles("A-M", "67 in. L X 32.75 in. W X 21 in. H").sku("PureScape 204A-M", "PS204A-M").price(3174.00).size("67", "33.5", "21.25"));
		resolvers.add(new Resolver("627722000684").key("Spoon").titles("B-M").sku("PureScape 204B-M", "PS204B-M", "Spoon2M-Blck").price(8280.00).size("70", "38.5", "20"));
		resolvers.add(new Resolver("627722005535").key("Spoon").titles("Bronze", "Brnz").sku("Spoon2M-Brnz").price(12000.00).size("66.25", "35.25", "22.5"));
		resolvers.add(new Resolver("627722000707").key("Spoon").titles("C-M").sku("PureScape 204C-M", "PS204C-M", "Spoon2WM").price(4495.00).size("73", "32.6", "22.75"));
		resolvers.add(new Resolver("627722004378").key("Spoon").titles("Black-Wht", "Blck-Wht").sku("Spoon2M-Blck-Wht").price(5495.00).size("66.25", "35.25", "22.5"));
		resolvers.add(new Resolver("627722007935").key("Spoon").titles("Spoon-2", "Oxide", "Red-Wht", "Oxide-Red-Wht").sku("Spoon2M-Oxide-Red-Wht").price(7495.00).size("66.25", "35.25", "22.5"));
		
		resolvers.add(new Resolver("627722008581").key("Sunrise").titles("Basic", "Basic-Spa").sku("Sunrise-Basic-Spa").price(17995.00).size("98.5", "98.5", "39.25"));
		resolvers.add(new Resolver("627722005818").key("Sunrise").titles("Sunrise-Spa").sku("Sunrise-Spa").price(36000.00).size("98.5", "98.5", "39.0"));
		resolvers.add(new Resolver("627722007614").key("Sunset").titles("Spa", "Sunset").sku("Sunset-Spa").price(36000.00).size("118.0", "118.0", "41.75"));
		resolvers.add(new Resolver("627722004804").key("Suri").titles("Rlx", "Relax", "Massage", "Gloss").sku("Suri-Rlx").price(6600.0).size("66.5", "66.5", "22.25"));
		resolvers.add(new Resolver("627722004798").key("Suri").titles("Wht", "White", "Corner", "Gloss").sku("Suri-Wht").price(3950.0).size("66.5", "66.5", "22.25"));
		resolvers.add(new Resolver("627722005160").key("Suri").titles("M-Rlx", "M-Relax", "Matte").sku("Suri-M-Rlx").price(4950.0).size("66.5", "66.5", "22.25"));
		resolvers.add(new Resolver("627722005153").key("Suri").titles("M-Wht", "M-White", "Corner", "Matte").sku("Suri-M-Wht").price(2960.0).size("66.5", "66.5", "22.25"));
		resolvers.add(new Resolver("627722004248").key("Storage-Lovers-Bath").titles("Storage Lovers Bathtub").sku("Storage-Lovers-Bath").price(10200.0).size("69.75", "39.25", "21.25"));
		resolvers.add(new Resolver("627722007119").key("Tessera").titles("Tessera", "Spa").sku("Tessera-Spa").price(34990.0).size("91.0", "91.0", "30.75"));
		resolvers.add(new Resolver("627722007126").key("Tessera").titles("Tessera", "Ergo-Spa").sku("Tessera-Ergo-Spa").price(34990.0).size("91.0", "91.0", "30.75"));
		resolvers.add(new Resolver("627722005672").key("Trinity").titles("Trinity-G-Wht", "Relax", "High", "Gloss").sku("Trinity-G-Wht-Rlx").price(8950.0).size("67.75", "59.0", "24.0"));
		resolvers.add(new Resolver("627722006426").key("Trinity").titles("Trinity-M-Wht", "Relax", "Fine", "Matte").sku("Trinity-M-Wht-Rlx").price(8950.0).size("67.75", "59.0", "24.0"));
		resolvers.add(new Resolver("627722001490").key("Tulip").titles("Blck-Wht", "Black", "Surface").sku("Tulip-Blck-Wht").price(4995.0));
		resolvers.add(new Resolver("627722001483").key("Tulip").titles("Wht", "White", "Surface", "Purescape 701M").sku("Tulip-Wht", "Purescape 701M").price(3995.0).size("62.2", "28.1", "23.8"));
		resolvers.add(new Resolver("627722004767").key("Tulip").titles("Grande-Wht", "White", "Grande").sku("Tulip-Grande-Wht").price(4495.0).size("71.0", "31.75", "25.0"));
		resolvers.add(new Resolver("627722007867").key("Tulip").titles("Grande-Blck-Wht", "Blck-Wht", "Grande").sku("Tulip-Grande-Blck-Wht").price(5495.0).size("71.0", "31.75", "25.0"));
		resolvers.add(new Resolver("627722005290").key("Trinity").titles("M-Wht", "M-White", "Matte", "Trinity").sku("Trinity-M-Wht").price(4950.0).size("67.75", "59.0", "24.0"));
		resolvers.add(new Resolver("627722005214").key("Trinity").titles("Wht", "White", "Gloss", "Trinity").sku("Trinity-Wht").price(4950.0).size("67.75", "59.0", "24.0"));
		
		resolvers.add(new Resolver("627722004026").key("TrueOfuro").sku("TrueOfuro-Wht", "True-Ofuro-Wht", "True Ofuro-Wht").price(6600.0).size("46.5", "36.25", "34.65"));
		resolvers.add(new Resolver("627722005641").key("TrueOfuro").sku("TrueOfuro-Blck", "True-Ofuro-Blck", "True Ofuro-Blck").price(8999.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722004415").key("TrueOfuro").sku("TrueOfuro-Wht-Mini", "Mini-Wht", "True-Ofuro-Mini-Wht", "True Ofuro Mini-Wht").price(5990.0).size("43.25", "43.0", "37.25"));
		resolvers.add(new Resolver("627722005634").key("TrueOfuro").sku("True Ofuro Blck-Mini", "Mini-Blck", "True-Ofuro-Blck-Wht", "True Ofuro Mini-Blck").price(8999.0).size("43.0", "43.0", "37.25"));
		resolvers.add(new Resolver("627722004484").key("TrueOfuro").titles("Wht-Tranq", "Tranquility", "White").sku("True Ofuro-Wht-Tranq", "True-Ofuro-Wht-Tranq", "TrueOfuro-Wht-Tranq", "True Ofuro Wht-Tranq").price(9425.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722004491").key("TrueOfuro").titles("Tranquility", "White").sku("True Ofuro-Wht-Tranq-Int", "True-Ofuro-Wht-Tranq-Int", "TrueOfuro-Wht-Tranq-Int", "True Ofuro Wht-Tranq-Int").price(9425.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722004453").key("TrueOfuro").titles("Wht-Tranq-Int", "Tranquility", "Mini", "White").sku("True Ofuro-Mini-Tranq-Int", "True Ofuro-Mini-Wht-Tranq-Int", "True-Ofuro-Mini-Wht-Tranq-Int", "TrueOfuro-Mini-Wht-Tranq-Int", "True Ofuro Mini-Wht-Tranq-Int").price(8815.0).size("43.25", "43.0", "37.25"));
		resolvers.add(new Resolver("627722005573").key("TrueOfuro").titles("Wht-Tranq", "Tranquility", "Mini", "White").sku("True Ofuro-Mini-Wht-Tranq", "True Ofuro-Mini-Tranq", "True Ofuro Mini-Wht-Tranq", "True-Ofuro-Mini-Wht-Tranq").price(8815.0).size("43.25", "43.0", "37.25"));
		resolvers.add(new Resolver("627722007157").key("TrueOfuro").titles("Blck-Tranq", "Tranq", "Tranquility", "Black").sku("True Ofuro-Blck-Tranq", "True-Ofuro-Blck-Tranq", "TrueOfuro-Blck-Tranq", "True Ofuro Blck-Tranq").price(11825.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722007164").key("TrueOfuro").titles("Blck-Tranq-Int", "Tranq-Int", "Tranquility", "Black").sku("True Ofuro-Blck-Tranq-Int", "True-Ofuro-Blck-Tranq-Int", "TrueOfuro-Blck-Tranq-Int", "True Ofuro Blck-Tranq-Int").price(11825.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722007171").key("TrueOfuro").titles("Blck-Tranq", "Tranquility", "Mini", "Black").sku("True Ofuro-Mini-Blck-Tranq", "True-Ofuro-Mini-Blck-Tranq", "True Ofuro Mini-Blck-Tranq").price(11825.0).size("43.0", "43.0", "37.25"));
		resolvers.add(new Resolver("627722007188").key("TrueOfuro").titles("Blck-Tranq-Int", "Tranquility", "Mini", "Black").sku("True Ofuro-Mini-Blck-Tranq-Int", "True-Ofuro-Mini-Blck-Tranq-Int", "TrueOfuro-Mini-Blck-Tranq-Int", "True Ofuro Mini-Blck-Tranq-Int").price(11825.0).size("43.0", "43.0", "37.25"));
		resolvers.add(new Resolver("627722007317").key("TrueOfuro").titles("Walnut", "Ofuro-Walnut").sku("True Ofuro-Walnut").price(34000.0).size("51.5", "36.25", "33.75"));
		resolvers.add(new Resolver("627722007911").key("TrueOfuro").titles("Duo-Wht").sku("True Ofuro Duo-Wht", "True-Ofuro-Duo-Wht", "True Ofuro-Duo-Wht").price(9200.0).size("61.5", "49.25", "37.25"));
		resolvers.add(new Resolver("627722008727").key("TrueOfuro").titles("Walnut", "Ofuro-Duo-Walnut", "Duo").sku("True Ofuro-Duo-Walnut", "True-Ofuro-Duo-Walnut", "True Ofuro Duo-Walnut").price(49000.0).size("61.5", "49.25", "37.25"));
		
		resolvers.add(new Resolver("627722006266").key("Wave").titles("Wave", "Spa").sku("Wave-Spa-Pro").price(21990.0).size("92.5", "92.5", "38.5"));
		resolvers.add(new Resolver("627722006259").key("Zen").titles("Zen", "Active-Spa").sku("Zen-Active-Spa-Pro").price(25990.0).size("118.0", "92.5", "33.5"));
		resolvers.add(new Resolver("627722006006").key("Zen").titles("Zen", "Spa").sku("Zen-Spa-Pro").price(29990.0).size("110.25", "92.5", "33.5"));
		
		// -------Showers---------
		
		resolvers.add(new Resolver("627722004132").key("Anette").titles("B-R", "Anette-B-R", "Shower Cabin").sku("Anette-B-R-Shower-Wall").price(2898.0).size("", "63.0", "61.0"));
		resolvers.add(new Resolver("627722004200").key("Anette").titles("B-L", "Anette-B-L", "Shower Cabin").sku("Anette-B-L-Shower-Wall").price(2898.0).size("", "63.0", "61.0"));
		resolvers.add(new Resolver("627722005405").key("Anette").titles("C-L", "Anette-C-L", "Shower Cabin").sku("Anette-C-L-Shower-Wall").price(2898.0).size("38.25", "67.0", "61.0"));
		resolvers.add(new Resolver("627722005399").key("Anette").titles("C-R", "Anette-C-R", "Shower Cabin").sku("Anette-C-R-Shower-Wall").price(2898.0).size("38.25", "67.0", "61.0"));
		resolvers.add(new Resolver("627722004217").key("Anette").titles("A-L", "Anette-A-L", "Shower Cabin").sku("Anette-A-L-Shower-Wall").price(2898.0).size("", "59.0", "61.0"));
		resolvers.add(new Resolver("627722004224").key("Anette").titles("A-R", "Anette-A-R", "Shower Cabin").sku("Anette-A-R-Shower-Wall").price(2898.0).size("", "59.0", "61.0"));
		resolvers.add(new Resolver("627722005269").key("Bollicine").titles("Handshower", "Chrome").sku("HS-300", "Bollicine-HS-300").price(260.0).size("", "", "11.75"));
		resolvers.add(new Resolver("627722005917").key("Bollicine").titles("3-Hole", "Shower Control").sku("SC-114", "Bollicine-SC-114").price(1199.0).size("17.25", "5.25", "2.25"));
		resolvers.add(new Resolver("627722005276").key("Bollicine").titles("Bollicine-689", "Shower Control").sku("SC-689", "Bollicine-SC-689").price(899.0).size("5.0", "4.25", "7.5"));
		resolvers.add(new Resolver("627722005900").key("Bollicine").titles("Bollicine-686", "Shower Control").sku("SC-686", "Bollicine-SC-686").price(849.0).size("5.0", "4.25", "7.5"));
		resolvers.add(new Resolver("627722006815").key("Celine").titles("Celine", "SC-751").sku("Celine-SC-751").price(1299.0).size("4.75", "4.0", "7.75"));
		resolvers.add(new Resolver("627722006877").key("Celine").titles("Celine", "SC-752").sku("Celine-SC-752").price(1350.0).size("4.75", "4.0", "7.75"));
		resolvers.add(new Resolver("627722006730").key("Celine").titles("Celine", "SC-753").sku("Celine-SC-753").price(1399.0).size("4.75", "4.0", "7.75"));
		resolvers.add(new Resolver("627722006341").key("Celine").titles("Celine", "SS-456").sku("Celine-SS-456").price(1699.0).size("7.5", "13.75", "51.5"));
		
		resolvers.add(new Resolver("627722005856").key("Gamma").titles("510", "G-510").sku("Gamma-510").price(4950.00).size("4.0", "23.5", "88.5"));
		resolvers.add(new Resolver("627722005863").key("Gamma").titles("514", "G-514").sku("Gamma-514").price(3300.00).size("2.25", "19.75", "87.25"));
		resolvers.add(new Resolver("627722005870").key("Gamma").titles("515", "G-515").sku("Gamma-515").price(3200.00).size("2.25", "19.75", "87.25"));
		resolvers.add(new Resolver("627722005887").key("Gamma").titles("520", "G-520").sku("Gamma-520").price(4950.00).size("4.0", "23.5", "88.5"));
		
		resolvers.add(new Resolver("627722008185").key("Lorena").titles("SC-608", "Lorena-608", "608", "Shower").sku("Lorena-SC-608").price(2050.00).size("11.25", "4.25", "3.0"));
		resolvers.add(new Resolver("627722008192").key("Lorena").titles("SC-610", "610", "Lorena-610", "Shower").sku("Lorena-SC-610").price(2100.00).size("11.25", "4.25", "3.0"));
		resolvers.add(new Resolver("627722008161").key("Lorena").titles("SC-686", "686", "Lorena-686", "Shower").sku("Lorena-SC-686").price(900.00).size("4.75", "4.25", "7.0"));
		resolvers.add(new Resolver("627722008178").key("Lorena").titles("SC-689", "689", "Lorena-689", "Shower").sku("Lorena-SC-689").price(950.00).size("4.75", "4.25", "7.0"));
		
		resolvers.add(new Resolver("627722006990").key("Lorena").titles("SC-773", "773", "Lorena-773", "Shower").sku("Lorena-SC-773").price(3750.00).size("4.75", "3.75", "19.5"));
		resolvers.add(new Resolver("627722007096").key("Lorena").titles("SC-774", "774", "Lorena-774", "Shower").sku("Lorena-SC-774").price(4600.00).size("4.75", "3.75", "24.5"));
		
		resolvers.add(new Resolver("627722008208").key("Lorena").titles("SS-619", "619", "Lorena-SS-619", "Shower").sku("Lorena-SS-619").price(3350.00).size("7.0", "7.0", "0.0"));
		
		resolvers.add(new Resolver("627722005139").key("HSSQ-200").titles("SQ-200", "Chrome").sku("HSSQ-200").price(320.0).size("3.75", "3.5", "8.25"));
		resolvers.add(new Resolver("627722006358").key("HSRD-200").titles("RD-200", "Chrome").sku("HSRD-200").price(250.0).size("0.0", "0.0", "8.25"));
		resolvers.add(new Resolver("627722003784").key("BCRD-240").titles("240", "Sparkle").sku("BCRD-240", "BCRD").price(1700.0).size("9.45", "9.45", "1.73"));
		resolvers.add(new Resolver("627722003791").key("WCRD-240").titles("240", "Sparkle").sku("WCRD-240", "WCRD").price(1700.0).size("9.45", "9.45", "1.73"));
		resolvers.add(new Resolver("627722003807").key("BCSQ-270").titles("270", "Galaxy").sku("BCSQ-270").price(2100.0).size("10.63", "10.63", "1.73"));
		resolvers.add(new Resolver("627722003814").key("WCSQ-270").titles("270", "Galaxy").sku("WCSQ-270").price(2100.0).size("10.63", "10.63", "1.73"));
		resolvers.add(new Resolver("627722003234").key("MCRD-300").titles("DYN", "Chrome").sku("DYN-MCRD-300", "MCDR").price(2599.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003241").key("MCRD-300").titles("DYN", "BLCK", "Black").sku("DYN-BLCK-MCRD-300", "MCDR").price(3299.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003258").key("MCRD-300").titles("DYN", "WHT", "White").sku("DYN-WHT-MCRD-300", "MCDR").price(3299.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003265").key("WCRD-300").titles("DYN", "Chrome", "WCRD").sku("DYN-WCRD-300").price(1699.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003272").key("WCRD-300").titles("DYN", "BLCK", "Black", "WCRD").sku("DYN-BLCK-WCRD-300").price(2399.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003289").key("WCRD-300").titles("DYN", "WHT", "White", "WCRD").sku("DYN-WHT-WCRD-300").price(2399.0).size("11.81", "11.81", "6.3"));
		resolvers.add(new Resolver("627722003302").key("MCSQ-340").titles("340", "DYN", "Dynamo").sku("DYN-MCSQ-340", "MCSQ-340").price(2799.0).size("13.4", "13.4", "6.3"));
		resolvers.add(new Resolver("627722003319").key("WCSQ-340").titles("340", "DYN", "Dynamo").sku("DYN-WCSQ-340", "WCSQ-340").price(1899.0).size("13.4", "13.4", "6.3"));
		resolvers.add(new Resolver("627722006525").key("MCRD-380").titles("380", "Polaris").sku("Polaris-MCRD-380", "MCRD-380").price(3100.0).size("15.0", "15.0", "2.5"));
		resolvers.add(new Resolver("627722006518").key("MCSQ-380").titles("380", "Polaris").sku("Polaris-MCSQ-380", "MCSQ-380").price(3200.0).size("15.0", "15.0", "2.25"));
		resolvers.add(new Resolver("627722003777").key("MCRD-400").titles("400", "Sparkle").sku("MCRD-400", "MCRD").price(3312.0).size("15.75", "15.75", "6.0"));
		resolvers.add(new Resolver("627722003876").key("MCSQ-400").titles("400", "Galaxy").sku("MCSQ-400").price(2029.0).size("15.7", "15.7", "3.9"));
		resolvers.add(new Resolver("627722004293").key("WCRD-425").titles("425", "Galaxy").sku("Galaxy-WCRD-425", "WCRD").price(1828.5).size("16.75", "16.75", "2.75"));
		resolvers.add(new Resolver("627722003883").key("MCRD-425").titles("425", "Galaxy").sku("MCRD-425", "GALAXY-MCRD-425").price(1956.0).size("16.75", "16.75", "2.75"));
		resolvers.add(new Resolver("627722006488").key("MCSQ-500").titles("500", "Polaris").sku("Polaris-MCSQ-500", "MCSQ-500").price(4800.0).size("19.75", "19.75", "3.0"));
		resolvers.add(new Resolver("627722004316").key("MCSQ-540").titles("540", "Galaxy").sku("MCSQ-540", "Galaxy-MCSQ-540").price(3850.0).size("21.25", "21.25", "5.5"));
		resolvers.add(new Resolver("627722006563").key("MCRD-600").titles("600", "Ajour").sku("Ajour-MCRD-600", "MCRD-600").price(1700.0).size("23.5", "23.5", "2.25"));
		resolvers.add(new Resolver("627722006549").key("MCSQ").titles("530/520", "530-520", "Ajour").sku("Ajour-MCSQ-530-520", "MCSQ-530-520").price(1700.0).size("20.75", "20.5", "2.25"));
		resolvers.add(new Resolver("627722003869").key("MCRC").titles("550/400", "540-400", "Aurora").sku("MCRC-550/400").price(2608.0).size("21.7", "15.7", "3.9"));
		resolvers.add(new Resolver("627722006532").key("MCRC").titles("590/270", "590-270", "Spring").sku("Spring-MCRC-590-270", "MCRC-590/270", "Spring-MCRC-590/270").price(2300.0).size("23.25", "10.75", "3.0"));
		resolvers.add(new Resolver("627722006495").key("MCRC").titles("700/380-A", "700-380-A", "Polaris").sku("Polaris-MCRC-700-380-A", "MCRC-700-380-A").price(5800.0).size("27.5", "15.75", "4.75"));
		resolvers.add(new Resolver("627722006501").key("MCRC").titles("700/380-B", "700-380-B", "Polaris").sku("Polaris-MCRC-700-380-B", "MCRC-700-380-B").price(4990.0).size("27.5", "15.75", "4.75"));
		resolvers.add(new Resolver("627722004309").key("WCRC").titles("700/400", "700-400", "Galaxy").sku("Galaxy-WCRC-700/400", "WCRC-700/400").price(3199.0).size("27.5", "15.75", "4.75"));
		resolvers.add(new Resolver("627722003890").key("MCRC").titles("700/400", "700-400", "Bluetooth", "Music").sku("MCRC-700/400").price(3333.0).size("27.6", "15.7", "4.7"));
		resolvers.add(new Resolver("627722006556").key("MCSQ").titles("710/690", "710-690", "Ajour").sku("Ajour-MCSQ-710-690", "MCSQ-710-690").price(2300.0).size("28.0", "27.25", "2.25"));
		resolvers.add(new Resolver("627722006570").key("MCRD-770").titles("770", "Ajour").sku("Ajour-MCRD-770", "MCRD-770").price(2300.0).size("30.5", "30.5", "2.25"));
		resolvers.add(new Resolver("627722003852").key("MCRC").titles("850/540", "850-540", "Infinity").sku("MCRC-850/540").price(4990.0).size("33.5", "21.3", "5.5"));
		
		resolvers.add(new Resolver("627722003135").key("Elise").titles("Show", "Shower", "Blck", "Black", "panel").sku("Elise-Show-Blck").price(2753.0).size("", "9.5", "58.75"));
		resolvers.add(new Resolver("627722003142").key("Elise").titles("Show", "Shower", "Red", "panel").sku("Elise-Show-Red").price(2753.1).size("", "9.5", "58.75"));
		resolvers.add(new Resolver("627722002121").key("Elise").titles("Show", "Shower", "Wht", "White", "panel").sku("Elise-Show-Wht").price(1799.0).size("", "9.5", "58.75"));
		resolvers.add(new Resolver("627722003586").key("Elise").titles("Wht", "Vessel").sku("Elise-Sink-Wht", "sink").price(683.0).size("23.6", "15.75", "5.9"));
		resolvers.add(new Resolver("627722004163").key("Idea").titles("Idea-L-Shower-Wall", "Idea-L", "Shower", "Wall").sku("Idea-L-Shower-Wall").price(1932.0).size("", "59.0", "57.5"));
		resolvers.add(new Resolver("627722004170").key("Idea").titles("Idea-R-Shower-Wall", "Idea-R", "Shower", "Wall").sku("Idea-R-Shower-Wall").price(1932.0).size("", "59.0", "57.5"));
		resolvers.add(new Resolver("627722006297").key("Multiplex").titles("SC-Duo", "Shower", "Black", "Blck").sku("Multiplex-SC-Duo").price(1600.0).size("3.25", "0.75", "6.25"));
		resolvers.add(new Resolver("627722005122").key("Multiplex").titles("SC-Trio", "Shower", "Black", "Blck").sku("Multiplex-SC-Trio-Blck").price(2200.0).size("3.25", "2.25", "7.25"));
		resolvers.add(new Resolver("627722005115").key("Multiplex").titles("SC-Trio", "Shower", "White", "Wht").sku("Multiplex-SC-Trio-Wht").price(2200.0).size("3.25", "2.25", "7.25"));
		
		resolvers.add(new Resolver("627722005016").key("310").titles("Spring", "Spring-SQ", "Large").sku("PD310", "Shower Arm").price(220.0).size("15.75", "1.75", "3.75"));
		resolvers.add(new Resolver("627722005023").key("410").titles("Spring", "Spring-SQ", "Small").sku("PD410", "Shower Arm").price(190.0).size("12.75", "1.75", "3.75"));
		resolvers.add(new Resolver("627722005030").key("411").titles("Spring", "Spring-RD", "Small").sku("PD411", "Shower Arm").price(120.0).size("13.75", "2.0", "2.75"));
		resolvers.add(new Resolver("627722005047").key("414").titles("Spring", "Spring-RD", "Large").sku("PD414", "Shower Arm").price(160.0).size("19.75", "2.0", "2.75"));
		resolvers.add(new Resolver("627722005054").key("422").titles("Spring", "Spring-SQ", "Small").sku("PD422", "Shower Arm").price(110.0).size("", "1.75", "7.75"));
		resolvers.add(new Resolver("627722005061").key("423").titles("Spring", "Spring-SQ", "Large").sku("PD423", "Shower Arm").price(150.0).size("", "1.75", "11.75"));
		resolvers.add(new Resolver("627722005078").key("425").titles("Spring", "Spring-RD", "Small").sku("PD425", "Shower Arm").price(110.0).size("", "2.0", "7.75"));
		resolvers.add(new Resolver("627722005085").key("426").titles("Spring", "Spring-RD", "Large").sku("PD426", "Shower Arm").price(150.0).size("", "2.0", "11.75"));
		resolvers.add(new Resolver("627722003340").key("427").titles("WM", "Wall", "Mounted", "Short", "White").sku("PD427WM", "Shower Arm").price(349.0).size("14.17", "", ""));
		resolvers.add(new Resolver("627722003326").key("427").titles("CP", "Wall", "Mounted", "Short", "Chrome").sku("PD427CP", "Shower Arm").price(349.0).size("14.17", "", ""));
		resolvers.add(new Resolver("627722003333").key("427").titles("BM", "Wall", "Mounted", "Short", "Black").sku("PD427BM", "Shower Arm").price(349.0).size("14.17", "", ""));
		resolvers.add(new Resolver("627722003371").key("917").titles("WM", "Wall", "Mounted", "High", "White").sku("PD917WM", "Shower Arm").price(299.0).size("15.75", "", ""));
		resolvers.add(new Resolver("627722003357").key("917").titles("CP", "Wall", "Mounted", "High", "Chrome").sku("PD917CP", "Shower Arm").price(299.0).size("15.75", "", ""));
		resolvers.add(new Resolver("627722003364").key("917").titles("BM", "Wall", "Mounted", "High", "Black").sku("PD917BM", "Shower Arm").price(299.0).size("15.75", "", ""));
		resolvers.add(new Resolver("627722003401").key("926").titles("WM", "Ceiling", "Small", "White").sku("PD926WM", "Shower Arm").price(249.0).size("", "", "5.9"));
		resolvers.add(new Resolver("627722003388").key("926").titles("CP", "Ceiling", "Small", "Chrome").sku("PD926CP", "Shower Arm").price(249.0).size("", "", "5.9"));
		resolvers.add(new Resolver("627722003395").key("926").titles("BM", "Ceiling", "Small", "Black").sku("PD926BM", "Shower Arm").price(249.0).size("", "", "5.9"));
		resolvers.add(new Resolver("627722003432").key("927").titles("WM", "Ceiling", "Medium", "White").sku("PD927WM", "Shower Arm").price(249.0).size("", "", "9.84"));
		resolvers.add(new Resolver("627722003418").key("927").titles("CP", "Ceiling", "Medium", "Chrome").sku("PD927CP", "Shower Arm").price(249.0).size("", "", "9.84"));
		resolvers.add(new Resolver("627722003425").key("927").titles("BM", "Ceiling", "Medium", "Black").sku("PD927BM", "Shower Arm").price(249.0).size("", "", "9.84"));
		resolvers.add(new Resolver("627722003463").key("929").titles("WM", "Ceiling", "Large", "White", "ceiling-mounted-large-shower-arm-chrome").sku("PD929WM", "Shower Arm").price(249.0).size("", "", "13.78"));
		resolvers.add(new Resolver("627722003449").key("929").titles("CP", "Ceiling", "Large", "Chrome").sku("PD929CP", "Shower Arm").price(249.0).size("", "", "13.78"));
		resolvers.add(new Resolver("627722003456").key("929").titles("BM", "Ceiling", "Large", "Black").sku("PD929BM", "Shower Arm").price(249.0).size("", "", "13.78"));
		
		resolvers.add(new Resolver("6277220SC774").key("Vertex").titles("Vertex", "SC-774").sku("Vertex-SC-774").price(4600.0).size("4.75", "3.75", "24.5"));
		resolvers.add(new Resolver("6277220SC773").key("Vertex").titles("Vertex", "SC-773").sku("Vertex-SC-773").price(3750.0).size("4.75", "3.75", "19.5"));
		resolvers.add(new Resolver("627722006846").key("Retro").titles("Retro-2", "SC-751").sku("Retro-2-SC-751").price(1399.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006907").key("Retro").titles("Retro-2", "SC-752").sku("Retro-2-SC-752").price(1450.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006952").key("Retro").titles("Retro-2", "SC-753").sku("Retro-2-SC-753").price(1499.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722007041").key("Retro").titles("Retro-2", "SC-783").sku("Retro-2-SC-783").price(2550.0).size("4.75", "3.5", "20.0"));
		resolvers.add(new Resolver("627722007089").key("Retro").titles("Retro-2", "SC-784").sku("Retro-2-SC-784").price(3150.0).size("4.75", "3.5", "24.75"));
		resolvers.add(new Resolver("627722006839").key("Retro").titles("Retro", "SC-751").sku("Retro-SC-751").price(1450.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006891").key("Retro").titles("Retro", "SC-752").sku("Retro-SC-752").price(1499.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006945").key("Retro").titles("Retro", "SC-753").sku("Retro-SC-753").price(1550.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722007034").key("Retro").titles("Retro", "SC-783").sku("Retro-SC-783").price(2650.0).size("4.75", "3.5", "20.0"));
		resolvers.add(new Resolver("627722007072").key("Retro").titles("Retro", "SC-784").sku("Retro-SC-784").price(3250.0).size("4.75", "3.5", "24.75"));
		resolvers.add(new Resolver("627722006808").key("SCRD").titles("SCRD", "RD-751").sku("SCRD-751").price(1199.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006860").key("SCRD").titles("SCRD", "RD-752").sku("SCRD-752").price(1250.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006921").key("SCRD").titles("SCRD", "RD-753").sku("SCRD-753").price(1299.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722007331").key("SCRD").titles("SCRD", "RD-782").sku("SCRD-782").price(1850.0).size("4.75", "3.5", "15.0"));
		resolvers.add(new Resolver("627722007010").key("SCRD").titles("SCRD", "RD-783").sku("SCRD-783").price(2300.0).size("4.75", "3.5", "20.0"));
		resolvers.add(new Resolver("627722007058").key("SCRD").titles("SCRD", "RD-784").sku("SCRD-784").price(2750.0).size("4.75", "3.5", "24.75"));
		resolvers.add(new Resolver("627722006822").key("SCSQ").titles("SCSQ", "SQ-751").sku("SCSQ-751").price(1399.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006884").key("SCSQ").titles("SCSQ", "SQ-752").sku("SCSQ-752").price(1450.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006938").key("SCSQ").titles("SCSQ", "SQ-753").sku("SCSQ-753").price(1499.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722007348").key("SCSQ").titles("SCSQ", "SQ-782").sku("SCSQ-782").price(2100.0).size("4.75", "3.5", "15.0"));
		resolvers.add(new Resolver("627722007027").key("SCSQ").titles("SCSQ", "SQ-783").sku("SCSQ-783").price(2550.0).size("4.75", "3.5", "20.0"));
		resolvers.add(new Resolver("627722007065").key("SCSQ").titles("SCSQ", "SQ-784").sku("SCSQ-784").price(3000.0).size("4.75", "3.5", "24.75"));
		
		resolvers.add(new Resolver("634392345656").key("Spring").titles("300", "140", "US", "Wall", "Mounted").sku("Spring-RC-300/140-US").price(705.0).size("5.5", "9.25", "2.25"));
		resolvers.add(new Resolver("634392345649").key("Spring").titles("235", "140", "US", "Wall", "Mounted").sku("Spring-RC-235/140-US").price(775.0).size("11.75", "3.25", "5.75"));
		
		resolvers.add(new Resolver("716894070769").key("Spring").titles("RD-380", "US").sku("Spring-RD-380-US").price(1160.0).size("15.0", "15.0", "2.5"));
		resolvers.add(new Resolver("716894070745").key("Spring").titles("SQ-300", "US").sku("Spring-SQ-300-US").price(900.0).size("15.0", "15.0", "2.25"));
		resolvers.add(new Resolver("716894070721").key("Spring").titles("SQ-380-A", "US").sku("Spring-SQ-380-A-US").price(1260.0).size("15.0", "15.0", "2.25"));
		resolvers.add(new Resolver("716894070707").key("Spring").titles("SQ-380-B", "US").sku("Spring-SQ-380-B-US").price(1240.0).size("15.0", "15.0", "3.25"));
		resolvers.add(new Resolver("716894070684").key("Spring").titles("SQ-500-A", "US").sku("Spring-SQ-500-A-US").price(2010.0).size("19.75", "19.75", "3.75"));
		resolvers.add(new Resolver("716894070660").key("Spring").titles("SQ-500-B", "US").sku("Spring-SQ-500-B-US").price(1710.0).size("19.75", "19.75", "2.75"));
		
		resolvers.add(new Resolver("627722004590").key("Spring").titles("300", "140", "Wall", "Mounted").sku("Spring-RC-300/140").price(1100.0).size("11.75", "5.5", "2.75"));
		resolvers.add(new Resolver("627722004606").key("Spring").titles("235", "140", "Wall", "Mounted").sku("Spring-RC-235/140").price(1100.0).size("9.25", "5.5", "2.25"));
		resolvers.add(new Resolver("627722004668").key("Spring").titles("450", "200", "Wall", "Mounted").sku("Spring-RC-450/200").price(1599.0).size("17.75", "7.75", "3.5"));
		resolvers.add(new Resolver("627722004712").key("Spring").titles("550/320-A", "Wall", "Mounted").sku("Spring-RC-550/320-A").price(1799.0).size("21.75", "12.5", "1.25"));
		resolvers.add(new Resolver("627722004729").key("Spring").titles("550/320-B", "Wall", "Mounted").sku("Spring-RC-550/320-B").price(1799.0).size("21.75", "12.5", "1.25"));
		resolvers.add(new Resolver("627722004743").key("Spring").titles("590/310-A", "Wall", "Mounted").sku("Spring-RC-590/310-A").price(1999.0).size("23.25", "12.25", "0.75"));
		resolvers.add(new Resolver("627722006723").key("Spring").titles("590/310-A-SS", "Wall", "Mounted").sku("Spring-RC-590/310-A-SS").price(2249.0).size("11.75", "11.75", "6.25"));
		resolvers.add(new Resolver("627722004750").key("Spring").titles("590/310-B", "Wall", "Mounted").sku("Spring-RC-590/310-B").price(1999.0).size("23.25", "12.25", "0.75"));
		resolvers.add(new Resolver("627722004613").key("Spring").titles("300", "Top", "Mounted").sku("Spring-RD-300").price(699.0).size("11.75", "11.75", "2.5"));
		resolvers.add(new Resolver("627722005580").key("Spring").titles("300", "Top", "Mounted", "Retro").sku("Spring-RD-300-Retro").price(1299.0).size("11.75", "11.75", "5.5"));
		resolvers.add(new Resolver("627722004620").key("Spring").titles("400", "Top", "Mounted").sku("Spring-RD-400").price(950.0).size("15.75", "15.75", "2.5"));
		resolvers.add(new Resolver("627722000240").key("Spring").titles("500", "Top", "Mounted").sku("Spring-RD-500").price(1499.0).size("19.75", "19.75", "2.5"));
		resolvers.add(new Resolver("627722004637").key("Spring").titles("600", "Top", "Mounted").sku("Spring-RD-600").price(1799.0).size("23.5", "23.5", "2.5"));
		resolvers.add(new Resolver("627722000660").key("Spring").titles("600", "Top", "Mounted").sku("Spring-SQ-600").price(1880.0).size("23.5", "23.5", "2.5"));
		resolvers.add(new Resolver("627722004644").key("Spring").titles("400", "Top", "Mounted").sku("Spring-SQ-400").price(1199.0).size("15.75", "15.75", "2.5"));
		resolvers.add(new Resolver("627722000578").key("Spring").titles("300", "Top", "Mounted").sku("Spring-SQ-300").price(799.0).size("11.75", "11.75", "2.5"));
		resolvers.add(new Resolver("627722004675").key("Spring").titles("250", "Top", "Mounted").sku("Spring-SQ-250").price(699.0).size("9.75", "9.75", "1.5"));
		resolvers.add(new Resolver("627722007201").key("Spring").titles("250-SS", "SQ-250-SS").sku("Spring-SQ-250-SS").price(2229.0).size("9.75", "9.75", "1.5"));
		resolvers.add(new Resolver("627722000752").key("Spring").titles("RC-300/400", "Top", "Mounted").sku("Spring RC-300/400").price(1150.0).size("15.75", "11.75", "2.5"));
		resolvers.add(new Resolver("627722000820").key("Spring").titles("400", "500", "Top", "Mounted").sku("Spring-RC-400/500").price(1699.0).size("15.75", "11.75", "2.5"));
		resolvers.add(new Resolver("627722000769").key("Spring").titles("OV-300/400", "Top", "Mounted").sku("Spring-OV-300/400").price(1150.0).size("19.75", "15.75", "2.5"));
		resolvers.add(new Resolver("627722004682").key("Spring").titles("340", "Built-In").sku("Spring-SQ-340").price(999.0).size("13.5", "13.5", "2.25"));
		resolvers.add(new Resolver("627722006716").key("Spring").titles("340-SS", "SQ-340").sku("Spring-SQ-340-SS").price(2349.0).size("11.75", "11.75", "6.25"));
		resolvers.add(new Resolver("627722004736").key("Spring").titles("380", "Built-In").sku("Spring-SQ-380").price(1599.0).size("15.0", "15.0", "2.5"));
		resolvers.add(new Resolver("627722004699").key("Spring").titles("SQ-500-A", "Built-In").sku("Spring-SQ-500-A").price(2279.0).size("19.75", "19.75", "3.75"));
		resolvers.add(new Resolver("627722004705").key("Spring").titles("SQ-500-B", "Built-In").sku("Spring-SQ-500-B").price(1999.0).size("19.75", "19.75", "2.75"));
		resolvers.add(new Resolver("627722004651").key("Spring").titles("SQ-500-C", "Top", "Mounted").sku("Spring-SQ-500-C").price(1699.0).size("19.75", "19.75", "2.5"));
		
		resolvers.add(new Resolver("627722006853").key("Techno").titles("Techno", "SC-751").sku("Techno-SC-751").price(1050.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006914").key("Techno").titles("Techno", "SC-752").sku("Techno-SC-752").price(1099.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006969").key("Techno").titles("Techno", "SC-753").sku("Techno-SC-753").price(1150.0).size("4.75", "3.5", "7.75"));
		resolvers.add(new Resolver("627722006778").key("Wave").titles("RC-250-80-A", "RC-250/80-A").sku("Wave-RC-250/80-A").price(1100.0).size("9.75", "4.0", "3.25"));
		resolvers.add(new Resolver("627722006785").key("Wave").titles("RC-250-80-B", "RC-250/80-B").sku("Wave-RC-250/80-B").price(1100.0).size("9.75", "4.0", "3.25"));
		
		// --------Sinks---------
		
		resolvers.add(new Resolver("627722007492").key("Abisco").titles("Abisco-Sink", "Stone").sku("Abisco-Sink-Wht").price(3499.0).size("55.0", "18.75", "28.5"));
		resolvers.add(new Resolver("627722007522").key("Albi").titles("Albi", "Baby").sku("Albi-Baby-Sink-Wht").price(1300.0).size("70.75", "21.25", "11.75"));
		resolvers.add(new Resolver("627722007546").key("Albi").titles("Albi", "Double").sku("Albi-Double-Sink-Wht").price(1200.0).size("63.0", "19.25", "8.75"));
		resolvers.add(new Resolver("627722007539").key("Albi").titles("Albi", "L-Sink").sku("Albi-L-Sink-Wht").price(799.0).size("31.5", "19.25", "8.75"));
		resolvers.add(new Resolver("627722003579").key("Arab-Sink").titles("Arabella", "Vessel").sku("Arab-Sink-Wht", "sink").price(683.0).size("21.65", "15.75", "5.9"));
		resolvers.add(new Resolver("627722004361").key("Arab-Sink").titles("Arabella", "Black-Wht", "Vessel", "Arab-Sink-Blck-Wht").sku("Arab-Sink-Blck-Wht", "sink").price(888.0).size("21.75", "15.75", "6.0"));
		resolvers.add(new Resolver("627722005771").key("Axiom").titles("Axiom", "Sink-Wht", "Stone").sku("Axiom-Sink-Wht").price(850.0).size("51.5", "15.75", "5.0"));
		resolvers.add(new Resolver("627722003494").key("Bravado").titles("Bravado-A").sku("Bravado-A-Wht", "Lavatory").price(699.0).size("27.5", "19.0", "4.1"));
		resolvers.add(new Resolver("627722003500").key("Bravado").titles("Bravado-B").sku("Bravado-B-Wht", "Lavatory").price(839.0).size("39.4", "19.3", "4.0"));
		resolvers.add(new Resolver("627722003517").key("Bravado").titles("Bravado-C", "C-R").sku("Bravado-C-R-Wht", "Lavatory").price(959.0).size("59.0", "19.3", "4.0"));
		resolvers.add(new Resolver("627722007591").key("Carre").titles("Wht", "Flexi", "Sink").sku("Carre-Flexi-Sink-Wht").price(2300.0).size("110.25", "21.25", "3.25"));
		resolvers.add(new Resolver("627722007607").key("Carre").titles("Wht", "Carre", "Sink").sku("Carre-Sink-Wht").price(995.0).size("27.5", "20.5", "5.0"));
		resolvers.add(new Resolver("627722005788").key("Chantilly").titles("Wht", "Chantilly-L", "Sink").sku("Chantilly-L-Sink-Wht").price(750.0).size("39.25", "21.25", "5.25"));
		
		resolvers.add(new Resolver("627722005696").key("Coletta").titles("Wht", "White", "Sink").sku("Coletta-A-Sink-Wht").price(683.0).size("23.75", "16.5", "4.0"));
		resolvers.add(new Resolver("627722005702").key("Coletta").titles("Wht", "White", "Sink").sku("Coletta-B-Sink-Wht").price(683.0).size("29.5", "16.5", "4.0"));
		resolvers.add(new Resolver("627722005979").key("Coletta").titles("Blck", "Black", "Sink").sku("Coletta-A-Sink-Blck").price(1199.0).size("23.75", "16.5", "4.0"));
		resolvers.add(new Resolver("627722006433").key("Coletta").titles("Blck", "Black", "Sink").sku("Coletta-B-Sink-Blck").price(1199.0).size("29.5", "16.5", "4.0"));
		resolvers.add(new Resolver("627722007942").key("Coletta").titles("Blue", "Jaffa", "Coletta-A").sku("Coletta-A-Sink-Jaffa-Blue-Wht").price(1083.0).size("23.75", "16.5", "4.25"));
		resolvers.add(new Resolver("627722007997").key("Coletta").titles("Red", "Oxide", "Coletta-A").sku("Coletta-A-Sink-Oxide-Red-Wht").price(1083.0).size("23.75", "16.5", "4.25"));
		resolvers.add(new Resolver("627722007959").key("Coletta").titles("Blue", "Jaffa", "Blue-Wht", "Coletta-B").sku("Coletta-B-Sink-Jaffa-Blue-Wht").price(1083.0).size("29.5", "16.5", "4.25"));
		resolvers.add(new Resolver("627722008000").key("Coletta").titles("Red", "Oxide", "Red-Wht", "Coletta-B").sku("Coletta-B-Sink-Oxide-Red-Wht").price(1083.0).size("29.5", "16.5", "4.25"));
		
		resolvers.add(new Resolver("627722007553").key("Cuadrato").titles("Cuadrato", "Vessel", "Sink").sku("Cuadrato-Sink-Wht").price(599.0).size("18.25", "18.0", "5.5"));
		resolvers.add(new Resolver("627722004019").key("Crystal").sku("Crystal-F-Wht", "Washbasin").price(3130.0).size("17.7", "33.85", "33.85"));
		resolvers.add(new Resolver("627722005719").key("Dante").sku("Dante-F-Wht", "White").sku("Dante-F-Sink-Wht").price(2499.0).size("20.5", "22.75", "35.5"));
		resolvers.add(new Resolver("627722007003").key("Dante").sku("Dante-Blck", "Black", "Dante-F-Blck").sku("Dante-F-Sink-Blck").price(4800.0).size("20.5", "22.75", "35.5"));
		resolvers.add(new Resolver("627722003999").key("Formosa").titles("Formosa-Rct-F-Wht").sku("Formosa-Rct-F-Wht", "Washbasin").price(1099.0).size("", "25.6", "34.5"));
		resolvers.add(new Resolver("627722003982").key("Formosa").titles("Formosa-R-F-Wht").sku("Formosa-R-F-Wht", "Washbasin").price(1069.0).size("", "19.7", "34.5"));
		resolvers.add(new Resolver("627722007645").key("Invictus").titles("Wht", "L-Sink").sku("Invictus-L-Sink-Wht").price(599.0).size("30.75", "15.0", "5.5"));
		resolvers.add(new Resolver("627722007652").key("Invictus").titles("Wht", "M-L-Sink").sku("Invictus-M-L-Sink-Wht").price(1499.0).size("78.75", "18.0", "4.75"));
		resolvers.add(new Resolver("627722005757").key("Kandi").titles("Cube", "Cube-Sink").sku("Kandi-Cube-Sink-Wht").price(499.0).size("23.5", "19.25", "6.75"));
		resolvers.add(new Resolver("627722005726").key("Kandi").titles("Flexi", "Flexi-Sink").sku("Kandi-Flexi-Sink-Wht").price(1100.0).size("79.25", "19.25", "0.75"));
		resolvers.add(new Resolver("627722005733").key("Kandi").titles("Drop-in", "Kandi-Sink").sku("Kandi-Sink-Wht").price(499.0).size("23.5", "19.25", "0.75"));
		
		resolvers.add(new Resolver("627722003616").key("Lantana").titles("Lantana-A").sku("Lantana-A-Wht", "Lavatory").price(2252.0).size("53.15", "17.7", "3.15"));
		resolvers.add(new Resolver("627722003623").key("Lantana").titles("Lantana-C").sku("Lantana-C-Wht", "Lavatory").price(2339.0).size("53.15", "17.7", "5.9"));
		resolvers.add(new Resolver("627722005993").key("Lotus").titles("Lotus-Blck", "Blck", "Black", "Sink").sku("Lotus-Sink-Blck").price(1199.0).size("23.25", "15.75", "6.25"));
		resolvers.add(new Resolver("627722005764").key("Lotus").titles("Lotus-Wht", "Wht", "White", "Sink").sku("Lotus-Sink-Wht").price(683.0).size("23.25", "15.75", "6.25"));
		resolvers.add(new Resolver("627722003708").key("Luna").titles("Luna-Blck-Lav", "Black", "Black Matte").sku("Luna-Blck-Lav", "Lavatory").price(1199.0).size("23.4", "14.6", "7.7"));
		resolvers.add(new Resolver("627722003692").key("Luna").titles("Luna-G-Wht-Lav", "Glossy").sku("Luna-G-Wht-Lav", "Lavatory").price(650.0).size("23.4", "14.6", "7.7"));
		resolvers.add(new Resolver("627722003685").key("Luna").titles("Luna-M-Wht-Lav", "Matte").sku("Luna-M-Wht-Lav", "Lavatory").price(759.0).size("23.4", "14.6", "7.7"));
		resolvers.add(new Resolver("627722008345").key("Luna").titles("Luna-Grey-Brown-Wht-Lav", "Grey", "Brown-Wht").sku("Luna-Grey-Brown-Wht-Lav", "Lavatory").price(1159.0).size("23.5", "14.5", "7.75"));
		resolvers.add(new Resolver("627722007669").key("Lunel").titles("Sink-Wht", "Lunel-Sink").sku("Lunel-Sink-Wht").price(1200.0).size("47.25", "22.5", "6.5"));
		resolvers.add(new Resolver("627722007683").key("Lyon").titles("Sink", "Double-Sink", "Double-Sink-Wht").sku("Lyon-Double-Sink-Wht").price(1400.0).size("70.75", "20.75", "6.5"));
		resolvers.add(new Resolver("627722007676").key("Lyon").titles("Sink", "Sink-Wht").sku("Lyon-Sink-Wht").price(799.0).size("23.5", "20.75", "6.5"));
		
		resolvers.add(new Resolver("627722007577").key("Maria").titles("Sink", "Double-Sink", "Double-Sink-Wht").sku("Maria-Double-Sink-Wht").price(1200.0).size("59.0", "19.0", "4.5"));
		resolvers.add(new Resolver("627722007560").key("Maria").titles("Sink", "Sink-Wht").sku("Maria-Sink-Wht").price(799.0).size("35.5", "19.0", "4.5"));
		resolvers.add(new Resolver("627722003722").key("Metamorfosi").titles("O-Blck-Wht", "Black-Wht", "Black-White", "Oval").sku("Metamorfosi-O-Blck-Wht", "sink").price(689.0).size("21.65", "16.55", "5.5"));
		resolvers.add(new Resolver("627722003760").key("Metamorfosi").titles("R-Blck-Wht", "Black-Wht", "Black-White", "Round").sku("Metamorfosi-R-Blck-Wht", "sink").price(689.0).size("21.65", "17.75", "5.5"));
		resolvers.add(new Resolver("627722003845").key("Metamorfosi").titles("Shapel-Blck-Wht", "Black-Wht", "Black-White", "Shapeless", "Black").sku("Metamorfosi-Shapel-Blck-Wht", "sink").price(689.0).size("21.65", "16.5", "5.5"));
		resolvers.add(new Resolver("627722003715").key("Metamorfosi").titles("O-Wht", "Oval").sku("Metamorfosi-O-Wht", "sink").price(482.0).size("21.65", "16.55", "5.5"));
		resolvers.add(new Resolver("627722003753").key("Metamorfosi").titles("R-Wht", "Round").sku("Metamorfosi-R-Wht", "sink").price(482.0).size("17.75", "17.75", "5.5"));
		resolvers.add(new Resolver("627722003906").key("Metamorfosi").titles("Shapel-Wht", "Shapeless").sku("Metamorfosi-Wht Shapeless", "Metamorfosi-Shapel-Wht", "sink").price(482.0).size("21.65", "16.5", "5.5"));
		resolvers.add(new Resolver("627722007706").key("Metz").titles("Metz", "Sink", "Wht").sku("Metz-Sink-Wht").price(999.0).size("55.75", "27.25", "4.0"));
		resolvers.add(new Resolver("627722007690").key("Nancy").titles("Nancy", "Wht").sku("Nancy-Sink-Wht").price(699.0).size("38.5", "20.0", "4.75"));
		resolvers.add(new Resolver("627722003548").key("Nosta").titles("Nosta-Blck-Sink-Legs", "Nosta-Blck", "Nostalgia-Black", "Black").sku("Nosta-Blck-Sink-Legs", "Lavatory").price(2187.0).size("47.25", "21.85", "34.5"));
		resolvers.add(new Resolver("627722003531").key("Nosta").titles("Nosta-Wht-Sink-Legs", "Nosta-Wht", "Nostalgia-Wht", "White").sku("Nosta-Wht-Sink-Legs", "Lavatory").price(1782.0).size("47.25", "21.85", "34.5"));
		resolvers.add(new Resolver("627722007720").key("Nox").titles("Nox", "Wht").sku("Nox-Sink-Wht").price(699.0).size("37.5", "20.0", "5.0"));
		
		resolvers.add(new Resolver("627722007737").key("Ocean").titles("F-Sink", "Ocean-F").sku("Ocean-F-Sink-Wht").price(799.0).size("23.5", "15.25", "2.75"));
		resolvers.add(new Resolver("627722007799").key("Ocean").titles("Ocean-Sink").sku("Ocean-Sink-Wht").price(599.0).size("23.5", "15.25", "0.75"));
		resolvers.add(new Resolver("627722007584").key("Origami").titles("Origami Sink", "Wht").sku("Origami-Sink-Wht").price(999.0).size("23.5", "17.0", "5.75"));
		resolvers.add(new Resolver("627722007836").key("OVO").titles("Bowl", "Drop-in").sku("OVO-Bowl-A-Sink-Wht").price(550.0).size("12.0", "15.75", "5.0"));
		resolvers.add(new Resolver("627722007829").key("OVO").titles("Countertop", "OVO-Countertop", "Vessel").sku("OVO-Countertop-Sink-Wht").price(899.0).size("14.25", "18.5", "14.25"));
		resolvers.add(new Resolver("627722007751").key("OVO").titles("Pillar", "AC", "Wall").sku("OVO-Pillar-AC-Sink-Wht").price(799.0).size("15.25", "15.25", "11.75"));
		resolvers.add(new Resolver("627722007812").key("OVO").titles("Pillar", "Pillar-F", "Freestanding").sku("OVO-Pillar-F-Sink-Wht").price(1499.0).size("16.25", "20.75", "32.0"));
		resolvers.add(new Resolver("627722007805").key("OVO").titles("Worktop", "OVO-Worktop").sku("OVO-Worktop-Sink-Wht").price(799.0).size("31.5", "19.25", "6.25"));
		resolvers.add(new Resolver("627722007713").key("Publica").titles("Publica", "Wht", "Sink").sku("Publica-Sink-Wht").price(599.0).size("27.5", "19.25", "6.25"));
		resolvers.add(new Resolver("627722007744").key("Rene").titles("Rene", "Wht", "Sink").sku("Rene-Sink-Wht").price(999.0).size("55.0", "19.75", "8.75"));
		
		resolvers.add(new Resolver("627722003593").key("Sens-Sink").titles("Sensuality", "Vessel", "White").sku("Sens-Sink-Wht", "sink").price(683.0).size("22.85", "15.75", "5.9"));
		resolvers.add(new Resolver("627722005313").key("Sens-Sink").titles("Sensuality", "Vessel", "Blck-Wht").sku("Sens-Sink-Blck-Wht", "sink").price(888.0).size("22.75", "15.75", "6.0"));
		resolvers.add(new Resolver("627722007768").key("Sibylla").titles("Sibylla M", "Sibylla-M-Sink").sku("Sibylla-M-Sink-Wht").price(1099.0).size("55.0", "19.75", "9.5"));
		resolvers.add(new Resolver("627722007775").key("Sibylla").titles("Public", "Public-A").sku("Sibylla-Public-A-Sink-Wht").price(1299.0).size("47.25", "23.5", "6.0"));
		resolvers.add(new Resolver("627722007782").key("Sibylla").titles("Sibylla-Sink").sku("Sibylla-Sink-Wht").price(750.0).size("47.25", "23.5", "1.25"));
		resolvers.add(new Resolver("627722006419").key("Solo").titles("Solo-F-Blck", "Blck", "Black", "Sink").sku("Solo-F-Sink-Blck").price(4800.0).size("20.75", "20.75", "35.75"));
		resolvers.add(new Resolver("627722005740").key("Solo").titles("Solo-F-Wht", "Wht", "White", "Sink").sku("Solo-F-Sink-Wht").price(2499.0).size("20.75", "20.75", "35.75"));
		resolvers.add(new Resolver("627722003746").key("Sophia").titles("Sophia-C-Wht", "Stone").sku("Sophia-C-Wht", "Lavatory").price(2655.0).size("49.2", "19.7", "6.0"));
		
		resolvers.add(new Resolver("627722007508").key("Split").titles("A-L", "Split-A").sku("Split-A-L-Sink-Wht").price(799.0).size("15.75", "9.75", "8.0"));
		resolvers.add(new Resolver("627722007515").key("Split").titles("B-L", "Split-B").sku("Split-B-L-Sink-Wht").price(1200.0).size("39.25", "15.75", "8.0"));
		resolvers.add(new Resolver("627722005689").key("Spoon").titles("Spoon2-Sink", "Sink-Wht", "White").sku("Spoon2-Sink-Wht", "sink").price(683.0).size("24.25", "15.75", "5.5"));
		resolvers.add(new Resolver("627722006662").key("Spoon").titles("Spoon2-Sink", "Sink-Blck", "Black").sku("Spoon2-Sink-Blck", "sink").price(1199.0).size("24.25", "15.75", "5.5"));
		resolvers.add(new Resolver("627722005986").key("Spoon").titles("Spoon2-Sink", "Sink-Brnz", "Bronze").sku("Spoon2-Sink-Brnz", "sink").price(1199.0).size("24.25", "15.75", "5.5"));
		resolvers.add(new Resolver("627722008222").key("Spoon").titles("Spoon2-Sink", "Moss", "Green-Wht").sku("Spoon2-Sink-Moss-Green-Wht").price(1083.0).size("24.5", "15.75", "5.5"));
		resolvers.add(new Resolver("627722007966").key("Spoon").titles("Spoon2-Sink", "Oxide", "Red-Wht").sku("Spoon2-Sink-Oxide-Red-Wht").price(1083.0).size("24.5", "15.75", "5.5"));
		resolvers.add(new Resolver("627722004231").key("Texture").titles("Bowl-Sink-Wht", "Bowl-Wht", "Vessel Sink").sku("Texture-Bowl-Sink-Wht").price(530.0).size("16.25", "16.25", "5.0"));
		resolvers.add(new Resolver("627722007638").key("Vincent").titles("Vincent-Sink", "Wht").sku("Vincent-Sink-Wht").price(899.0).size("39.25", "19.25", "4.0"));
		
		// ------Waste Overflow Kit--------
		
		resolvers.add(new Resolver("627722002886").key("Installer").sku("AQ-FB-INST").price(199.00).size("", "", ""));
		resolvers.add(new Resolver("627722003937").key("Euroclicker").titles("BN", "Brushed", "Nickel").sku("Euroclicker-BN").price(172.5));
		resolvers.add(new Resolver("627722002008").key("Euroclicker").titles("BN", "Brushed", "Nickel", "FA").sku("Euroclicker-FA-BN").price(240.0).size("1.25", "2.75", ""));
		resolvers.add(new Resolver("627722003944").key("Euroclicker").titles("CP", "Chrome").sku("Euroclicker-CP").price(172.5));
		resolvers.add(new Resolver("627722001933").key("Euroclicker").titles("CP", "Chrome", "FA").sku("Euroclicker-FA-CP").price(199.0).size("1.25", "2.75", ""));
		resolvers.add(new Resolver("627722003951").key("Euroclicker").titles("VB", "Venetian", "Bronze").sku("Euroclicker-VB").price(173.0));
		resolvers.add(new Resolver("627722002435").key("Euroclicker").titles("VB", "Venetian", "Bronze", "FA").sku("Euroclicker-FA-VB").price(240.0));
		resolvers.add(new Resolver("627722002442").key("Euroclicker").titles("ORB", "Oil Rubbed", "Bronze").sku("Euroclicker-FA-ORB").price(240.0));
		resolvers.add(new Resolver("627722005955").key("Euroclicker").titles("Blck", "Black", "Euroclicker-FA-Wht", "S-FA").sku("Euroclicker-S-FA-Blck").price(199.0).size("1.75", "1.75", ""));
		resolvers.add(new Resolver("627722005948").key("Euroclicker").titles("Wht", "White", "Euroclicker-FA-Blck", "S-FA").sku("Euroclicker-S-FA-Wht").price(199.0).size("1.75", "1.75", ""));
		resolvers.add(new Resolver("627722008277").key("Euroclicker").titles("CP", "3500-CP", "3500", "3500Q").sku("Euroclicker-3500-CP").price(150.0).size("2.25", "2.25", "6.25"));
		resolvers.add(new Resolver("627722008284").key("Euroclicker").titles("CP", "3535-CP", "3535", "3535Q").sku("Euroclicker-3535-CP").price(150.0).size("2.25", "2.25", "5.25"));
		resolvers.add(new Resolver("627722008673").key("Euroclicker").titles("FA-BM", "Black").sku("Euroclicker-FA-BM").price(240.0).size("1.25", "2.75", ""));
		resolvers.add(new Resolver("627722008666").key("Euroclicker").titles("FA-Wht", "White").sku("Euroclicker-FA-Wht").price(240.0).size("1.25", "2.75", ""));
		
		resolvers.add(new Resolver("627722003036").key("KN6389").titles("corner", "drain", "brushed", "nickel", "6389").sku("KN6389").price(138.0));
		resolvers.add(new Resolver("627722003029").key("KN6392").titles("corner", "drain", "old", "brass", "6392").sku("KN6392").price(138.0));
		resolvers.add(new Resolver("627722001186").key("KN049289").titles("Deep", "Immersion", "DI", "drain", "brushed", "nickel", "49289").sku("KN049289").price(172.5));
		resolvers.add(new Resolver("627722001209").key("KN049251").titles("Deep", "Immersion", "DI", "drain", "chrome", "49251").sku("KN049251").price(172.5));
		resolvers.add(new Resolver("627722001216").key("KN049291").titles("Deep", "Immersion", "DI", "drain", "copper", "49291").sku("KN049291").price(207.0));
		resolvers.add(new Resolver("627722001223").key("KN049252").titles("Deep", "Immersion", "DI", "drain", "gold", "49252").sku("KN049252").price(276.0));
		resolvers.add(new Resolver("627722003012").key("KN049294").titles("Deep", "Immersion", "DI", "drain", "matte", "chrome", "49294").sku("KN049294").price(172.5));
		resolvers.add(new Resolver("627722001193").key("KN049292").titles("Deep", "Immersion", "DI", "drain", "old", "brass", "6392").sku("KN049292").price(172.5));
		resolvers.add(new Resolver("627722002978").key("FX450").titles("Flex", "waste", "kit", "brushed", "nickel", "938250-BN").sku("KT-FX450-BN").price(138.0));
		resolvers.add(new Resolver("627722002985").key("FX450").titles("Flex", "waste", "kit", "oil", "bronze", "938250-BZ").sku("KT-FX450-BZ").price(138.0));
		resolvers.add(new Resolver("640263091416").key("FX450").titles("Flex", "spare", "waste", "chrome", "FX450-CP").sku("FX450-CP").price(172.5));
		
		resolvers.add(new Resolver("627722001124").key("RI09789").titles("drain", "brushed", "nickel", "BN", "9789").sku("RI09789").price(172.5));
		resolvers.add(new Resolver("627722001148").key("RI09751").titles("drain", "chrome", "CP", "9751").sku("RI09751").price(172.5));
		resolvers.add(new Resolver("627722001155").key("RI09791").titles("drain", "copper", "CO", "9791").sku("RI09791").price(207.0));
		resolvers.add(new Resolver("627722001179").key("RI09752").titles("drain", "gold", "G", "9752").sku("RI09752").price(276.0));
		resolvers.add(new Resolver("627722001131").key("RI09792").titles("drain", "old", "brass", "OB", "9792").sku("RI09792").price(172.5));
		resolvers.add(new Resolver("627722005337").key("RS440").titles("waste", "brushed", "nickel", "BN", "Retro").sku("RS440-BN").price(599.0).size("17.25", "10.75", "0.0"));
		resolvers.add(new Resolver("627722005221").key("RS440").titles("waste", "chrome", "CP", "Retro").sku("RS440-CP").price(499.0).size("17.25", "10.75", "0.0"));
		resolvers.add(new Resolver("627722005238").key("RS440").titles("waste", "old", "brass", "OB", "Retro").sku("RS440-OB").price(599.0).size("17.25", "10.75", "0.0"));
		
		resolvers.add(new Resolver("627722001674").key("BDWOF").titles("ST", "BRN", "waste", "Brushed", "Nickel").sku("BDWOF-ST/BRN").price(172.5));
		resolvers.add(new Resolver("627722001247").key("BDWOF").titles("ST", "CPB", "waste", "Polished", "Chrome").sku("BDWOF-ST/CPB").price(172.5));
		// resolvers.add(new Resolver("627722001681").key("BDWOF").titles("ST",
		// "ORB", "waste", "Oil", "Rubbed",
		// "Bronze").sku("BDWOF-ST/ORB").price(172.5));
		resolvers.add(new Resolver("627722002015").key("BDWOF").titles("ST", "ORB", "waste", "Oil", "Rubbed", "Bronze").sku("BDWOF-ST/ORB").price(172.5));
		resolvers.add(new Resolver("627722001254").key("BDWOF").titles("ST", "PN", "waste", "Polished", "Nickel").sku("BDWOF-ST/PN").price(172.5));
		resolvers.add(new Resolver("627722001230").key("BDWOF").titles("ST", "PVD", "waste", "Polished", "Brass").sku("BDWOF-ST/PVD").price(207.0));
		resolvers.add(new Resolver("627722001261").key("BDWOF").titles("ST", "VB", "waste", "Venetian", "Bronze").sku("BDWOF-ST/VB").price(172.5));
		resolvers.add(new Resolver("627722008260").key("BOT").titles("1-1/4", "CP").sku("BOT-CP").price(150.5).size("13.25", "1.25", "6.0"));
		
		resolvers.add(new Resolver("627722001322").key("Euroflipper").titles("White", "Matte").sku("Euroflipper-Wht-M").price(173.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001346").key("Euroflipper").titles("Chrome", "CP").sku("Euroflipper-FA-CP").price(240.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001308").key("Euroflipper").titles("Blck-M", "Euroflipper-Blck").sku("Euroflipper-Blck-M").price(173.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001285").key("Euroflipper").titles("Brushed", "Nickel", "Brushed Nickel").sku("Euroflipper-BN").price(173.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001278").key("Euroflipper").titles("CP", "Chrome").sku("Euroflipper-CP").price(173.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001407").key("Euroflipper").titles("FA-Blck-M", "FA", "FA-Blck", "Euroflipper-FA-Blck").sku("Euroflipper-FA-Blck-M").price(240.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001360").key("Euroflipper").titles("FA-BN", "FA-Brushed-Nickel", "Euroflipper-FA-BN").sku("Euroflipper-FA-BN").price(240.00).size("2.75", "2.75", "2.5"));
		resolvers.add(new Resolver("627722001414").key("Euroflipper").titles("FA-Wht-M", "FA", "FA-Wht", "Euroflipper-FA-Wht").sku("Euroflipper-FA-Wht-M").price(240.00).size("2.75", "2.75", "2.5"));
		
		resolvers.add(new Resolver("627722008291").key("FD").titles("3783-A", "LavDrain").sku("FD-LavDrain-3783-A").price(150.0).size("2.25", "2.25", "8.5"));
		resolvers.add(new Resolver("627722008307").key("FD").titles("3783-B", "LavDrain").sku("FD-LavDrain-3783-B").price(150.0).size("2.25", "2.25", "8.75"));
		resolvers.add(new Resolver("627722008314").key("FD").titles("3783-C", "LavDrain").sku("FD-LavDrain-3783-C").price(150.0).size("2.25", "2.25", "11.0"));
		resolvers.add(new Resolver("627722003739").key("Metamorfosi").titles("SD-Wht Metamorfosi Sink Drain w. White Ceramic Cover").sku("Metamorfosi-SD-Wht").price(138.0).size("2.5", "2.5", "0.75"));
		resolvers.add(new Resolver("627722003555").key("Organic").titles("Org-Sink", "Sink-Coffee").sku("Org-Sink-Coffee", "Washbasin", "Sink").price(650.0).size("27.2", "19.0", "7.3"));
		resolvers.add(new Resolver("627722003562").key("Organic").titles("Org-Sink", "Sink-Wht").sku("Org-Sink-Wht", "Washbasin", "Sink").price(565.0).size("27.2", "19.0", "7.3"));
		resolvers.add(new Resolver("627722002152").key("Waste").titles("Bronze").sku("AQ-CD-BWOF-BP").price(172.50).size("33", "", ""));
		resolvers.add(new Resolver("627722002145").key("Waste").titles("Nickel").sku("AQ-CD-BWOF-BN").price(172.50).size("33", "", ""));
		resolvers.add(new Resolver("627722002138").key("Waste").titles("Chrome").sku("AQ-CD-BWOF-CP").price(172.50).size("33", "", ""));
	}
	
	public static class ResolverResultItem implements Comparable<ResolverResultItem> {
		public final Resolver resolver;
		public final Double result;
		
		public ResolverResultItem(Resolver resolver, double result) {
			this.resolver = resolver;
			this.result = result;
		}
		
		@Override
		public int compareTo(ResolverResultItem o) {
			return result.compareTo(o.result);
		}
		
		@Override
		public String toString() {
			return resolver + " (" + result + ") ";
		}
	}
	
	public static class ResolverResult {
		public final Resolver resolver;
		public final Double result;
		public final TreeSet<ResolverResultItem> lst;
		
		public ResolverResult(Resolver resolver, Double result, TreeSet<ResolverResultItem> lst) {
			this.resolver = resolver;
			this.result = result;
			this.lst = lst;
		}
		
		@Override
		public String toString() {
			return resolver + " (" + result + ")\r\n    " + lst.descendingSet();
		}
	}
	
	public ResolverResult getUPC(String title, Double price, String info, String sku) {
		Resolver maxRes = DEFAULT;
		Double max = 5.0;
		TreeSet<ResolverResultItem> lst = new TreeSet<ResolverResultItem>();
		for (Resolver resolver : resolvers) {
			Double res = resolver.test(title, price, info, sku);
			if (res > max) {
				max = res;
				maxRes = resolver;
			}
			if (res > 5) {
				lst.add(new ResolverResultItem(resolver, res));
			}
		}
		return new ResolverResult(maxRes, max, lst);
	}
	
	public Resolver getByUPC(String upc, String sku) {
		for (Resolver resolver : resolvers) {
			if (resolver.getUpc().equalsIgnoreCase(upc)) {
				for (String resolverSku : resolver.getSkus()) {
					if ((resolverSku.trim()).equalsIgnoreCase(sku.trim())) {
						return resolver;
					}
				}
				LOG.logInfo("Not equal sku. New sku = " + sku);
			}
		}
		return null;
	}
}
