package com.Aquatica;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

import org.apache.commons.io.output.FileWriterWithEncoding;

import com.Aquatica.window.LOG;
import com.google.common.base.Charsets;

public class TestResult {

	public final Collection<String> foundItems = new ArrayList<String>();
	public final Collection<Item> items = new ArrayList<Item>();
	public boolean hasUPC = false;

	public void addItem(Item item) {
		hasUPC = hasUPC || item.UPC.length() > 0;
		if (item.price == null) {
			LOG.logWarning("Invalid price -> [" + item.UPC + "] " + item.title + " (" + item.price + ")");
		} else {
			LOG.logSuccess("Add item -> [" + item.UPC + "] " + item.title + " (" + item.price + ")");
			items.add(item);
		}
	}

	public TestResult loadFrom(File f) {
		loadFrom(f, Item.class);
		return this;
	}

	public TestResult loadFrom(File f, Class<? extends Item> clazz) {
		try {
			Scanner s = new Scanner(f);
			while (s.hasNextLine()) {
				String line = s.nextLine();
				if (line == null || line.trim().isEmpty()) {
					continue;
				}
				Item item = clazz.newInstance();
				item.parseString(line);
				hasUPC = hasUPC || item.UPC.length() > 0;
				items.add(item);
			}
			s.close();
		} catch (Exception e) {
			LOG.logError(e, "Problem when load results");
		}
		return this;
	}

	public void saveTo(File f) {
		try {
			FileWriterWithEncoding fw = new FileWriterWithEncoding(f, Charsets.UTF_8);
			Item[] results = items.toArray(new Item[items.size()]);
			Arrays.sort(results);
			for (Item o : results) {
				fw.write(o.serialize() + "\r\n");
			}
			fw.flush();
			fw.close();
		} catch (Exception e) {
			LOG.logError(e, "Problem when save results");
		}
	}
}
