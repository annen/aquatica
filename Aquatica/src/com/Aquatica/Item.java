package com.Aquatica;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import com.Aquatica.UPCResolver.ResolverResult;
import com.Aquatica.window.ItemValueProvider;
import com.Aquatica.window.LOG;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public class Item implements Comparable<Item> {
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Joiner joiner = Joiner.on('\t').useForNull("");
	
	private static final Splitter subListSplitter = Splitter.on("~~~").omitEmptyStrings().trimResults();
	private static final Joiner subListJoiner = Joiner.on("~~~").skipNulls();
	
	public String title;
	public String UPC;
	public Double price = 0.0;
	public String href;
	public Integer qty = 0;
	public String SKU = "";
	public Double length = 0.0;
	public Double width = 0.0;
	public Double height = 0.0;
	public Collection<String> images = new LinkedHashSet<String>();
	
	public void parseString(String data) {
		if (data == null || data.trim().isEmpty())
			return;
		List<String> values = splitter.splitToList(data);
		if (values.size() > 0) {
			title = values.get(0);
		}
		if (values.size() > 1) {
			UPC = values.get(1);
		}
		if (values.size() > 2) {
			SKU = values.get(2);
		}
		if (values.size() > 3) {
			price = Double.parseDouble(values.get(3));
		}
		if (values.size() > 4) {
			qty = Integer.parseInt(values.get(4));
		}
		if (values.size() > 5) {
			length = Utils.tryParseDouble(values.get(5), 0.0);
		}
		if (values.size() > 6) {
			width = Utils.tryParseDouble(values.get(6), 0.0);
		}
		if (values.size() > 7) {
			height = Utils.tryParseDouble(values.get(7), 0.0);
		}
		if (values.size() > 8) {
			href = values.get(8);
		}
		if (values.size() > 9) {
			images = subListSplitter.splitToList(values.get(9));
		}
	}
	
	public String serialize() {
		return joiner.join(title, UPC, SKU, price, qty, length, width, height, href, subListJoiner.join(images));
	}
	
	@Override
	public String toString() {
		return "[" + UPC + "] " + title + (SKU.isEmpty() ? "" : " | " + SKU) + " (" + price + ")";
	}
	
	@Override
	public int compareTo(Item arg0) {
		if (arg0 == null)
			return 1;
		return title.compareTo(arg0.title);
	}
	
	public String resolveUPC(String info, Collection<String> upcs) {
		ResolverResult resolver = UPCResolver.instance().getUPC(title, price, info + ' ' + SKU, SKU);
		String upc = resolver.resolver.getUpc();
		if (upcs != null && !upcs.isEmpty()) {
			if (!upcs.contains(upc)) {
				LOG.logWarning("Oracle matched wrong upc (check config) -> [" + upcs + "] " + title + " | " + info + " !=! " + resolver);
			} else {
				UPCResolverTest.instance().addTest(resolver.resolver, title, price, info + ' ' + SKU, SKU);
			}
		} else {
			LOG.logOracle("Match : " + title + " | " + info + " " + SKU);
			if (resolver.resolver.equals(UPCResolver.DEFAULT)) {
				LOG.logWarning("Result: UNKNOWN " + resolver.resolver);
			} else {
				LOG.logOracle("Result: " + resolver.resolver);
			}
			// LOG.logOracle("Purpos: " + resolver.lst.descendingSet());
		}
		return upc;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(UPC, SKU, title, price, qty, length, width, height);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item o = (Item) obj;
		return Objects.equals(UPC, o.UPC) && Objects.equals(SKU, o.SKU) && Objects.equals(href, o.href) && Objects.equals(title, o.title) && Objects.equals(price, o.price) && Objects.equals(qty, o.qty) && Objects.equals(images, o.images) && Objects.equals(length, o.length) && Objects.equals(width, o.width) && Objects.equals(height, o.height);
	}
	
	public boolean equalsAnother(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item o = (Item) obj;
		return Objects.equals(UPC, o.UPC) && Objects.equals(SKU, o.SKU) && Objects.equals(title, o.title) && Objects.equals(price, o.price) && Objects.equals(qty, o.qty) && Objects.equals(images.size(), o.images.size()) && Objects.equals(length, o.length) && Objects.equals(width, o.width) && Objects.equals(height, o.height);
	}
	
	public static ItemValueProvider titleVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return item.title;
		}
	};
	public static ItemValueProvider UPCVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return item.UPC;
		}
	};
	public static ItemValueProvider priceVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return Utils.roundStr(item.price, "");
		}
	};
	public static ItemValueProvider hrefVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return item.href;
		}
	};
	public static ItemValueProvider qtyVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return String.valueOf(item.qty);
		}
	};
	public static ItemValueProvider SKUVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return item.SKU;
		}
	};
	public static ItemValueProvider lengthVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return String.valueOf(item.length);
		}
	};
	public static ItemValueProvider widthVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return String.valueOf(item.width);
		}
	};
	public static ItemValueProvider heightVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return String.valueOf(item.height);
		}
	};
	public static ItemValueProvider imagesVP = new ItemValueProvider() {
		@Override
		public String getValue(Item item) {
			return subListJoiner.join(item.images);
		}
	};
}
