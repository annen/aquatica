package com.Aquatica;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.Aquatica.window.CrmDataRow;
import com.Aquatica.window.LOG;
import com.Aquatica.window.PriceStatus;
import com.Aquatica.window.SizeStatus;
import com.google.common.base.Charsets;

public class Utils {
	public static final Integer ZERO = Integer.valueOf(0);
	public static final DecimalFormat formatter = new DecimalFormat("#0.##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
	public static final Pattern sizeAdditionPattern = Pattern.compile("-\\s*(\\d+)[/,-]\\s*(\\d+)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern proxyPattern = Pattern.compile("\"result\"\\s*:\\s*\"([^\"]+)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	
	public static Integer tryParseInt(Object o, Integer _default) {
		Double result = tryParseDouble(o, _default == null ? null : _default.doubleValue());
		return result == null ? _default : result.intValue();
	}
	
	public static Double tryParseDouble(Object o, Double _default) {
		if (o == null)
			return _default;
		String s = String.valueOf(o);
		if (s.contains(",") && !s.contains(".")) {
			s.replace(',', '.');
		}
		s = s.replaceAll("[^0-9.-]", "").trim();
		if (s.isEmpty())
			return _default;
		try {
			return Double.parseDouble(s);
		} catch (Exception ex) {
			return _default;
		}
	}
	
	public static Double tryParseDouble(String source, Pattern pattern, Double _default) {
		Matcher matcher = pattern.matcher(source);
		if (matcher.find())
			return Utils.tryParseDouble(matcher.group(1), _default);
		else
			return _default;
	}
	
	public static String replaceSizeAdditions(String size) {
		Matcher matcher = sizeAdditionPattern.matcher(size);
		while (matcher.find()) {
			Double d1 = tryParseDouble(matcher.group(1), 1.0);
			Double d2 = tryParseDouble(matcher.group(2), 1.0);
			Double res = d1 / d2;
			res = res - Math.floor(res);
			String subpart = roundStr(res, "0.0").substring(1);
			size = matcher.replaceFirst(subpart);
			matcher = sizeAdditionPattern.matcher(size);
		}
		
		return size;
	}
	
	public static double round(Double value) {
		if (value == null)
			return 0.0;
		return Math.round((value.doubleValue() * 100.0) / 100.0);
	}
	
	public static String roundStr(Double value, String defaultValue) {
		if (value == null)
			return defaultValue;
		double valueR = Math.round((value.doubleValue() * 100.0)) / 100.0;
		return formatter.format(valueR);
	}
	
	public static Color getContrastColor(Color color, boolean isSelected) {
		if (isSelected)
			return new Color(50, 50, 200);
		double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
		return y >= 128 ? Color.black : Color.white;
	}
	
	public static PriceStatus getPriceStatus(Collection<Item> items, Double rowMap) {
		if (items.isEmpty())
			return PriceStatus.EMPTY;
		
		int colorMax = 0;
		for (Item item : items) {
			Double price = item.price;
			if (rowMap != null && rowMap > 0.0) {
				if (price == null || price < 20) {
					colorMax = Math.max(colorMax, 2);
				} else {
					double dif = price - rowMap;
					if (Math.abs(dif) < 50.0) {
						colorMax = Math.max(colorMax, 1);
					} else {
						if (dif > 0) {
							colorMax = Math.max(colorMax, 3);
						} else {
							colorMax = Math.max(colorMax, 4);
						}
					}
				}
			}
		}
		if (colorMax == 1)
			return PriceStatus.VALID;
		if (colorMax == 2)
			return PriceStatus.UNKNOWN;
		if (colorMax == 3)
			return PriceStatus.BIGGER;
		if (colorMax == 4)
			return PriceStatus.LOWER;
		return PriceStatus.UNKNOWN;
	}
	
	public static SizeStatus getSizeStatus(Collection<Item> items, CrmDataRow row) {
		if (items.isEmpty())
			return SizeStatus.VALID;
		Double _length = row.length;
		Double _width = row.width;
		Double _height = row.height;
		List<Integer> _sizes = new ArrayList<Integer>(3);
		_sizes.add(prepareSize(_length));
		_sizes.add(prepareSize(_width));
		_sizes.add(prepareSize(_height));
		for (int i = 0; i < 5; i++) {
			_sizes.remove(ZERO);
		}
		if (_sizes.isEmpty())
			return SizeStatus.VALID;
		
		for (Item item : items) {
			Double length = item.length;
			Double width = item.width;
			Double height = item.height;
			List<Integer> sizes = new ArrayList<Integer>(3);
			sizes.add(prepareSize(length));
			sizes.add(prepareSize(width));
			sizes.add(prepareSize(height));
			for (int i = 0; i < 3; i++) {
				sizes.remove(ZERO);
			}
			if (sizes.isEmpty()) {
				continue;
			}
			List<Integer> test = new ArrayList<Integer>(_sizes);
			for (Integer size : sizes) {
				test.remove(size);
			}
			if (test.isEmpty()) {
				continue;
			}
			for (Integer _size : test) {
				boolean found = false;
				for (Integer size : sizes) {
					if (Math.abs(size - _size) < 10) {
						found = true;
						break;
					}
				}
				if (!found)
					return SizeStatus.INVALID;
			}
		}
		return SizeStatus.VALID;
	}
	
	public static Integer prepareSize(Double size) {
		if (size == null)
			return ZERO;
		if (size <= 2.0)
			return ZERO;
		return Integer.valueOf((int) Math.floor(size * 10));
	}
	
	public static byte[] fileToBytes(String filename) throws IOException {
		File file = null;
		FileInputStream fis = null;
		ByteArrayOutputStream bos = null;
		int read = 0;
		try {
			file = new File(filename);
			fis = new FileInputStream(file);
			bos = new ByteArrayOutputStream();
			while ((read = fis.read()) != -1) {
				bos.write(read);
			}
			return (bos.toByteArray());
		} finally {
			if (fis != null) {
				try {
					fis.close();
					fis = null;
				} catch (IOException ioEx) {
				}
			}
		}
	}
	
	public static void createNewTab(WebDriver newMainDriver) {
		newMainDriver.get("about:blank");
		//WebElement name = newMainDriver.findElement(By.name("q"));
		//name.sendKeys("test");
		//name.sendKeys(Keys.CONTROL + "t");
		((JavascriptExecutor)newMainDriver).executeScript("window.open('about:blank', '_blank');");
		//new Actions(newMainDriver).moveToElement(name).keyDown(Keys.CONTROL).sendKeys("t").keyUp(Keys.CONTROL).build().perform();
		sleep1Sec();
	}
	
	public static void changeToTab1(WebDriver newMainDriver) {
		Set<String> windowHandles = newMainDriver.getWindowHandles();
		if (windowHandles.size() > 1) {
			newMainDriver.switchTo().window(windowHandles.iterator().next());
		} else {
			newMainDriver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.CONTROL, Keys.NUMPAD1));
		}
		newMainDriver.switchTo().defaultContent();
		sleep1Sec();
	}
	
	public static void sleep3Sec() {
		for (int i = 3; i > 0; i--) {
			LOG.logInfo("Sleep " + i + " sec");
			sleep1Sec();
		}
	}
	
	public static void sleep1Sec() {
		try {
			for (int i = 5; i > 0; i--) {
				Thread.sleep(200);
			}
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public static void changeToTab2(WebDriver newMainDriver) throws Exception {
		Set<String> windowHandles = newMainDriver.getWindowHandles();
		if (windowHandles.size() > 1) {
			Iterator<String> iterator = windowHandles.iterator();
			iterator.next();
			newMainDriver.switchTo().window(iterator.next());
		} else {
			newMainDriver.findElement(By.tagName("body")).sendKeys(Keys.chord(Keys.CONTROL, Keys.NUMPAD2));
		}
		newMainDriver.switchTo().defaultContent();
		sleep1Sec();
	}
	
	public static String getProxyUrl(String url) {
		try {
			String newUrl = IOUtils.toString(new URI("http://noblockme.ru/api/anonymize?url=" + URLEncoder.encode(url, Charsets.UTF_8.name())));
			Matcher m = proxyPattern.matcher(newUrl);
			if (!m.find())
				throw new RuntimeException("Not found new proxy url");
			return m.group(1);
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with get proxy url " + url);
			return "";
		}
	}
	
}
