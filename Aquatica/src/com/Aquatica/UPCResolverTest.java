package com.Aquatica;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.Aquatica.UPCResolver.Resolver;
import com.Aquatica.UPCResolver.ResolverResult;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public class UPCResolverTest implements SiteTester {
	private static class TestData {
		public String title;
		public Double price;
		public String info;
		public String upc;
		public String sku;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((info == null) ? 0 : info.hashCode());
			result = prime * result + ((price == null) ? 0 : price.hashCode());
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			result = prime * result + ((upc == null) ? 0 : upc.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TestData other = (TestData) obj;
			if (info == null) {
				if (other.info != null)
					return false;
			} else if (!info.equals(other.info))
				return false;
			if (price == null) {
				if (other.price != null)
					return false;
			} else if (!price.equals(other.price))
				return false;
			if (title == null) {
				if (other.title != null)
					return false;
			} else if (!title.equals(other.title))
				return false;
			if (upc == null) {
				if (other.upc != null)
					return false;
			} else if (!upc.equals(other.upc))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "UPC:" + upc + " TITLE:" + title + " INFO:" + info + " PRICE:" + price;
		}
	}

	private final static UPCResolverTest INSTANCE = new UPCResolverTest();
	private final static String fileName = "UPCResolverTest.tests.txt";
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Joiner joiner = Joiner.on('\t').useForNull("");

	public static UPCResolverTest instance() {
		return INSTANCE;
	}

	private final Map<String, Collection<TestData>> tests = new LinkedHashMap<String, Collection<TestData>>();

	public void reload() {
		try {
			tests.clear();
			File f = new File("./" + fileName);
			if (!f.exists())
				return;
			Scanner s = new Scanner(f);
			while (s.hasNextLine()) {
				String line = s.nextLine();
				if (line == null || line.trim().isEmpty()) {
					continue;
				}
				List<String> values = splitter.splitToList(line);
				TestData resolverTest = new TestData();
				resolverTest.upc = values.get(0);
				resolverTest.title = values.get(1);
				resolverTest.price = tryParse(values);
				resolverTest.info = values.get(3);
				Collection<TestData> resolverTests = tests.get(resolverTest.upc);
				if (resolverTests == null) {
					resolverTests = new ArrayList<TestData>();
					tests.put(resolverTest.upc, resolverTests);
				}
				resolverTests.add(resolverTest);
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: WHEN LOAD RESOLVER TESTS ->" + e.getMessage());
		}
	}

	private double tryParse(List<String> values) {
		try {
			return Double.parseDouble(values.get(2));
		} catch (Exception ex) {
			return 1.0;
		}
	}

	public void save() {
		try {
			File f = new File("./" + fileName);
			FileWriter fw = new FileWriter(f);
			for (Collection<TestData> resolverTests : tests.values()) {
				for (TestData resolverTest : resolverTests) {
					fw.write(joiner.join(resolverTest.upc, resolverTest.title, resolverTest.price, resolverTest.info) + "\r\n");
				}
			}
			fw.flush();
			fw.close();
		} catch (Exception e) {
			System.err.println("ERROR: WHEN SAVE RESOLVER TESTS ->" + e.getMessage());
		}
	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) {
		TestResult result = new TestResult();
		for (Collection<TestData> resolverTests : tests.values()) {
			for (TestData test : resolverTests) {
				ResolverResult resolver = UPCResolver.instance().getUPC(test.title, test.price, test.info, test.sku);
				if (!test.upc.equalsIgnoreCase(resolver.resolver.getUpc())) {
					System.err.println("FAILED TEST");
					System.out.println("\t TEST DATA " + test.toString());
					System.out.println("\t RESOLVE DATA " + resolver.toString());
				} else {
					System.out.println("VALID TEST " + test.toString());
				}
				result.foundItems.add(test.toString());
			}
		}
		return result;
	}

	public void addTest(Resolver resolver, String title, Double price, String info, String sku) {
		Collection<TestData> resolverTests = tests.get(resolver.getUpc());
		if (resolverTests == null) {
			resolverTests = new ArrayList<TestData>();
			tests.put(resolver.getUpc(), resolverTests);
		}
		TestData resolverTest = new TestData();
		resolverTest.upc = resolver.getUpc();
		resolverTest.title = title.replace("\r", " ").replace("\n", " ").replace("\t", " ").replace("  ", " ");
		resolverTest.price = price;
		resolverTest.info = info.replace("\r", " ").replace("\n", " ").replace("\t", " ").replace("  ", " ");
		resolverTest.sku = sku;

		Resolver testResolver = UPCResolver.instance().getUPC(resolverTest.title, resolverTest.price, resolverTest.info, resolverTest.sku).resolver;
		if (!testResolver.getUpc().equals(resolver.getUpc())) {
			System.err.println("WHEN ADD TEST NOT EQUAL UPC -> " + resolver + " !=! " + testResolver);
		} else if (!resolverTests.contains(resolverTest)) {
			resolverTests.add(resolverTest);
		}
	}

	public static void main(String[] args) {
		UPCResolverTest.instance().reload();
		UPCResolverTest.instance().test(null, null);
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		// TODO Auto-generated method stub
		
	}
}
