package com.Aquatica.window;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.WebElement;

import com.Aquatica.DRIVER;
import com.Aquatica.UPCResolver;
import com.Aquatica.UPCResolver.Resolver;
import com.Aquatica.Utils;
import com.Aquatica.drivers.CrmDataDriver;

public class CrmDataInfo {
	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
	
	private List<CrmDataRow> rows = new ArrayList<CrmDataRow>();
	private static Map<Integer, CrmDataInfo> infos = new HashMap<>();
	
	public void addRow(CrmDataRow row) {
		rows.add(row);
	}
	
	public List<CrmDataRow> getRows() {
		return rows;
	}
	
	public static void init(int i, CrmDataInfo info) {
		infos.put(i, info);
		for (CrmDataRow crmDataRow : info.rows) {
			Resolver resolver = UPCResolver.instance().getByUPC(crmDataRow.upc, crmDataRow.sku);
			if (resolver == null || resolver == UPCResolver.DEFAULT) {
				LOG.logInfo("Oracle unknow product " + crmDataRow.toString().replace('\t', ' '));
			} else {
				if (!resolver.getUpc().equals(crmDataRow.upc)) {
					LOG.logOracle("Invalid upc " + resolver + " <!=> " + crmDataRow);
				} else {
					Double price = resolver.getPrice();
					if (price == null) {
						price = 0.0;
					}
					if (Math.abs(price - crmDataRow.map) > 10) {
						LOG.logOracle("Corrected price from " + price + " to " + crmDataRow.map + " for " + resolver);
					}
					resolver.price(crmDataRow.map);
				}
			}
		}
	}
	
	public static CrmDataInfo get() {
		return get(0);
	}
	
	public static CrmDataInfo get(int i) {
		return infos.get(i);
	}
	
	public static CrmDataInfo loadFromFile() {
		try {
			JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory(new File(Settings.get(Settings.DIR_DEFAULT)));
			fc.setDialogTitle("Select Data File (ex. Aquatica active products for import.xls)");
			if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				if (!file.exists() || file.isDirectory() || (!file.getName().toLowerCase().endsWith("xls") && !file.getName().toLowerCase().endsWith("xlsx"))) {
					LOG.logError("Invalid File");
					JOptionPane.showMessageDialog(null, "Invalid File");
					return new CrmDataInfo();
				}
				CrmDataInfo result = CrmDataInfo.loadFromFile(file);
				saveSource(Utils.fileToBytes(file.getAbsolutePath()));
				return result;
			}
			return new CrmDataInfo();
		} catch (Exception ex) {
			LOG.logError(ex, "Problem when load from file");
			return new CrmDataInfo();
		}
	}

	public static CrmDataInfo loadFromFile(File file) {
		try {
			LOG.log("Scan Crm Data from " + file.getAbsolutePath());
			CrmDataInfo info = CrmDataFileReader.readFile(file);
			LOG.log("Loaded " + info.getRows().size() + " products");
			return info;
		} catch (Exception ex) {
			LOG.logError(ex, "Problem when load from file");
			return new CrmDataInfo();
		}
		
	}
	
	public static CrmDataInfo loadFromNet() {
		LOG.log("Load CRM BOT");
		CrmDataDriver driver = DRIVER.getCrmDriver();
		try {
			LOG.log("Open CRM and AUTH");
			driver.get("https://crm.aquaticagroup.com/AquaticaGroup/main.aspx");
			try {
				Thread.sleep(1000);
				LOG.log("Open products");
				driver.get("https://crm.aquaticagroup.com/AquaticaGroup/_root/homepage.aspx?etc=1024&pagemode=iframe&sitemappath=SFA|SFA|nav_products");
				Thread.sleep(2000);
				int attemps = 1;
				while (driver.findElementsByXPath("//ul[@class='ms-crm-VS-Menu']").isEmpty() && attemps < 10) {
					LOG.log("Change product lists to 'Aquatica active products for import' (Attemp " + attemps + ")");
					List<WebElement> button = driver.findElementsById("crmGrid_SavedNewQuerySelector");
					if (!button.isEmpty()) {
						button.get(0).click();
					}
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					attemps++;
				}
				LOG.log("Update product list");
				driver.findElementsByXPath("//a[contains(@class,'ms-crm-VS-MenuItem-Anchor')][contains(.,'for import')]").get(0).click();
				for (int loadWait = 5; loadWait > 0; loadWait--) {
					Thread.sleep(1000);
					LOG.log("Waiting load " + loadWait + " sec");
				}
				LOG.log("Scan  product list");
				String source = driver.findElementByXPath("//table[@id='gridBodyTable']").getAttribute("innerHTML");
				saveSource(source.getBytes("UTF-8"));
				CrmDataInfo info = CrmDataFileReader.readHtml(source);
				LOG.log("Loaded " + info.getRows().size() + " products");
				return info;
			} catch (Exception ex) {
				LOG.logError(ex, "Problem when load from CRM");
				return new CrmDataInfo();
			}
		} finally {
			DRIVER.freeCrmDriver(driver);
		}
	}
	
	public static void saveSource(byte[] source) throws IOException {
		LocalDate date = new LocalDate();
		String fileName = formatter.print(date) + ".html";
		File crmData = new File("./CrmData/");
		if (!crmData.exists()) {
			crmData.mkdirs();
		}
		File crmFile = new File(crmData, fileName);
		if (!crmFile.exists()) {
			crmFile.createNewFile();
		}
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(crmFile));
		bos.write(source);
		bos.flush();
		bos.close();
	}
	
	public static File lastSource() throws IOException {
		File crmFile = null;
		File crmData = new File("./CrmData/");
		if (!crmData.exists())
			return crmFile;
		LocalDate maxDate = new LocalDate(2000, 1, 1);
		File[] listFiles = crmData.listFiles();
		for (File file : listFiles) {
			if (!file.isFile()) {
				continue;
			}
			String name = file.getName();
			if (name.length() == 155 && !name.endsWith(".html")) {
				continue;
			}
			name = name.substring(0, 10);
			LocalDate date = formatter.parseLocalDate(name);
			if (date.isAfter(maxDate)) {
				maxDate = date;
				crmFile = file;
			}
		}
		return crmFile;
	}
}
