package com.Aquatica.window;

import java.awt.Color;

public enum PriceStatus {

	EMPTY(Color.LIGHT_GRAY, 0), VALID(new Color(0, 150, 0), 1), UNKNOWN(new Color(102, 102, 0), 2), BIGGER(new Color(250, 250, 50), 3), LOWER(new Color(250, 150, 50), 4);

	private Color color;
	private int danger;

	private PriceStatus(Color color, int danger) {
		this.color = color;
		this.danger = danger;
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getDanger() {
		return danger;
	}

}
