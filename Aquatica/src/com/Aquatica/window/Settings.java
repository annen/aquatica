package com.Aquatica.window;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.Aquatica.UPCResolver;
import com.Aquatica.testers.*;

public class Settings {
	
	public static final String CRM_USER = "CRM.User";
	public static final String CRM_PASSWORD = "CRM.Password";
	public static final String CRM_DOMAIN = "CRM.Domain";
	public static final String AMAZONADMIN_LOGIN = "AmazonAdmin.Login";
	public static final String AMAZONADMIN_PASSWORD = "AmazonAdmin.Password";
	public static final String PROXY_SERVER = "Proxy.Server";
	public static final String DIR_DEFAULT = "Dir.Default";
	public static final String DRIVER = "Driver";
	public static final String CHROME_DRIVER_PATH = "webdriver.chrome.driver";
	public static final String CHROME_PROFILE_PATH = "webdriver.chrome.profile.path";
	
	private static final String AQUATICA_PREFIX = "Aquatica.";
	private static final String UPC_RESOLVERS_PREFIX = "UPCResolvers.";
	
	public static final String BOTS_ATGSTORES_URLS = "Bots.AtgStores.Urls";
	public static final String BOTS_WAYFAIR_URLS = "Bots.WayFair.Urls";
	public static final String BOTS_WAYFAIR_COOKIE_SID = "Bots.WayFair.Cookie.D_SID";
	public static final String BOTS_WAYFAIR_COOKIE_PID = "Bots.WayFair.Cookie.D_PID";
	public static final String BOTS_WAYFAIR_COOKIE_IID = "Bots.WayFair.Cookie.D_IID";
	public static final String BOTS_WAYFAIR_COOKIE_UID = "Bots.WayFair.Cookie.D_UID";
	public static final String BOTS_WAYFAIR_COOKIE_HID = "Bots.WayFair.Cookie.D_HID";
	public static final String BOTS_WAYFAIRUK_URLS = "Bots.WayFairUk.Urls";
	public static final String BOTS_WAYFAIRUK_COOKIE_SID = "Bots.WayFairUk.Cookie.D_SID";
	public static final String BOTS_WAYFAIRUK_COOKIE_PID = "Bots.WayFairUk.Cookie.D_PID";
	public static final String BOTS_WAYFAIRUK_COOKIE_IID = "Bots.WayFairUk.Cookie.D_IID";
	public static final String BOTS_WAYFAIRUK_COOKIE_UID = "Bots.WayFairUk.Cookie.D_UID";
	public static final String BOTS_WAYFAIRUK_COOKIE_HID = "Bots.WayFairUk.Cookie.D_HID";
	public static final String BOTS_HOMEDEPOT_URLS = "Bots.HomeDepot.Urls";
	public static final String BOTS_OVERSTOCK_URLS = "Bots.OverStock.Urls";
	public static final String BOTS_HOUZZ_URLS = "Bots.Houzz.Urls";
	public static final String BOTS_HOUZZ_COOKIE_UTMA = "Bots.Houzz.Cookie.__utma";
	public static final String BOTS_HOUZZ_COOKIE_UTMB = "Bots.Houzz.Cookie.__utmb";
	public static final String BOTS_HOUZZ_COOKIE_UTMC = "Bots.Houzz.Cookie.__utmc";
	public static final String BOTS_HOUZZ_COOKIE_UTMLI = "Bots.Houzz.Cookie.__utmli";
	public static final String BOTS_HOUZZ_COOKIE_UTMT = "Bots.Houzz.Cookie.__utmt";
	public static final String BOTS_HOUZZ_COOKIE_UTMZ = "Bots.Houzz.Cookie.__utmz";
	public static final String BOTS_AMAZONCOM_URLS = "Bots.AmazonCom.Urls";
	public static final String BOTS_AMAZONCOM_COOKIE_CAPTHCA1 = "Bots.AmazonCom.Cookie.CAPTHCA1";
	public static final String BOTS_AMAZONCOM_COOKIE_CAPTHCA2 = "Bots.AmazonCom.Cookie.CAPTHCA2";
	
	public static final String BOTS_AMAZONCOMNEW_URLS = "Bots.AmazonComNew.Urls";
	public static final String BOTS_AMAZONCOMNEW_COOKIE_CAPTHCA1 = "Bots.AmazonCom.Cookie.CAPTHCA1";
	public static final String BOTS_AMAZONCOMNEW_COOKIE_CAPTHCA2 = "Bots.AmazonCom.Cookie.CAPTHCA2";
	
	public static final String BOTS_AMAZONCOMADMIN_URLS = "Bots.AmazonComAdmin.Urls";
	public static final String BOTS_AMAZONCOMADMIN_COOKIE_CAPTHCA1 = "Bots.AmazonCom.Cookie.CAPTHCA1";
	public static final String BOTS_AMAZONCOMADMIN_COOKIE_CAPTHCA2 = "Bots.AmazonCom.Cookie.CAPTHCA2";
	
	public static final String BOTS_AMAZONCA_URLS = "Bots.AmazonCa.Urls";
	public static final String BOTS_OPENSKY_URLS = "Bots.OpenSky.Urls";
	public static final String BOTS_FAUCETTOWNUSA_URLS = "Bots.FaucetTownUsa.Urls";
	public static final String BOTS_HOMECLICK_URLS = "Bots.HomeClick.Urls";
	public static final String BOTS_MENARDS_URLS = "Bots.Menards.Urls";
	public static final String BOTS_BLUEBATH_URLS = "Bots.Bluebath.Urls";
	public static final String BOTS_HAYNEEDLE_URLS = "Bots.Hayneedle.Urls";
	public static final String BOTS_HAYNEEDLE_COOKIE_SID = "Bots.Hayneedle.Cookie.D_SID";
	public static final String BOTS_HAYNEEDLE_COOKIE_PID = "Bots.Hayneedle.Cookie.D_PID";
	public static final String BOTS_HAYNEEDLE_COOKIE_IID = "Bots.Hayneedle.Cookie.D_IID";
	public static final String BOTS_HAYNEEDLE_COOKIE_UID = "Bots.Hayneedle.Cookie.D_UID";
	public static final String BOTS_HAYNEEDLE_COOKIE_HID = "Bots.Hayneedle.Cookie.D_HID";
	public static final String BOTS_ALLMODERN_URLS = "Bots.AllModern.Urls";
	public static final String BOTS_ALLMODERN_COOKIE_SID = "Bots.AllModern.Cookie.D_SID";
	public static final String BOTS_ALLMODERN_COOKIE_PID = "Bots.AllModern.Cookie.D_PID";
	public static final String BOTS_ALLMODERN_COOKIE_IID = "Bots.AllModern.Cookie.D_IID";
	public static final String BOTS_ALLMODERN_COOKIE_UID = "Bots.AllModern.Cookie.D_UID";
	public static final String BOTS_ALLMODERN_COOKIE_HID = "Bots.AllModern.Cookie.D_HID";
	public static final String BOTS_MODERNBATHROOM_URLS = "Bots.ModernBathroom.Urls";
	public static final String BOTS_GOEDEKERS_URLS = "Bots.GoEdekers.Urls";
	public static final String BOTS_GOEDEKERS_COOKIE_SID = "Bots.GoEdekers.Cookie.D_SID";
	public static final String BOTS_GOEDEKERS_COOKIE_PID = "Bots.GoEdekers.Cookie.D_PID";
	public static final String BOTS_GOEDEKERS_COOKIE_IID = "Bots.GoEdekers.Cookie.D_IID";
	public static final String BOTS_GOEDEKERS_COOKIE_UID = "Bots.GoEdekers.Cookie.D_UID";
	public static final String BOTS_GOEDEKERS_COOKIE_HID = "Bots.GoEdekers.Cookie.D_HID";
	public static final String BOTS_GOEDEKERS_COOKIE_ZID = "Bots.GoEdekers.Cookie.D_ZID";
	public static final String BOTS_QUALITYBATH_URLS = "Bots.QualityBath.Urls";
	public static final String BOTS_QUALITYBATHWITHOUTCAPTCHA_URLS = "Bots.QualityBathWithoutCaptcha.Urls";
	public static final String BOTS_VINTAGETUB_URLS = "Bots.VintageTub.Urls";
	public static final String BOTS_BRAVOBUSINESSMEDIA_URLS = "Bots.BravoBusinessMedia.Urls";
	public static final String BOTS_BATHTUBSPLUS_URLS = "Bots.BathtubsPlus.Urls";
	public static final String BOTS_ULTRABATHROOMVANITIES_URLS = "Bots.UltraBathRoomVanities.Urls";
	public static final String BOTS_ARCHIEXPO_URLS = "Bots.Archiexpo.Urls";
	public static final String BOTS_ARCHIPRODUCTS_URLS = "Bots.ArchiProducts.Urls";
	public static final String BOTS_GORGEROUSTUBS_URLS = "Bots.GorgeousTubs.Urls";
	public static final String BOTS_PREMIUMBATHS_URLS = "Bots.PremiumBaths.Urls";
	public static final String BOTS_KITCHENSOURCE_URLS = "Bots.KitchenSource.Urls";
	public static final String BOTS_TUBZ_URLS = "Bots.Tubz.Urls";
	public static final String BOTS_CLOUDSHOWERS_URLS = "Bots.CloudShowers.Urls";
	public static final String BOTS_LOWES_URLS = "Bots.Lowes.Urls";
	
	public static final String BOTS_MOTHER_URLS = "Bots.Mother.Urls";
	
	private static final boolean resolversSave = false;
	
	private static Settings INSTANCE;
	
	private static Settings instance() {
		if (INSTANCE == null) {
			INSTANCE = new Settings();
		}
		return INSTANCE;
	}
	
	private final Map<String, String> properties = new LinkedHashMap<>();
	private final Map<String, String> upcResolvers = new LinkedHashMap<>();
	private File propertyFile;
	
	public Settings() {
		propertyFile = new File(System.getProperty("user.home"), "aquatica.properties");
		if (!propertyFile.exists()) {
			try {
				propertyFile.createNewFile();
			} catch (Exception ex) {
				LOG.logError(ex, "Problem when create new settings");
			}
		}
	}
	
	public static void init() {
		Settings settings = instance();
		addDefaultProperties();
		for (Entry<Object, Object> prop : getPropertyFile().entrySet()) {
			String key = prop.getKey().toString();
			String value = prop.getValue().toString();
			if (key.startsWith(AQUATICA_PREFIX)) {
				settings.properties.put(key.substring(AQUATICA_PREFIX.length()), value);
			}
			if (key.startsWith(UPC_RESOLVERS_PREFIX)) {
				settings.upcResolvers.put(key.substring(UPC_RESOLVERS_PREFIX.length()), value);
			}
		}
		
		if (settings.upcResolvers.isEmpty()) {
			flush();
		} else {
			UPCResolver.instance().resetResolvers(settings.upcResolvers.values());
		}
	}
	
	public static Properties getPropertyFile() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(instance().propertyFile));
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with load settings");
		}
		return properties;
	}
	
	private static void addDefaultProperties() {
		Settings settings = instance();
		Map<String, String> p = settings.properties;
		p.put(CRM_USER, "karina");
		p.put(CRM_PASSWORD, "kariwka_galinka1989");
		p.put(CRM_DOMAIN, "aquatica");
		p.put(AMAZONADMIN_LOGIN, "goran@aquaticabath.com");
		p.put(AMAZONADMIN_PASSWORD, "tubbies2018%^");
		p.put(PROXY_SERVER, "188.166.64.58:1234");
		p.put(DIR_DEFAULT, new File(System.getProperty("user.home"), "Desktop").getAbsolutePath());
		p.put(DRIVER, "CHROME");
		p.put(CHROME_DRIVER_PATH, "D:\\AQUATICA\\chromedriver.exe");
		p.put(CHROME_PROFILE_PATH, "C:/Users/Vlad/AppData/Local/Google/Chrome/User Data");
		
		p.put(BOTS_ATGSTORES_URLS, AtgStoresSiteTester.MAIN_PAGE);
		p.put(BOTS_WAYFAIR_URLS, WayFairSiteTester.MAIN_PAGE);
		p.put(BOTS_WAYFAIRUK_URLS, WayFairUkSiteTester.MAIN_PAGE);
		p.put(BOTS_HOMEDEPOT_URLS, HomeDepotSiteTester.MAIN_PAGE);
		p.put(BOTS_OVERSTOCK_URLS, OverStockSiteTester.MAIN_PAGE);
		p.put(BOTS_HOUZZ_URLS, HouzzSiteTester.MAIN_PAGE);
		p.put(BOTS_AMAZONCOM_URLS, AmazonComSiteTester.MAIN_PAGE);
		p.put(BOTS_AMAZONCOMNEW_URLS, AmazonComNewSiteTester.MAIN_PAGE);
		p.put(BOTS_AMAZONCOMADMIN_URLS, AmazonComSelfAdminTester.MAIN_PAGE);
		p.put(BOTS_AMAZONCA_URLS, AmazonCaSiteTester.MAIN_PAGE);
		p.put(BOTS_OPENSKY_URLS, OpenSkySiteTester.MAIN_PAGE);
		p.put(BOTS_FAUCETTOWNUSA_URLS, FaucetTownUsaTester.MAIN_PAGE);
		p.put(BOTS_HOMECLICK_URLS, HomeClickSiteTester.MAIN_PAGE);
		p.put(BOTS_MENARDS_URLS, MenardsSiteTester.MAIN_PAGE);
		p.put(BOTS_BLUEBATH_URLS, BluebathSiteTester.MAIN_PAGE);
		p.put(BOTS_HAYNEEDLE_URLS, HayneedleSiteTester.MAIN_PAGE);
		p.put(BOTS_ALLMODERN_URLS, AllModernSiteTester.MAIN_PAGE);
		p.put(BOTS_MODERNBATHROOM_URLS, ModernBathroomSiteTester.MAIN_PAGE);
		p.put(BOTS_GOEDEKERS_URLS, GoEdekersSiteTester.MAIN_PAGE);
		p.put(BOTS_QUALITYBATH_URLS, QualityBathSiteTester.MAIN_PAGE);
		p.put(BOTS_QUALITYBATHWITHOUTCAPTCHA_URLS, QualityBathSiteTesterWithoutCaptcha.MAIN_PAGE);
		p.put(BOTS_VINTAGETUB_URLS, VintageTubSiteTester.MAIN_PAGE);
		p.put(BOTS_BRAVOBUSINESSMEDIA_URLS, BravoBusinessMediaSiteTester.MAIN_PAGE);
		p.put(BOTS_BATHTUBSPLUS_URLS, BathtubsPlusSiteTester.MAIN_PAGE);
		p.put(BOTS_ULTRABATHROOMVANITIES_URLS, UltraBathRoomVanitiesSiteTester.MAIN_PAGE);
		p.put(BOTS_ARCHIEXPO_URLS, ArchiExpoSiteTester.MAIN_PAGE);
		p.put(BOTS_ARCHIPRODUCTS_URLS, ArchiProductsSiteTester.MAIN_PAGE);
		p.put(BOTS_GORGEROUSTUBS_URLS, GorgeousTubsTester.MAIN_PAGE);
		p.put(BOTS_PREMIUMBATHS_URLS, PremiumBathsTester.MAIN_PAGE);
		p.put(BOTS_KITCHENSOURCE_URLS, KitchenSourceSiteTester.MAIN_PAGE);
		p.put(BOTS_TUBZ_URLS, TubzSiteTester.MAIN_PAGE);
		p.put(BOTS_CLOUDSHOWERS_URLS, CloudShowersSiteTester.MAIN_PAGE);
		p.put(BOTS_LOWES_URLS, LowesSiteTester.MAIN_PAGE);
		
		p.put(BOTS_MOTHER_URLS, MotherBots.MAIN_PAGE);
		
		
		// p.put(BOTS_WAYFAIR_COOKIE_SID,
		// "46.46.79.101:lHJtXZl1tN21jJUxDR0mtlp/wCOSf2wgz9CIQACG0Z0");
		// p.put(BOTS_WAYFAIR_COOKIE_PID,
		// "703F990A-AE82-3F22-B757-CCC6761BE1C4");
		// p.put(BOTS_WAYFAIR_COOKIE_IID,
		// "BA2F9151-3C2E-3772-ADA2-EF8A0DE0BC2B");
		// p.put(BOTS_WAYFAIR_COOKIE_UID,
		// "E6239DEC-8294-3ADC-99A5-85C136F8D424");
		// p.put(BOTS_WAYFAIR_COOKIE_HID,
		// "KcmAQBFLMbSzQI7/xFWFBoZXhan18FlrKz17YI6QVSg");
		
		p.put(BOTS_WAYFAIR_COOKIE_SID, "94.240.172.169:g0LXkDOMJ8aBe2c+FSu9RJmgBhcwWam2JRKkq+PuiNI");
		p.put(BOTS_WAYFAIR_COOKIE_PID, "D5CFFC8C-AD27-39A7-8B6C-6B0502F1E06F");
		p.put(BOTS_WAYFAIR_COOKIE_IID, "01C5E33B-DA9A-3388-B86C-19F60DCE2708");
		p.put(BOTS_WAYFAIR_COOKIE_UID, "483660CC-66B0-3142-BF49-F0CDEFCFDCD0");
		p.put(BOTS_WAYFAIR_COOKIE_HID, "uK/Xey+P0wnS64Ur41S0py73gI/bltRghKNblgGLWhw");
		
		p.put(BOTS_WAYFAIRUK_COOKIE_SID, "94.240.172.169:g0LXkDOMJ8aBe2c+FSu9RJmgBhcwWam2JRKkq+PuiNI");
		p.put(BOTS_WAYFAIRUK_COOKIE_PID, "D5CFFC8C-AD27-39A7-8B6C-6B0502F1E06F");
		p.put(BOTS_WAYFAIRUK_COOKIE_IID, "01C5E33B-DA9A-3388-B86C-19F60DCE2708");
		p.put(BOTS_WAYFAIRUK_COOKIE_UID, "483660CC-66B0-3142-BF49-F0CDEFCFDCD0");
		p.put(BOTS_WAYFAIRUK_COOKIE_HID, "uK/Xey+P0wnS64Ur41S0py73gI/bltRghKNblgGLWhw");
		
		p.put(BOTS_AMAZONCOM_COOKIE_CAPTHCA1, "1463232765344987");
		p.put(BOTS_AMAZONCOM_COOKIE_CAPTHCA2, "yJI2NFe55U6B971u13Yimg==");
		
		p.put(BOTS_HAYNEEDLE_COOKIE_SID, "94.240.162.194:ddqI/ksAJNuiApffWlvaG1T1jZrmBTPKfRyfR88R5Ms");
		p.put(BOTS_HAYNEEDLE_COOKIE_PID, "BBD346D4-A00B-395C-B075-B209EAC131F3");
		p.put(BOTS_HAYNEEDLE_COOKIE_IID, "FEE01588-DF54-3E69-80CD-3D3B6F2901EF");
		p.put(BOTS_HAYNEEDLE_COOKIE_UID, "1704A4E2-BCA8-316E-A899-F5509B90E427");
		p.put(BOTS_HAYNEEDLE_COOKIE_HID, "Bl9JAWFIGFHe4/QMS6Nu+jvfFQ8eSrM0LEV8bj2jA8g");
		
		p.put(BOTS_HOUZZ_COOKIE_UTMA, "252751504.1857489571.1502382101.1502899740.1502903828.6");
		p.put(BOTS_HOUZZ_COOKIE_UTMB, "252751504.5.10.1502903828");
		p.put(BOTS_HOUZZ_COOKIE_UTMC, "252751504");
		p.put(BOTS_HOUZZ_COOKIE_UTMLI, "mainContent");
		p.put(BOTS_HOUZZ_COOKIE_UTMT, "1");
		p.put(BOTS_HOUZZ_COOKIE_UTMZ, "252751504.1502382101.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
		
		p.put(BOTS_ALLMODERN_COOKIE_SID, "94.240.162.194:M9pqqQkWyc6pzv0ZA3XPEbPgobLdh0GBt4gvMSlJJQY");
		p.put(BOTS_ALLMODERN_COOKIE_PID, "C7BEEA55-B18F-3D80-8CE4-0AA899E18785");
		p.put(BOTS_ALLMODERN_COOKIE_IID, "0B8060E1-0FDC-3080-9F64-04E842FC0AE2");
		p.put(BOTS_ALLMODERN_COOKIE_UID, "FE7046EF-6E9D-3C87-82B1-7064810C7EAA");
		p.put(BOTS_ALLMODERN_COOKIE_HID, "WzxzEFZshCGRiHwNUdfoNnaQehZRQKnbfndp5i+HDTI");
		
		p.put(BOTS_GOEDEKERS_COOKIE_SID, "94.240.165.250:nzyoFwXIwEr2TakVwzXSeY6xh6E7c6Q6UfcvIWXOpZI");
		p.put(BOTS_GOEDEKERS_COOKIE_PID, "AEFA967D-10AF-3D90-A088-03032A9BA7CA");
		p.put(BOTS_GOEDEKERS_COOKIE_IID, "E2D75DF5-87F8-37A8-8479-445F8993BD97");
		p.put(BOTS_GOEDEKERS_COOKIE_UID, "CF9E2099-270E-3255-8402-73EC698C7FE7");
		p.put(BOTS_GOEDEKERS_COOKIE_HID, "3uauK7elkakViWfcMiV3IAZdAvclKmcflU4jiUCRO5Q");
		p.put(BOTS_GOEDEKERS_COOKIE_ZID, "3FC78BA4-836C-3B14-93FD-1746FE48FF28");
	}
	
	public static Map<String, String> getProperties() {
		return instance().properties;
	}
	
	public static Map<String, String> getUpcResolvers() {
		return instance().upcResolvers;
	}
	
	public static String get(String key) {
		return instance().properties.get(key);
	}
	
	public static void flush() {
		Properties propertyFile = getPropertyFile();
		for (Entry<String, String> prop : instance().properties.entrySet()) {
			String key = prop.getKey().toString();
			String value = prop.getValue().toString();
			propertyFile.setProperty(AQUATICA_PREFIX + key, value);
		}
		if (resolversSave) {
			instance().upcResolvers.clear();
			instance().upcResolvers.putAll(UPCResolver.instance().serialize());
			for (Entry<String, String> prop : instance().upcResolvers.entrySet()) {
				String key = prop.getKey().toString();
				String value = prop.getValue().toString();
				propertyFile.setProperty(UPC_RESOLVERS_PREFIX + key, value);
			}
		}
		try {
			propertyFile.store(new FileOutputStream(instance().propertyFile), "");
		} catch (Exception ex) {
			LOG.logError(ex, "Problem with save settings");
		}
	}
}
