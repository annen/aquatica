package com.Aquatica.window;

import static com.Aquatica.Utils.*;
import static com.Aquatica.window.SizeStatus.VALID;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.SiteTester.ProcessListener;
import com.Aquatica.TestResult;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.testers.*;
import com.Aquatica.window.commerceHub.CommerceHubFrame;
import com.google.common.base.Splitter;

public class Aquatica extends JFrame {
	
	private ValueProvider<Boolean> LOAD_IF_EXIST = new ValueProvider<Boolean>() {
		
		@Override
		public Boolean getValue() {
			return chckbxUseOldData.isSelected();
		}
	};
	
	private void registerTesters() {
		siteTesters = new ArrayList<SiteTester>();
		siteTesters.add(new AmazonComSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_AMAZONCOM_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new AmazonComSelfAdminTester(splitterS.splitToList(Settings.get(Settings.BOTS_AMAZONCOMADMIN_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new AmazonComNewSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_AMAZONCOMNEW_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new BluebathSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_BLUEBATH_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new BravoBusinessMediaSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_BRAVOBUSINESSMEDIA_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new CloudShowersSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_CLOUDSHOWERS_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new FaucetTownUsaTester(splitterS.splitToList(Settings.get(Settings.BOTS_FAUCETTOWNUSA_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new GoEdekersSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_GOEDEKERS_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new GorgeousTubsTester(splitterS.splitToList(Settings.get(Settings.BOTS_GORGEROUSTUBS_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new HouzzSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_HOUZZ_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new KitchenSourceSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_KITCHENSOURCE_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new MenardsSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_MENARDS_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new ModernBathroomSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_MODERNBATHROOM_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new OverStockSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_OVERSTOCK_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new QualityBathSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_QUALITYBATH_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new TubzSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_TUBZ_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new VintageTubSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_VINTAGETUB_URLS)), LOAD_IF_EXIST));
		siteTesters.add(new WayFairSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_WAYFAIR_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new AllModernSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_ALLMODERN_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new AmazonCaSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_AMAZONCA_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new ArchiExpoSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_ARCHIEXPO_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new ArchiProductsSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_ARCHIPRODUCTS_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new AtgStoresSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_ATGSTORES_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new BathtubsPlusSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_BATHTUBSPLUS_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new HayneedleSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_HAYNEEDLE_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new HomeClickSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_HOMECLICK_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new HomeDepotSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_HOMEDEPOT_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new LowesSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_LOWES_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new OpenSkySiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_OPENSKY_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new PremiumBathsTester(splitterS.splitToList(Settings.get(Settings.BOTS_PREMIUMBATHS_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new QualityBathSiteTesterWithoutCaptcha(splitterS.splitToList(Settings.get(Settings.BOTS_QUALITYBATHWITHOUTCAPTCHA_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new UltraBathRoomVanitiesSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_ULTRABATHROOMVANITIES_URLS)), LOAD_IF_EXIST));
//		siteTesters.add(new WayFairUkSiteTester(splitterS.splitToList(Settings.get(Settings.BOTS_WAYFAIRUK_URLS)), LOAD_IF_EXIST));
		
//		siteTesters.add(new MotherBots(splitterS.splitToList(Settings.get(Settings.BOTS_MOTHER_URLS)), LOAD_IF_EXIST));
		
		
		Collections.sort(siteTesters, new Comparator<SiteTester>() {
			@Override
			public int compare(SiteTester o1, SiteTester o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		
	}
	
	private final class ProcessResultListener implements ProcessListener {
		private final Map<String, Object[]> rows;
		private final PlaceModel dataModel;
		private final SiteTester siteTester;
		private final int testerNumber;
		
		private boolean partResult = false;
		
		private ProcessResultListener(Map<String, Object[]> rows, PlaceModel dataModel, SiteTester siteTester, int testerNumber) {
			this.rows = rows;
			this.dataModel = dataModel;
			this.siteTester = siteTester;
			this.testerNumber = testerNumber;
		}
		
		@Override
		public void itemFinish(Item item) {
			partResult = true;
			String upc = item.UPC;
			Object[] list = rows.get(upc);
			if (list == null) {
				dataModel.addDiscontinuedItem(siteTester, item);
				return;
			}
			TesterCell testerCell = (TesterCell) list[testerNumber + 1];
			testerCell.setItem(item);
			dataModel.fireTableCellUpdated(testerCell.getRowIndex(), testerCell.getColIndex());
		}
		
		@Override
		public void allFinish(TestResult result) {
			if (!partResult) {
				for (Item item : result.items) {
					itemFinish(item);
				}
			}
			dataModel.addTestResults(testerNumber + 1, result);
			for (Item item : dataModel.getDiscontinuedItems(siteTester)) {
				LOG.logWarning("DISCONTINUED: " + item.toString().replace("\t", " "));
			}
			for (Object[] row : rows.values()) {
				TesterCell testerCell = (TesterCell) row[testerNumber + 1];
				if (!testerCell.isSetResult()) {
					testerCell.setItem(null);
					dataModel.fireTableCellUpdated(testerCell.getRowIndex(), testerCell.getColIndex());
				}
			}
		}
	}
	
	public static class TesterJCheckBox extends JCheckBox {
		
		public final SiteTester tester;
		
		public TesterJCheckBox(SiteTester tester) {
			super(tester.toString());
			this.tester = tester;
		}
		
	}
	
	public static class MultiLineTableCellRenderer extends JPanel implements TableCellRenderer {
		public static final Font font = new JLabel("123").getFont();
		
		private final JTextArea area;
		private final JButton next;
		private final JButton back;
		
		public MultiLineTableCellRenderer() {
			SpringLayout sl_contentPane = new SpringLayout();
			setLayout(sl_contentPane);
			setBorder(VALID.getBorder());
			area = new JTextArea();
			next = new JButton(">");
			next.setFont(area.getFont());
			next.setPreferredSize(new Dimension(25, 15));
			next.setMargin(new Insets(1, 1, 1, 1));
			sl_contentPane.putConstraint(SpringLayout.SOUTH, next, -2, SpringLayout.SOUTH, this);
			sl_contentPane.putConstraint(SpringLayout.EAST, next, -2, SpringLayout.EAST, this);
			add(next);
			
			back = new JButton("<");
			back.setFont(area.getFont());
			back.setPreferredSize(new Dimension(25, 15));
			back.setMargin(new Insets(1, 1, 1, 1));
			sl_contentPane.putConstraint(SpringLayout.SOUTH, back, -2, SpringLayout.SOUTH, this);
			sl_contentPane.putConstraint(SpringLayout.EAST, back, -27, SpringLayout.EAST, this);
			add(back);
			
			area.setLineWrap(false);
			area.setWrapStyleWord(false);
			area.setOpaque(true);
			area.setFont(font);
			sl_contentPane.putConstraint(SpringLayout.NORTH, area, 0, SpringLayout.NORTH, this);
			sl_contentPane.putConstraint(SpringLayout.WEST, area, 0, SpringLayout.WEST, this);
			sl_contentPane.putConstraint(SpringLayout.SOUTH, area, 0, SpringLayout.SOUTH, this);
			sl_contentPane.putConstraint(SpringLayout.EAST, area, 0, SpringLayout.EAST, this);
			add(area);
		}
		
		@Override
		public Component getTableCellRendererComponent(final JTable table, Object value, boolean isSelected, boolean hasFocus, final int row, int col) {
			next.setVisible(false);
			back.setVisible(false);
			String text = "";
			Color background = table.getBackground();
			if (isSelected) {
				background = table.getSelectionBackground();
			}
			area.setBorder(VALID.getBorder());
			if (col == 0) {
				CrmDataRow rowData = (CrmDataRow) value;
				StringBuilder sb = new StringBuilder();
				sb.append(rowData.name.replace("Aquatica ", "")).append(" (").append(rowData.upc).append(")").append("\n");
				sb.append(rowData.map).append("\n");
				sb.append(rowData.length + " x " + rowData.width + " x " + rowData.height).append("\n");
				sb.append(rowData.sku).append(rowData.iRow == null ? "" : "   Count:" + rowData.iRow.count).append("\n");
				text = sb.toString();
			} else {
				TesterCell rowData = (TesterCell) value;
				if (rowData.isSetResult()) {
					List<Item> items = rowData.items;
					int itemIndex = rowData.itemIndex;
					next.setVisible(items.size() > 1 && itemIndex < (items.size() - 1));
					back.setVisible(items.size() > 1 && itemIndex > 0);
					PriceStatus priceStatus = getPriceStatus(items, rowData.row.map);
					background = priceStatus.getColor();
					SizeStatus sizeStatus = getSizeStatus(items, rowData.row);
					area.setBorder(sizeStatus.getBorder());
					if (items.isEmpty()) {
						text = "Not Found";
					} else {
						StringBuilder sb = new StringBuilder();
						Item item = items.get(itemIndex);
						sb.append(item.title.replace("Aquatica ", "")).append(" (").append(item.UPC).append(")").append("\n");
						sb.append(item.price).append("\n");
						sb.append(item.length + " x " + item.width + " x " + item.height).append("\n");
						sb.append(item.SKU).append(item.qty == null ? "" : "   Count:" + item.qty).append("\n");
						text = sb.toString();
					}
				}
			}
			area.setText(text);
			area.setForeground(getContrastColor(background, isSelected));
			area.setBackground(background);
			return this;
		}
	}
	
	public static class TesterCell {
		private final CrmDataRow row;
		private List<Item> items = new ArrayList<Item>();
		private int itemIndex = 0;
		private boolean setResult = false;
		private int rowIndex;
		private int colIndex;
		
		public TesterCell(CrmDataRow row, int rowIndex, int colIndex) {
			this.row = row;
			this.rowIndex = rowIndex;
			this.colIndex = colIndex;
		}
		
		public void setItem(Item item) {
			if (item != null) {
				this.items.add(item);
				if (setResult) {
					LOG.logWarning("Item " + item + " already added.");
					LOG.logInfo(" Exist Items:");
					for (Item i : items) {
						LOG.logInfo(" " + i.toString());
					}
				}
				
			}
			setResult = true;
		}
		
		public boolean isSetResult() {
			return setResult;
		}
		
		public int getRowIndex() {
			return rowIndex;
		}
		
		public int getColIndex() {
			return colIndex;
		}
		
		public List<Item> getItems() {
			return items;
		}
		
		@Override
		public String toString() {
			if (items.isEmpty())
				return "";
			return items.toString();
		}
		
		public boolean processClick(Rectangle rect, Point point) {
			Integer buttonNumber = getButtonNumber(rect, point);
			if (buttonNumber == null)
				return false;
			if (buttonNumber.equals(2)) {
				if (itemIndex < items.size() - 1) {
					itemIndex++;
				}
			} else {
				if (itemIndex > 0) {
					itemIndex--;
				}
			}
			return true;
		}
		
		private Integer getButtonNumber(Rectangle rect, Point point) {
			double x = rect.getWidth() - (point.getX() - rect.getX());
			double y = rect.getHeight() - (point.getY() - rect.getY());
			boolean isButtons = y >= 2 && y <= 20 && x >= 2 && x <= 52;
			Integer buttonNumber = !isButtons ? null : x < 28 ? 2 : 1;
			return buttonNumber;
		}
	}
	
	private JPanel contentPane;
	private List<SiteTester> siteTesters;
	private static final Splitter splitterS = Splitter.on('\n').omitEmptyStrings().trimResults();
	private static final Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
	
	// private static final Joiner joiner = Joiner.on('\n').useForNull("");
	private JTable table;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Aquatica frame = new Aquatica();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	List<TesterJCheckBox> checks = new ArrayList<TesterJCheckBox>();
	private JScrollPane checksScrollPane;
	private JPanel panelChecks;
	
	private CrmDataFrame crmDataFrame;
	private CommerceHubFrame commerceHubFrame;
	private SettingsFrame settingsFrame;
	private OracleFrame oracleFrame;
	
	public Aquatica() {
		LOG.init();
		Settings.init();
		setTitle("Aquatica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 909, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		final MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		table = new JTable() {
			
			@Override
			public TableCellRenderer getCellRenderer(int row, int column) {
				return renderer;
			}
			
			@Override
			public String getToolTipText(MouseEvent e) {
				java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				Object value = getValueAt(rowIndex, colIndex);
				String tip = "";
				if (colIndex == 0) {
					CrmDataRow rowData = (CrmDataRow) value;
					StringBuilder sb = new StringBuilder();
					sb.append("<b>Title: </b>").append(rowData.name.replace("Aquatica ", "")).append("<br />");
					sb.append("<b>  UPC: </b>").append(rowData.upc).append("<br />");
					sb.append("<b>Price: </b>").append(rowData.map).append("<br />");
					sb.append("<b> Size: </b>").append(rowData.length + " x " + rowData.width + " x " + rowData.height).append("<br />");
					sb.append("<b>  SKU: </b>").append(rowData.sku).append("<br />");
					sb.append("<b>  QTY: </b>").append(rowData.iRow == null ? "" : "   Count:" + rowData.iRow.count).append("<br />");
					tip = sb.toString();
				} else {
					TesterCell rowData = (TesterCell) value;
					if (rowData.isSetResult()) {
						Collection<Item> items = rowData.items;
						
						if (items.isEmpty()) {
							tip = "Not Found";
						} else {
							StringBuilder sb = new StringBuilder();
							for (Item item : items) {
								if (sb.length() > 0) {
									sb.append("<hr />");
								}
								sb.append("<b>Title: </b>").append(item.title.replace("Aquatica ", "")).append("<br />");
								sb.append("<b>  UPC: </b>").append(item.UPC).append("<br />");
								sb.append("<b>Price: </b>").append(item.price).append("<br />");
								sb.append("<b> Size: </b>").append(item.length + " x " + item.width + " x " + item.height).append("<br />");
								sb.append("<b>  SKU: </b>").append(item.SKU).append("<br />");
								sb.append("<b>  QTY: </b>").append(item.qty).append("<br />");
								sb.append("<hr />");
								sb.append("<b>IMAGES:</b>").append("<br />");
								for (String image : item.images) {
									sb.append("<img style='height:50px; max-height:50px; max-width:200px;' src='").append(image).append("' />");
								}
							}
							
							tip = sb.toString();
						}
					} else {
						tip = "Not set";
					}
				}
				return "<html>" + tip.replaceAll("\n", "<br />") + "</html>";
			}
			
			@Override
			public synchronized void addMouseMotionListener(MouseMotionListener l) {
				if (l instanceof ToolTipManager)
					return;
				super.addMouseMotionListener(l);
			}
		};
		table.setRowSelectionAllowed(false);
		table.setCellSelectionEnabled(true);
		table.setRowHeight(60);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
				if (event.getClickCount() == 2) {
					Point point = event.getPoint();
					int r = table.rowAtPoint(point);
					int c = table.columnAtPoint(point);
					if (!(r >= 0 && r < table.getRowCount() && c > 0 && c < table.getColumnCount()))
						return;
					
					Object value = table.getModel().getValueAt(r, c);
					if (!(value instanceof TesterCell))
						return;
					final TesterCell testerCell = (TesterCell) value;
					if (!testerCell.isSetResult() || testerCell.items.isEmpty())
						return;
					Item item = testerCell.items.get(testerCell.itemIndex);
					String url = item.href;
					try {
						Desktop.getDesktop().browse(new URI(url));
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with open url " + url);
					}
				}
			}
			
			@Override
			public void mouseReleased(final MouseEvent e) {
				Point point = e.getPoint();
				int r = table.rowAtPoint(point);
				int c = table.columnAtPoint(point);
				if (!(r >= 0 && r < table.getRowCount() && c > 0 && c < table.getColumnCount()))
					return;
				
				Object value = table.getModel().getValueAt(r, c);
				if (!(value instanceof TesterCell))
					return;
				final TesterCell testerCell = (TesterCell) value;
				if (!testerCell.isSetResult())
					return;
				if (!e.isPopupTrigger()) {
					Rectangle rect = table.getCellRect(r, c, true);
					if (testerCell.processClick(rect, point)) {
						((PlaceModel) table.getModel()).fireTableCellUpdated(testerCell.getRowIndex(), testerCell.getColIndex());
						return;
					}
					
				}
				if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
					JPopupMenu popup = new JPopupMenu();
					JMenuItem menuItem = new JMenuItem("Show Info");
					menuItem.setIcon(null);
					menuItem.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent ae) {
							ToolTipManager.sharedInstance().mouseMoved(e);
						}
					});
					popup.add(menuItem);
					menuItem = new JMenuItem("Open Url");
					menuItem.setIcon(null);
					menuItem.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent ae) {
							if (testerCell.items.isEmpty())
								return;
							Item item = testerCell.items.get(testerCell.itemIndex);
							String url = item.href;
							try {
								Desktop.getDesktop().browse(new URI(url));
							} catch (Exception ex) {
								LOG.logError(ex, "Problem with open url " + url);
							}
						}
					});
					popup.add(menuItem);
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}
			
			@Override
			public void mouseMoved(MouseEvent event) {
				Point point = event.getPoint();
				int r = table.rowAtPoint(point);
				int c = table.columnAtPoint(point);
				if (!(r >= 0 && r < table.getRowCount() && c > 0 && c < table.getColumnCount()))
					return;
				Object value = table.getModel().getValueAt(r, c);
				if (!(value instanceof TesterCell))
					return;
				final TesterCell testerCell = (TesterCell) value;
				if (!testerCell.isSetResult() || testerCell.items.size() < 2)
					return;
				Rectangle rect = table.getCellRect(r, c, true);
				Integer bn = testerCell.getButtonNumber(rect, point);
				table.setCursor(bn == null ? Cursor.getDefaultCursor() : handCursor);
			}
		});
		scrollPane.setViewportView(table);
		ToolTipManager.sharedInstance().unregisterComponent(table);
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(200, 1000));
		contentPane.add(panel, BorderLayout.WEST);
		panel.setLayout(null);
		
		checksScrollPane = new JScrollPane();
		checksScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		checksScrollPane.setBounds(10, 0, 180, 496);
		panel.add(checksScrollPane);
		
		panelChecks = new JPanel();
		checksScrollPane.setViewportView(panelChecks);
		panelChecks.setLayout(null);
		
		btnScan = new JButton("Scan");
		btnScan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doScan();
			}
		});
		btnScan.setBounds(10, 533, 180, 23);
		panel.add(btnScan);
		
		JButton btnExportAsXls = new JButton("Export as XLS");
		btnExportAsXls.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showXlsExportForm();
			}
		});
		btnExportAsXls.setBounds(10, 566, 180, 23);
		panel.add(btnExportAsXls);
		
		chckbxUseOldData = new JCheckBox("Use old data if exist");
		chckbxUseOldData.setBounds(10, 503, 184, 23);
		panel.add(chckbxUseOldData);
		buildMenu();
		init();
	}
	
	public void init() {
		registerTesters();
		int y = 5;
		for (SiteTester tester : siteTesters) {
			TesterJCheckBox chckbx = new TesterJCheckBox(tester);
			chckbx.setBounds(5, y, 160, 25);
			checks.add(chckbx);
			panelChecks.add(chckbx);
			y = y + 25;
		}
		panelChecks.setPreferredSize(new Dimension(170, y));
	}
	
	Thread scanThread;
	private JButton btnScan;
	private XlsExporter xlsExporter;
	private JCheckBox chckbxUseOldData;
	private JMenuBar menuBar;
	private JMenu mnViews;
	private JMenu mnSettings;
	private JMenuItem mntmCRMDataLoader;
	private JMenuItem mntmInventoryChanger;
	private JMenuItem mntmOracleConfig;
	private JMenuItem mntmSettings;
	
	private void doScan() {
		btnScan.setEnabled(false);
		scanThread = new Thread(new Runnable() {
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void run() {
				try {
					List<SiteTester> siteTesters = new ArrayList<SiteTester>();
					for (TesterJCheckBox checkBox : checks) {
						if (checkBox.isSelected()) {
							siteTesters.add(checkBox.tester);
						}
					}
					if (siteTesters.isEmpty()) {
						JOptionPane.showMessageDialog(Aquatica.this, "Not selected site bots");
						return;
					}
					CrmDataInfo crmData = CrmDataInfo.get();
					if (crmData == null) {
						File source = CrmDataInfo.lastSource();
						boolean noSource = source == null;
						int result = JOptionPane.showConfirmDialog(Aquatica.this, "Not set CRM data. \r\n " + "Yes - Load from File. " + (noSource ? "No - Skip Scan" : "No - Load from last file " + source.getName() + "\r\n Cancel - Skip Scan"), "Load Crm Data", (noSource ? JOptionPane.YES_NO_OPTION : JOptionPane.YES_NO_CANCEL_OPTION));
						if (result == JOptionPane.CANCEL_OPTION || (noSource && result == JOptionPane.NO_OPTION)) {
							LOG.logWarning("Not set CRM data. Canceled.");
							return;
						}
						
						if (result == JOptionPane.NO_OPTION) {
							LOG.log("Load from file " + source.getAbsolutePath());
							crmData = CrmDataInfo.loadFromFile(source);
						}
						if (result == JOptionPane.YES_OPTION) {
							LOG.log("Load from user File ");
							crmData = CrmDataInfo.loadFromFile();
						}
						CrmDataInfo.init(0, crmData);
					}
					
					List<Thread> botThreads = new ArrayList<Thread>(siteTesters.size());
					final CountDownLatch latch = new CountDownLatch(siteTesters.size());
					String[] columnNames = new String[siteTesters.size() + 1];
					columnNames[0] = "Product";
					int i = 1;
					for (final SiteTester tester : siteTesters) {
						columnNames[i++] = tester.toString();
						final String name = tester.toString() + " Bot";
						Thread botThread = new Thread(new Runnable() {
							
							@Override
							public void run() {
								AquaticaDriver mainDriver = DRIVER.getDriver();
								AquaticaDriver detailsDriver = DRIVER.getDetailsDriver();
								try {
									LOG.log("Start Working");
									tester.test(mainDriver, detailsDriver);
								} catch (Exception ex) {
									LOG.logError(ex, "Problem when BOT work");
								}
								DRIVER.freeMainDriver(mainDriver);
								DRIVER.freeDetailsDriver(detailsDriver);
								LOG.log("Finish Working");
								latch.countDown();
							}
						});
						botThread.setName("~" + name);
						botThread.setDaemon(true);
						botThreads.add(botThread);
					}
					final PlaceModel dataModel = new PlaceModel(columnNames);
					int rowIndex = 0;
					final Map<String, Object[]> rows = new HashMap<>();
					for (CrmDataRow row : crmData.getRows()) {
						Object[] rowData = new Object[siteTesters.size() + 1];
						int index = 0;
						rowData[index++] = row;
						for (int colIndex = 0; colIndex < siteTesters.size(); colIndex++) {
							rowData[index++] = new TesterCell(row, rowIndex, colIndex + 1);
						}
						rows.put(row.upc, rowData);
						dataModel.addRow(rowData);
						rowIndex++;
					}
					
					Collections.sort(dataModel.getDataVector(), new Comparator<Vector>() {
						@Override
						public int compare(Vector o1, Vector o2) {
							return ((CrmDataRow) o1.get(0)).compareTo((CrmDataRow) o2.get(0));
						}
					});
					
					table.setModel(dataModel);
					table.getColumnModel().getColumn(0).setPreferredWidth(100);
					for (int j = 0; j < siteTesters.size(); j++) {
						final int testerNumber = j;
						SiteTester tester = siteTesters.get(j);
						tester.addProcessListener(new ProcessResultListener(rows, dataModel, tester, testerNumber));
					}
					for (Thread thread : botThreads) {
						thread.start();
					}
					latch.await();
				} catch (Exception ex) {
					LOG.logError(ex, "Problem when scanner works");
				} finally {
					btnScan.setEnabled(true);
					scanThread = null;
				}
			}
		});
		
		scanThread.setDaemon(true);
		scanThread.start();
	}
	
	private void showXlsExportForm() {
		if (table.getModel().getRowCount() > 0) {
			if (xlsExporter == null) {
				xlsExporter = new XlsExporter();
			}
			xlsExporter.initData((PlaceModel) table.getModel());
			xlsExporter.setVisible(true);
		}
	}
	
	private void buildMenu() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnViews = new JMenu("Views");
		menuBar.add(mnViews);
		
		JMenuItem mntmLogWindow = new JMenuItem("Log Window");
		mntmLogWindow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				LogFrame frame = LOG.getLogFrame();
				if (frame != null) {
					frame.setVisible(true);
				}
			}
		});
		mnViews.add(mntmLogWindow);
		
		mntmCRMDataLoader = new JMenuItem("CRM Data Loader");
		mntmCRMDataLoader.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (crmDataFrame == null) {
						crmDataFrame = new CrmDataFrame();
					}
					crmDataFrame.setVisible(true);
				} catch (Exception ex) {
					LOG.logError(ex, "");
				}
			}
		});
		mnViews.add(mntmCRMDataLoader);
		
		mntmInventoryChanger = new JMenuItem("CommerceHub Inventory Changer");
		mntmInventoryChanger.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (commerceHubFrame == null) {
						commerceHubFrame = new CommerceHubFrame();
					}
					commerceHubFrame.setVisible(true);
				} catch (Exception ex) {
					LOG.logError(ex, "");
				}
			}
		});
		// mnViews.add(mntmInventoryChanger);
		
		mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);
		
		mntmOracleConfig = new JMenuItem("Oracle Config");
		mntmOracleConfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (oracleFrame == null) {
						oracleFrame = new OracleFrame();
					}
					oracleFrame.init();
					oracleFrame.setVisible(true);
				} catch (Exception ex) {
					LOG.logError(ex, "");
				}
			}
		});
		mnSettings.add(mntmOracleConfig);
		
		mntmSettings = new JMenuItem("Settings");
		mntmSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (settingsFrame == null) {
						settingsFrame = new SettingsFrame();
					}
					settingsFrame.init(Settings.getProperties());
					settingsFrame.setVisible(true);
				} catch (Exception ex) {
					LOG.logError(ex, "");
				}
			}
		});
		mnSettings.add(mntmSettings);
	}
}
