package com.Aquatica.window;

import java.awt.Color;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public enum SizeStatus {

	VALID(new EmptyBorder(0, 0, 0, 0)), INVALID(new LineBorder(new Color(200, 0, 0), 3));

	private Border border;

	private SizeStatus(Border border) {
		this.border = border;
	}
	
	public Border getBorder() {
		return border;
	}

}
