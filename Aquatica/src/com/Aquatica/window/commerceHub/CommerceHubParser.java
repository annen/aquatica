package com.Aquatica.window.commerceHub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.Aquatica.window.InventoryRow;
import com.Aquatica.window.LOG;
import com.Aquatica.window.SiteRowStatus;
import com.google.common.primitives.Longs;
import com.trustename.net.util.html.HtmlParserUtils;

public class CommerceHubParser {
	private static final Pattern patternBeginTable = Pattern.compile("<table class=\"searchresults\"[^>]*>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternRow = Pattern.compile("<tr(.*?)</tr>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final Pattern patternEditUrl = Pattern.compile(".*maybeGotoDetail\\('[^']+productId=(\\d+)&[^']+'\\).*", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MM/dd/yyyy");

	private final String html;

	public final Map<String, InventoryRow> active = new LinkedHashMap<String, InventoryRow>();
	public final Map<String, InventoryRow> discontinued = new LinkedHashMap<String, InventoryRow>();
	public final Map<String, InventoryRow> unavailable = new LinkedHashMap<String, InventoryRow>();

	public final Map<String, InventoryRow> known = new LinkedHashMap<String, InventoryRow>();
	public final List<InventoryRow> unknown = new ArrayList<InventoryRow>();

	public CommerceHubParser(String html) {
		this.html = html;
	}

	public void parse() throws Exception {
		active.clear();
		discontinued.clear();
		unavailable.clear();
		known.clear();
		unknown.clear();
		LOG.log("Parse report table");
		String table = HtmlParserUtils.extractArea(html, patternBeginTable, HtmlParserUtils.patternEndTable, "main table");
		LOG.log("Parse report rows");
		List<String> trs = HtmlParserUtils.parseTrs(table);
		trs.remove(0); // remove header
		List<String> trs_url = new ArrayList<String>();
		LOG.log("Found rows: " + trs.size());
		LOG.log("Parse Urls");
		Matcher rowMatcher = patternRow.matcher(table);
		while (rowMatcher.find()) {
			String tr = rowMatcher.group(1);
			Matcher urlMatcher = patternEditUrl.matcher(tr);
			if (urlMatcher.matches()) {
				trs_url.add(urlMatcher.group(1));
			}
		}
		LOG.log("Found urls: " + trs_url.size());
		if (trs_url.size() != trs.size()) {
			throw new RuntimeException("Expeced equal row size (Url<>Upc)");
		}
		LOG.log("Parse products");
		Iterator<String> iterator = trs_url.iterator();
		for (String tr : trs) {
			String url = iterator.next();
			List<String> tds = HtmlParserUtils.parseTds(tr);
			if (tds.isEmpty() || tds.get(0).contains("<a"))
				continue;
			InventoryRow row = parseRow(tds);
			row.info = url;
			if (!row.upc.startsWith("6277")) {
				unknown.add(row);
				continue;
			}
			known.put(row.upc, row);
			String status = prepareValue(tds.get(9).toLowerCase());
			switch (status) {
			case "available":
				row.siteStatus = SiteRowStatus.ACTIVE;
				active.put(row.upc, row);
				break;
			case "discontinued":
				row.siteStatus = SiteRowStatus.DISCONTINUED;
				discontinued.put(row.upc, row);
				break;
			case "unavailable":
			default:
				row.siteStatus = SiteRowStatus.UNAVAILABLE;
				unavailable.put(row.upc, row);
				break;
			}
		}
	}

	private InventoryRow parseRow(List<String> tds) {
		InventoryRow row = new InventoryRow();
		row.upc = prepareValue(tds.get(3));
		row.name = prepareValue(tds.get(1)) + " : " + prepareValue(tds.get(4));
		String count = prepareValue(tds.get(5));
		row.siteCount = Longs.tryParse(count);
		String expCount = prepareValue(tds.get(11).toLowerCase());
		row.expCount = Longs.tryParse(expCount);
		String expDate = prepareValue(tds.get(12).toLowerCase());
		if (!expDate.equalsIgnoreCase("N/A")) {
			row.expDate = dateFormat.parseLocalDate(expDate);
		}
		for (String td : tds) {
			row.row.add(prepareValue(td));
		}
		return row;
	}

	private String prepareValue(String td) {
		td = td.replace("\t", " ").replace("\r", " ").replace("\n", " ").trim();
		while (td.contains("  ")) {
			td = td.replace("  ", " ");
		}
		return td;
	}
}
