package com.Aquatica.window;

public enum RowStatus {
	ORIGINAL_ROW, FOUND_NEW_ROW, FOUND_OLD_ROW, DIFFERENT_COUNT, UNKNOWN
}
