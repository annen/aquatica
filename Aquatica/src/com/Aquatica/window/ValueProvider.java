package com.Aquatica.window;

public interface ValueProvider<T> {
	T getValue();
}
