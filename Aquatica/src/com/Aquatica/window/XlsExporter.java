package com.Aquatica.window;

import static java.util.Collections.singleton;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.AUTOMATIC;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.joda.time.DateTime;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.Utils;
import com.Aquatica.drivers.AjaxDriver;
import com.Aquatica.window.Aquatica.TesterCell;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class XlsExporter extends JFrame {
	private static class StyleParams {
		private short borderLeft = CellStyle.NO_FILL;
		private short color = IndexedColors.AUTOMATIC.getIndex();
		private boolean bold = false;
		
		public StyleParams(Short borderLeft, Short color, boolean bold) {
			this.borderLeft = borderLeft != null ? borderLeft.shortValue() : this.borderLeft;
			this.color = color != null ? color.shortValue() : this.color;
			this.bold = bold;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (bold ? 1231 : 1237);
			result = prime * result + borderLeft;
			result = prime * result + color;
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StyleParams other = (StyleParams) obj;
			if (bold != other.bold)
				return false;
			if (borderLeft != other.borderLeft)
				return false;
			if (color != other.color)
				return false;
			return true;
		}
	}
	
	private JPanel contentPane;
	private PlaceModel data;
	private JLabel lblRowsnumber;
	private JCheckBox chckbxDescriptionPages;
	private JCheckBox chckbxSummaryPage;
	private JCheckBox chckbxImages;
	private JTextArea txtrBots;
	private List<String> logs = new ArrayList<String>();
	
	public List<String> getLogs() {
		return logs;
	}
	
	public void setLogs(String logs) {
		this.logs.add(logs);
	}
	
	private LoadingCache<StyleParams, CellStyle> cache;
	
	private LoadingCache<StyleParams, CellStyle> createCache(final Workbook wb) {
		return CacheBuilder.newBuilder().maximumSize(4000).build(new CacheLoader<StyleParams, CellStyle>() {
			
			@Override
			public CellStyle load(StyleParams params) throws Exception {
				CellStyle style = wb.createCellStyle();
				style.setBorderLeft(params.borderLeft);
				style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
				if (params.color != AUTOMATIC.index) {
					style.setFillForegroundColor(params.color);
					style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				}
				if (params.bold) {
					org.apache.poi.ss.usermodel.Font font = wb.createFont();
					font.setBold(true);
					style.setFont(font);
					style.setAlignment(CellStyle.ALIGN_CENTER);
				}
				return style;
			}
		});
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					XlsExporter frame = new XlsExporter();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public XlsExporter() {
		setTitle("Export to XLS");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnExport = new JButton("Export");
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							doExport();
						} catch (Exception ex) {
							LOG.logError(ex, "Problem when save export file " + ex.getMessage());
						}
					}
				});
				t.setDaemon(true);
				t.start();
			}
			
		});
		contentPane.add(btnExport, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		chckbxSummaryPage = new JCheckBox("Summary Page");
		chckbxSummaryPage.setSelected(true);
		chckbxSummaryPage.setBounds(6, 7, 200, 23);
		panel.add(chckbxSummaryPage);
		
		chckbxDescriptionPages = new JCheckBox("Description Pages");
		chckbxDescriptionPages.setSelected(true);
		chckbxDescriptionPages.setBounds(6, 33, 200, 23);
		panel.add(chckbxDescriptionPages);
		
		chckbxImages = new JCheckBox("Images");
		chckbxImages.setBounds(6, 59, 200, 23);
		panel.add(chckbxImages);
		
		JLabel lblRowsNumberLabel = new JLabel("Rows Number");
		lblRowsNumberLabel.setBounds(242, 11, 81, 14);
		panel.add(lblRowsNumberLabel);
		
		lblRowsnumber = new JLabel("0");
		lblRowsNumberLabel.setLabelFor(lblRowsnumber);
		lblRowsnumber.setBounds(333, 11, 81, 14);
		panel.add(lblRowsnumber);
		
		JLabel lblBotsLabel = new JLabel("Bots");
		lblBotsLabel.setBounds(242, 37, 81, 14);
		panel.add(lblBotsLabel);
		
		txtrBots = new JTextArea();
		txtrBots.setTabSize(4);
		txtrBots.setFont(new Font("Monospaced", Font.PLAIN, 10));
		lblBotsLabel.setLabelFor(txtrBots);
		txtrBots.setBounds(242, 57, 172, 160);
		panel.add(txtrBots);
		
	}
	
	public void initData(PlaceModel model) {
		data = model;
		lblRowsnumber.setText(String.valueOf(data.getRowCount()));
		int columnCount = data.getColumnCount();
		StringBuilder sb = new StringBuilder();
		for (int column = 1; column < columnCount; column++) {
			String columnName = data.getColumnName(column);
			if (sb.length() > 0) {
				sb.append("\r\n");
			}
			sb.append(columnName);
		}
		txtrBots.setText(sb.toString());
	}
	
	private void doExport() throws Exception {
		FileDialog fileDialog = new FileDialog(this, "Select file to export", FileDialog.SAVE);
		String pref = "Aquatica_check_export";
		if (data.getColumnCount() == 2) {
			pref = data.getColumnName(1);
		}
		fileDialog.setFile(pref + "_" + new DateTime().toString("yyyy-MM-dd") + ".xls");
		fileDialog.setVisible(true);
		String filename = fileDialog.getFile();
		fileDialog.setVisible(false);
		
		File file = new File(fileDialog.getDirectory(), filename);
		if (file.exists()) {
			file.delete();
		}
		
		Workbook wb = new HSSFWorkbook();
		cache = createCache(wb);
		if (chckbxSummaryPage.isSelected()) {
			addSummarySheet(wb);
		}
		if (chckbxDescriptionPages.isSelected()) {
			addDetailsSheet(wb);
		}
		if (chckbxImages.isSelected()) {
			addImageSheet(wb);
		}
		addLogSheet(wb);
		
		FileOutputStream fileOut = new FileOutputStream(file);
		wb.write(fileOut);
		fileOut.close();
		
		int result = JOptionPane.showConfirmDialog(this, "Export complete. Open file?", "Export complete.", JOptionPane.YES_NO_OPTION);
		if (result == JOptionPane.YES_OPTION) {
			try {
				Desktop.getDesktop().open(file);
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with open file " + file.getAbsolutePath());
			}
		}
	}
	
	public void addLogSheet(Workbook wb) throws Exception {
		Collection<String> data = LogFrame.logsExporter.getLogs();
		Sheet sheet = wb.createSheet("Logs");
		int r = 0;
		for (String log : data) {
			Row row = sheet.createRow(r + 1);
			createCell(row, 1, log, "");
			short foreColor = IndexedColors.WHITE.index;
			if (log.contains("WARNING")) {
				foreColor = IndexedColors.LIGHT_ORANGE.index;
			}
			if (log.contains("ORACLE")) {
				foreColor = IndexedColors.SKY_BLUE.index;
			}
			if (log.contains("ERROR")) {
				foreColor = IndexedColors.CORAL.index;
			}
			if (log.contains("SUCCESS")) {
				foreColor = IndexedColors.LIGHT_GREEN.index;
			}
			CellStyle colorStyle = cache.get(new StyleParams(null, foreColor, false));
			row.getCell(1).setCellStyle(colorStyle);
			r++;
		}
		data.clear();
	}
	
	public void addSummarySheet(Workbook wb) throws ExecutionException {
		int rowCount = data.getRowCount();
		int columnCount = data.getColumnCount();
		Sheet sheet = wb.createSheet("Summary");
		{
			Row row = sheet.createRow(0);
			header(wb, row, 0, "Aquatica Values", IndexedColors.SKY_BLUE.index);
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
			for (int c = 1; c < columnCount; c++) {
				String columnName = data.getColumnName(c);
				int startI = 8 + ((c - 1) * 9) + c;
				header(wb, row, startI, columnName, (short) (40 + c));
				sheet.addMergedRegion(new CellRangeAddress(0, 0, startI, startI + 9));
			}
		}
		Set<Integer> urlColumns = new HashSet<Integer>();
		{
			int startI = 0;
			Row row = sheet.createRow(1);
			header(wb, row, startI++, "Status", IndexedColors.AQUA.index);
			header(wb, row, startI++, "UPC", IndexedColors.AQUA.index);
			header(wb, row, startI++, "SKU", IndexedColors.AQUA.index);
			header(wb, row, startI++, "Product Name", IndexedColors.AQUA.index);
			header(wb, row, startI++, "MAP", IndexedColors.AQUA.index);
			header(wb, row, startI++, "MSRP", IndexedColors.AQUA.index);
			header(wb, row, startI++, "Length", IndexedColors.AQUA.index);
			header(wb, row, startI++, "Width", IndexedColors.AQUA.index);
			header(wb, row, startI++, "Height", IndexedColors.AQUA.index);
			
			for (int c = 1; c < columnCount; c++) {
				short color = (short) (40 + c);
				header(wb, row, startI++, "Status", color);
				header(wb, row, startI++, "Items", color);
				header(wb, row, startI++, "Site SKU", color);
				header(wb, row, startI++, "Site Title", color);
				header(wb, row, startI++, "Price", color);
				header(wb, row, startI++, "QTY", color);
				header(wb, row, startI++, "Length", color);
				header(wb, row, startI++, "Width", color);
				header(wb, row, startI++, "Height", color);
				header(wb, row, startI++, "Url", color);
				urlColumns.add(startI - 1);
			}
			
		}
		for (int r = 0; r < rowCount; r++) {
			int startI = 0;
			CrmDataRow dataRow = (CrmDataRow) data.getValueAt(r, 0);
			SizeStatus rowSizeStatus = SizeStatus.VALID;
			PriceStatus rowPriceStatus = PriceStatus.EMPTY;
			Row row = sheet.createRow(r + 2);
			row.createCell(startI++).setCellValue("");
			createCell(row, startI++, dataRow.upc, "");
			createCell(row, startI++, dataRow.sku, "");
			createCell(row, startI++, dataRow.name, "");
			createCell(row, startI++, dataRow.map, 0.0);
			createCell(row, startI++, dataRow.msrp, 0.0);
			createCell(row, startI++, dataRow.length, "");
			createCell(row, startI++, dataRow.width, "");
			createCell(row, startI++, dataRow.height, "");
			for (int c = 1; c < columnCount; c++) {
				TesterCell cell = (TesterCell) data.getValueAt(r, c);
				List<Item> items = cell.getItems();
				if (items.size() > 0) {
					SizeStatus sizeStatus = Utils.getSizeStatus(items, dataRow);
					PriceStatus priceStatus = Utils.getPriceStatus(items, dataRow.map);
					int danger = priceStatus.getDanger();
					Item item = items.get(0);
					String status = "";
					if (!PriceStatus.VALID.equals(priceStatus)) {
						status += "Price: " + priceStatus + " ";
					}
					if (rowPriceStatus.getDanger() < danger) {
						rowPriceStatus = priceStatus;
					}
					if (!SizeStatus.VALID.equals(sizeStatus)) {
						status += "Size: " + sizeStatus;
						rowSizeStatus = sizeStatus;
						danger++;
					}
					status = status.trim();
					if (status.isEmpty()) {
						status = "VALID";
					}
					short foreColor = IndexedColors.LIGHT_GREEN.index;
					if (danger > 1) {
						foreColor = IndexedColors.LIGHT_YELLOW.index;
					}
					if (danger > 2) {
						foreColor = IndexedColors.LIGHT_ORANGE.index;
					}
					if (danger > 3) {
						foreColor = IndexedColors.CORAL.index;
					}
					createCell(row, startI++, status, "");
					CellStyle style = cache.get(new StyleParams(CellStyle.BORDER_THICK, foreColor, false));
					row.getCell(startI - 1).setCellStyle(style);
					int tStartI = startI;
					createCell(row, startI++, items.size(), 0);
					createCell(row, startI++, item.SKU, "");
					createCell(row, startI++, item.title, "");
					createCell(row, startI++, item.price, 0.0);
					createCell(row, startI++, item.qty, "");
					createCell(row, startI++, item.length, "");
					createCell(row, startI++, item.width, "");
					createCell(row, startI++, item.height, "");
					createCell(row, startI++, item.href, "");
					CellStyle colorStyle = cache.get(new StyleParams(null, foreColor, false));
					for (int i = tStartI; i < startI; i++) {
						row.getCell(i).setCellStyle(colorStyle);
					}
					
					if (items.size() > 1) {
						CreationHelper helper = wb.getCreationHelper();
						for (int i = tStartI + 1; i < startI; i++) {
							String commentText = "";
							int index = i - tStartI - 1;
							switch (index) {
							case 0:
								commentText = getComment(items, Item.SKUVP);
								break;
							case 1:
								commentText = getComment(items, Item.titleVP);
								break;
							case 2:
								commentText = getComment(items, Item.priceVP);
								break;
							case 3:
								commentText = getComment(items, Item.qtyVP);
								break;
							case 4:
								commentText = getComment(items, Item.lengthVP);
								break;
							case 5:
								commentText = getComment(items, Item.widthVP);
								break;
							case 6:
								commentText = getComment(items, Item.heightVP);
								break;
							case 7:
								commentText = getComment(items, Item.hrefVP);
								break;
							default:
								commentText = "";
								break;
							}
							Cell cellI = row.getCell(i);
							Drawing drawing = sheet.createDrawingPatriarch();
							
							ClientAnchor anchor = helper.createClientAnchor();
							anchor.setCol1(cellI.getColumnIndex() - 1);
							anchor.setCol2(cellI.getColumnIndex() + 2);
							anchor.setRow1(row.getRowNum());
							anchor.setRow2(row.getRowNum() + items.size() + 2);
							
							Comment comment = drawing.createCellComment(anchor);
							comment.setString(helper.createRichTextString(commentText));
							cellI.setCellComment(comment);
						}
					}
				} else {
					Cell firstCell = row.createCell(startI++);
					CellStyle style = cache.get(new StyleParams(CellStyle.BORDER_THICK, null, false));
					firstCell.setCellStyle(style);
					firstCell.setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
					row.createCell(startI++).setCellValue("");
				}
			}
			
			String status = "";
			if (rowPriceStatus.getDanger() > 1) {
				status += "Price: " + rowPriceStatus + " ";
			}
			if (!SizeStatus.VALID.equals(rowSizeStatus)) {
				status += "Size: " + rowSizeStatus;
			}
			status = status.trim();
			row.getCell(0).setCellValue(status);
		}
		int r = rowCount + 2;
		sheet.createRow(r++);
		for (Entry<SiteTester, List<Item>> discontinuedItems : data.discontinuedItems.entrySet()) {
			for (Item item : discontinuedItems.getValue()) {
				Row row = sheet.createRow(r++);
				int startI = 0;
				createCell(row, startI++, "DISCONTINUED:" + discontinuedItems.getKey(), "");
				createCell(row, startI++, item.UPC, "");
				createCell(row, startI++, item.SKU, "");
				createCell(row, startI++, item.title, "");
				createCell(row, startI++, item.price, "");
				createCell(row, startI++, item.qty, "");
				createCell(row, startI++, item.length, "");
				createCell(row, startI++, item.width, "");
				createCell(row, startI++, item.height, "");
				createCell(row, startI++, item.href, "");
				CellStyle cellStyle = cache.get(new StyleParams(null, IndexedColors.ROSE.index, false));
				for (int i = 0; i < startI; i++) {
					row.getCell(i).setCellStyle(cellStyle);
				}
			}
		}
		
		for (int i = 0; i < (columnCount * 10); i++) {
			if (!urlColumns.contains(i)) {
				sheet.autoSizeColumn(i);
			}
		}
	}
	
	private void createCell(Row row, int i, Object value, Object defaultValue) {
		if (value == null) {
			createCell(row, i, defaultValue, "");
			return;
		}
		if (value instanceof String) {
			row.createCell(i).setCellValue((String) value);
		} else if (value instanceof Number) {
			row.createCell(i).setCellValue(((Number) value).doubleValue());
		} else if (value instanceof Boolean) {
			row.createCell(i).setCellValue((Boolean) value);
		} else {
			row.createCell(i).setCellValue(String.valueOf(value));
		}
	}
	
	private String getComment(List<Item> items, ItemValueProvider vp) {
		StringBuilder sb = new StringBuilder();
		for (Item item : items) {
			if (sb.length() > 0) {
				sb = sb.append("\r\n");
			}
			String value = vp.getValue(item);
			sb.append(value != null ? value : "");
		}
		return sb.toString();
	}
	
	private void header(Workbook wb, Row row, int i, String caption, Short color) throws ExecutionException {
		Cell cell = row.createCell(i);
		CellStyle style = cache.get(new StyleParams(null, color, true));
		cell.setCellStyle(style);
		cell.setCellValue(caption);
	}
	
	public void addDetailsSheet(Workbook wb) throws Exception {
		int columnCount = data.getColumnCount();
		for (int column = 1; column < columnCount; column++) {
			String columnName = data.getColumnName(column);
			Sheet sheet = wb.createSheet(columnName);
			int startI = 0;
			Row row = sheet.createRow(0);
			short color = (short) (40 + column);
			
			header(wb, row, startI++, "Status", color);
			header(wb, row, startI++, "UPC", color);
			header(wb, row, startI++, "Site SKU", color);
			header(wb, row, startI++, "Product Name", color);
			header(wb, row, startI++, "Site Title", color);
			header(wb, row, startI++, "Map", color);
			header(wb, row, startI++, "Site Price", color);
			header(wb, row, startI++, "Diff", color);
			header(wb, row, startI++, "QTY", color);
			header(wb, row, startI++, "Valid Length", color);
			header(wb, row, startI++, "Site Length", color);
			header(wb, row, startI++, "Valid Width", color);
			header(wb, row, startI++, "Site Width", color);
			header(wb, row, startI++, "Valid Height", color);
			header(wb, row, startI++, "Site Height", color);
			header(wb, row, startI++, "Url", color);
			int recRow = 1;
			for (int r = 0; r < data.getRowCount(); r++) {
				CrmDataRow dataRow = (CrmDataRow) data.getValueAt(r, 0);
				TesterCell cell = (TesterCell) data.getValueAt(r, column);
				List<Item> items = cell.getItems();
				for (Item item : items) {
					startI = 0;
					row = sheet.createRow(recRow++);
					SizeStatus sizeStatus = Utils.getSizeStatus(singleton(item), dataRow);
					PriceStatus priceStatus = Utils.getPriceStatus(singleton(item), dataRow.map);
					int danger = priceStatus.getDanger();
					String status = "";
					if (!PriceStatus.VALID.equals(priceStatus)) {
						status += "Price: " + priceStatus + " ";
					}
					if (!SizeStatus.VALID.equals(sizeStatus)) {
						status += "Size: " + sizeStatus;
						danger++;
					}
					status = status.trim();
					if (status.isEmpty()) {
						status = "VALID";
					}
					short foreColor = IndexedColors.LIGHT_GREEN.index;
					if (danger > 1) {
						foreColor = IndexedColors.LIGHT_YELLOW.index;
					}
					if (danger > 2) {
						foreColor = IndexedColors.LIGHT_ORANGE.index;
					}
					if (danger > 3) {
						foreColor = IndexedColors.CORAL.index;
					}
					createCell(row, startI++, status, "");
					createCell(row, startI++, item.UPC, "");
					createCell(row, startI++, item.SKU, "");
					createCell(row, startI++, dataRow.name, "");
					createCell(row, startI++, item.title, "");
					createCell(row, startI++, dataRow.map, 0.0);
					createCell(row, startI++, item.price, 0.0);
					row.createCell(startI++).setCellFormula("G" + recRow + "-F" + recRow);
					createCell(row, startI++, item.qty, "");
					createCell(row, startI++, dataRow.length, "");
					createCell(row, startI++, item.length, "");
					createCell(row, startI++, dataRow.width, "");
					createCell(row, startI++, item.width, "");
					createCell(row, startI++, dataRow.height, "");
					createCell(row, startI++, item.height, "");
					if (item.href.length() < 255) {
						row.createCell(startI++).setCellFormula("HYPERLINK(\"" + item.href + "\", \"" + item.href + "\")");
					}
					
					CellStyle colorStyle = cache.get(new StyleParams(null, foreColor, items.size() > 1));
					for (int i = 0; i < startI; i++) {
						row.getCell(i).setCellStyle(colorStyle);
					}
				}
			}
			
			for (int i = 0; i < 16; i++) {
				sheet.autoSizeColumn(i);
			}
		}
	}
	
	private void addImageSheet(Workbook wb) throws Exception {
		AddDimensionedImage addDimensionedImage = new AddDimensionedImage();
		AjaxDriver driver = DRIVER.getAjaxDriver();
		int columnCount = data.getColumnCount();
		for (int column = 1; column < columnCount; column++) {
			String columnName = data.getColumnName(column);
			HSSFSheet sheet = (HSSFSheet) wb.createSheet(columnName + "_images");
			int startI = 0;
			Row row = sheet.createRow(0);
			header(wb, row, startI++, "UPC", (Short) null);
			header(wb, row, startI++, "Product Name", (Short) null);
			int recRow = 1;
			for (int r = 0; r < data.getRowCount(); r++) {
				TesterCell cell = (TesterCell) data.getValueAt(r, column);
				List<Item> items = cell.getItems();
				for (Item item : items) {
					startI = 0;
					row = sheet.createRow(recRow++);
					row.createCell(startI++).setCellValue(item.UPC);
					row.createCell(startI++).setCellValue(item.title);
					Collection<String> images = item.images;
					for (String image : images) {
						try {
							LOG.logInfo("Download #" + (startI - 1) + " " + item.title + " from " + image);
							byte[] imageFile = driver.download(image);
							addDimensionedImage.addImageToSheet(new Character((char) ('A' + startI)).toString() + recRow, sheet, imageFile, 20, 20, AddDimensionedImage.EXPAND_ROW_AND_COLUMN, image);
							startI++;
						} catch (Exception ex) {
							row.createCell(startI++).setCellValue(ex.getMessage());
							LOG.logError(ex, "Problem when save image file " + image);
						}
					}
				}
			}
			for (int i = 0; i < 2; i++) {
				sheet.autoSizeColumn(i);
			}
		}
		DRIVER.freeAjaxDriver(driver);
	}
}
