package com.Aquatica.window;

public enum SiteRowStatus {
	ACTIVE, DISCONTINUED, UNAVAILABLE;
}
