package com.Aquatica.window;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import com.Aquatica.UPCResolver;
import com.Aquatica.UPCResolver.Resolver;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

public class OracleFrame extends JFrame {
	private static final Splitter splitter = Splitter.on('\n').trimResults();
	
	public static class MultiLineTableCellRenderer extends DefaultTableCellRenderer {
		
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int col) {
				
			if (col == 1 || col == 7) {
				String string = value.toString();
				if (string.contains("\n")) {
					value = string.replace("\n", "   ");
				}
			}
			Component c = super.getTableCellRendererComponent(table, value,
					isSelected, hasFocus, row, col);
			return c;
		}
	}
	
	public static class MultiLineTableCellEditor extends DefaultCellEditor implements TableCellEditor {
		
		JTextPane component = new JTextPane();
		JScrollPane scrollPane = new JScrollPane();
		boolean used = false;
		int row;
		int height;
		JTable table;
		
		public MultiLineTableCellEditor() {
			super(new JTextField());
			scrollPane.setViewportView(component);
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
				int rowIndex, int colIndex) {
			if (colIndex == 1 || colIndex == 7) {
				component.setText((String) value);
				row = rowIndex;
				height = table.getRowHeight(rowIndex);
				this.table = table;
				table.setRowHeight(rowIndex, Math.max(38, splitter.splitToList(value.toString()).size() * 17));
				used = true;
				return scrollPane;
			}
			return super.getTableCellEditorComponent(table, value, isSelected, rowIndex, colIndex);
		}
		
		@Override
		public Object getCellEditorValue() {
			if (used == true) {
				used = false;
				table.setRowHeight(row, height);
				return component.getText().trim();
			}
			return super.getCellEditorValue();
		}
	}
	
	private JPanel contentPane;
	private JTable table;
	private JTextField txtUpc;
	private JTextField txtKey;
	private JTextField txtPrice;
	private JTextField txtSizel;
	private JTextField txtSizew;
	private JTextField txtSizeh;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					OracleFrame frame = new OracleFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public OracleFrame() {
		setTitle("Oracle Config");
		setBounds(100, 100, 1000, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable() {
			@Override
			public String getToolTipText(MouseEvent e) {
				String tip = null;
				java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				
				try {
					tip = getValueAt(rowIndex, colIndex).toString();
					if (!tip.contains("\n"))
						return "";
					return "<html>" + tip.replaceAll("\n", "<br>") + "</html>";
				} catch (RuntimeException e1) {
				}
				return tip;
			}
			
		};
		table.setModel(new DefaultTableModel(
				new Object[][] {
						{ null, null, null, null, null, null, null, null },
				},
				new String[] {
						"UPC", "SKU", "Key", "Price", "Length", "Width", "Height", "Keywords"
				}) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
					String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class
			};
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(7).setPreferredWidth(100);
		table.setDefaultRenderer(String.class, new MultiLineTableCellRenderer());
		table.setDefaultEditor(String.class, new MultiLineTableCellEditor());
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectedRow = table.getSelectedRow();
				if (selectedRow >= 0) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					int i = selectedRow;
					String upc = model.getValueAt(i, 0).toString();
					if (Strings.isNullOrEmpty(upc))
						return;
					String sku = model.getValueAt(i, 1).toString();
					String key = model.getValueAt(i, 2).toString();
					String price = model.getValueAt(i, 3).toString();
					String length = model.getValueAt(i, 4).toString();
					String width = model.getValueAt(i, 5).toString();
					String height = model.getValueAt(i, 6).toString();
					String titles = model.getValueAt(i, 7).toString();
					txtUpc.setText(upc);
					txtpnSkus.setText(sku);
					txtKey.setText(key);
					txtPrice.setText(price);
					txtSizel.setText(length);
					txtSizew.setText(width);
					txtSizeh.setText(height);
					txtpnKeywords.setText(titles);
				}
			}
		});
		
		scrollPane.setViewportView(table);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		txtUpc = new JTextField();
		txtUpc.setColumns(10);
		
		txtKey = new JTextField();
		txtKey.setColumns(10);
		
		txtPrice = new JTextField();
		txtPrice.setColumns(10);
		
		txtSizel = new JTextField();
		txtSizel.setColumns(10);
		
		txtSizew = new JTextField();
		txtSizew.setColumns(10);
		
		txtSizeh = new JTextField();
		txtSizeh.setColumns(10);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		JLabel lblUpc = new JLabel("UPC");
		
		JLabel lblKey = new JLabel("Key");
		
		JLabel lblPrice = new JLabel("Price");
		
		JLabel lblLength = new JLabel("Length");
		
		JLabel lblWidth = new JLabel("Width");
		
		JLabel lblHeight = new JLabel("Height");
		
		JLabel lblKeywords = new JLabel("Keywords =>");
		
		JButton btnAddSave = new JButton("Add / Save");
		btnAddSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String upc = txtUpc.getText();
				if (Strings.isNullOrEmpty(upc))
					return;
				String sku = txtpnSkus.getText().replace("\r", "");
				String key = txtKey.getText();
				String price = txtPrice.getText();
				String length = txtSizel.getText();
				String width = txtSizew.getText();
				String height = txtSizeh.getText();
				String titles = txtpnKeywords.getText().replace("\r", "");
				Resolver resolver = Resolver.buildResolver(upc, sku, key, price, length, width, height, titles);
				
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				for (int i = 0; i < model.getRowCount(); i++) {
					if (model.getValueAt(i, 0).toString().equalsIgnoreCase(resolver.getUpc())) {
						model.setValueAt(resolver.getUpc(), i, 0);
						model.setValueAt(joiner.join(resolver.getSkus()), i, 1);
						model.setValueAt(Strings.nullToEmpty(resolver.getKey()), i, 2);
						model.setValueAt(Strings.nullToEmpty(resolver.getPrice() == null ? "" : resolver.getPrice().toString()), i, 3);
						model.setValueAt(Strings.nullToEmpty(resolver.getSizeL()), i, 4);
						model.setValueAt(Strings.nullToEmpty(resolver.getSizeW()), i, 5);
						model.setValueAt(Strings.nullToEmpty(resolver.getSizeH()), i, 6);
						model.setValueAt(joiner.join(resolver.getTitles()), i, 7);
						return;
					}
				}
				addRow(model, resolver);
			}
		});
		
		JScrollPane scrollPane_2 = new JScrollPane();
		
		JLabel lblSku = new JLabel("Skus =>");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
										.addGroup(gl_panel.createSequentialGroup()
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(txtUpc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(lblUpc))
												.addGap(8))
										.addGroup(gl_panel.createSequentialGroup()
												.addComponent(lblSku)
												.addPreferredGap(ComponentPlacement.RELATED)))
								.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
										.addGroup(gl_panel.createSequentialGroup()
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(txtKey, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(lblKey))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(txtPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(lblPrice))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblLength)
														.addComponent(txtSizel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(txtSizew, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(lblWidth))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblHeight)
														.addComponent(txtSizeh, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addComponent(lblKeywords))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnAddSave, GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
								.addContainerGap()));
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createSequentialGroup()
												.addContainerGap()
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_panel.createSequentialGroup()
																.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
																		.addComponent(lblUpc)
																		.addComponent(lblKey)
																		.addComponent(lblPrice)
																		.addComponent(lblLength)
																		.addComponent(lblWidth)
																		.addComponent(lblHeight))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
																		.addComponent(txtUpc, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
																		.addComponent(txtKey, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(txtPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(txtSizel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(txtSizew, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(txtSizeh, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(ComponentPlacement.UNRELATED)
																.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																		.addComponent(lblSku)
																		.addComponent(lblKeywords)))
														.addComponent(scrollPane_1, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
														.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)))
										.addGroup(gl_panel.createSequentialGroup()
												.addGap(37)
												.addComponent(btnAddSave)))
								.addContainerGap()));
								
		txtpnSkus = new JTextPane();
		scrollPane_2.setViewportView(txtpnSkus);
		
		txtpnKeywords = new JTextPane();
		scrollPane_1.setViewportView(txtpnKeywords);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Resolver> newResolvers = new ArrayList<Resolver>();
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				for (int i = 0; i < model.getRowCount(); i++) {
					String upc = model.getValueAt(i, 0).toString();
					if (upc.isEmpty()) {
						continue;
					}
					String sku = model.getValueAt(i, 1).toString();
					String key = model.getValueAt(i, 2).toString();
					String price = model.getValueAt(i, 3).toString();
					String length = model.getValueAt(i, 4).toString();
					String width = model.getValueAt(i, 5).toString();
					String height = model.getValueAt(i, 6).toString();
					String titles = model.getValueAt(i, 7).toString();
					Resolver resolver = Resolver.buildResolver(upc, sku, key, price, length, width, height, titles);
					newResolvers.add(resolver);
				}
				UPCResolver.instance().resetResolvers(newResolvers);
				Settings.flush();
				setVisible(false);
			}
		});
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				OracleFrame.this.setVisible(false);
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap(120, Short.MAX_VALUE)
								.addComponent(btnOk, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addContainerGap()));
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(btnExit)
										.addComponent(btnOk))
								.addContainerGap()));
		panel_1.setLayout(gl_panel_1);
	}
	
	private static final Joiner joiner = Joiner.on('\n').useForNull("");
	private JTextPane txtpnKeywords;
	private JTextPane txtpnSkus;
	
	public void init() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		List<Resolver> resolvers = new ArrayList<Resolver>(UPCResolver.instance().getResolvers());
		Collections.sort(resolvers, new Comparator<Resolver>() {
			
			@Override
			public int compare(Resolver o1, Resolver o2) {
				if (o1 == UPCResolver.DEFAULT)
					return 100;
				if (o2 == UPCResolver.DEFAULT)
					return 100;
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		for (Resolver resolver : resolvers) {
			if (resolver != null) {
				addRow(model, resolver);
			}
		}
	}
	
	public void addRow(DefaultTableModel model, Resolver resolver) {
		model.addRow(new String[] {
				resolver.getUpc(),
				joiner.join(resolver.getSkus()),
				Strings.nullToEmpty(resolver.getKey()),
				Strings.nullToEmpty(resolver.getPrice() == null ? "" : resolver.getPrice().toString()),
				Strings.nullToEmpty(resolver.getSizeL()),
				Strings.nullToEmpty(resolver.getSizeW()),
				Strings.nullToEmpty(resolver.getSizeH()),
				joiner.join(resolver.getTitles()) });
	}
}
