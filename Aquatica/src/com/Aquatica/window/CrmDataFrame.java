package com.Aquatica.window;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class CrmDataFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnAddInventory;
	private JButton btnSelectFile;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					CrmDataFrame frame = new CrmDataFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CrmDataFrame() {
		setTitle("CRM Data");
		setBounds(100, 100, 1200, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		btnSelectFile = new JButton("Select file");
		btnSelectFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				progressBar.setVisible(true);
				DefaultTableModel dataModel = new DefaultTableModel(new String[] { "UPC", "SKU", "Name", "MAP", "MSRP", "Length", "Width", "Height" }, 0);
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new File(Settings.get(Settings.DIR_DEFAULT)));
				fc.setDialogTitle("Select Data File (ex. Aquatica active products for import.xls)");
				if (fc.showOpenDialog(CrmDataFrame.this) == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					if (!file.exists() || file.isDirectory() || (!file.getName().toLowerCase().endsWith("xls") && !file.getName().toLowerCase().endsWith("xlsx"))) {
						LOG.logError("Invalid File");
						JOptionPane.showMessageDialog(CrmDataFrame.this, "Invalid File");
						return;
					}
					CrmDataInfo info = CrmDataInfo.loadFromFile(file);
					CrmDataInfo.init(0, info);
					for (CrmDataRow row : info.getRows()) {
						dataModel.addRow(new Object[] { row.upc, row.sku, row.name.replace("Aquatica ", ""), row.map, row.msrp, row.length, row.width, row.height });
					}
				}
				table.setModel(dataModel);
				table.getColumnModel().getColumn(0).setPreferredWidth(100);
				table.getColumnModel().getColumn(1).setPreferredWidth(100);
				table.getColumnModel().getColumn(2).setPreferredWidth(350);
				btnAddInventory.setEnabled(dataModel.getRowCount() > 0);
				progressBar.setVisible(false);
			}
		});

		final JButton btnLoadFromCrm = new JButton("Load from CRM");
		btnLoadFromCrm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnSelectFile.setEnabled(false);
				btnLoadFromCrm.setEnabled(false);
				progressBar.setVisible(true);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						final DefaultTableModel dataModel = new DefaultTableModel(new String[] { "UPC", "SKU", "Name", "MAP", "MSRP", "Length", "Width", "Height" }, 0);
						CrmDataInfo info = CrmDataInfo.loadFromNet();
						CrmDataInfo.init(0, info);
						for (CrmDataRow row : info.getRows()) {
							dataModel.addRow(new Object[] { row.upc, row.sku, row.name.replace("Aquatica ", ""), row.map, row.msrp, row.length, row.width, row.height });
						}
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								btnLoadFromCrm.setEnabled(true);
								table.setModel(dataModel);
								table.getColumnModel().getColumn(0).setPreferredWidth(100);
								table.getColumnModel().getColumn(1).setPreferredWidth(100);
								table.getColumnModel().getColumn(2).setPreferredWidth(350);
								btnAddInventory.setEnabled(dataModel.getRowCount() > 0);
								btnSelectFile.setEnabled(true);
								progressBar.setVisible(false);
							}
						});
					}
				});
				t.setDaemon(true);
				t.start();
			}
		});

		btnAddInventory = new JButton("Add Inventory");
		btnAddInventory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				progressBar.setVisible(true);
				CrmDataInfo info = CrmDataInfo.get();
				if (info == null) {
					JOptionPane.showMessageDialog(CrmDataFrame.this, "Select Data File first");
					return;
				}
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Select INVENTORY File (ex. ItemRemain.xlsx)");
				fc.setCurrentDirectory(new File(Settings.get(Settings.DIR_DEFAULT)));
				int returnVal = fc.showOpenDialog(CrmDataFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					if (!file.exists() || file.isDirectory() || (!file.getName().toLowerCase().endsWith("xls") && !file.getName().toLowerCase().endsWith("xlsx"))) {
						JOptionPane.showMessageDialog(CrmDataFrame.this, "Invalid inventory File");
						LOG.logWarning("Invalid inventory File");
						return;
					}
					LOG.log("Open inventory file " + file.getAbsolutePath());
					try {
						Map<String, InventoryRow> inventoryFile = InventoryFileReader.readFile(file, false);
						List<CrmDataRow> rows = info.getRows();
						for (CrmDataRow row : rows) {
							InventoryRow irow = inventoryFile.get(row.upc);
							row.iRow = irow;
						}
						DefaultTableModel dataModel = new DefaultTableModel(new String[] { "UPC", "SKU", "Name", "MAP", "MSRP", "Length", "Width", "Height", "Count", "Lead Time" }, 0);
						for (CrmDataRow row : info.getRows()) {
							dataModel.addRow(new Object[] {
									row.upc,
									row.sku,
									row.name.replace("Aquatica ", ""),
									row.map,
									row.msrp,
									row.length,
									row.width,
									row.height,
									row.iRow == null ? "" : row.iRow.count,
									row.iRow == null ? "" : row.iRow.info,
							});
						}
						table.setModel(dataModel);
						table.getColumnModel().getColumn(0).setPreferredWidth(100);
						table.getColumnModel().getColumn(1).setPreferredWidth(100);
						table.getColumnModel().getColumn(2).setPreferredWidth(350);
					} catch (Exception ex) {
						LOG.logError(ex, "");
					}
					progressBar.setVisible(false);
				}
			}
		});
		btnAddInventory.setEnabled(false);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(btnSelectFile, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnLoadFromCrm, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnAddInventory, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(437, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(btnSelectFile)
										.addComponent(btnLoadFromCrm)
										.addComponent(btnAddInventory))
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		scrollPane.setViewportView(table);

		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setMaximum(10);
		progressBar.setValue(2);
		progressBar.setVisible(false);
		contentPane.add(progressBar, BorderLayout.SOUTH);
	}

}
