package com.Aquatica.window;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;

public class PlaceModel extends DefaultTableModel {
	
	public final Map<SiteTester, List<Item>> discontinuedItems = new LinkedHashMap<SiteTester, List<Item>>();
	public final Map<Integer, TestResult> results = new LinkedHashMap<Integer, TestResult>();
	
	public PlaceModel(String[] columnNames) {
		super(columnNames, 0);
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public void addDiscontinuedItem(SiteTester tester, Item item) {
		List<Item> items = discontinuedItems.get(tester);
		if (items == null) {
			items = new ArrayList<Item>();
			discontinuedItems.put(tester, items);
		}
		items.add(item);
	}

	public void addTestResults(Integer testerNum, TestResult result) {
		results.put(testerNum, result);
	}

	public Map<Integer, TestResult> getResults() {
		return results;
	}
	
	public List<Item> getDiscontinuedItems(SiteTester tester) {
		List<Item> list = discontinuedItems.get(tester);
		if (list == null)
			return Collections.emptyList();
		return list;
	}
}