package com.Aquatica.window;

public class CrmDataRow {
	public String upc = "";
	public String sku = "";
	public String name = "";
	public Double map = 0.0;
	public Double msrp = 0.0;
	public Double length = 0.0;
	public Double width = 0.0;
	public Double height = 0.0;
	public InventoryRow iRow;
	
	@Override
	public String toString() {
		return upc + "\t" + sku + "\t" + name + "\t" + map + "\t" + msrp + "\t" + length + "\t" + width + "\t" + height;
	}
	
	public int compareTo(CrmDataRow arg0) {
		if (arg0 == null)
			return 1;
		return name.compareTo(arg0.name);
	}
}
