package com.Aquatica.window;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

public class InventoryRow {
	public static class Purpose {

		public SiteRowStatus newSiteStatus;
		public Long newCount;
		public Long needCount;
		public boolean remoteExp;
		public boolean setExp;

		public boolean isChanged() {
			return newSiteStatus != null || newCount != null || remoteExp || setExp;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if (newSiteStatus == SiteRowStatus.ACTIVE) {
				sb.append("Need Activate ");
			}
			if (newSiteStatus == SiteRowStatus.DISCONTINUED) {
				sb.append("Need Discont. ");
			}
			if (remoteExp) {
				sb.append("Remove Next");
			}
			if (setExp) {
				sb.append("Add Next");
			}
			if (newCount != null) {
				if (sb.length() > 0) {
					sb.append(" ");
				}
				sb.append("Change Count:" + newCount);
			}
			return sb.toString();
		}
	}

	public String upc;
	public RowStatus status;
	public Long count = 0L;
	public Long oldCount = 0L;
	public String name;
	public String info;
	public List<Object> row = new ArrayList<Object>();
	public Long siteCount = 0L;
	public SiteRowStatus siteStatus;
	public Long expCount;
	public LocalDate expDate;

	@Override
	public String toString() {
		return count + "\t" + upc + "\t" + name + "\t" + info;
	}

	public Purpose getPurpose() {
		Purpose pur = new Purpose();
		boolean needActive = count > 2 && status != RowStatus.UNKNOWN && status != RowStatus.FOUND_OLD_ROW;
		// if (siteStatus != SiteRowStatus.ACTIVE && needActive) {
		// pur.newSiteStatus = SiteRowStatus.ACTIVE;
		// } else
		if (siteStatus == SiteRowStatus.UNAVAILABLE) {
			pur.newSiteStatus = needActive ? SiteRowStatus.ACTIVE : SiteRowStatus.DISCONTINUED;
		}
		if (needActive) {
			if (expDate != null) {
				pur.remoteExp = true;
			}
		} else {
			if (siteStatus == SiteRowStatus.ACTIVE) {
				if (expDate == null || expDate.minusDays(5).isBefore(new LocalDate())) {
					pur.setExp = true;
				}
			}
		}
		if (!needActive && siteStatus == SiteRowStatus.DISCONTINUED && expDate != null) {
			pur.remoteExp = true;
		}
		pur.needCount = 0L;
		if (siteStatus != SiteRowStatus.DISCONTINUED) {
			if (needActive) {
				pur.needCount = count;
				if (pur.needCount < 6) {
					pur.needCount = 5L;
				}
			}
		}
		if (!siteCount.equals(pur.needCount)) {
			pur.newCount = pur.needCount;
		}
		return pur;
	}
}
