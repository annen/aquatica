package com.Aquatica.window;

import com.Aquatica.Item;

public interface ItemValueProvider {
	String getValue(Item item);
}