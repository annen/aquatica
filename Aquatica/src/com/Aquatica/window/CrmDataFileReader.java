package com.Aquatica.window;

import static com.Aquatica.Utils.tryParseDouble;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.util.Strings;

import com.google.common.base.Charsets;

public class CrmDataFileReader {

	public static final Pattern patternRow = Pattern.compile("<tr[^>]*/>|<(tr)[^>]*>(.*?)</tr[^>]*>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	public static final Pattern patternCell = Pattern.compile("<t[dh]+[^>]*/>|<(t[dh]+)[^>]*>(.*?)</\\1[^>]*>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	public static final Pattern patternRandomTag = Pattern.compile("<\\w+[^>]*/>|<(\\w+)[^>]*>(.*?)</\\1[^>]*>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	public static CrmDataInfo readFile(File xlsxFile) throws Exception {
		if (!xlsxFile.exists() || !xlsxFile.isFile())
			return new CrmDataInfo();
		try (FileInputStream fis = new FileInputStream(xlsxFile)) {
			String string = IOUtils.toString(fis, Charsets.UTF_16LE);
			if (string.contains("<tr"))
				return readHtml(string);
		}
		try (FileInputStream fis = new FileInputStream(xlsxFile)) {
			try (XSSFWorkbook workbook = new XSSFWorkbook(fis)) {
				XSSFSheet mySheet = workbook.getSheetAt(0);
				Collection<Collection<Object>> jSheet = new ArrayList<Collection<Object>>();
				Iterator<Row> rowIterator = mySheet.rowIterator();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					ArrayList<Object> rowData = new ArrayList<Object>();
					jSheet.add(rowData);
					Iterator<Cell> cellIterator = row.cellIterator();
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						rowData.add(getObject(cell));
					}
				}
				return parse(jSheet);
			}
		}
	}

	public static CrmDataInfo readHtml(String html) {
		Collection<Collection<Object>> jSheet = new ArrayList<Collection<Object>>();
		Matcher matcher = patternRow.matcher(html);
		while (matcher.find()) {
			ArrayList<Object> rowData = new ArrayList<Object>();
			String rowValue = Strings.isNullOrEmpty(matcher.group(2)) ? "" : matcher.group(2);
			Matcher cellMatcher = patternCell.matcher(rowValue);
			while (cellMatcher.find()) {
				String value = Strings.isNullOrEmpty(cellMatcher.group(2)) ? "" : cellMatcher.group(2);
				Matcher randomTagMatcher = patternRandomTag.matcher(value);
				while (randomTagMatcher.find()) {
					value = Strings.isNullOrEmpty(randomTagMatcher.group(2)) ? "" : randomTagMatcher.group(2);
					randomTagMatcher = patternRandomTag.matcher(value);
				}
				value = value.replaceAll("&#\\d\\d\\d\\d;", "");
				value = value.trim();
				String s = value.replace("  ", " ");
				while (value.length() != s.length()) {
					value = s;
					s = value.replace("  ", " ");
				}
				rowData.add(value);
			}
			jSheet.add(rowData);
		}
		return parse(jSheet);
	}

	private static CrmDataInfo parse(Collection<Collection<Object>> mySheet) {
		CrmDataInfo info = new CrmDataInfo();
		Iterator<Collection<Object>> rowIterator = mySheet.iterator();
		int rowNumber = 0;
		int upc = -1;
		int name = -1;
		int sku = -1;
		int map = -1;
		int msrp = -1;
		int length = -1;
		int width = -1;
		int height = -1;
		
		while (rowIterator.hasNext()) {
			Collection<Object> row = rowIterator.next();
			Iterator<Object> cellIterator = row.iterator();
			int cellNumber = 0;
			if (rowNumber == 0) {
				while (cellIterator.hasNext()) {
					Object o = cellIterator.next();
					String columnName = String.valueOf(o).trim();
					if ("UPC Code".equalsIgnoreCase(columnName) || "UPC".equalsIgnoreCase(columnName)) {
						upc = cellNumber;
					} else if ("Vendor Part Number".equalsIgnoreCase(columnName) || "SKU".equalsIgnoreCase(columnName)) {
						sku = cellNumber;
					} else if ("Product Name".equalsIgnoreCase(columnName) || "Name".equalsIgnoreCase(columnName)) {
						name = cellNumber;
					} else if ("MAP Price".equalsIgnoreCase(columnName) || "MAP Price, USD".equalsIgnoreCase(columnName) || "MAP".equalsIgnoreCase(columnName) || "MAP Price, EUR".equalsIgnoreCase(columnName)) {
						map = cellNumber;
					} else if ("MSRP Price, USD".equalsIgnoreCase(columnName) || "Full Retail Price (MSRP)".equalsIgnoreCase(columnName) || "MSRP".equalsIgnoreCase(columnName) || "MSRP Price, EUR".equalsIgnoreCase(columnName)) {
						msrp = cellNumber;
					} else if ("Exterior Product Length".equalsIgnoreCase(columnName)) {
						length = cellNumber;
					} else if ("Exterior Product Width".equalsIgnoreCase(columnName)) {
						width = cellNumber;
					} else if ("Exterior Product Height".equalsIgnoreCase(columnName)) {
						height = cellNumber;
					}
					cellNumber++;
				}
				rowNumber++;
				continue;
			}
			
			if (upc < 0) {
				JOptionPane.showMessageDialog(null, "Headers in file are not found. \n Check file. First line must have headers");
				return info;
			}
			CrmDataRow data = new CrmDataRow();
			while (cellIterator.hasNext()) {
				Object o = cellIterator.next();
				if (cellNumber == upc) {
					data.upc = (o instanceof Number) ? String.valueOf(((Number) o).longValue()) : String.valueOf(o);
				} else if (cellNumber == sku) {
					data.sku = String.valueOf(o);
				} else if (cellNumber == name) {
					data.name = String.valueOf(o).replace(" (main)", "");
				} else if (cellNumber == map) {
					data.map = tryParseDouble(o, 0.0);
				} else if (cellNumber == msrp) {
					data.msrp = tryParseDouble(o, 0.0);
				} else if (cellNumber == length) {
					data.length = tryParseDouble(o, 0.0);
				} else if (cellNumber == width) {
					data.width = tryParseDouble(o, 0.0);
				} else if (cellNumber == height) {
					data.height = tryParseDouble(o, 0.0);
				}
				cellNumber++;
			}
			if (!data.upc.isEmpty()) {
				info.addRow(data);
			}
			rowNumber++;
		}
		return info;
	}

	private static Object getObject(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		case Cell.CELL_TYPE_NUMERIC:
			return new Double(cell.getNumericCellValue()).doubleValue();
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();
		default:
			return "";
		}
	}
}