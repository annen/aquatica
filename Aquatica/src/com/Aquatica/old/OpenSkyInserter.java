package com.Aquatica.old;

import static com.Aquatica.DRIVER.*;

import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.DRIVER;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

public class OpenSkyInserter {
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Splitter subListSplitter = Splitter.on("~~~").trimResults();

	public static void main(String[] args) throws Exception {
		File dataFile = new File("./OPENSKY_DATA.txt");
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		List<String> lines = Files.readLines(dataFile, Charsets.UTF_8);
		int columnId = 0;
		Map<String, List<String>> rows = new LinkedHashMap<String, List<String>>();
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		Integer UPC_COLUMN_INDEX = 0;
		Integer WEIGHT_COLUMN_INDEX = columns.get("WEIGHT");
		Integer LENGTH_COLUMN_INDEX = columns.get("LENGTH");
		Integer WIDTH_COLUMN_INDEX = columns.get("WIDTH");
		Integer HEIGHT_COLUMN_INDEX = columns.get("HEIGHT");
		Integer COUNT_COLUMN_INDEX = columns.get("COUNT");
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			ArrayList<String> dataList = new ArrayList<String>(splitter.splitToList(line));
			if (dataList.size() != columns.size()) {
				System.err.println("NOT EQUAL COLUMN SIZE");
			}
			String upc = dataList.get(UPC_COLUMN_INDEX);
			if (rows.containsKey(upc)) {
				System.err.println("DUPLICATE UPC");
			}
			rows.put(upc, dataList);
		}
		AquaticaDriver mainDriver = getDriver();
		AquaticaDriver detailsDriver = getDetailsDriver();
		try {
			mainDriver.manage().window().setPosition(new Point(0, 0));
			java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			mainDriver.manage().window().setSize(new Dimension(screenSize.width / 2, screenSize.height - 40));
			detailsDriver.manage().window().setPosition(new Point(screenSize.width / 2, 0));
			detailsDriver.manage().window().setSize(new Dimension(screenSize.width / 2, screenSize.height - 40));
			mainDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			detailsDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

			mainDriver.get("https://www.opensky.com/login");
			mainDriver.findElementById("email").sendKeys("joe@aquaticausa.com");
			mainDriver.findElementById("password").sendKeys("8725password1");
			mainDriver.findElementById("auth-sign-in-button").click();
			Thread.sleep(3000);
			detailsDriver.get("https://www.opensky.com/login");
			detailsDriver.findElementById("email").sendKeys("joe@aquaticausa.com");
			detailsDriver.findElementById("password").sendKeys("8725password1");
			detailsDriver.findElementById("auth-sign-in-button").click();
			Thread.sleep(3000);
			mainDriver.get("https://www.opensky.com/aquatica/merchant/products");
			Thread.sleep(3000);

			boolean first = true;
			int i = 0;
			int minI = 0;
			while (!mainDriver.findElementsByXPath("//a[.='Next']").isEmpty()) {
				if (!first) {
					mainDriver.findElementsByXPath("//a[.='Next']").get(0).click();
					Thread.sleep(1000);
				}
				first = false;
				for (WebElement editLink : mainDriver.findElementsByXPath("//div[@class='edit edited']/a")) {
					i++;
					String href = editLink.getAttribute("href");
					System.out.println(i + " " + href);
					if (i < minI) {
						continue;
					}
					detailsDriver.get(href);
					Thread.sleep(1000);
					List<WebElement> upcElems = detailsDriver.findElementsByXPath("//div[@class='ship-detail'][contains(.,'6277')]");
					if (upcElems.size() != 1) {
						System.err.println("BAD UPC. MANUAL CHECK FOR ITEM" + i + " " + href);
						continue;
					}
					String upc = upcElems.get(0).getText();
					List<String> row = rows.get(upc);
					if (row == null) {
						System.err.println("Not Found ROW ??? " + href);
						continue;
					}
					List<WebElement> check = detailsDriver.findElementsByXPath("//div[@class='ship-detail'][contains(.,'14 days')]");
					if (check.size() == 1) {
						System.out.println("Found 14 days");
					}
					detailsDriver.findElementsByXPath("//h3[@class='subsubtitle']/a").get(3).click();
					Thread.sleep(2000);
					WebElement textArea = detailsDriver.findElementById("supplier_sellable_returnPolicy");
					detailsDriver.executeScript("arguments[0].parentNode.parentNode.className='';", textArea);
					Thread.sleep(1000);
					Select select = new Select(textArea);
					select.selectByIndex(0);
					Thread.sleep(1000);
					// WebElement input = detailsDriver.findElementById("supplier_sellable_weight");
					// input.clear();
					// input.sendKeys(row.get(WEIGHT_COLUMN_INDEX));
					//
					// input = detailsDriver.findElementById("supplier_sellable_length");
					// input.clear();
					// input.sendKeys(row.get(LENGTH_COLUMN_INDEX));
					//
					// input = detailsDriver.findElementById("supplier_sellable_width");
					// input.clear();
					// input.sendKeys(row.get(WIDTH_COLUMN_INDEX));
					//
					// input = detailsDriver.findElementById("supplier_sellable_height");
					// input.clear();
					// input.sendKeys(row.get(HEIGHT_COLUMN_INDEX));

					detailsDriver.findElementByXPath("//button[@name='review']").click();
					Thread.sleep(1000);
					check = detailsDriver.findElementsByXPath("//div[@class='ship-detail'][contains(.,'14 days')]");
					if (check.size() != 1) {
						System.err.println("Not found 14 days");
						System.err.println(i + " " + href);
					}
					// detailsDriver.findElementsByXPath("//h3[@class='subsubtitle']/a").get(2).click();
					// Thread.sleep(1000);
					// input = detailsDriver.findElementById("supplier_sellable_quantity");
					// input.clear();
					// input.sendKeys(row.get(COUNT_COLUMN_INDEX));
					// detailsDriver.findElementByXPath("//button[@name='review']").click();
					// Thread.sleep(3000);
				}
			}
		} finally {
			DRIVER.freeMainDriver(mainDriver);
			DRIVER.freeDetailsDriver(detailsDriver);
		}
	}

	private static void addText(String key, String text, AquaticaDriver mainDriver) throws InterruptedException {
		mainDriver.executeScript("arguments[0].click();", mainDriver.findElementByXPath("//a[@id='" + key + "']"));
		Thread.sleep(2000);
		if (!mainDriver.findElementByXPath("//div[contains(@class,'ChangeLanguagePopup ')]").getCssValue("display").equals("none")) {
			mainDriver.findElementByXPath("//div[contains(@class,'ChangeLanguagePopup ')]//button[.='Save']").click();
			Thread.sleep(3000);
		}
		text = text.replace("'", "`").replace("~~~", "<br />");
		List<WebElement> removeButton = mainDriver.findElementsByXPath("//button[@class='removeparagraph']");
		if (removeButton.size() == 1) {
			mainDriver.findElementByXPath("//button[@class='addparagraph']").click();
		}
		WebElement head = mainDriver.findElementsByXPath("//div[@class='head']").get(1);
		mainDriver.executeScript("arguments[0].innerHTML = arguments[1];", head, "");
		WebElement field = mainDriver.findElementsByXPath("//div[@class='textitem']").get(1);
		mainDriver.executeScript("arguments[0].innerHTML = arguments[1];", field, text);
	}

	private static void addRow(Map<String, Integer> columns, List<List<String>> data, int addedIndex, AquaticaDriver mainDriver) throws InterruptedException {
		String windowHandle = mainDriver.getWindowHandle();
		List<String> row = data.get(addedIndex);
		mainDriver.findElementsByXPath("//div[contains(@class,'blue_add_box')]").get(0).click();
		Thread.sleep(1000);
		// Add Images
		List<String> images = subListSplitter.splitToList(row.get(columns.get("IMAGES")));
		addImages(images, mainDriver);
		// Add Url
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'En')]").sendKeys(row.get(columns.get("EN_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'De')]").sendKeys(row.get(columns.get("DE_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'Fr')]").sendKeys(row.get(columns.get("FR_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'Es')]").sendKeys(row.get(columns.get("ES_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'It')]").sendKeys(row.get(columns.get("IT_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		// Add Model
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//input[contains(@id,'marque')]").sendKeys(row.get(columns.get("NAME")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);

		mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//span").get(2).click();
		Thread.sleep(3000);
		Iterator<String> iterator = mainDriver.getWindowHandles().iterator();
		iterator.next();
		mainDriver.switchTo().window(iterator.next());
		// Add Def
		mainDriver.findElementByXPath("//input[@name='keywordEn']").sendKeys(row.get(columns.get("EN_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordDe']").sendKeys(row.get(columns.get("DE_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordFr']").sendKeys(row.get(columns.get("FR_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordEs']").sendKeys(row.get(columns.get("SP_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordIt']").sendKeys(row.get(columns.get("IT_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@value='Save']").click();
		Thread.sleep(2000);
		mainDriver.switchTo().window(windowHandle);

		addText("En", row.get(columns.get("EN_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("De", row.get(columns.get("DE_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("Fr", row.get(columns.get("FR_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("Es", row.get(columns.get("ES_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("It", row.get(columns.get("IT_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		do {
			Thread.sleep(1000);
		} while (mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]").size() > 0);
		Thread.sleep(1000);
	}

	private static void addImages(List<String> images, AquaticaDriver mainDriver) throws InterruptedException {
		mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//span").get(0).click();
		Thread.sleep(1000);
		List<WebElement> fields = mainDriver.findElementsByXPath("//li[@class='input-upload-link']//input");
		int fieldIndex = 0;
		for (int i = 0; i < images.size(); i++) {
			for (int x = 0; x < fields.get(fieldIndex).getText().length(); x++) {
				fields.get(fieldIndex).sendKeys("\b");
			}
			fields.get(fieldIndex).sendKeys(images.get(i));
			if (fieldIndex == 5) {
				fieldIndex = 0;
				WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
				uploadButton.click();
				do {
					Thread.sleep(1000);
				} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
				Thread.sleep(1000);
			} else {
				fieldIndex++;
			}
		}
		if (fieldIndex > 0) {
			fieldIndex = 0;
			WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
			uploadButton.click();
			do {
				Thread.sleep(1000);
			} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
			Thread.sleep(1000);
		}
		mainDriver.findElementsByXPath("//a[contains(@class,'ui-dialog-titlebar-close')]").get(1).click();
		Thread.sleep(1000);
	}
}
