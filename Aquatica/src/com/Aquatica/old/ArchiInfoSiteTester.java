package com.Aquatica.old;

import static com.Aquatica.DRIVER.getDetailsDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.SiteTester.ProcessListener;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Splitter;

public class ArchiInfoSiteTester implements SiteTester {
	private static final String UPC_START = "6";
	
	private static final int MAX_ITEMS = 500;
	// private static final String MAIN_PAGE = "http://www.aquaticabagno.it/all";
	// private static final String MAIN_PAGE = "http://www.aquaticabano.com/all";
	// private static final String MAIN_PAGE = "http://www.aquaticabain.fr/all";
	// private static final String MAIN_PAGE = "http://www.aquaticabad.de/all";
	// private static final String MAIN_PAGE = "http://www.aquaticausa.com/all";
	
	private static final String MAIN_PAGE = "http://www.aquaticausa.com/waste-overflow-kit";
	private static final ArchiInfoSiteTester INSTANCE = new ArchiInfoSiteTester();
	private static final Splitter splitter = Splitter.on(',').trimResults();
	private static final Collection<String> allowedUpc = Arrays.asList("627722001759", "627722001766", "627722002374", "627722002336", "627722000011", "627722000035", "627722000882", "627722001971", "627722002916", "627722002572", "627722002565", "627722002060", "627722002107", "627722002923", "627722002114",
			"627722002046", "627722002404", "627722002053", "627722002299", "627722002190", "627722002398", "627722002237", "627722002411", "627722000028", "627722002749", "627722001032", "627722002077", "627722001834", "627722001865", "627722002084", "627722001803", "627722002381", "627722002930",
			"627722002947", "627722000448", "627722000462", "627722000622", "627722000639", "627722000653", "627722000738", "627722000981", "627722001438", "627722000400", "627722002756", "627722002350", "627722002367", "627722001940", "627722001162", "627722000592", "627722001568", "627722001025", "627722002619");
			
	public static final ArchiInfoSiteTester instance() {
		return INSTANCE;
	}
	
	private ArchiInfoSiteTester() {
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./EN_" + getClass().getSimpleName() + ".source.txt");
		if (f.exists())
			return result.loadFrom(f, ItemExt.class);
		mainDriver.get(MAIN_PAGE);
		List<WebElement> divs = mainDriver.findElements(By.xpath("//div[@class='name']/a"));
		List<String> urls = new ArrayList<String>(divs.size());
		for (WebElement a : divs) {
			urls.add(a.getAttribute("href"));
		}
		for (String href : urls) {
			result.foundItems.add(href);
			System.out.println("\t INFO: FOUND ITEM -> " + href + " [" + result.foundItems.size() + "]");
			for (ItemExt item : getDetails(href, href, detailsDriver)) {
				result.addItem(item);
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		result.saveTo(f);
		return result;
	}
	
	private Collection<ItemExt> getDetails(String href, String searchTitle, AquaticaDriver detailsDriver) {
		Collection<ItemExt> result = new ArrayList<ItemExt>();
		detailsDriver.get(href);
		List<WebElement> same = detailsDriver.findElementsById("same-product");
		List<String> urls = new ArrayList<String>();
		if (!same.isEmpty()) {
			Select select = new Select(same.get(0));
			List<WebElement> options = select.getOptions();
			for (WebElement option : options) {
				String value = option.getAttribute("value");
				if (!value.contains(UPC_START)) {
					continue;
				}
				urls.add(value);
			}
		}
		ItemExt item = parseItem(href, detailsDriver);
		result.add(item);
		for (String url : urls) {
			detailsDriver.get(url);
			item = parseItem(url, detailsDriver);
			result.add(item);
		}
		return result;
	}
	
	private ItemExt parseItem(String href, AquaticaDriver detailsDriver) {
		String upc = href;
		upc = upc.substring(upc.indexOf(UPC_START), upc.indexOf("-", upc.indexOf(UPC_START)));
		// if (!allowedUpc.contains(upc)) {
		// System.err.println("\t WARNING: NOT FOUND ALLOWED UPC -> " + upc + " " + href);
		// }
		List<String> upcs = new ArrayList<String>(splitter.splitToList(upc));
		if (upcs.size() == 0) {
			System.err.println("\t WARNING: NOT FOUND UPC CODES -> " + upc);
		}
		ItemExt item = new ItemExt();
		item.href = href;
		item.title = detailsDriver.findElementByClassName("heading_title").getText();
		item.price = tryParsePrice(getPrice());
		item.UPC = upc;
		item.qty = 0;
		String text = detailsDriver.findElement(By.xpath("//div[@id='tab-description']")).getText();
		text = text.replace("\"", "�").replace("\r", "");
		List<WebElement> lis = detailsDriver.findElements(By.xpath("//div[@id='tab-description']//li"));
		if (lis.size() > 0) {
			String text2 = lis.get(0).getText();
			int index = text.indexOf(text2);
			if (index > 0) {
				text = text.substring(0, index) + "� " + text.substring(index).replace("\n", "\n� ");
			}
		}
		// text = "\"" + text + "\"";
		item.info = text;
		List<WebElement> img = detailsDriver.findElements(By.xpath("//div[@class='fotorama__nav-wrap']//img[@class='fotorama__img']"));
		if (img.isEmpty()) {
			img = detailsDriver.findElements(By.xpath("//img[@class='fotorama__img']"));
		}
		if (img.isEmpty()) {
			System.err.println("\t WARNING: NOT FOUND IMAGES " + href);
		}
		for (WebElement i : img) {
			item.images.add(i.getAttribute("src"));
		}
		return item;
	}
	
	private String getPrice() {
		WebElement priceElement = getDetailsDriver().findElement(By.xpath("//div[contains(@class,'price-new')]"));
		return priceElement.getText();
	}
	
	private Double tryParsePrice(String testPrice) {
		try {
			testPrice = testPrice.replace("$", "");
			testPrice = testPrice.replace("�", "");
			testPrice = testPrice.replace(",", "");
			testPrice = testPrice.replace(" ", "");
			double price = Double.parseDouble(testPrice);
			return price;
		} catch (Exception ex) {
			return 0.0;
		}
	}
	
	@Override
	public String toString() {
		return "Aquatica sites";
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		// TODO Auto-generated method stub
		
	}
}
