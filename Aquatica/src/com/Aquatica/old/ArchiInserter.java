package com.Aquatica.old;

import static com.Aquatica.DRIVER.getDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.Aquatica.DRIVER;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

public class ArchiInserter {
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Splitter subListSplitter = Splitter.on("~~~").trimResults();
	
	public static void main(String[] args) throws Exception {
		File dataFile = new File("./ARCHIDATA.txt");
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		List<List<String>> data = new ArrayList<List<String>>();
		List<String> lines = Files.readLines(dataFile, Charsets.UTF_8);
		int columnId = 0;
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			ArrayList<String> dataList = new ArrayList<String>(splitter.splitToList(line));
			if (dataList.size() != columns.size()) {
				System.err.println("NOT EQUAL COLUMN SIZE");
			}
			data.add(dataList);
		}
		AquaticaDriver mainDriver = getDriver();
		try {
			mainDriver.get("http://www.archiexpo.com/restricted/FicheSociete.php?s=68046&w=239814U&droit=CLIENT");
			mainDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			Thread.sleep(1000);
			for (int addedIndex = 31; addedIndex < data.size(); addedIndex++) {
				addRow(columns, data, addedIndex, mainDriver);
				System.err.println("ADDED " + addedIndex);
			}
		} finally {
			DRIVER.freeMainDriver(mainDriver);
		}
	}
	
	private static void addRow(Map<String, Integer> columns, List<List<String>> data, int addedIndex, AquaticaDriver mainDriver) throws InterruptedException {
		String windowHandle = mainDriver.getWindowHandle();
		List<String> row = data.get(addedIndex);
		mainDriver.findElementsByXPath("//div[contains(@class,'blue_add_box')]").get(0).click();
		Thread.sleep(1000);
		// Add Images
		List<String> images = subListSplitter.splitToList(row.get(columns.get("IMAGES")));
		addImages(images, mainDriver);
		// Add Url
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'En')]").sendKeys(row.get(columns.get("EN_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'De')]").sendKeys(row.get(columns.get("DE_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'Fr')]").sendKeys(row.get(columns.get("FR_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'Es')]").sendKeys(row.get(columns.get("ES_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//input[@class='idpLien'][contains(@id,'It')]").sendKeys(row.get(columns.get("IT_URL")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		// Add Model
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//input[contains(@id,'marque')]").sendKeys(row.get(columns.get("NAME")));
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]").click();
		Thread.sleep(1000);
		
		mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//span").get(2).click();
		Thread.sleep(3000);
		Iterator<String> iterator = mainDriver.getWindowHandles().iterator();
		iterator.next();
		mainDriver.switchTo().window(iterator.next());
		// Add Def
		mainDriver.findElementByXPath("//input[@name='keywordEn']").sendKeys(row.get(columns.get("EN_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordDe']").sendKeys(row.get(columns.get("DE_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordFr']").sendKeys(row.get(columns.get("FR_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordEs']").sendKeys(row.get(columns.get("SP_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@name='keywordIt']").sendKeys(row.get(columns.get("IT_DEF")).replace("'", "`"));
		mainDriver.findElementByXPath("//input[@value='Save']").click();
		Thread.sleep(2000);
		mainDriver.switchTo().window(windowHandle);
		
		addText("En", row.get(columns.get("EN_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("De", row.get(columns.get("DE_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("Fr", row.get(columns.get("FR_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("Es", row.get(columns.get("ES_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		addText("It", row.get(columns.get("IT_TEXT")), mainDriver);
		mainDriver.switchTo().window(windowHandle);
		do {
			Thread.sleep(1000);
		} while (mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]").size() > 0);
		Thread.sleep(1000);
	}
	
	private static void addText(String key, String text, AquaticaDriver mainDriver) throws InterruptedException {
		text = text.replace("'", "`").replace("~~~", "\n");
		while (text.length() > 1650) {
			text = text.substring(0, text.lastIndexOf('\n'));
		}
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//td[@class='iconTxt']//img[contains(@id,'" + key + "')]").click();
		Thread.sleep(3000);
		Iterator<String> iterator = mainDriver.getWindowHandles().iterator();
		String handle = iterator.next();
		while (iterator.hasNext()) {
			handle = iterator.next();
		}
		mainDriver.switchTo().window(handle);
		Thread.sleep(1000);
		mainDriver.findElementsByXPath("//textArea").get(0).sendKeys(text);
		mainDriver.findElementByXPath("//img[@alt='Enregistrer']").click();
		Thread.sleep(2000);
	}
	
	private static void addImages(List<String> images, AquaticaDriver mainDriver) throws InterruptedException {
		mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//span").get(0).click();
		Thread.sleep(1000);
		List<WebElement> fields = mainDriver.findElementsByXPath("//li[@class='input-upload-link']//input");
		int fieldIndex = 0;
		for (int i = 0; i < images.size(); i++) {
			for (int x = 0; x < fields.get(fieldIndex).getText().length(); x++) {
				fields.get(fieldIndex).sendKeys("\b");
			}
			fields.get(fieldIndex).sendKeys(images.get(i));
			if (fieldIndex == 5) {
				fieldIndex = 0;
				WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
				uploadButton.click();
				do {
					Thread.sleep(1000);
				} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
				Thread.sleep(1000);
			} else {
				fieldIndex++;
			}
		}
		if (fieldIndex > 0) {
			fieldIndex = 0;
			WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
			uploadButton.click();
			do {
				Thread.sleep(1000);
			} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
			Thread.sleep(1000);
		}
		mainDriver.findElementsByXPath("//a[contains(@class,'ui-dialog-titlebar-close')]").get(1).click();
		Thread.sleep(1000);
	}
}
