package com.Aquatica.old;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.window.LOG;

public class HomeDepotOldSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	private static final String AQUATICA = "Aquatica";
	private static final String MAIN_PAGE = "http://www.homedepot.com/b/Bath/Aquatica/N-5yc1vZbzb3Zdse";
	// private static final String MAIN_PAGE = "http://www.homedepot.com/b/Aquatica/N-5yc1vZbzb3Zdse?Ns=P_REP_PRC_MODE|0&style=List#";//
	private static final HomeDepotOldSiteTester INSTANCE = new HomeDepotOldSiteTester();
	
	public static final HomeDepotOldSiteTester instance() {
		return INSTANCE;
	}
	
	private HomeDepotOldSiteTester() {
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (f.exists())
			return result.loadFrom(f);
		mainDriver.get(MAIN_PAGE);
		
		String urlNextPage;
		do {
			WebElement resultsElement = mainDriver.findElementById("products");
			List<WebElement> divs = resultsElement.findElements(By.xpath("div[contains(@class,'product')]"));
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//a[contains(@class,'item_description')]"));
				String href = a.getAttribute("href");
				String title = a.getText();
				if (!title.toLowerCase().contains(AQUATICA.toLowerCase())) {
					System.err.println("\t WARNING: BAD BRAND -> " + title);
					continue;
				}
				result.foundItems.add(title);
				System.out.println("\t INFO: FOUND ITEM -> " + title + " [" + result.foundItems.size() + "]");
				for (Item item : getDetails(href, title, detailsDriver)) {
					result.addItem(item);
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
			urlNextPage = null;
			List<WebElement> nextElement = mainDriver.findElements(By.xpath("//a[@title='Next']"));
			if (!nextElement.isEmpty()) {
				if (nextElement.size() != 1) {
					LOG.logWarning("Expected only one next page button");
				}
				urlNextPage = nextElement.get(0).getAttribute("href");
				mainDriver.get(urlNextPage);
			}
		} while (urlNextPage != null);
		result.saveTo(f);
		return result;
	}
	
	private Collection<Item> getDetails(String href, String searchTitle, AquaticaDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		detailsDriver.get(href);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		WebElement div = detailsDriver.findElementById("productinfo_ctn");
		String model = div.findElement(By.xpath(".//h2[contains(@class,'modelNo')]")).getText().trim();
		model = model.replace("Model # ", "");
		String realTitle = div.findElement(By.xpath(".//h1[@class='product_title']")).getText();
		realTitle = model + " " + realTitle;
		String testPrice = div.findElement(By.xpath(".//span[@itemprop='price']")).getText();
		boolean mustBeMorePrice = testPrice.contains("-") || tryParsePrice(testPrice) == null;
		boolean hasOptions = false;
		boolean hasSelect = false;
		List<WebElement> options = div.findElements(By.xpath(".//div[@class='grid_15']//select"));
		if (!options.isEmpty()) {
			hasOptions = true;
			hasSelect = true;
		}
		if (!mustBeMorePrice && !hasOptions) {
			Item item = new Item();
			item.href = href;
			item.title = realTitle;
			item.price = tryParsePrice(testPrice);
			item.UPC = getUPC(item, detailsDriver);
			result.add(item);
			return result;
		} else {
			System.err.println("\t ERROR: NOT EXPECTED STATE");
			if (!hasSelect) {
				System.err.println("\t ERROR: NOT FOUND SELECT");
				return result;
			}
			if (options.size() != 1) {
				System.err.println("\t ERROR: EXPECTED ONE SELECT ->" + options.size());
				return result;
			}
			detailsDriver.executeScript("arguments[0].setAttribute('style', 'display:block')", options.get(0).findElement(By.xpath("..")));
			Collection<String> selectedStrings = new ArrayList<String>();
			Select select = new Select(options.get(0));
			for (WebElement option : select.getOptions()) {
				String value = option.getAttribute("value");
				if (value == null || value.trim().length() == 0) {
					value = "0";
				}
				if ("0".equals(value.trim())) {
					continue;
				}
				selectedStrings.add(option.getText());
			}
			for (String text : selectedStrings) {
				select.selectByVisibleText(text);
				testPrice = div.findElement(By.xpath(".//div[@id='divPrice']")).getText();
				Item item = new Item();
				item.href = href;
				item.title = realTitle + " -> " + text;
				item.price = tryParsePrice(testPrice);
				item.UPC = getUPC(item, detailsDriver);
				result.add(item);
				select.deselectByVisibleText(text);
			}
			return result;
		}
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		String upc = (String) detailsDriver.executeScript("return THD.PIP.products.primary.data.info.upc");
		if (upc != null && upc.startsWith("627722"))
			return upc;
		upc = (String) detailsDriver.executeScript("return CI_ItemUPC");
		if (upc != null && upc.startsWith("627722"))
			return upc;
		System.err.println("\t WARNING: NOT FOUND UPC -> " + upc);
		List<WebElement> tds = detailsDriver.findElementsByXPath("div[@id='specsContainer']/table[1]//td");
		StringBuilder sb = new StringBuilder();
		for (WebElement td : tds) {
			sb.append(td.getText()).append(" ");
		}
		item.resolveUPC(sb.toString(), Arrays.asList(upc));
		return upc;
	}
	
	private Double tryParsePrice(String testPrice) {
		try {
			testPrice = testPrice.replace("$", "");
			testPrice = testPrice.replace(",", "");
			testPrice = testPrice.replace(" ", "");
			double price = Double.parseDouble(testPrice);
			return price;
		} catch (Exception ex) {
			return null;
		}
	}
	
	@Override
	public String toString() {
		return "HomeDepot.com";
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		// TODO Auto-generated method stub
		
	}
}
