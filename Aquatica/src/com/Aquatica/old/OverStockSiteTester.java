package com.Aquatica.old;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.SiteTester.ProcessListener;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Splitter;

public class OverStockSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	private static final String AQUATICA = "Aquatica";
	private static final String MAIN_PAGE = "http://www.overstock.com/Home-Garden/Aquatica,/brand,/1/store.html?sort=Lowest%20Price";
	private static final OverStockSiteTester INSTANCE = new OverStockSiteTester();
	private static final Splitter splitter = Splitter.on(',').trimResults();

	public static final OverStockSiteTester instance() {
		return INSTANCE;
	}

	private OverStockSiteTester() {

	}

	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (f.exists())
			return result.loadFrom(f);
		mainDriver.get(MAIN_PAGE);
		for (int i = 0; i < 4; i++) {
			mainDriver.findElementById("ft").sendKeys(Keys.chord(Keys.CONTROL, Keys.END));
			Thread.sleep(500);
		}
		List<WebElement> divs = mainDriver.findElements(By.xpath("//li[@class='product']"));
		for (WebElement div : divs) {
			WebElement a = div.findElement(By.xpath("div/a"));
			String href = a.getAttribute("href");
			String title = a.findElement(By.xpath("span[contains(@class, 'pro-name')]")).getText();
			if (!title.toLowerCase().contains(AQUATICA.toLowerCase())) {
				System.err.println("\t WARNING: BAD BRAND -> " + title);
				continue;
			}
			result.foundItems.add(title);
			System.out.println("\t INFO: FOUND ITEM -> " + title + " [" + result.foundItems.size() + "]");

			for (Item item : getDetails(href, title, detailsDriver)) {
				result.addItem(item);
				if (result.foundItems.size() == MAX_ITEMS)
					return result;
			}
		}
		result.saveTo(f);
		return result;
	}

	private Collection<Item> getDetails(String href, String searchTitle, AquaticaDriver detailsDriver) {
		Collection<Item> result = new ArrayList<Item>();
		detailsDriver.get(href);
		WebElement div = detailsDriver.findElement(By.xpath("//section[@id='mainContent']/section[position()=1]/div/div[position()=2]"));
		String realTitle = div.findElement(By.xpath("span[@itemprop='name']")).getText();
		String metaInfo = detailsDriver.findElementsByTagName("meta").get(2).getAttribute("content");
		metaInfo = metaInfo.substring(metaInfo.indexOf("627722"), metaInfo.indexOf(", compare"));
		List<String> upcs = new ArrayList<String>(splitter.splitToList(metaInfo));
		if (upcs.size() == 0) {
			System.err.println("\t WARNING: NOT FOUND UPC CODES -> " + metaInfo);
		}
		// os.productpage.defaultOption.sellingPrice
		String testPrice = getPrice(div, null, detailsDriver);
		boolean mustBeMorePrice = testPrice.contains("-") || tryParsePrice(testPrice) == null || upcs.size() > 1;
		boolean hasOptions = false;
		boolean hasSelect = false;
		// boolean hasSwatch = false;

		List<WebElement> options = div.findElements(By.xpath(".//select[@class='options-dropdown']"));
		if (!options.isEmpty()) {
			hasOptions = true;
			hasSelect = true;
		}

		if (!mustBeMorePrice && !hasOptions) {
			Item item = new Item();
			item.href = href;
			item.title = realTitle;
			item.price = tryParsePrice(testPrice);
			item.UPC = getUPC(item, upcs);
			item.qty = div.findElements(By.xpath(".//select[contains(@id,'addqty')]/option")).size();
			result.add(item);
			return result;
		} else {
			if (!hasSelect) {
				System.err.println("\t ERROR: NOT FOUND SELECT");
				return result;
			}
			if (options.size() != 1) {
				System.err.println("\t ERROR: EXPECTED ONE SELECT ->" + options.size());
				return result;
			}
			Collection<String> selectedStrings = new ArrayList<String>();
			Map<String, String> mapValues = new HashMap<String, String>();
			Map<String, Integer> mapQTY = new HashMap<String, Integer>();
			Select select = new Select(options.get(0));
			for (WebElement option : select.getOptions()) {
				String value = option.getAttribute("value");
				if (value == null || value.trim().length() == 0) {
					value = "0";
				}
				if ("0".equals(value.trim())) {
					continue;
				}
				int qty = Integer.parseInt(option.getAttribute("data-qty"));
				String text = option.getText();
				selectedStrings.add(text);
				mapValues.put(text, value);
				mapQTY.put(text, qty);
			}
			for (String text : selectedStrings) {
				select.selectByVisibleText(text);
				testPrice = getPrice(div, mapValues.get(text), detailsDriver);
				Item item = new Item();
				item.href = href;
				item.title = realTitle + " -> " + text;
				item.price = tryParsePrice(testPrice);
				item.qty = mapQTY.get(text);
				item.UPC = getUPC(item, upcs);
				upcs.remove(item.UPC);
				result.add(item);
				select.deselectByVisibleText(text);
			}
			return result;
		}
	}

	private String getUPC(Item item, List<String> upcs) {
		return item.resolveUPC("", upcs);
	}

	private String getPrice(WebElement div, String value, AquaticaDriver detailsDriver) {
		List<WebElement> priceElement = div.findElements(By.xpath(".//span[@itemprop='price']"));
		if (priceElement.size() == 1)
			return priceElement.get(0).getText();
		else {
			if (value == null)
				return (String) detailsDriver.executeScript("return os.productpage.defaultOption.sellingPrice");
			else
				return (String) detailsDriver.executeScript("return os.productpage.options['" + value + "'].sellingPrice");
		}
	}

	private Double tryParsePrice(String testPrice) {
		try {
			testPrice = testPrice.replace("$", "");
			testPrice = testPrice.replace(",", "");
			testPrice = testPrice.replace(" ", "");
			double price = Double.parseDouble(testPrice);
			return price;
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public String toString() {
		return "OverStock";
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		// TODO Auto-generated method stub

	}
}
