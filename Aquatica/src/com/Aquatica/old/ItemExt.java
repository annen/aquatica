package com.Aquatica.old;

import java.util.ArrayList;
import java.util.List;

import com.Aquatica.Item;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public class ItemExt extends Item {
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Joiner joiner = Joiner.on('\t').useForNull("");
	
	private static final Splitter subListSplitter = Splitter.on("~~~").trimResults();
	private static final Joiner subListJoiner = Joiner.on("!!!").skipNulls();
	
	public String info = "";
	public List<String> images = new ArrayList<String>();
	
	@Override
	public void parseString(String data) {
		super.parseString(data);
		if (data == null || data.trim().isEmpty())
			return;
		List<String> values = splitter.splitToList(data);
		if (values.size() > 5) {
			info = values.get(5);
		}
		if (values.size() > 6) {
			images = subListSplitter.splitToList(values.get(6));
		}
	}
	
	@Override
	public String serialize() {
		return joiner.join(title, UPC, price, href, qty, info.replace("\t", " ").replace("\r", "").replace("\n", "~~~"), subListJoiner.join(images));
	}
}
