package com.Aquatica.old;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.SiteTester.ProcessListener;
import com.Aquatica.drivers.AquaticaDriver;

public class HouzzSiteTester implements SiteTester {
	private static final int MAX_ITEMS = 500;
	private static final String AQUATICA = "Aquatica";
	private static final String MAIN_PAGE = "http://www.houzz.com/photos/products/buy-on-houzz/seller--Aquatica-Plumbing-Group/ls=2";
	private static final HouzzSiteTester INSTANCE = new HouzzSiteTester();
	
	public static final HouzzSiteTester instance() {
		return INSTANCE;
	}
	
	private HouzzSiteTester() {
	}
	
	@Override
	public TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception {
		TestResult result = new TestResult();
		File f = new File("./" + getClass().getSimpleName() + ".source.txt");
		if (f.exists())
			return result.loadFrom(f);
		mainDriver.get(MAIN_PAGE);
		
		String urlNextPage;
		do {
			WebElement resultsElement = mainDriver.findElementById("browseSpacesContext");
			List<WebElement> divs = resultsElement.findElements(By.xpath("div[contains(@class,'whiteCard')]"));
			for (WebElement div : divs) {
				WebElement a = div.findElement(By.xpath(".//div[@class='photoMeta']/a[contains(@class,'product-title')]"));
				String href = a.getAttribute("href");
				String title = a.getText();
				if (!title.startsWith(AQUATICA)) {
					title = AQUATICA + " " + title;
				}
				if (!title.toLowerCase().contains(AQUATICA.toLowerCase())) {
					System.err.println("\t WARNING: BAD BRAND -> " + title);
					continue;
				}
				result.foundItems.add(title);
				System.out.println("\t INFO: FOUND ITEM -> " + title + " [" + result.foundItems.size() + "]");
				for (Item item : getDetails(href, title, detailsDriver)) {
					result.addItem(item);
					if (result.foundItems.size() == MAX_ITEMS)
						return result;
				}
			}
			urlNextPage = null;
			List<WebElement> nextElement = mainDriver.findElements(By.xpath("//div[@class='pagination-wrapper']/ul[@id='paginationBar']/li/a[contains(@class,'next')]"));
			if (!nextElement.isEmpty()) {
				if (nextElement.size() != 1) {
					System.err.println("\t WARNING: EXPECTED ONE NEXT PAGE BUTTON");
				}
				urlNextPage = nextElement.get(0).getAttribute("href");
				mainDriver.get(urlNextPage);
			}
		} while (urlNextPage != null);
		result.saveTo(f);
		return result;
	}
	
	private Collection<Item> getDetails(String href, String searchTitle, AquaticaDriver detailsDriver) throws Exception {
		Collection<Item> result = new ArrayList<Item>();
		detailsDriver.get(href);
		WebElement div = detailsDriver.findElement(By.xpath("//div[@itemtype='http://schema.org/Product']"));
		String realTitle = getTitle(div);
		String testPrice = div.findElement(By.xpath(".//div[@itemprop='price']")).getText();
		boolean mustBeMorePrice = testPrice.contains("-") || tryParsePrice(testPrice) == null;
		boolean hasOptions = false;
		List<WebElement> options = div.findElements(By.xpath(".//div[@class='variationOptions']"));
		if (!options.isEmpty()) {
			hasOptions = true;
		}
		if (!mustBeMorePrice && !hasOptions) {
			Item item = new Item();
			item.href = href;
			item.title = realTitle;
			item.price = tryParsePrice(testPrice);
			item.UPC = getUPC(item, detailsDriver);
			result.add(item);
			return result;
		} else {
			if (options.size() != 1 && !realTitle.contains("Inflection")) {
				System.err.println("\t ERROR: EXPECTED ONE SELECT ->" + options.size());
				return result;
			}
			Collection<String> selectedStrings = new ArrayList<String>();
			List<WebElement> optionA = options.get(0).findElements(By.xpath("a[contains(@class,'variationOption')]"));
			for (WebElement option : optionA) {
				String value = option.getAttribute("optionvalue");
				selectedStrings.add(value);
			}
			for (String text : selectedStrings) {
				div = detailsDriver.findElement(By.xpath("//div[@itemtype='http://schema.org/Product']"));
				WebElement option = div.findElement(By.xpath(".//div[@class='variationOptions']/a[@optionvalue='" + text + "']"));
				option.click();
				Thread.sleep(1000);
				testPrice = div.findElement(By.xpath(".//div[@itemprop='price']")).getText();
				Item item = new Item();
				item.href = href;
				item.title = getTitle(div);
				item.price = tryParsePrice(testPrice);
				item.UPC = getUPC(item, detailsDriver);
				result.add(item);
			}
			return result;
		}
	}
	
	private String getTitle(WebElement div) {
		String realTitle = div.findElement(By.xpath(".//h1[@itemprop='name']")).getText().trim();
		if (!realTitle.startsWith(AQUATICA)) {
			realTitle = AQUATICA + " " + realTitle;
		}
		return realTitle;
	}
	
	private String getUPC(Item item, AquaticaDriver detailsDriver) {
		WebElement div = detailsDriver.findElement(By.xpath("//div[@itemtype='http://schema.org/Product']"));
		StringBuilder info = new StringBuilder();
		for (WebElement dd : div.findElements(By.xpath(".//div[@class='productSpec']/dl/dd"))) {
			info.append(dd.getText() + " ");
		}
		// Resolver resolver = UPCResolver.instance().getUPC(item.title, item.price, info.toString());
		return item.resolveUPC(info.toString(), null);
	}
	
	private Double tryParsePrice(String testPrice) {
		try {
			testPrice = testPrice.replace("$", "");
			testPrice = testPrice.replace(",", "");
			testPrice = testPrice.replace(" ", "");
			double price = Double.parseDouble(testPrice);
			return price;
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public String toString() {
		return "Houzz";
	}
	
	@Override
	public void addProcessListener(ProcessListener listener) {
		// TODO Auto-generated method stub

	}
}
