package com.Aquatica.old;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

@SuppressWarnings("unused")
public class CRMInserter {

	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Double CAD = 1.32463;
	private static final Double EUR = 0.88082;
	private static final Double UK = 0.64822;
	private static final NumberFormat formatter = new DecimalFormat("#0");

	public static void main(String[] args) throws Exception {
		File dataFile = new File("./CRM_MONEY.txt");
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		List<String> lines = Files.readLines(dataFile, Charsets.UTF_8);
		int columnId = 0;
		Map<String, List<String>> rows = new LinkedHashMap<String, List<String>>();
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		Integer UPC = 1;
		Integer MSRP = 2;
		Integer MAP = 3;
		// Integer DESC = 4;
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			ArrayList<String> dataList = new ArrayList<String>(splitter.splitToList(line));
			if (dataList.size() != columns.size()) {
				System.err.println("NOT EQUAL COLUMN SIZE");
			}
			String upc = dataList.get(UPC);
			if (rows.containsKey(upc)) {
				System.err.println("DUPLICATE UPC");
			}
			rows.put(upc, dataList);
		}

		FirefoxProfile ffProfile = new FirefoxProfile(new File("C:/Users/Annenkov/AppData/Roaming/Mozilla/Firefox/Profiles/n0xgr86r.default"));
		ffProfile.setPreference("browser.usedOnWindows10", false);
		ffProfile.setPreference("browser.usedOnWindows10.introURL", "about:blank");
		FirefoxDriver driver = new FirefoxDriver(ffProfile);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		String mainWindow = driver.getWindowHandle();
		driver.navigate().to("https://crm.aquaticagroup.com/AquaticaGroup/_root/homepage.aspx?etc=1024&pagemode=iframe&sitemappath=SFA|SFA|nav_products");
		Thread.sleep(1000);
		driver.findElementById("crmGrid_SavedNewQuerySelector").click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.findElementsByXPath("//a[contains(@class,'ms-crm-VS-MenuItem-Anchor')][contains(.,'for import')]").get(0).click();
		Thread.sleep(1000);
		List<WebElement> aa = driver.findElementsByXPath("//a[contains(@id,'primary')][@class='ms-crm-List-Link']");
		Collection<String> ids = new ArrayList<String>();
		for (WebElement a : aa) {
			String id = a.getAttribute("id");
			id = id.substring(id.indexOf('{') + 1, id.indexOf('}'));
			ids.add(id);
		}
		int i = 0;
		int minI = 0;
		for (String id : ids) {
			i++;
			System.out.println(i + "\t" + id);
			if (i < minI) {
				continue;
			}
			// String href = "https://crm.aquaticagroup.com/AquaticaGroup/userdefined/edit.aspx?etc=1024&id={" + id + "}&pagemode=iframe"; /// ONLY DATA
			String href = "https://crm.aquaticagroup.com/AquaticaGroup/main.aspx?etn=product&id={" + id + "}&pagetype=entityrecord";
			driver.navigate().to(href);
			Thread.sleep(2000);
			driver.switchTo().frame("contentIFrame");
			String upc = driver.findElementById("productnumber").getAttribute("value");
			System.out.println(upc);
			// List<String> row = rows.get(upc);
			// if (row == null) {
			// System.err.println("NOT FOUND ROW " + "627722001186");
			// continue;
			// }
			// System.out.println(row);

			// Update main Info
			// setDescription(driver, row);
			// Double msrp = Double.parseDouble(row.get(MSRP).replace(" ", ""));
			// Double map = Double.parseDouble(row.get(MAP).replace(" ", ""));

			// setMainMsrp(driver, msrp);
			// setMainMap(driver, msrp * .69);
			// saveNotClose(driver);

			// Update All prices
			// goToPrices(driver);
			// changeOneCurrency(driver, mainWindow, formatter.format(msrp), formatter.format(map), "USD");
			// changeOneCurrency(driver, mainWindow, formatter.format(msrp * UK), formatter.format(map * UK), "UK");
			// changeOneCurrency(driver, mainWindow, formatter.format(msrp * EUR), formatter.format(map * EUR), "Euro");
			// changeOneCurrency(driver, mainWindow, formatter.format(msrp * CAD), formatter.format(map * CAD), "CAD");
			// driver.switchTo().defaultContent();

			// Set 85% Euro off
			// goToPrices(driver);
			// changeOneCurrency(driver, mainWindow, formatter.format(msrp * UK * 0.95) + ".00", formatter.format(msrp * UK * 0.95 * 0.69) + ".00", "UK");

			// saveImages(driver, upc);
			saveImages2(driver, upc, null, true, true);

			driver.switchTo().defaultContent();
		}
		driver.close();
	}

	public static void setDescription(FirefoxDriver driver, List<String> row) throws Exception {
		int DESC = 3;
		String text = row.get(DESC);
		text = text.replace("~~~", "\r\n");
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		WebElement desc = driver.findElementById("description");
		while (!desc.isDisplayed()) {
			driver.findElementByXPath("//a[@class='ms-crm-InlineTabHeaderText'][.='Description']").click();
		}
		desc.clear();
		desc.sendKeys(text);
		driver.switchTo().defaultContent();
		driver.executeScript("var evt = document.getElementById('contentIFrame').contentDocument.createEvent('HTMLEvents'); evt.initEvent('change', false, true); document.getElementById('contentIFrame').contentDocument.getElementById('description').dispatchEvent(evt);");
	}

	public static void goToPrices(FirefoxDriver driver) throws InterruptedException {
		driver.switchTo().frame(0);
		driver.findElementById("navPrices").click();
		Thread.sleep(3000);
		driver.findElementById("navPrices").click();
		Thread.sleep(2000);
	}

	public static void saveNotClose(FirefoxDriver driver) throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.findElementById("product|NoRelationship|Form|Mscrm.Form.product.Save-Large").click();
		Thread.sleep(2000);
	}

	public static void setMainMsrp(FirefoxDriver driver, Double msrp) throws Exception {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		WebElement price = driver.findElementById("price");
		price.clear();
		price.sendKeys(formatter.format(msrp) + ".00");
		Thread.sleep(1000);
		driver.executeScript("var evt = document.createEvent('HTMLEvents');  evt.initEvent('change', true, true); arguments[0].dispatchEvent(evt);", price);
	}

	public static void setMainMap(FirefoxDriver driver, Double map) throws Exception {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		WebElement cost = driver.findElementById("standardcost");
		cost.clear();
		cost.sendKeys(formatter.format(map) + ".00");
		Thread.sleep(1000);
		// driver.executeScript("arguments[0].onchange();", cost);
		driver.executeScript("var evt = document.createEvent('HTMLEvents');  evt.initEvent('change', true, true); arguments[0].dispatchEvent(evt);", cost);
	}

	public static void changeOneCurrency(FirefoxDriver driver, String mainWindow, String msrp, String map, String currency) throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		driver.switchTo().frame("areaPricesFrame");
		List<WebElement> curElements = driver.findElementsByXPath("//table[@id='gridBodyTable']//tr[@class='ms-crm-List-Row']/td[contains(.,'" + currency + "')]");
		if (curElements.isEmpty()) {
			System.err.println("NOT FOUND CURRENCY " + currency);
			return;
		}
		if (curElements.size() > 1) {
			System.err.println("FOUND MANY CURRENCY " + currency);
			return;
		}

		WebElement usdPriceLink = curElements.get(0);
		driver.executeScript("var evt = document.createEvent('HTMLEvents');  evt.initEvent('dblclick', true, true); arguments[0].dispatchEvent(evt);", usdPriceLink);
		Thread.sleep(3000);
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		windowHandles.remove(mainWindow);
		driver.switchTo().window(windowHandles.get(0));
		Thread.sleep(1000);
		driver.switchTo().frame(0);
		WebElement price = driver.findElementById("roundingoptionamount");
		if (!price.getAttribute("value").startsWith(msrp)) {
			System.err.println("WRONG " + currency + " MSRP " + price.getAttribute("value") + "<>" + msrp);
			price.clear();
			price.sendKeys(msrp);
			driver.executeScript("var evt = document.createEvent('HTMLEvents');  evt.initEvent('change', true, true); arguments[0].dispatchEvent(evt);", price);
			Thread.sleep(1000);
		}

		WebElement cost = driver.findElementById("amount");
		if (!cost.getAttribute("value").startsWith(map)) {
			System.err.println("WRONG " + currency + " MAP " + cost.getAttribute("value") + "<>" + map);
			cost.clear();
			cost.sendKeys(map);
			driver.executeScript("var evt = document.createEvent('HTMLEvents');  evt.initEvent('change', true, true); arguments[0].dispatchEvent(evt);", cost);
			Thread.sleep(1000);
		}
		driver.switchTo().defaultContent();

		driver.findElementById("productpricelevel|NoRelationship|Form|Mscrm.Form.productpricelevel.SaveAndClose-Large").click();
		Thread.sleep(3000);
		driver.switchTo().window(mainWindow);
	}

	public static void saveImages(FirefoxDriver driver, String upc) throws Exception {
		// String home = "C:\\Documents and Settings\\Annen\\������� ����\\IMAGES\\";
		String home = "D:\\IMAGES\\";
		String tmp = "C:\\tmp\\";
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		String SKU = driver.findElementById("vendorpartnumber").getAttribute("value");
		WebElement notes = driver.findElementById("notescontrol");
		while (!notes.isDisplayed()) {
			driver.findElementByXPath("//a[@class='ms-crm-InlineTabHeaderText'][.='Notes']").click();
		}
		Thread.sleep(2000);
		String base = home + "SKU\\" + SKU;
		File fol = new File(base);
		fol.mkdirs();
		Process p = new ProcessBuilder("C:\\WINDOWS\\system32\\junction.exe", "-s", home + "UPC\\" + upc, base).start();
		p.waitFor();

		driver.switchTo().frame("notescontrol");
		List<WebElement> urls = driver.findElementsByXPath("//span[@class='attachment']");
		int imgNum = 0;
		int pdfNum = 0;
		for (WebElement urlE : urls) {
			driver.executeScript("arguments[0].scrollIntoView();", urlE);
			String name = urlE.getAttribute("title");
			name = name.substring(0, name.lastIndexOf("(", name.length() - 5));
			name = name.trim();
			String ext = name.substring(name.lastIndexOf(".")).toLowerCase();
			boolean isPDF = ext.equalsIgnoreCase(".pdf");
			boolean isImg = ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".jpeg");
			if (!isPDF && !isImg) {
				System.err.println("SKIP " + name);
				continue;
			}
			int num = 0;
			if (isImg) {
				imgNum++;
				num = imgNum;
			}
			if (isPDF) {
				pdfNum++;
				num = pdfNum;
			}
			File tmpFile = new File(tmp + name);
			if (tmpFile.exists()) {
				tmpFile.delete();
			}
			urlE.click();
			Thread.sleep(500);
			tmpFile = new File(tmp + name);
			while (tmpFile.length() == 0) {
				Thread.sleep(500);
				tmpFile = new File(tmp + name);
			}
			// File newFile = new File(fol, upc + "-" + (num < 10 ? "0" : "") + num + ext);
			File newFile = new File(fol, name);
			Files.copy(tmpFile, newFile);
			System.out.println(newFile.getName() + "<==" + name);
			tmpFile.delete();
			// URL website = getSaveUrl(urlE);
			// And as before now you can use URL and URLConnection
		}
	}

	public static void saveImages2(FirefoxDriver driver, String upc, Collection<String> needUPC, boolean needImg, boolean needPdf) throws Exception {
		// String home = "C:\\Documents and Settings\\Annen\\������� ����\\IMAGES\\";
		if (needUPC != null && !needUPC.contains(upc))
			return;
		String home = "D:\\IMAGES\\";
		String tmp = "C:\\tmp\\";
		driver.switchTo().defaultContent();
		driver.switchTo().frame("contentIFrame");
		String SKU = driver.findElementById("vendorpartnumber").getAttribute("value");
		WebElement notes = driver.findElementById("notescontrol");
		while (!notes.isDisplayed()) {
			driver.findElementByXPath("//a[@class='ms-crm-InlineTabHeaderText'][.='Notes']").click();
		}
		Thread.sleep(2000);
		String base = home + "SKU\\" + SKU;
		File fol = new File(base);
		fol.mkdirs();
		Process p = new ProcessBuilder("C:\\WINDOWS\\system32\\junction.exe", "-s", home + "UPC\\" + upc, base).start();
		p.waitFor();

		driver.switchTo().frame("notescontrol");
		List<WebElement> urls = driver.findElementsByXPath("//span[@class='attachment']");
		int imgNum = 0;
		int pdfNum = 0;
		for (WebElement urlE : urls) {
			driver.executeScript("arguments[0].scrollIntoView();", urlE);
			String name = urlE.getAttribute("title");
			name = name.substring(0, name.lastIndexOf("(", name.length() - 5));
			name = name.trim();
			String ext = name.substring(name.lastIndexOf(".")).toLowerCase();
			boolean isPDF = ext.equalsIgnoreCase(".pdf");
			boolean isImg = ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".jpeg");
			if (!isPDF && !isImg) {
				System.err.println("SKIP " + name);
				continue;
			}
			if (isImg && !needImg) {
				System.err.println("SKIP NOT NEED" + name);
				continue;
			}
			if (isPDF && !needPdf) {
				System.err.println("SKIP NOT NEED" + name);
				continue;
			}
			int num = 0;
			if (isImg) {
				imgNum++;
				num = imgNum;
			}
			if (isPDF) {
				pdfNum++;
				num = pdfNum;
			}
			File tmpFile = new File(tmp + name);
			if (tmpFile.exists()) {
				tmpFile.delete();
			}
			urlE.click();
			Thread.sleep(500);
			tmpFile = new File(tmp + name);
			while (tmpFile.length() == 0) {
				Thread.sleep(500);
				tmpFile = new File(tmp + name);
			}
			File newFile = new File(fol, upc + "-" + (num < 10 ? "0" : "") + num + ext);
			// File newFile = new File(fol, name);
			Files.copy(tmpFile, newFile);
			System.out.println(newFile.getName() + "<==" + name);
			tmpFile.delete();
			// URL website = getSaveUrl(urlE);
			// And as before now you can use URL and URLConnection
		}
	}

	private static URL getSaveUrl(WebElement urlE) throws MalformedURLException {
		String domain = "https://crm.aquaticagroup.com";
		String path = urlE.getAttribute("url");
		String attachmentid = urlE.getAttribute("attachmentid");
		String attachmenttype = urlE.getAttribute("attachmenttype");
		String userid = urlE.getAttribute("userid");
		String merchantid = urlE.getAttribute("merchantid");
		String isnotestabattachment = urlE.getAttribute("isnotestabattachment");
		String wrpctokenurl = urlE.getAttribute("wrpctokenurl");
		String downloadUrl = domain + path + "?" + "AttachmentType=" + attachmenttype + "&AttachmentId=" + attachmentid + "&IsNotesTabAttachment=" + isnotestabattachment + wrpctokenurl.replace("&amp;", "&");
		URL website = new URL(downloadUrl);
		return website;
	}
}
