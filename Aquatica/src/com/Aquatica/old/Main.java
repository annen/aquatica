package com.Aquatica.old;

import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import com.Aquatica.DRIVER;
import com.Aquatica.Item;
import com.Aquatica.SiteTester;
import com.Aquatica.TestResult;
import com.Aquatica.UPCResolverTest;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

public class Main {
	public static enum SearchMode {
		UPC, MAP, MATCHER;
	}
	
	private static final String PRICE_COLUMN = "MAP Price";
	private static final String UPC_COLUMN = "UPC Code";
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Joiner joiner = Joiner.on('\t').useForNull("");
	
	public static void main(String[] args) throws Exception {
		UPCResolverTest.instance().reload();
		System.out.println("---START---");
		System.out.println("---BROWSER OPENED---");
		AquaticaDriver mainDriver = DRIVER.getDriver();
		AquaticaDriver detailsDriver = DRIVER.getDetailsDriver();
		try {
			mainDriver.manage().window().setPosition(new Point(0, 0));
			java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			mainDriver.manage().window().setSize(new Dimension(200 + screenSize.width / 2, screenSize.height - 40));
			detailsDriver.manage().window().setPosition(new Point(screenSize.width / 2, 0));
			detailsDriver.manage().window().setSize(new Dimension(200 + screenSize.width / 2, screenSize.height - 40));
			mainDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			detailsDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			System.out.println("---BROWSER OPEN---");
			Collection<String> needTests = Arrays.asList(args);
			Collection<SiteTester> siteTesters = getSiteTesters();
			for (SiteTester siteTester : siteTesters) {
				String testName = siteTester.getClass().getSimpleName();
				System.out.println("---TEST FOR " + testName + "---");
				if (!needTests.contains(testName)) {
					System.out.println("---IGNORED " + testName + "---");
					continue;
				}
				TestResult testResult = siteTester.test(mainDriver, detailsDriver);
				System.out.println("---STATISTICS---");
				System.out.println("      FOUND ITEMS = " + testResult.foundItems.size() + "---");
				System.out.println("      ADDED ITEMS = " + testResult.items.size() + "---");
				File fr = new File("./" + testName + ".result" + ".txt");
				File f = new File("./TESTDATA.txt");
				if (!f.exists()) {
					testResult.saveTo(fr);
				} else {
					saveResultsWithData(f, fr, testName, testResult);
				}
			}
		} finally {
			DRIVER.freeMainDriver(mainDriver);
			DRIVER.freeDetailsDriver(detailsDriver);
		}
		UPCResolverTest.instance().save();
	}
	
	private static void saveResultsWithData(File dataFile, File resultFile, String testName, TestResult testResult) throws Exception {
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		Collection<List<String>> data = new ArrayList<List<String>>();
		List<String> lines = Files.readLines(dataFile, Charsets.UTF_8);
		if (lines.isEmpty()) {
			testResult.saveTo(resultFile);
			return;
		}
		int columnId = 0;
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			data.add(new ArrayList<String>(splitter.splitToList(line)));
		}
		final Integer priceColumnIndex = columns.get(PRICE_COLUMN);
		final Integer upcColumnIndex = columns.get(UPC_COLUMN);
		final Integer mapColumnIndex = columns.get(testName);
		SearchMode mode = null;
		if (upcColumnIndex != null && testResult.hasUPC) {
			mode = SearchMode.UPC;
		} else if (mapColumnIndex != null) {
			mode = SearchMode.MAP;
		}
		
		if (mode == null) {
			System.err.println("---NOT FOUND LINK MODE FOR " + testName + " ---");
			return;
		}
		Collection<Item> foundedWebItems = new ArrayList<Item>();
		Map<String, Item> items = new HashMap<String, Item>();
		for (Item item : testResult.items) {
			if (mode.equals(SearchMode.MAP)) {
				items.put(item.title.toLowerCase().trim(), item);
			} else {
				items.put(item.UPC.toLowerCase().trim(), item);
			}
		}
		for (List<String> row : data) {
			String id = mode.equals(SearchMode.MAP) ? row.get(mapColumnIndex) : row.get(upcColumnIndex);
			id = id.toLowerCase().trim();
			Item webItem = items.get(id);
			if (webItem == null) {
				row.add(1, "NOT FOUND");
				row.add(2, "");
				continue;
			}
			foundedWebItems.add(webItem);
			if (priceColumnIndex != null) {
				String sPrice = row.get(priceColumnIndex);
				double rowPrice = getPrice(sPrice);
				double webPrice = webItem.price == null ? 0.0 : webItem.price;
				if (Math.abs(webPrice - rowPrice) > 1.0) {
					row.add(1, "WRONG PRICE");
					row.add(2, String.valueOf(webPrice));
				} else {
					row.add(1, "VALID");
					row.add(2, "");
				}
			} else {
				row.add(1, "EXIST");
				row.add(2, "");
			}
		}
		if (foundedWebItems.size() != testResult.items.size()) {
			data.add(new ArrayList<String>());
			data.add(new ArrayList<String>());
			data.add(Arrays.asList("Items did not mention in list"));
			for (Item item : testResult.items) {
				if (!foundedWebItems.contains(item)) {
					data.add(Arrays.asList(item.title, item.price.toString().replace('.', ','), item.href));
				}
			}
		}
		FileWriterWithEncoding fw = new FileWriterWithEncoding(resultFile, Charsets.UTF_8);
		List<String> headers = new ArrayList<String>(columns.keySet());
		headers.add(1, "Check Result");
		headers.add(2, "Site Price");
		fw.write(joiner.join(headers) + "\r\n");
		for (List<String> row : data) {
			fw.write(joiner.join(row) + "\r\n");
		}
		fw.flush();
		fw.close();
	}
	
	private static double getPrice(String sPrice) {
		if (sPrice == null)
			return 0.0;
		sPrice = sPrice.trim().replace(" ", "").replace("$", "");
		sPrice = sPrice.replace(",", "");
		if (sPrice.isEmpty())
			return 0.0;
		try {
			double rowPrice = Double.parseDouble(sPrice);
			return rowPrice;
		} catch (Exception e) {
			return 0.0;
		}
	}
	
	private static Collection<SiteTester> getSiteTesters() {
		Collection<SiteTester> siteTesters = new ArrayList<SiteTester>();
		// siteTesters.add(new AtgStoresSiteTester(AtgStoresSiteTester.MAIN_PAGE, true));
		// siteTesters.add(OverStockSiteTester.instance());
		// siteTesters.add(HomeDepotOldSiteTester.instance());
		// siteTesters.add(HouzzSiteTester.instance());
		// siteTesters.add(ArchiInfoSiteTester.instance());
		// siteTesters.add(new AmazonComSiteTester(AmazonComSiteTester.MAIN_PAGE, true));
		//
		siteTesters.add(UPCResolverTest.instance());
		return siteTesters;
	}
	
	private static void closeDriver() throws InterruptedException {
		for (int i = 3; i > 0; i--) {
			System.out.println("---CLOSE AFTER " + i + " sec---");
			Thread.sleep(1000);
		}
		DRIVER.getDriver().quit();
		DRIVER.getDetailsDriver().quit();
		System.out.println("---CLOSED---");
	}
	
}
