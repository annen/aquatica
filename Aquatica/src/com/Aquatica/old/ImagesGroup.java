package com.Aquatica.old;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ImagesGroup {

	public static void main(String[] args) {
		File home = new File("D:\\IMAGES\\PF");
		File[] listFiles = home.listFiles();
		Map<String, List<File>> gr = new LinkedHashMap<String, List<File>>();
		for (File file : listFiles) {
			String name = file.getName();
			String upc = name.substring(0, name.indexOf("-"));
			List<File> list = gr.get(upc);
			if (list == null) {
				list = new ArrayList<File>();
				gr.put(upc, list);
			}
			list.add(file);
		}

		for (Entry<String, List<File>> g : gr.entrySet()) {
			System.out.print(g.getKey() + "\t");
			for (File file : g.getValue()) {
				System.out.print(file.getName() + "\t");
			}
			System.out.println();
		}

	}
}
