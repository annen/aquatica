package com.Aquatica.old;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.commons.io.output.FileWriterWithEncoding;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

public class Images {
	public static String homePath = "C:/Documents and Settings/Annen/������� ����/IMAGES/";
	private static final String SKY_COLUMN = "Vendor Part Number";
	private static final String UPC_COLUMN = "UPC Code";
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Joiner joiner = Joiner.on('\t').useForNull("");

	public static void main(String[] args) throws Exception {
		System.out.println("---START---");
		File f = new File("./TESTDATA.txt");
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		List<String> lines = Files.readLines(f, Charsets.UTF_8);
		int columnId = 0;
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		final Integer upcColumnIndex = columns.get(UPC_COLUMN);
		final Integer skyColumnIndex = columns.get(SKY_COLUMN);
		if (upcColumnIndex == null || upcColumnIndex < 0) {
			System.err.println("---NOT FOUND UPC COLUMN");
			return;
		}
		if (skyColumnIndex == null || skyColumnIndex < 0) {
			System.err.println("---NOT FOUND SKY COLUMN");
			return;
		}
		Map<String, Collection<String>> images = new LinkedHashMap<String, Collection<String>>();
		Map<String, Collection<String>> pdf = new LinkedHashMap<String, Collection<String>>();
		Map<String, String> skus = new HashMap<String, String>();
		int maxImg = 0;
		int maxPdf = 0;
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			List<String> data = splitter.splitToList(line);
			String upc = data.get(upcColumnIndex);
			String sky = data.get(skyColumnIndex);
			images.put(upc, new TreeSet<String>());
			pdf.put(upc, new TreeSet<String>());
			if (skus.containsValue(sky)) {
				System.err.println(upc + " " + sky);
			}
			skus.put(upc, sky);
		}
		File homeUPC = new File(homePath + "UPC");
		for (String upcFolder : homeUPC.list()) {
			if (!skus.containsKey(upcFolder)) {
				System.err.println("---UNKNOWN  UPC " + upcFolder);
				continue;
			}
			File imFiles = new File(homeUPC, upcFolder);
			for (String imFile : imFiles.list()) {
				if (imFile.endsWith(".db"))
					continue;
				imFile = imFile.toLowerCase();
				File fi = new File(imFiles, imFile);
				// Files.copy(fi, new File(skyFolder, imFile));
				if (!imFile.startsWith(upcFolder + "-0")) {
					System.err.println("---WRONG NAME " + fi);
				}
				if (imFile.endsWith(".pdf")) {
					Collection<String> upcPDF = pdf.get(upcFolder);
					upcPDF.add(imFile);
					maxPdf = Math.max(maxPdf, upcPDF.size());
				} else if (imFile.endsWith(".jpg")) {
					Collection<String> upcIMG = images.get(upcFolder);
					upcIMG.add(imFile);
					maxImg = Math.max(maxImg, upcIMG.size());
				} else {
					System.err.println("---UNKNOWN  FILE TYPE " + fi);
				}
				// System.out.println(fi);
			}
		}
		checkNames(images);
		checkNames(pdf);
		copyFiles(images, skus);
		copyFiles(pdf, skus);
		printFileResult(images, pdf, skus, maxImg, maxPdf);
		System.out.println("---FINISH---");
	}

	private static void printFileResult(Map<String, Collection<String>> images, Map<String, Collection<String>> pdf, Map<String, String> skus, int maxImg, int maxPdf) throws Exception {
		String tabSpacer = new String(new char[maxImg]).replace('\0', '\t');
		File fr = new File("./images.result" + ".txt");
		FileWriterWithEncoding fw = new FileWriterWithEncoding(fr, Charsets.UTF_8);
		List<String> headers = new ArrayList<String>();
		headers.add("UPC");
		headers.add("SKY");
		for (int i = 0; i < maxImg; i++) {
			headers.add("Image " + (i + 1));
		}
		for (int i = 0; i < maxPdf; i++) {
			headers.add("Pdf " + (i + 1));
		}
		fw.write(joiner.join(headers) + "\r\n");
		for (Entry<String, Collection<String>> upcData : images.entrySet()) {
			String upc = upcData.getKey();
			Collection<String> img = upcData.getValue();
			fw.write(upc + "\t" + skus.get(upc) + "\t" + joiner.join(img) + tabSpacer.substring(0, maxImg - img.size() + 1) + joiner.join(pdf.get(upc)) + "\r\n");
		}
		fw.flush();
		fw.close();
	}

	private static void checkNames(Map<String, Collection<String>> images) {
		for (Entry<String, Collection<String>> upc : images.entrySet()) {
			int i = 1;
			for (String name : upc.getValue()) {
				String needName = upc.getKey() + "-0";
				if (i < 10) {
					needName = needName + "0";
				}
				needName = needName + i + ".";
				if (!name.startsWith(needName)) {
					System.err.println("---WRONG NAME " + name);
				}
				++i;
			}
		}
	}

	private static void copyFiles(Map<String, Collection<String>> images, Map<String, String> skus) throws Exception {
		File homeUPC = new File(homePath + "UPC");
		File homeSKY = new File(homePath + "SKY");
		if (!homeSKY.exists()) {
			homeUPC.mkdirs();
		}
		for (Entry<String, Collection<String>> upc : images.entrySet()) {
			File upcFolder = new File(homeUPC, upc.getKey());
			File skyFolder = new File(homeSKY, skus.get(upc.getKey()).replace("/", "_").replace("\\", "_"));
			if (!skyFolder.exists()) {
				skyFolder.mkdirs();
			}
			for (String name : upc.getValue()) {
				File file = new File(upcFolder, name);
				File newFile = new File(skyFolder, name);
				if (!file.exists()) {
					System.err.println("---NOT FOUND FILE " + file);
					continue;
				}
				if (newFile.exists()) {
					newFile.delete();
				}
				System.out.println("COPY " + upc.getKey() + "/" + name + " => " + newFile);
				Files.copy(file, newFile);
			}
		}
	}

	@SuppressWarnings("unused")
	private static void showSKY(Map<String, String> skus) {
		for (Entry<String, String> ss : skus.entrySet()) {
			System.out.println(ss.getKey() + "\t" + ss.getValue());
		}
	}
}
