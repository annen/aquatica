package com.Aquatica.old;

import static com.Aquatica.DRIVER.getDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Aquatica.DRIVER;
import com.Aquatica.drivers.AquaticaDriver;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;

public class PriceFallsInserter {
	private static final Splitter splitter = Splitter.on('\t').trimResults();
	private static final Splitter catSplitter = Splitter.on('|').trimResults();
	private static final Splitter subListSplitter = Splitter.on("~~~").trimResults();

	public static void main(String[] args) throws Exception {
		File dataFile = new File("./PRICEFALLSDATA.txt");
		Map<String, Integer> columns = new LinkedHashMap<String, Integer>();
		Map<String, List<String>> data = new LinkedHashMap<String, List<String>>();
		List<String> lines = Files.readLines(dataFile, Charsets.UTF_8);
		int columnId = 0;
		for (String header : splitter.split(lines.get(0))) {
			columns.put(header, columnId++);
		}
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line == null || line.trim().isEmpty()) {
				continue;
			}
			ArrayList<String> dataList = new ArrayList<String>(splitter.splitToList(line));
			if (dataList.size() != columns.size()) {
				System.err.println("NOT EQUAL COLUMN SIZE");
			}
			String upc = dataList.get(0);
			data.put(upc, dataList);
		}
		
		AquaticaDriver mainDriver = getDriver();
		try {
			mainDriver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
			mainDriver.get("https://www.pricefalls.com/my/store");
			mainDriver.findElementById("login_name").sendKeys("Aquatica Freestanding Bathtubs");
			mainDriver.findElementById("password").sendKeys("tubz456!!");
			mainDriver.findElementById("submit_button").click();
			Thread.sleep(3000);
			
			Collection<List<String>> rows = data.values();
			int skip = 127;
			Iterator<List<String>> iterator = rows.iterator();
			for (int addedIndex = 0; addedIndex < data.size(); addedIndex++) {
				List<String> row = iterator.next();
				if (addedIndex <= skip) {
					System.out.println("SKIP " + row.get(1));
					continue;
				}
				do {
					mainDriver.get("https://www.pricefalls.com/sell/product");
					Thread.sleep(2000);
				} while (mainDriver.findElementsById("selling_method-submit_button").isEmpty());
				mainDriver.findElementById("selling_method-submit_button").click();
				Thread.sleep(1000);
				addRow(columns, row, mainDriver);
				System.err.println("ADDED " + addedIndex + " " + row.get(1));
			}
		} finally {
			DRIVER.freeMainDriver(mainDriver);
		}
	}

	private static void addRow(Map<String, Integer> columns, List<String> row, AquaticaDriver mainDriver) throws InterruptedException {
		int catIndex = 1;
		for (String cat : catSplitter.split(row.get(4))) {
			String id = "product_data-categories-category_" + catIndex;
			new Select(getTrusted(id, mainDriver)).selectByVisibleText(cat);
			catIndex++;
		}
		mainDriver.findElementById("product_data-basic_information-title").sendKeys(row.get(43));
		mainDriver.findElementById("product_data-basic_information-brand").sendKeys("Aquatica");
		new Select(mainDriver.findElementById("product_data-basic_information-condition")).selectByValue("New");
		String text = row.get(44);
		text = text.replace("'", "`").replace("~~~", "\n");
		String checkText = "";

		// do {
		// mainDriver.switchTo().frame(2);
		// mainDriver.executeScript("arguments[0].innerHTML = arguments[1];", mainDriver.findElementByTagName("body"), text.replace("\n", "<br />"));
		// mainDriver.switchTo().defaultContent();
		// mainDriver.switchTo().frame(2);
		// checkText = (String) mainDriver.executeScript("return arguments[0].innerHTML;", mainDriver.findElementByTagName("body"), text.replace("\n", "<br />"));
		// mainDriver.switchTo().defaultContent();
		// } while (checkText.length() < 100);

		mainDriver.switchTo().frame(mainDriver.findElementByXPath("//iframe[contains(@class,'cke_wysiwyg_frame')]"));
		// Thread.sleep(1000);
		// mainDriver.findElementByTagName("body").click();
		// Thread.sleep(1000);
		// mainDriver.executeScript("arguments[0].innerHTML = arguments[1];", mainDriver.findElementByTagName("body"), text.replace("\n", "<br />"));
		// Thread.sleep(1000);
		mainDriver.executeScript("arguments[0].innerHTML = arguments[1];", mainDriver.findElementByTagName("body"), text.replace("\n", "<br />"));
		mainDriver.switchTo().defaultContent();
		// Add Images
		String home = "D:\\IMAGES\\SKU\\" + row.get(1) + "\\";
		File[] listFiles = new File(home).listFiles();
		Arrays.sort(listFiles);
		int picId = 1;
		for (File file : listFiles) {
			String path = file.getAbsolutePath();
			if (path.toLowerCase().endsWith(".jpg") && picId < 6) {
				WebElement fileInput = mainDriver.findElementById("picture" + picId);
				fileInput.sendKeys(path);
				picId++;
			}
		}

		mainDriver.findElementById("product_data-pricing-fixed_price").sendKeys(row.get(5).replace(" ", ""));
		mainDriver.findElementById("product_data-pricing-advertised_price").sendKeys(row.get(6).replace(" ", ""));
		new Select(mainDriver.findElementById("product_data-pricing-product_quantity")).selectByVisibleText("5");
		new Select(mainDriver.findElementById("product_data-durations-listing_duration")).selectByValue("90");
		String lt = row.get(36);
		int ilt = Integer.parseInt(lt);
		if (ilt > 30) {
			ilt = 30;
		}
		lt = String.valueOf(ilt);
		new Select(mainDriver.findElementById("product_data-durations-lead_time")).selectByValue(lt);

		String upc = row.get(0);
		mainDriver.findElementById("product_data-product_identifiers-upc").sendKeys(upc);
		mainDriver.findElementById("product_data-product_identifiers-mpn").sendKeys(upc);
		mainDriver.findElementById("product_data-product_identifiers-sku").sendKeys(row.get(1));
		mainDriver.findElementById("product_data-product_identifiers-merchant_id").sendKeys(upc);

		mainDriver.findElementById("product_data-product_attributes-color").sendKeys(row.get(16));
		mainDriver.findElementById("product_data-product_attributes-material").sendKeys(row.get(15));
		mainDriver.findElementById("product_data-product_attributes-pattern").sendKeys(row.get(17));
		mainDriver.findElementById("product_data-product_attributes-size").sendKeys(row.get(42));
		String kwd = row.get(13);
		mainDriver.findElementById("product_data-product_attributes-keywords").sendKeys(kwd.substring(0, Math.min(kwd.length(), 240)));
		mainDriver.findElementById("product_data-shipping-shipping_dimensions_x").sendKeys(row.get(38));
		mainDriver.findElementById("product_data-shipping-shipping_dimensions_y").sendKeys(row.get(39));
		mainDriver.findElementById("product_data-shipping-shipping_dimensions_z").sendKeys(row.get(40));
		mainDriver.findElementById("product_data-shipping-shipping_weight").sendKeys(row.get(37));
		Thread.sleep(1000);
		mainDriver.findElementById("product_data-submit_button").click();
		Thread.sleep(1000);
		getTrusted("confirm-submit_button", mainDriver).click();
		Thread.sleep(3000);
		// Add Url
	}

	private static WebElement getTrusted(String id, AquaticaDriver mainDriver) throws InterruptedException {
		while (mainDriver.findElementsById(id).isEmpty()) {
			Thread.sleep(500);
		}
		return mainDriver.findElementById(id);
	}

	private static void addText(String key, String text, AquaticaDriver mainDriver) throws InterruptedException {
		text = text.replace("'", "`").replace("~~~", "\n");
		while (text.length() > 1650) {
			text = text.substring(0, text.lastIndexOf('\n'));
		}
		mainDriver.findElementByXPath("//div[contains(@class,'ui-dialog')]//td[@class='iconTxt']//img[contains(@id,'" + key + "')]").click();
		Thread.sleep(3000);
		Iterator<String> iterator = mainDriver.getWindowHandles().iterator();
		String handle = iterator.next();
		while (iterator.hasNext()) {
			handle = iterator.next();
		}
		mainDriver.switchTo().window(handle);
		Thread.sleep(1000);
		mainDriver.findElementsByXPath("//textArea").get(0).sendKeys(text);
		mainDriver.findElementByXPath("//img[@alt='Enregistrer']").click();
		Thread.sleep(2000);
	}

	private static void addImages(List<String> images, AquaticaDriver mainDriver) throws InterruptedException {
		mainDriver.findElementsByXPath("//div[contains(@class,'ui-dialog')]//div[@class='productActions']//span").get(0).click();
		Thread.sleep(1000);
		List<WebElement> fields = mainDriver.findElementsByXPath("//li[@class='input-upload-link']//input");
		int fieldIndex = 0;
		for (int i = 0; i < images.size(); i++) {
			for (int x = 0; x < fields.get(fieldIndex).getText().length(); x++) {
				fields.get(fieldIndex).sendKeys("\b");
			}
			fields.get(fieldIndex).sendKeys(images.get(i));
			if (fieldIndex == 5) {
				fieldIndex = 0;
				WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
				uploadButton.click();
				do {
					Thread.sleep(1000);
				} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
				Thread.sleep(1000);
			} else {
				fieldIndex++;
			}
		}
		if (fieldIndex > 0) {
			fieldIndex = 0;
			WebElement uploadButton = mainDriver.findElementByXPath("//input[@id='js-submit-upload']");
			uploadButton.click();
			do {
				Thread.sleep(1000);
			} while (!mainDriver.findElementByXPath("//div[@id='upload-ajax-loader']").getCssValue("display").equals("none"));
			Thread.sleep(1000);
		}
		mainDriver.findElementsByXPath("//a[contains(@class,'ui-dialog-titlebar-close')]").get(1).click();
		Thread.sleep(1000);
	}
}
