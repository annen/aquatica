package com.Aquatica;

import static com.Aquatica.window.Settings.CHROME_DRIVER_PATH;
import static com.Aquatica.window.Settings.PROXY_SERVER;
import static com.Aquatica.window.Settings.CHROME_PROFILE_PATH;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import com.Aquatica.drivers.AjaxDriver;
import com.Aquatica.drivers.AquaticaDriver;
import com.Aquatica.drivers.CrmDataDriver;
import com.Aquatica.window.Settings;

public class DRIVER {
	
	private static final Set<AquaticaDriver> drivers = new HashSet<AquaticaDriver>();
	private static final Set<CrmDataDriver> crmDrivers = new HashSet<CrmDataDriver>();
	private static final Set<AjaxDriver> ajaxDrivers = new HashSet<AjaxDriver>();
	private static final Set<WebDriver> platformDrivers = new HashSet<WebDriver>();
	
	private static Lock aquaticaLock = new ReentrantLock();
	private static Lock crmLock = new ReentrantLock();
	private static Lock ajaxLock = new ReentrantLock();
	private static Lock platformLock = new ReentrantLock();
	
	private static AquaticaDriver getOrNew() {
		if (drivers.isEmpty())
			return new AquaticaDriver();
		else {
			AquaticaDriver result = drivers.iterator().next();
			drivers.remove(result);
			return result;
		}
	}
	
	private static CrmDataDriver getOrNewCrm() {
		if (crmDrivers.isEmpty())
			return new CrmDataDriver();
		else {
			CrmDataDriver result = crmDrivers.iterator().next();
			crmDrivers.remove(result);
			return result;
		}
	}
	
	private static AjaxDriver getOrNewAjax() {
		if (ajaxDrivers.isEmpty())
			return new AjaxDriver();
		else {
			AjaxDriver result = ajaxDrivers.iterator().next();
			ajaxDrivers.remove(result);
			return result;
		}
	}
	
	private static WebDriver getOrNewPlatform(boolean proxy, boolean useProfile) {
		if (platformDrivers.isEmpty()) {
			System.setProperty("webdriver.chrome.driver", Settings.get(CHROME_DRIVER_PATH));
			ChromeOptions options = new ChromeOptions();
			if (useProfile) {
				options.addArguments("user-data-dir=" + Settings.get(CHROME_PROFILE_PATH));
			}
			if (proxy) {
				options.addArguments("--proxy-server=http://" + Settings.get(PROXY_SERVER));
			}
			options.addArguments("--start-maximized");
			switch (Settings.get(Settings.DRIVER)) {
			case "FIREFOX":
				return new FirefoxDriver();
			case "CHROME":
				return new ChromeDriver(options);
			case "OPERA":
				return new OperaDriver();
			case "EDGE":
				return new EdgeDriver();
			case "IE":
				return new InternetExplorerDriver();
			default:
				return new FirefoxDriver();
			}
		} else {
			WebDriver result = platformDrivers.iterator().next();
			platformDrivers.remove(result);
			return result;
		}
	}
	
	private static WebDriver getOrNewAnonymusPlatform(String proxy, String userAgent) {
		System.setProperty("webdriver.chrome.driver", Settings.get(CHROME_DRIVER_PATH));
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--proxy-server=http://" + proxy);
		options.addArguments("--user-agent=" + userAgent);
		options.addArguments("--start-maximized");
		return new ChromeDriver(options);
	}
	
	public static AquaticaDriver getDriver() {
		aquaticaLock.lock();
		AquaticaDriver result = getOrNew();
		aquaticaLock.unlock();
		return result;
	}
	
	public static AquaticaDriver getDetailsDriver() {
		aquaticaLock.lock();
		AquaticaDriver result = getOrNew();
		aquaticaLock.unlock();
		return result;
	}
	
	public static CrmDataDriver getCrmDriver() {
		crmLock.lock();
		CrmDataDriver result = getOrNewCrm();
		crmLock.unlock();
		return result;
	}
	
	public static AjaxDriver getAjaxDriver() {
		ajaxLock.lock();
		AjaxDriver result = getOrNewAjax();
		ajaxLock.unlock();
		return result;
	}
	
	public static WebDriver getPlatformDriver() {
		platformLock.lock();
		WebDriver result = getOrNewPlatform(false, true);
		platformLock.unlock();
		return result;
	}
	
	public static WebDriver getPlatformDriverWithProxy() {
		platformLock.lock();
		WebDriver result = getOrNewPlatform(true, true);
		platformLock.unlock();
		return result;
	}
	
	public static WebDriver getPlatformDriverWithProxy(boolean useProfile) {
		platformLock.lock();
		WebDriver result = getOrNewPlatform(true, useProfile);
		platformLock.unlock();
		return result;
	}
	
	public static WebDriver getPlatformForMothersBot(String proxy, String userAgent) {
		platformLock.lock();
		WebDriver result = getOrNewAnonymusPlatform(proxy, userAgent);
		platformLock.unlock();
		return result;
	}
	
	public static <T> void freeDriver(T driver, Set<T> drivers, Lock lock) {
		lock.lock();
		drivers.add(driver);
		lock.unlock();
	}
	
	public static void freeMainDriver(AquaticaDriver driver) {
		freeDriver(driver, drivers, aquaticaLock);
	}
	
	public static void freeDetailsDriver(AquaticaDriver driver) {
		freeDriver(driver, drivers, aquaticaLock);
	}
	
	public static void freeCrmDriver(CrmDataDriver driver) {
		freeDriver(driver, crmDrivers, crmLock);
	}
	
	public static void freeAjaxDriver(AjaxDriver driver) {
		freeDriver(driver, ajaxDrivers, ajaxLock);
	}
	
	public static void freePlatformDriver(WebDriver driver) {
		freeDriver(driver, platformDrivers, platformLock);
	}
}
