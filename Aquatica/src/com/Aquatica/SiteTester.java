package com.Aquatica;

import com.Aquatica.drivers.AquaticaDriver;

public interface SiteTester {

	public interface ProcessListener {
		void allFinish(TestResult result);
		
		void itemFinish(Item item);
	}

	TestResult test(AquaticaDriver mainDriver, AquaticaDriver detailsDriver) throws Exception;
	
	void addProcessListener(ProcessListener listener);
}
