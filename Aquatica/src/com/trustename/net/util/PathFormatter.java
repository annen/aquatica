package com.trustename.net.util;

import com.google.common.base.Function;

public final class PathFormatter {
	private final String pathTemplate;

	public PathFormatter(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}

	public String getPath(final String host, final int networkId, final String fileName) {
		return StringUtils.replaceVariables(pathTemplate, "{", "}", new Function<String, String>() {
			@Override
			public String apply(String input) {
				if (input.equals("network_id"))
					return Integer.toString(networkId);
				if (input.equals("host"))
					return host;
				if (input.equals("file"))
					return fileName;
				return "";
			}
		});
	}

	public String getPath(final int networkId) {
		return StringUtils.replaceVariables(pathTemplate, "{", "}", new Function<String, String>() {
			@Override
			public String apply(String input) {
				if (input.equals("network_id"))
					return Integer.toString(networkId);
				return "";
			}
		});
	}

}
