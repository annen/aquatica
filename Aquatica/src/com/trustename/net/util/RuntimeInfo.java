package com.trustename.net.util;

import java.lang.management.ManagementFactory;

public final class RuntimeInfo {

	private RuntimeInfo() {
	}

	public static String getInfo() {
		Runtime rt = Runtime.getRuntime();
		return "Load Average: " + ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage() + "\n" +
				"Free Memory: " + rt.freeMemory() / 1048576 + " MB" + "\n" +
				"Max Memory: " + rt.maxMemory() / 1048576 + " MB" + "\n" +
				"Total Memory: " + rt.totalMemory() / 1048576 + " MB";
	}

	public static long getAvailableMemory() {
		Runtime rt = Runtime.getRuntime();
		return rt.maxMemory() - rt.totalMemory() + rt.freeMemory();
	}
}
