package com.trustename.net.util;

public interface Factory<T> {
	T create();
}
