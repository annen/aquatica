package com.trustename.net.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;

import com.google.common.annotations.VisibleForTesting;

public abstract class ImageSizeMatcher {
	private final static double BIGGER_KOEFF = 2.0;
	private final static double SMALLER_KOEFF = 1.5;
	private final static double PROPORTION_KOEFF = 1.2;

	private final static ImageSizeMatcher EXACT_SIZE_MATCHER = new ExactSizeMatcher();
	private final static ImageSizeMatcher DEFAULT_FUZZY_SIZE_MATCHER = new FuzzySizeMatcher(BIGGER_KOEFF, SMALLER_KOEFF, PROPORTION_KOEFF);
	private final static ImageSizeMatcher MIN_SIZE_MATCHER = new FuzzySizeMatcher(10.0, 0.0, 1.12);

	public static ImageSizeMatcher exactSizeMatcher() {
		return EXACT_SIZE_MATCHER;
	}

	public static ImageSizeMatcher fuzzySizeMatcher(double biggerKoeff, double smallerKoeff, double proportionKoeff) {
		return new FuzzySizeMatcher(biggerKoeff, smallerKoeff, proportionKoeff);
	}

	public static ImageSizeMatcher defaultFuzzySizeMatcher() {
		return DEFAULT_FUZZY_SIZE_MATCHER;
	}

	public static ImageSizeMatcher minSizeMatcher() {
		return MIN_SIZE_MATCHER;
	}

	private static final class SizeComparator implements Comparator<Size> {
		static final SizeComparator INSTANCE = new SizeComparator();

		@Override
		public int compare(Size o1, Size o2) {
			int c = Integer.compare(o1.getWidth(), o2.getWidth());
			if (c != 0)
				return c;
			return Integer.compare(o1.getHeight(), o2.getHeight());
		}
	}

	@VisibleForTesting
	static double compareSize(Size s1, Size s2) {
		double w1 = Math.min(s1.getWidth(), s2.getWidth());
		double w2 = Math.max(s1.getWidth(), s2.getWidth());
		double wdiff = w2 / w1;
		double h1 = Math.min(s1.getHeight(), s2.getHeight());
		double h2 = Math.max(s1.getHeight(), s2.getHeight());
		double hdiff = h2 / h1;
		return Math.max(wdiff, hdiff);
	}

	@VisibleForTesting
	static double compareProportion(Size s1, Size s2) {
		double p1 = calcProportion(s1);
		double p2 = calcProportion(s2);
		return Math.max(p1, p2) / Math.min(p1, p2);
	}

	private static double calcProportion(Size s) {
		double w = s.getWidth();
		return w / s.getHeight();
	}

	public <T> T match(Map<Size, T> available, Size required) {
		Size size = match(available.keySet(), required);
		if (size == null)
			return null;
		return available.get(size);
	}

	public Size match(Collection<Size> available, Size required) {
		if (required == null || available.isEmpty())
			return null;
		if (available.contains(required))
			return required;
		return fuzzyMatch(available, required);
	}

	protected abstract Size fuzzyMatch(Collection<Size> available, Size required);

	private static class ExactSizeMatcher extends ImageSizeMatcher {

		@Override
		protected Size fuzzyMatch(Collection<Size> available, Size required) {
			return null;
		}

	}

	private static class FuzzySizeMatcher extends ImageSizeMatcher {
		private final double biggerKoeff;
		private final double smallerKoeff;
		private final double proportionKoeff;

		FuzzySizeMatcher(double biggerKoeff, double smallerKoeff, double proportionKoeff) {
			this.biggerKoeff = biggerKoeff;
			this.smallerKoeff = smallerKoeff;
			this.proportionKoeff = proportionKoeff;
		}

		@Override
		protected Size fuzzyMatch(Collection<Size> available, Size required) {
			TreeSet<Size> ts = new TreeSet<Size>(SizeComparator.INSTANCE);
			ts.addAll(available);
			Size size = matchBigger(ts.tailSet(required), required);
			if (size != null)
				return size;
			size = matchSmaller(ts.headSet(required), required);
			return size;
		}

		private Size matchBigger(Collection<Size> sizes, Size required) {
			for (Size size : sizes) {
				if (compareSize(size, required) > biggerKoeff)
					break;
				if (compareProportion(size, required) <= proportionKoeff)
					return size;
			}
			return null;
		}

		private Size matchSmaller(Collection<Size> sizes, Size required) {
			for (Size size : sizes) {
				if (compareSize(size, required) > smallerKoeff)
					break;
				if (compareProportion(size, required) <= proportionKoeff)
					return size;
			}
			return null;
		}

	}

}
