package com.trustename.net.util.value;

public interface Value<T> {
	T get();
}
