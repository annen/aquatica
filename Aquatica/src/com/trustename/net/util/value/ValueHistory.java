package com.trustename.net.util.value;

import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.joda.time.LocalDate;

/**
 * Stores value which changed over time
 * Allows get appropriable value for a date
 */
public final class ValueHistory<T> {
	private T defaultValue;
	private final NavigableMap<LocalDate, T> history = new TreeMap<LocalDate, T>();

	public void setDefaultValue(T value) {
		this.defaultValue = value;
	}

	public void setHistoryValue(LocalDate date, T value) {
		history.put(date, value);
	}

	public T get(LocalDate date) {
		if (history.isEmpty())
			return defaultValue;
		Entry<LocalDate, T> e = history.floorEntry(date);
		if (e != null)
			return e.getValue();
		return null;
	}

	@Override
	public String toString() {
		return String.valueOf(defaultValue);
	}

}
