package com.trustename.net.util.value;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Collection;
import java.util.Map;

import org.joda.time.LocalDate;

public class EntryValueHistory<K, V> {
	private final Map<K, ValueHistory<V>> itemHistory = newHashMap();

	private ValueHistory<V> ensureValueHistory(K key) {
		ValueHistory<V> history = itemHistory.get(key);
		if (history == null) {
			history = new ValueHistory<V>();
			itemHistory.put(key, history);
		}
		return history;
	}

	public void addDefaultValue(K key, V value) {
		ValueHistory<V> valueHistory = ensureValueHistory(key);
		valueHistory.setDefaultValue(value);
	}

	public void addHistoryValue(K key, LocalDate date, V value) {
		ValueHistory<V> valueHistory = ensureValueHistory(key);
		valueHistory.setHistoryValue(date, value);
	}

	public V get(K key, LocalDate date) {
		ValueHistory<V> history = itemHistory.get(key);
		if (history == null)
			return null;
		return history.get(date);
	}

	public V get(K key, LocalDate date, V defaultValue) {
		V value = get(key, date);
		if (value == null)
			return defaultValue;
		return value;
	}

	public Collection<K> getKeys() {
		return itemHistory.keySet();
	}

	@Override
	public String toString() {
		return itemHistory.toString();
	}

}
