package com.trustename.net.util.value;


public interface ReloadingValue<T> extends Value<T> {
	void reload();
}
