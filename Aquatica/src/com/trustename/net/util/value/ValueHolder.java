package com.trustename.net.util.value;

public final class ValueHolder<T> {
	private volatile T value;

	public ValueHolder() {
	}

	public ValueHolder(T value) {
		this.value = value;
	}

	public T get() {
		return value;
	}

	public void set(T value) {
		this.value = value;
	}
}
