package com.trustename.net.util.value;

import static com.google.common.base.Preconditions.checkNotNull;

import org.joda.time.Period;


public class CacheValue<T> implements Value<T> {

	public static <T> CacheValue<T> of(ValueReloader<T> reloader, Period ttl) {
		return new CacheValue<T>(reloader, ttl);
	}

	private final ValueReloader<T> reloader;
	private final long ttl;
	private volatile T value;
	private volatile long lastLoad;

	public CacheValue(ValueReloader<T> reloader, Period ttl) {
		this.reloader = reloader;
		this.ttl = ttl == null ? 0 : ttl.toStandardDuration().getMillis();
	}

	@Override
	public T get() {
		if (isExpired()) {
			synchronized (this) {
				if (isExpired())
					value = load(value);
			}
		}
		return value;
	}

	private boolean isExpired() {
		if (value == null)
			return true;
		if (ttl == 0)
			return false;
		return lastLoad + ttl < System.currentTimeMillis();
	}

	private T load(T prevValue) {
		try {
			T result = reloader.reload(prevValue);
			checkNotNull(result);
			lastLoad = System.currentTimeMillis();
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
