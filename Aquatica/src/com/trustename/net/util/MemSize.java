package com.trustename.net.util;

import static com.google.common.base.Preconditions.checkArgument;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MemSize {
	private static final Pattern PATTERN = Pattern.compile("([0-9]+([\\.][0-9]+)?)\\s*(|K|M|G|T)B?", Pattern.CASE_INSENSITIVE);
	private final long bytes;

	private MemSize(long bytes) {
		checkArgument(bytes >= 0, "Bytes cannot be negative.");
		this.bytes = bytes;
	}

	public static MemSize bytes(long bytes) {
		return new MemSize(bytes);
	}

	public static MemSize kilobytes(long kilobytes) {
		return bytes(kilobytes * 1024);
	}

	public static MemSize megabytes(long megabytes) {
		return kilobytes(megabytes * 1024);
	}

	public static MemSize gigabytes(long gigabytes) {
		return megabytes(gigabytes * 1024);
	}

	public static MemSize terabytes(long terabytes) {
		return gigabytes(terabytes * 1024);
	}

	public static MemSize bytes(double bytes) {
		return bytes(Math.round(bytes));
	}

	public static MemSize kilobytes(double kilobytes) {
		return bytes(kilobytes * 1024.0);
	}

	public static MemSize megabytes(double megabytes) {
		return kilobytes(megabytes * 1024.0);
	}

	public static MemSize gigabytes(double gigabytes) {
		return megabytes(gigabytes * 1024.0);
	}

	public static MemSize terabytes(double terabytes) {
		return gigabytes(terabytes * 1024.0);
	}

	public long getBytes() {
		return bytes;
	}

	public double getKilobytes() {
		return bytes / 1024.0;
	}

	public double getMegabytes() {
		return getKilobytes() / 1024.0;
	}

	public double getGigabytes() {
		return getMegabytes() / 1024.0;
	}

	public double getTerabytes() {
		return getGigabytes() / 1024.0;
	}

	public static MemSize valueOf(String string) {
		Matcher matcher = PATTERN.matcher(string);
		if (matcher.matches()) {
			try {
				double value = Double.valueOf(matcher.group(1));
				String units = matcher.group(3).toLowerCase();

				switch (units) {
					case "":
						return bytes(value);
					case "k":
						return kilobytes(value);
					case "m":
						return megabytes(value);
					case "g":
						return gigabytes(value);
					case "t":
						return terabytes(value);
					default:
						throw new IllegalArgumentException("Units not recognized: " + string);
				}
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Unable to parse numeric part: " + string, e);
			}
		} else {
			throw new IllegalArgumentException("Unable to parse bytes: " + string);
		}
	}

	@Override
	public String toString() {
		return format();
	}

	public String format() {
		double terabytes = getTerabytes();
		if (terabytes >= 1.0)
			return format(terabytes) + "Tb";

		double gigabytes = getGigabytes();
		if (gigabytes >= 1.0)
			return format(gigabytes) + "Gb";

		double megabytes = getMegabytes();
		if (megabytes >= 1.0)
			return format(megabytes) + "Mb";

		double kilobytes = getKilobytes();
		if (kilobytes >= 1.0)
			return format(kilobytes) + "Kb";

		return bytes + "b";
	}

	private static String format(double value) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
		df.applyPattern("###.###");
		return df.format(value);
	}
}