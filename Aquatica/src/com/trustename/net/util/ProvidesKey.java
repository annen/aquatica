package com.trustename.net.util;

public interface ProvidesKey<K, T> {
	K getKey(T item);
}
