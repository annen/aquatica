package com.trustename.net.util;

import com.google.common.base.Function;

public final class StringFunctions {
	private StringFunctions() {
	}

	public static Function<String, String> upperFunction() {
		return UpperFunction.INSTANCE;
	}

	private enum UpperFunction implements Function<String, String> {
		INSTANCE;

		@Override
		public String apply(String s) {
			if (s == null)
				return null;
			return s.toUpperCase();
		}
	}

	public static Function<String, String> lowerFunction() {
		return LowerFunction.INSTANCE;
	}

	private enum LowerFunction implements Function<String, String> {
		INSTANCE;

		@Override
		public String apply(String s) {
			if (s == null)
				return null;
			return s.toLowerCase();
		}
	}

}
