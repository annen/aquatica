package com.trustename.net.util;

import static com.trustename.net.util.ArgValidation.validateHexNumber;
import static com.trustename.net.util.ArgValidation.validateMd5Hash;
import static com.trustename.net.util.ArgValidation.validateNotEmpty;
import static com.trustename.net.util.ArgValidation.validateNotNull;
import static com.trustename.net.util.ArgValidation.validateNotNumber;
import static com.trustename.net.util.ArgValidation.validatePositiveNumber;
import static com.trustename.net.util.ArgValidation.validateWithPattern;

import java.util.regex.Pattern;

public final class ArgChecks {

	private static void check(String error, String argument) {
		if (error != null)
			throw new IllegalArgumentException(argument + " " + error);
	}

	public static <T> T checkNotNull(T reference, String argument) {
		check(validateNotNull(reference), argument);
		return reference;
	}

	public static String checkNotEmpty(String s, String argument) {
		check(validateNotEmpty(s), argument);
		return s;
	}

	public static String checkWithPattern(String s, Pattern pattern, String argument) {
		check(validateWithPattern(s, pattern), argument);
		return s;
	}

	public static String checkPositiveNumber(String s, String argument) {
		check(validatePositiveNumber(s), argument);
		return s;
	}

	public static String checkNotNumber(String s, String argument) {
		check(validateNotNumber(s), argument);
		return s;
	}

	public static String checkMd5Hash(String s, String argument) {
		check(validateMd5Hash(s), argument);
		return s;
	}

	public static String checkHexNumber(String s, int minLen, int maxLen, String argument) {
		check(validateHexNumber(s, minLen, maxLen), argument);
		return s;
	}

	public static String checkHexNumber(String s, String argument) {
		return checkHexNumber(s, 0, Integer.MAX_VALUE, argument);
	}
}
