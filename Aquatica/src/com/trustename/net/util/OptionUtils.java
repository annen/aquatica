package com.trustename.net.util;

import java.util.Collection;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.collect.Collections2;

public final class OptionUtils {
	private static final Splitter splitComma = Splitter.on(',').trimResults();
	private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");

	public static Collection<String> parseStrings(String s) {
		return splitComma.splitToList(s);
	}

	public static Collection<Integer> parseInts(String s) {
		return Collections2.transform(parseStrings(s), new Function<String, Integer>() {
			@Override
			public Integer apply(String input) {
				return Integer.parseInt(input);
			}
		});
	}

	public static LocalDateRange parseDateRange(String s) {
		int index = s.indexOf('_');
		if (index < 0)
			return new LocalDateRange(dateFormat.parseLocalDate(s));
		if (index == 0) {
			LocalDate date = dateFormat.parseLocalDate(s.substring(1));
			return new LocalDateRange(new LocalDate(), date);
		}
		if (index == s.length() - 1) {
			LocalDate date = dateFormat.parseLocalDate(s.substring(0, s.length() - 1));
			return new LocalDateRange(date, new LocalDate());
		}
		LocalDate date1 = dateFormat.parseLocalDate(s.substring(0, index));
		LocalDate date2 = dateFormat.parseLocalDate(s.substring(index + 1));
		return new LocalDateRange(date1, date2);
	}

	public static LocalDateRange parseStartDate(String s) {
		LocalDate date = dateFormat.parseLocalDate(s);
		return new LocalDateRange(date, new LocalDate());
	}

	public static LocalDateRange parseDatePeriodDays(String s) {
		int daysAfter = 0;
		int index = s.indexOf('-');
		if (index > 0) {
			daysAfter = Integer.parseInt(s.substring(index + 1));
			s = s.substring(0, index);
		}
		int days = Integer.parseInt(s);
		LocalDate date = new LocalDate();
		return new LocalDateRange(date.minusDays(days), date.minusDays(daysAfter));
	}

}
