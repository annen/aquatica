package com.trustename.net.util;

public interface HasTypeInfo<T> {

	Class<T> getTypeClass();

}
