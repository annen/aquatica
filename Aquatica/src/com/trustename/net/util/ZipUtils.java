package com.trustename.net.util;

import java.util.zip.*;

public final class ZipUtils {

	public static int compress(byte[] inBuffer, int inOffset, int inSize, byte[] outBuffer, int level) {
		Deflater deflater = new Deflater(level, true);
		deflater.setInput(inBuffer, inOffset, inSize);
		deflater.finish();
		int compressedSize = deflater.deflate(outBuffer);
		if (!deflater.finished())
			throw new IllegalArgumentException("Output buffer " + outBuffer.length + " is too small");
		deflater.end();
		return compressedSize;
	}

	public static int compress(byte[] inBuffer, byte[] outBuffer, int level) {
		return compress(inBuffer, 0, inBuffer.length, outBuffer, level);
	}

	public static int decompress(byte[] buffer, int offset, int size, byte[] outBuffer) throws DataFormatException {
		Inflater inflater = new Inflater(true);
		inflater.setInput(buffer, offset, size);
		int decomressedSize = inflater.inflate(outBuffer);
		if (!inflater.finished())
			throw new IllegalArgumentException("Output buffer " + outBuffer.length + " is too small");
		inflater.end();
		return decomressedSize;
	}

	public static int decompress(byte[] buffer, byte[] outBuffer) throws DataFormatException {
		return decompress(buffer, 0, buffer.length, outBuffer);
	}

}
