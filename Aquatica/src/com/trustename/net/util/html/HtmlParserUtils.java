package com.trustename.net.util.html;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public final class HtmlParserUtils {
	public static final Pattern patternEndTable = Pattern.compile("</table>", Pattern.CASE_INSENSITIVE);
	public static final Pattern patternEndSelect = Pattern.compile("</select>", Pattern.CASE_INSENSITIVE);
	public static final Pattern patternEndForm = Pattern.compile("</form>", Pattern.CASE_INSENSITIVE);
	public static final Pattern patternEndDiv = Pattern.compile("</div>", Pattern.CASE_INSENSITIVE);

	public static final Pattern patternSelectOption = Pattern.compile("<option\\s+value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>(.*?)</option>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternInput = Pattern.compile("<input([^>]*)>", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternInputType = Pattern.compile("\\s+type\\s*=\\s*['\"]?([^'\"\\s>]+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternInputName = Pattern.compile("\\s+name\\s*=\\s*['\"]?([^'\"\\s>]+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternInputValue = Pattern.compile("\\s+value\\s*=\\s*['\"]?([^'\"]*)['\"\\s/]?", Pattern.CASE_INSENSITIVE);
	private static final Pattern patternHtmlTag = Pattern.compile("<[^>]*>");
	private static final Pattern HTML_COMMENT = Pattern.compile("<!--.*?-->", Pattern.DOTALL);

	public static String extractAreaSafe(String html, Pattern begin, Pattern end) {
		Matcher m = begin.matcher(html);
		if (!m.find())
			return null;
		html = html.substring(m.end());
		m = end.matcher(html);
		if (!m.find())
			return null;
		html = html.substring(0, m.start());
		return html;
	}

	public static String extractArea(String html, Pattern begin, Pattern end, String name) throws Exception {
		Matcher m = begin.matcher(html);
		if (!m.find())
			throw new Exception("Begin of " + name + " area is not found");
		html = html.substring(m.end());
		m = end.matcher(html);
		if (!m.find())
			throw new Exception("End of " + name + " area is not found");
		html = html.substring(0, m.start());
		return html;
	}

	public static String extractArea(String html, String begin, String end, String name) throws Exception {
		int index1 = html.indexOf(begin);
		if (index1 < 0)
			throw new Exception("Begin of " + name + " area is not found");
		index1 += begin.length();
		int index2 = html.indexOf(end, index1);
		if (index2 < 0)
			throw new Exception("End of " + name + " area is not found");
		html = html.substring(index1, index2);
		return html;
	}

	// returns null if tag not found
	public static String extractInnerHtml(String html, String tag) throws Exception {
		final String beginTag = "<" + tag;
		final String endTag = "</" + tag + ">";
		Pair<Integer, Integer> pos = findTagInnerHtml(html, beginTag, endTag, 0);
		if (pos == null)
			return null;
		if (pos.getA().equals(pos.getB()))
			return "";
		return html.substring(pos.getA(), pos.getB());
	}

	// returns Pair<startPos, endPos>
	// returns null if tag is not found
	private static Pair<Integer, Integer> findTagInnerHtml(String html, String beginTag, String endTag, int pos) throws Exception {
		pos = html.indexOf(beginTag, pos);
		if (pos < 0)
			return null;
		pos = html.indexOf('>', (pos + beginTag.length()) - 1);
		if (pos < 0)
			throw new Exception("Could not find end of open tag " + beginTag);
		++pos;
		int beginTagPos = pos;
		int endTagPos = pos;
		while (true) {
			int pos2 = html.indexOf(endTag, endTagPos);
			if (pos2 < 0)
				throw new Exception("Could not find close tag " + endTag);
			int innerTagPos = html.indexOf(beginTag, beginTagPos);
			if (innerTagPos == -1 || innerTagPos > pos2)
				return new Pair<>(pos, pos2);

			beginTagPos = innerTagPos + beginTag.length();
			endTagPos = pos2 + endTag.length();
		}
	}

	public static Map<String, String> parseSelectOptions(String s) throws Exception {
		Map<String, String> options = Maps.newLinkedHashMap();
		Matcher m = patternSelectOption.matcher(s);
		while (m.find()) {
			options.put(m.group(1), m.group(2));
		}
		return options;
	}

	public static Map<String, String> parseInputElements(String s) throws Exception {
		Map<String, String> elements = Maps.newLinkedHashMap();
		Matcher mInput = patternInput.matcher(s);
		while (mInput.find()) {
			String sInput = mInput.group(1);
			Matcher mType = patternInputType.matcher(sInput);
			if (!mType.find())
				throw new Exception("Input element's type is not found: " + sInput);
			String type = mType.group(1).toLowerCase();
			if (type.equals("button") || type.equals("image")) {
				continue;
			}
			Matcher mName = patternInputName.matcher(sInput);
			if (!mName.find()) {
				continue;
			}
			String name = mName.group(1);
			Matcher mValue = patternInputValue.matcher(sInput);
			String value = null;
			if (mValue.find()) {
				value = mValue.group(1);
			}
			elements.put(name, value);
		}
		return elements;
	}

	public static Multimap<String, String> parseInputMultiElements(String s) throws Exception {
		Multimap<String, String> elements = LinkedHashMultimap.create();
		Matcher mInput = patternInput.matcher(s);
		while (mInput.find()) {
			String sInput = mInput.group(1);
			Matcher mType = patternInputType.matcher(sInput);
			if (!mType.find())
				throw new Exception("Input element's type is not found: " + sInput);
			String type = mType.group(1).toLowerCase();
			if (type.equals("button") || type.equals("image")) {
				continue;
			}
			Matcher mName = patternInputName.matcher(sInput);
			if (!mName.find()) {
				continue;
			}
			String name = mName.group(1);
			Matcher mValue = patternInputValue.matcher(sInput);
			String value = null;
			if (mValue.find()) {
				value = mValue.group(1);
			}
			elements.put(name, value);
		}
		return elements;
	}

	public static List<String> parseTagsInnerHtml(String html, Pattern pattern) throws Exception {
		List<String> res = Lists.newArrayList();
		Matcher m = pattern.matcher(html);
		while (m.find()) {
			res.add(m.group(1));
		}
		return res;
	}

	public static List<String> parseTagsInnerHtml(String html, String beginTag, String endTag) throws Exception {
		List<String> res = Lists.newArrayList();
		int pos = 0;
		while (true) {
			Pair<Integer, Integer> poses = findTagInnerHtml(html, beginTag, endTag, pos);
			if (poses == null) {
				break;
			}
			String s = poses.getA().equals(poses.getB()) ? "" : html.substring(poses.getA(), poses.getB());
			res.add(s);
			pos = poses.getB() + endTag.length();
		}
		return res;
	}

	public static List<String> parseTagsInnerHtml(String html, String tag) throws Exception {
		final String beginTag = "<" + tag;
		final String endTag = "</" + tag + ">";
		return parseTagsInnerHtml(html, beginTag, endTag);
	}

	public static String getInnerText(String html) {
		Matcher m = patternHtmlTag.matcher(html);
		return m.replaceAll("");
	}

	public static void checkTableColumns(String thead, List<String> tds, String[] reportHeader) throws Exception {
		if (tds.size() != reportHeader.length)
			throw new Exception("Invalid report columns size. Expected " + reportHeader.length + ", but was " + tds.size() + ". HTML: " + thead);
		for (int i = 0; i < tds.size(); i++) {
			String td = HtmlParserUtils.getInnerText(tds.get(i)).trim();
			if (!td.equals(reportHeader[i]))
				throw new Exception("Invalid report column. Expected '" + reportHeader[i] + "', but was '" + td + "'. HTML: " + thead);
		}
	}

	public static void checkTableColumns(String thead, String[] reportHeader) throws Exception {
		if (thead == null)
			throw new NullPointerException("thead");
		List<String> tds = parseTagsInnerHtml(thead, "th");
		if (tds.isEmpty()) {
			tds = parseTds(thead);
		}
		checkTableColumns(thead, tds, reportHeader);
	}

	public static List<String> parseTrs(String table) throws Exception {
		return parseTagsInnerHtml(table, "tr");
	}

	public static List<String> parseTds(String row) throws Exception {
		return parseTagsInnerHtml(row, "td");
	}

	public static Map<String, String> parseForm(String html, Pattern patternBeginForm) throws Exception {
		html = extractArea(html, patternBeginForm, HtmlParserUtils.patternEndForm, "form");
		return parseInputElements(html);
	}

	public static Multimap<String, String> parseMultiForm(String html, Pattern patternBeginForm) throws Exception {
		html = extractArea(html, patternBeginForm, HtmlParserUtils.patternEndForm, "form");
		return parseInputMultiElements(html);
	}

	public static String removeComments(String html) {
		return HTML_COMMENT.matcher(html).replaceAll("");
	}
}
