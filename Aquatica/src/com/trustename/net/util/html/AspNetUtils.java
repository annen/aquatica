package com.trustename.net.util.html;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class AspNetUtils {

	public static final String EVENTTARGET = "__EVENTTARGET";
	public static final String EVENTARGUMENT = "__EVENTARGUMENT";
	public static final String VIEWSTATE = "__VIEWSTATE";

	private static final Pattern patternDefaultAspnetForm = Pattern.compile("<form[^>]*>");

	public static Map<String, String> parseAspnetForm(String html) throws Exception {
		return parseAspnetForm(html, patternDefaultAspnetForm);
	}

	public static Map<String, String> parseAspnetForm(String html, String id) throws Exception {
		return parseAspnetForm(html, Pattern.compile("<form [^>]*id=\"" + id + "\"[^>]*>"));
	}

	public static Map<String, String> parseAspnetForm(String html, Pattern patternAspnetForm) throws Exception {
		html = HtmlParserUtils.extractArea(html, patternAspnetForm, HtmlParserUtils.patternEndForm, "aspnet form");
		Map<String, String> elements = HtmlParserUtils.parseInputElements(html);
		if (!elements.containsKey(VIEWSTATE))
			throw new Exception("Not found option element: __VIEWSTATE at html form:\n" + html.substring(0, 200));
		return elements;
	}

	public static Map<String, String> getBaseAspnetFormArguments(String html) throws Exception {
		Map<String, String> form = parseAspnetForm(html);
		Map<String, String> baseForm = new HashMap<>();
		baseForm.put(EVENTTARGET, form.get(EVENTTARGET));
		baseForm.put(EVENTARGUMENT, form.get(EVENTARGUMENT));
		baseForm.put(VIEWSTATE, form.get(VIEWSTATE));
		return baseForm;
	}

	private AspNetUtils() {
	}
}
