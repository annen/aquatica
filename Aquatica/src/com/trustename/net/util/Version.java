package com.trustename.net.util;

public final class Version implements Comparable<Version> {

    public static Version parseVersion(String version) throws IllegalArgumentException {
	    int index = version.indexOf('.');
	    if (index <= 0)
		    throw new IllegalArgumentException(version);
	    short major = Short.parseShort(version.substring(0, index));
	    short minor = Short.parseShort(version.substring(index + 1));
        return new Version(major, minor);
    }

    public final short major;
    public final short minor;

    public Version(short major, short minor) {
        this.major = major;
        this.minor = minor;
    }

    @Override
    public String toString() {
        return major + "." + minor;
    }


	@Override
	public int hashCode() {
		return HashCodeUtils.hashCode(major, minor);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Version that = (Version) o;
		return this.major == that.major && this.minor == that.minor;
	}

    @Override
    public int compareTo(Version o) {
        int compare = Short.compare(this.major, o.major);
        if (compare != 0)
            return compare;
        return Short.compare(this.minor, o.minor);
    }
}