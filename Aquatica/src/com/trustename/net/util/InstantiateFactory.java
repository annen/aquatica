package com.trustename.net.util;

import java.lang.reflect.Constructor;

public class InstantiateFactory<T> implements Factory<T> {
	 
    private final Constructor<T> iConstructor;
 
    public InstantiateFactory(Class<T> classToInstantiate) {
        try {
            iConstructor = classToInstantiate.getConstructor();
        } catch (NoSuchMethodException ex) {
            throw new IllegalArgumentException("InstantiateFactory: The constructor must exist and be public ");
        }
    }
 
    public T create() {
        try {
            return iConstructor.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } 
    }
    
}
