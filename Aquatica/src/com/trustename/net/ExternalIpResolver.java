package com.trustename.net;

import java.net.InetAddress;

public interface ExternalIpResolver {
	InetAddress getExternalIp();
}
