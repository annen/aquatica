package com.trustename.net.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Multimap;
import com.google.common.net.MediaType;
import com.trustename.net.ProxyAuthenticator;
import com.trustename.net.SSLCertificateUtil;
import com.trustename.net.WebProxy;

public class HttpClient {
	private final CookiesManager cookiesManager;
	private final HttpHeaders persistentHeaders;
	private final WebProxy proxy;
	private final ProxyAuthenticator auth;
	private volatile boolean trustAllSslCertificates = false;

	private int readTimeout = 120 * 1000;
	private int connectTimeout = 30 * 1000;
	private boolean followRedirecs = true;
	private boolean checkHttpErrors = true;

	public HttpClient(CookiesManager cookiesManager, HttpHeaders persistentHeaders, WebProxy proxy) {
		this.cookiesManager = cookiesManager;
		this.persistentHeaders = persistentHeaders;
		this.proxy = proxy;
		this.auth = proxy == null ? null : proxy.getAuthenticator();
	}

	public HttpClient(CookiesManager cookiesManager, HttpHeaders persistentHeaders) {
		this(cookiesManager, persistentHeaders, null);
	}

	public HttpClient(WebProxy proxy) {
		this(null, null, proxy);
	}

	public HttpClient() {
		this(null, null, null);
	}

	public void setTrustAllSslCertificates(boolean trustAllSslCertificates) {
		this.trustAllSslCertificates = trustAllSslCertificates;
	}

	public boolean isTrustAllSslCertificates() {
		return trustAllSslCertificates;
	}

	public void setConnectTimeout(int timeout) {
		connectTimeout = timeout;
	}

	public void setReadTimeout(int timeout) {
		readTimeout = timeout;
	}

	public void setFollowRedirects(boolean followRedirecs) {
		this.followRedirecs = followRedirecs;
	}

	public void setCheckHttpErrors(boolean checkHttpErrors) {
		this.checkHttpErrors = checkHttpErrors;
	}

	public CookiesManager cookiesManager() {
		return cookiesManager;
	}

	public HttpResponse get(HttpRequest req) throws Exception {
		return request(req, "GET", null);
	}

	public HttpResponse get(String url) throws Exception {
		return get(new HttpRequest(url));
	}

	public static String urlQueryString(Map<String, String> q, String enc) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> e : q.entrySet()) {
			String name = encode(e.getKey(), enc);
			sb.append(name);
			if (e.getValue() != null) {
				sb.append('=');
				sb.append(encode(e.getValue(), enc));
			}
			sb.append('&');
		}
		if (sb.length() > 0)
			sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	public static String urlQueryString(Multimap<String, String> q, String enc) {
		StringBuilder sb = new StringBuilder();
		for (String key : q.keySet()) {
			Collection<String> values = q.get(key);
			for (String value : values) {
				sb.append(encode(key, enc));
				if (value != null) {
					sb.append('=');
					sb.append(encode(value, enc));
				}
				sb.append('&');
			}
		}
		if (sb.length() > 0)
			sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	private static String encode(String s, String enc) {
		try {
			return URLEncoder.encode(s, enc);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public HttpResponse post(HttpRequest req, Map<String, String> data) throws Exception {
		req.headers().set(com.google.common.net.HttpHeaders.CONTENT_TYPE, MediaType.FORM_DATA.toString());
		String s = data == null ? null : urlQueryString(data, "UTF-8");
		return request(req, "POST", s);
	}

	public HttpResponse post(String url, Map<String, String> data) throws Exception {
		return post(new HttpRequest(url), data);
	}

	public HttpResponse post(HttpRequest req, Multimap<String, String> data) throws Exception {
		req.headers().set(com.google.common.net.HttpHeaders.CONTENT_TYPE, MediaType.FORM_DATA.toString());
		String s = data == null ? null : urlQueryString(data, "UTF-8");
		return request(req, "POST", s);
	}

	public HttpResponse post(String url, Multimap<String, String> data) throws Exception {
		return post(new HttpRequest(url), data);
	}

	public HttpResponse request(HttpRequest req, String method, String data) throws Exception {
		if (auth != null)
			auth.register();
		try {
			return doRequest(req, method, data);
		} finally {
			if (auth != null)
				auth.unregister();
		}
	}

	private HttpResponse doRequest(HttpRequest req, String method, String data) throws Exception {
		HttpURLConnection conn;
		if (proxy == null)
			conn = (HttpURLConnection) req.url().openConnection();
		else
			conn = (HttpURLConnection) req.url().openConnection(proxy.getProxy());
		conn.setConnectTimeout(connectTimeout);
		conn.setReadTimeout(readTimeout);
		conn.setInstanceFollowRedirects(false);
		HttpHeaders headers = new HttpHeaders();
		if (persistentHeaders != null)
			headers.setAll(persistentHeaders);
		if (req.headers() != null)
			headers.setAll(req.headers());

		List<HttpCookie> cookies = req.getCookies();
		if (cookies == null && cookiesManager != null) {
			cookies = cookiesManager.getForHost(req.url().getHost());
		}
		if (cookies != null && !cookies.isEmpty())
			headers.set(com.google.common.net.HttpHeaders.COOKIE, join(cookies, "; "));

		applyHeaders(conn, headers);
		conn.setRequestMethod(method);
		if (data != null)
			conn.setDoOutput(true);

		if (trustAllSslCertificates) {
			SSLCertificateUtil.setTrustAll(conn);
		}
		conn.connect();
		if (data != null)
			writeData(conn, data);
		InputStream inputStream = getInputStream(conn);
		HttpResponse resp = new HttpResponse(conn, inputStream);
		if (cookiesManager != null)
			cookiesManager.set(resp.cookies(), req.url().getHost());
		if (followRedirecs && conn.getResponseCode() >= 300 && conn.getResponseCode() < 400) {
			String location = conn.getHeaderField(com.google.common.net.HttpHeaders.LOCATION);
			URL url = new URL(req.url(), location);
			resp.close();
			req = new HttpRequest(url, req.headers());
			return get(req);
		}
		return resp;
	}

	private InputStream getInputStream(HttpURLConnection conn) throws IOException {
		if (Thread.currentThread().isInterrupted())
			throw new IOException(new InterruptedException());
		if (checkHttpErrors) {
			if (conn.getResponseCode() >= 400 && conn.getResponseCode() != 404)
				throw new IOException("Server returned HTTP response code: " + conn.getResponseCode() + " (" + conn.getResponseMessage() + ") for URL: " + conn.getURL());
			return conn.getInputStream();
		}
		try {
			return conn.getInputStream();
		} catch (IOException e) {
			if (conn.getResponseCode() >= 200 && conn.getResponseCode() < 300)
				throw e;
			return conn.getErrorStream();
		}
	}

	private static void applyHeaders(HttpURLConnection conn, HttpHeaders headers) {
		for (Map.Entry<String, List<String>> e : headers.all().entrySet()) {
			String v = join(e.getValue(), ", ");
			conn.setRequestProperty(e.getKey(), v);
		}
	}

	private static void writeData(HttpURLConnection conn, String data) throws IOException {
		OutputStream os = conn.getOutputStream();
		try {
			OutputStreamWriter w = new OutputStreamWriter(os);
			w.write(data);
			w.close();
		} finally {
			if (os != null)
				os.close();
		}
	}

	private static <T> String join(final Iterable<T> objs, final String delimiter) {
		Iterator<T> iter = objs.iterator();
		if (!iter.hasNext())
			return "";
		StringBuffer buffer = new StringBuffer(String.valueOf(iter.next()));
		while (iter.hasNext())
			buffer.append(delimiter).append(String.valueOf(iter.next()));
		return buffer.toString();
	}
}
