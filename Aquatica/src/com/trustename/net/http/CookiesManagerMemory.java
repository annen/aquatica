package com.trustename.net.http;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/*
 * Simple implementation
 */
public class CookiesManagerMemory implements CookiesManager {
	private Set<HttpCookie> cookies = new LinkedHashSet<>();

	@Override
	public void set(HttpCookie c, String defaultDomain) {
		if (c.getDomain() == null)
			c.setDomain(defaultDomain);
		set(c);
	}

	@Override
	public synchronized void set(HttpCookie c) {
		if (c.getDomain() == null || c.getDomain().isEmpty())
			throw new IllegalArgumentException("Domain is not specified");
		cookies.remove(c);
		cookies.add(c);
	}

	@Override
	public void set(Collection<HttpCookie> cookies, String defaultDomain) {
		for (HttpCookie c : cookies)
			set(c, defaultDomain);
	}

	@Override
	public void set(Collection<HttpCookie> cookies) {
		set(cookies, null);
	}

	@Override
	public synchronized List<HttpCookie> getForHost(String host) {
		ArrayList<HttpCookie> res = new ArrayList<HttpCookie>();
		for (HttpCookie c : cookies) {
			if (c.getDiscard() || c.hasExpired())
				continue;
			if (matchByDomain(c.getDomain(), host))
				res.add(c);
		}
		return res;
	}

	private static boolean matchByDomain(String domain, String host) {
		if (domain.equalsIgnoreCase(host))
			return true;
		if (HttpCookie.domainMatches(domain, host))
			return true;
		if (HttpCookie.domainMatches("." + domain, host))
			return true;
		if (domain.startsWith(".") && host.endsWith(domain))
			return true;
		return false;
	}
}
