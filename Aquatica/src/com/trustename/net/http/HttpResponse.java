package com.trustename.net.http;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.CharStreams;

public final class HttpResponse implements Closeable {
	private static final Logger logger = LoggerFactory.getLogger(HttpResponse.class);
	private final HttpURLConnection conn;
	private final InputStream is;

	HttpResponse(HttpURLConnection c, InputStream is) {
		this.conn = c;
		this.is = is;
	}

	public HttpURLConnection connection() {
		return conn;
	}

	public URL url() {
		return conn.getURL();
	}

	public HttpHeaders headers() {
		return new HttpHeaders(conn.getHeaderFields());
	}

	public List<HttpCookie> cookies() {
		ArrayList<HttpCookie> res = new ArrayList<>();
		for (int i = 0; i < 1024; ++i) {
			String key = conn.getHeaderFieldKey(i);
			if (key == null)
				continue;
			if (key.equals(com.google.common.net.HttpHeaders.SET_COOKIE) || key.equals(com.google.common.net.HttpHeaders.SET_COOKIE2)) {
				String val = conn.getHeaderField(i);
				try {
					List<HttpCookie> cookies = HttpCookie.parse(val);
					res.addAll(cookies);
				} catch (IllegalArgumentException e) {
					logger.warn("Illegal cookie format: {}", val);
				}
			}
		}
		return res;
	}

	public int contentLength() {
		return conn.getContentLength();
	}

	public String contentEncoding() {
		return conn.getContentEncoding();
	}

	public InputStream stream() throws IOException {
		return is;
	}

	public String readAll() throws IOException {
		String encoding = contentEncoding();
		if (encoding == null)
			encoding = "UTF-8"; // "ISO-8859-1";
		if (encoding.equals("gzip")) {
			return readAllGzip();
		}
		return readAll(encoding);
	}

	public String readAll(String encoding) throws IOException {
		InputStream is = stream();
		try {
			InputStreamReader r = new InputStreamReader(is, encoding);
			String result = CharStreams.toString(r);
			return result;
		} finally {
			is.close();
		}
	}

	public String readAllGzip() throws IOException {
		InputStream is = new GZIPInputStream(stream(), 4096);
		try {
			InputStreamReader r = new InputStreamReader(is);
			String result = CharStreams.toString(r);
			return result;
		} finally {
			is.close();
		}
	}

	@Override
	public void close() {
		try {
			InputStream is = stream();
			is.close();
		} catch (IOException ex) {
		}
	}
}
