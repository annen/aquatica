package com.trustename.net;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public final class UrlEncode {
	private static final String ENCODING = "UTF-8";

	public static String encode(String s, String enc) {
		if (s == null)
			return null;
		try {
			return URLEncoder.encode(s, enc);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String encode(String s) {
		return encode(s, ENCODING);
	}

	public static String decode(String s, String enc) {
		if (s == null)
			return null;
		try {
			return URLDecoder.decode(s, enc);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String decode(String s) {
		return decode(s, ENCODING);
	}

}
