package com.trustename.net;

import java.io.IOException;
import java.net.Socket;

public interface BlockingSocketService {
	void serve(Socket socket) throws IOException;

	void stop();
}
