package com.aquatica;

import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.ExecutionException;

import javax.swing.table.DefaultTableModel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

public class XlsExporter {
	public void doExport(DefaultTableModel model, File file) throws Exception {
		Workbook wb = createWorkbook();
		addByTableModel(model, wb);

		FileOutputStream fileOut = new FileOutputStream(file);
		wb.write(fileOut);
		fileOut.close();
	}

	public void addByTableModel(DefaultTableModel model, Workbook wb) throws Exception {
		Sheet sheet = wb.createSheet("Table");
		int columnCount = model.getColumnCount();
		int rowCount = model.getRowCount();
		Row row = sheet.createRow(0);
		for (int c = 0; c < columnCount; c++) {
			header(wb, row, c, model.getColumnName(c));
		}
		for (int r = 0; r < rowCount; r++) {
			row = sheet.createRow(r + 1);
			for (int c = 0; c < columnCount; c++) {
				String value = (String) model.getValueAt(r, c);
				if (value.startsWith("   ")) {
					empty(wb, row, c);
				} else {
					createCell(row, c, value, "");
				}
			}
		}
		for (int i = 0; i < columnCount; i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void setComment(Workbook wb, Sheet sheet, Row row, String commentText, int countRows) {
		CreationHelper helper = wb.getCreationHelper();
		Cell cellI = row.getCell(3);
		Drawing drawing = sheet.createDrawingPatriarch();

		ClientAnchor anchor = helper.createClientAnchor();
		anchor.setCol1(cellI.getColumnIndex());
		anchor.setCol2(cellI.getColumnIndex() + 6);
		anchor.setRow1(cellI.getRowIndex());
		anchor.setRow2(cellI.getRowIndex() + countRows + 3);

		Comment comment = drawing.createCellComment(anchor);
		comment.setString(helper.createRichTextString(commentText));
		cellI.setCellComment(comment);
	}

	private void createCell(Row row, int i, Object value, Object defaultValue) {
		if (value == null) {
			createCell(row, i, defaultValue, "");
			return;
		}
		if (value instanceof String) {
			row.createCell(i).setCellValue((String) value);
		} else if (value instanceof Number) {
			row.createCell(i).setCellValue(((Number) value).doubleValue());
		} else if (value instanceof Boolean) {
			row.createCell(i).setCellValue((Boolean) value);
		} else {
			row.createCell(i).setCellValue(String.valueOf(value));
		}
	}

	private void header(Workbook wb, Row row, int i, String caption) throws ExecutionException {
		Cell cell = row.createCell(i);
		cell.setCellStyle(headerStyle);
		cell.setCellValue(caption);
	}

	private void empty(Workbook wb, Row row, int i) throws ExecutionException {
		Cell cell = row.createCell(i);
		cell.setCellStyle(emptyStyle);
	}

	private CellStyle headerStyle;
	private CellStyle emptyStyle;

	private HSSFWorkbook createWorkbook() {
		HSSFWorkbook wb = new HSSFWorkbook();

		headerStyle = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBold(true);
		headerStyle.setFont(font);
		headerStyle.setWrapText(true);
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);

		emptyStyle = wb.createCellStyle();
		emptyStyle.setFillBackgroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		emptyStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		emptyStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		return wb;
	}

}
