package com.aquatica;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.aquatica.Translater.LangFile;
import com.aquatica.Translater.SiteInfo;

public class SendFilesConfig extends JFrame {

	private JPanel contentPane;

	static class SiteCheckBox extends JCheckBox {
		SiteInfo siteInfo;
		LangFile file;
		DefaultListModel<SiteCheckBox> model;

		public SiteCheckBox(SiteInfo siteInfo, LangFile file) {
			super("   " + file.fileKey);
			this.file = file;
			this.siteInfo = siteInfo;
		}

		public SiteCheckBox(SiteInfo siteInfo) {
			super(siteInfo.host);
			this.siteInfo = siteInfo;
		}

		public void setParentModel(DefaultListModel<SiteCheckBox> model) {
			this.model = model;
		}

		@Override
		public void setSelected(boolean b) {
			super.setSelected(b);
			if (this.file == null) {
				for (int i = 0; i < model.getSize(); i++) {
					SiteCheckBox el = model.getElementAt(i);
					if (el.file != null && el.siteInfo == this.siteInfo) {
						el.setSelected(b);
					}
				}
			}
		}
	}

	/**
	 * Create the frame.
	 */
	public SendFilesConfig(List<SiteInfo> sites) {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 370, 713);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		CheckBoxList list = new CheckBoxList();
		list.setModel(buildModel(sites));
		scrollPane.setViewportView(list);

		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Map<SiteInfo, ArrayList<LangFile>> sendFiles = new LinkedHashMap<>();
				DefaultListModel<SiteCheckBox> model = (DefaultListModel<SiteCheckBox>) list.getModel();
				for (int i = 0; i < model.getSize(); i++) {
					SiteCheckBox el = model.getElementAt(i);
					if (el.file != null && el.isSelected()) {
						ArrayList<LangFile> files = sendFiles.computeIfAbsent(el.siteInfo, (s) -> new ArrayList<>());
						files.add(el.file);
					}
				}

				for (Entry<SiteInfo, ArrayList<LangFile>> site : sendFiles.entrySet()) {
					try {
						Utils.uploadTranslates(site.getKey(), site.getValue());
					} catch (Exception ex) {
						LOG.logError(ex, "Problem with send files to " + site.getKey().host);
					}
				}
				SendFilesConfig.this.setVisible(false);
			}
		});
		contentPane.add(btnSend, BorderLayout.SOUTH);
	}

	private DefaultListModel<SiteCheckBox> buildModel(List<SiteInfo> sites) {
		ArrayList<SiteCheckBox> values = new ArrayList<>();
		for (SiteInfo siteInfo : sites) {
			values.add(new SiteCheckBox(siteInfo));
			for (LangFile file : siteInfo.files) {
				values.add(new SiteCheckBox(siteInfo, file));
			}
		}
		// SiteCheckBox[] { new SiteCheckBox("1"), new SiteCheckBox("2"), new SiteCheckBox("3") };

		DefaultListModel<SiteCheckBox> model = new DefaultListModel<SiteCheckBox>() {
			@Override
			public int getSize() {
				return values.size();
			}

			@Override
			public SiteCheckBox get(int index) {
				return getElementAt(index);
			};
			
			@Override
			public SiteCheckBox getElementAt(int index) {
				return values.get(index);
			}
		};
		for (SiteCheckBox value : values) {
			value.setParentModel(model);
		}

		return model;
	}

}
