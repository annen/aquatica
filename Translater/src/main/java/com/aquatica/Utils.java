package com.aquatica;

import java.awt.Desktop;
import java.awt.FileDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;

import com.aquatica.Translater.LangFile;
import com.aquatica.Translater.SiteInfo;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.mashape.unirest.http.Unirest;

final class Utils {
	
	private Utils() {
	}
	
	static List<String> getFileList(Session session, String parentPath) throws JSchException, IOException {
		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand("find " + parentPath);
		channel.setInputStream(null);
		((ChannelExec) channel).setErrStream(System.err);
		InputStream in = channel.getInputStream();
		
		channel.connect();
		
		StringBuilder sb = new StringBuilder();
		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0) {
					break;
				}
				sb.append(new String(tmp, 0, i));
			}
			if (channel.isClosed()) {
				if (in.available() > 0) {
					continue;
				}
				if (channel.getExitStatus() != 0)
					throw new RuntimeException("Invalid status code when get file list");
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}
		channel.disconnect();
		List<String> files = new ArrayList<>();
		for (String file : sb.toString().split("\r?\n")) {
			file = file.replace(parentPath, "");
			if (file.isEmpty() || file.equals("/")) {
				continue;
			}
			files.add(file);
		}
		return files;
	}
	
	private static final String KEY_PATH = "C:\\Users\\Annenkov\\.ssh\\id_rsa";
	
	static void downloadTranslates(SiteInfo siteInfo) throws JSchException, IOException, Exception {
		LOG.log("Connect to " + siteInfo.host);
		JSch jsch = new JSch();
		Session session = jsch.getSession("root", siteInfo.host, 22);
		jsch.addIdentity(KEY_PATH);
		session.setUserInfo(new UiUserInfo());
		session.connect();
		LOG.log("Get files list from " + siteInfo.host);
		List<String> files = getFileList(session, siteInfo.path);
		File home = new File("./temp/" + siteInfo.host);
		if (home.exists()) {
			deleteDir(home);
		}
		home.mkdirs();
		siteInfo.files.clear();
		for (String file : files) {
			File outFile = copyRemoteToLocal(session, siteInfo.path, home, file);
			if (outFile != null) {
				LangFile lang = parseLand(outFile, file);
				if (lang != null) {
					siteInfo.files.add(lang);
					
				}
			}
		}
		session.disconnect();
	}
	
	static File copyRemoteToLocal(Session session, String from, File to, String fileName) throws Exception {
		
		if (!fileName.contains(".json") && !fileName.contains(".php")) {
			File dir = new File(to, fileName);
			dir.mkdirs();
			return null;
		}
		
		from = from + fileName;
		String prefix = null;
		
		if (to.isDirectory()) {
			prefix = to.getAbsolutePath() + File.separator;
		}
		
		// exec 'scp -f rfile' remotely
		String command = "scp -f " + from;
		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);
		
		// get I/O streams for remote scp
		OutputStream out = channel.getOutputStream();
		InputStream in = channel.getInputStream();
		
		channel.connect();
		
		byte[] buf = new byte[1024];
		
		// send '\0'
		buf[0] = 0;
		out.write(buf, 0, 1);
		out.flush();
		File outFile = new File(prefix + fileName);
		
		while (true) {
			int c = checkAck(in);
			if (c != 'C') {
				break;
			}
			
			// read '0644 '
			in.read(buf, 0, 5);
			
			long filesize = 0L;
			while (true) {
				if (in.read(buf, 0, 1) < 0) {
					// error
					break;
				}
				if (buf[0] == ' ') {
					break;
				}
				filesize = filesize * 10L + buf[0] - '0';
			}
			
			// read file name
			for (int i = 0;; i++) {
				in.read(buf, i, 1);
				if (buf[i] == (byte) 0x0a) {
					break;
				}
			}
			
			LOG.log("HOST : " + to.getName() + "      FILE : " + fileName + "      size : " + filesize);
			
			// send '\0'
			buf[0] = 0;
			out.write(buf, 0, 1);
			out.flush();
			
			// read a content of lfile
			
			try (FileOutputStream fos = new FileOutputStream(outFile)) {
				int x;
				while (true) {
					if (buf.length < filesize) {
						x = buf.length;
					} else {
						x = (int) filesize;
					}
					x = in.read(buf, 0, x);
					if (x < 0) {
						// error
						break;
					}
					fos.write(buf, 0, x);
					filesize -= x;
					if (filesize == 0L) {
						break;
					}
				}
				
				if (checkAck(in) != 0)
					throw new RuntimeException("Invalid ACK");
				// send '\0'
				buf[0] = 0;
				out.write(buf, 0, 1);
				out.flush();
				
				if (fos != null) {
					fos.close();
				}
			}
		}
		channel.disconnect();
		return outFile;
	}
	
	static int checkAck(InputStream in) throws IOException {
		int b = in.read();
		// b may be 0 for success,
		// 1 for error,
		// 2 for fatal error,
		// -1
		if (b == 0)
			return b;
		if (b == -1)
			return b;
		
		if (b == 1 || b == 2) {
			StringBuffer sb = new StringBuffer();
			int c;
			do {
				c = in.read();
				sb.append((char) c);
			} while (c != '\n');
			if (b == 1) { // error
				LOG.logError(sb.toString());
			}
			if (b == 2) { // fatal error
				LOG.logError(sb.toString());
			}
		}
		return b;
	}
	
	static void uploadTranslates(SiteInfo siteInfo, Collection<LangFile> files) throws JSchException, IOException, Exception {
		LOG.log("Connect to " + siteInfo.host);
		JSch jsch = new JSch();
		Session session = jsch.getSession("root", siteInfo.host, 22);
		jsch.addIdentity(KEY_PATH);
		session.setUserInfo(new UiUserInfo());
		session.connect();
		for (LangFile file : files) {
			if (file.finishFile == null)
				throw new RuntimeException("Invalid Finish File");
			String serverPath = siteInfo.path + file.fileKey;
			LOG.log("Send file " + file.fileKey + " to " + serverPath);
			copyLocalToRemote(session, file.finishFile, serverPath);
		}

		session.disconnect();
	}
	
	static void copyLocalToRemote(Session session, File from, String to) throws Exception {
		
		// exec 'scp -t rfile' remotely
		to = to.replace("'", "'\"'\"'");
		to = "'" + to + "'";
		String command = "scp " + " -t " + to;
		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);
		
		// get I/O streams for remote scp
		OutputStream out = channel.getOutputStream();
		InputStream in = channel.getInputStream();
		
		channel.connect();
		
		if (checkAck(in) != 0)
			throw new RuntimeException("Invalid ACK");
		
		// send "C0644 filesize filename", where filename should not include '/'
		long filesize = from.length();
		command = "C0644 " + filesize + " ";
		command += from.getName();
		command += "\n";
		out.write(command.getBytes());
		out.flush();
		if (checkAck(in) != 0)
			throw new RuntimeException("Invalid ACK");
		
		// send a content of lfile
		FileInputStream fis = new FileInputStream(from);
		byte[] buf = new byte[1024];
		while (true) {
			int len = fis.read(buf, 0, buf.length);
			if (len <= 0) {
				break;
			}
			out.write(buf, 0, len);
		}
		out.flush();
		fis.close();
		fis = null;
		// send '\0'
		buf[0] = 0;
		out.write(buf, 0, 1);
		out.flush();
		if (checkAck(in) != 0)
			throw new RuntimeException("Invalid ACK");
		out.close();
		
		channel.disconnect();
	}
	
	private static final Pattern PHP_LINE_KEY = Pattern.compile("\\$_\\[['\"](.+)['\"]\\]\\s*=\\s*['\"](.*)['\"]\\s*;");
	
	static LangFile parseLand(File outFile, String fileKey) throws Exception {
		LangFile file = new LangFile(outFile, fileKey);
		if (outFile.getName().toLowerCase().endsWith(".json"))
			return null;
		// JSONTokener tokener = new JSONTokener(new BufferedInputStream(new FileInputStream(outFile)));
		// JSONObject root = new JSONObject(tokener);
		// for (Object key : root.keySet()) {
		// file.values.put((String) key, root.getString((String) key));
		// }
		if (outFile.getName().toLowerCase().endsWith(".php")) {
			List<String> lines = Files.readAllLines(outFile.toPath());
			for (String line : lines) {
				line = line.trim();
				if (!line.startsWith("$_[")) {
					continue;
				}
				Matcher matcher = PHP_LINE_KEY.matcher(line);
				if (matcher.find()) {
					file.values.put(matcher.group(1), matcher.group(2));
				}
			}
		}
		return file;
	}
	
	static void registerCert() {
		LOG.logInfo("REGISTER WHORE CERTIFICATE");
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}
					
					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					
					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};
		
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			SSLContext sslcontext = SSLContexts.custom()
					.loadTrustMaterial(null, new TrustSelfSignedStrategy())
					.build();
			
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
			CloseableHttpClient httpclient = HttpClients.custom()
					.setSSLSocketFactory(sslsf)
					.build();
			Unirest.setHttpClient(httpclient);
		} catch (GeneralSecurityException e) {
		}
	}
	
	static void deleteDir(File dir) throws Exception {
		if (!dir.exists())
			return;
		if (dir.isDirectory()) {
			for (File c : dir.listFiles()) {
				deleteDir(c);
			}
		}
		if (dir.exists()) {
			dir.delete();
		}
	}
	
	static File selectFile(JFrame frame) {
		FileDialog fileDialog = new FileDialog(frame, "Select file to export", FileDialog.SAVE);
		String pref = "Transtales_export";
		fileDialog.setFile(pref + "_" + new DateTime().toString("yyyy-MM-dd") + ".xls");
		fileDialog.setVisible(true);
		String filename = fileDialog.getFile();
		fileDialog.setVisible(false);
		
		File file = new File(fileDialog.getDirectory(), filename);
		if (file.exists()) {
			file.delete();
		}
		return file;
	}

	static void openFile(File file) {
		int result = JOptionPane.showConfirmDialog(null, "Export complete. Open file?", "Export complete.",
				JOptionPane.YES_NO_OPTION);
		if (result == JOptionPane.YES_OPTION) {
			try {
				Desktop.getDesktop().open(file);
			} catch (Exception ex) {
				LOG.logError(ex, "Problem with open file " + file.getAbsolutePath());
			}
		}
	}
	
}
