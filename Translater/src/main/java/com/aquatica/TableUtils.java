package com.aquatica;

import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.DefaultTableModel;

public final class TableUtils {
	
	private TableUtils() {
	}
	
	static int lastSearchRow = -1;
	static int lastSearchCol = -1;
	
	static void searchNext(JTable table, String text) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		if (model.getRowCount() < 1 || model.getColumnCount() < 1) {
			JOptionPane.showMessageDialog(null, "Not Found");
			return;
		}
		
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		if (row < 0) {
			row = lastSearchRow;
		}
		if (col < 0) {
			col = lastSearchCol;
		}
		if (row < 0) {
			row = 0;
		}
		if (col < 0) {
			col = 0;
		}
		col = col + 1;
		if (col >= table.getColumnCount()) {
			col = 1;
			row = row + 1;
		}
		if (row > table.getRowCount()) {
			row = 0;
			col = 1;
		}
		
		int startRow = row;
		int startCol = col;
		boolean found = false;
		do {
			Object value = model.getValueAt(row, table.convertColumnIndexToModel(col));
			if (value instanceof String) {
				if (((String) value).toLowerCase().contains(text)) {
					found = true;
					break;
				}
			}
			col++;
			
			if (col >= table.getColumnCount()) {
				col = 1;
				row = row + 1;
			}
			if (row >= table.getRowCount()) {
				row = 0;
				col = 1;
			}
			
		} while (startRow != row || startCol != col);
		
		if (!found) {
			JOptionPane.showMessageDialog(null, "Not Found");
		}
		showCell(row, col, table);
	}
	
	static void showCell(int row, int col, JTable table) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		scrollToVisible(table, row);
		table.changeSelection(row, row, true, true);
		int startRow = lastSearchRow;
		int startCol = lastSearchCol;
		lastSearchRow = row;
		lastSearchCol = col;
		model.fireTableCellUpdated(startRow, table.convertColumnIndexToModel(startCol));
		model.fireTableCellUpdated(row, table.convertColumnIndexToModel(lastSearchCol));
	}
	
	static void scrollToVisible(JTable table, int rowIndex) {
		if (!(table.getParent() instanceof JViewport))
			return;
		JViewport viewport = (JViewport) table.getParent();
		Rectangle rect = table.getCellRect(rowIndex, 0, true);
		Point pt = viewport.getViewPosition();
		rect.setLocation(rect.x - pt.x, rect.y - pt.y);
		table.scrollRectToVisible(rect);
	}
	
}
