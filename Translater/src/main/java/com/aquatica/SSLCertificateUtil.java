package com.aquatica;

import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.net.ssl.*;


public class SSLCertificateUtil {

	private static class TrustAllManager implements X509TrustManager {
		private static TrustAllManager INSTANCE = new TrustAllManager();

		public static TrustAllManager instance() {
			return INSTANCE;
		}

        @Override
		public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        @Override
		public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
        @Override
		public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
	}

	public static TrustManager getTrustAllManager() {
		return TrustAllManager.instance();
	}

	public static SSLSocketFactory getTrustAllSSLSocketFactory() throws GeneralSecurityException {
		TrustManager[] trustAllCerts = new TrustManager[] {
			getTrustAllManager()
		};
		SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        return sc.getSocketFactory();
	}

	public static void setTrustAllSocketFactory(URLConnection conn) throws GeneralSecurityException {
		if (conn instanceof HttpsURLConnection) {
			HttpsURLConnection c = (HttpsURLConnection) conn;
			c.setSSLSocketFactory(getTrustAllSSLSocketFactory());
		}
	}

	private static class TrustAllHostnameVerifier implements HostnameVerifier {
		private static TrustAllHostnameVerifier INSTANCE = new TrustAllHostnameVerifier();

		public static TrustAllHostnameVerifier instance() {
			return INSTANCE;
		}

		@Override
		public boolean verify(String urlHostname, SSLSession certHostname) {
			return true;
		}
	}

	public static HostnameVerifier getTrustAllHostnameVerifier() {
		return TrustAllHostnameVerifier.instance();
	}

	public static void setTrustAllHostnameVerifier(URLConnection conn) {
		if (conn instanceof HttpsURLConnection) {
			HttpsURLConnection c = (HttpsURLConnection) conn;
			c.setHostnameVerifier(TrustAllHostnameVerifier.instance());
		}
	}

	public static void setTrustAll(URLConnection conn) throws GeneralSecurityException {
		setTrustAllSocketFactory(conn);
		setTrustAllHostnameVerifier(conn);
	}

	private static class SslState {
		SSLSocketFactory socketFactory;
		HostnameVerifier hostnameVerifier;
	}

	private static final ThreadLocal<SslState> localSslState = new ThreadLocal<SslState>();

	public static void setDefaultTrustAll() throws GeneralSecurityException {
		SslState state = new SslState();
		state.socketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
		state.hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
		localSslState.set(state);

		try {
			HttpsURLConnection.setDefaultSSLSocketFactory(getTrustAllSSLSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(getTrustAllHostnameVerifier());
		} catch (RuntimeException e) {
			restoreDefault();
			throw e;
		} catch (GeneralSecurityException e) {
			restoreDefault();
			throw e;
		}
	}

	public static void restoreDefault() {
		SslState state = localSslState.get();
		if (state == null)
			return;
		HttpsURLConnection.setDefaultSSLSocketFactory(state.socketFactory);
		HttpsURLConnection.setDefaultHostnameVerifier(state.hostnameVerifier);
	}

}
