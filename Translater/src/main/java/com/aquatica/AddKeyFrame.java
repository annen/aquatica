package com.aquatica;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class AddKeyFrame extends JFrame {
	
	private JPanel contentPane;
	private JTextField txtKey;
	private JTextField txtEng;
	private JTextField txtRus;
	private JTextField txtIt;
	private JTextField txtEs;
	private JTextField txtFr;
	
	/**
	 * Create the frame.
	 */
	public AddKeyFrame(DefaultTableModel model) {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.setBounds(10, 26, 400, 20);
		panel.add(comboBox);
		
		Set<String> keys = new LinkedHashSet<>();
		for (int r = 0; r < model.getRowCount(); r++) {
			String value = (String) model.getValueAt(r, 0);
			if (value != null) {
				keys.add(value.trim());
			}
		}
		
		DefaultComboBoxModel<String> cModel = new DefaultComboBoxModel<>(keys.toArray(new String[0]));
		comboBox.setModel(cModel);
		
		if (!keys.isEmpty()) {
			comboBox.setSelectedIndex(0);
		}
		
		JLabel lblSelectFileFor = new JLabel("Select file for key");
		lblSelectFileFor.setBounds(10, 11, 400, 14);
		panel.add(lblSelectFileFor);
		
		JLabel lblKey = new JLabel("Key");
		lblKey.setBounds(10, 70, 80, 14);
		panel.add(lblKey);
		
		txtKey = new JTextField();
		txtKey.setBounds(10, 85, 400, 20);
		panel.add(txtKey);
		txtKey.setColumns(10);
		
		JLabel lblEnglish = new JLabel("English");
		lblEnglish.setBounds(10, 125, 80, 14);
		panel.add(lblEnglish);
		
		txtEng = new JTextField();
		txtEng.setBounds(10, 140, 400, 20);
		txtEng.setColumns(10);
		panel.add(txtEng);
		
		JLabel lblRussian = new JLabel("Russian");
		lblRussian.setBounds(10, 170, 80, 14);
		panel.add(lblRussian);
		
		txtRus = new JTextField();
		txtRus.setText("");
		txtRus.setBounds(10, 185, 400, 20);
		txtRus.setColumns(10);
		panel.add(txtRus);
		
		JLabel lblItalian = new JLabel("Italian");
		lblItalian.setBounds(10, 215, 80, 14);
		panel.add(lblItalian);
		
		txtIt = new JTextField();
		txtIt.setText("");
		txtIt.setBounds(10, 230, 400, 20);
		txtIt.setColumns(10);
		panel.add(txtIt);
		
		JLabel lblEs = new JLabel("Spain / MX");
		lblEs.setBounds(10, 260, 80, 14);
		panel.add(lblEs);
		
		txtEs = new JTextField();
		txtEs.setText("");
		txtEs.setBounds(10, 275, 400, 20);
		txtEs.setColumns(10);
		panel.add(txtEs);
		
		JLabel lblFr = new JLabel("Francian");
		lblFr.setBounds(10, 305, 80, 14);
		panel.add(lblFr);
		
		txtFr = new JTextField();
		txtFr.setText("");
		txtFr.setBounds(10, 320, 400, 20);
		txtFr.setColumns(10);
		panel.add(txtFr);
		
		JButton btnAddKey = new JButton("Add Key");
		btnAddKey.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() >= 0) {
					String keyFile = (String) comboBox.getSelectedItem();
					int lastFoundR = -1;
					for (int r = 0; r < model.getRowCount(); r++) {
						String value = (String) model.getValueAt(r, 0);
						if (value != null && value.trim().equalsIgnoreCase(keyFile)) {
							lastFoundR = r;
						}
					}
					if (lastFoundR >= 0) {
						String fileName = (String) model.getValueAt(lastFoundR, 0);
						String key = txtKey.getText();
						String value = txtEng.getText();
						String eng = txtEng.getText();
						String ru = txtRus.getText();
						String it = txtIt.getText();
						String es = txtEs.getText();
						String fr = txtFr.getText();
						model.insertRow(lastFoundR + 1, new String[] {
								fileName, key, value, eng, eng, eng, eng, eng, ru, it, es, fr
						});
					}
				}
				AddKeyFrame.this.setVisible(false);
			}
		});
		btnAddKey.setBounds(170, 355, 100, 30);
		panel.add(btnAddKey);
	}
}
