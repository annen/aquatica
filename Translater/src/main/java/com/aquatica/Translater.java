package com.aquatica;

import static com.aquatica.TableUtils.*;
import static com.aquatica.Utils.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.codec.Charsets;

public class Translater {

	private static final String ENGLISH = "english";
	private static final String RUSSIAN = "russian";
	private static final String ITALIAN = "italian";
	private static final String SPANISH = "spanish";
	private static final String FRENCH = "french";

	static class LangFile {
		final File serverFile;
		final String fileKey;
		final Map<String, String> values = new LinkedHashMap<>();
		File finishFile;

		public LangFile(File serverFile, String fileKey) {
			this.serverFile = serverFile;
			this.fileKey = fileKey.toLowerCase();
		}
	}

	static class SiteInfo {
		final String lang;
		final String host;
		final String path;
		final List<LangFile> files = new ArrayList<>();

		public SiteInfo(String lang, String host, String path) {
			this.lang = lang;
			this.host = host;
			this.path = "/var/www/" + path + "/app/language/" + lang + "/";
		}

		public String searchValue(String fileKey, String key) {
			for (LangFile langFile : files) {
				if (langFile.fileKey.equalsIgnoreCase(fileKey)) {
					String value = langFile.values.getOrDefault(key, "");
					if (!value.isEmpty())
						return value;
				}
			}
			return "";
		}

		public LangFile searchFile(String path) {
			for (LangFile langFile : files) {
				if (path.toLowerCase().contains(langFile.fileKey))
					return langFile;
			}
			return null;
		}

	}

	private JFrame frame;
	private DefaultTableModel model;
	private List<SiteInfo> sites = Arrays.<SiteInfo> asList(
			new SiteInfo(ENGLISH, "dev.aquaticaplumbing.com", "aquatica-new"),
			new SiteInfo(ENGLISH, "www.aquaticausa.com", "aquaticausa.com"),
			new SiteInfo(ENGLISH, "www.aquaticabath.ca", "aquaticabath.ca"),
			new SiteInfo(ENGLISH, "www.aquaticabath.co.uk", "aquaticabath.co.uk"),
			new SiteInfo(ENGLISH, "www.aquaticabath.eu", "aquaticabath.eu"),
			new SiteInfo(RUSSIAN, "www.vivalusso.ru", "aquaticavanni.ru"),
			new SiteInfo(ITALIAN, "www.aquaticabagno.it", "aquaticabagno.it"),
			new SiteInfo(SPANISH, "www.aquaticabano.com.mx", "aquaticabano.com.mx"),
			new SiteInfo(FRENCH, "www.aquaticabain.fr", "aquaticabain.fr"));
	private SiteInfo original;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				LOG.init();
			}
		});
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Translater window = new Translater();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public Translater() {
		try {
			initialize();
		} catch (Exception e) {
			new RuntimeException(e);
		}
	}

	private JTable table;
	private JTextField txtSearch;

	private void initialize() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		frame = new JFrame();
		frame.setBounds(50, 50, 1000, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(10, 0));

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(10);
		panel.add(panel_1, BorderLayout.WEST);

		JButton btnReadFiles = new JButton("Read Server Files");
		panel_1.add(btnReadFiles);

		JButton btnLocReset = new JButton("Localization Reset");
		btnLocReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					sendResetLocalization();
					LOG.logSuccess("Localization reset COMPLETE");
				} catch (Exception ex) {
					LOG.logError(ex, ex.getMessage());
				}
			}
		});

		JButton btnBuildFolders = new JButton("Build Language Folders");
		btnBuildFolders.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					buildFolders();
					LOG.logSuccess("Rebuild folders COMPLETE");
				} catch (Exception ex) {
					LOG.logError(ex, ex.getMessage());
				}
			}
		});
		panel_1.add(btnBuildFolders);
		panel_1.add(btnLocReset);

		JButton btnSendFiles = new JButton("Send Files To Servers");
		btnSendFiles.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					buildFolders();
					LOG.logSuccess("Rebuild folders COMPLETE");
					SendFilesConfig config = new SendFilesConfig(sites);
					config.setVisible(true);
					// uploadTranslates(sites.get(0), Arrays.asList(sites.get(0).files.get(0)));
				} catch (Exception ex) {
					LOG.logError(ex, ex.getMessage());
				}
			}
		});
		panel_1.add(btnSendFiles);
		
		JButton btnAddNewTranslate = new JButton("Add New Translate Key");
		btnAddNewTranslate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddKeyFrame frame = new AddKeyFrame(model);
				frame.setVisible(true);
			}
		});
		panel_1.add(btnAddNewTranslate);
		
		JButton btnExportToFile = new JButton("Export To File");
		btnExportToFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							File file = Utils.selectFile(frame);
							new XlsExporter().doExport(model, file);
							openFile(file);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				t.start();
			}
		});
		panel_1.add(btnExportToFile);
		btnReadFiles.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							getFilesFromServers();
							LOG.logSuccess("Get Server Files COMPLETE");
						} catch (Exception ex) {
							LOG.logError(ex, ex.getMessage());
						}
					}

				});
				t.start();
			}

		});

		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

		model = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
				});
		table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(model);
		table.setCellSelectionEnabled(true);

		table.setDefaultRenderer(String.class, new DefaultTableCellRenderer() {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				String s = ((String) value);
				Component component = super.getTableCellRendererComponent(table, s.startsWith("   ") ? "" : s, isSelected, hasFocus, row, column);
				if (s.startsWith("   ")) {
					component.setBackground(new Color(230, 230, 230));
				} else {
					if (isSelected) {
						component.setBackground(table.getSelectionBackground());
					} else {
						component.setBackground(Color.WHITE);
					}
				}
				if (row == lastSearchRow && column == lastSearchCol) {
					component.setBackground(new Color(200, 255, 255));
				}
				return component;
			}
		});
		
		InputMap im = table.getInputMap(JTable.WHEN_FOCUSED);
		ActionMap am = table.getActionMap();
		
		Action copyAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn >= 0) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					String data = (String) model.getValueAt(selectedRow, selectedColumn);
					StringSelection stringSelection = new StringSelection(data);
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
				}
			}
		};
		
		Action pasteAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.convertColumnIndexToModel(table.getSelectedColumn());
				if (selectedRow >= 0 && selectedColumn >= 0) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					Transferable data = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
					if (data != null && data.isDataFlavorSupported(DataFlavor.stringFlavor)) {
						try {
							model.setValueAt(data.getTransferData(DataFlavor.stringFlavor), selectedRow, selectedColumn);
							model.fireTableCellUpdated(selectedRow, selectedColumn);
						} catch (Exception ex) {
							LOG.logError(ex, "Problem with paste value");
						}
					}
				}
			}
		};
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK), "Copy");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK), "Paste");
		am.put("Copy", copyAction);
		am.put("Paste", pasteAction);

		scrollPane.setViewportView(table);

		JPanel panel_2 = new JPanel();
		frame.getContentPane().add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));

		JPanel panelSearch = new JPanel();
		panel_2.add(panelSearch, BorderLayout.WEST);

		JLabel lblSearch = new JLabel("Search: ");
		panelSearch.add(lblSearch);

		txtSearch = new JTextField();
		panelSearch.add(txtSearch);
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10) {
					searchNext(table, txtSearch.getText().toLowerCase());
				}
			}
		});
		txtSearch.setColumns(50);

	}

	private void sendResetLocalization() throws MalformedURLException, IOException, ProtocolException {
		registerCert();
		for (SiteInfo siteInfo : sites) {
			String url = "https://" + siteInfo.host + "/api/LocalizationReset";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Aquatica Translater");
			con.getResponseCode();
			LOG.log("Send request to " + url);
		}
	}

	private void getFilesFromServers() throws Exception {
		registerCert();
		final CountDownLatch latch = new CountDownLatch(sites.size());
		for (SiteInfo siteInfo : sites) {
			new Thread(() -> {
				try {
					downloadTranslates(siteInfo);
				} catch (Exception ex) {
					LOG.logError(ex, "Problem with download for site " + siteInfo.host);
				}
				LOG.log("Download Complete " + siteInfo.host);
				latch.countDown();
			}).start();

		}
		latch.await();
		SiteInfo original = initOriginal(sites);
		renderTable(original);
	}

	private void buildFolders() throws Exception {
		File parent = new File("./new/");
		if (parent.exists()) {
			deleteDir(parent);
		}
		Thread.sleep(500);
		parent.mkdir();

		int col = 3;
		for (SiteInfo siteInfo : sites) {
			File home = new File(parent, siteInfo.host + "/" + siteInfo.lang);
			home.mkdirs();
			String lastFileName = null;
			StringBuilder sb = new StringBuilder();
			for (int row = 0; row < model.getRowCount(); row++) {
				String fileName = (String) model.getValueAt(row, 0);
				String key = (String) model.getValueAt(row, 1);
				String value = (String) model.getValueAt(row, col);
				fileName = fileName.trim();
				key = key.trim();
				if (!key.isEmpty() && !value.isEmpty()) {
					sb.append("$_['" + key + "']	=	'" + escape(value) + "';").append("\n");
				}
				if (!fileName.equalsIgnoreCase(lastFileName)) {
					if (sb.length() > 0 && lastFileName != null) {
						File file = new File(home, lastFileName);
						file.getParentFile().mkdirs();
						BufferedWriter bw = Files.newBufferedWriter(file.toPath(), Charsets.UTF_8, StandardOpenOption.CREATE_NEW);
						bw.append("<?php\n");
						bw.append(sb.toString());
						bw.append("?>");
						bw.flush();
						bw.close();
						LangFile langFile = siteInfo.searchFile(lastFileName);
						if (langFile != null) {
							langFile.finishFile = file;
						} else {
							LOG.logWarning("Not found lang file for " + lastFileName);
						}
					}
					sb = new StringBuilder();
					lastFileName = fileName;
				}
			}
			if (sb.length() > 0 && lastFileName != null) {
				File file = new File(home, lastFileName);
				file.getParentFile().mkdirs();
				BufferedWriter bw = Files.newBufferedWriter(file.toPath(), Charsets.UTF_8, StandardOpenOption.CREATE_NEW);
				bw.append("<?php\n");
				bw.append(sb.toString());
				bw.append("?>");
				bw.flush();
				bw.close();
				LangFile langFile = siteInfo.searchFile(lastFileName);
				if (langFile != null) {
					langFile.finishFile = file;
				} else {
					LOG.logWarning("Not found lang file for " + lastFileName);
				}
			}
			LOG.log("Build folder for " + siteInfo.host);
			col++;
		}
	}

	private String escape(String value) {
		int start = -1;
		while (true) {
			int index = value.indexOf("'", start);
			if (index > 0) {
				if (value.charAt(index - 1) != '\\') {
					value = value.substring(0, index) + "\\" + value.substring(index);
					index++;
				}
				start = index + 1;
			} else {
				break;
			}
		}
		return value;
	}

	private void renderTable(SiteInfo original) {
		int rowCount = 0;
		for (LangFile file : original.files) {
			rowCount++;
			rowCount = rowCount + file.values.size();
		}
		List<String> columns = new ArrayList<>();
		columns.add("FILE");
		columns.add("KEY");
		columns.add("VALUE");
		for (SiteInfo siteInfo : sites) {
			columns.add(siteInfo.lang.toUpperCase() + " - " + siteInfo.host);
		}

		model = new DefaultTableModel(new Object[rowCount][columns.size()],
				columns.toArray(new String[columns.size()])) {
			@Override
			public Class<String> getColumnClass(int columnIndex) {
				return String.class;
			}

			@Override
			public String getColumnName(int column) {
				return columns.get(column);
			}
		};
		table.setModel(model);

		int row = 0;
		for (LangFile lFile : original.files) {
			String fileKey = lFile.fileKey;
			model.setValueAt(fileKey, row, 0);
			model.setValueAt("   ", row, 1);
			model.setValueAt("   ", row, 2);
			int col = 3;
			for (SiteInfo siteInfo : sites) {
				model.setValueAt("   ", row, col++);
			}
			row++;
			for (Entry<String, String> e : lFile.values.entrySet()) {
				String key = e.getKey();
				model.setValueAt("   " + fileKey, row, 0);
				model.setValueAt(key, row, 1);
				model.setValueAt(e.getValue(), row, 2);
				col = 3;
				for (SiteInfo siteInfo : sites) {
					String value = siteInfo.searchValue(fileKey, key);
					model.setValueAt(value, row, col++);
				}
				row++;
			}
		}
	}

	private static SiteInfo initOriginal(List<SiteInfo> sites) {
		Map<String, LangFile> files = new LinkedHashMap<>();
		for (SiteInfo siteInfo : sites) {
			for (LangFile file : siteInfo.files) {
				String fileKey = file.fileKey;
				LangFile langFile = files.get(fileKey);
				if (langFile == null) {
					langFile = new LangFile(file.serverFile, fileKey);
					files.put(fileKey, langFile);
				}

				for (Entry<String, String> e : file.values.entrySet()) {
					String key = e.getKey();
					Map<String, String> values = langFile.values;
					if (!values.containsKey(key) || values.get(key).isEmpty()) {
						if (siteInfo.lang.equalsIgnoreCase(ENGLISH)) {
							values.put(key, e.getValue());
						} else {
							values.put(key, "");
						}
					}
				}
			}
		}
		SiteInfo original = new SiteInfo(ENGLISH, "", "");
		original.files.addAll(files.values());
		return original;
	}

}
